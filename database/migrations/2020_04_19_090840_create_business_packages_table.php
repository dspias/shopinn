<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_packages', function (Blueprint $table) {
            $table->id();

            $table->string('name');

            $table->tinyInteger('package_type');

            $table->decimal('first_percentage', 8, 2)->nullable();
            $table->tinyInteger('first_day')->nullable();

            $table->decimal('total_percentage', 8, 2)->nullable();
            $table->decimal('total_payment', 8, 2)->nullable();
            $table->tinyInteger('year')->nullable();
            $table->tinyInteger('month')->nullable();
            $table->tinyInteger('day')->nullable();
            $table->longText('description');

            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_packages');
        Schema::table("business_packages", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
