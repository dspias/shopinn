<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiderDeliveryOrderListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_delivery_order_lists', function (Blueprint $table) {
            $table->id();

            $table->foreignId('report_id');
            $table->foreign('report_id')->references('id')->on('rider_delivery_reports');

            $table->foreignId('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rider_delivery_order_lists');
        Schema::table("rider_delivery_order_lists", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
