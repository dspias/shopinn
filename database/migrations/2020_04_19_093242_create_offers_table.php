<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->foreignId('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products');

            $table->string('title');
            $table->tinyInteger('discount');

            $table->date('expire_date');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
        Schema::table("offers", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
