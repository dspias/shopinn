<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();

            $table->foreignId('order_id');
            $table->foreign('order_id')->references('id')->on('orders');

            $table->foreignId('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('quantity')->default(1);

            $table->string('color')->nullable();
            $table->string('size')->nullable();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->integer('price');           //if shopinn update price
            $table->integer('s_sell_price');    // shop provided price
            $table->integer('discount')->default(0);
            $table->boolean('paid')->default(false);    //paid from admin to vendor

            //income traceing
            $table->double('shop_earn', 9,4)->default(0.0000);
            $table->double('admin_earn', 9,4)->default(0.0000);

            $table->tinyInteger('status')->default(0);
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::table("order_items", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
