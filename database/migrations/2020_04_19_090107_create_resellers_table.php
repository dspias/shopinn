<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resellers', function (Blueprint $table) {
            $table->id();

            $table->foreignId('reseller_id');
            $table->foreign('reseller_id')->references('id')->on('users');

            $table->string('business_mobile');
            $table->string('business_email');
            
            
            $table->string('owner_name');
            $table->string('owner_mobile')->nullable();
            $table->string('owner_email')->nullable();
            $table->text('owner_address');

            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();

            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resellers');
        Schema::table("resellers", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
