<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->foreignId('role_id');
            $table->foreign('role_id')->references('id')->on('roles');

            $table->string('name');

            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();

            $table->string('mobile')->unique()->nullable();
            $table->timestamp('mobile_verified_at')->nullable();

            $table->string('password');

            $table->binary('avatar')->nullable();

            $table->string('lang')->default('en');

            $table->rememberToken();
            $table->timestamp('approved_at')->nullable();
            $table->boolean('is_active')->default(true);
            
            $table->string('city')->nullable();
            $table->text('address')->nullable();
            $table->text('note')->nullable();

            $table->integer('reward')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::table("users", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
