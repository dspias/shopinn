<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_packages', function (Blueprint $table) {
            $table->id();

            // $table->foreignId('type_id');
            // $table->foreign('type_id')->references('id')->on('advertise_types');

            $table->string('name');
            $table->integer('price');
            $table->boolean('is_active')->default(true);
            $table->text('details');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_packages');
        Schema::table("advertise_packages", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
