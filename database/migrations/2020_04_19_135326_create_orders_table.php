<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('oid')->unique();

            $table->foreignId('customer_id');
            $table->foreign('customer_id')->references('id')->on('users');

            $table->boolean('is_reseller')->default(false);
            $table->double('reseller_earn',9,4)->default(0.0000);
            $table->boolean('reseller_paid')->default(false);

            $table->foreignId('rider_id')->nullable();
            $table->foreign('rider_id')->references('id')->on('users');
            $table->double('rider_earn', 9, 4)->default(0.0000);
            $table->boolean('rider_paid')->default(false);

            $table->double('extra_cost', 9, 4)->default(0.0000);

            $table->foreignId('coupon_id')->nullable();
            $table->foreign('coupon_id')->references('id')->on('coupons');

            $table->string('name')->nullable();
            $table->string('city');
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->text('address');

            $table->tinyInteger('status')->default(0);
            $table->integer('delivery')->default(0);    // dellivery charge

            $table->tinyInteger('has_reviewed')->default(0);
            $table->timestamp('status_at')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::table("orders", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
