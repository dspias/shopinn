<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_logs', function (Blueprint $table) {
            $table->id();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->foreignId('package_id');
            $table->foreign('package_id')->references('id')->on('business_packages');

            $table->date('start');
            $table->date('end')->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('paid');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_logs');
        Schema::table("package_logs", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
