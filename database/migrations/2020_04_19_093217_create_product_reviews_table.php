<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->id();

            $table->foreignId('customer_id');
            $table->foreign('customer_id')->references('id')->on('users');

            $table->foreignId('product_id');
            $table->foreign('product_id')->references('id')->on('products');

            $table->text('review');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
        Schema::table("product_reviews", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
