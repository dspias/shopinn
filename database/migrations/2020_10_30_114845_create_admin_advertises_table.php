<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAdvertisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_advertises', function (Blueprint $table) {
            $table->id();

            $table->foreignId('type_id');
            $table->foreign('type_id')->references('id')->on('advertise_types');

            $table->string('title');
            $table->text('url_link');
            $table->integer('day')->min(0)->max(30);

            $table->date('start');
            $table->date('end');

            $table->binary('ad_image');
            $table->boolean('is_active')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_advertises');
        Schema::table("admin_advertises", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
