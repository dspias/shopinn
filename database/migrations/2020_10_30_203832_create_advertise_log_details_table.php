<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertiseLogDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_log_details', function (Blueprint $table) {
            $table->id();

            $table->foreignId('log_id');
            $table->foreign('log_id')->references('id')->on('advertise_logs');

            $table->foreignId('type_id');
            $table->foreign('type_id')->references('id')->on('advertise_types');

            $table->string('title');
            $table->text('url_link');

            $table->date('start');
            $table->date('end');

            $table->binary('ad_image');
            $table->boolean('is_active')->default(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_log_details');
        Schema::table("advertise_log_details", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
