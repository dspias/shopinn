<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertiseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_types', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->integer('width');
            $table->integer('height');
            $table->integer('min_size');
            $table->integer('max_size');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_types');
        Schema::table("advertise_types", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
