<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentOrderListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_order_lists', function (Blueprint $table) {
            $table->id();

            $table->foreignId('pay_rep_id');
            $table->foreign('pay_rep_id')->references('id')->on('payment_reports');

            $table->foreignId('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_order_lists');
        Schema::table("payment_order_lists", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
