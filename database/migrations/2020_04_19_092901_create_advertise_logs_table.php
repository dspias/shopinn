<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertiseLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_logs', function (Blueprint $table) {
            $table->id();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->foreignId('package_id');
            $table->foreign('package_id')->references('id')->on('advertise_packages');
            $table->tinyInteger('day');
            $table->date('start');
            $table->date('end');
            $table->boolean('is_active');

            $table->integer('price');
            $table->boolean('paid');
            $table->string('pay_by');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_logs');
        Schema::table("advertise_logs", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
