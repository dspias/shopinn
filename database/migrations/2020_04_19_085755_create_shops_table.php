<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->string('shop_mobile');
            $table->string('shop_email');
            $table->text('shop_address');
            
            
            $table->string('owner_name');
            $table->string('owner_mobile');
            $table->string('owner_email')->nullable();
            $table->text('owner_address')->nullable();

            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();

            $table->integer('inside_city')->nullable();
            $table->integer('outside_city')->nullable();
            
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
        Schema::table("shops", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
