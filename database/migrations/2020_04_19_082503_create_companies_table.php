<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();

            $table->foreignId('compnay_id');
            $table->foreign('compnay_id')->references('id')->on('users');

            $table->string('name')->unique();
            $table->string('type');
            $table->json('json_data')->nullable();
            $table->string('string_data')->nullable();
            $table->longText('text_data')->nullable();
            $table->integer('integer_data')->nullable();
            $table->double('double_data', 8, 2)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE companies ADD avatar MEDIUMBLOB AFTER type");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::table("companies", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
