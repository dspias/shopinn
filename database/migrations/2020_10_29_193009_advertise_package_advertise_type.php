<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdvertisePackageAdvertiseType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertise_package_advertise_type', function (Blueprint $table) {
            $table->id();

            $table->foreignId('advertise_type_id')
                ->constrained('advertise_types')
                ->onDelete('cascade');

            $table->foreignId('advertise_package_id')
                ->constrained('advertise_packages')
                ->onDelete('cascade');

            $table->integer('max_ad');

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertise_package_advertise_type');
    }
}
