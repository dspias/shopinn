<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->string('pid')->unique();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->foreignId('category_id');
            $table->foreign('category_id')->references('id')->on('product_categories');

            $table->longText('description');
            $table->string('name');
            $table->integer('stock');
            $table->double('buy_price', 9, 2)->nullable();
            $table->double('sell_price', 9, 2);
            $table->double('updated_price', 9, 2)->nullable();

            $table->boolean('is_active')->default(true);
            $table->boolean('is_featured')->default(false);

            $table->integer('reward')->default(0);

            $table->timestamp('approved_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::table("products", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
