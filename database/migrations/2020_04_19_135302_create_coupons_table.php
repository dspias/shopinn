<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();

            $table->foreignId('shop_id');
            $table->foreign('shop_id')->references('id')->on('users');

            $table->string('title');
            $table->string('code')->unique();
            $table->integer('discount');
            $table->integer('min_purchase');
            $table->tinyInteger('limit')->default(1);
            $table->date('expire_date');
            $table->tinyInteger('is_all')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
        Schema::table("coupons", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
