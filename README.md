## About SHOPINN-BD

**[Shopinn BD](http://shopinnbd.com)** is an online marketplace to connect local home-based businesses with its prospective customers in a wider segment of the market. The platform also aims to work with the local high street businesses to establish their online presence furthermore. 

In this platform, a customer can find his/her desired products in one place without shopping around too many doors.  It is a kind of virtual shopping mall for everyone.

In addition, we also work on a specific shop’s business growth by increasing their sale, minimizing their overhead costs, and focusing on online marketing issues. We come up with a time-consuming shopping idea where one can connect with all the local brands in one place.

## Developer Details

**[Shopinn BD](http://shopinnbd.com)** developed by two Developer *[Pias Das](https://gitlab.com/pd_sperrow)* and *[Nikhil Kurmi](https://gitlab.com/nikhil.lu210)* in 2020. 
