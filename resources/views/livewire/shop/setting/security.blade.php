<div>                              
    <div class="stg-form-area">
        <form wire:submit.prevent="changePassword" class="c-form">
            <div>
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
            <div>
                <label>Current Password</label>
                <input type="password" wire:model="current_password" placeholder="********" class="@error('current_password') is-invalid @enderror"/>
                @error('current_password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <label>{{ 'New Password' }}</label>
                <input type="password" wire:model="password"  placeholder="********" class="@error('password') is-invalid @enderror"/>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <label>Confirm Password</label>
                <input type="password" wire:model="password_confirmation" placeholder="********"/>
                @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <button type="submit">Save</button>
            </div>
        </form>
    </div>
</div>
