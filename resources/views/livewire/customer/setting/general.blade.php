<div>
    <div class="stg-form-area">
        <div>
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        <form wire:submit.prevent="saveInformation"class="c-form">
            <div>
                <label>Your Name</label>
                <input type="text" wire:model="name" value="{{ $name }}" placeholder="Jack Carter" class="@error('name') is-invalid @enderror"/>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <label>Email <sup class="text-info">username</sup></label>
                <input type="email" wire:model="email" value="{{ $email }}" placeholder="abc@pitnikmail.com" class="@error('email') is-invalid @enderror"/>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <label>Mobile <sup class="text-info">username</sup></label>
                <input type="text" wire:model="mobile" value="{{ $mobile }}" placeholder="01948819191" class="@error('mobile') is-invalid @enderror"/>
                @error('mobile')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label>{{ __('Select City') }} <sup>({{ __('District') }})</sup></label>
                <select wire:model="city" class="form-control @error('city') is-invalid @enderror" required style="height: 50px; background: #edf2f6; padding-left: 10px;">
                    <option value="" disabled>{{ __('Select City') }} ({{ __('District') }})</option>
                    @if ($user->city)
                    <option value="{{ $user->city }}" selected>{{ __($user->city) }}</option>
                    @endif
                    <option value="Bagerhat">{{ __('Bagerhat') }}</option>
                    <option value="Bandarban">{{ __('Bandarban') }}</option>
                    <option value="Barguna">{{ __('Barguna') }}</option>
                    <option value="Barisal">{{ __('Barisal') }}</option>
                    <option value="Bhola">{{ __('Bhola') }}</option>
                    <option value="Bogra">{{ __('Bogra') }}</option>
                    <option value="Brahmanbaria">{{ __('Brahmanbaria') }}</option>
                    <option value="Chandpur">{{ __('Chandpur') }}</option>
                    <option value="Chittagong">{{ __('Chittagong') }}</option>
                    <option value="Chuadanga">{{ __('Chuadanga') }}</option>
                    <option value="Comilla">{{ __('Comilla') }}</option>
                    <option value="Cox's Bazar">{{ __('Cox\'s Bazar') }}</option>
                    <option value="Dhaka">{{ __('Dhaka') }}</option>
                    <option value="Dinajpur">{{ __('Dinajpur') }}</option>
                    <option value="Faridpur">{{ __('Faridpur') }}</option>
                    <option value="Feni">{{ __('Feni') }}</option>
                    <option value="Gaibandha">{{ __('Gaibandha') }}</option>
                    <option value="Gazipur">{{ __('Gazipur') }}</option>
                    <option value="Gopalganj">{{ __('Gopalganj') }}</option>
                    <option value="Habiganj">{{ __('Habiganj') }}</option>
                    <option value="Jaipurhat">{{ __('Jaipurhat') }}</option>
                    <option value="Jamalpur">{{ __('Jamalpur') }}</option>
                    <option value="Jessore">{{ __('Jessore') }}</option>
                    <option value="Jhalakati">{{ __('Jhalakati') }}</option>
                    <option value="Jhenaidah">{{ __('Jhenaidah') }}</option>
                    <option value="Khagrachari">{{ __('Khagrachari') }}</option>
                    <option value="Khulna">{{ __('Khulna') }}</option>
                    <option value="Kishoreganj">{{ __('Kishoreganj') }}</option>
                    <option value="Kurigram">{{ __('Kurigram') }}</option>
                    <option value="Kushtia">{{ __('Kushtia') }}</option>
                    <option value="Lakshmipur">{{ __('Lakshmipur') }}</option>
                    <option value="Lalmonirhat">{{ __('Lalmonirhat') }}</option>
                    <option value="Madaripur">{{ __('Madaripur') }}</option>
                    <option value="Magura">{{ __('Magura') }}</option>
                    <option value="Manikganj">{{ __('Manikganj') }}</option>
                    <option value="Meherpur">{{ __('Meherpur') }}</option>
                    <option value="Moulvibazar">{{ __('Moulvibazar') }}</option>
                    <option value="Munshiganj">{{ __('Munshiganj') }}</option>
                    <option value="Mymensingh">{{ __('Mymensingh') }}</option>
                    <option value="Naogaon">{{ __('Naogaon') }}</option>
                    <option value="Narail">{{ __('Narail') }}</option>
                    <option value="Narayanganj">{{ __('Narayanganj') }}</option>
                    <option value="Narsingdi">{{ __('Narsingdi') }}</option>
                    <option value="Natore">{{ __('Natore') }}</option>
                    <option value="Nawabganj">{{ __('Nawabganj') }}</option>
                    <option value="Netrakona">{{ __('Netrakona') }}</option>
                    <option value="Nilphamari">{{ __('Nilphamari') }}</option>
                    <option value="Noakhali">{{ __('Noakhali') }}</option>
                    <option value="Pabna">{{ __('Pabna') }}</option>
                    <option value="Panchagarh">{{ __('Panchagarh') }}</option>
                    <option value="Parbattya Chattagram">{{ __('Parbattya Chattagram') }}</option>
                    <option value="Patuakhali">{{ __('Patuakhali') }}</option>
                    <option value="Pirojpur">{{ __('Pirojpur') }}</option>
                    <option value="Rajbari">{{ __('Rajbari') }}</option>
                    <option value="Rajshahi">{{ __('Rajshahi') }}</option>
                    <option value="Rangpur">{{ __('Rangpur') }}</option>
                    <option value="Satkhira">{{ __('Satkhira') }}</option>
                    <option value="Shariatpur">{{ __('Shariatpur') }}</option>
                    <option value="Sherpur">{{ __('Sherpur') }}</option>
                    <option value="Sirajganj">{{ __('Sirajganj') }}</option>
                    <option value="Sunamganj">{{ __('Sunamganj') }}</option>
                    <option value="Sylhet">{{ __('Sylhet') }}</option>
                    <option value="Tangail">{{ __('Tangail') }}</option>
                    <option value="Thakurgaon">{{ __('Thakurgaon') }}</option>
                </select>
                @error('city')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div>
                <label>Address</label>
                <input type="text" value="{{ $address }}" wire:model="address" placeholder="Address" class="@error('address') is-invalid @enderror">
                @error('address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div>
                <label>Language</label>

                <div class="form-radio">
                    <div class="radio">
                        <label>
                        <input wire:model="lang" type="radio" @if($lang == 'ban')checked="checked" @endif name="radio" value="ban" class="@error('lang') is-invalid @enderror"><i class="check-box"></i>Bangla
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                        <input wire:model="lang" type="radio"  @if($lang == 'en')checked="checked" @endif name="radio" value="en" class="@error('lang') is-invalid @enderror"><i class="check-box"></i>English
                        </label>
                    </div>
                </div>
                @error('lang')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <button>Save</button>
            </div>
        </form>
    </div>
</div>
