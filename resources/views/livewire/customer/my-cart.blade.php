<div>
    <a title="Your Cart Items" href="{{ route('customer.cart.index') }}" class="shopping-cart" data-toggle="tooltip">Cart <i class="fa fa-shopping-bag"></i>
        <span>{{ $cartTotal }}</span>
    </a>
    
</div>
