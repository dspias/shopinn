<div>
    <div class="member-des">
        <form wire:submit.prevent="submit">
        @csrf
            <div class="product-des">
                <table class="overview table table-bordered table-responsive">
                    <tbody>
                        @if (count($product->sizes) > 0)
                            <tr>
                                <th class="align-middle display-sm-none " style="width: 30%; font-weight: bold;">
                                    <span class="h6">{{ __('Size') }}</span>
                                </th>
                                <td class="align-middle" style="width: 70%;">
                                    <div class="size">
                                        @foreach ($product->sizes as $key => $size)
                                            <input type="radio" id="size_{{ $key }}_{{ $size->id }}" wire:model="size" value="{{ $size->name }}" name="size" class="@error('size') is-invalid @enderror"/>
                                            <label class="text-capitalize m-b-2" for="size_{{ $key }}_{{ $size->id }}">{{ __($size->name) }}</label>
                                        @endforeach

                                        @error('size')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </td>
                            </tr>
                        @endif

                        @if (count($product->colors) > 0)
                            <tr>
                                <th class="align-middle display-sm-none " style="width: 30%; font-weight: bold;">
                                    <span class="h6">{{ __('Color') }}</span>
                                </th>
                                <td class="align-middle" style="width: 70%;">
                                    <div class="color">
                                        @foreach ($product->colors as $key => $color)
                                            <input type="radio" id="color_{{ $key }}_{{ $color->id }}" wire:model="color" value="{{ $color->name }}" name="color" class="@error('color') is-invalid @enderror"/>
                                            <label class="text-capitalize m-b-2" for="color_{{ $key }}_{{ $color->id }}">{{ __($color->name) }}</label>
                                        @endforeach

                                        @error('color')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </td>
                            </tr>
                        @endif

                        <tr>
                            <th class="align-middle display-sm-none " style="width: 30%; font-weight: bold;">
                                <span class="h6">{{ __('Quantity') }}</span>
                            </th>
                            <td class="align-middle" style="width: 70%;">
                                <div class="quantity">
                                    <span wire:click="decrease" class="input-number-decrement">–</span>
                                    <input class="input-number m-l-10 @error('quantity') is-invalid @enderror" wire:model="quantity" type="number" min="1" value="{{ $quantity }}" max="{{ $product->stock }}" step="1" readonly />
                                    <span wire:click="increase" class="input-number-increment">+</span>
                                </div>

                                @error('quantity')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="bottom-meta">
                @if($product->stock >= 1)
                <a wire:click="buynow" class="main-btn2 btn-shopinn-outline float-left" data-toggle="tooltip" data-original-title="{{ __('Buy Now') }}" style="cursor: pointer;">
                    <i class="fa fa-shopping-bag" style="font-size: 20px;"></i>
                </a>

                <button class="main-btn btn-shopinn float-left m-l-5" data-toggle="tooltip" data-original-title="{{ __('Add to Cart') }}">
                    <i class="fa fa-cart-plus" style="font-size: 20px;"></i>
                </button>
                @endif


                <div class="share">
                    {{--  <!-- Go to www.addthis.com/dashboard to customize your tools -->  --}}
                    <div class="addthis_inline_share_toolbox_0h61" style="display: inline-flex;">
                        <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="{{ __('Copy Share Link') }}" id="copyURL" class="js-copy m-r-6">
                            <input type="text" id="copy_url" name="url" class="js-copy d-none" value="{{ 'page_link_here' }}">
                            <i class="fa fa-clone border p-10"></i>
                        </a>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>

