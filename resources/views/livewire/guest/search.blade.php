<div>

<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="central-meta">
                        <div class="title-block">
                            <div class="row c-form">
                                <div class="col-md-3 m-b-10">
                                    <input autocomplete="off" type="text" name="title" class="border bg-transparent" wire:model="title" value="{{ $title }}" placeholder="{{ __('Search Keyword') }}">
                                </div>

                                <div class="col-md-4 m-b-10">
                                    <div class="row">
                                        <div class="col p-r-0">
                                            <input autocomplete="off" type="number" name="min" class="border bg-transparent"  wire:model="min" value="{{ $min }}" min="0" placeholder="{{ __('Min Price') }}">
                                        </div>
                                        <div class="col p-l-0">
                                            <input autocomplete="off" type="number" name="max" class="border bg-transparent" wire:model="max" value="{{ $max }}" min="0" placeholder="{{ __('Max Price') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 m-b-10">
                                    <select wire:model="cat_id" name="cat_id" id="category" class="form-control">
                                        <option value="" aria-readonly="true">{{ __('Select Category') }}</option>
                                        @foreach($types as $type)
                                            <optgroup label="{{ $type->name }}">
                                                @foreach($type->categories as $cat)
                                                <option value="{{ $cat->id }}">{{ __($cat->name) }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2 m-b-10">
                                    <a wire:click="filter()" class="btn btn-shopinn-outline btn-block border-radius-0 p-7">Filter</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="central-meta padding30">
                        <div class="row merged20">
                            @foreach ($products as $product)
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="friend-block box-shadow">
                                        <b class="rating color-orange">
                                            <i class="fa fa-star"></i> {{ get_avarage_rating($product->ratings) }}
                                        </b>

                                        @php
                                            $url = get_image($product, 'thumb');
                                            if($url == null) $url = asset('vendor/images/resources/frnd-figure.jpg');
                                        @endphp
                                        <figure>
                                            <img src="{{ $url }}" alt="84*84" style="height: 84px; width: 84px;">
                                        </figure>

                                        <div class="frnd-meta">
                                            <div class="frnd-name">
                                                <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="title-1-line">{{ __($product->name) }}</a>

                                                <span>
                                                    <a href="{{ route('guest.shop.show', ['shop_id' => $product->shop->id,'shop_name' => get_name($product->shop->name)]) }}" class="title-1-line">{{ __($product->shop->name) }}</a>
                                                </span>

                                                @if (offer_price($product)['isOffer'] == 1)
                                                    <h6 class="text-bold text-shopinn m-t-4 m-b-0">
                                                        <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['price'] }}
                                                        <sup class="text-muted">
                                                            <small>
                                                                (<del>
                                                                    {{ offer_price($product)['main'] }}<sup><i class="text-bold">৳</i></sup>
                                                                </del>)
                                                            </small>
                                                        </sup>
                                                        <sup class="text-shopinn">
                                                            (- {{ offer_price($product)['offer'] }}%)
                                                        </sup>
                                                    </h6>
                                                @else
                                                    <h6 class="text-bold text-shopinn m-t-4 m-b-0">
                                                        <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['main'] }}
                                                    </h6>
                                                @endif
                                            </div>

                                            <br>

                                            <a class="btn btn-shopinn-outline" href="#"><i class="fa fa-shopping-bag"></i></a>

                                            {{-- @if ($product->sizes->count() > 0 || $product->colors->count() > 0) --}}
                                                <a class="btn btn-shopinn" href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}"><i class="fa fa-cart-plus"></i></a>
                                            {{-- @else
                                                @livewire('guest.add-to-cart', ['product' => $product])
                                            @endif --}}
                                            {{-- <a class="send-mesg btn btn-shopinn" href="#" data-toggle="tooltip" data-original-title="Add To Cart"><i class="fa fa-cart-plus"></i></a> --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        @if (count($products) < 1)
                            <div class="row justify-content-center">
                                <div class="col d-flex justify-content-center text-center">
                                    <h4 class="text-muted text-center">{{ __('No Result Found') }}</h4>
                                </div>
                            </div>
                        @endif

                        {{-- <div class="row justify-content-center">
                            <div class="col d-flex justify-content-center">
                                {{ $products->links() }}
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
