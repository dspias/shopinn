<div>
    <div class="row merged20" id="page-contents">

        <div class="user-profile">
            @php
                $logo = get_logo($shop, 'thumb');
                if($logo == null) $logo = asset('vendor/images/resources/author.jpg');

                $cover = get_cover($shop, 'profile');
                if($cover == null) $cover = asset('vendor/images/resources/profile-image.jpg');
            @endphp
            <figure>
                <img src="{{ $cover }}" alt="{{ __('Image Not Found') }}">
            </figure>

            @auth
                <div class="profile-section">
                    <div class="row">
                        <div class="col-lg-2 col-md-3">
                            <div class="profile-author">
                                <a class="profile-author-thumb" href="javascript:void(0);">
                                    <img alt="author" src="{{ $logo }}">
                                </a>
                                <div class="author-content m-t-40">
                                    <a class="h3 author-name" href="javascript:void(0);">{{ $shop->name }}</a>
                                    {{-- <div class="country">{{ __('Shop_Er_thikana_Hoibo_ikhano') }}</div> --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-10 col-md-9">
                            <ul class="align-right user-ben m-t-15 m-b-15">
                                <li class="search-for">
                                    <a data-ripple="" class="circle-btn search-data" title="" href="#"><i class="ti-search"></i></a>
                                    <form class="searchees" wire:submit.prevent="submit" @if($key != null)style="display:block;"@endif>
                                        <span class="cancel-search"><i class="ti-close"></i></span>
                                        <input type="text" wire:model="key" name="key" placeholder="Search in Posts">
                                        <button type="submit"></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endauth
        </div><!-- user profile banner  -->

        <div class="col-lg-3">
            <form wire:submit.prevent="filter">
                <aside class="sidebar static left">
                    {{-- Filter Posts --}}
                    <div class="widget">
                        <h4 class="widget-title">{{ __('Filter Posts') }}</h4>
                        <ul class="widget-body">
                            <p class="text-dark m-b-0">{{ __('Filter Your Posts') }}</p>
                        </ul>
                    </div>
                    {{-- Filter Posts Ends --}}


                    {{-- My Categories --}}
                    <div class="widget">
                        <h4 class="widget-title">{{ __('My Categories') }}</h4>
                        <ul class="widget-body">
                            @foreach($categories as $category)
                            <li>
                                <div class="checkbox m-0">
                                    <label>
                                    <input type="checkbox" wire:model="cat_ids" value="{{ $category->id }}"><i class="check-box"></i>{{ $category->name }}
                                    </label>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    {{-- My Categories Ends --}}


                    {{-- Price Range --}}
                    <div class="widget">
                        <h4 class="widget-title m-b-10">{{ __('Price Range') }}</h4>
                        <div class="widget-body c-form p-10 p-t-0">
                            <div class="row p-b-20">
                                <div class="col-md-6">
                                    <input type="number" wire:model="min" name="min" placeholder="Min" min="0" class="bg-transparent">
                                </div>
                                <div class="col-md-6">
                                    <input type="number" wire:model="max" name="max" placeholder="Max" min="0" class="bg-transparent">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Price Range Ends --}}


                    <div class="widget stick-widget bg-transparent">
                        <div class="widget-body bg-transparent">
                            <button class="btn btn-danger btn-shopinn btn-block text-center">{{ __('Filter') }}</button>
                        </div>
                    </div>
                </aside>
            </form>
        </div><!-- sidebar -->

        <div class="col-lg-9">
            <div class="load-mored">
                <div class="central-meta">
                    <div class="row merged20 remove-ext">
                        @foreach($products as $product)
                            @php
                                $url = null;
                                $avatar = $product->getFirstMedia('product');
                                if($avatar != null) $url = $avatar->getUrl('card');
                                $str = str_replace(" ","-",$product->name);
                            @endphp
                            <div class="col-md-3 col-sm-6 col-sx-6 item link-div" onclick="location.href='{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($str)]) }}';">
                                <div class="pitdate-user">
                                    {{-- <figure><img src="{{ asset('vendor/images/resources/date-user1.jpg') }}" alt="">  --}}
                                    <figure>
                                        @if ($url != null)
                                            <img src="{{ $url }}" alt="Image Not Found">
                                        @else
                                            <img src="{{ asset('vendor/images/resources/date-user1.jpg') }}" alt="">
                                        @endif

                                        <div class="likes heart" title="Rating">
                                            <i class="fa fa-star color-orange"></i>
                                            <span class="m-t-1">
                                                <b>{{ get_avarage_rating($product->ratings) }}</b>
                                            </span>
                                        </div>
                                        {{-- <div class="more">
                                            <div class="more-post-optns">
                                                <i class="ti-more-alt"></i>
                                                <ul>
                                                    <li class="send-mesg">
                                                        <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => $str]) }}">
                                                            <i class="fa fa-info"></i>
                                                            Details
                                                        </a>
                                                    </li>
                                                    <li class="get-link">
                                                        <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="{{ __('Copy Share Link') }}" id="copyURL" class="js-copy">
                                                            <i class="fa fa-link"></i>Copy Link
                                                        </a>
                                                    </li>
                                                    <li class="bad-report"><i class="fa fa-trash"></i>Delete</li>
                                                </ul>
                                            </div>
                                        </div> --}}
                                    </figure>

                                    <h4>
                                        <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($str)]) }}" class="title-1-line">{{ $product->name }}</a>
                                    </h4>

                                    <span class="text-shopinn text-bold">
                                        <i class="text-shopinn text-bold">৳</i>
                                        {{ __($product->sell_price) }}
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
