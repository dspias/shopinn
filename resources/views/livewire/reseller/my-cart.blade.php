<div>
    <a title="Your Cart Items" href="{{ route('reseller.cart.index') }}" class="shopping-cart" data-toggle="tooltip">Cart <i class="fa fa-shopping-bag"></i>
        <span>{{ $cartTotal }}</span>
    </a>
    
</div>
