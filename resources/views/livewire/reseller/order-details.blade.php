<div>
    <div class="central-meta">
        <h4 class="create-post">
            {{ __('My Shopping Cart') }} 
            <a class="see-all text-bold" href="#">
                <b>
                    <span class="text-dark">{{ __('Payment By: ') }}</span>
                    <span>{{ __('Cash On Delivery') }}</span>
                </b>
            </a>
        </h4>
        <div class="cart-sec">
            <table class="table table-responsive">
                <tr>
                    <th>{{ __('Product Name') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Quantity') }}</th>
                    <th class="text-shopinn">{{ __('Total') }}</th>
                </tr>

                @if(count($cart) > 0)
                    @php
                        $shops = array();
                        // dd($cart);
                        foreach((array)$cart as $item){
                            $shops[$item['shop_id']][] = $item;                            
                        } 
                    @endphp
                    @foreach($shops as $shop)
                        <tr>
                            @php
                                $temp = \App\User::find($shop[0]['shop_id']);
                            @endphp
                            <td colspan="4">
                                <a target="_blank" href="{{ route('guest.shop.show', ['shop_id' => $temp->id, 'shop_name' => get_name($temp->name)]) }}" class="btn btn-link text-shopinn text-bold">{{ $temp->name }}</a>
                            </td>                            
                        </tr>
                        
                        @foreach ($shop as $item)
                            @php
                                // dd($item);
                                $product = \App\Models\Product::find($item['product_id']);
                            @endphp
                            <tr>
                                <td>
                                    <div class="cart-avatar">
                                        <img src="{{ get_image($product, 'thumb') }}" alt="" />
                                    </div>
                                    <div class="cart-meta">
                                        <span>{{ $product->name }}</span><br>
                                        @if($item['details'] != null)
                                        <span style="font-size: 8px;">{{ $item['details']['size'] }} | {{ $item['details']['color'] }}</span>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <span class="cart-prices">
                                        @if(offer_price($product)['isOffer'] == 1)
                                            <del>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['main'] }}
                                                </span>
                                            </del>
                                            <ins>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['price'] }}
                                                </span>
                                            </ins>
                                        @else
                                            <ins>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['main'] }}
                                                </span>
                                            </ins>
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <span class="cart-prices">
                                        <ins>
                                            <span class="woocommerce-Price-amount amount">
                                                {{ $item['quantity'] }}
                                            </span>
                                        </ins>
                                    </span>
                                </td>
                                <td>
                                    <span class="total-price text-shopinn">
                                        @php
                                            $data = offer_price($product);
                                            if($data['isOffer'] == 1){
                                                $sub = ($item['quantity'] * $data['price']);
                                                $totalCost += $sub;
                                            } else{
                                                $sub = ($item['quantity'] * $data['main']);
                                                $totalCost += $sub;
                                            }
                                        @endphp
                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                        {{ $sub }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach

                    <tr>
                        <td colspan="1" style="border: 0px solid #fff;"></td>
                        <td colspan="2" class="text-left">
                            <span><b class="text-dark">{{ __('Total Cost:') }}</b></span>
                        </td>
                        <td>
                            <span>
                                <b class="text-dark">
                                    <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($totalCost) }}</i>
                                </b>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="1" style="border: 0px solid #fff;"></td>
                        <td colspan="2" class="text-left">
                            <span><b class="text-dark">{{ __('Discount:') }}</b></span>
                        </td>
                        <td>
                            <span>
                                <b class="text-dark">
                                    <i>(-)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($couponDiscount) }}</i>
                                </b>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="1" style="border: 0px solid #fff;"></td>
                        <td colspan="2" class="text-left">
                            <span><b class="text-dark">{{ __('Delivery Charge:') }}</b></span>
                        </td>
                        <td>
                            <span>
                                <b class="text-dark">
                                    <i>(+)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($deliveryCharge) }}</i>
                                </b>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        @php
                            $grandTotal = ($totalCost-$couponDiscount)+$deliveryCharge;
                        @endphp
                        <td colspan="1" style="border: 0px solid #fff;"></td>
                        <td colspan="2" class="text-left">
                            <span><b class="text-shopinn">{{ __('GRAND TOTAL:') }}</b></span>
                        </td>
                        <td>
                            <span>
                                <b class="text-dark">
                                    <i class="text-shopinn" style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i class="text-shopinn">{{ __($grandTotal) }}</i>
                                </b>
                            </span>
                        </td>
                    </tr>
                @endif
            </table>
        </div>        

        <form action="{{ route('reseller.cart.confirmation') }}" method="post">
            @csrf
            <div class="row" style="margin-top: 20px;">
                <div class="col-12">
                    <div class="payment-method m-b-20">
                        <h4 class="create-post">
                            <span>{{ __('Delivery Address & Informations') }}</span>
                        </h4>
                        
                        <div class="row c-form d-flex" style="width: auto;">
                            <div class="col-md-6 m-b-10">
                                <input autocomplete="off" type="text" name="name" class="border bg-transparent @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Customer Name *') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 m-b-10">
                                <input autocomplete="off" type="text" name="mobile" class="border bg-transparent @error('mobile') is-invalid @enderror" value="{{ old('mobile') }}" placeholder="{{ __('Customer Mobile *') }}" required>

                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 m-b-10">
                                <input autocomplete="off" type="email" name="email" class="border bg-transparent @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="{{ __('Customer Email') }}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="col-md-6 m-b-10">
                                <select wire:model="customer_city" name="customer_city" class="form-control  @error('customer_city') is-invalid @enderror display-block" style="height: 50px; background: transparent; padding-left: 10px; display:block !important;" required>
                                    <option value="">{{ 'Select City (District) *' }}</option>
                                    <option value="Bagerhat">Bagerhat</option>
                                    <option value="Bandarban">Bandarban</option>
                                    <option value="Barguna">Barguna</option>
                                    <option value="Barisal">Barisal</option>
                                    <option value="Bhola">Bhola</option>
                                    <option value="Bogra">Bogra</option>
                                    <option value="Brahmanbaria">Brahmanbaria</option>
                                    <option value="Chandpur">Chandpur</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Chuadanga">Chuadanga</option>
                                    <option value="Comilla">Comilla</option>
                                    <option value="Cox's Bazar">{{ "Cox's Bazar" }}</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Dinajpur">Dinajpur</option>
                                    <option value="Faridpur">Faridpur</option>
                                    <option value="Feni">Feni</option>
                                    <option value="Gaibandha">Gaibandha</option>
                                    <option value="Gazipur">Gazipur</option>
                                    <option value="Gopalganj">Gopalganj</option>
                                    <option value="Habiganj">Habiganj</option>
                                    <option value="Jaipurhat">Jaipurhat</option>
                                    <option value="Jamalpur">Jamalpur</option>
                                    <option value="Jessore">Jessore</option>
                                    <option value="Jhalakati">Jhalakati</option>
                                    <option value="Jhenaidah">Jhenaidah</option>
                                    <option value="Khagrachari">Khagrachari</option>
                                    <option value="Khulna">Khulna</option>
                                    <option value="Kishoreganj">Kishoreganj</option>
                                    <option value="Kurigram">Kurigram</option>
                                    <option value="Kushtia">Kushtia</option>
                                    <option value="Lakshmipur">Lakshmipur</option>
                                    <option value="Lalmonirhat">Lalmonirhat</option>
                                    <option value="Madaripur">Madaripur</option>
                                    <option value="Magura">Magura</option>
                                    <option value="Manikganj">Manikganj</option>
                                    <option value="Meherpur">Meherpur</option>
                                    <option value="Moulvibazar">Moulvibazar</option>
                                    <option value="Munshiganj">Munshiganj</option>
                                    <option value="Mymensingh">Mymensingh</option>
                                    <option value="Naogaon">Naogaon</option>
                                    <option value="Narail">Narail</option>
                                    <option value="Narayanganj">Narayanganj</option>
                                    <option value="Narsingdi">Narsingdi</option>
                                    <option value="Natore">Natore</option>
                                    <option value="Nawabganj">Nawabganj</option>
                                    <option value="Netrakona">Netrakona</option>
                                    <option value="Nilphamari">Nilphamari</option>
                                    <option value="Noakhali">Noakhali</option>
                                    <option value="Pabna">Pabna</option>
                                    <option value="Panchagarh">Panchagarh</option>
                                    <option value="Parbattya Chattagram">Parbattya Chattagram</option>
                                    <option value="Patuakhali">Patuakhali</option>
                                    <option value="Pirojpur">Pirojpur</option>
                                    <option value="Rajbari">Rajbari</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Rangpur">Rangpur</option>
                                    <option value="Satkhira">Satkhira</option>
                                    <option value="Shariatpur">Shariatpur</option>
                                    <option value="Sherpur">Sherpur</option>
                                    <option value="Sirajganj">Sirajganj</option>
                                    <option value="Sunamganj">Sunamganj</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Tangail">Tangail</option>
                                    <option value="Thakurgaon">Thakurgaon</option>
                                </select>

                                @error('customer_city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-12 m-b-10">
                                <textarea autocomplete="off" name="address" class="border bg-transparent @error('address') is-invalid @enderror" placeholder="{{ __('Customer Address *') }}" rows="4" required>{{ old('address') }}</textarea>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="proceed">
                        <a href="{{ route('reseller.cart.index') }}" title="" class="btn btn-shopinn-outline">{{ __('Back To Cart') }}</a>
                        <button type="submit" class="btn btn-shopinn m-l-507">{{ __('Confirm Order') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
