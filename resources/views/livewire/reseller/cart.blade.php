<div>
    <div class="central-meta">
        <h4 class="create-post">{{ __('My Shopping Cart') }} <a class="see-all text-bold" href="/">{{ __('Add More Product') }}</a></h4>
        <div class="cart-sec">
            <table class="table table-responsive">
                <tr>
                    <th>{{ __('Product Name') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Quantity') }}</th>
                    <th class="text-shopinn">{{ __('Total') }}</th>
                </tr>

                @if(count($cart) > 0)
                    @php
                        $shops = array();
                        //dd($cart);
                        foreach((array)$cart as $aa => $item){
                            $shops[$item['shop_id']][] = array(
                                'key'   => $aa,
                                'item'  => $item
                            );                            
                        } 
                    @endphp
                    @foreach($shops as $shop)
                        <tr>
                            @php
                                $temp = \App\User::find($shop[0]['item']['shop_id']);
                            @endphp
                            <td colspan="4">
                                <a target="_blank" href="{{ route('guest.shop.show', ['shop_id' => $temp->id, 'shop_name' => get_name($temp->name)]) }}" class="btn btn-link text-shopinn text-bold title-1-line">{{ $temp->name }}</a>
                            </td>                            
                        </tr>
                        
                        @foreach ($shop as $tt)
                            @php
                                $key = $tt['key'];
                                $item = $tt['item'];
                                $product = \App\Models\Product::find($item['product_id']);
                            @endphp
                            <tr>
                                <td>
                                    <a wire:click="removeFromCart({{ $key }})"  class="delete-cart"><i class="ti-close"></i></a>
                                    <div class="cart-avatar">
                                        <img src="{{ get_image($product, 'thumb') }}" alt="" />
                                    </div>
                                    <div class="cart-meta">
                                        <span>{{ $product->name }}</span><br>
                                        @if($item['details'] != null)
                                        <span style="font-size: 8px;">{{ $item['details']['size'] }} | {{ $item['details']['color'] }}</span>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <span class="cart-prices">
                                        @if(offer_price($product)['isOffer'] == 1)
                                            <del>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['main'] }}
                                                </span>
                                            </del>
                                            <ins>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['price'] }}
                                                </span>
                                            </ins>
                                        @else
                                            <ins>
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    </span>{{ offer_price($product)['main'] }}
                                                </span>
                                            </ins>
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <div class="quantity">
                                        <span wire:click="updateCart({{ $product->id }}, {{-1}}, {{ $key }})" class="input-number-decrement">–</span>
                                        <input class="input-number m-l-10" type="number" value="{{ $item['quantity'] }}" min="1" max="{{ $product->stock }}" step="1" readonly />
                                        <span wire:click="updateCart({{ $product->id }}, {{+1}}, {{ $key }})" class="input-number-increment">+</span>
                                    </div>
                                </td>
                                <td>
                                    <span class="total-price text-shopinn">
                                        @php
                                            $data = offer_price($product);
                                            if($data['isOffer'] == 1){
                                                $sub = ($item['quantity'] * $data['price']);
                                                $totalCost += $sub;
                                            } else{
                                                $sub = ($item['quantity'] * $data['main']);
                                                $totalCost += $sub;
                                            }
                                        @endphp
                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                        {{ $sub }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                @endif
            </table>
        </div>
        {{-- <form wire:submit.prevent="submit" class="coupon-code">
            <input type="text" wire:model="code" name="code" class="@error('code') is-invalid @enderror" placeholder="{{ __('Enter your Coupon') }}" />
            <button><i class="fa fa-paper-plane-o"></i></button>
            
            @error('code')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            @if($couponAlert != NULL)
                <span class="text-danger">
                    <small>{{ __($couponAlert) }}</small>
                </span>
            @endif
        </form> --}}
        

        <div class="row m-t-20">
            <div class="col-lg-6">
                <div class="amount-area">
                    <div class="total-area">
                        <ul>
                            @php
                                $grandTotal = ($totalCost-$couponDiscount)+$deliveryCharge;
                            @endphp
                            <li>
                                <span>{{ __('Total Cost:') }}</span>
                                <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($totalCost) }}</i>
                            </li>
                            <li>
                                <span>{{ __('Discount:') }}</span>
                                <i>(-)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($couponDiscount) }}</i>
                            </li>
                            <li>
                                <span>{{ __('Delivery Charge:') }}</span>
                                <i>(+)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($deliveryCharge) }}</i>
                            </li>
                            <li class="order-total">
                                <span class="text-shopinn">{{ __('GRAND TOTAL:') }}</span>
                                <i class="text-shopinn" style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __($grandTotal) }}</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="payment-method m-b-20">
                    <h4 class="create-post">{{ __('Select Payment Method') }}</h4>
                    <ul class="payment-options">
                        <li class="options">
                            <input type="radio" id="pay_by_bkash" name="pay_by" required disabled/>
                            <label class="text-capitalize m-b-0" for="pay_by_bkash">{{ __('bKash') }}</label>
                        </li>
                        <li class="options">
                            <input type="radio" id="pay_by_rocket" name="pay_by" required disabled/>
                            <label class="text-capitalize m-b-0" for="pay_by_rocket">{{ __('Rocket') }}</label>
                        </li>
                        <li class="options">
                            <input type="radio" id="pay_by_card" name="pay_by" required disabled/>
                            <label class="text-capitalize m-b-0" for="pay_by_card">{{ __('Card') }}</label>
                        </li>
                        <li class="options">
                            <input type="radio" id="cash_on_delivery" name="pay_by" required checked/>
                            <label class="text-capitalize m-b-0" for="cash_on_delivery">{{ __('Cash On Delivery') }}</label>
                        </li>
                    </ul>
                </div>
                <div class="proceed">
                    <a href="/" title="" class="btn btn-shopinn-outline">{{ __('Back To ShopInn') }}</a>
                    <button wire:click="checkout()" @if(count($cart) < 1) disabled @endif class="btn btn-shopinn">{{ __('Proceed to Checkout') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
