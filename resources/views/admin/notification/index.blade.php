@extends('layouts.admin.app')

@section('page_title', 'Notifications | All Notifications')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .row-click{
        cursor: pointer;
    }

    .readed{
        background-color: #f6f7fb !important;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('All Notifications') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('All Notifications') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block p-20 p-t-20">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('#') }}</th>
                                    <th class="text-center">{{ __('Notification') }}</th>
                                    <th class="text-center">{{ __('Arrived At') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $sl => $data)
                                    <tr class="row-click @if($data->read_at == null) readed @endif" data-link="{{ route('markasread', ['id' => $data->id]) }}" data-target="_self" role="row">
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ __($sl+1) }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{!! $data->data['message'] !!}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ ago_time($data->created_at) }}</b>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    
    </script>
@endsection
