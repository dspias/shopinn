@extends('layouts.admin.app')

@section('page_title', 'Riders | Create New Rider')

@section('css_links')
{{--  External CSS  --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
{{--  External CSS  --}}
<style>
/* Custom CSS Here */
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }

    .responsive-image{
        border-radius: 5px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        height: 80px;
        width: 100%;
    }
    .card-block.task-file.p-0 {
        height: 80px;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }
    a.deactive{
        cursor: pointer;
    }
    li.social-media-list{
        border: 0px !important;
        border-bottom: 1px solid #e6ecf5 !important;
        margin-bottom: 10px;
    }
    li.social-media-list:last-child{
        border: 0px !important;
        border-bottom: 0px solid #e6ecf5 !important;
        margin-bottom: 0px;
    }

    .password-generate {
        position: absolute;
        right: 15px;
        font-weight: bold;
        text-transform: uppercase;
    }


    .note-fontname button{
        min-width: 200px;
    }

    .note-para .note-btn-group{
        display: none;
    }

    .note-insert button:nth-child(2),
    .note-insert button:nth-child(3){
        display: none;
    }
</style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Create New Rider
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Riders</li>
<li class="breadcrumb-item">Create New Rider</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.rider.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-block p-25">
                                <div class="row justify-content-center">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Login Email <span class="required">*</span></label>
                                            <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror send-email" value="{{ old('email') }}" placeholder="shop@mail.com" required>
                                            {{-- <small class="text-info">{{ __('Please enter a correct email. The shop can login with this email.') }}</small> --}}
        
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Login Mobile Number <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{ old('mobile') }}" placeholder="+880 1712 345678" required>
                                            {{-- <small class="text-info">{{ __('Please enter a correct mobile number. The shop can login with this email.') }}</small> --}}
        
                                            @error('mobile')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Password. <span class="required">*</span> <a href="#" class="password-generate text-shopinn" id="passwordGenerate">Generate</a></label>
                                            <input autocomplete="off" type="text" name="password" id="password_generator" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="123ABCabc!@#$%" required>
                                            {{-- <small class="text-info">{{ __('Please enter at least 8 character or Click on generate.') }}</small> --}}
        
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <ul class="list-unstyled list-inline pull-left">
                                            <li class="list-inline-item">
                                                <div class="checkbox checkbox-primary font-size-12">
                                                    <input id="is_email_info" name="is_email_info" type="checkbox" checked>
                                                    <label for="is_email_info" class="m-b-0 text-bold text-dark">{{ __('Send Login Information on') }} <span class="text-shopinn send-email-show">-</span></label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="card m-b-0">
                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <div>
                                            <label for="img-upload" class="pointer">
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">
        
                                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Rider Avatar') }}</span>
                                                <input type="file" name="avatar" class="d-none @error('avatar') is-invalid @enderror" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                                            </label>
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Rider Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Rider Name') }}" required>
        
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Rider Mobile Number') }}</label>
                                            <input autocomplete="off" type="text" name="rider_mobile" class="form-control @error('rider_mobile') is-invalid @enderror" value="{{ old('rider_mobile') }}" placeholder="{{ __('+880 1712 345678') }}">
        
                                            @error('rider_mobile')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>{{ __('Rider Email') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="email" name="rider_email" class="form-control @error('rider_email') is-invalid @enderror" value="{{ old('rider_email') }}" placeholder="rideremail@mail.com" required>
        
                                            @error('rider_email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ __('Select City') }} <span class="required">*</span> <sup>({{ __('District') }})</sup></label>
                                            <select name="city" class="@error('city') is-invalid @enderror" id="selectize-dropdown" required>
                                                <option value="" disabled selected>{{ __('Select City') }} ({{ __('District') }})</option>
                                                <option value="Bagerhat">{{ __('Bagerhat') }}</option>
                                                <option value="Bandarban">{{ __('Bandarban') }}</option>
                                                <option value="Barguna">{{ __('Barguna') }}</option>
                                                <option value="Barisal">{{ __('Barisal') }}</option>
                                                <option value="Bhola">{{ __('Bhola') }}</option>
                                                <option value="Bogra">{{ __('Bogra') }}</option>
                                                <option value="Brahmanbaria">{{ __('Brahmanbaria') }}</option>
                                                <option value="Chandpur">{{ __('Chandpur') }}</option>
                                                <option value="Chittagong">{{ __('Chittagong') }}</option>
                                                <option value="Chuadanga">{{ __('Chuadanga') }}</option>
                                                <option value="Comilla">{{ __('Comilla') }}</option>
                                                <option value="Cox's Bazar">{{ __('Cox\'s Bazar') }}</option>
                                                <option value="Dhaka">{{ __('Dhaka') }}</option>
                                                <option value="Dinajpur">{{ __('Dinajpur') }}</option>
                                                <option value="Faridpur">{{ __('Faridpur') }}</option>
                                                <option value="Feni">{{ __('Feni') }}</option>
                                                <option value="Gaibandha">{{ __('Gaibandha') }}</option>
                                                <option value="Gazipur">{{ __('Gazipur') }}</option>
                                                <option value="Gopalganj">{{ __('Gopalganj') }}</option>
                                                <option value="Habiganj">{{ __('Habiganj') }}</option>
                                                <option value="Jaipurhat">{{ __('Jaipurhat') }}</option>
                                                <option value="Jamalpur">{{ __('Jamalpur') }}</option>
                                                <option value="Jessore">{{ __('Jessore') }}</option>
                                                <option value="Jhalakati">{{ __('Jhalakati') }}</option>
                                                <option value="Jhenaidah">{{ __('Jhenaidah') }}</option>
                                                <option value="Khagrachari">{{ __('Khagrachari') }}</option>
                                                <option value="Khulna">{{ __('Khulna') }}</option>
                                                <option value="Kishoreganj">{{ __('Kishoreganj') }}</option>
                                                <option value="Kurigram">{{ __('Kurigram') }}</option>
                                                <option value="Kushtia">{{ __('Kushtia') }}</option>
                                                <option value="Lakshmipur">{{ __('Lakshmipur') }}</option>
                                                <option value="Lalmonirhat">{{ __('Lalmonirhat') }}</option>
                                                <option value="Madaripur">{{ __('Madaripur') }}</option>
                                                <option value="Magura">{{ __('Magura') }}</option>
                                                <option value="Manikganj">{{ __('Manikganj') }}</option>
                                                <option value="Meherpur">{{ __('Meherpur') }}</option>
                                                <option value="Moulvibazar">{{ __('Moulvibazar') }}</option>
                                                <option value="Munshiganj">{{ __('Munshiganj') }}</option>
                                                <option value="Mymensingh">{{ __('Mymensingh') }}</option>
                                                <option value="Naogaon">{{ __('Naogaon') }}</option>
                                                <option value="Narail">{{ __('Narail') }}</option>
                                                <option value="Narayanganj">{{ __('Narayanganj') }}</option>
                                                <option value="Narsingdi">{{ __('Narsingdi') }}</option>
                                                <option value="Natore">{{ __('Natore') }}</option>
                                                <option value="Nawabganj">{{ __('Nawabganj') }}</option>
                                                <option value="Netrakona">{{ __('Netrakona') }}</option>
                                                <option value="Nilphamari">{{ __('Nilphamari') }}</option>
                                                <option value="Noakhali">{{ __('Noakhali') }}</option>
                                                <option value="Pabna">{{ __('Pabna') }}</option>
                                                <option value="Panchagarh">{{ __('Panchagarh') }}</option>
                                                <option value="Parbattya Chattagram">{{ __('Parbattya Chattagram') }}</option>
                                                <option value="Patuakhali">{{ __('Patuakhali') }}</option>
                                                <option value="Pirojpur">{{ __('Pirojpur') }}</option>
                                                <option value="Rajbari">{{ __('Rajbari') }}</option>
                                                <option value="Rajshahi">{{ __('Rajshahi') }}</option>
                                                <option value="Rangpur">{{ __('Rangpur') }}</option>
                                                <option value="Satkhira">{{ __('Satkhira') }}</option>
                                                <option value="Shariatpur">{{ __('Shariatpur') }}</option>
                                                <option value="Sherpur">{{ __('Sherpur') }}</option>
                                                <option value="Sirajganj">{{ __('Sirajganj') }}</option>
                                                <option value="Sunamganj">{{ __('Sunamganj') }}</option>
                                                <option value="Sylhet">{{ __('Sylhet') }}</option>
                                                <option value="Tangail">{{ __('Tangail') }}</option>
                                                <option value="Thakurgaon">{{ __('Thakurgaon') }}</option>
                                            </select>
        
                                            @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ __('Rider Address') }} <span class="required">*</span></label>
                                            {{-- <div id="summernote-usage"></div> --}}
                                            <textarea autocomplete="off" name="address" class="form-control summernote @error('address') is-invalid @enderror" placeholder="{{ __('Rider Address') }}" rows="5" required>{{ old('address') }}</textarea>
        
                                            @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ __('Note') }}</label>
                                            {{-- <div id="summernote-usage"></div> --}}
                                            <textarea autocomplete="off" name="note" class="form-control summernote @error('note') is-invalid @enderror" placeholder="{{ __('Note') }}" rows="5" required>{{ old('note') }}</textarea>
        
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active" class="m-b-0 text-bold text-shopinn">Activate This Rider?</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Assign As New Rider
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    
    <script>
        $( document ).ready(function() {
            $( '#passwordGenerate' ).on( "click", function(){
                var text = "";
                var possible = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";

                for (var i = 0; i < 8; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                var sendpass = document.getElementById("password_generator");
                sendpass.value = text;
            });


            $( '#copyPass' ).on( "click", function(){
                var copyPass = document.getElementById("password_generator");
                copyPass.select();
                document.execCommand("copy");
            });
        });
    </script>

    <script>
        var summernote = $('.summernote');
        // var placeholder = summernote.attr('placeholder');
        summernote.summernote({
            height: 200,
            // placeholder: placeholder
        });
    </script>
@endsection
