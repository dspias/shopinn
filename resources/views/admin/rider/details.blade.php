@extends('layouts.admin.app')

@section('page_title', 'Rider | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Details rider
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Riders</li>
<li class="breadcrumb-item"><a href="{{ route($route) }}"> {{ $page }} </a></li>
<li class="breadcrumb-item">{{ $rider->name }}</li>
<li class="breadcrumb-item">Details</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu">
                                {{--  <li>
                                    <a href="#">
                                        <i class="ti-pencil text-info pdd-right-10"></i>
                                        <span>Edit rider</span>
                                    </a>
                                </li>  --}}
                                @if($rider->approved_at == null)
                                <li>
                                    <a href="{{ route('admin.rider.approve', ['rider_id' => encrypt($rider->id)]) }}" class="confirmation">
                                        <i class="ti-star text-success pdd-right-10"></i>
                                        <span>Approve</span>
                                    </a>
                                </li>
                                @endif
                                @if($rider->is_active == 1)
                                <li>
                                    <a href="{{ route('admin.rider.change_status', ['rider_id' => encrypt($rider->id)]) }}" class="confirmation">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactivate</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('admin.rider.change_status', ['rider_id' => encrypt($rider->id)]) }}" class="confirmation">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Activate</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="profile border bottom">
                    <img class="mrg-top-30" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                    <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $rider->name }}
                        <sup>
                            @if($rider->approved_at == null)
                            <i class="ti-star text-danger pdd-right-5"></i>(Not Approved)
                            @elseif($rider->is_active == 1)
                            <i class="ti-check text-success"></i>
                            @else
                            <i class="ti-close text-danger"></i>
                            @endif
                        </sup>
                    </h4>
                    <div>{!! optional($rider->rider)->rider_address !!}</div>
                </div>
                <div class="pdd-horizon-30 pdd-vertical-20">
                    @if(optional($rider->rider)->note != null)
                        <h5 class="text-bold text-dark">{{ __('About Rider') }}</h5>
                        <div>{!! optional($rider->rider)->note !!}</div>
                    @endif

                    <h5 class="text-bold text-dark">{{ __('Rider Details') }}</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Rider Name') }}</th>
                                        <td>{{ $rider->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Rider Mobile Number') }}</th>
                                        <td>{{ optional($rider->rider)->rider_mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Rider Email') }}</th>
                                        <td>{{ optional($rider->rider)->rider_email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Rider Address') }}</th>
                                        <td>{!! optional($rider->rider)->rider_address !!}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $date = new DateTime($rider->created_at);
                                        @endphp
                                        <th>{{ __('Rider Register Date') }}</th>
                                        <td>{{ $date->format('d M Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Owner Name') }}</th>
                                        <td>{{ optional($rider->rider)->owner_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Mobile Number') }}</th>
                                        <td>{{ optional($rider->rider)->owner_mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Email') }}</th>
                                        <td>{{ optional($rider->rider)->owner_email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Address') }}</th>
                                        <td>{!! optional($rider->rider)->owner_address !!}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            if($rider->approved_at != null)
                                                $approved = new DateTime($rider->approved_at);
                                            else
                                                $approved = null;
                                        @endphp
                                        <th>{{ __('rider Activation Date') }}</th>
                                        <td class="text-info">{{ ($approved != null) ? $approved->format('d M Y'):'Pending' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="mrg-top-30 text-center">
                        <ul class="list-unstyled list-inline">
                            @if(optional($rider->rider)->facebook != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $rider->rider->facebook }}" class="btn btn-facebook btn-icon btn-rounded">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            @elseif(optional($rider->rider)->instagram != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $rider->rider->instagram }}" class="btn btn-instagram btn-icon btn-rounded">
                                    <i class="ti-instagram"></i>
                                </a>
                            </li>
                            @elseif(optional($rider->rider)->twitter != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $rider->rider->twitter }}" class="btn btn-twitter btn-icon btn-rounded">
                                    <i class="ti-twitter"></i>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('All Orders Of') }} <span class="text-riderinn">{{ $rider->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Order ID') }}</th>
                                    <th class="text-center">{{ __('Quantity') }}</th>
                                    <th class="text-center">
                                        {{ __('Total Amount') }}<sup class="text-shopinn">{{ __('(In TK)') }}</sup>
                                    </th>
                                    <th class="text-center">{{ __('Order Date') }}</th>
                                    <th class="text-center">{{ __('Completion Date') }}</th>
                                    <th class="text-center">{{ __('Order Status') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $orders = $rider->orderRiders()->get();
                                @endphp
                                @foreach ($orders as $sl => $order)
                                <tr>
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            <span class="text-dark">
                                                <b class="text-shopinn">{{ $order->oid }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    @php
                                        $cost = 0;
                                        $quantity = 0;
                                        foreach($order->items as $item){
                                            $quantity += $item->quantity;
                                            $cost += ($item->quantity*$item->price);
                                        }
                                        if(optional($order->coupon)->discount != null) $cost -= optional($order->coupon)->discount;
                                    @endphp
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            <span>
                                                <b>{{ $quantity }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            <span>
                                                <b>{{ ($cost+$order->delivery) }}<sup class="text-shopinn">{{ __('TK') }}</sup></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            <span>
                                                <b>{{ get_date($order->created_at, 'd M Y') }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            @php
                                                if($order->status == 2) $date = get_date($order->status_at, 'd M Y');
                                                else $date = '-';
                                            @endphp
                                            <span>
                                                <b>{{ $date }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="m-t-0">
                                            <span>
                                                @if($order->status == 0)
                                                    <b class="text-info">{{ 'Pending' }}</b>
                                                @elseif($order->status == 1)
                                                    <b class="text-warning">{{ 'Running' }}</b>
                                                @elseif($order->status == 2)
                                                    <b class="text-success">{{ 'Completed' }}</b>
                                                @elseif($order->status == -1)
                                                    <b class="text-danger">{{ 'Canceled' }}</b>
                                                @endif
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.order.show', ['order_type' => 'All Orders' ,'order_id' => encrypt($order->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                {{-- <li>
                                                    <a href="#">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Mark As Completed</span>
                                                    </a>
                                                </li> --}}

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="card">
                {{--  <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu" style="width: 180px;">
                                <li>
                                    <a href="#">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Active Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactive Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-reload text-info pdd-right-10"></i>
                                        <span>Pending Products</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>  --}}

                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Payment Details Of') }} <span class="text-riderinn">{{ $rider->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table class="table table-lg table-hover table-bordered table-responsive data-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">{{ __('Invoice ID') }}</th>
                                    <th class="text-center">{{ __('Payment Date') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $payments = $rider->paymentReports;
                                @endphp
                                @foreach ($payments as $sl => $payment)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $payment->invoice_id }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-bold">{{ get_date($payment->updated_at, 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.payment.show', ['payment_id' => $payment->id]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
