@extends('layouts.admin.app')

@section('page_title', 'Settings | Product | Product Types')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Product Types
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Settings</li>
<li class="breadcrumb-item">Product</li>
<li class="breadcrumb-item">Product Types</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">All Product Types</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#new_type" class="btn btn-primary btn-sm float-right text-bold m-b-0">Add New</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Type Name') }}</th>
                                    <th>{{ __('Note') }}</th>
                                    <th>{{ __('Total Categories') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($types as $sl => $type)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $type->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span class="btn btn-icon btn-flat btn-squre dropdown-toggle" data-toggle="tooltip" data-placement="top" title="{{ $type->note }}"><i class="fa fa-eye pdd-right-10 text-info"></i> {{ __('Show') }}</span>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span data-toggle="tooltip" data-placement="top" title="@foreach($type->categories as $cat){{ $cat->name.", " }}@endforeach">{{ count($type->categories) }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($type->is_active == 1)
                                                <b class="text-success"><i class="fa fa-check"></i></b>
                                            @else
                                                <b class="text-danger"><i class="fa fa-times"></i></b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="javascript:void(0);" data-keyboard="false" data-toggle="modal" data-target="#edit_type" data-todo="{{ $type }}">
                                                        <i class="fa fa-edit pdd-right-10 text-info"></i>
                                                        <span>{{ __('Edit') }}</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    @if($type->is_active == 1)
                                                    <a href="{{ route('admin.setting.product.type.change_status', ['type_id' => encrypt($type->id)]) }}" class="confirmation">
                                                        <i class="fa fa-times pdd-right-10 text-danger"></i>
                                                        <span>{{ __('Deactivate') }}</span>
                                                    </a>
                                                    @else
                                                    <a href="{{ route('admin.setting.product.type.change_status', ['type_id' => encrypt($type->id)]) }}" class="confirmation">
                                                        <i class="fa fa-times pdd-right-10 text-success"></i>
                                                        <span>{{ __('Activate') }}</span>
                                                    </a>
                                                    @endif
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- D:\laragon\www\shopinn\resources\views\admin\setting\product\type\modals\new_type.blade.php --}}
    @include('admin.setting.product.type.modals.new_type')
    @include('admin.setting.product.type.modals.edit_type')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#edit_type').on('show.bs.modal', function (e) {
            // do something...
            var button = $(e.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);
            modal.find('.modal-content #update_type_id').val(data.id);
            modal.find('.modal-content #update_type_name').val(data.name);
            modal.find('.modal-content #update_type_note').val(data.note);
            
            if(data.is_active == 1){
                modal.find('.modal-content #update_type_is_active').attr('checked', 'checked');
            }
          })
    </script>
@endsection
