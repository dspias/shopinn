@extends('layouts.admin.app')

@section('page_title', 'Settings | Product | Product Categories')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Product Categories
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Settings</li>
<li class="breadcrumb-item">Product</li>
<li class="breadcrumb-item">Product Categories</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">All Product Categories</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#new_category" class="btn btn-primary btn-sm float-right text-bold m-b-0">Add New</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Category Name') }}</th>
                                    <th>{{ __('Type') }}</th>
                                    <th>{{ __('Note') }}</th>
                                    <th>{{ __('Total Items') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $sl => $cat)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $cat->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $cat->type->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span class="btn btn-icon btn-flat btn-squre dropdown-toggle" data-toggle="tooltip" data-placement="top" title="{{ $cat->note }}"><i class="fa fa-eye pdd-right-10 text-info"></i> {{ __('Show') }}</span>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ count($cat->products) }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($cat->is_active == 1)
                                                <b class="text-success"><i class="fa fa-check"></i></b>
                                            @else
                                                <b class="text-danger"><i class="fa fa-times"></i></b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="javascript:void(0);" data-keyboard="false" data-toggle="modal" data-target="#edit_category" data-todo="{{ $cat }}">
                                                        <i class="fa fa-edit pdd-right-10 text-info"></i>
                                                        <span>{{ __('Edit') }}</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    @if($cat->is_active == 1)
                                                    <a href="{{ route('admin.setting.product.category.change_status', ['cat_id' => encrypt($cat->id)]) }}" class="confirmation">
                                                        <i class="fa fa-times pdd-right-10 text-danger"></i>
                                                        <span>{{ __('Deactivate') }}</span>
                                                    </a>
                                                    @else
                                                    <a href="{{ route('admin.setting.product.category.change_status', ['cat_id' => encrypt($cat->id)]) }}" class="confirmation">
                                                        <i class="fa fa-times pdd-right-10 text-success"></i>
                                                        <span>{{ __('Activate') }}</span>
                                                    </a>
                                                    @endif
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- D:\laragon\www\shopinn\resources\views\admin\setting\product\category\modals\new_category.blade.php --}}
    @include('admin.setting.product.category.modals.new_category')
    @include('admin.setting.product.category.modals.edit_category')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
        $('#selectize-dropdown1').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });

        //modal data
        $('#edit_category').on('show.bs.modal', function (e) {
            // do something...
            var button = $(e.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);
            modal.find('.modal-content #update_category_id').val(data.id);
            modal.find('.modal-content #update_category_name').val(data.name);
            modal.find('.modal-content #update_category_note').val(data.note);

            modal.find('.modal-content .update_category_type option[value=' + data.type_id + ']').attr('selected', 'selected');

            // var name = modal.find('.modal-body #domain_customer_id option[value=' + customer_id + ']').html();
            // modal.find('.modal-body #domain_customer_id').val(customer_id);
            // modal.find('.modal-body #select2-domain_customer_id-container').attr('title', name).html(name);

            
            if(data.is_active == 1){
                modal.find('.modal-content #update_category_is_active').attr('checked', 'checked');
            }
          })
    </script>
@endsection
