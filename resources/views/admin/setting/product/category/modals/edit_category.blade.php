<div class="modal slide-in-right modal-right fade " id="edit_category">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">Edit Product Category</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.setting.product.category.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" id="update_category_id">
                                <div class="form-group">
                                    <label>{{ __('Category Name') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" id="update_category_name" type="text" name="name" class="form-control input-lg @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Category Name') }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Select Type') }} <span class="required">*</span></label>
                                    <select name="type_id" id="selectize-dropdown1" class="update_category_type" required>
                                        <option value="" disabled>Select Type</option>
                                        @foreach($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Category Note') }}</label>
                                    <textarea name="note" id="update_category_note" rows="5" class="form-control @error('note') is-invalid @enderror" placeholder="{{ __('Category Note') }}">{{ old('note') }}</textarea>

                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input autocomplete="off" id="update_category_is_active" name="is_active" type="checkbox">
                                    <label for="update_category_is_active">Active This Category?</label>
                                </div>
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">Update <span class="text-uppercase">Product Category</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
