@extends('layouts.admin.app')

@section('page_title', 'Settings | Company | About')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }

        .password-generate {
            position: absolute;
            right: 15px;
            font-weight: bold;
            text-transform: uppercase;
        }


        .note-popover .popover-content .dropdown-menu, .panel-heading.note-toolbar .dropdown-menu {
            min-width: 150px;
        }
        .note-fontname button {
            min-width: 200px;
        }
        .note-popover .popover-content .dropdown-menu, .panel-heading.note-toolbar .dropdown-menu {
            min-width: 200px !important;
        }

        .note-para .note-btn-group {
            display: block;
        }

        .note-insert button:nth-child(2),
        .note-insert button:nth-child(3) {
            display: block;
        }

        .note-popover .popover-content .dropdown-menu,
        .panel-heading.note-toolbar .dropdown-menu {
            width: 100%;
        }

        .radio.radio-primary input[type=radio]:checked + label:before {
            color: #ff084e;
        }
        .note-placeholder {
            color: #8080804f;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Company About') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">{{ __('Company') }}</li>
<li class="breadcrumb-item">{{ __('About') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.setting.company.about.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card m-b-0">
                    <div class="card-heading">
                        <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Details Of') }} <span class="text-shopinn">{{ __('Shopinn') }}</span></h4>
                    </div>

                    <hr class="m-t-0">

                    <div class="card-block p-25">
                        {{--  <div class="row justify-content-center m-b-20">
                            <div class="col-lg-4 text-center">
                                <div>
                                    <label for="img-upload" class="pointer">
                                        @php
                                            $logo = company_get('logo');
                                            $url = ($logo == null) ? asset('assets/images/others/img-10.jpg'):url(env('IMG_STORE').$logo)
                                        @endphp
                                        <img id="img-preview" src="{{ $url }}"  width="117" alt="">

                                        <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Shopinn Logo') }}</span>
                                        <input class="d-none @error('logo') is-invalid @enderror" type="file" name="logo" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ company_get('logo') }}">
                                    </label>
                                    @error('logo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>  --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('Shopinn Email') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ company_get('email') }}" placeholder="{{ __('Shopinn Email') }}" required>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('Shopinn Contact Number') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="mobile" class="form-control @error('mobile') is-invalid @enderror" value="{{ company_get('mobile') }}" placeholder="{{ __('Shopinn Contact Number') }}" required>

                                    @error('mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('Shopinn Address') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="address" class="form-control @error('address') is-invalid @enderror" value="{{ company_get('address') }}" placeholder="{{ __('Shopinn Address') }}" required>

                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>{{ __('Facebook Page') }}</label>
                                    <input autocomplete="off" type="url" name="facebook_page" class="form-control @error('facebook_page') is-invalid @enderror" value="{{ company_get('facebook_page') }}" placeholder="{{ __('Facebook Page') }}">

                                    @error('facebook_page')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>{{ __('Facebook Group') }}</label>
                                    <input autocomplete="off" type="url" name="facebook_group" class="form-control @error('facebook_group') is-invalid @enderror" value="{{ company_get('facebook_group') }}" placeholder="{{ __('Facebook Group') }}">

                                    @error('facebook_group')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>{{ __('Instagram') }}</label>
                                    <input autocomplete="off" type="url" name="instagram" class="form-control @error('instagram') is-invalid @enderror" value="{{ company_get('instagram') }}" placeholder="{{ __('Instagram') }}">

                                    @error('instagram')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>{{ __('Twitter') }}</label>
                                    <input autocomplete="off" type="url" name="twitter" class="form-control @error('twitter') is-invalid @enderror" value="{{ company_get('twitter') }}" placeholder="{{ __('Twitter') }}">

                                    @error('twitter')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('Shopinn Short Note') }} <span class="required">*</span></label>
                                    <textarea name="short_note" class="form-control @error('short_note') is-invalid @enderror" placeholder="{{ __('Shopinn Short Description For Homepage') }}" required>{{ company_get('short_note') }}</textarea>

                                    @error('short_note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('About Shopinn') }} <span class="required">*</span></label>
                                    {{-- <div id="summernote-usage"></div> --}}
                                    <textarea autocomplete="off" name="about" class="form-control summernote @error('about') is-invalid @enderror" placeholder="{{ __('Details About Shopinn') }}" rows="5" required>{{ company_get('about') }}</textarea>

                                    @error('about')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    {{ __('Update About') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('admin.product.modals.add_new')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        var summernote = $('.summernote');
        var placeholder = summernote.attr('placeholder');
        summernote.summernote({
            height: 200,
            placeholder: placeholder
        });
    </script>
@endsection
