@extends('layouts.admin.app')

@section('page_title', 'Settings | Company | Profile')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Profile
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Settings</li>
<li class="breadcrumb-item">Company</li>
<li class="breadcrumb-item">Profile</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}

@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
