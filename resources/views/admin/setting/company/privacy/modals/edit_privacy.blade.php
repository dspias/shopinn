<div class="modal slide-in-right modal-right fade " id="edit_privacy">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Edit Privacy and Policy') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.setting.company.privacy.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="key" id="update_key">
                                <div class="form-group">
                                    <label>{{ __('Policy Title') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" name="title" id="update_title" class="form-control input-lg @error('title') is-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Type Title') }}" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Type Description') }}</label>
                                    <textarea name="description" rows="5" id="update_description" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Type Description') }}">{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                {{-- <div class="checkbox checkbox-primary font-size-12">
                                    <input autocomplete="off" id="update_type_is_active" name="is_active" type="checkbox">
                                    <label for="update_type_is_active">Active This Type?</label>
                                </div> --}}
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">Update <span class="text-uppercase">Policy</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
