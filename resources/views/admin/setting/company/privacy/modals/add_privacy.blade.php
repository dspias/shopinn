<div class="modal slide-in-right modal-right fade " id="add_privacy">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('New Policy') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.setting.company.privacy.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Policy Title') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" name="title" class="form-control input-lg @error('title') is-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Policy Title') }}" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Policy Description') }}</label>
                                    <textarea name="description" rows="5" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Policy Description') }}">{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Create as new') }} <span class="text-uppercase">{{ __('Policy') }}</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
