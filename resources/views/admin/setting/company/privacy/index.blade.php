@extends('layouts.admin.app')

@section('page_title', 'Settings | Company | Privacy Policy')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Privacy & Policy') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">{{ __('Company') }}</li>
<li class="breadcrumb-item">{{ __('Privacy Policy') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">{{ __('All Privacy Policy') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#add_privacy" class="btn btn-primary btn-sm float-right text-bold m-b-0">{{ __('Add New') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Title') }}</th>
                                    <th>{{ __('Description') }}</th>
                                    <th>{{ __('Added Date') }}</th>
                                    <th>{{ __('Updated Date') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ((array)$policies as $sl => $policy)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $policy['title'] }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $policy['description'] }}</span>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ get_date($policy['created_at'], 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ get_date($policy['updated_at'], 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    @php
                                                        $en_policy = json_encode($policy);
                                                    @endphp
                                                    <a href="javascript:void(0);" data-keyboard="false" data-toggle="modal" data-target="#edit_privacy" data-key="{{ $sl }}" data-todo="{{ $en_policy }}">
                                                        <i class="fa fa-edit pdd-right-10 text-info"></i>
                                                        <span>{{ __('Edit') }}</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="confirmation text-danger" href="{{ route('admin.setting.company.privacy.delete', ['key' => $sl]) }}">
                                                        <i class="fa fa-trash pdd-right-10 text-danger"></i>
                                                        <span>{{ __('Delete') }}</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- D:\laragon\www\shopinn\resources\views\admin\setting\product\type\modals\new_type.blade.php --}}
    @include('admin.setting.company.privacy.modals.add_privacy')
    @include('admin.setting.company.privacy.modals.edit_privacy')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#edit_privacy').on('show.bs.modal', function (e) {
            // do something...
            var button = $(e.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');
            var key = button.data('key');

            var modal = $(this);
            modal.find('.modal-content #update_title').val(data.title);
            modal.find('.modal-content #update_description').val(data.description);
            modal.find('.modal-content #update_key').val(key);
          })
    </script>
@endsection
