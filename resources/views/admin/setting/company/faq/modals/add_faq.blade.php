<div class="modal slide-in-right modal-right fade " id="add_faq">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('New Faq') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.setting.company.faq.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Question') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" name="title" class="form-control input-lg @error('title') is-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Question') }}" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Answer') }}</label>
                                    <textarea name="description" rows="5" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Answer here...') }}">{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Create as new') }} <span class="text-uppercase">{{ __('faq') }}</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
