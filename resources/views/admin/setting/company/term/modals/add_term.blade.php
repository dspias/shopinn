<div class="modal slide-in-right modal-right fade " id="add_term">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('New Term') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.setting.company.term.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Term Title') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" name="title" class="form-control input-lg @error('title') is-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Term Title') }}" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Term Description') }}</label>
                                    <textarea name="description" rows="5" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Term Description') }}">{{ old('description') }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Create as new') }} <span class="text-uppercase">{{ __('Term') }}</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
