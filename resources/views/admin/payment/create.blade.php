@extends('layouts.admin.app')

@section('page_title', 'Payments | Create New Payment')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        .card{
            border: 1px solid #ff084e;
            transition: 0.3s all ease-in-out !important;
        }
        .card:hover{
            background-color: #ff084e;
            transition: 0.3s all ease-in-out !important;
        }
        .card .text-shopinn{
            transition: 0.3s all ease-in-out !important;
        }
        .card:hover .text-shopinn{
            color: #ffffff !important;
            transition: 0.3s all ease-in-out !important;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Create New Payment') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Payments') }}</li>
<li class="breadcrumb-item">{{ __('Create New Payment') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container">
    <div class="row h-100">
        {{-- For Shop --}}
        <a href="javascript:void(0);" data-toggle="modal" data-target="#paymentForShop" class="col-md-4">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('For Shop') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Payment For Shop') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End For Shop --}}

        {{-- For Reseller --}}
        <a href="javascript:void(0);" data-toggle="modal" data-target="#paymentForReseller" class="col-md-4">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('For Reseller') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Payment For Reseller') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End For Shop --}}

        {{-- For Rider --}}
        <a href="javascript:void(0);" data-toggle="modal" data-target="#paymentForRider" class="col-md-4">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('For Rider') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Payment For Rider') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End For Rider --}}
    </div>

    @include('admin.payment.modals.shop')
    @include('admin.payment.modals.reseller')
    @include('admin.payment.modals.rider')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
