@extends('layouts.admin.app')

@section('page_title', 'Show | Invoice')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Invoice') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Show') }}</li>
<li class="breadcrumb-item">{{ __('Invoice') }}</li>
@endsection

@php
    $user = $payment->user;
@endphp


@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="pdd-vertical-5 pdd-horizon-10 border bottom print-invisible">
                <ul class="list-unstyle list-inline text-right">
                    <li class="list-inline-item">
                        <a href="#" class="btn text-gray text-hover display-block padding-10 no-mrg-btm" onclick="window.print();">
                            <i class="ti-printer text-info pdd-right-5"></i>
                            <b>{{ __('Print Invoice') }}</b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="pdd-horizon-30">
                    <div class="row mrg-top-20">
                        <div class="col-md-6">
                            <div class="inline-block">
                                <img class="img-responsive" src="{{ asset('assets/images/logo/logo.png') }}" alt="" />
                                <address class="pdd-left-10 mrg-top-20">
                                    <b class="text-dark">{{ __('SHOPINN BD') }}</b><br />
                                    <b class="text-dark" title="{{ __('Email:') }}">{{ __('Email:') }}</b>
                                    <span class="text-info">{{ company_get('email') }}</span>
                                    <br>
                                    <b class="text-dark" title="Phone">Phone:</b>
                                    <span class="text-info">{{ company_get('mobile') }}</span>
                                </address>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 offset-md-7">
                                    <div class="mrg-top-20">
                                        <h2 class="text-bold text-right">{{ __('INVOICE') }}</h2>
                                        {{-- <div class="text-dark text-uppercase inline-block"><b>Invoice No :</b></div> --}}
                                        {{-- <div class="pull-right">#1668</div> --}}

                                        <div class="text-dark text-uppercase inline-block"><b>Date :</b></div>
                                        <div class="pull-right">{{ get_date($payment->updated_at, 'd M Y') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-6">
                            <h3 class="pdd-left-10 mrg-top-10">{{ __('Invoice To') }}</h3>
                            <address class="pdd-left-10 mrg-top-10">
                                @php
                                    $address = ($user->role_id == 2) ? $user->shop->shop_address : $user->address;
                                @endphp
                                <b class="text-dark">{{ $user->name }}</b><br />
                                <span>{!! $address !!}</span>
                            </address>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-12">
                            {{-- add livewire here --}}
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">{{ __('Sl.') }}</th>
                                        <th class="text-center">{{ __('Order ID') }}</th>
                                        <th class="text-center">{{ __('Amount') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0.00;
                                        $orderList = $payment->orders;
                                    @endphp
                                    @foreach ($orderList as $key => $report)
                                        <tr>
                                            @php
                                                $order = $report->order;
                                                $sub_total = 0.00;
                                                if($user->role_id == 2){
                                                    $sub_total = get_shop_earn_per_order_paid($order->id, $user->id);
                                                } elseif($user->role_id == 3){
                                                    $sub_total = $order->reseller_earn;
                                                } elseif($user->role_id == 4){
                                                    $sub_total = $order->rider_earn;
                                                }
                                                $total += $sub_total;
                                            @endphp
                                            <td class="text-center">{{ $key+1 }}</td>
                                            <td class="text-center" style="padding-top: 20px;">{{ $order->oid }}</td>
                                            <td class="text-center" style="padding-top: 20px;">{{ $sub_total }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row mrg-top-30">
                                <div class="col-md-12">
                                    <div class="pull-right text-right">
                                        <hr />
                                        <input type="hidden" id="total_value" value="{{ $total }}">
                                        <h3><b>Total :</b> <span id="total_display">{{ $total }}</span></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row mrg-top-30">
                                <div class="col-md-12">
                                    <div class="border top bottom pdd-vertical-20">
                                        {{-- <p class="text-opacity">
                                            <small>
                                                {{ company_get('short_note') }}
                                            </small>
                                        </p> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row mrg-vertical-20">
                                <div class="col-md-6">
                                    <img class="img-responsive text-opacity" width="100" src="{{ asset('assets/images/logo/logo.png') }}" alt="" />
                                </div>
                                <div class="col-md-6 text-right">
                                    <small><b>Phone:</b> {{ company_get('mobile') }}</small>
                                    <br />
                                    <small>{{ company_get('email') }}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
