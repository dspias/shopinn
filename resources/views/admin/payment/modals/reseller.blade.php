<div class="modal fade" id="paymentForReseller">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.payment.create.invoice') }}" method="get">
                <div class="modal-header">
                    <h4 class="text-bold">{{ __('Make Payment For Reseller') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>{{ __('Select Reseller') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <select class="@error('user_id') is-invalid @enderror" name="user_id" id="selectize-dropdown" required>
                                        <option value="" disabled selected>{{ __('Select Reseller') }}</option>
                                        @foreach ($resellers as $reseller)
                                            <option value="{{ $reseller->id }}">{{ $reseller->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-border">
                    <div class="text-right">
                        <button class="btn btn-dark btn-sm" data-dismiss="modal">{{ __('Cancel') }}</button>
                        <button type="submit" class="btn btn-primary btn-sm">{{ __('Make Payment') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
