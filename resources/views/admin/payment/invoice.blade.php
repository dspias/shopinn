@extends('layouts.admin.app')

@section('page_title', 'Payments | Invoice')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Invoice') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Payments') }}</li>
<li class="breadcrumb-item">{{ __('Invoice') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="pdd-vertical-5 pdd-horizon-10 border bottom print-invisible">
                <ul class="list-unstyle list-inline text-right">
                    <li class="list-inline-item">
                        <a href="#" class="btn text-gray text-hover display-block padding-10 no-mrg-btm" onclick="window.print();">
                            <i class="ti-printer text-info pdd-right-5"></i>
                            <b>{{ __('Print Invoice') }}</b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="pdd-horizon-30">
                    <div class="row mrg-top-20">
                        <div class="col-md-6">
                            <div class="inline-block">
                                <img class="img-responsive" src="{{ asset('assets/images/logo/logo.png') }}" alt="" />
                                <address class="pdd-left-10 mrg-top-20">
                                    <b class="text-dark">{{ __('SHOPINN BD') }}</b><br />
                                    <b class="text-dark" title="{{ __('Email:') }}">{{ __('Email:') }}</b>
                                    <span class="text-info">{{ company_get('email') }}</span>
                                    <br>
                                    <b class="text-dark" title="Phone">Phone:</b>
                                    <span class="text-info">{{ company_get('mobile') }}</span>
                                </address>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 offset-md-7">
                                    <div class="mrg-top-20">
                                        <h2 class="text-bold text-right">{{ __('INVOICE') }}</h2>
                                        {{-- <div class="text-dark text-uppercase inline-block"><b>Invoice No :</b></div> --}}
                                        {{-- <div class="pull-right">#1668</div> --}}

                                        <div class="text-dark text-uppercase inline-block"><b>Date :</b></div>
                                        <div class="pull-right">{{ date('d M Y') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-6">
                            <h3 class="pdd-left-10 mrg-top-10">{{ __('Invoice To') }}</h3>
                            <address class="pdd-left-10 mrg-top-10">
                                @php
                                    $address = ($user->role_id == 2) ? $user->shop->shop_address : $user->address;
                                @endphp
                                <b class="text-dark">{{ $user->name }}</b><br />
                                <span>{!! $address !!}</span>
                            </address>
                        </div>
                    </div>

                    <form action="{{ route('admin.payment.invoice', ['user_id' => $user->id]) }}" method="post">
                        @csrf
                        <div class="row mrg-top-20">
                            <div class="col-md-12">
                                {{-- add livewire here --}}
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{ __('Action') }}</th>
                                            <th class="text-center">{{ __('Order ID') }}</th>
                                            <th class="text-center">{{ __('Amount') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0.00;
                                        @endphp
                                        @foreach ($orders as $key => $order)
                                            <tr>
                                                @php
                                                    $sub_total = 0.00;
                                                    if($user->role_id == 2){
                                                        $sub_total = get_shop_earn_per_order($order->id, $user->id);
                                                    }
                                                    elseif($user->role_id == 3){
                                                        $sub_otal = $order->reseller_earn;
                                                    } elseif($user->role_id == 4){
                                                        $sub_total = $order->rider_earn;
                                                    }
                                                    $total += $sub_total;
                                                @endphp
                                                <td class="text-center">
                                                    <div class="checkbox text-center">
                                                        <input id="checkbox_{{ $key }}_{{ $order->id }}" name="orders[]" value="{{ $order->id }}" onclick="clickOrder(this);" type="checkbox" checked>
                                                        <label for="checkbox_{{ $key }}_{{ $order->id }}"></label>
                                                        <input type="hidden" class="sub_total" value="{{ $sub_total }}">
                                                    </div>
                                                </td>
                                                <td class="text-center" style="padding-top: 20px;">{{ $order->oid }}</td>
                                                <td class="text-center" style="padding-top: 20px;">{{ $sub_total }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row mrg-top-30">
                                    <div class="col-md-12">
                                        <div class="pull-right text-right">
                                            <hr />
                                            <input type="hidden" id="total_value" value="{{ $total }}">
                                            <h3><b>Total :</b> <span id="total_display">{{ $total }}</span></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mrg-top-30">
                                    <div class="col-md-12">
                                        <div class="border top bottom pdd-vertical-20">
                                            {{-- <p class="text-opacity">
                                                <small>
                                                    {{ company_get('short_note') }}
                                                </small>
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row mrg-vertical-20">
                                    <div class="col-md-6">
                                        <img class="img-responsive text-opacity" width="100" src="{{ asset('assets/images/logo/logo.png') }}" alt="" />
                                    </div>
                                    <div class="col-md-6 text-right">
                                        {{-- <small><b>Phone:</b> {{ company_get('mobile') }}</small>
                                        <br />
                                        <small>{{ company_get('email') }}</small> --}}
                                        <button class="btn btn-primary btn-sm" type="submit">{{ __('Make Payment') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function clickOrder(that)
        {
            var input = $(that);
            var total = Number($('#total_value').val());
            var value = 0.00;
            if(input.prop("checked") == true){
                value = Number(input.parent().find('.sub_total').val());
                total += value;
                $('#total_value').val(total)
                $('#total_display').html(total);
            }
            else if(input.prop("checked") == false){
                value = Number(input.parent().find('.sub_total').val());
                total -= value;
                $('#total_value').val(total)
                $('#total_display').html(total);
            }
        }
    </script>
@endsection
