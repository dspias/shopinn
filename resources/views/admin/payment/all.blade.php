@extends('layouts.admin.app')

@section('page_title', 'Payments | All Payments')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .row-click{
        cursor: pointer;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('All Payments') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Payments') }}</li>
<li class="breadcrumb-item">{{ __('All Payments') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block p-20 p-t-20">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th class="text-center">{{ __('Invoice ID') }}</th>
                                    <th class="text-center">{{ __('Author') }}</th>
                                    <th class="text-center">{{ __('Role') }}</th>
                                    <th class="text-center">{{ __('Payment Date') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($payments as $sl => $payment)
                                    <tr class="row-click" data-link="{{ route('admin.payment.show', ['payment_id' => $payment->id]) }}" data-target="_self">
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $payment->invoice_id }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $payment->user->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $payment->user->role->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-bold">{{ get_date($payment->updated_at, 'd M Y') }}</span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });
    </script>
@endsection
