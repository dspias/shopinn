{{-- Upload File Modal --}}
<div class="modal fade" id="add_new">
    {{-- <div class="modal-dialog modal-lg" role="document"> --}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header border bottom">
                <h4 class="pull-left"><b>{{ __('Add New') }} <span class="text-shopinn">{{ __('Shop / Reseller') }}</span></b></h4>
                <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                    <i class="ti-close"></i>
                </button>
            </div>

            <div class="modal-body media-details-modal">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ route('admin.shop.create') }}" target="_blank" class="btn btn-primary text-bold btn-block float-right m-b-5 p-50 text-uppercase" data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Add New Shop</b>">{{ __('Shop') }}</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('admin.reseller.create') }}" target="_blank" class="btn btn-primary text-bold btn-block float-right m-b-5 p-50 text-uppercase" data-toggle="tooltip" data-placement="top" data-html="true" title="<b>Add New Reseller</b>">{{ __('Reseller') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
