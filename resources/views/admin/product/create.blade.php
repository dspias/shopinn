@extends('layouts.admin.app')

@section('page_title', 'Products | Create New Product')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }

        .password-generate {
            position: absolute;
            right: 15px;
            font-weight: bold;
            text-transform: uppercase;
        }


        .note-fontname button{
            min-width: 200px;
        }

        .note-para .note-btn-group{
            display: none;
        }

        .note-insert button:nth-child(2),
        .note-insert button:nth-child(3){
            display: none;
        }

        .radio.radio-primary input[type=radio]:checked + label:before {
            color: #ff084e;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Create New Product
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Products</li>
<li class="breadcrumb-item">Create New Product</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form action="#" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Details Of') }} <span class="text-shopinn">{{ __('Shop / Reseller') }}</span></h4>

                                <a href="javascript:void(0);" data-toggle="modal" data-target="#add_new"  class="btn btn-primary text-bold btn-sm float-right m-b-0">{{ __('Add New Shop / Reseller') }}</a>
                            </div>

                            <hr class="m-t-0">

                            <div class="card-block p-25">
                                <div class="row justify-content-center">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>{{ __('Creating Product For?') }} <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <div class="radio radio-inline radio-primary">
                                                    <input type="radio" name="create_for" id="for_shop">
                                                    <label for="for_shop">{{ __('Shop') }}</label>
                                                </div>
                                                <div class="radio radio-inline radio-primary">
                                                    <input type="radio" name="create_for" id="for_reseller">
                                                    <label for="for_reseller">{{ __('Reseller') }}</label>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="col-12 d-none" id="select_shop">
                                        <div class="form-group">
                                            <label>{{ __('Select Shop') }} <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <select class="@error('shop_id') is-invalid @enderror" name="shop_id" id="selectize-dropdown" required>
                                                    <option value="" disabled selected>{{ __('Select Shop') }}</option>
                                                    {{-- @foreach ($shops as $shop) --}}
                                                        <option value="{{ 'shop_id' }}">{{ 'Shop_Name_Here' }}</option>
                                                    {{-- @endforeach --}}
                                                </select>
                                            </div>
        
                                            @error('shop_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12 d-none" id="select_reseller">
                                        <div class="form-group">
                                            <label>{{ __('Select Reseller') }} <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <select class="@error('reseller_id') is-invalid @enderror" name="reseller_id" id="selectize-dropdown" required>
                                                    <option value="" disabled selected>{{ __('Select Reseller') }}</option>
                                                    {{-- @foreach ($resellers as $reseller) --}}
                                                        <option value="{{ 'reseller_id' }}">{{ 'reseller_Name_Here' }}</option>
                                                    {{-- @endforeach --}}
                                                </select>
                                            </div>
        
                                            @error('reseller_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card m-b-0">
                            <div class="card-heading">
                                <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Details Of') }} <span class="text-shopinn">{{ __('Product') }}</span></h4>
                            </div>

                            <hr class="m-t-0">

                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <div>
                                            <label for="img-upload" class="pointer">
                                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">
        
                                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Product Image') }}</span>
                                                <input class="d-none @error('product_image') is-invalid @enderror" type="file" name="product_image" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('product_image') }}">
                                            </label>
                                            @error('product_image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{ __('Product Name') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Product Name') }}" required>
        
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>{{ __('Price') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="number" min="0" step="0.1" name="price" class="form-control @error('price') is-invalid @enderror" value="{{ old('price') }}" placeholder="{{ __('250tk') }}" required>
        
                                            @error('price')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>{{ __('Selling Price') }} <span class="required">*</span></label>
                                            <input autocomplete="off" type="number" min="0" step="0.1" name="selling_price" class="form-control @error('selling_price') is-invalid @enderror" value="{{ old('selling_price') }}" placeholder="{{ __('250tk') }}" required>
        
                                            @error('selling_price')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>{{ __('Product Available') }}</label>
                                            <input autocomplete="off" type="number" min="0" step="1" name="available" class="form-control @error('available') is-invalid @enderror" value="{{ old('available') }}" placeholder="{{ __('100') }}">
        
                                            @error('available')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ __('Product Details') }} <span class="required">*</span></label>
                                            {{-- <div id="summernote-usage"></div> --}}
                                            <textarea autocomplete="off" name="details" class="form-control summernote @error('details') is-invalid @enderror" placeholder="{{ __('Product Details') }}" rows="5" required>{{ old('details') }}</textarea>
        
                                            @error('details')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active" class="m-b-0 text-bold text-shopinn">{{ __('Activate This Product?') }}</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    {{ __('Create Product') }}
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('admin.product.modals.add_new')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        var summernote = $('.summernote');
        // var placeholder = summernote.attr('placeholder');
        summernote.summernote({
            height: 200,
            // placeholder: placeholder
        });
    </script>


    <script>
        $( document ).ready(function(){
            var shop = $('#for_shop');
            var reseller = $('#for_reseller');

            shop.click(function() { 
                $('#select_reseller').addClass('d-none');
                $('#select_reseller select').attr('disabled', 'disabled');

                $('#select_shop').removeClass('d-none');
                $('#select_shop select').removeAttr('disabled');
            });

            reseller.click(function() { 
                $('#select_shop').addClass('d-none');
                $('#select_shop select').attr('disabled', 'disabled');

                $('#select_reseller').removeClass('d-none');
                $('#select_reseller select').removeAttr('disabled');
            });
        });
    </script>
@endsection
