@extends('layouts.admin.app')

@section('page_title', 'Product | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Details Product
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Products</li>
<li class="breadcrumb-item"><a href="{{ route($route) }}"> {{ $page }} </a></li>
<li class="breadcrumb-item">{{ $product->name }}</li>
<li class="breadcrumb-item">Details</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu">
                                {{--  <li>
                                    <a href="#">
                                        <i class="ti-pencil text-info pdd-right-10"></i>
                                        <span>Edit product</span>
                                    </a>
                                </li>  --}}
                                @if($product->approved_at == null)
                                <li>
                                    <a href="{{ route('admin.product.approve', ['product_id' => encrypt($product->id)]) }}" class="confirmation">
                                        <i class="ti-star text-success pdd-right-10"></i>
                                        <span>Approve</span>
                                    </a>
                                </li>
                                @endif
                                @if($product->is_active == 1)
                                <li>
                                    <a href="{{ route('admin.product.change_status', ['product_id' => encrypt($product->id)]) }}" class="confirmation">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactivate</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('admin.product.change_status', ['product_id' => encrypt($product->id)]) }}" class="confirmation">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Activate</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class=" bottom">
                    @php
                        $urls = (array)get_images($product, 'details');
                    @endphp
                    <div class="row mrg-top-70">
                        @foreach ($urls as $url)
                            <div class="col-md-3">
                                <img class="img-thumbnail img-responsive" src="{{ $url }}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <h4 class="mrg-top-20 no-mrg-btm text-semibold text-center">{{ $product->name }}
                        <sup>
                            @if($product->approved_at == null)
                            <i class="ti-star text-danger pdd-right-5"></i>(Not Approved)
                            @elseif($product->is_active == 1)
                            <i class="ti-check text-success"></i>
                            @else
                            <i class="ti-close text-danger"></i>
                            @endif
                        </sup>
                    </h4>
                </div>
                <div class="pdd-horizon-30 pdd-vertical-20">
                    @if($product->description != null)
                        <h5 class="text-bold text-dark">{{ __('About Product') }}</h5>
                        <div>{!! $product->description !!}</div>
                    @endif

                    <h5 class="text-bold text-dark">{{ __('Product Details') }}</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Name') }}</th>
                                        <td class="text-info">{{ $product->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Shop Name') }}</th>
                                        <td class="text-shopinn">{{ $product->shop->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Category') }}</th>
                                        <td class="text-info">
                                            <b>{{ $product->category->name }} <sup class="text-mute">({{ $product->category->type->name }})</sup></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Stocks') }}</th>
                                        <td class="text-info">{{ $product->stock }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Purchase Price') }}</th>
                                        <td class="text-info">{{ $product->buy_price }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Selling Price') }}</th>
                                        <td class="text-info">{{ $product->sell_price }}</td>
                                    </tr>
                                    @if($product->updated_price != null)
                                    <tr>
                                        <th>{{ __('Shopinn Provided Price') }}</th>
                                        <td class="text-info">
                                            <form action="{{ route('admin.product.update_price', ['product_id' => encrypt($product->id)]) }}" method="post">
                                                @csrf
                                                <input autocomplete="off" type="number" name="updated_price" class="form-control @error('updated_price') is-invalid @enderror" value="{{ $product->updated_price }}" required>
                                                @error('updated_price')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <button type="submit" class="btn btn-primary btn-xs text-bold mrg-top-10">
                                                    <i class="ti-save"></i>
                                                    update
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @else
                                    <tr>
                                        <th>{{ __('Add new price from shopinn') }}</th>
                                        <td class="text-info">
                                            <form action="{{ route('admin.product.update_price', ['product_id' => encrypt($product->id)]) }}" method="post">
                                                @csrf
                                                <input autocomplete="off" type="number" name="updated_price" class="form-control @error('updated_price') is-invalid @enderror" value="{{ old('updated_price') }}" required>
                                                @error('updated_price')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <button type="submit" class="btn btn-primary btn-xs text-bold mrg-top-10">
                                                    <i class="ti-save"></i>
                                                    add
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Sizes') }}</th>
                                        <td>
                                            @foreach ($product->sizes as $size)
                                                <span class="badge badge-info p-2 m-1">{{ $size->name }}</span>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Colors') }}</th>
                                        <td>
                                            @foreach ($product->colors as $color)
                                                <span class="badge badge-danger p-2 m-1">{{ $color->name }}</span>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Tags') }}</th>
                                        <td>
                                            @foreach ($product->tags as $tag)
                                                <span class="badge badge-success p-2 m-1">{{ $tag->name }}</span>
                                            @endforeach
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
