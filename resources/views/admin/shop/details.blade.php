@extends('layouts.admin.app')

@section('page_title', 'Shop | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Shop Details') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Shops') }}</li>
<li class="breadcrumb-item"><a href="{{ route($route) }}"> {{ $page }} </a></li>
<li class="breadcrumb-item">{{ $shop->name }}</li>
<li class="breadcrumb-item">{{ __('Details') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu">
                                {{--  <li>
                                    <a href="#">
                                        <i class="ti-pencil text-info pdd-right-10"></i>
                                        <span>Edit Shop</span>
                                    </a>
                                </li>  --}}
                                @if($shop->approved_at == null)
                                <li>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#delivery_charge">
                                        <i class="ti-star text-success pdd-right-10"></i>
                                        <span>{{ __('Approve') }}</span>
                                    </a>
                                    {{-- <a href="{{ route('admin.shop.approve', ['shop_id' => encrypt($shop->id)]) }}" class="confirmation">
                                        <i class="ti-star text-success pdd-right-10"></i>
                                        <span>Approve</span>
                                    </a> --}}
                                </li>
                                @endif
                                @if($shop->is_active == 1)
                                <li>
                                    <a href="{{ route('admin.shop.change_status', ['shop_id' => encrypt($shop->id)]) }}" class="confirmation">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>{{ __('Deactivate') }}</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('admin.shop.change_status', ['shop_id' => encrypt($shop->id)]) }}" class="confirmation">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>{{ __('Activate') }}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="profile border bottom">
                    @php
                        $logo = get_logo($shop, 'thumb');
                        if($logo == null) $logo = asset('vendor/images/resources/author.jpg');
                    @endphp
                    <img class="mrg-top-30" src="{{ $logo }}" alt="">
                    <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $shop->name }}
                        <sup>
                            @if($shop->approved_at == null)
                            <i class="ti-star text-danger pdd-right-5"></i>({{ __('Not Approved') }})
                            @elseif($shop->is_active == 1)
                            <i class="ti-check text-success"></i>
                            @else
                            <i class="ti-close text-danger"></i>
                            @endif
                        </sup>
                    </h4>
                    <div>{!! optional($shop->shop)->shop_address !!}</div>
                </div>
                <div class="pdd-horizon-30 pdd-vertical-20">
                    @if(optional($shop->shop)->note != null)
                        <h5 class="text-bold text-dark">{{ __('About Shop') }}</h5>
                        <div>{!! optional($shop->shop)->note !!}</div>
                    @endif

                    <h5 class="text-bold text-dark">{{ __('Shop Details') }}</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Shop Name') }}</th>
                                        <td>{{ $shop->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Shop Mobile Number') }}</th>
                                        <td>{{ optional($shop->shop)->shop_mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Shop Email') }}</th>
                                        <td>{{ optional($shop->shop)->shop_email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Shop City') }} ({{ __('District') }})</th>
                                        <td>{{ $shop->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Shop Address') }}</th>
                                        <td>{!! optional($shop->shop)->shop_address !!}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $date = new DateTime($shop->created_at);
                                        @endphp
                                        <th>{{ __('Shop Register Date') }}</th>
                                        <td>{{ $date->format('d M Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Owner Name') }}</th>
                                        <td>{{ optional($shop->shop)->owner_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Mobile Number') }}</th>
                                        <td>{{ optional($shop->shop)->owner_mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Email') }}</th>
                                        <td>{{ optional($shop->shop)->owner_email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Owner Address') }}</th>
                                        <td>{!! optional($shop->shop)->owner_address !!}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            if($shop->approved_at != null)
                                                $approved = new DateTime($shop->approved_at);
                                            else
                                                $approved = null;
                                        @endphp
                                        <th>{{ __('Shop Activation Date') }}</th>
                                        <td class="text-info">{{ ($approved != null) ? $approved->format('d M Y'):'Pending' }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Active Package') }}</th>
                                        <td class="text-shopinn">
                                            @if (subsribed($shop->id))
                                                {{ optional(subsribed($shop->id))->package->name }}

                                            @else
                                            {{ __('Not Subscribed') }}
                                            @endif
                                        </td>
                                    </tr>

                                    @if(optional($shop)->shop != null)
                                        <tr>
                                            <th>{{ __('Delivery Charge') }}</th>
                                            <td>
                                                <form action="{{ route('admin.shop.update_delivery', ['shop_id' => encrypt($shop->id)]) }}" method="post">
                                                    @csrf
                                                    <div class="input-group">
                                                        {{--  <label for="inside_city">{{ __('Inside City') }}</label>  --}}
                                                        <input autocomplete="off" min="0" type="number" name="inside_city" class="form-control @error('inside_city') is-invalid @enderror" placeholder="{{ __('Inside City') }}" value="{{ optional($shop->shop)->inside_city }}" required>

                                                        {{--  <label for="outside_city">{{ __('Outside City') }}</label>  --}}
                                                        <input autocomplete="off" min="0" type="number" name="outside_city" class="form-control @error('outside_city') is-invalid @enderror" placeholder="{{ __('Outside City') }}" value="{{ optional($shop->shop)->outside_city }}" required>

                                                        @error('inside_city')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                        @error('outside_city')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-sm text-bold mrg-top-10">
                                                        <i class="ti-save"></i>
                                                        update
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="mrg-top-30 text-center">
                        <ul class="list-unstyled list-inline">
                            @if(optional($shop->shop)->facebook != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $shop->shop->facebook }}" class="btn btn-facebook btn-icon btn-rounded">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            @endif
                            @if(optional($shop->shop)->instagram != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $shop->shop->instagram }}" class="btn btn-instagram btn-icon btn-rounded">
                                    <i class="ti-instagram"></i>
                                </a>
                            </li>
                            @endif
                            @if(optional($shop->shop)->twitter != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $shop->shop->twitter }}" class="btn btn-twitter btn-icon btn-rounded">
                                    <i class="ti-twitter"></i>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu" style="width: 180px;">
                                {{-- <li>
                                    <a  href="javascript:void(0);" data-toggle="modal" data-target="#requested_package">
                                        <i class="ti-reload text-shopinn pdd-right-10"></i>
                                        <span>{{ __('Request') }}</span>
                                    </a>
                                </li> --}}
                                <li>
                                    <a  href="javascript:void(0);" data-toggle="modal" data-target="#assign_package">
                                        <i class="ti-plus text-info pdd-right-10"></i>
                                        <span>{{ __('Assign Package') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3">{{ __('Package history Of') }} <span class="text-shopinn">{{ $shop->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table class="table table-lg table-hover table-bordered table-responsive data-table">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl') }}</th>
                                    <th class="text-center">{{ __('Package') }}</th>
                                    <th class="text-center">{{ __('Activation Date') }}</th>
                                    <th class="text-center">{{ __('End Date') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($packages as $sl => $package)
                                <tr>
                                    <td class="text-center">
                                        {{ __($sl+1) }}
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $package->package->name }}</b>
                                            </span>
                                        </div>
                                    </td>

                                    {{-- @php
                                        $dateTime = new DateTime($order->created_at);
                                    @endphp --}}

                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ get_date($package->start, 'd M Y') }}</b>
                                            </span>
                                        </div>
                                    </td>

                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if(is_null($package->end))
                                                <span>{{ __('Active Package') }}</span>
                                            @else
                                                <span>{{ get_date($package->end, 'd M Y') }}</span>
                                            @endif
                                        </div>
                                    </td>

                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($package->is_active == 1)
                                                <b class="text-success">{{ __('Active') }}</b>
                                            @else
                                                <b class="text-danger">{{ __('Deactive') }}</b>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('All Products Of') }} <span class="text-shopinn">{{ $shop->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th>{{ __('Product Name') }}</th>
                                    <th>{{ ('Shop Name') }}</th>
                                    <th>{{ ('Upload Date') }}</th>
                                    <th>{{ __('Stock') }}</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($shop->products as $sl => $product)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $product->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $product->shop->name }}</b>
                                            </span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ get_date($product->created_at, 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $product->stock }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($product->approved_at == null)
                                                <b class="text-info">Pending</b>
                                            @elseif($product->approved_at != null && $product->is_active)
                                                <b class="text-success">Active</b>
                                            @elseif($product->approved_at != null && !$product->is_active)
                                                <b class="text-danger">Inactive</b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.product.details', ['page'=>'all','product_id'=>encrypt($product->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="card">
                {{--  <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu" style="width: 180px;">
                                <li>
                                    <a href="#">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Active Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactive Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-reload text-info pdd-right-10"></i>
                                        <span>Pending Products</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>  --}}

                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Payment Details Of') }} <span class="text-shopinn">{{ $shop->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table class="table table-lg table-hover table-bordered table-responsive data-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">{{ __('Invoice ID') }}</th>
                                    <th class="text-center">{{ __('Payment Date') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $payments = $shop->paymentReports;
                                @endphp
                                @foreach ($payments as $sl => $payment)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $payment->invoice_id }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-bold">{{ get_date($payment->updated_at, 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.payment.show', ['payment_id' => $payment->id]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.shop.modals.delivery_charge')
    @include('admin.shop.modals.request')
    @include('admin.shop.modals.assign')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>
@endsection
