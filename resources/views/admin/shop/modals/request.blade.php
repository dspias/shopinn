<div class="modal slide-in-right modal-right fade " id="requested_package">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Requested Package') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="#" method="post">
                                @csrf

                                <h2 class="text-shopinn text-bold">{{ __('Package_Name_Here') }}</h2>
                                <input type="hidden" name="{{ 'package_id_here' }}">

                                <div class="checkbox checkbox-primary font-size-12">
                                    <input autocomplete="off" id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active">{{ __('Activate This Package') }}?</label>
                                </div>
                                
                                <br>

                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Approve Package') }}</button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
