<div class="modal slide-in-right modal-right fade " id="assign_package">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Assign New Package') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.shop.package.assign', ['shop_id' => encrypt($shop->id)]) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Select Package') }} <span class="required">*</span></label>
                                    <select name="package_id" id="selectize-dropdown" required>
                                        <option value="" disabled selected>{{ __('Select Package') }}</option>
                                        @foreach($all_packages as $package)
                                            <option value="{{ $package->id }}">{{ __($package->name) }}</option>
                                        @endforeach
                                    </select>

                                    @error('package_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                {{-- <div class="checkbox checkbox-primary font-size-12">
                                    <input autocomplete="off" id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active">{{ __('Activate This Package') }}?</label>
                                </div> --}}

                                <br>

                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Assign Package') }}</button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
