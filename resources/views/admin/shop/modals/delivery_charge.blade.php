<div class="modal slide-in-right modal-right fade " id="delivery_charge">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Assign Delivery Charge') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.shop.approve', ['shop_id' => encrypt($shop->id)]) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Inside City') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="number" min="0" step="1" name="inside_city" class="form-control input-lg @error('inside_city') is-invalid @enderror" value="{{ old('inside_city') }}" placeholder="{{ __('Inside City') }}" required>

                                    @error('inside_city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Outside City') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="number" min="0" step="1" name="outside_city" class="form-control input-lg @error('outside_city') is-invalid @enderror" value="{{ old('outside_city') }}" placeholder="{{ __('Outside City') }}" required>

                                    @error('outside_city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">{{ __('Approve Shop') }}</button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
