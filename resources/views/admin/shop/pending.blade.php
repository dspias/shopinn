@extends('layouts.admin.app')

@section('page_title', 'Shops | Pending Shops')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Pending Shops
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Shops</li>
<li class="breadcrumb-item">Pending Shops</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card m-b-0">
                                <div class="card-block p-15">
                                    <h3 class="no-mrg-vertical text-center font-size-35">
                                        <sup class="text-bold text-info">{{ $pending }}</sup> 
                                        / 
                                        <sub class="text-bold">{{ $all }}</sub>
                                    </h3>
                                    <div class="mrg-top-10">
                                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Pending Shops</span>
                                        <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%</span>
                                        <div class="progress progress-success">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Shop Email</th>
                                    <th>Join At</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($shops as $sl => $shop)
                                @php
                                    $verified = ($shop->shop)? 1:0;
                                @endphp
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $shop->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>@if($verified) {{ $shop->shop->owner_name }} @else {{ 'Not Verified' }} @endif</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>@if($verified) {{ $shop->shop->shop_email }} @else {{ 'Not Verified' }} @endif</b>
                                            </span>
                                        </div>
                                    </td>

                                    @php
                                        $date = new DateTime($shop->created_at);
                                    @endphp

                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $date->format('d M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($shop->approved_at == null)
                                                <b class="text-info">Pending</b>
                                            @elseif($shop->approved_at != null && $shop->is_active)
                                                <b class="text-success">Active</b>
                                            @elseif($shop->approved_at != null && !$shop->is_active)
                                                <b class="text-danger">Inactive</b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.shop.details', ['page' => 'pending', 'shop_id' => encrypt($shop->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="{{ route('admin.shop.reset.password', ['shop_id' => encrypt($shop->id)]) }}" class="confirmation">
                                                        <i class="ti-lock pdd-right-10 text-warning"></i>
                                                        <span>{{ __('Reset Pass') }}</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
