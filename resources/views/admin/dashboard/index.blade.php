@extends('layouts.admin.app')

@section('page_title', 'Dashboard')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/nvd3/build/nv.d3.min.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection


@section('page_header_title')
{{ __('Dashboard') }}
@endsection

@section('breadcrumb_item_lists')
<li class="breadcrumb-item">{{ __('Dashboard') }}</li>
@endsection


@section('main_content')

<div class="container-fluid">

    {{-- profit, sale and cost calculation start --}}
    <div class="row">
        {{-- Total Sale --}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Total Sale') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">
                        {{ $total_sale }}<sup><b class="font-size-20">৳</b></sup>
                    </h1>
                </div>
            </div>
        </div>
        {{-- End Total Sale --}}

        {{-- Shopinn Total Income --}}
        <div class="col-md-6">
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Shopinn Total Income') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-success">
                        {{ $total_shopinn_earn }}<sup><b class="font-size-20">৳</b></sup>
                    </h1>
                </div>
            </div>
        </div>
        {{-- End Shopinn Total Income --}}

        {{-- Shopinn Total Income --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Total Extra Cost') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">
                        {{ $extra_cost }}<sup><b class="font-size-20">৳</b></sup>
                    </h1>
                </div>
            </div>
        </div>
        {{-- End Shopinn Total Income --}}

        {{-- Shopinn Total Income --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Total Rider Cost') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">
                        {{ $rider_cost }}<sup><b class="font-size-20">৳</b></sup>
                    </h1>
                </div>
            </div>
        </div>
        {{-- End Shopinn Total Income --}}

        {{-- Shopinn Total Income --}}
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Total Reseller Cost') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">
                        {{ $reseller_cost }}<sup><b class="font-size-20">৳</b></sup>
                    </h1>
                </div>
            </div>
        </div>
        {{-- End Shopinn Total Income --}}
    </div>
    {{-- profit, sale and cost calculation end --}}



    {{-- graph start --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="widget card">
                <div class="card-block">
                    <h5 class="card-title text-bold">{{ __('Yearly Overview') }} <span class="text-shopinn">{{ date('Y') }}</span></h5>
                    @php
                        $avg_sale = array_sum($chart['sale']) / 12;
                        $avg_earn = array_sum($chart['earn']) / 12;
                        $avg_cost = array_sum($chart['cost']) / 12;
                    @endphp
                    <div class="row mrg-top-30">
                        <div class="col-md-4 col-sm-12 col-12 border right border-hide-md">
                            <div class="text-center pdd-vertical-10">
                                <h2 class="font-primary no-mrg-top">{{ number_format($avg_sale, 2, '.', ' ') }}<sup class="font-size-20">৳</sup></h2>
                                <p class="no-mrg-btm">{{ __('Avg. Sale') }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12 border right border-hide-md">
                            <div class="text-center pdd-vertical-10">
                                <h2 class="font-primary no-mrg-top">{{ number_format($avg_earn, 2, '.', ' ') }}<sup class="font-size-20">৳</sup></h2>
                                <p class="no-mrg-btm">{{ __('Avg. Income') }}</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="text-center pdd-vertical-10">
                                <h2 class="font-primary no-mrg-top">{{ number_format($avg_cost, 2, '.', ' ') }}<sup class="font-size-20">৳</sup></h2>
                                <p class="no-mrg-btm">{{ __('Avg. Expense') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mrg-top-35">
                        <div class="col-md-12">
                            <div>
                                <canvas id="line-chart" height="220"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- graph end --}}


    {{-- all components tracking start --}}
    <div class="row">
        <div class="col-lg-3">
            {{-- Total Orders --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Order') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $orders['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $orders['running'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Running Orders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($orders['all'], $orders['running']) }}%</span>
                        <div class="progress progress-info">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($orders['all'], $orders['running']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($orders['all'], $orders['running']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $orders['pending'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Orders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($orders['all'], $orders['pending']) }}%</span>
                        <div class="progress progress-primary">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($orders['all'], $orders['pending']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($orders['all'], $orders['pending']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $orders['cancel'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Canceled Orders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($orders['all'], $orders['cancel']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($orders['all'], $orders['cancel']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($orders['all'], $orders['cancel']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $orders['complete'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Completed Orders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($orders['all'], $orders['complete']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($orders['all'], $orders['complete']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($orders['all'], $orders['complete']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Orders End --}}


            {{-- Total Shopinn Advertise --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Shopinn Advertise') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $advertises['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $advertises['all'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Advertises') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($advertises['all'], $advertises['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($advertises['all'], $advertises['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($advertises['all'], $advertises['active']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Shopinn Advertise End --}}
        </div>

        <div class="col-lg-3">
            {{-- Total Shops --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Shop') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $shops['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $shops['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Shops') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($shops['all'], $shops['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($shops['all'], $shops['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($shops['all'], $shops['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $shops['pending'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Shops') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($shops['all'], $shops['pending']) }}%</span>
                        <div class="progress progress-primary">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($shops['all'], $shops['pending']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($shops['all'], $shops['pending']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $shops['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Inactive Shops') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($shops['all'], $shops['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($shops['all'], $shops['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($shops['all'], $shops['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Shops End --}}


            {{-- Total Products --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Product') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $products['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $products['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Products') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($products['all'], $products['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($products['all'], $products['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($products['all'], $products['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $products['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Inactive Products') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($products['all'], $products['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($products['all'], $products['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($products['all'], $products['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Products End --}}
        </div>

        <div class="col-lg-3">
            {{-- Total Packages --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Package') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $packages['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $packages['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Packages') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($packages['all'], $packages['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($packages['all'], $packages['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($packages['all'], $packages['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $packages['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Inactive Packages') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($packages['all'], $packages['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($packages['all'], $packages['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($packages['all'], $packages['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Packages End --}}

            {{-- Total Resellers --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Reseller') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $resellers['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $resellers['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Resellers') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($resellers['all'], $resellers['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($resellers['all'], $resellers['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($resellers['all'], $resellers['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $resellers['pending'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Resellers') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($resellers['all'], $resellers['pending']) }}%</span>
                        <div class="progress progress-primary">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($resellers['all'], $resellers['pending']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($resellers['all'], $resellers['pending']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $resellers['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Inactive Resellers') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($resellers['all'], $resellers['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($resellers['all'], $resellers['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($resellers['all'], $resellers['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Resellers End --}}
        </div>

        <div class="col-lg-3">
            {{-- Total Resellers --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Rider') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $riders['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $riders['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Riders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($riders['all'], $riders['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($riders['all'], $riders['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($riders['all'], $riders['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $riders['pending'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Riders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($riders['all'], $riders['pending']) }}%</span>
                        <div class="progress progress-primary">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($riders['all'], $riders['pending']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($riders['all'], $riders['pending']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $riders['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Inactive Riders') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($riders['all'], $riders['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($riders['all'], $riders['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($riders['all'], $riders['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Resellers End --}}


            {{-- Total Customers --}}
            <div class="card">
                <div class="card-block">
                    <p class="mrg-btm-5 text-center">{{ __('Total Customer') }}</p>
                    <h1 class="no-mrg-vertical font-size-35 text-center text-bold text-shopinn">{{ $customers['all'] }}</h1>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $customers['active'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Active Customers') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($customers['all'], $customers['active']) }}%</span>
                        <div class="progress progress-success">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($customers['all'], $customers['active']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($customers['all'], $customers['active']) }}%">
                            </div>
                        </div>
                    </div>
                    <div class="mrg-top-10">
                        <h2 class="no-mrg-btm">{{ $customers['inactive'] }}</h2>
                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Banned Customers') }}</span>
                        <span class="pull-right pdd-right-10 font-size-13">{{ get_percent($customers['all'], $customers['inactive']) }}%</span>
                        <div class="progress progress-danger">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ get_percent($customers['all'], $customers['inactive']) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ get_percent($customers['all'], $customers['inactive']) }}%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Total Customers End --}}
        </div>
    </div>
    {{--  all components tracking end --}}


    {{-- last 5 reviews start --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-heading border bottom">
                    <h4 class="card-title">{{ __('Latest Product Reviews From Customers') }}</h4>
                </div>
                <div class="widget-feed">
                    <ul class="list-info overflow-y-auto relative scrollable" style="max-height: 340px">
                        @foreach($reviews as $review)
                            <li class="border bottom mrg-btm-10">
                                <div class="pdd-vertical-10">
                                    <span class="thumb-img bg-primary">
                                        @php
                                            $logo = get_logo($review->product->shop, 'thumb');
                                        @endphp
                                        <img class="thumb-img" src="{{ $logo }}" height="40px" width="40px">
                                    </span>
                                    <div class="info">
                                        <a href="{{ route('admin.shop.details', ['page' => 'all', 'shop_id' => encrypt($review->product->shop->id)]) }}" class="text-link" target="_blank" style="text-decoration: none;">
                                            <span class="title">
                                                <b class="font-size-15" data-toggle="tooltip" data-title="{{ __('View Shop') }}">{{ __($review->product->shop->name) }}</b>
                                            </span>
                                        </a>
                                        <span class="sub-title">{{ ago_time($review->updated_at) }}</span>
                                    </div>
                                    <div class="mrg-top-10">
                                        <p class="no-mrg-btm">
                                            {{ $review->review }}
                                        </p>
                                    </div>
                                    <ul class="feed-action">
                                        <li>
                                            <a href="javascript:void(0);">
                                                <i class="ti-star text-shopinn pdd-right-5"></i>
                                                <span>{{ optional(\App\Models\ProductRating::where('customer_id', $review->customer_id)->first())->rate }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.customer.details', ['page' => 'all', 'customer_id' => encrypt($review->customer->id)]) }}" target="_blank" data-toggle="tooltip" data-title="{{ __('View Customer') }}">
                                                <i class="ti-user text-info pdd-right-5"></i>
                                                <span>{{ $review->customer->name }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.product.details', ['page'=>'all','product_id'=>encrypt($review->product->id)]) }}" target="_blank" data-toggle="tooltip" data-title="{{ __('View Product') }}">
                                                <i class="ti-shopping-cart text-dark pdd-right-5"></i>
                                                <span>{{ $review->product->name }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{-- last 5 reviews end --}}

</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/js/maps/jquery-jvectormap-us-aea.js') }}"></script>
    <script src="{{ asset('assets/vendors/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/nvd3/build/nv.d3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery.sparkline/index.js') }}"></script>
    <script src="{{ asset('assets/vendors/chart.js/dist/Chart.min.js') }}"></script>

    {{-- <script src="{{ asset('assets/js/dashboard/dashboard.js') }}"></script> --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        (function ($) {
            var lineChart       = document.getElementById("line-chart");
            var lineCtx         = lineChart.getContext('2d');
            lineChart.height    = 150;
            var sale          = @php echo json_encode($chart['sale']); @endphp;
            var income          = @php echo json_encode($chart['earn']); @endphp;
            var expense         = @php echo json_encode($chart['cost']); @endphp;

            var lineConfig = new Chart(lineCtx, {
                type: 'line',
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],

                    datasets: [{
                        label: 'Monthly Sale',
                        backgroundColor: 'rgba(129, 58, 173,  .1)',
                        borderColor: '#007bff',
                        pointBackgroundColor: '#007bff',
                        borderWidth: 1,
                        data: sale
                    }, {
                        label: 'Monthly Income',
                        backgroundColor: 'rgba(55, 201, 54, 0.1)',
                        borderColor: '#37c936',
                        pointBackgroundColor: '#37c936',
                        borderWidth: 1,
                        data: income
                    }, {
                        label: 'Monthly Expense',
                        backgroundColor: 'rgba(255, 60, 126, 0.1)',
                        borderColor: '#d31e25',
                        pointBackgroundColor: '#d31e25',
                        borderWidth: 1,
                        data: expense
                    }]
                },

                options: {
                    legend: {
                        display: true,
                    },
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                min: 0,
                                max: 100000,
                                stepSize: 10000,
                            }
                        }]
                    }
                }
            });

        })(jQuery);
    </script>
@endsection
