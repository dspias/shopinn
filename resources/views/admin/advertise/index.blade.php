@extends('layouts.admin.app')

@section('page_title', 'Advertises | All Admin Advertises')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Admin Advertises') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">{{ __('Admin Advertises') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block p-20 p-t-20">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Advertise Title') }}</th>
                                    <th>{{ __('Advertise Type') }}</th>
                                    <th>{{ __('Start Date') }}</th>
                                    <th>{{ __('End Date') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($advertises as $sl => $data)
                                {{-- {{ dd($data) }} --}}
                                    <tr>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $data->title }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $data->type->name }}</b>
                                                </span>
                                            </div>
                                        </td>

                                        {{-- @php
                                            $dateTime = new DateTime($order->created_at);
                                        @endphp --}}

                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-success text-bold">{{ get_date($data->start, 'd-m-Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-shopinn text-bold">{{ get_date($data->end, 'd-m-Y') }}</span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                @if ($data->is_active == 1)
                                                    <b class="text-success">{{ __('Acitve') }}</b>
                                                @else
                                                    <b class="text-danger">{{ __('Inacitve') }}</b>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-10 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('admin.advertise.show', encrypt($data->id)) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="{{ route('admin.advertise.edit', encrypt($data->id)) }}">
                                                            <i class="ti-pencil pdd-right-10 text-shopinn"></i>
                                                            <span>{{ ('Edit') }}</span>
                                                        </a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
