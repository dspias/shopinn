@extends('layouts.admin.app')

@section('page_title', 'Advertises | Shop Package Logs | All Shop Package Logs')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Shop Package Log Details') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.shop.ad.pack') }}">{{ __('Shop Package Logs') }}</a>
</li>
<li class="breadcrumb-item">{{ __('Shop Package Log Details') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">
                        {{ __($log->package->name) }}
                        @if ($log->is_active == 1)
                           <sup class="text-success">({{ __('Active') }})</sup>
                        @else
                           <sup class="text-danger">({{ __('Inactive') }})</sup>
                        @endif
                    </h4>
                    @if($log->is_active == 1)
                    <a href="{{ route('admin.advertise.change_log_status', encrypt($log->id)) }}" class="btn btn-primary btn-sm float-right text-bold m-b-0 confirmation">{{ __('Deactivate') }}</a>
                    @else
                    <a href="{{ route('admin.advertise.change_log_status', encrypt($log->id)) }}" class="btn btn-primary btn-sm float-right text-bold m-b-0 confirmation">{{ __('Activate') }}</a>
                    @endif
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="card">
                        <div class="card-block">
                            <div class="table-overflow">
                                <table class="table table-lg table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-shopinn">
                                                        <b>{{ __('Shop') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-shopinn">
                                                        <b>{{ $log->shop->name }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Package') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $log->package->name }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Start') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-success">
                                                        <b>{{  get_date($log->start, 'd M Y')  }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('End') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-success">
                                                        <b>{{  get_date($log->end, 'd M Y')  }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Total Price') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $log->price }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Is Paid') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        @php
                                                            $paid = ($log->paid == 0) ? 'Unpaid' : 'Paid';
                                                        @endphp
                                                        <b>{{ $paid }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Status') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    @if ($log->is_active == 1)
                                                        <b class="text-success">{{ __('Acitve') }}</b>
                                                    @else
                                                        <b class="text-danger">{{ __('Inacitve') }}</b>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
