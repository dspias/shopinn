<div class="modal slide-in-right modal-right fade " id="new_type">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('New Advertise Type') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.advertise.setting.type.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Type Name') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" name="name" class="form-control input-lg @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Type Name') }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group" id="width_height">
                                    <label>{{ __('Image Width * Height') }} <span class="required">*</span></label>
                                    <select name="width_height" id="selectize-dropdown" required>
                                        <option value="" disabled selected>{{ __('Select Width * Height') }}</option>
                                        <option value="1920-650">{{ __('1920px * 650px') }}</option>
                                        <option value="796-485">{{ __('796px * 485px') }}</option>
                                    </select>

                                    @error('width_height')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                {{-- <div class="form-group">
                                    <label>{{ __('Image Width & Height') }} <sup>({{ __('in PX') }})</sup> <span class="required">*</span></label>

                                    <div class="input-group">
                                        <input autofocus autocomplete="off" type="number" min="0" name="width" class="form-control input-lg @error('width') is-invalid @enderror" value="{{ old('width') }}" placeholder="{{ __('Width') }}" required>

                                        <input autofocus autocomplete="off" type="number" min="0" name="height" class="form-control input-lg @error('height') is-invalid @enderror" value="{{ old('height') }}" placeholder="{{ __('Height') }}" required>
                                    </div>

                                    @error('width')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    @error('height')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> --}}

                                <div class="form-group">
                                    <label>{{ __('Image Size') }} <sup>({{ __('in KB') }})</sup> <span class="required">*</span></label>
                                    <div class="input-group">

                                        <input autofocus autocomplete="off" type="number" min="0" name="min_size" class="form-control input-lg @error('min_size') is-invalid @enderror" value="{{ old('min_size') }}" placeholder="{{ __('Image Min Size') }}" required>

                                        <input autofocus autocomplete="off" type="number" min="0" name="max_size" class="form-control input-lg @error('max_size') is-invalid @enderror" value="{{ old('max_size') }}" placeholder="{{ __('Image Max Size') }}" required>
                                    </div>

                                    @error('min_size')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    @error('max_size')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">
                                    <span class="text-uppercase">{{ __('Create Type') }}</span>
                                </button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
