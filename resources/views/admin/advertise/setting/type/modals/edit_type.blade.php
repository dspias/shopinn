<div class="modal slide-in-right modal-right fade " id="edit_type">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Update Advertise Type') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.advertise.setting.type.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" id="type_id">
                                <div class="form-group">
                                    <label>{{ __('Type Name') }} <span class="required">*</span></label>
                                    <input autofocus autocomplete="off" type="text" id="type_name" name="name" class="form-control input-lg @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Type Name') }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group" id="width_height">
                                    <label>{{ __('Image Width * Height') }} <span class="required">*</span></label>
                                    <select name="width_height" id="selectize-dropdown" required>
                                        <option value="" disabled>{{ __('Select Width * Height') }}</option>
                                        <option value="1920-650">{{ __('1920px * 650px') }}</option>
                                        <option value="796-485">{{ __('796px * 485px') }}</option>
                                    </select>

                                    @error('width_height')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('Image Size') }} <sup>({{ __('in KB') }})</sup> <span class="required">*</span></label>
                                    <div class="input-group">

                                        <input autofocus autocomplete="off" type="number" min="0" id="type_min_size" name="min_size" class="form-control input-lg @error('min_size') is-invalid @enderror" value="{{ old('min_size') }}" placeholder="{{ __('Image Min Size') }}" required>

                                        <input autofocus autocomplete="off" type="number" min="0" id="type_max_size" name="max_size" class="form-control input-lg @error('max_size') is-invalid @enderror" value="{{ old('max_size') }}" placeholder="{{ __('Image Max Size') }}" required>
                                    </div>

                                    @error('min_size')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    @error('max_size')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <button class="btn btn-primary btn-sm btn-block text-bold" type="submit">
                                    <span class="text-uppercase">{{ __('Update Type') }}</span>
                                </button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
