@extends('layouts.admin.app')

@section('page_title', 'Advertises | Settings | Advertise Types')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }
    .row-click{
        cursor: pointer;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Advertise Types') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">{{ __('Advertise Types') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">{{ __('All Advertise Types') }}</h4>

                    <a href="javascript:void(0);" data-toggle="modal" data-target="#new_type" class="btn btn-primary btn-sm float-right text-bold m-b-0">{{ __('Add New Type') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Type Name') }}</th>
                                    <th>{{ __('Width') }}</th>
                                    <th>{{ __('Height') }}</th>
                                    <th>{{ __('Min File Size') }}</th>
                                    <th>{{ __('Max File Size') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($types as $sl => $type)
                                <tr>
                                {{--  <tr class="row-click" data-link="javascript:void(0);" target="_self" data-toggle="modal" data-target="#edit_type" data-todo="{{ $type }}" role="row">  --}}
                                    <td class="text-center">
                                        <div class="mrg-top-5">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-5">
                                            <span>
                                                <b>{{ $type->name }}
                                                    <sup class="text-shopinn">{{ '('.$type->width.' * '.$type->height.')' }}</sup>
                                                </b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-5">
                                            <span>
                                                <b>{{ $type->width }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-5">
                                            <span>
                                                <b>{{ $type->height }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-5">
                                            <span>
                                                <b>{{ $type->min_size }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-5">
                                            <span>
                                                <b>{{ $type->max_size }}</b>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modals --}}
    @include('admin.advertise.setting.type.modals.new_type')
    @include('admin.advertise.setting.type.modals.edit_type')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown, #selectize-dropdown1').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>
    <script>
        // Custom Script Here

        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('target'));
                });
            });
        });
    </script>

    <script>
        // Edit Type
        $('#edit_type').on('show.bs.modal', function (e) {
            // do something...
            var button = $(e.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);
            // console.log(data);
            var option = data.width+'-'+data.height;
            modal.find('.modal-content #type_id').val(data.id);
            modal.find('.modal-content #type_name').val(data.name);
            modal.find('.modal-content #width_height').val(data.width);
            modal.find('.modal-content #type_min_size').val(data.min_size);
            modal.find('.modal-content #type_max_size').val(data.max_size);
          })
    </script>
@endsection
