@extends('layouts.admin.app')

@section('page_title', 'Advertises | Settings | Advertise Packages')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .row-click{
        cursor: pointer;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Advertise Packages') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">{{ __('Advertise Packages') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">{{ __('All Advertise Packages') }}</h4>

                    <a href="{{ route('admin.advertise.setting.package.create') }}" class="btn btn-primary btn-sm float-right text-bold m-b-0">{{ __('Add New Package') }}</a>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('Package Name') }}</th>
                                    <th>{{ __('Price') }} <sub>/ {{ __('Day') }}</sub></th>
                                    <th>{{ __('Total Types') }}</th>
                                    <th>{{ __('Total status') }}</th>
                                    <th>{{ __('Active Advertises') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($packages as $sl => $package)
                                    <tr class="row-click" data-link="{{ route('admin.advertise.setting.package.show', encrypt($package->id)) }}" data-target="_self" role="row">
                                        <td class="text-center">
                                            <div class="mrg-top-5">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    <b>{{ $package->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    <b>{{ $package->price }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    <b>{{ count($package->types) }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    @if ($package->is_active == 1)
                                                        <b class="text-success">{{ __('Active') }}</b>
                                                    @else
                                                        <b class="text-danger">{{ __('Inactive') }}</b>
                                                    @endif
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    <b>{{ $package->logs->count() }}</b>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });
    </script>
@endsection
