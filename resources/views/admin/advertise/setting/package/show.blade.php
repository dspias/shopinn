@extends('layouts.admin.app')

@section('page_title', 'Advertises | Settings | Advertise Packages | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .row-click{
        cursor: pointer;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Advertise Packages') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.setting.package.all') }}">{{ __('Advertise Packages') }}</a>
</li>
<li class="breadcrumb-item">{{ __($package->name) }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">
                        {{ __($package->name) }}
                        <sup class="text-dark">
                            (<span class="text-bold" style="font-size: 20px;">৳</span>{{ $package->price }} / {{ __('Day') }})
                        </sup>
                        @if ($package->is_active == 1)
                           <sup class="text-success">({{ __('Active') }})</sup>
                        @else
                           <sup class="text-danger">({{ __('Inactive') }})</sup>
                        @endif
                    </h4>

                    @if ($package->logs->count() < 1)
                        <a href="{{ route('admin.advertise.setting.package.edit', encrypt($package->id)) }}" class="btn btn-primary btn-sm float-right text-bold m-b-0">{{ __('Edit Package') }}</a>
                    @else
                        <h4 class="text-bold float-right">{{ __('Active Advertise: ') }}{{ $package->logs->count() }}</h4>
                    @endif
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="card">
                        <div class="card-block">
                            {!! $package->details !!}
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-block">
                            <div class="table-overflow">
                                <table id="dt-opt" class="table table-lg table-hover table-striped table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{ __('Sl.') }}</th>
                                            <th class="text-center">{{ __('Advertise Type') }}</th>
                                            <th class="text-center">{{ __('Advertises Limit') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($package->types as $sl => $type)
                                            <tr>
                                                <td class="text-center">
                                                    <div class="mrg-top-5">
                                                        <span class="text-dark">
                                                            <b>{{ $sl+1 }}</b>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="mrg-top-5">
                                                        <span>
                                                            <b>{{ $type->name }}</b>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="mrg-top-5">
                                                        <span>
                                                            <b>{{ $type->pivot->max_ad }}</b>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });
    </script>
@endsection
