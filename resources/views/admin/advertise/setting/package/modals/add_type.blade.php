<div class="modal slide-in-right modal-right fade " id="add_type">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Add Advertise Type') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="#" method="post">
                                @csrf
                                <div class="form-group" id="select_type_id_value">
                                    <label>{{ __('Select Type') }} <span class="required">*</span></label>
                                    <select name="type_id" id="selectize-dropdown" required>
                                        <option value="" disabled selected>{{ __('Select Type') }}</option>
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group" id="get_type_max_ad_value">
                                    <label>{{ __('Maximum Ad') }} <span class="required">*</span></label>

                                    <input autofocus autocomplete="off" type="number" min="0" name="max_ad" class="form-control input-lg @error('max_ad') is-invalid @enderror" value="{{ old('max_ad') }}" placeholder="{{ __('Maximum Ad') }}" required>

                                    @error('max_ad')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <a href="javascript:void(0);" id="addNewType" class="btn btn-primary btn-sm btn-block text-bold" data-dismiss="modal">
                                    <span class="text-uppercase">{{ __('Add Type') }}</span>
                                </a>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
