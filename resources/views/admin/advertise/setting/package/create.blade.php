@extends('layouts.admin.app')

@section('page_title', 'Advertises | Settings | Advertise Packages | Create Package')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }


    .note-fontname button{
        min-width: 200px;
    }

    .note-para .note-btn-group{
        display: none;
    }

    .note-insert button:nth-child(2),
    .note-insert button:nth-child(3){
        display: none;
    }
    .note-placeholder {
        color: #d6d6d6;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Advertise Packages') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">{{ __('Settings') }}</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.setting.package.all') }}">{{ __('Advertise Packages') }}</a>
</li>
<li class="breadcrumb-item">{{ __('Create Package') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin.advertise.setting.package.store') }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-heading">
                        <div class="row">
                            <div class="col-xl-6 col-md-6 col-8">
                                <div class="form-group m-b-0">
                                    <input autocomplete="off" type="text" name="name" class="form-control input-lg @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Package Name') }}*" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-4 col-4">
                                <div class="form-group m-b-0">
                                    <input autocomplete="off" type="number" min="0" name="price" class="form-control input-lg @error('price') is-invalid @enderror" value="{{ old('price') }}" placeholder="{{ __('Package Price') }}*" required>

                                    @error('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-2">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#add_type" class="btn btn-primary text-bold m-b-0 m-t-5 btn-block">{{ __('Add Type') }}</a>
                            </div>
                        </div>
                    </div>
                    <hr class="m-t-0">
                    <div class="card-block p-20 p-t-0">
                        <div class="table-overflow">
                            <table id="dt-opt" class="table table-lg table-hover table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Maximum Ad') }}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="type_table_body" class="type-table-body">
                                    {{-- <tr id="type_table_row" class="type-table-row">
                                        <td>
                                            <div class="mrg-top-5">
                                                <input autocomplete="off" type="text" min="0" name="type_id" class="form-control @error('type_id') is-invalid @enderror" value="{{ __('Type_Name_Here') }}" readonly required>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <input autocomplete="off" type="number" min="0" name="max_ad" class="form-control @error('max_ad') is-invalid @enderror" value="10" readonly required>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-5">
                                                <span>
                                                    <a href="#" class="btn btn-danger btn-sm confirmation" data-toggle="tooltip" title="{{ __('Delete?') }}"><i class="fa fa-trash"></i></a>
                                                </span>
                                            </div>
                                        </td>
                                    </tr> --}}
                                </tbody>
                            </table>

                            <hr class="m-t-0">

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        {{-- <label>{{ __('Package Details') }} <span class="required">*</span></label> --}}
                                        <textarea autocomplete="off" name="details" class="form-control summernote @error('details') is-invalid @enderror" placeholder="{{ __('Package Details') }}" rows="5" required>{{ old('details') }}</textarea>

                                        @error('details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active" class="m-b-0 text-bold text-shopinn">{{ __('Active Package') }}</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button class="btn btn-primary float-right" type="submit">{{ __('Create Package') }}</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Modals --}}
    @include('admin.advertise.setting.package.modals.add_type')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}

    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>

    <script>
        var summernote = $('.summernote');
        var placeholder = summernote.attr('placeholder');
        summernote.summernote({
            height: 200,
            placeholder: placeholder
        });
    </script>

    <script>

        function deleteMe(that){
            var target = $(that).parent().parent().parent().parent();
            target.remove();
        }

        $(document).ready(function(){
            var tableBody = $('#type_table_body');

            var type = $('#select_type_id_value select');
            var maxAd = $('#get_type_max_ad_value input');

            $('#addNewType').click(function(){
                var tableRow = makeString();
                tableBody.append(tableRow);
            });

            function makeString(){
                var tableRow =
                `<tr">
                    <td>
                        <div class="mrg-top-5">
                            <input autocomplete="off" type="hidden" min="0" name="type[]" class="form-control" value="`+type.val()+`" readonly required>
                            <input autocomplete="off" type="text" class="form-control" value="`+type.text()+`" readonly required>
                        </div>
                    </td>
                    <td>
                        <div class="mrg-top-5">
                            <input autocomplete="off" type="number" min="0" name="max_ad[]" class="form-control" value="`+maxAd.val()+`" readonly required>
                        </div>
                    </td>
                    <td>
                        <div class="mrg-top-5">
                            <span>
                                <a href="#" class="btn btn-primary btn-sm remove-type" onclick="deleteMe(this);"><i class="fa fa-trash"></i></a>
                            </span>
                        </div>
                    </td>
                </tr>`;
                return tableRow;
            }
        });
    </script>
@endsection
