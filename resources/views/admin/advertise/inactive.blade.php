@extends('layouts.admin.app')

@section('page_title', 'Advertises | Inactive Advertises')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Inactive Advertises
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Advertises</li>
<li class="breadcrumb-item">Inactive Advertises</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{--  <div class="card-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card m-b-0">
                                <div class="card-block p-15">
                                    <h3 class="no-mrg-vertical text-center font-size-35">
                                        <sup class="text-bold text-danger">10</sup> 
                                        / 
                                        <sub class="text-bold">100</sub>
                                    </h3>
                                    <div class="mrg-top-10">
                                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Inactive Advertises</span>
                                        <span class="pull-right pdd-right-10 font-size-13">10%</span>
                                        <div class="progress progress-danger">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="m-t-0">  --}}
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('SL.') }}</th>
                                    <th class="text-center">{{ __('Shop') }}</th>
                                    <th class="text-center">{{ __('Advertise Title') }}</th>
                                    <th class="text-center">{{ __('Advertise Type') }}</th>
                                    <th class="text-center">{{ __('Start Date') }}</th>
                                    <th class="text-center">{{ __('End Date') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ads as $sl => $ad)
                                    <tr class="row-click" data-link="#" data-target="_self">
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $ad->ad_log->shop->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $ad->title }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $ad->type->name }}</b>
                                                </span>
                                            </div>
                                        </td>

                                        {{-- @php
                                            $dateTime = new DateTime($order->created_at);
                                        @endphp --}}

                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-success text-bold">{{ get_date($ad->start, 'd-m-Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-shopinn text-bold">{{ get_date($ad->end, 'd-m-Y') }}</span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                @if ($ad->is_active == 1)
                                                    <b class="text-success">{{ __('Acitve') }}</b>
                                                @else
                                                    <b class="text-danger">{{ __('Inacitve') }}</b>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-10 dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('admin.advertise.view', ['ad_id' => encrypt($ad->id)]) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
