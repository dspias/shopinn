@extends('layouts.admin.app')

@section('page_title', 'Advertises | Admin Advertises | Edit')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <!-- font -->
    <link href="{{ asset('fileuploader/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
    <!-- css -->
    <link href="{{ asset('fileuploader/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        /* FileUploader  */
        .fileuploader-input .fileuploader-input-caption{
            color: #ff084e;
        }
        .fileuploader-input .fileuploader-input-button:active,
        .fileuploader-input .fileuploader-input-button:hover,
        .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:active,
        .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:hover{
            box-shadow: none;
            transform: none;
        }
        .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
            background: #ff084e !important;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Admin Advertises') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.index') }}">{{ __('Admin Advertises') }}</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.show', encrypt($advertise->id)) }}">{{ __($advertise->title) }}</a>
</li>
<li class="breadcrumb-item">{{ __('Edit Advertise') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 mx-auto">
            <form action="{{ route('admin.advertise.update', ['ad_id' => encrypt($advertise->id)]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="select_type_id_value">
                                    {{-- <label>{{ __('Select Type') }} <span class="required">*</span></label> --}}
                                    <select name="type_id" id="selectize-dropdown" required>
                                        <option value="" disabled>{{ __('Select Type') }} *</option>
                                        @foreach($types as $type)
                                            <option value="{{ $type->id }}" @if($type->id == $advertise->type->id) selected @endif>{{ $type->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-block p-20 p-t-0">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group m-b-20">
                                    <label for="title">{{ __('Ad Title') }} <span class="required text-shopinn">*</span></label>
                                    <input autocomplete="off" type="text" name="title" class="form-control input-lg @error('title') is-invalid @enderror" value="{{ $advertise->title }}" placeholder="{{ __('Advertise Title') }}*" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group m-b-20">
                                    <label for="url_link">{{ __('Advertise URL') }} <span class="required text-shopinn">*</span></label>
                                    <input autocomplete="off" type="url" name="url_link" class="form-control input-lg @error('url_link') is-invalid @enderror" value="{{ $advertise->url_link }}" placeholder="{{ __('Advertise Link') }}*" required>

                                    @error('url_link')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group m-b-20">
                                    <label for="day">{{ __('Days') }} <span class="required text-shopinn">*</span></label>
                                    <input autocomplete="off" type="number" min="0" max="30" name="day" class="form-control input-lg @error('day') is-invalid @enderror" value="{{ $advertise->day }}" placeholder="{{ __('Advertise Days') }}*" required>

                                    @error('day')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <small class="text-bold">
                                    {{ __('Please Upload Following Size Image:') }}
                                    <span class="text-shopinn">{{ __('Width:') }} <span id="imgWidth">{{ $advertise->type->width }}</span>PX</span> |
                                    <span class="text-shopinn">{{ __('Height:') }} <span id="imgHeight">{{ $advertise->type->height }}</span>PX</span> |
                                    <span class="text-shopinn">{{ __('Min Size:') }} <span id="minSize">{{ $advertise->type->min_size }}</span>KB</span> |
                                    <span class="text-shopinn">{{ __('Max Size:') }} <span id="maxSize">{{ $advertise->type->max_size }}</span>KB</span>
                                </small>

                                <input type="file" name="ad_image" class="files @error('ad_image') is-invalid @enderror" accept="image/*">

                                @error('ad_image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-4 mx-auto">
                                <figure class="text-center">
                                    <img src="{{ url(env('IMG_STORE').$advertise->ad_image) }}" alt="{{ __('Advertise Image Not Found') }}" class="img-thumbnail img-responsive">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="is_active" name="is_active" type="checkbox" @if($advertise->is_active == 1) checked @endif>
                                    <label for="is_active" class="m-b-0 text-bold text-shopinn">{{ __('Active Advertise') }}</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button class="btn btn-primary float-right text-bold" type="submit">{{ __('Update Advertise') }}</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>

    <script src="{{ asset('fileuploader/jquery.fileuploader.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            // enable fileuploader plugin
            $('input[name="ad_image"]').fileuploader({
                limit: 1,
                // maxSize: 1,

                extensions: ["image/*"],
                addMore: true,
            });


            $('.fileuploader-input-caption span').html('Choose or Drag & Drop Your Advertise Image to Upload');
        });
    </script>

    <script>
        // type wise input change
        var types = @php echo json_encode($types); @endphp

        $('#selectize-dropdown').on('change', function(){
            var id = $(this).val();
            var type = null;
            types.forEach(function(temp){
                if(temp.id == id) type = temp;
            });

            let input = $('#input_id');

            $('#imgWidth').html(type.width);
            $('#imgHeight').html(type.height);
            $('#minSize').html(type.min_size);
            $('#maxSize').html(type.max_size);
        });
    </script>
@endsection
