@extends('layouts.admin.app')

@section('page_title', 'Shop Package Logs | All Shop Package Logs')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Shop Package Logs') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Advertises</li>
<li class="breadcrumb-item">{{ __('Shop Package Logs') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{--  <div class="card-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card m-b-0">
                                <div class="card-block p-15">
                                    <h3 class="no-mrg-vertical text-center font-size-35">
                                        <sup class="text-bold text-success">50</sup> 
                                        / 
                                        <sub class="text-bold">100</sub>
                                    </h3>
                                    <div class="mrg-top-10">
                                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Active Shop Package Logs</span>
                                        <span class="pull-right pdd-right-10 font-size-13">50%</span>
                                        <div class="progress progress-success">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card m-b-0">
                                <div class="card-block p-15">
                                    <h3 class="no-mrg-vertical text-center font-size-35">
                                        <sup class="text-bold text-danger">50</sup> 
                                        / 
                                        <sub class="text-bold">100</sub>
                                    </h3>
                                    <div class="mrg-top-10">
                                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Inactive Shop Package Logs</span>
                                        <span class="pull-right pdd-right-10 font-size-13">50%</span>
                                        <div class="progress progress-danger">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  --}}
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('SL.') }}</th>
                                    <th class="text-center">{{ __('Shop') }}</th>
                                    <th class="text-center">{{ __('Package') }}</th>
                                    <th class="text-center">{{ __('Start') }}</th>
                                    <th class="text-center">{{ __('End') }}</th>
                                    <th class="text-center">{{ __('Price') }}</th>
                                    <th class="text-center">{{ __('Is Paid') }}</th>
                                    <th class="text-center">{{ __('Status') }}</th>
                                    <th class="text-center">{{ __('Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($logs as $sl => $log)
                                    <tr class="row-click" data-link="#" data-target="_self">
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $sl+1 }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                <span class="text-dark">
                                                    <b>{{ $log->shop->name }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $log->package->name }}</b>
                                                </span>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-success text-bold">{{ get_date($log->start, 'd M Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span class="text-shopinn text-bold">{{ get_date($log->end, 'd M Y') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                <span>
                                                    <b>{{ $log->price }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="mrg-top-15">
                                                @php
                                                    $paid = ($log->paid == 0) ? 'Unpaid' : 'Paid';
                                                @endphp
                                                <span>
                                                    <b>{{ $paid }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-15">
                                                @if ($log->is_active == 1)
                                                    <b class="text-success">{{ __('Acitve') }}</b>
                                                @else
                                                    <b class="text-danger">{{ __('Inacitve') }}</b>
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mrg-top-10 dropdown">

                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('admin.advertise.shop.ad.pack.show', ['pack_id' => encrypt($log->id)]) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>{{ __('Details') }}</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
