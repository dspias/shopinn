@extends('layouts.admin.app')

@section('page_title', 'Advertises | Admin Advertises | Details')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('All Advertises') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Advertises') }}</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.advertise.all') }}">{{ __('All Advertises') }}</a>
</li>
<li class="breadcrumb-item">{{ __($advertise->title) }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold text-shopinn m-t-4">
                        {{ __($advertise->title) }}
                        @if ($advertise->is_active == 1)
                           <sup class="text-success">({{ __('Active') }})</sup>
                        @else
                           <sup class="text-danger">({{ __('Inactive') }})</sup>
                        @endif
                    </h4>
                    @if($advertise->is_active == 1)
                    <a href="{{ route('admin.advertise.change_status', encrypt($advertise->id)) }}" class="btn btn-primary btn-sm float-right text-bold m-b-0 confirmation">{{ __('Deactivate') }}</a>
                    @else
                    <a href="{{ route('admin.advertise.change_status', encrypt($advertise->id)) }}" class="btn btn-primary btn-sm float-right text-bold m-b-0 confirmation">{{ __('Activate') }}</a>
                    @endif
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="card">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-12">
                                    <figure class="text-center">
                                        <img src="{{ url(env('IMG_STORE').$advertise->ad_image) }}" alt="{{ __('Advertise Image Not Found') }}" class="img-thumbnail img-responsive">
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-block">
                            <div class="table-overflow">
                                <table class="table table-lg table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-shopinn">
                                                        <b>{{ __('Shop') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-shopinn">
                                                        <b>{{ $advertise->ad_log->shop->name }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise type') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $advertise->type->name }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise Image Dimension') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $advertise->type->width . '*' . $advertise->type->height }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise Image Min Size') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $advertise->type->min_size }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise Image Max Size') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ $advertise->type->max_size }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise URL') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <a href="{{ $advertise->url_link }}" class="btn btn-primary btn-xs text-bold" target="_blank">
                                                            {{ __('Go To Link') }}
                                                        </a>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise Start Date') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ get_date($advertise->start, 'd-m-Y') }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ __('Advertise End Date') }}</b>
                                                    </span>
                                                </div>
                                            </th>
                                            <td class="text-center">
                                                <div class="mrg-top-5">
                                                    <span class="text-dark">
                                                        <b>{{ get_date($advertise->end, 'd-m-Y') }}</b>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
