@extends('layouts.admin.app')

@section('page_title', 'Orders | Pending Orders')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Pending Orders
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item">Pending Orders</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card m-b-0 border">
                                <div class="card-block p-15">
                                    <h3 class="no-mrg-vertical text-center font-size-35">
                                        <sup class="text-bold text-info">{{ $pending }}</sup> 
                                        / 
                                        <sub class="text-bold">{{ $all }}</sub>
                                    </h3>
                                    <div class="mrg-top-10">
                                        <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Orders') }}</span>
                                        <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%</span>
                                        <div class="progress progress-info">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Order ID') }}</th>
                                    <th class="text-center">{{ __('Quantity') }}</th>
                                    <th class="text-center">
                                        {{ __('Total Amount') }}<sup class="text-shopinn">{{ __('(In TK)') }}</sup>
                                    </th>
                                    <th class="text-center">{{ __('Order Date') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $sl => $order)
                                    <tr>
                                        <td class="text-center">
                                            <div class="m-t-0">
                                                <span class="text-dark">
                                                    <b class="text-shopinn">{{ $order->oid }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        @php
                                            $cost = 0;
                                            $quantity = 0;
                                            foreach($order->items as $item){
                                                $quantity += $item->quantity;
                                                $cost += ($item->quantity*$item->price);
                                            }
                                            if(optional($order->coupon)->discount != null) $cost -= optional($order->coupon)->discount;
                                        @endphp
                                        <td class="text-center">
                                            <div class="m-t-0">
                                                <span>
                                                    <b>{{ $quantity }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-0">
                                                <span>
                                                    <b>{{ ($cost+$order->delivery) }}<sup class="text-shopinn">{{ __('TK') }}</sup></b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="m-t-0">
                                                <span>
                                                    <b>{{ get_date($order->created_at, 'd M Y ( h:i A )') }}</b>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="{{ route('admin.order.show', ['order_type' => 'Pending Orders' ,'order_id' => encrypt($order->id)]) }}">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>See Details</span>
                                                        </a>
                                                    </li>

                                                    {{-- <li>
                                                        <a href="#">
                                                            <i class="ti-check pdd-right-10 text-success"></i>
                                                            <span>Mark As Completed</span>
                                                        </a>
                                                    </li> --}}

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
