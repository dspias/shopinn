@extends('layouts.admin.app')

@section('page_title', 'Order | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .card.disabled{
        background-color: #dfdfdf6b;
    }
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Order Details
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="{{ route($route) }}"> {{ $order_type }} </a></li>
<li class="breadcrumb-item">{{ $order->oid }}</li>
<li class="breadcrumb-item">Details</li>
@endsection



@section('main_content')
{{-- Main Contents Here --}}
@php
    function itemsConfirmed($items){
        foreach($items as $item){
            if($item->status == 0) return false;
        } return true;
    }
@endphp
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        @if(itemsConfirmed($order->items) && $order->status != 2)
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="ti-more"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    @if ($order->status == 0)
                                        <li>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#add_rider">
                                                <i class="ti-check text-success pdd-right-10"></i>
                                                <span>{{ __('Confirm Order') }}</span>
                                            </a>
                                        </li>
                                        {{-- <li>
                                            <a class="confirmation" href="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => 1]) }}">
                                                <i class="ti-check text-success pdd-right-10"></i>
                                                <span>{{ __('Confirm Order') }}</span>
                                            </a>
                                        </li> --}}
                                        <li>
                                            <a class="confirmation" href="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => -1]) }}">
                                                <i class="ti-close text-danger pdd-right-10"></i>
                                                <span>{{ __('Cancel Order') }}</span>
                                            </a>
                                        </li>
                                    @elseif($order->status == -1 && $order->status != 0 && $order->status != 2)
                                        <li>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#add_rider">
                                                <i class="ti-check text-success pdd-right-10"></i>
                                                <span>{{ __('Confirm Order') }}</span>
                                            </a>
                                        </li>
                                        {{-- <li>
                                            <a class="confirmation" href="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => 1]) }}">
                                                <i class="ti-check text-success pdd-right-10"></i>
                                                <span>{{ __('Confirm Order') }}</span>
                                            </a>
                                        </li> --}}
                                    @elseif($order->status == 1)
                                        <li>
                                            <a class="confirmation" href="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => 2]) }}">
                                                <i class="ti-pin text-success pdd-right-10"></i>
                                                <span>{{ __('Complete Order') }}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="confirmation" href="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => -1]) }}">
                                                <i class="ti-close text-danger pdd-right-10"></i>
                                                <span>{{ __('Cancel Order') }}</span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" data-toggle="tooltip" data-original-title="{{ __('Back To Previous Page') }}">
                                <i class="ti-arrow-left"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                @php
                    if($order->status == 0) $status = 'Pending';
                    if($order->status == 1) $status = 'Running';
                    if($order->status == 2) $status = 'Completed';
                    if($order->status == -1) $status = 'Canceled';
                @endphp
                <div class="card-heading">
                    <h4 class="card-title">
                        {{ __('Order ID: ') }}
                        <span class="text-shopinn text-bold">{{ $order->oid }}</span>
                        <sup class="text-muted">({{ $status }})</sup>
                        @if ($order->status == -1)
                            <sup class="text-muted">{{ __('(Order Canceled By ShopInn)') }}</sup>
                        @endif
                    </h4>
                </div>
                <hr class="m-t-0">
                @php

                    function itemActive($shop_id, $shop){
                        $flag = 1;
                        if($shop[0]->shop->id != $shop_id)
                            return $flag = 0;
                        //dd($flag);
                        return $flag;
                    }

                    $grand_price = 0;
                    $discount = 0;
                @endphp
                <div class="card-body">
                    @foreach($shops as $shop)
                        @php $flag = ($shop[0]->status == -1) ? 1 : 0; @endphp
                        <div class="card @if($flag)disabled @endif">
                            <div class="card-heading text-center">
                                <h4 class="card-title text-shopinn text-bold">
                                    {{ $shop[0]->shop->name }}
                                    @if ($shop[0]->status == 1)
                                        <i class="ti-check text-success" data-toggle="tooltip" data-title="Order Accepted from {{ $shop[0]->shop->name }}"></i>
                                    @endif
                                </h4>
                            </div>
                            <hr class="m-t-0">
                            <div class="card-body">
                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{ __('Sl.') }}</th>
                                            <th class="text-center">{{ __('Product') }}</th>
                                            <th class="text-center">{{ __('Quantity') }}</th>
                                            <th class="text-center">{{ __('Total Price') }}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @php
                                            $shopTotal = 0;
                                            $products = 0
                                        @endphp
                                        @foreach ($shop as $sl => $item)
                                            <tr>
                                                <td class="text-center">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($sl+1) }}</span>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">
                                                            <a href="{{ route('guest.product.show', ['product_id'=> $item->product->id, 'product_name' => get_name($item->product->name)]) }}" target="_blank" class="text-shopinn text-bold">{{ $item->product->name }}</a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($item->quantity) }}</span>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    @php
                                                        $price = $item->quantity * $item->price;
                                                        $shopTotal +=$price;
                                                        $products++;
                                                    @endphp
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($price) }} <sup class="text-shopinn">@if($item->discount > 0)<del>{{ $item->discount }}%</del>@endif</sup></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            @php
                                $shop_dis = 0;
                                $grand_shop = 0;
                                if(optional($order->coupon)->discount != null && $discount == 0 && itemActive($order->coupon->shop_id, $shop)){
                                    $shop_dis = optional($order->coupon)->discount;
                                }
                                if(!$flag){
                                    $grand_price += $shopTotal;
                                    $discount = $shop_dis;
                                }
                                $grand_shop = $shopTotal - $shop_dis;
                            @endphp

                            <div class="card-footer">
                                <table class="table table-lg table-hover table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="text-left">{{ __('Total Product') }}</th>
                                            <td class="text-right">{{ __($products) }}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">{{ __('Total Price') }}</th>
                                            <td class="text-right">{{ __($shopTotal) }} <sup class="text-shopinn text-bold">TK</sup></td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">{{ __('Coupon') }}</th>
                                            <td class="text-right">{{ __($shop_dis) }}  <sup>( {{ optional($order->coupon)->code }} )</sup></td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">{{ __('Grand Total') }}</th>
                                            <td class="text-right">{{ __($grand_shop) }} <sup class="text-shopinn text-bold">TK</sup></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                    @endforeach


                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered table-responsive-sm">
                                <tbody>
                                    @php
                                        $complete = get_date($order->status_at, 'd M Y');
                                        if($order->status == -1){
                                            $alert = 'danger';
                                            $status = 'Canceled';
                                        } else if($order->status == 0){
                                            $alert = 'info';
                                            $status = 'Pending';
                                        } else if($order->status == 1){
                                            $alert = 'warning';
                                            $status = 'Running';
                                        }else {
                                            $alert = 'success';
                                            $status = 'Completed';
                                        }
                                    @endphp
                                    <tr>
                                        <th colspan="3" class="text-center text-bold">{{ __('Order Date') }}</th>
                                        <td class="text-center text-bold">{{ get_date($order->created_at, 'd M Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center text-bold">{{ __('Order Completed Date') }}</th>
                                        <td class="text-center text-bold">{{ $complete }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center text-bold">{{ __('Status') }}</th>
                                        <td class="text-center text-bold text-{{ $alert }}">{{ $status }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center text-bold">{{ __('Rider') }}</th>
                                        <td class="text-center text-bold text-shopinn">
                                            @if ($order->rider_id != null)
                                                <a href="{{ route('admin.rider.details', ['page' => 'all', 'rider_id' => encrypt($order->rider->id)]) }}" class="text-shopinn" target="_blank">{{ $order->rider->name }}</a>
                                            @else
                                                {{ __('Not Assigned') }}
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <table class="table table-lg table-hover table-bordered table-responsive-sm">
                                <tbody>
                                    <tr>
                                        <th colspan="3" class="text-left text-bold">{{ __('Total Price') }}</th>
                                        <td class="text-right text-bold">{{ $grand_price }} <sup class="text-shopinn">{{ __('TK') }}</sup></td>
                                    </tr>
                                    @php
                                        $grand_price -= $discount;
                                    @endphp
                                    <tr>
                                        <th colspan="3" class="text-left text-bold">{{ __('Total Discount') }}</th>
                                        <td class="text-right text-bold">(-){{ $discount }} <sup class="text-shopinn">TK</sup></td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-left text-bold">{{ __('Delivery Charge') }}</th>
                                        <td class="text-right text-bold">(+){{ $order->delivery }} <sup class="text-shopinn">TK</sup></td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-left text-bold">{{ __('Grand Total') }}</th>
                                        <td class="text-right text-bold">{{ ($grand_price+$order->delivery) }}
                                            <sup class="text-shopinn">{{ __('TK') }}</sup>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="portlet">
                                    <ul class="portlet-item navbar">
                                        @if($order->is_reseller == true)
                                            <li>
                                                <a href="{{ route('admin.reseller.details', ['page' => 'all', 'reseller_id' => encrypt($order->customer_id)]) }}" target="_blank" class="btn btn-link text-shopinn text-bold" data-toggle="tooltip" data-original-title="{{ __('This Order Was Placed By: ') . $order->customer->name }}">
                                                    {{ $order->customer->name }}
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="card-heading">
                                    <h4 class="card-title text-shopinn text-bold">
                                        {{ __('Customer Details') }}
                                    </h4>
                                </div>
                                <hr class="m-t-0">
                                <div class="card-body">
                                    <table class="table table-lg table-hover table-bordered">
                                        <tbody>
                                            <tr>
                                                <th class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __('Name') }}</span>
                                                    </div>
                                                </th>
                                                <td class="text-left">
                                                    <div class="m-t-0">
                                                        @php
                                                            $name = (is_null($order->name)) ? $order->customer->name : $order->name;
                                                        @endphp
                                                        <span class="text-dark">{{ __($name) }}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __('Mobile') }}</span>
                                                    </div>
                                                </th>
                                                <td class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($order->mobile) }}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __('Email') }}</span>
                                                    </div>
                                                </th>
                                                <td class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($order->email) }}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __('City') }}</span>
                                                    </div>
                                                </th>
                                                <td class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($order->city) }}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __('Full Address') }}</span>
                                                    </div>
                                                </th>
                                                <td class="text-left">
                                                    <div class="m-t-0">
                                                        <span class="text-dark">{{ __($order->address) }}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modals --}}
    @include('admin.order.modals.add_rider')
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>
@endsection
