<div class="modal slide-in-right modal-right fade " id="add_rider">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4 class="text-bold text-shopinn">{{ __('Assign Rider') }}</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.order.status', ['order_id'=> $order->id, 'value' => 1]) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>{{ __('Select Rider') }} <span class="required">*</span></label>
                                    <select name="rider_id" id="selectize-dropdown" required>
                                        <option value="" disabled selected>{{ __('Select Rider') }}</option>
                                        @foreach($riders as $rider)
                                            <option value="{{ $rider->id }}">{{ $rider->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('rider_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Rider Charge') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" step="1" name="rider_charge" class="form-control @error('rider_charge') is-invalid @enderror" value="{{ old('rider_charge')??40 }}" placeholder="{{ __('Rider Charge') }}" required>

                                    @error('rider_charge')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Extra Cost') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" step=".1" name="extra_cost" class="form-control @error('extra_cost') is-invalid @enderror" value="{{ old('extra_cost')??00.00 }}" placeholder="{{ __('Ex. Courier cost / ...') }}" required>

                                    @error('extra_cost')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-primary btn-sm btn-block text-bold">
                                    <span class="text-uppercase">{{ __('Assign & Confirm Order') }}</span>
                                </button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
