@extends('layouts.admin.app')

@section('page_title', 'Artisan Commands')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .card{
        border: 1px solid #ff084e;
        transition: 0.3s all ease-in-out !important;
    }
    .card:hover{
        background-color: #ff084e;
        transition: 0.3s all ease-in-out !important;
    }
    .card .text-shopinn{
        transition: 0.3s all ease-in-out !important;
    }
    .card:hover .text-shopinn{
        color: #ffffff !important;
        transition: 0.3s all ease-in-out !important;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Artisan Commands
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Artisan Commands</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        {{-- View Cache --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'view:cache']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('View Cache') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('View Cache') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End View Cache --}}
        
        {{-- View Clear --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'view:clear']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('View Clear') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('View Clear') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End View Clear --}}
        
        {{-- Config Cache --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'config:cache']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Config Cache') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Config Cache') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End View Clear --}}
        
        {{-- Config Clear --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'config:clear']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Config Clear') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Config Clear') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Config Clear --}}
        
        {{-- Route Clear --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'route:clear']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Route Clear') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Route Clear') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Route Clear --}}
        
        {{-- Migrate --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'migrate']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Migrate') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Migrate') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Migrate --}}
        
        {{-- Migrate Seed --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'migrate --seed']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Migrate Seed') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Migrate Seed') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Migrate Seed --}}
        
        {{-- Migrate Fresh --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'migrate:fresh']) }}" class="col-md-3 confirmation">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Migrate Fresh') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Migrate Fresh') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Migrate --}}
        
        {{-- Storage Link --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'migrate:fresh --seed']) }}" class="col-md-3 confirmation">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Migrate Fresh Seed') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Migrate Fresh Seed') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Migrate Fresh Seed --}}
        
        {{-- Storage Link --}}
        <a href="{{ route('admin.artisan.command', ['command' => 'storage:link']) }}" class="col-md-3">
            <div class="card">
                <div class="card-block">
                    {{-- <p class="mrg-btm-5 text-center text-bold text-dark">{{ __('Storage Link') }}</p> --}}
                    <h5 class="no-mrg-vertical text-center text-bold text-shopinn">
                        {{ __('Storage Link') }}
                    </h5>
                </div>
            </div>
        </a>
        {{-- End Storage Link --}}
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
