@extends('layouts.admin.app')

@section('page_title', 'Customer | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Details Customer

@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Customers</li>
<li class="breadcrumb-item">{{ $page }}</li>
<li class="breadcrumb-item">Details</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu">
                                {{--  <li>
                                    <a href="#">
                                        <i class="ti-pencil text-info pdd-right-10"></i>
                                        <span>Edit Customer</span>
                                    </a>
                                </li>  --}}
                                @if($customer->is_active == 1)
                                <li>
                                    <a href="{{ route('admin.customer.change_status', ['customer_id' => encrypt($customer->id)]) }}" class="confirmation">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>{{ __('Deactivate') }}</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('admin.customer.change_status', ['customer_id' => encrypt($customer->id)]) }}" class="confirmation">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>{{ __('Activate') }}</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="profile border bottom">
                    @php
                        $logo = get_logo(auth()->user(), 'thumb');
                        if($logo == null) $logo = asset('vendor/images/resources/author.jpg');
                    @endphp
                    <img class="mrg-top-30" src="{{ $logo }}" alt="">
                    <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $customer->name }}
                        <sup>
                            @if($customer->approved_at == null)
                            <i class="ti-star text-danger pdd-right-5"></i>({{ __('Not Approved') }})
                            @elseif($customer->is_active == 1)
                            <i class="ti-check text-success"></i>
                            @else
                            <i class="ti-close text-danger"></i>
                            @endif
                        </sup>
                    </h4>
                </div>
                <div class="pdd-horizon-30 pdd-vertical-20">
                    <h5 class="text-bold text-dark">{{ __('Customer Details') }}</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Customer Name') }}</th>
                                        <td>{{ $customer->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Customer Mobile Number') }}</th>
                                        <td>{{ $customer->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Customer Email') }}</th>
                                        <td>{{ $customer->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Customer City') }} ({{ __('District') }})</th>
                                        <td>{{ $customer->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Customer Address') }}</th>
                                        <td>{{ $customer->address  }}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $date = new DateTime($customer->created_at);
                                        @endphp
                                        <th>{{ __('Customer Register Date') }}</th>
                                        <td>{{ $date->format('d M Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
