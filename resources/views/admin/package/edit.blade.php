@extends('layouts.admin.app')

@section('page_title', 'Packages | Update Package')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }

    .responsive-image{
        border-radius: 5px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        height: 80px;
        width: 100%;
    }
    .card-block.task-file.p-0 {
        height: 80px;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }
    a.deactive{
        cursor: pointer;
    }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Update Package
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Packages</li>
<li class="breadcrumb-item">Update Package</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.package.update', ['package_id' => encrypt($package->id)]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('Package Name') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $package->name }}" placeholder="{{ __('Package Name') }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="radio radio-inline radio-shopinn">
                                    <input type="radio" name="package_type" value="1" id="freeType" @if($package->package_type == 1)checked @endif>
                                    <label for="freeType">Free</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="radio radio-inline radio-shopinn">
                                    <input type="radio" name="package_type" value="2" id="commissionType" @if($package->package_type == 2)checked @endif>
                                    <label for="commissionType">Commission Based</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="radio radio-inline radio-shopinn">
                                    <input type="radio" name="package_type" value="3" id="paymentType" @if($package->package_type == 3)checked @endif>
                                    <label for="paymentType">Commission & Payment</label>
                                </div>
                            </div>

                            
                            <div class="col-lg-4 commission-section @if($package->package_type != 2)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Total Percentage') }} <span class="required">*</span></label>
                                    <input autocomplete="off" step="0.1" type="number" min="0" max="100" name="total_percentage" class="form-control @error('total_percentage') is-invalid @enderror" value="{{ $package->total_percentage }}" placeholder="{{ __('Total Percentage') }}" @if($package->package_type != 2)disabled @endif required>

                                    @error('total_percentage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 commission-section @if($package->package_type != 2)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('First Percentage') }}</label>
                                    <input autocomplete="off" type="number" step="0.1" min="0" max="100" name="first_percentage" class="form-control @error('first_percentage') is-invalid @enderror" value="{{ $package->first_percentage }}" placeholder="{{ __('First Percentage') }}" @if($package->package_type != 2)disabled @endif>

                                    @error('first_percentage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-4 commission-section @if($package->package_type != 2)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('First days') }}</label>
                                    <input autocomplete="off" type="number" min="0" name="first_day" class="form-control @error('first_day') is-invalid @enderror" value="{{ $package->first_day }}" placeholder="{{ __('First days') }}" @if($package->package_type != 2)disabled @endif>

                                    @error('first_day')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            
                            <div class="col-lg-3 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Total Percentage') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" step="0.1" min="0" max="100" name="total_percentage" class="form-control @error('total_percentage') is-invalid @enderror" value="{{ $package->total_percentage }}" placeholder="{{ __('Total Percentage') }}" @if($package->package_type != 3)disabled @endif required>

                                    @error('total_percentage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-3 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Total Payment') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" name="total_payment" class="form-control @error('total_payment') is-invalid @enderror" value="{{ $package->total_payment }}" placeholder="{{ __('Total Payment') }}" @if($package->package_type != 3)disabled @endif required>

                                    @error('total_payment')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-2 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Year') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" name="year" class="form-control @error('year') is-invalid @enderror" value="{{ $package->year }}" placeholder="{{ __('Year') }}" @if($package->package_type != 3)disabled @endif required>

                                    @error('year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-2 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Month') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" max="11" name="month" class="form-control @error('month') is-invalid @enderror" value="{{ $package->month }}" placeholder="{{ __('Month') }}" @if($package->package_type != 3)disabled @endif required>

                                    @error('month')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-2 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('Days') }} <span class="required">*</span></label>
                                    <input autocomplete="off" type="number" min="0" max="29" name="day" class="form-control @error('day') is-invalid @enderror" value="{{ $package->day }}" placeholder="{{ __('Days') }}" @if($package->package_type != 3)disabled @endif required>

                                    @error('day')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('First Percentage') }}</label>
                                    <input autocomplete="off" type="number" step="0.1" min="0" max="100" name="first_percentage" class="form-control @error('first_percentage') is-invalid @enderror" value="{{ $package->first_percentage }}" placeholder="{{ __('First Percentage') }}" @if($package->package_type != 3)disabled @endif>

                                    @error('first_percentage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6 payment-section @if($package->package_type != 3)d-none @endif">
                                <div class="form-group">
                                    <label>{{ __('First days') }}</label>
                                    <input autocomplete="off" type="number" min="0" name="first_day" class="form-control @error('first_day') is-invalid @enderror" value="{{ $package->first_day }}" placeholder="{{ __('First days') }}" @if($package->package_type != 3)disabled @endif>

                                    @error('first_day')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('Package Description') }} <span class="required">*</span></label>
                                    {{-- <div id="summernote-usage"></div> --}}
                                    <textarea autocomplete="off" name="description" class="form-control summernote @error('description') is-invalid @enderror" placeholder="{{ __('Package Description') }}" rows="5" required>{{ $package->description }}</textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-left">
                            <li class="list-inline-item">
                                <div class="checkbox checkbox-primary font-size-12">
                                    <input id="is_active" name="is_active" type="checkbox" checked>
                                    <label for="is_active" class="m-b-0 text-bold text-shopinn">Activate This Package?</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Update Package
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        var summernote = $('.summernote');
        summernote.summernote({
            height: 200,
            // placeholder: placeholder
        });

        $(document).ready(function(){
            var commission = $('.commission-section');
            var comInp = $('.commission-section input');

            var payment = $('.payment-section');
            var payInp = $('.payment-section input');
            
            $('#freeType').click(function() {
                if( !$(commission).hasClass("d-none") ) commission.addClass('d-none');
                comInp.attr('disabled', 'disabled');

                if( !$(payment).hasClass("d-none") ) payment.addClass('d-none');
                payInp.attr('disabled', 'disabled');
            });

            $('#commissionType').click(function() {
                commission.removeClass('d-none');
                comInp.removeAttr('disabled');

                if( !$(payment).hasClass("d-none") ) payment.addClass('d-none');
                payInp.attr('disabled', 'disabled');
            });

            $('#paymentType').click(function() {
                payment.removeClass('d-none');
                payInp.removeAttr('disabled');
                
                if( !$(commission).hasClass("d-none") ) commission.addClass('d-none');
                comInp.attr('disabled', 'disabled');
            });
        });
    </script>
@endsection
