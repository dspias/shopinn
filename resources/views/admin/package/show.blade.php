@extends('layouts.admin.app')

@section('page_title', 'Packages | Show Pakcage')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
{{ __('Package Details') }}
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">{{ __('Packages') }}</li>
<li class="breadcrumb-item">{{ __('Show Package') }}</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Details of') }} <span class="text-shopinn">{{ $data->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="pdd-horizon-30 pdd-vertical-20">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Package Name') }}</th>
                                        <td>{{ $data->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Package Type') }}</th>
                                        <td class="text-info">
                                            @if($data->package_type == 1)
                                                <b>{{ 'Free' }}</b>
                                            @elseif($data->package_type == 2)
                                                <b>{{ 'Commission Based' }}</b>
                                            @elseif($data->package_type == 3)
                                                <b>{{ 'Commission & Payment' }}</b>
                                            @endif
                                        </td>
                                    </tr>
                                    @if($data->package_type == 2)
                                    <tr>
                                        <th>{{ __('Payment Details') }}</th>
                                        <td>
                                            {{ __('Total Percentage') }} : {{ $data->total_percentage }} <br><br>

                                            {{ __('First Percentage') }} : {{ $data->first_percentage }} <br>
                                            {{ __('First Percentage Days') }} : {{ $data->first_day }}
                                        </td>
                                    </tr>
                                    @elseif($data->package_type == 3)
                                    <tr>
                                        <th>{{ __('Payment Details') }}</th>
                                        <td>
                                            {{ __('Total Percentage') }} : {{ $data->total_percentage }} <br>
                                            {{ __('Total Payment') }} : {{ $data->total_payment }} <br>
                                            {{ __('Validate Time') }} : {{ $data->year.' Year ' }}{{ $data->month.' Month ' }}{{ $data->day.' Day' }} <br><br>

                                            {{ __('First Percentage') }} : {{ $data->first_percentage }} <br>
                                            {{ __('First Percentage Days') }} : {{ $data->first_day }}
                                        </td>
                                    </tr>
                                    @endif
                                    
                                    <tr>
                                        <th>{{ __('Package Details') }}</th>
                                        <td>{!! $data->description !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            


            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('All Shops Of') }} <span class="text-shopinn">{{ $data->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Shop Email</th>
                                    <th>Join At</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data->logs as $sl => $log)
                                @php                               
                                    if($log->is_active != 1 || $log->end != null) continue;
                                    $shop = $log->shop;
                                    $verified = ($shop->shop)? 1:0;
                                @endphp
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $shop->name }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>@if($verified) {{ $shop->shop->owner_name }} @else {{ 'Not Verified' }} @endif</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>@if($verified) {{ $shop->shop->shop_email }} @else {{ 'Not Verified' }} @endif</b>
                                            </span>
                                        </div>
                                    </td>

                                    @php
                                        $date = new DateTime($shop->created_at);
                                    @endphp

                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $date->format('d M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            @if ($shop->approved_at == null)
                                                <b class="text-info">Pending</b>
                                            @elseif($shop->approved_at != null && $shop->is_active)
                                                <b class="text-success">Active</b>
                                            @elseif($shop->approved_at != null && !$shop->is_active)
                                                <b class="text-danger">Inactive</b>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.shop.details', ['page' => 'all', 'shop_id' => encrypt($shop->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                {{-- <li>
                                                    <a href="#">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Mark As Completed</span>
                                                    </a>
                                                </li> --}}

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
