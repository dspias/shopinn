@extends('layouts.admin.app')

@section('page_title', 'Reseller | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Details reseller
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Resellers</li>
<li class="breadcrumb-item"><a href="{{ route($route) }}"> {{ $page }} </a></li>
<li class="breadcrumb-item">{{ $reseller->name }}</li>
<li class="breadcrumb-item">Details</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu">
                                {{--  <li>
                                    <a href="#">
                                        <i class="ti-pencil text-info pdd-right-10"></i>
                                        <span>Edit Reseller</span>
                                    </a>
                                </li>  --}}
                                @if($reseller->approved_at == null)
                                <li>
                                    <a href="{{ route('admin.reseller.approve', ['reseller_id' => encrypt($reseller->id)]) }}" class="confirmation">
                                        <i class="ti-star text-success pdd-right-10"></i>
                                        <span>Approve</span>
                                    </a>
                                </li>
                                @endif
                                @if($reseller->is_active == 1)
                                <li>
                                    <a href="{{ route('admin.reseller.change_status', ['reseller_id' => encrypt($reseller->id)]) }}" class="confirmation">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactivate</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('admin.reseller.change_status', ['reseller_id' => encrypt($reseller->id)]) }}" class="confirmation">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Activate</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="profile border bottom">
                    <img class="mrg-top-30" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                    <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $reseller->name }}
                        <sup>
                            @if($reseller->approved_at == null)
                            <i class="ti-star text-danger pdd-right-5"></i>(Not Approved)
                            @elseif($reseller->is_active == 1)
                            <i class="ti-check text-success"></i>
                            @else
                            <i class="ti-close text-danger"></i>
                            @endif
                        </sup>
                    </h4>
                    {{-- <div>{!! optional($reseller->reseller)->reseller_address !!}</div> --}}
                </div>
                <div class="pdd-horizon-30 pdd-vertical-20">
                    {{-- @if(optional($reseller->reseller)->note != null)
                        <h5 class="text-bold text-dark">{{ __('About reseller') }}</h5>
                        <div>{!! optional($reseller->reseller)->note !!}</div>
                    @endif --}}

                    <h5 class="text-bold text-dark">{{ __('Reseller Details') }}</h5>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-lg table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>{{ __('Reseller Name') }}</th>
                                        <td>{{ $reseller->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Reseller Mobile Number') }}</th>
                                        <td>{{ $reseller->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Reseller Email') }}</th>
                                        <td>{{ $reseller->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ __('Reseller Address') }}</th>
                                        <td>{!! $reseller->address !!}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $date = new DateTime($reseller->created_at);
                                        @endphp
                                        <th>{{ __('Reseller Register Date') }}</th>
                                        <td>{{ $date->format('d M Y') }}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            if($reseller->approved_at != null)
                                                $approved = new DateTime($reseller->approved_at);
                                            else
                                                $approved = null;
                                        @endphp
                                        <th>{{ __('Reseller Activation Date') }}</th>
                                        <td class="text-info">{{ ($approved != null) ? $approved->format('d M Y'):'Pending' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- <div class="mrg-top-30 text-center">
                        <ul class="list-unstyled list-inline">
                            @if(optional($reseller->reseller)->facebook != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $reseller->reseller->facebook }}" class="btn btn-facebook btn-icon btn-rounded">
                                    <i class="ti-facebook"></i>
                                </a>
                            </li>
                            @elseif(optional($reseller->reseller)->instagram != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $reseller->reseller->instagram }}" class="btn btn-instagram btn-icon btn-rounded">
                                    <i class="ti-instagram"></i>
                                </a>
                            </li>
                            @elseif(optional($reseller->reseller)->twitter != null)
                            <li class="list-inline-item no-pdd-horizon">
                                <a href="{{ $reseller->reseller->twitter }}" class="btn btn-twitter btn-icon btn-rounded">
                                    <i class="ti-twitter"></i>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div> --}}
                </div>
            </div>


            <div class="card">
                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('All Products Of') }} <span class="text-resellerinn">{{ $reseller->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ __('Sl.') }}</th>
                                    <th>{{ __('PID') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Price') }}</th>
                                    <th>{{ __('Stock') }}</th>
                                    <th>{{ __('Shop Name') }}</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $products = $reseller->resellerProducts()->get();
                                @endphp
                                @foreach ($products as $sl => $pro)
                                @php
                                    $product = $pro->product;
                                    if(is_null($product)) continue;
                                @endphp
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $product->pid }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $product->name }}</b>
                                            </span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ get_price($product) }}</span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $product->stock }}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>{{ $product->shop->name }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.product.details', ['page'=>'all','product_id'=>encrypt($product->id)]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>

                                                {{-- <li>
                                                    <a href="#">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Mark As Completed</span>
                                                    </a>
                                                </li> --}}

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="card">
                {{--  <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li class="dropdown">
                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false" title="Click to view more Options">
                                <i class="ti-more text-info"></i>
                            </a>
                            <ul class="dropdown-menu" style="width: 180px;">
                                <li>
                                    <a href="#">
                                        <i class="ti-check text-success pdd-right-10"></i>
                                        <span>Active Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-close text-danger pdd-right-10"></i>
                                        <span>Deactive Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-reload text-info pdd-right-10"></i>
                                        <span>Pending Products</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>  --}}

                <div class="card-heading">
                    <h4 class="card-title float-left text-bold p-t-3 text-uppercase">{{ __('Payment Details Of') }} <span class="text-resellerinn">{{ $reseller->name }}</span></h4>
                </div>

                <hr class="m-t-0">
                <div class="card-block p-20 p-t-0">
                    <div class="table-overflow">
                        <table class="table table-lg table-hover table-bordered table-responsive data-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">{{ __('Invoice ID') }}</th>
                                    <th class="text-center">{{ __('Payment Date') }}</th>
                                    <th class="text-center">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $payments = $reseller->paymentReports;
                                @endphp
                                @foreach ($payments as $sl => $payment)
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>{{ $sl+1 }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b>{{ $payment->invoice_id }}</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-bold">{{ get_date($payment->updated_at, 'd M Y') }}</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('admin.payment.show', ['payment_id' => $payment->id]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Details</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
