@extends('layouts.admin.app')

@section('page_title', 'Resellers | Commission')

@section('css_links')
{{--  External CSS  --}}
@endsection

@section('custom_css')
{{--  External CSS  --}}
<style>
/* Custom CSS Here */
    .form-control{
        padding: 0.700rem 0.75rem;
    }
    .selectize-input {
        padding: 0.640rem 0.75rem;
    }

    .responsive-image{
        border-radius: 5px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        height: 80px;
        width: 100%;
    }
    .card-block.task-file.p-0 {
        height: 80px;
    }
    .selectize-dropdown.single{
        z-index: 99999999;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px !important;
    }
    a.deactive{
        cursor: pointer;
    }
    li.social-media-list{
        border: 0px !important;
        border-bottom: 1px solid #e6ecf5 !important;
        margin-bottom: 10px;
    }
    li.social-media-list:last-child{
        border: 0px !important;
        border-bottom: 0px solid #e6ecf5 !important;
        margin-bottom: 0px;
    }

    .password-generate {
        position: absolute;
        right: 15px;
        font-weight: bold;
        text-transform: uppercase;
    }


    .note-fontname button{
        min-width: 200px;
    }

    .note-para .note-btn-group{
        display: none;
    }

    .note-insert button:nth-child(2),
    .note-insert button:nth-child(3){
        display: none;
    }
</style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Reseller Commission
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Resellers</li>
<li class="breadcrumb-item">Reseller Commission</li>
@endsection




@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('admin.reseller.commission.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-block p-25">
                                <div class="row justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ __('Reseller Commission ') }}<span class="required">*</span></label>
                                            <input autocomplete="off" type="number" step=".1" name="commission" class="form-control @error('commission') is-invalid @enderror" value="{{ company_get('reseller_commision') ??old('commission') }}" placeholder="5" min="0.00" required>
                                            {{-- <small class="text-info">{{ __('Please enter a correct commission. The shop can login with this commission.') }}</small> --}}

                                            @error('commission')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline pull-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Update Reseller Commission
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
    </script>
@endsection
