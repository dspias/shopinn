@extends('layouts.admin.app')

@section('page_title', 'JOBS | Admin | Profile')

@section('css_links')
    {{--  External CSS  --}}
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .profile-pic {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }

        .file-upload {
            display: none;
        }
        .circle {
            border-radius: 1000px !important;
            overflow: hidden;
            width: 160px;
            height: 160px;
            border: 8px solid rgba(222, 226, 225, .8);
        }
        img {
            max-width: 100%;
            height: auto;
        }

        element.style {
        }
        .p-image:hover {
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }
        .p-image {
            position: absolute;
            bottom: 6px;
            left: 137px;
            color: #000000;
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }
        .p-image:hover {
          transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }
        .upload-button {
          font-size: 1.5em;
        }

        .upload-button:hover {
          transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
          color: #999;
        }
    </style>
@endsection

@section('page_header_title')
{{-- Page Title Here --}}
Profile
@endsection

@section('breadcrumb_item_lists')
{{-- Breadcrumb Items Here --}}
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item">Profile</li>
@endsection





@section('main_content')
{{-- Main Contents Here --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="widget-profile-1 card">
                <div class="portlet">
                    <ul class="portlet-item navbar">
                        <li>
                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" title="Back To Previous Page">
                                <i class="ti-arrow-left text-danger"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <hr class="m-t-0">
                <div class="pdd-horizon-30 pdd-vertical-20">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card card-dark card-child author-box">
                                        <div class="card-body">
                                            <div class="author-box-center">
                                                @if(auth()->user()->avatar != null)
                                                    <img alt="image" src="{{ url(env('IMG_STORE', 'storage/').auth()->user()->avatar) }}" class="rounded-circle author-box-picture">
                                                @else
                                                    <img alt="image" src="{{ asset('assets/images/user.jpg') }}" class="rounded-circle author-box-picture">
                                                @endif
                                                <div class="clearfix"></div>
                                                <div class="author-box-name">
                                                    <a href="{{ route('admin.profile.index') }}">{{ Auth::user()->name }}</a>
                                                </div>
                                                <div class="author-box-job">{{ Auth::user()->role->name }}</div>
                                            </div>


                                            <div class="py-4">
                                                <p class="clearfix">
                                                    <span class="float-left">
                                                        Name:
                                                    </span>
                                                    <span class="float-right text-muted">
                                                        {{ auth()->user()->name }}
                                                    </span>
                                                </p>

                                                <p class="clearfix">
                                                    <span class="float-left">
                                                        Email:
                                                    </span>
                                                    <span class="float-right text-muted">
                                                        {{ auth()->user()->email }}
                                                    </span>
                                                </p>

                                                <p class="clearfix">
                                                    <span class="float-left">
                                                        Mobile:
                                                    </span>
                                                    <span class="float-right text-muted">
                                                        @if( auth()->user()->mobile != null)
                                                            {{ auth()->user()->mobile }}
                                                        @else
                                                            {{ '-' }}
                                                        @endif
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card card-dark card-child">
                                        <div class="card-header">
                                            <h4>Password Change</h4>
                                        </div>

                                        <form action="{{ route('change_password') }}" method="post">
                                            @csrf
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Old Password <sup class="required">*</sup></label>
                                                            <input type="password" name="old_password" class="form-control @error('old_password') is-invalid @enderror" required>

                                                            @error('old_password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>New Password <sup class="required">*</sup></label>
                                                            <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" required>

                                                            @error('new_password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Confirm Password <sup class="required">*</sup></label>
                                                            <input type="password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror" required>

                                                            @error('confirm_password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer" style="padding-top: 0px;">
                                                <button type="submit" class="btn bg-purple btn-custom btn-submit float-right m-b-10">
                                                    Change Password
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-7">
                            <div class="card card-dark card-child">
                                <div class="card-header">
                                    <h4>Update Profile Info</h4>
                                </div>

                                <form action="{{ route('admin.profile.update') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        @php
                                            if(auth()->user()->avatar != null)
                                                $url = url(env('IMG_STORE', 'storage/').auth()->user()->avatar);
                                            else
                                                $url = asset('assets/images/user.jpg');
                                        @endphp


                                        <div class="col-md-4 mx-auto">
                                            <div class="form-group">
                                                <div class="circle">
                                                  <!-- User Profile Image -->
                                                  <img class="profile-pic" src="{{ $url }}">

                                                  <!-- Default Image -->
                                                  <!-- <i class="fa fa-user fa-5x"></i> -->
                                                </div>
                                                <div class="p-image">
                                                  <i class="fa fa-camera upload-button"></i>
                                                   <input class="file-upload" type="file" name="avatar" accept="image/*"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Admin Full Name <sup class="required">*</sup></label>
                                                <input type="text" name="name" value="{{ auth()->user()->name }}" class="form-control @error('name') is-invalid @enderror" placeholder="Mr. Jhon Doe" required>

                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Login Email <sup class="required">*</sup></label>
                                                <input type="text" name="email" value="{{ auth()->user()->email }}" class="form-control @error('email') is-invalid @enderror" placeholder="Mr. Jhon Doe" required>

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Mobile Number<sup class="required">*</sup></label>
                                                <input type="text" name="mobile" value="{{ auth()->user()->mobile }}" class="form-control @error('mobile') is-invalid @enderror" placeholder="mobile number here..." required>

                                                @error('mobile')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-footer" style="padding-top: 0px;">
                                        <button type="submit" class="btn bg-purple btn-custom btn-submit float-right m-b-10">
                                            Update Profile
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
        $(document).ready(function() {
            var readURL = function(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('.profile-pic').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }


                $(".file-upload").on('change', function(){
                    readURL(this);
                });

                $(".upload-button").on('click', function() {
                $(".file-upload").click();
                });
            });
    </script>
@endsection
