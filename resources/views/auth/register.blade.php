<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>SHOPINN || REGISTER</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/logo/favicon.png') }}">

        <!-- plugins css -->
        <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/PACE/themes/blue/pace-theme-minimal.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" />

        <!-- core css -->
        <link href="{{ asset('assets/css/ei-icon.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/margin_padding.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

        <style>
            .send-code-email{
                    position: inherit;
                    top: 36px;
                    left: -1px;
                    background: #ff084e;
                    padding: 8px 10px;
                    color: #fff !important;
            }
        </style>
    </head>

    <body>
        <div class="app">
            <div class="authentication">
                <div class="sign-in-2">
                    <div class="container-fluid no-pdd-horizon bg" style="background-image: url('assets/images/others/img-29.jpg')">
                        <div class="row">
                            <div class="col-md-6 mr-auto ml-auto">
                                <div class="row">
                                    <div class="mr-auto ml-auto full-height height-100 d-flex align-items-center m-t-30" style="width: 100%;">
                                        <div class="vertical-align full-height">
                                            <div class="table-cell">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="pdd-horizon-30 pdd-vertical-30">
                                                            <div class="mrg-btm-30 text-center">
                                                                <a href="/" data-toggle="tooltip" title="{{ __('Go to Homepage') }}">
                                                                    <img class="img-responsive inline-block" src="assets/images/logo/logo.png" alt="">
                                                                </a>
                                                            </div>
                                                            <h2 class="text-uppercase border bottom">{{ __('Register in') }} <span class="text-shopinn text-bold">{{ __('ShopInn') }}</span></h2>
                                                            <form method="POST" action="{{ route('register') }}">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="name">
                                                                                {{ __('Full Name') }} <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" value="{{ old('name') }}" name="name" autocomplete="off" autofocus class="form-control @error('name') is-invalid @enderror" placeholder="{{ __('Full Name') }}" tabindex="0" required>

                                                                            @error('name')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="email" class="label-with-link">
                                                                                {{ __('Email')." *" }}
                                                                                <span style="position: relative;">
                                                                                    <a href="javascript:void(0);" tabindex="10" class="send-code-email text-bold tooltip-msg" id="sendMail" data-toggle="tooltip" data-placement="top" title="{{ __('Send Verification Code On Your Email') }}">
                                                                                        {{ __('Send Code') }}
                                                                                    </a>
                                                                                </span>
                                                                            </label>
                                                                            <input type="email" value="{{ old('email') }}" name="email" autocomplete="off" autofocus class="form-control @error('email') is-invalid @enderror" id="emailAddress" placeholder="{{ __('Email') }}" tabindex="0" required style="padding-right: 95px;">

                                                                            @error('email')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="phone" class="label-with-link">
                                                                                {{ __('Phone') }} {{ '*' }}
                                                                                <span style="position: relative;">
                                                                                    <a href="javascript:void(0);" tabindex="10" class="send-code-email text-bold d-none" id="sendPhoneCode" data-toggle="tooltip" data-placement="top" title="{{ __('Send Verification Code On Your Mobile Number') }}">
                                                                                        {{ __('Send Code') }}
                                                                                    </a>
                                                                                </span>
                                                                            </label>
                                                                            <input type="text" value="{{ old('mobile') }}" name="mobile" autocomplete="off" tabindex="0" class="form-control @error('mobile') is-invalid @enderror" id="mobileNumber" placeholder="{{ __('mobile') }}" style="padding-right: 95px;">

                                                                            @error('mobile')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="code">
                                                                                {{ __('Verification Code') }} <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" id="code_input" name="code" required autocomplete="off" tabindex="0" class="form-control @error('code') is-invalid @enderror" placeholder="{{ __('Verification Code') }}">

                                                                            @error('code')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                            <span id="alrt"></span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="password">
                                                                                {{ __('Password') }}  <span class="required">*</span>
                                                                            </label>
                                                                            <input type="password" value="{{ old('password') }}" name="password" required autocomplete="off" tabindex="0" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password Minimum 8 Characters') }}">

                                                                            @error('password')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group reg-as-s-r d-none">
                                                                    <label for="register_as">
                                                                        {{ __('Register As') }}  <span class="required">*</span>
                                                                    </label>
                                                                    <select tabindex="0" id="selectize-dropdown" name="register_as" disabled autocomplete="off" class="form-control registration-type @error('register_as') is-invalid @enderror">
                                                                        <option value="" disabled selected>{{ __('Register As') }}</option>
                                                                        <option value="shop">{{ __('Shop') }}</option>
                                                                        <option value="reseller">{{ __('Reseller') }}</option>
                                                                    </select>

                                                                    @error('register_as')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>

                                                                <div class="checkbox font-size-12">
                                                                    <input id="dynamic_check" type="checkbox" {{ old('dynamic_check') ? 'checked' : '' }} type="checkbox" tabindex="0">
                                                                    <label for="dynamic_check" tabindex="0">{{ __('Register as Shop / Reseller ?') }}</label>
                                                                </div>

                                                                <button type="submit" tabindex="0" class="btn btn-primary btn-block text-uppercase text-bold" id="submit_button" disabled>
                                                                    {{ __('Register') }}
                                                                </button>
                                                            </form>
                                                            <div class="text-center mt-2">
                                                                <p>
                                                                    <strong>Or Sign In with</strong>
                                                                </p>
                                                                {{-- <a href="{{ route('login.provider','facebook') }}" class="btn btn-outline-primary">Facebook</a> --}}
                                                                <a href="{{ route('login.provider','google') }}" class="btn btn-outline-danger">Google</a>
                                                            </div>
                                                            <span class="font-size-13 pull-right pdd-top-10">{{ __('Already have an account?') }} <a href="{{ route('login') }}" class="text-bold text-shopinn">{{ __('Sign In') }}</a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- //added sweetalert --}}
        @include('sweetalert::alert')

        <script src="{{ asset('assets/js/vendor.js') }}"></script>

        <script src="{{ asset('assets/js/app.min.js') }}"></script>


        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>

        <script>
            $(document).ready(function(){
                var checkbox = $('#dynamic_check');

                checkbox.click(function(){
                    var div = $('.reg-as-s-r');
                    var select = $('.registration-type');
                    var sendPhoneCode = $('#sendPhoneCode');

                    if($(this).prop("checked") == true){
                        div.removeClass('d-none');
                        sendPhoneCode.removeClass('d-none');
                        select.removeAttr('disabled', 'disabled');
                        select.removeAttr('required', 'required');
                    }
                    else{
                        div.addClass('d-none');
                        sendPhoneCode.addClass('d-none');
                        select.attr('disabled', 'disabled');
                        select.attr('required', 'required');
                    }
                });
            });
        </script>

        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>











        {{-- Ajax Coding  --}}
        <script>
            $(document).ready(function(){
                function validateMobile(mobile){
                    var filter = /^\d*(?:\.\d{1,2})?$/;

                    if (filter.test(mobile)) {
                        if(mobile.length==11){
                            return true;
                        } else {
                            alert('Please put 11  digit mobile number');
                            return false;
                        }
                    }
                    else {
                        alert('Not a valid number');
                        return false;
                    }
                }


                function validateEmail(email) {
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
                    {
                        return (true)
                    }
                    alert("You have entered an invalid email address!")
                    return (false)
                }


                $("#sendPhoneCode").click(function (e) {
                    e.preventDefault();
                    var mobile = $("#mobileNumber").val();
                    var validate = validateMobile(mobile);
                    if(validate == true){
                        $.ajax({
                            type: "POST",
                            url: "{{ route('sendcode') }}",
                            data: {
                                mobile: mobile,
                            },

                            success: function (data) {
                                alert(data);
                            },
                        });
                    }
                });

                $("#sendMail").click(function (e) {
                    e.preventDefault();
                    var email = $("#emailAddress").val();
                    var validate = validateEmail(email);
                    if(validate == true){
                        $.ajax({
                            type: "POST",
                            url: "{{ route('sendcode') }}",
                            data: {
                                email: email,
                            },

                            success: function (data) {
                                alert(data);
                            },
                        });
                    }
                });

                $("#code_input").focusout(function (e) {
                    e.preventDefault();
                    var mobile = $("#mobileNumber").val();
                    var email = $("#emailAddress").val();
                    var code = $('#code_input').val();
                    console.log(mobile);
                    var validateMail = validateEmail(email);
                    var validateMbl = (mobile != '') ? validateMobile(mobile):true;
                    if(validateMail == true && validateMbl == true){
                        $.ajax({
                            type: "POST",
                            url: "{{ route('checkcode') }}",
                            data: {
                                mobile: mobile,
                                email: email,
                                code: code,
                            },

                            success: function (data) {
                                if(data == false){
                                    var alert = '<small class="text-danger">Code Does not matched.</small>';
                                    $('#alrt').empty().append(alert);
                                    $('#submit_button').attr('disabled', 'disabled');
                                } else {
                                    var alert = '<small class="text-success">Code Matched.</small>';
                                    $('#alrt').empty().append(alert);
                                    $('#submit_button').removeAttr('disabled', 'disabled');
                                }
                            },
                        });
                    }
                });
            });
        </script>
    </body>
</html>
