<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>SHOPINN || REGISTER</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/logo/favicon.png') }}">

        <!-- plugins css -->
        <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/PACE/themes/blue/pace-theme-minimal.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" />

        <!-- core css -->
        <link href="{{ asset('assets/css/ei-icon.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/margin_padding.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    </head>

    <body>
        <div class="app">
            <div class="authentication">
                <div class="sign-in-2">
                    <div class="container-fluid no-pdd-horizon bg" style="background-image: url('assets/images/others/img-29.jpg')">
                        <div class="row">
                            <div class="col-md-6 mr-auto ml-auto">
                                <div class="row">
                                    <div class="mr-auto ml-auto full-height height-100 d-flex align-items-center m-t-30" style="width: 100%;">
                                        <div class="vertical-align full-height">
                                            <div class="table-cell">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="pdd-horizon-30 pdd-vertical-30">
                                                            <div class="mrg-btm-30 text-center">
                                                                <img class="img-responsive inline-block" src="assets/images/logo/logo.png" alt="">
                                                            </div>
                                                            <h2 class="text-uppercase border bottom">{{ __('Account') }} <span class="text-shopinn text-bold">{{ __('Verification') }}</span></h2>
                                                            <p class="text-mute">{{ __('A fresh verification link has been sent to your Phone / Email.') }}</p>
                                                            <form method="POST" action="{{ route('verification.resend') }}">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="codde">
                                                                                {{ __('Verification Code') }} <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" value="{{ old('code') }}" name="code" autocomplete="off" autofocus class="form-control @error('code') is-invalid @enderror" placeholder="{{ __('Verification Code') }}" tabindex="0" required>

                                                                            @error('code')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <button type="submit" tabindex="0" class="btn btn-primary btn-block text-uppercase text-bold" disabled>
                                                                    {{ __('Verify Account') }}
                                                                </button>
                                                            </form>
                                                            <span class="font-size-13 pull-right pdd-top-10">{{ __('Didn\'t Receive Verification Code?') }} <a href="{{ route('login') }}" class="text-bold text-shopinn">{{ __('Send Code Again') }}</a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/js/vendor.js') }}"></script>

        <script src="{{ asset('assets/js/app.min.js') }}"></script>
    </body>
</html>