<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>Shopinnbd || Register</title>
    <link rel="icon" href="{{ asset('vendor/images/fav.png') }}" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/main.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/weather-icon.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/weather-icons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/color.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css/responsive.css') }}">

</head>
<body>
	<div class="www-layout">
        <section>
        	<div class="gap no-gap signin whitish medium-opacity register">
                <div class="bg-image" style="background-image:url('{{ asset('vendor/images/resources/theme-bg.jpg') }}')"></div>
                <div class="container">
                	<div class="row">
                        <div class="col-lg-8">
                            <div class="big-ad">
                                <figure><img src="{{ asset('vendor/images/logo2.png') }}" alt=""></figure>
                                <h1>Welcome to the Shopinnbd</h1>
                                <p>
                                     Pitnik is a social network template that can be used to connect people.  this template for multipurpose social activities like job, dating, posting, bloging and much more. Now join & Make Cool Friends around the world !!!                             
                                </p>
                                <div class="barcode">
                                    <figure><img src="{{ asset('vendor/images/resources/Barcode.jpg') }}" alt=""></figure>
                                    <div class="app-download">
                                        <span>Download Mobile App and Scan QR Code to login</span>
                                        <ul class="colla-apps">
                                            <li><a title="" href="https://play.google.com/store?hl=en"><img src="{{ asset('vendor/images/android.png') }}" alt="">android</a></li>
                                            <li><a title="" href="https://www.apple.com/lae/ios/app-store/"><img src="{{ asset('vendor/images/apple.png') }}" alt="">iPhone</a></li>
                                            <li><a title="" href="https://www.microsoft.com/store/apps"><img src="{{ asset('vendor/images/windows.png') }}" alt="">Windows</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="we-login-register">
                                <div class="form-title">
                                    <i class="fa fa-key"></i>Sign Up
                                    <span>Sign Up now and meet the awesome friends around the world.</span>
                                </div>
                                <form class="we-form" method="post" action="{{ route('register') }}">
                                    @csrf
                                    <input type="text" name="name" value="{{ old('name') }}" required  placeholder="Name">

                                    <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">

                                    <input type="text" name="number" placeholder="+1234-567-987">

                                    <input type="password" name="password" placeholder="{{ __('Password') }}">

                                    <input type="password" name="password_confirmation" placeholder="{{ __('Confirm Password') }}">

                                    <input type="checkbox"><label>Send code to Mobile</label>
                                    <button type="submit" data-ripple="">Register</button>
                                    @if (Route::has('password.request'))
                                        <a class="forgot underline" href="{{ route('password.request') }}" title="">{{ __('Forgot Your Password?') }}</a>
                                    @endif
                                </form>
                                <a class="with-smedia facebook" href="#" title="" data-ripple="">Register with facebook</a>
                                <a class="with-smedia twitter" href="#" title="" data-ripple="">Register with twitter</a>
                                <span>already have an account? <a class="we-account underline" href="{{ route('login') }}" title="">Sign in</a></span>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
         
    </div>
    
    <script src="{{ asset('vendor/js/main.min.js') }}"></script>
    <script src="{{ asset('vendor/js/script.js') }}"></script>
</body>	
</html>
