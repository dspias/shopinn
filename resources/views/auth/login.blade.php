<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>SHOPINN || LOGIN</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/logo/favicon.png') }}">

        <!-- plugins css -->
        <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/PACE/themes/blue/pace-theme-minimal.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" />

        <!-- core css -->
        <link href="{{ asset('assets/css/ei-icon.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/css/margin_padding.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

        <style>
            label.pass{
                display: flex;
                justify-content: space-between;
            }
            .checkbox input[type=checkbox]:checked + label:before {
                color: #ff084e;
            }
            .checkbox input[type=checkbox] + label:before {
                border: 2px solid #ff084e;
            }
            .authentication .sign-in .login-footer{
                border-top: 0px solid #ffffff !important;
            }
        </style>
    </head>

    <body>
        <div class="app">
            <div class="authentication">
                <div class="sign-in-2">
                    <div class="container-fluid no-pdd-horizon bg" style="background-image: url('assets/images/others/img-29.jpg');">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 m-auto full-height height-100 d-flex align-items-center">
                                        <div class="vertical-align full-height">
                                            <div class="table-cell">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="pdd-horizon-30 pdd-vertical-30" style="padding-bottom: 0px !important;">
                                                            <div class="mrg-btm-30 text-center">
                                                                <img class="img-responsive inline-block" src="assets/images/logo/logo.png" alt="" />
                                                            </div>
                                                            <h2 class="text-uppercase border bottom m-b-40 text-center">{{ __('Login to') }} <span class="text-shopinn text-bold">{{ __('ShopInn') }}</span></h2>
                                                            
                                                            <form method="POST" action="{{ route('login_with_email_or_mobile') }}">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="email">{{ __('E-Mail Or Mobile') }}</label>
                                                                    <input type="text" value="{{ old('email') }}" name="email" required autocomplete="off" autofocus tabindex="0" class="form-control @error('email') is-invalid @enderror" placeholder="{{ __('E-Mail Or Mobile') }}">
                
                                                                    @error('email')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                
                                                                <div class="form-group">
                                                                    <label for="password" class="pass">{{ __('Password') }} <span><a href="{{ route('password.request') }}" tabindex="10" class="text-danger">{{ __('Forgot Password?') }}</a></span></label>
                                                                    <input type="password" value="{{ old('password') }}" name="password" required autocomplete="off"  tabindex="0" class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}">
                
                                                                    @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                
                                                                <div class="checkbox font-size-12">
                                                                    <input id="agreement" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }} type="checkbox">
                                                                    <label for="agreement">{{ __('Keep Me Signed In') }}</label>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary btn-block text-uppercase text-bold">{{ __('Login') }}</button>
                                                            </form>                
                                                        </div>
                                                    </div>

                                                    <div class="card-footer text-center">
                                                        <div class="text-center">
                                                            <p class="mb-0">
                                                                <strong>Or Sign In with</strong>
                                                            </p>
                                                            {{-- <a href="{{ route('login.provider','facebook') }}" class="btn btn-outline-primary">Facebook</a> --}}
                                                            <a href="{{ route('login.provider','google') }}" class="btn btn-outline-danger">Google</a>
                                                        </div>
                                                        <span class="font-size-13 text-center pdd-top-10">{{ __('Don\'t have an account?') }} <a href="{{ route('register') }}" class="text-bold text-shopinn">{{ __('Sign Up') }}</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        


        {{-- //added sweetalert --}}
        @include('sweetalert::alert')

        <script src="{{ asset('assets/js/vendor.js') }}"></script>

        <script src="{{ asset('assets/js/app.min.js') }}"></script>

        <!-- page js -->

    </body>
</html>
