@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Product Details')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Product Details')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('vendor/starrating/rate.css') }}">
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .left-detail-meta {
        width: 100%;
    }
    /* input number style */
    .input-number {
        width: 80px;
        padding: 0 12px;
        vertical-align: top;
        text-align: center;
        outline: none;
        border: 1px solid #ffffff;
    }

    .input-number,
    .input-number-decrement,
    .input-number-increment {
        height: 40px;
        user-select: none;
    }

    .input-number-decrement, .input-number-increment {
        display: inline-block;
        width: 40px;
        line-height: 38px;
        background: #f1f1f1;
        color: #444;
        text-align: center;
        /* font-weight: bold; */
        cursor: pointer;
        font-size: 1.5rem;
    }
    .input-number-decrement:active,
    .input-number-increment:active {
        color: #ffffff;
        background: #ff084e;
    }

    .input-number-decrement,
    .input-number-increment {
        border: 1px solid #f1f1f1;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .st-line {
        padding-top:2px;
        margin-top: 0rem;
        margin-bottom: 0rem;
        border: 1px;
        border-top: 10px solid rgba(0,0,0,.1);
    }

</style>


<style>

.magnify {
  border-radius: 50%;
  border: 5px solid #ffffff;
  position: absolute;
  z-index: 20;
  background-repeat: no-repeat;
  background-color: white;
  box-shadow: inset 0 0 20px rgba(0, 0, 0, 0.5);
  display: none;
  cursor: none;
}
.at-style-responsive .at-share-btn{
    padding: 2px !important;
}
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')


<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="prod-detail">
                <div class="row">
                    <div class="col-md-6">
                        <div class="central-meta">
                            @php
                                $urls = (array)get_images($product, 'details');
                                // $urls = (array)get_images($product, 'slider');
                            @endphp
                            <div class="slick-single">
                                @foreach ($urls as $url)
                                    <div>
                                        <img class="img-fluid magnifiedImg" src="{{ $url }}" alt="Image Not Found (500*460)">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-lg-6">
                        <div class="prod-avatar">
                            @php
                                $urls = (array)get_images($product, 'details');
                            @endphp
                            <ul class="slider-for-gold">
                                @foreach ($urls as $url)
                                    <li><img class="magnifiedImg" src="{{ $url }}" alt="Image Not Found"></li>
                                @endforeach
                            </ul>
                            <ul class="slider-nav-gold">
                                @foreach ($urls as $url)
                                    <li><img src="{{ $url }}" alt="Image Not Found"></li>
                                @endforeach
                            </ul>
                        </div>
                    </div> --}}

                    <div class="col-lg-6">
                        <div class="full-postmeta">
                            <div class="left-detail-meta">
                                <span>{{ __('ID:') }} {{ $product->pid }}
                                    @php
                                        $user = auth()->user();
                                    @endphp

                                    @if ( Auth::check() && $product->shop_id == $user->id)
                                        <span class="offline text-shopinn float-right"><i class="fa fa-circle"></i> {{ $product->stock." " }} {{ __(' Available') }}</span>
                                        @if($product->approved_at == null)
                                            <span class="text-warning">{{ ('Under Review') }}</span>
                                        @elseif($product->is_active == 0)
                                            <span class="text-danger">{{ ('Inactive') }}</span>
                                        @elseif($product->is_featured == 1)
                                            <span class="text-primary">{{ ('Featured') }}</span>
                                        @else
                                            <span class="text-success">{{ ('Active') }}</span>
                                        @endif
                                    @elseif(Auth::check() && $user->role_id == 3)
                                        @php
                                            $is_added = isAdded($product->id, $user->id);
                                        @endphp
                                        @if(!$is_added)  {{-- already added on reseller shop --}}
                                            <a href="{{ route('reseller.product.add', ['product_id' => $product->id, 'reseller_id' => $user->id]) }}" class="btn btn-shopinn btn-sm confirmation float-right">{{ __('Add To Fav') }}</a>
                                        @else  {{-- add on reseller shop --}}
                                            <a href="{{ route('reseller.product.remove', ['product_id' => $product->id, 'reseller_id' => $user->id]) }}" class="btn btn-shopinn btn-sm confirmation float-right">{{ __('Remove From Fav') }}</a>
                                        @endif
                                    @elseif ($product->stock < 1)
                                        <span class="offline text-shopinn float-right"><i class="fa fa-circle"></i> {{ __('Not Available') }}</span>
                                    @else
                                        <span class="offline text-success float-right"><i class="fa fa-circle"></i> {{ $product->stock." " }} {{ __(' Available') }}</span>
                                    @endif
                                </span>
                                <h4 class="m-t-10">{{ __($product->name) }}</h4>
                                <span>
                                    <ul class="p-l-0 m-0">
                                        <li class="d-inline-block">{{ __('Shop: ') }}<a href="{{ route('guest.shop.show', ['shop_id' => $product->shop->id,'shop_name' => get_name($product->shop->name)]) }}" class="text-shopinn text-bold">{{ $product->shop->name }}</a></li>
                                    </ul>
                                </span>
                                <span>
                                    <ul class="p-l-0 m-0">
                                        <li class="d-inline-block">
                                            <div id="rateBox"></div>
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <ul class="media-info">
                                <li style="width: 100%;">
                                    @if(offer_price($product)['isOffer'] == 1)
                                        <span class="text-bold text-shopinn h1">
                                            {{ offer_price($product)['price'] }}<sup><i class="text-shopinn text-bold">৳</i></sup>
                                        </span>
                                        <span class="discount h5">
                                            (<del>{{ offer_price($product)['main'] }}<sup><i class="text-bold">৳</i></sup></del>)
                                            <span class="dis-percentage"><sup><i class="text-bold text-shopinn">-{{ offer_price($product)['offer'] }}%</i></sup></span>
                                        </span>
                                    @else
                                        <span class="text-bold text-shopinn h1">
                                            {{ offer_price($product)['main'] }}<sup><i class="text-shopinn text-bold">৳</i></sup>
                                        </span>
                                    @endif
                                </li>
                            </ul>
                            @livewire('guest.product-details', ['product' => $product])
                        </div>

                    </div>
                </div>

                {{--  product details start  --}}
                <div class="gap no-bottom">
                    <div class="more-about">
                        <div class="central-meta">
                            <span class="title2">{{ __('Product details of') }} {{ __($product->name) }}</span>
                            <div class="prduct-dtls">
                                {!! $product->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                {{--  product tetails close  --}}

                {{--  product rating start  --}}
                <div class="central-meta">
                    <h3 class="title2">{{ __('Ratings & Reviews of') }} {{ $product->name }}</h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="p-l-0 m-0">
                                <li class="d-inline-block" style="width: 100%;">
                                    <div class="row">
                                        <div class="col-8">
                                            <div id="rateBoxMain"></div>
                                        </div>
                                        <div class="col-4"><h4>{{ $product->ratings->count() }} <span style="font-size: 10px;">{{ __('People') }}</span></h4></div>
                                        <div class="col-12"><h4>{{ __('Average Rating') }}</h4></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="p-l-0 m-0">
                                <li class="d-inline-block">
                                    <div class="row">
                                        <div class="col-6"><div id="rateBox5"></div></div>
                                        <div class="col-4"><hr class="st-line"></div>
                                        <div class="col-2">{{ $product->ratings->where('rate', 5)->count() }}</div>

                                        <div class="col-6"><div id="rateBox4"></div></div>
                                        <div class="col-4"><hr class="st-line"></div>
                                        <div class="col-2">{{ $product->ratings->where('rate', 4)->count() }}</div>

                                        <div class="col-6"><div id="rateBox3"></div></div>
                                        <div class="col-4"><hr class="st-line"></div>
                                        <div class="col-2">{{ $product->ratings->where('rate', 3)->count() }}</div>

                                        <div class="col-6"><div id="rateBox2"></div></div>
                                        <div class="col-4"><hr class="st-line"></div>
                                        <div class="col-2">{{ $product->ratings->where('rate', 2)->count() }}</div>

                                        <div class="col-6"><div id="rateBox1"></div></div>
                                        <div class="col-4"><hr class="st-line"></div>
                                        <div class="col-2">{{ $product->ratings->where('rate', 1)->count() }}</div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('Reviews') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product->reviews as $review)
                                    <tr>
                                        <td>
                                            <i class="fa fa-star text-shopinn"></i>
                                            <a class="text-shopinn" href="javascript:void(0);">{{ $review->customer->name }}</a>
                                            <p>{{ $review->review }}</p>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{--  product rating close  --}}



                {{--  product comments start  --}}
                <div class="central-meta">
                    <h3 class="title2">{{ __('Comments & Suggestions of') }} {{ $product->name }}</h3>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table">
                                <tbody>
                                    @auth
                                        <tr>
                                            <td>
                                                <form action="{{ route('guest.product.comment', ['product_id' => $product->id]) }}" method="post">
                                                @csrf
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <input type="text" class=" form-control border-shopinn border-radius-0" placeholder="{{ __('Write your comment or suggestion') }}" name="comment">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button type="submit" class="btn btn-shopinn float-right btn-block border-radius-0">{{ __('Comment') }}</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endauth

                                    @forelse ($product->comments as $item => $comment)
                                        <tr>
                                            <td>
                                                <div class="comments">
                                                    <i class="fa fa-comment-o text-shopinn"></i>
                                                    <a class="text-shopinn" href="javascript:void(0);">{{ $comment->user->name }}</a>
                                                    <p>{{ $comment->comment }}</p>
                                                </div>

                                                @foreach ($comment->replies as $sl => $reply)
                                                    <div class="comment-reply p-l-30">
                                                        <i class="fa fa-comments-o text-shopinn"></i>
                                                        <a class="text-shopinn" href="javascript:void(0);">{{ $reply->user->name }}</a>
                                                        <p>{{ $reply->reply }}</p>
                                                    </div>
                                                @endforeach

                                                @auth
                                                    <div class="comment-new-reply p-l-30">
                                                        <form action="{{ route('guest.product.reply', ['comment_id' => $comment->id]) }}" method="post">
                                                        @csrf
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <input type="text" class=" form-control border-shopinn border-radius-0" placeholder="{{ __('Comment Reply') }}" name="reply">
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button type="submit" class="btn btn-shopinn float-right btn-block border-radius-0">{{ __('Reply') }}</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                @endauth
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>{{ __('No Comments Available') }}</tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{--  product comments close  --}}



                {{--  more matches  start--}}
                <div class="central-meta">
                    <span class="title2">{{ __('More Matches') }}</span>
                    <div class="row remove-ext">
                        @foreach($matches as $match)
                        <div class="col-md-3 item">
                            <div class="pitdate-user">
                                <figure><img src="{{ get_image($match, 'card') }}" alt="">
                                    <div class="likes heart text-shopinn" title="Rating" aria-disabled="true">
                                        <i class="fa fa-star text-shopinn"></i>
                                        <span class="m-t-1 text-shopinn"><b>{{ get_avarage_rating($match->ratings) }}</b></span>
                                    </div>
                                    {{--  <div class="more">
                                        <div class="more-post-optns">
                                            <i class="ti-more-alt"></i>
                                            <ul>
                                                <li class="send-mesg"><a href="#"><i class="fa fa-info"></i>{{ __('Details') }}</a></li>
                                                <li class="get-link"><i class="fa fa-link"></i>{{ __('Copy Share Link') }}</li>
                                                <li class="bad-report"><i class="fa fa-trash"></i>{{ __('Delete') }}</li>
                                            </ul>
                                        </div>
                                    </div>  --}}
                                </figure>
                                <h4>
                                    <a href="{{ route('guest.product.show', ['product_id' => $match->id, 'product_name' => get_name($match->name)]) }}" class="title-1-line">{{ $match->name }}</a>
                                </h4>
                                <span class="send-mesg"><i style="font-size: 12px; font-weight: 700;">৳</i> {{ $match->sell_price }}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                {{--  more matches close  --}}
            </div>

        </div>
    </div>
</div>
@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('vendor/starrating/rate.js') }}"></script>

    {{--  <!-- Go to www.addthis.com/dashboard to customize your tools -->  --}}
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f83070b2a72cac8"></script>

@endsection


{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function() {
            // $('.js-tooltip').tooltip();

            $('#copyURL').click(function() {
                // var text = $('#copy_url').val();
                var text = document.URL;
                var el = $(this);
                copyToClipboard(text, el);
            });
        });

        function copyToClipboard(text, el) {
            var copyTest = document.queryCommandSupported('copy');
            var elOriginalText = el.attr('data-original-title');

            if (copyTest === true) {
                var copyTextArea = document.createElement("textarea");
                copyTextArea.value = text;
                document.body.appendChild(copyTextArea);
                copyTextArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'Link Copied!' : 'Whoops, not copied!';
                    el.attr('data-original-title', msg).tooltip('show');
                } catch (err) {
                    alert('Oops, unable to copy');
                }
                document.body.removeChild(copyTextArea);
                el.attr('data-original-title', elOriginalText);
            } else {
                // Fallback if browser doesn't support .execCommand('copy')
                window.prompt("Copy Link form URL Bar", text);
            }
        }
    </script>


    <script>
        (function () {
            window.inputNumber = function (el) {
                var min = el.attr("min") || false;
                var max = el.attr("max") || false;

                var els = {};

                els.dec = el.prev();
                els.inc = el.next();

                el.each(function () {
                    init($(this));
                });

                function init(el) {
                    els.dec.on("click", decrement);
                    els.inc.on("click", increment);

                    function decrement() {
                        var value = el[0].value;
                        value--;
                        if (!min || value >= min) {
                            el[0].value = value;
                        }
                    }

                    function increment() {
                        var value = el[0].value;
                        value++;
                        if (!max || value <= max) {
                            el[0].value = value++;
                        }
                    }
                }
            };
        })();

        inputNumber($(".input-number"));
    </script>


    <script>
        $("#rateBox").rate({
            length: 5,
            value: {{ get_avarage_rating($product->ratings) }},
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });

        $("#rateBox1").rate({
            length: 5,
            value: 1,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
        $("#rateBox2").rate({
            length: 5,
            value: 2,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
        $("#rateBox3").rate({
            length: 5,
            value: 3,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
        $("#rateBox4").rate({
            length: 5,
            value: 4,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
        $("#rateBox5").rate({
            length: 5,
            value: 5,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
        $("#rateBoxMain").rate({
            length: 5,
            value: {{ get_avarage_rating($product->ratings) }},
            readonly: true,
            size: '20px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
    </script>



    <script>
        /*Size is  set in pixels... supports being written as: '250px' */
        var magnifierSize = 200;

        /*How many times magnification of image on page.*/
        var magnification = 4;

        function magnifier() {

        this.magnifyImg = function(ptr, magnification, magnifierSize) {
            var $pointer;
            if (typeof ptr == "string") {
            $pointer = $(ptr);
            } else if (typeof ptr == "object") {
            $pointer = ptr;
            }

            if(!($pointer.is('img'))){
            alert('Object must be image.');
            return false;
            }

            magnification = +(magnification);

            $pointer.hover(function() {
            $(this).css('cursor', 'none');
            $('.magnify').show();
            //Setting some variables for later use
            var width = $(this).width();
            var height = $(this).height();
            var src = $(this).attr('src');
            var imagePos = $(this).offset();
            var image = $(this);

            if (magnifierSize == undefined) {
                magnifierSize = '150px';
            }

            $('.magnify').css({
                'background-size': width * magnification + 'px ' + height * magnification + "px",
                'background-image': 'url("' + src + '")',
                'width': magnifierSize,
                'height': magnifierSize
            });

            //Setting a few more...
            var magnifyOffset = +($('.magnify').width() / 2);
            var rightSide = +(imagePos.left + $(this).width());
            var bottomSide = +(imagePos.top + $(this).height());

            $(document).mousemove(function(e) {
                if (e.pageX < +(imagePos.left - magnifyOffset / 6) || e.pageX > +(rightSide + magnifyOffset / 6) || e.pageY < +(imagePos.top - magnifyOffset / 6) || e.pageY > +(bottomSide + magnifyOffset / 6)) {
                $('.magnify').hide();
                $(document).unbind('mousemove');
                }
                var backgroundPos = "" - ((e.pageX - imagePos.left) * magnification - magnifyOffset) + "px " + -((e.pageY - imagePos.top) * magnification - magnifyOffset) + "px";
                $('.magnify').css({
                'left': e.pageX - magnifyOffset,
                'top': e.pageY - magnifyOffset,
                'background-position': backgroundPos
                });
            });
            }, function() {

            });
        };

        this.init = function() {
            $('body').prepend('<div class="magnify"></div>');
        }

        return this.init();
        }

        var magnify = new magnifier();
        magnify.magnifyImg('.magnifiedImg', magnification, magnifierSize);
    </script>
@endsection




