@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Search')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Search Result')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
    <style>
        .page-header {
            background: #ff084e none repeat scroll 0 0;
        }
        .gap2{
            padding-top: 0px;
        }

        .chosen-container-active .chosen-choices{
            box-shadow: none !important;
        }
        .chosen-container-multi .chosen-choices li.search-field input[type=text]{
            padding: 7px 10px !important;
            width: 100% !important;
        }
        .chosen-choices{
            height: 37px !important;
            border-radius: 5px !important;
            border: 1px solid #dddddd !important;
            padding: 10px !important;
            background-image: none !important;
        }
        .chosen-container .chosen-results li {
            padding: 10px 6px !important;
        }
        .chosen-container .chosen-results li.active-result.highlighted {
            background-image: none !important;
            background-color: #ff084e !important;
            color: #ffffff !important;
        }

        .chosen-container-single .chosen-single {
        box-shadow: none !important;
        height: 38px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 9px 10px !important;
        background-image: none !important;
        background: none !important;
    }
        .chosen-container-single .chosen-single div {
            position: absolute;
            top: 0px !important;
            right: 0;
            display: block;
            width: 18px;
            height: 100%;
        }
        .friend-block>figure img {
            border-radius: 5px !important;
        }

        .title-1-line{
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        a[class*="btn-shopinn"]{
            border-radius: 5px !important;
        }

        .box-shadow{
            -webkit-box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
            -moz-box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
            box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
        }

        .rating{
            position: absolute;
            top: 5px;
            right: 10px;
            font-weight: bold;
        }





        .gap2{
            padding-top: 0px;
        }
        .c-form {
            display: flex;
        }
        .c-form>div input, .c-form>div textarea, .c-form>input, .c-form>textarea {
            padding: 0px 10px;
        }
        .select2-container .select2-selection--single{
            height: 30px;
        }
        ..select2-container--default .select2-selection--single,
        .select2 select2-container.select2-container--default.select2-container--below.select2-container--focus,
        .select2-container--default .select2-selection--single{
            border: 1px solid #dee2e6 !important;
            outline: none !important;
        }
        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #dee2e6 !important;
            border-radius: 4px;
            outline: none !important;
        }
        .select2 select2-container.select2-container--default.select2-container--below,
        .select2-container{
            width: 100% !important;
        }

        /* .chosen-container.chosen-container-single{
            display: none !important;
        } */
        .select2-container--default .select2-results__option--selected,
        .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable{
            background-color: #ff084e;
            color: #ffffff;
        }
        .profile-menu>li>a.active::after{
            display: none !important;
        }

        .group-result{
            color: #ff084e;
        }
    </style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Featured Products') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('List of all ') }} <span class="text-bold">{{ __('Featured Products') }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>





<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="central-meta">
                        <div class="title-block">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="align-left m-t-3">
                                        <h5>{{ __('Total Featured Products') }} <span class="m-t-m-3 bg-shopinn">{{ __($products->count()) }}</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="row merged20">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <form method="get" action="#">
                                                <input type="text" id="search-products" placeholder="{{ __('Search Product') }}" />
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- title block -->


                    <div class="central-meta">
                        <div class="row merged20 remove-ext">
                            @foreach($products as $product)
                                <div class="col-md-3 col-sm-6 col-sx-6 item product link-div" onclick="location.href='{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($product->name)]) }}';">
                                    <div class="pitdate-user product-data">
                                        <figure>
                                            {{-- @if ($url != null)
                                                <img src="{{ $url }}" alt="Image Not Found">
                                            @else --}}
                                                <img src="{{ get_image($product, 'card') }}" alt="">
                                            {{-- @endif --}}

                                            <div class="likes heart" title="Rating">
                                                <i class="fa fa-star color-orange"></i>
                                                <span class="m-t-1">
                                                    <b>{{ get_avarage_rating($product->ratings) }}</b>
                                                </span>
                                            </div>
                                        </figure>

                                        <h4>
                                            <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($product->name)]) }}" class="title-1-line">{{ $product->name }}</a>
                                        </h4>

                                        <span class="text-shopinn text-bold">
                                            @if(offer_price($product)['isOffer'] == 1)
                                                    <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['price'] }}

                                                    <span class="discount fs-12px">
                                                        <span class="text-muted">(<del>{{ offer_price($product)['main'] }}<sup><i class="text-bold">৳</i></sup></del>)</span>
                                                        <span class="dis-percentage"><sup><i class="text-bold text-shopinn">-{{ offer_price($product)['offer'] }}%</i></sup></span>
                                                    </span>
                                                @else
                                                    <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['main'] }}
                                                @endif
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    // search
    <script>
        $(document).ready(function(){
            var searchProducts = $('#search-products'),
            products = document.querySelectorAll('.product'),
            productsData = document.querySelectorAll('.product-data'),
            searchVal;

            searchProducts.on('input', function() {
                searchVal = this.value.toLowerCase();

                for (var i = 0; i < products.length; i++) {
                    if(searchVal == '') {
                        products[i].style['display'] = 'flex';
                    }
                    else if (!searchVal || productsData[i].textContent.toLowerCase().indexOf(searchVal) > -1) {
                        products[i].style['display'] = 'flex';
                    }
                    else {
                        products[i].style['display'] = 'none';
                    }
                }
            });

        });
    </script>
@endsection
