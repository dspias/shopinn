@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, About')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'About')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .gap2.gray-bg{
        padding-bottom: 0px;
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 7px 10px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 10px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 0px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }
    .friend-block>figure img {
        border-radius: 0% !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('About') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('About ') }} <span class="text-bold">{{ __('ShopInn-BD') }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>



<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="central-meta">
                        <figure class="text-center">
                            {{--  @php
                                $logo = company_get('logo');
                                $url = ($logo == null) ? asset('assets/images/others/img-10.jpg'):url(env('IMG_STORE').$logo)
                            @endphp
                            <img src="{{ $url }}" alt="">  --}}
                            <img src="{{ asset('vendor/images/logo/logo.png') }}" alt="">
                        </figure>
                        <p class="text-justify">
                            {!! company_get('about') !!}
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="widget">
                    <div class="foot-logo">
                        <div class="logo">
                            {{-- <a href="/"><img src="{{ $url }}" alt=""></a> --}}
                            <a href="/"><img src="{{ asset('vendor/images/logo/logo.png') }}" alt=""></a>
                        </div>
                        <p>
                            {{ company_get('short_note') }}
                        </p>
                    </div>
                    <ul class="location">
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <p>{{ company_get('address') }}</p>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <p>{{ company_get('mobile') }}</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <div class="widget">
                    <div class="widget-title"><h4>{{ __('follow') }}</h4></div>
                    <ul class="list-style">
                        <li>
                            <i class="fa fa-facebook-square"></i>
                            <a href="{{ company_get('facebook_page') }}" title="">{{ __('facebook') }}</a>
                        </li>
                        <li>
                            <i class="fa fa-twitter-square"></i>
                            <a href="{{ company_get('twitter') }}" title="">{{ __('twitter') }}</a>
                        </li>
                        <li>
                            <i class="fa fa-instagram"></i>
                            <a href="{{ company_get('instagram') }}" title="">{{ __('instagram') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <div class="widget">
                    <div class="widget-title"><h4>{{ __('Navigate') }}</h4></div>
                    <ul class="list-style">
                        <li><a href="{{ route('guest.homepage.about') }}">{{ __('about us') }}</a></li>
                        <li><a href="{{ route('guest.contact.index') }}">{{ __('contact us') }}</a></li>
                        <li><a href="{{ route('guest.terms.index') }}">{{ __('terms & Conditions') }}</a></li>
                        <li><a href="{{ route('guest.faq.index') }}">{{ __('FAQ\'s') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <div class="widget">
                    <div class="widget-title"><h4>{{ __('useful links') }}</h4></div>
                    <ul class="list-style">
                        <li><a href="https://blogs.shopinnbd.com">{{ __('Shopinn Blogs') }}</a></li>
                        <li><a href="{{ route('guest.shop.index') }}">{{ __('Shopinn Shops') }}</a></li>
                        <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <div class="widget">
                    <div class="widget-title"><h4>{{ __('download apps') }}</h4></div>
                    <ul class="colla-apps">
                        <li><a href="javascript:void(0)" title=""><i class="fa fa-android"></i>{{ __('android') }}</a></li>
                        <li><a href="javascript:void(0)" title=""><i class="ti-apple"></i>{{ __('iPhone') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer -->

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>

    </script>
@endsection
