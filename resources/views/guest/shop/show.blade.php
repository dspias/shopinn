@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shop Details')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Profile')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @livewire('guest.shop-details', ['shop' => $shop])
        </div>
    </div>
</div>
@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
@endsection




