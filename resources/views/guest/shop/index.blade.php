@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shops')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || All Shops')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 7px 10px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 10px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 0px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }
    .friend-block>figure img {
        border-radius: 0% !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('All Shops') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Shop List of ') }} <span class="text-bold">ShopInn-BD</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>



<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="central-meta">
                        <div class="title-block">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="align-left m-t-3">
                                        <h5>{{ __('Total Shops') }} <span class="m-t-m-3 bg-shopinn">{{ __($shops->count()) }}</span></h5>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row merged20">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <form method="get" action="#">
                                                <input type="text" id="search-shops" placeholder="{{ __('Search Shops') }}" />
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </form>
                                        </div>
                                        {{-- <div class="col-lg-5 col-md-5 col-sm-5">
                                            <div class="select-options">
                                                <select class="select">
                                                    <option value="all">{{ __('All Products') }}</option>
                                                    <option value="review">{{ __('Under Review Products') }}</option>
                                                    <option value="featured">{{ __('Featured Products') }}</option>
                                                    <option value="active">{{ __('Active Products') }}</option>
                                                    <option value="inactive">{{ __('Inactive Products') }}</option>
                                                </select>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- title block -->


                    <div class="central-meta padding30">
                        <div class="row">
                            @foreach ($shops as $shop)
                                <div class="col-lg-3 col-md-6 col-sm-6 shop">
                                    <div class="friend-box shop-data">
                                        @php
                                            $logo = get_logo($shop, 'thumb');
                                            if($logo == null) $logo = asset('vendor/images/resources/frnd-figure1.jpg');

                                            $cover = get_cover($shop, 'card');
                                            if($cover == null) $cover = asset('vendor/images/resources/frnd-cover1.jpg');
                                        @endphp
                                        <figure>
                                            <img src="{{ $cover }}" alt="Image Not Found">
                                        </figure>
                                        <div class="frnd-meta">
                                            <img src="{{ $logo }}" style="max-width: 85px;" alt="Image Not Found">
                                            <div class="frnd-name">
                                                <a href="{{ route('guest.shop.show', ['shop_id' => $shop->id,'shop_name' => get_name($shop->name)]) }}" class="title-1-line">{{ __($shop->name) }}</a>
                                                <span class="text-dark title-1-line">{!! __(optional($shop->shop)->shop_address) !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    // search
    <script>
        $(document).ready(function(){
            var searchProducts = $('#search-shops'),
            products = document.querySelectorAll('.shop'),
            productsData = document.querySelectorAll('.shop-data'),
            searchVal;

            searchProducts.on('input', function() {
                searchVal = this.value.toLowerCase();

                for (var i = 0; i < products.length; i++) {
                    if(searchVal == '') {
                        products[i].style['display'] = 'flex';
                    }
                    else if (!searchVal || productsData[i].textContent.toLowerCase().indexOf(searchVal) > -1) {
                        products[i].style['display'] = 'flex';
                    }
                    else {
                        products[i].style['display'] = 'none';
                    }
                }
              });

        });
    </script>
@endsection
