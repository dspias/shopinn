@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Search')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Search Result')

{{-- page src stylesheets here --}}
@section('page_src_styles')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 7px 10px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 10px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single {
    box-shadow: none !important;
    height: 40px !important;
    border-radius: 5px !important;
    border: 1px solid #dddddd !important;
    padding: 8px 10px !important;
    background-image: none !important;
    background: none !important;
}
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 0px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }
    .friend-block>figure img {
        border-radius: 5px !important;
    }

    .title-1-line{
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    
    a[class*="btn-shopinn"]{
        border-radius: 5px !important;
    }

    .box-shadow{
        -webkit-box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
        -moz-box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
        box-shadow: 0px 0px 20px -13px rgba(0,0,0,0.50);
    }

    .rating{
        position: absolute;
        top: 5px;
        right: 10px;
        font-weight: bold;
    }




    
    .gap2{
        padding-top: 0px;
    }
    .c-form {
        display: flex;
    }
    .c-form>div input, .c-form>div textarea, .c-form>input, .c-form>textarea {
        padding: 0px 10px;
    }
    .select2-container .select2-selection--single{
        height: 30px;
    }
    ..select2-container--default .select2-selection--single,
    .select2 select2-container.select2-container--default.select2-container--below.select2-container--focus,
    .select2-container--default .select2-selection--single{
        border: 1px solid #dee2e6 !important;
        outline: none !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #dee2e6 !important;
        border-radius: 4px;
        outline: none !important;
    }
    .select2 select2-container.select2-container--default.select2-container--below,
    .select2-container{
        width: 100% !important;
    }

    /* .chosen-container.chosen-container-single{
        display: none !important;
    } */
    .select2-container--default .select2-results__option--selected,
    .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable{
        background-color: #ff084e;
        color: #ffffff;
    }
    .profile-menu>li>a.active::after{
        display: none !important;
    }

    .group-result{
        color: #ff084e;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            {{-- <h1 class="text-bold text-uppercase text-white">{{ __('Result') }}</h1> --}}
            <h4 class="text-capitalize text-white p-b-50">{{ __('Search Result of ') }} <span class="text-bold">{{ $key }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>

@livewire('guest.search', ['key' => $key])

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        
    </script>
@endsection