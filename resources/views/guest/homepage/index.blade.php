@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Homepage')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Home')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('vendor/starrating/rate.css') }}">
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .fixed-sidebar{
        z-index: 99;
    }
    .classic-pst-meta {
        max-width: 80%;
    }
    #totalRating {
        position: absolute;
        bottom: 25px;
        left: 260px;
        color: red;
        font-weight: bold;
    }

    .we-video-info>ul{
        margin-top: 0px;
        width: auto;
    }
    .btn-shopinn-solid,
    .btn-shopinn-solid:hover {
        background-color: #ff084e !important;
        border: 1px solid #ff084e !important;
        color: #ffffff !important;
        transition: 0.3s all ease-in-out;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row merged20" id="page-contents">

                <div class="col-md-12">
                    <div class="central-meta p-0">
                        @php
                            $sliders = ads(1920, 650);
                        @endphp
                        <div class="slick-single">
                            @foreach((array) $sliders as  $slider)
                            <a href="{{ $slider->url_link }}">
                                <img src="{{ image($slider->ad_image) }}" class="img-fluid" alt="1920px * 650px">
                            </a>
                            @endforeach
                            {{--  <a href="#">
                                <img src="{{ asset('vendor/images/resources/pitpoint-feature.jpg') }}" class="img-fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="{{ asset('vendor/images/resources/pitpoint-feature.jpg') }}" class="img-fluid" alt="">
                            </a>  --}}
                        </div>
                    </div>
                </div>

                @include('layouts.guest.partials.shortcut_menu')


                <div class="col-lg-3">
                    <aside class="sidebar static left">
                        {{-- Categories --}}
                        <div class="widget">
                        {{-- <div class="widget stick-widget"> --}}
                            <h4 class="widget-title">{{ __('Categories') }}</h4>
                            <ul class="naves type-category">
                                @foreach ($types as $key => $type)
                                    <li>
                                        <a data-toggle="collapse" href="#collapseExample_{{ $key.$type->id }}" role="button" aria-expanded="false" class="title-1-line" aria-controls="collapseExample_{{ $key.$type->id }}">
                                            {{ __($type->name) }}
                                        </a>
                                        <i class="ti-arrow-down text-right float-right m-t-m-15" style="font-size: 10px;" data-toggle="collapse" href="#collapseExample_{{ $key.$type->id }}" role="button" aria-expanded="false" aria-controls="collapseExample_{{ $key.$type->id }}"></i>

                                        <div class="collapse m-t-10" id="collapseExample_{{ $key.$type->id }}">
                                            <ul class="naves">
                                                @foreach ($type->categories as $category)
                                                    <li style="margin-bottom: 5px;">
                                                        <a href="{{ route('guest.homepage.category', ['cat_id' => encrypt($category->id)]) }}" class="title-1-line">{{ __($category->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        {{-- Categories Ends --}}


                        {{-- Latest 6 Posts --}}
                        <div class="widget">
                            <h4 class="widget-title">{{ __('Latest 6 Products') }}</h4>
                            <ul class="recent-photos">
                                @foreach ($latest as $item)
                                    <li>
                                        {{-- <a href="#" title=""><img src="{{ asset('vendor/images/resources/classic-new1.jpg') }}" alt=""></a> --}}
                                        <a href="{{ route('guest.product.show', ['product_id'=> $item->id, 'product_name' => get_name($item->name)]) }}">
                                            <img src="{{ get_image($item, 'thumb') }}" alt="Image Not Found">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        {{-- Latest 6 Posts Ends --}}


                        {{-- Ad Banner --}}
                        <div class="advertisment-box">
                            @php
                                $ad1 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad1))
                            <h4 class="">{{ __('advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ image($ad1->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        {{-- Ad Banner Ends --}}


                        {{-- Top 6 Shops --}}
                        <div class="widget stick-widget">
                            <h4 class="widget-title">{{ __('Latest 6 Shops') }}</h4>
                            <ul class="recent-photos">
                                @foreach($shops as $shop)
                                @php
                                    $logo = get_logo($shop, 'thumb');
                                    if($logo == null) continue;
                                @endphp
                                    <li>
                                        <a href="{{ route('guest.shop.show', ['shop_id' => $shop->id, 'shop_name' => get_name($shop->name)]) }}" title="{{ $shop->name }}"><img src="{{ $logo }}" alt=""></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        {{-- Top 6 Shops Ends --}}
                    </aside>
                </div><!-- sidebar -->


                <div class="col-lg-6">
                    {{-- Featured Post Starts --}}
                    <div class="central-meta">
                        <h6 class="create-post">{{ __('Featured Products') }}</h6>
                        <div class="slick-autoplay">
                            @foreach ($features as $item)
                                <a href="{{ route('guest.product.show', ['product_id'=> $item->id, 'product_name' => get_name($item->name)]) }}" class="slick-slide-item">
                                    {{-- <img src="{{ asset('vendor/images/resources/slick1.jpg') }}" class="img-fluid" alt="Image Not Found"> --}}
                                    <img src="{{ get_image($item, 'slider') }}" class="img-fluid" alt="Image Not Found">
                                </a>
                            @endforeach
                        </div>
                    </div>
                    {{-- Featured Post End --}}



                    {{-- Posts Starts --}}
                    @foreach ($products as $product)
                        <div class="central-meta item p-b-10">
                            <div class="user-post">
                                <div class="friend-info">
                                    @php
                                        $url = get_logo($product->shop, 'thumb');
                                        if($url == null) $url = asset('vendor/images/resources/nearly1.jpg');
                                    @endphp
                                    <figure>
                                        <img src="{{ $url }}" alt="" />
                                    </figure>
                                    <div class="friend-name">
                                        @auth
                                            @if ($product->shop_id == auth()->user()->id)
                                                <div class="more">
                                                    <div class="more-post-optns">
                                                        <i class="ti-more-alt"></i>
                                                        <ul>
                                                            <li><i class="fa fa-pencil-square-o"></i><a href="{{ route('shop.product.edit', ['product_id' => encrypt($product->id)]) }}">{{ __('Edit') }}</a></li>
                                                            <li>
                                                                <i class="fa fa-trash-o"></i><a href="{{ route('shop.product.change_status', ['product_id' => encrypt($product->id)]) }}" class="text-danger confirmation">{{ __('Deactive') }}</a>
                                                            </li>
                                                            <li><i class="fa fa-wpexplorer"></i><a href="{{ route('shop.product.change_featured_status', ['product_id' => encrypt($product->id)]) }}">{{ __('Change Feature') }}</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
                                        @endauth

                                        <ins><a href="{{ route('guest.shop.show', ['shop_id' => $product->shop->id,'shop_name' => get_name($product->shop->name)]) }}">{{ $product->shop->name }}</a></ins>
                                        {{-- <span><i class="ti-timer"></i> {{ get_date($product->approved_at, 'd M Y (h:i:A)') }}</span> --}}
                                        {{-- <span><i class="ti-timer"></i> {{ ago_time($product->approved_at) }}</span> --}}
                                    </div>
                                    <div class="post-meta">
                                        <figure onclick="window.location.href='{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}'" style="cursor: pointer;">
                                            <img src="{{ get_image($product, 'timeline') }}" alt="448px * 224px" />
                                        </figure>
                                        <div class="description">
                                            <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="learnmore btn-shopinn-solid" data-ripple="" style="left: 20px; width: fit-content;">
                                                <i class="fa fa-star text-bold"> {{ get_avarage_rating($product->ratings) }}</i>
                                            </a>

                                            <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="learnmore btn-shopinn-solid" data-ripple="">{{ __('More Details') }}</a>


                                            <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="h5 text-dark d-inline-block float-left title-1-line">{{ $product->name }}</a>

                                            <h4 class="text-bold text-shopinn d-inline-block float-right m-t-0">
                                                @if(offer_price($product)['isOffer'] == 1)
                                                    <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['price'] }}

                                                    <span class="discount fs-12px">
                                                        <span class="text-muted">(<del>{{ offer_price($product)['main'] }}<sup><i class="text-bold">৳</i></sup></del>)</span>
                                                        <span class="dis-percentage"><sup><i class="text-bold text-shopinn">-{{ offer_price($product)['offer'] }}%</i></sup></span>
                                                    </span>
                                                @else
                                                    <sup><i class="text-shopinn text-bold">৳</i></sup>{{ offer_price($product)['main'] }}
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="we-video-info m-t-20">
                                            <ul class="m-t-0">
                                                <li>
                                                    <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="btn btn-shopinn-outline btn-sm">{{ __('Buy Now') }}</a>
                                                </li>
                                            </ul>
                                            <div class="users-thumb-list text-right">
                                                @if ($product->sizes->count() > 0 || $product->colors->count() > 0)
                                                    <a href="{{ route('guest.product.show', ['product_id' => $product->id,'product_name' => get_name($product->name)]) }}" class="btn btn-shopinn btn-sm" data-ripple="">{{ __('Add To Cart') }}</a>
                                                @else
                                                    @livewire('customer.add-to-cart', ['product' => $product])
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- digital sponsors -->

                    <div class="col-md-12 text-center">
                        {{ $products->links() }}
                    </div>

                    {{-- Posts Ends --}}
                </div><!-- centerl meta -->


                <div class="col-lg-3">
                    <aside class="sidebar static right">

                        {{-- Facebook Likes --}}
                        <div class="fb-like" data-href="https://www.facebook.com/shopinnbd" data-width="" data-layout="standard" data-action="like" data-size="large" data-share="true"></div>
                        {{-- <div class="fb-page" data-href="https://www.facebook.com/shopinnbd/"  data-width="500" data-height="300" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shopinnbd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shopinnbd/">{{ __('Shopinn-BD') }}</a></blockquote></div> --}}
                        {{-- Facebook Likes Ends --}}

                        {{-- Ad Banner --}}
                        <div class="advertisment-box">
                            @php
                                $ad2 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad2))
                            <h4 class="">{{ __('advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ image($ad2->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        {{-- Ad Banner Ends --}}


                        {{-- Ad Banner --}}
                        <div class="advertisment-box">
                            @php
                                $ad3 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad3))
                            <h4 class="">{{ __('advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ image($ad3->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        {{-- Ad Banner Ends --}}


                        {{-- ShopInn Intro --}}
                        <div class="widget stick-widget">
                            <h4 class="widget-title">{{ __('ShopInn Intro') }}</h4>
                            <ul class="short-profile">
                                <li>
                                    <span>{{ __('About ShopInn') }}</span>
                                    <span class="text-justify" style="text-align: justify;">
                                        {{  company_get('short_note')  }}
                                    </span>
                                </li>

                                <li>
                                    <span>{{ __('ShopInn Contact') }}</span>

                                    <ul class="p-l-0">
                                        <li>
                                            <i class="ti-email"></i>
                                            <a href="mailto:{{ company_get('email') }}" class="text-lowercase">{{ company_get('email') }}</a>
                                        </li>
                                        <li>
                                            <i class="ti-mobile"></i>
                                            <a href="tel:{{ company_get('mobile') }}" class="text-uppercase">{{ company_get('mobile') }}</a>
                                        </li>
                                        <li>
                                            <i class="ti-location-pin"></i>
                                            <a href="javascript:void(0);" class="text-uppercase">{{ company_get('address') }}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="share">
                                        @if (!is_null(company_get('facebook_page')))
                                            <a href="{{ company_get('facebook_page') }}" target="_blank" data-toggle="tooltip" data-original-title="{{ __('Shopinn Official Facebook Page') }}">
                                                <i class="fa fa-facebook border p-10 p-l-13 p-r-13"></i>
                                            </a>
                                        @endif
                                        @if (!is_null(company_get('facebook_group')))
                                            <a href="{{ company_get('facebook_group') }}" target="_blank" data-toggle="tooltip" data-original-title="{{ __('Shopinn Official Facebook Group') }}">
                                                <i class="fa fa-facebook border p-10 p-l-13 p-r-13"></i>
                                            </a>
                                        @endif
                                        @if (!is_null(company_get('instagram')))
                                            <a href="{{ company_get('instagram') }}" target="_blank" data-toggle="tooltip" data-original-title="{{ __('Shopinn Official Instagram Account') }}">
                                                <i class="fa fa-instagram border p-10"></i>
                                            </a>
                                        @endif
                                        @if (!is_null(company_get('twitter')))
                                            <a href="{{ company_get('twitter') }}" target="_blank" data-toggle="tooltip" data-original-title="{{ __('Shopinn Official Twitter Profile') }}">
                                                <i class="fa fa-twitter border p-10"></i>
                                            </a>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {{-- ShopInn Intro Ends --}}
                    </aside>
                </div><!-- sidebar -->

            </div>
        </div>
    </div>
</div>
@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('vendor/starrating/rate.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $("#rateBox").rate({
            text: false,
            length: 5,
            value: 3.7,
            readonly: true,
            size: '14px',
            selectClass: 'fxss_rate_select',
            incompleteClass: 'fxss_rate_no_all_select',
            customClass: 'custom_class',
            callback: function(object){
                //console.log(object.index)
            }
        });
    </script>


    <script language="javascript" type="text/javascript">
        function rudr_favorite(a) {
            pageTitle=document.title;
            pageURL=document.location;
            try {
                // Internet Explorer solution
                eval("window.external.AddFa-vorite(pageURL, pageTitle)".replace(/-/g,''));
            }
            catch (e) {
                try {
                    // Mozilla Firefox solution
                    window.sidebar.addPanel(pageTitle, pageURL, "");
                }
                catch (e) {
                    // Opera solution
                    if (typeof(opera)=="object") {
                        a.rel="sidebar";
                        a.title=pageTitle;
                        a.url=pageURL;
                        return true;
                    } else {
                        // The rest browsers (i.e Chrome, Safari)
                        alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
                    }
                }
            }
            return false;
        }
    </script>
@endsection




