@extends('layouts.guest.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, FAQ')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'FAQ')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 7px 10px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 10px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 0px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }
    .friend-block>figure img {
        border-radius: 0% !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('All FAQ\'') }}<small>{{ __('s') }}</small></h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('FAQ of ') }} <span class="text-bold">{{ __('ShopInn-BD') }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>
                


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="faq-area">
                        <div class="accordion" id="accordion">

                            @foreach((array)$faqs as $sl => $faq)
                                <div class="card">
                                    <div class="card-header" id="{{ 'heading_id_'.$sl }}">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_id_{{ $sl }}" aria-expanded="false" aria-controls="collapse_id_{{ $sl }}">
                                                {{ $faq['title'] }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse_id_{{ $sl }}" class="collapse" aria-labelledby="{{ 'heading_id_'.$sl }}" data-parent="#accordion">
                                        <div class="card-body">
                                            {{ $faq['description'] }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        
    </script>
@endsection