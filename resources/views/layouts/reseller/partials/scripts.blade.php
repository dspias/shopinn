 {{-- <script src="{{ asset('js/app.js') }}"></script>  --}}

<script src="{{ asset('vendor/js/main.min.js') }}"></script>
<script src="{{ asset('vendor/js/main.js') }}"></script>
<script src="{{ asset('assets/js/confirmation_alert/jquery-confirmv3.3.2.min.js') }}"></script>



{{-- Livewire JS --}}
@livewireScripts

@yield('page_src_scripts')

<script src="{{ asset('vendor/js/script.js') }}"></script>



@yield('page_scripts')
