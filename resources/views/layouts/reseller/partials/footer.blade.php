@livewire('reseller.my-cart')

<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">{{ __('Copyright') }} &copy;<script>document.write(new Date().getFullYear());</script> {{ __('Developer By') }} <a href="{{ 'http://shopinnbd.com/' }}" target="_blank" class="text-shopinn text-bold">{{ __('ShopInnBD') }}</a></span>
                <i><img src="{{ asset('vendor/images/credit-cards.png') }}"></i>
            </div>
        </div>
    </div>
</div>