<div class="fixed-sidebar left">
    <div class="menu-left">
        <ul class="left-menu">
            <li>
                <a class="menu-small" href="javascript:void(0);" title="">
                    <i class="ti-menu"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('reseller.dashboard.index') }}" title="{{ __('Dashboard') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-home"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('reseller.order.all') }}" title="{{ __('All Orders') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-announcement"></i>
                </a>
            </li>

            {{-- <li>
                <a href="#" title="{{ __('Payment Reports') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-credit-card"></i>
                </a>
            </li> --}}

            <li>
                <a href="{{ route('reseller.product.my') }}" title="{{ __('My Products') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-shopping-cart-full"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('reseller.cart.index') }}" title="{{ __('My Cart') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-shopping-cart"></i>
                </a>
            </li>
        </ul>
    </div>


    {{-- main menu left side --}}
    <div class="left-menu-full">
        <ul class="menu-slide">
            <li><a class="closd-f-menu" href="javascript:void(0);" title=""><i class="ti-close"></i> {{ __('Close Menu') }}</a></li>
        </ul>

        <ul class="menu-slide">
            <li>
                <a class="" href="{{ route('reseller.dashboard.index') }}" title="{{ __('Dashboard') }}"><i class="ti-home"></i> {{ __('Dashboard') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('reseller.product.my') }}" title="{{ __('My Products') }}"><i class="ti-shopping-cart-full"></i> {{ __('My Products') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('reseller.order.all') }}" title="{{ __('My Orders') }}"><i class="ti-announcement"></i> {{ __('My Orders') }}</a>
            </li>
{{--
            <li>
                <a class="" href="#" title="{{ __('Payments') }}"><i class="ti-money"></i> {{ __('Payments') }}</a>
            </li> --}}

            <li>
                <a class="" href="{{ route('reseller.cart.index') }}" title="{{ __('My Cart') }}"><i class="ti-shopping-cart"></i> {{ __('My Cart') }}</a>
            </li>
        </ul>
    </div>
</div><!-- left sidebar menu -->
