<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="@yield('meta_page_description')"/>
	<meta name="keywords" content="@yield('meta_page_keywords')"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title>ShopinnBD || @yield('page_name')</title>

	<link rel="shortcut icon" href="{{ asset('assets/images/logo/favicon.png') }}" type="image/png" sizes="16x16" />
	
	@include('layouts.shop.partials.stylesheet')

</head>
<body>
<div class="se-pre-con"></div>
<div class="theme-layout gray-bg">
	
	<div class="postoverlay"></div>

	@include('layouts.shop.partials.topnav')

	@include('layouts.shop.partials.sidebarleft')
	
	<section>
		<div class="gap2 gray-bg">
			@yield('content')
		</div>	 
	</section><!-- content -->
	
	@include('layouts.shop.partials.footer')
	
	@yield('page_popup')

	@include('layouts.shop.partials.scripts')
                
	{{-- //added sweetalert --}}
	@include('sweetalert::alert')
</div>
	

	
<!-- Messenger Chat Plugin Code -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
	FB.init({
	  xfbml            : true,
	  version          : 'v10.0'
	});
  };

  (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
	fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
  attribution="biz_inbox"
  page_id="112846536824962">
</div>
</body>	
</html>