
<div class="user-profile">
    {{-- <figure>
        <img src="{{ asset('vendor/images/resources/shop-topbg.jpg') }}" alt="">									
        <ul class="profile-controls">
            <li><a href="#" title="Like" data-toggle="tooltip"><i class="fa fa-thumbs-up"></i></a></li>
            <li><a href="#" title="Follow" data-toggle="tooltip"><i class="fa fa-star"></i></a></li>
            <li><a class="send-mesg" href="#" title="Send Message" data-toggle="tooltip"><i class="fa fa-comment"></i></a></li>
            <li>
                <div class="edit-seting" title="Edit Profile image" data-toggle="tooltip"><i class="fa fa-sliders"></i>
                    <ul class="more-dropdown">
                        <li>
                            <a class="share-pst" href="#" title="">Share this Page</a>
                        </li>
                        <li>
                            <a href="setting.html" title="">Update Profile Photo</a>
                        </li>
                        <li>
                            <a href="setting.html">Update Header Photo</a>
                        </li>
                        <li>
                            <a href="setting.html" title="" data-toggle="tooltip">Account Settings</a>
                        </li>
                        <li>
                            <a href="timeline-friends2.html" title="" >Invite Friends</a>
                        </li>
                        <li>
                            <a href="support-and-help.html" title="">Find support</a>
                        </li>
                        <li>
                            <a href="setting.html" title="">Block Page</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </figure> --}}
    <div class="profile-section">
        <div class="row">
            {{-- <div class="col-lg-2">
                <div class="profile-author">
                    <a class="profile-author-thumb" href="02-ProfilePage.html">
                        <img alt="author" src="{{ asset('vendor/images/resources/shop-dp.jpg') }}">
                    </a>
                    <div class="author-content">
                        <a class="h4 author-name" href="02-ProfilePage.html">{{ auth()->user()->name }}</a>
                    </div>
                </div>
            </div> --}}
            <div class="col-lg-12">
                <ul class="profile-menu">
                    @yield('sub_sections')
                </ul>
            </div>
        </div>
    </div>	
</div><!-- user profile banner  -->