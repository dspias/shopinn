<div class="fixed-sidebar left">
    <div class="menu-left">
        <ul class="left-menu">
            <li>
                <a class="menu-small" href="javascript:void(0);" title="">
                    <i class="ti-menu"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('shop.dashboard.index') }}" title="{{ __('Dashboard') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-home"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('shop.order.new') }}" title="{{ __('New Orders') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-announcement"></i>
                </a>
            </li>

            {{-- <li>
                <a href="{{ route('shop.payment.index') }}" title="{{ __('Payment Reports') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-credit-card"></i>
                </a>
            </li> --}}

            <li>
                <a href="{{ route('shop.product.index') }}" title="{{ __('My Products') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-shopping-cart"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('shop.product.featured') }}" title="{{ __('Featured Products') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-heart"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('shop.setting.offer.index') }}" title="{{ __('Offers') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-gift"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('shop.setting.coupon.index') }}" title="{{ __('Coupons') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-light-bulb"></i>
                </a>
            </li>
        </ul>
    </div>


    {{-- main menu left side --}}
    <div class="left-menu-full">
        <ul class="menu-slide">
            <li><a class="closd-f-menu" href="javascript:void(0);" title=""><i class="ti-close"></i> {{ __('Close Menu') }}</a></li>
        </ul>

        <ul class="menu-slide">
            <li>
                <a class="" href="{{ route('shop.dashboard.index') }}" title="{{ __('Dashboard') }}"><i class="ti-home"></i> {{ __('Dashboard') }}</a>
            </li>

            {{-- <li>
                <a class="" href="{{ route('shop.payment.index') }}" title="{{ __('Payments') }}"><i class="ti-money"></i> {{ __('Payments') }}</a>
            </li> --}}

            <li  class="menu-item-has-children">
                <a class="" href="javascript:void(0);" title="{{ __('Orders') }}"><i class="ti-announcement"></i> {{ __('Orders') }}</a>
                <ul class="submenu">
                    <li><a href="{{ route('shop.order.index') }}" title="{{ __('All Orders') }}">{{ __('All Orders') }}</a></li>
                    <li><a href="{{ route('shop.order.new') }}" title="{{ __('New Orders') }}">{{ __('New Orders') }}</a></li>
                    <li><a href="{{ route('shop.order.running') }}" title="{{ __('Running Orders') }}">{{ __('Running Orders') }}</a></li>
                    <li><a href="{{ route('shop.order.completed') }}" title="{{ __('Completed Orders') }}">{{ __('Completed Orders') }}</a></li>
                    <li><a href="{{ route('shop.order.canceled') }}" title="{{ __('Canceled Orders') }}">{{ __('Canceled Orders') }}</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="javascript:void(0);" title="{{ __('Products') }}"><i class="ti-shopping-cart"></i> {{ __('Products') }}</a>
                <ul class="submenu">
                    <li><a href="{{ route('shop.product.index') }}" title="{{ __('All Products') }}">{{ __('All Products') }}</a></li>
                    <li><a href="{{ route('shop.product.review') }}" title="{{ __('Under Review') }}">{{ __('Under Review') }}</a></li>
                    <li><a href="{{ route('shop.product.featured') }}" title="{{ __('Featured Products') }}">{{ __('Featured Products') }}</a></li>
                    <li><a href="{{ route('shop.product.active') }}" title="{{ __('Active Products') }}">{{ __('Active Products') }}</a></li>
                    <li><a href="{{ route('shop.product.inactive') }}" title="{{ __('Inactive Products') }}">{{ __('Inactive Products') }}</a></li>
                    <li><a href="{{ route('shop.product.create') }}" title="{{ __('Add New Product') }}">{{ __('Add New Product') }}</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="javascript:void(0);" title="{{ __('Settings') }}"><i class="ti-settings"></i> {{ __('Settings') }}</a>
                <ul class="submenu">
                    <li><a href="{{ route('shop.setting.shop.index') }}" title="{{ __('Shop') }}">{{ __('Shop') }}</a></li>
                    <li><a href="{{ route('shop.setting.package.index') }}" title="{{ __('Packages') }}">{{ __('Packages') }}</a></li>
                    <li><a href="{{ route('shop.setting.advertise.index') }}" title="{{ __('Advertises') }}">{{ __('Advertises') }}</a></li>
                    <li><a href="{{ route('shop.setting.offer.index') }}" title="{{ __('Offers') }}">{{ __('Offers') }}</a></li>
                    <li><a href="{{ route('shop.setting.coupon.index') }}" title="{{ __('Coupons') }}">{{ __('Coupons') }}</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div><!-- left sidebar menu -->
