{{--  <script src="{{ asset('vendor/js/main.min.js') }}"></script>  --}}
<script src="{{ asset('vendor/js/main.js') }}"></script>
<script src="{{ asset('assets/js/confirmation_alert/jquery-confirmv3.3.2.min.js') }}"></script>
@yield('page_src_scripts')

{{-- Livewire JS --}}
{{-- @livewireScripts --}}
@livewireScripts(['base_url' => ENV('APP_URL')])

<script src="{{ asset('vendor/js/script.js') }}"></script>



@yield('page_scripts')