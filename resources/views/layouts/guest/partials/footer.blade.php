@if(Auth::check() && auth()->user()->role_id == 3)
@livewire('reseller.my-cart')
@else
    @livewire('customer.my-cart')
@endif

<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Developer By <a href="{{ 'http://shopinnbd.com/' }}" target="_blank" class="text-shopinn text-bold">ShopInnBD</a></span>
                {{-- <i><img src="{{ asset('vendor/images/credit-cards.png') }}" alt=""></i> --}}
            </div>
        </div>
    </div>
</div>