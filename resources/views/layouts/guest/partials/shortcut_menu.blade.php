<div class="col-lg-12">
    <div class="gap2 no-top p-b-20">
        <div class="central-meta no-margin">
            <ul class="nave-area">
                <li><a href="{{ route('guest.homepage.index') }}"><i class="ti-home"></i> {{ __('Home') }}</a></li>
                <li><a href="{{ route('guest.shop.index') }}"><i class="ti-shopping-cart"></i> {{ __('Shops') }}</a></li>
                {{-- <li><a href="#"><i class="ti-bag"></i> {{ __('Resellers') }}</a></li> --}}
                <li><a href="{{ route('guest.product.featured') }}"><i class="ti-shine"></i> {{ __('Featured Products') }}</a></li>

                {{-- <li><a href="#"><i class="fa fa-braille"></i> Categories</a>
                    <ul class="mega-menu">                        
                        <li>
                            <a href="#" title="New Category" data-toggle="tooltip">
                                <i class="fa fa-star"></i>
                                <span>Cat-01</span>
                            </a>
                        </li>
                    </ul>
                </li> --}}

                <li><a href="http://blogs.shopinnbd.com" target="_blank"><i class="ti-book"></i> {{ __('Blog') }}</a></li>
                <li><a href="{{ route('guest.homepage.about') }}"><i class="ti-info"></i>{{ __('About') }}</a></li>
                <li><a href="{{ route('guest.contact.index') }}"><i class="ti-comments"></i> {{ __('Contact') }}</a></li>
            </ul>
            <ul class="align-right user-ben">
                <li class="search-for">
                    <a data-ripple="" class="circle-btn search-data" title="Search For Post" data-toggle="tooltip" href="#"><i class="ti-search"></i></a>
                    <form action="{{ route('guest.homepage.search') }}" class="searchees" method="get">
                        <span class="cancel-search"><i class="ti-close"></i></span>
                        <input type="text" name="keyword" placeholder="{{ __('Search anything') }}">
                        <button type="submit"></button>
                    </form>
                </li>
                
                {{-- @auth
                    <li><a href="#" class="main-btn create-pst" data-ripple="">Add New Post</a></li>
                @endauth --}}
            </ul>
        </div>
    </div>	
</div><!-- nave list -->