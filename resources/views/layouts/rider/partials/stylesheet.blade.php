<link rel="stylesheet" href="{{ asset('vendor/css/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/weather-icons.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/confirmation_alert/jquery-confirmv3.3.2.min.css') }}">

@yield('page_src_styles')

<link rel="stylesheet" href="{{ asset('vendor/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/color.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/responsive.css') }}">

<link rel="stylesheet" href="{{ asset('vendor/css/custom.css') }}">

@yield('page_styles')