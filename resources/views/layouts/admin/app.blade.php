<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    {{-- CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="This Administration Built for ShopInn" />
	<meta name="keywords" content="ShopInn" />

    {{--  Page Title  --}}
    <title> ShopInn | ADMIN | @yield('page_title') </title>


    @include('layouts.admin.partials.stylesheet')
</head>

<body>
    <div class="app">
        <div class="layout">
            {{-- Side Nav --}}
            @include('layouts.admin.partials.sidenav')

            {{-- Page Container START --}}
            <div class="page-container">
                {{-- Top Nav --}}
                @include('layouts.admin.partials.topnav')
                
                {{-- Main Content --}}
                <div class="main-content">
                    {{-- breadcrumb --}}
                    @include('layouts.admin.partials.breadcrumb')

                    @yield('main_content')
                </div>

                {{-- Footer --}}
                @include('layouts.admin.partials.footer')
            </div>
            {{-- Page Container END --}}
        </div>
                
        {{-- //added sweetalert --}}
        @include('sweetalert::alert')
    </div>

    @include('layouts.admin.partials.scripts')
</body>
</html>