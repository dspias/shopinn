<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('admin.dashboard.index') }}">
                <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="javascript:void(0);">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">

            {{-- ======================< Dashoboard >====================== --}}
            <li class="nav-item {{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                <a class="mrg-top-30" href="{{ route('admin.dashboard.index') }}">
                    <span class="icon-holder">
                        <i class="ti-home"></i>
                    </span>
                    <span class="title">{{ __('Dashboard') }}</span>
                </a>
            </li>
            {{-- ======================< Dashoboard End >====================== --}}



            {{-- ======================< Orders >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/orders*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-announcement"></i>
                    </span>
                    <span class="title">{{ __('Orders') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/orders/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.all') }}">{{ __('All Orders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/orders/pending*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.pending') }}">{{ __('Pending Orders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/orders/running*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.running') }}">{{ __('Running Orders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/orders/completed*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.completed') }}">{{ __('Completed Orders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/orders/canceled*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.canceled') }}">{{ __('Canceled Orders') }}</a>
                    </li>
                    {{-- <li class="{{ Request::is('admin/orders/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.create') }}">{{ __('Create New Order') }}</a>
                    </li> --}}
                </ul>
            </li>
            {{-- ======================< Orders End >====================== --}}



            {{-- ======================< Products >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/products*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-shopping-cart"></i>
                    </span>
                    <span class="title">{{ __('Products') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/products/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.all') }}">{{ __('All Products') }}</a>
                    </li>
                    {{-- <li class="{{ Request::is('admin/products/pending*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.pending') }}">{{ __('Pending Products') }}</a>
                    </li> --}}
                    <li class="{{ Request::is('admin/products/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.active') }}">{{ __('Active Products') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/products/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.inactive') }}">{{ __('Inactive Products') }}</a>
                    </li>
                    {{-- <li class="{{ Request::is('admin/products/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.product.create') }}">{{ __('Create New Product') }}</a>
                    </li> --}}
                </ul>
            </li>
            {{-- ======================< Products End >====================== --}}



            {{-- ======================< Shops >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/shops*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-shopping-cart-full"></i>
                    </span>
                    <span class="title">{{ __('Shops') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/shops/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.shop.all') }}">{{ __('All Shops') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/shops/pending*') ? 'active' : '' }}">
                        <a href="{{ route('admin.shop.pending') }}">{{ __('Pending Shops') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/shops/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.shop.active') }}">{{ __('Active Shops') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/shops/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.shop.inactive') }}">{{ __('Inactive Shops') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/shops/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.shop.create') }}">{{ __('Create New Shop') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Shops End >====================== --}}



            {{-- ======================< Resellers >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/resellers*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-bag"></i>
                    </span>
                    <span class="title">{{ __('Resellers') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/resellers/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.all') }}">{{ __('All Resellers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/resellers/pending*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.pending') }}">{{ __('Pending Resellers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/resellers/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.active') }}">{{ __('Active Resellers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/resellers/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.inactive') }}">{{ __('Inactive Resellers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/resellers/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.create') }}">{{ __('Create New Reseller') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/resellers/commission*') ? 'active' : '' }}">
                        <a href="{{ route('admin.reseller.commission') }}">{{ __('Reseller Commission') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Resellers End >====================== --}}



            {{-- ======================< Riders >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/riders*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-truck"></i>
                    </span>
                    <span class="title">{{ __('Riders') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/riders/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.rider.all') }}">{{ __('All Riders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/riders/pending*') ? 'active' : '' }}">
                        <a href="{{ route('admin.rider.pending') }}">{{ __('Pending Riders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/riders/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.rider.active') }}">{{ __('Active Riders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/riders/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.rider.inactive') }}">{{ __('Inactive Riders') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/riders/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.rider.create') }}">{{ __('Create New Rider') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Riders End >====================== --}}



            {{-- ======================< Business Packages >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/packages*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-package"></i>
                    </span>
                    <span class="title">{{ __('Packages') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/packages/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.package.all') }}">{{ __('All Packages') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/packages/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.package.active') }}">{{ __('Active Packages') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/packages/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.package.inactive') }}">{{ __('Inactive Packages') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/packages/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.package.create') }}">{{ __('Create New Package') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Business Packages End >====================== --}}



            {{-- ======================< Advertise >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/advertises*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-flag"></i>
                    </span>
                    <span class="title">{{ __('Advertises') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/advertises/admin*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.index') }}">{{ __('Admin Advertises') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/advertises/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.all') }}">{{ __('All Advertises') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/advertises/active*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.active') }}">{{ __('Active advertises') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/advertises/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.inactive') }}">{{ __('Inactive advertises') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/advertises/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.create') }}">{{ __('Create New Advertise') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/advertises/shop_ad_pack_log*') ? 'active' : '' }}">
                        <a href="{{ route('admin.advertise.shop.ad.pack') }}">{{ __('Advertise Package Logs') }}</a>
                    </li>

                    <li class="nav-item dropdown {{ Request::is('admin/advertises/setting*') ? 'open' : '' }}"">
                        <a href="javascript:void(0);">
                            <span>{{ __('Settings') }}</span>
                            <span class="arrow">
                                    <i class="ti-angle-right"></i>
                                </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/advertises/settings/types*') ? 'active' : '' }}">
                                <a href="{{ route('admin.advertise.setting.type.all') }}">{{ __('Advertise Types') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/advertises/settings/packages*') ? 'active' : '' }}">
                                <a href="{{ route('admin.advertise.setting.package.all') }}">{{ __('Advertise Packages') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- ======================< Advertise End >====================== --}}



            {{-- ======================< Payments >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/payments*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-money"></i>
                    </span>
                    <span class="title">{{ __('Payments') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/payments/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.payment.all') }}">{{ __('All Payments') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/payments/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.payment.create') }}">{{ __('Make New Payment') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Payments End >====================== --}}



            {{-- ======================< Customers >====================== --}}
            <li class="nav-item dropdown {{ Request::is('admin/customers*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-user"></i>
                    </span>
                    <span class="title">{{ __('Customers') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/customers/all*') ? 'active' : '' }}">
                        <a href="{{ route('admin.customer.all') }}">{{ __('All Customers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/customers/banned*') ? 'active' : '' }}">
                        <a href="{{ route('admin.customer.banned') }}">{{ __('Banned Customers') }}</a>
                    </li>
                    <li class="{{ Request::is('admin/customers/create*') ? 'active' : '' }}">
                        <a href="{{ route('admin.customer.create') }}">{{ __('Create New Customer') }}</a>
                    </li>
                </ul>
            </li>
            {{-- ======================< Customers End >====================== --}}

            <li class="nav-devider">
                <hr class="devider">
            </li>



            {{-- ======================< Settings >====================== --}}
            <li class="nav-item setting-items dropdown {{ Request::is('admin/settings*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-settings"></i>
                    </span>
                    <span class="title">{{ __('Settings') }}</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item dropdown {{ Request::is('admin/settings/admin*') ? 'open' : '' }}"">
                        <a href="javascript:void(0);">
                            <span>{{ __('Administration') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/settings/administration/all*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.administration.all') }}">{{ __('All Admins') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/administration/active*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.administration.active') }}">{{ __('Active Admins') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/administration/inactive*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.administration.inactive') }}">{{ __('Inactive Admins') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/administration/create*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.administration.create') }}">{{ __('Create New Admin') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown {{ Request::is('admin/settings/product*') ? 'open' : '' }}"">
                        <a href="javascript:void(0);">
                            <span>{{ __('Product Settings') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ Request::is('admin/settings/products/type*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.product.type.index') }}">{{ __('Product Types') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/products/categories*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.product.category.index') }}">{{ __('Product Categories') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown {{ Request::is('admin/settings/companies*') ? 'open' : '' }}"">
                        <a href="javascript:void(0);">
                            <span>{{ __('Company Settings') }}</span>
                            <span class="arrow">
                                <i class="ti-angle-right"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            {{-- <li class="{{ Request::is('admin/settings/companies/profile*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.company.profile.index') }}">{{ __('Profile') }}</a>
                            </li> --}}
                            <li class="{{ Request::is('admin/settings/companies/about*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.company.about.index') }}">{{ __('About') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/companies/privacy*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.company.privacy.index') }}">{{ __('Privacy Policy') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/companies/term*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.company.term.index') }}">{{ __('Terms & Conditions') }}</a>
                            </li>
                            <li class="{{ Request::is('admin/settings/companies/faq*') ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.company.faq.index') }}">{{ __('Faq\'s') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- ======================< Riders End >====================== --}}
        </ul>
    </div>
</div>
<!-- Side Nav END -->
