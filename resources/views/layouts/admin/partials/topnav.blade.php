<div class="header navbar">
    <div class="header-container">
        <ul class="nav-left">
            <li>
                <a class="side-nav-toggle" href="javascript:void(0);">
                    <i class="ti-view-grid"></i>
                </a>
            </li>
            <li class="search-box">
                <a class="search-toggle no-pdd-right" href="javascript:void(0);">
                    <i class="search-icon ti-search pdd-right-10"></i>
                    <i class="search-icon-close ti-close pdd-right-10"></i>
                </a>
            </li>
            <li class="search-input">
                <input class="form-control search" type="text" placeholder="Search Anything From Here">
            </li>
        </ul>
        <ul class="nav-right">
            {{-- Notifications --}}
            @php
                $notifications = auth()->user()->notifications()->limit(9)->get();
            @endphp
            <li class="notifications dropdown pdd-right-10" style="border-right: 1px solid #efefef;">
                <span class="counter mrg-right-10">
                    @if(auth()->user()->unreadNotifications()->count() <=9)
                        {{ '0' }}{{ auth()->user()->unreadNotifications()->count() }}
                    @else
                        {{ '9+' }}
                    @endif
                </span>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="ti-bell"></i>
                </a>

                <ul class="dropdown-menu">
                    <li class="notice-header">
                        <i class="ti-bell pdd-right-10"></i>
                        <span>{{ __('Notifications') }}</span>
                    </li>
                    <li>
                        <ul class="list-info overflow-y-auto relative scrollable ps-container ps-theme-default ps-active-y" data-ps-id="8964a8c7-94b9-3a1f-b6b9-c6f2dc7d257c">
                            @foreach($notifications as $serial => $notification)
                                <li @if($notification->read_at == null) style="background-color:#f6f7fb;" @endif>
                                    <a href="{{ route('markasread', ['id' => $notification->id]) }}">
                                        <span class="thumb-img bg-danger">
                                            <span class="text-white"><i class="ti-bell"></i></span>
                                        </span>
                                        <div class="info">
                                            <span class="title">
                                                <span class="font-size-14 text-semibold">{!! $notification->data['message'] !!}</span>
                                            </span>
                                            <span class="sub-title">{{ ago_time($notification->created_at) }}</span>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="notice-footer">
                        <span>
                            <a href="{{ route('admin.notification.index') }}" class="text-gray">{{ __('Check all notifications') }}<i class="ei-right-chevron pdd-left-5 font-size-10"></i></a>
                        </span>
                    </li>
                </ul>
            </li>

            {{-- Language --}}
            <li class="notifications message dropdown pdd-right-10 pdd-left-5" style="border-right: 1px solid #efefef;">
                <span class="counter mrg-right-10">
                    @if(auth()->user()->lang == 'en')ENG
                    @else
                        বাংলা
                    @endif
                </span>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="ti-world"></i>
                </a>

                <ul class="dropdown-menu">
                    <li class="notice-header">
                        <i class="ti-world pdd-right-10"></i>
                        <span>{{ __('Languages') }}</span>
                    </li>
                    <li>
                        <ul class="list-info overflow-y-auto relative scrollable ps-container ps-theme-default ps-active-y" data-ps-id="8964a8c7-94b9-3a1f-b6b9-c6f2dc7d257c">
                            <li>
                                <a href="{{ route('guest.change_lang', ['lang' => 'en']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_en').submit();">
                                    <img class="thumb-img" title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/US.png') }}">
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">English(US)</span>
                                        </span>
                                    </div>
                                </a>

                                <form id="change_lang_en" action="{{ route('guest.change_lang', ['lang' => 'en']) }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                            <li>
                                <a href="{{ route('guest.change_lang', ['lang' => 'ban']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_ban').submit();">
                                    <img class="thumb-img" title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/BAN.png') }}">
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">বাংলা</span>
                                        </span>
                                    </div>
                                </a>

                                <form id="change_lang_ban" action="{{ route('guest.change_lang', ['lang' => 'ban']) }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            {{-- User Profile --}}
            <li class="user-profile dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @php
                        $url = (auth()->user()->avatar != null) ? image(auth()->user()->avatar):asset('vendor/images/resources/audio-user3.jpg');
                    @endphp
                    <img class="profile-img img-fluid" src="{{ $url }}" alt="">
                    <div class="user-info">
                        <span class="name pdd-right-5">{{ Auth::user()->name }}</span>
                        <i class="ti-angle-down font-size-10"></i>
                    </div>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('admin.profile.index') }}">
                            <i class="ti-user pdd-right-10"></i>
                            <span>Profile</span>
                        </a>
                    </li>
                    {{-- <li>
                        <a href="#">
                            <i class="ti-settings pdd-right-10"></i>
                            <span>Setting</span>
                        </a>
                    </li> --}}
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="ti-power-off pdd-right-10"></i>
                            <span>Logout</span>
                        </a>
                    </li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </li>
        </ul>
    </div>
</div>
