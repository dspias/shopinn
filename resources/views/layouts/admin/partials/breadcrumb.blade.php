<div class="page-title">
    <ul class="breadcrumb">
        <li class="breadcrumb-item">
            <h4 class="page-header-title">
                @yield('page_header_title')
            </h4>
        </li>
        <li class="breadcrumb-item home">
            <a href="{{ route('admin.dashboard.index') }}"><i class="ti-home"></i></a>
        </li>
        @yield('breadcrumb_item_lists')
    </ul>
</div>