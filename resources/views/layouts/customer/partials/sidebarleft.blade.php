<div class="fixed-sidebar left">
    <div class="menu-left">
        <ul class="left-menu">            
            <li>
                <a href="/" title="{{ __('Homepage') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-home"></i>
                </a>
            </li>
            
            <li>
                <a href="{{ route('customer.order.index') }}" title="{{ __('My Orders') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-announcement"></i>
                </a>
            </li>

            {{--  <li>
                <a href="{{ route('customer.payment.index') }}" title="{{ __('Payment Reports') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-credit-card"></i>
                </a>
            </li>  --}}

            <li>
                <a href="{{ route('customer.cart.index') }}" title="{{ __('My Cart') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-shopping-cart"></i>
                </a>
            </li>

            <li>
                <a href="{{ route('customer.profile.index') }}" title="{{ __('Profile') }}" data-toggle="tooltip" data-placement="right">
                    <i class="ti-user"></i>
                </a>
            </li>
        </ul>
    </div>
</div><!-- left sidebar menu -->