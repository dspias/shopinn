 {{-- <script src="{{ asset('js/app.js') }}"></script>  --}}

<script src="{{ asset('vendor/js/main.min.js') }}"></script> 
<script src="{{ asset('vendor/js/main.js') }}"></script>
<script src="{{ asset('assets/js/confirmation_alert/jquery-confirmv3.3.2.min.js') }}"></script>


	
@php
    $review = \App\Models\Order::with(['items' => function($query){
        $query->where('status', '!=', -1);
    }])->where('customer_id', auth()->user()->id)->where('status', 2)->where('has_reviewed', 0)->orderBy('id', 'desc')->first();
@endphp

@if($review != null)
    @include('layouts.customer.partials.review')
    <script>
        $(document).ready(function(){
            $('#myModalscroll').modal('toggle');
            $('#myModalscroll').modal('show');
        });
    </script>
@endif


{{-- Livewire JS --}}
@livewireScripts

@yield('page_src_scripts')

<script src="{{ asset('vendor/js/script.js') }}"></script>



@yield('page_scripts')