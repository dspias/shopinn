<div class="responsive-header">
    <div class="mh-head first Sticky">
        <span class="mh-btns-left">
            <a class="" href="#menu"><i class="fa fa-sliders"></i></a>
        </span>
        <span class="mh-text">
            <a title="{{ __('Go To Homepage') }}" href="{{ route('guest.homepage.index') }}">
                <img src="{{ asset('vendor/images/logo/logo-white.png') }}" alt="">
            </a>
        </span>
        {{-- <span class="mh-btns-right">
            <a class="fa fa-sliders" href="#shoppingbag"></a>
        </span> --}}
    </div>
    <div class="mh-head second">
        <form class="mh-form" method="POST" action="{{ route('guest.search.find') }}">
            @csrf
            <input name="search" required placeholder="Search for Products Or Shop" />
            <button type="submit" class="fa fa-search"></button>
        </form>
    </div>
    <nav id="menu" class="res-menu">
        <ul>
            <li>
                <a class="" href="/" title="{{ __('Homepage') }}"><i class="ti-home"></i> {{ __('Homepage') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.order.index') }}" title="{{ __('My Orders') }}"><i class="ti-announcement"></i> {{ __('My Orders') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.payment.index') }}" title="{{ __('My Payments') }}"><i class="ti-credit-card"></i> {{ __('My Payments') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.cart.index') }}" title="{{ __('My Cart') }}"><i class="ti-shopping-cart"></i> {{ __('My Cart') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.profile.index') }}" title="{{ __('Profile') }}"><i class="ti-user"></i> {{ __('Profile') }}</a>
            </li>

            @auth
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form_01').submit();">
                        <i class="ti-power-off" ></i>
                        {{ __('Logout') }}
                    </a>
                </li>

                <form id="logout-form_01" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                <li>
                    <a class="" href="{{ route('login') }}" title="{{ __('Login') }}"><i class="ti-home"></i> {{ __('Login') }}</a>
                </li>
            @endauth
        </ul>
    </nav>
    {{-- <nav id="shoppingbag">
        <div>
            <div class="">
                <form method="post">
                    <div class="setting-row">
                        <span>used night mode</span>
                        <input type="checkbox" id="nightmode"/>
                        <label for="nightmode" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Notifications</span>
                        <input type="checkbox" id="switch2"/>
                        <label for="switch2" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Notification sound</span>
                        <input type="checkbox" id="switch3"/>
                        <label for="switch3" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>My profile</span>
                        <input type="checkbox" id="switch4"/>
                        <label for="switch4" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Show profile</span>
                        <input type="checkbox" id="switch5"/>
                        <label for="switch5" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                </form>
                <h4 class="panel-title">Account Setting</h4>
                <form method="post">
                    <div class="setting-row">
                        <span>Sub users</span>
                        <input type="checkbox" id="switch6" />
                        <label for="switch6" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>personal account</span>
                        <input type="checkbox" id="switch7" />
                        <label for="switch7" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Business account</span>
                        <input type="checkbox" id="switch8" />
                        <label for="switch8" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Show me online</span>
                        <input type="checkbox" id="switch9" />
                        <label for="switch9" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Delete history</span>
                        <input type="checkbox" id="switch10" />
                        <label for="switch10" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Expose author name</span>
                        <input type="checkbox" id="switch11" />
                        <label for="switch11" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                </form>
            </div>
        </div>
    </nav> --}}
</div><!-- responsive header -->


{{-- main sectoin --}}
<div class="topbar stick">
    <div class="logo">
        <a title="Go to Homepage" href="{{ route('guest.homepage.index') }}">
            <img src="{{ asset('vendor/images/logo/logo-white.png') }}" alt="">
        </a>
    </div>
    <div class="top-area">
        {{-- <div class="main-menu">
            <span>
                <i class="fa fa-braille"></i>
            </span>
        </div>   --}}
        {{-- menu icon --}}
        <div class="top-search">
            <form class="mh-form" method="POST" action="{{ route('guest.search.find') }}">
                @csrf
                <input name="search" required placeholder="Search for Products Or Shop" />
                <button type="submit" class="fa fa-search"></button>
            </form>
        </div> {{-- search box --}}
        {{--<div class="page-name">
            <span>@yield('page_name')</span>
        </div>  nav page title --}}

        <ul class="setting-area">
            {{-- Notification Starts --}}
            <li>
                @php
                    $notifications = auth()->user()->notifications()->limit(9)->get();
                @endphp
                <a href="#" title="Notification" data-ripple="">
                    <i class="fa fa-bell"></i><em class="bg-purple">

                        @if(auth()->user()->unreadNotifications()->count() <=9)
                            {{ auth()->user()->unreadNotifications()->count() }}
                        @else
                            {{ '9+' }}
                        @endif
                    </em>
                </a>
                <div class="dropdowns">
                    <span class="text-danger">
                        @if(auth()->user()->unreadNotifications()->count() <=9)
                            {{ auth()->user()->unreadNotifications()->count() }}
                        @else
                            {{ '9+' }}
                        @endif
                        {{ " Unread Notification's" }}
                         {{--  <a href="{{  }}" title="Mark All Notification as Read" data-toggle="tooltip" class="text-success">Mark all as read</a></span>  --}}
                    <ul class="drops-menu">
                        @foreach($notifications as $serial => $notification)
                            <li>
                                <a href="{{ route('markasread', ['id' => $notification->id]) }}" title="">
                                    <figure>
                                        <i class="fa fa-bell fa-3x text-shopinn"></i>
                                    </figure>
                                    <div class="mesg-meta">
                                        <h6>{!! $notification->data['message'] !!}</h6>
                                        <i>{{ ago_time($notification->created_at) }}</i>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{ route('customer.notification.index') }}" title="View All Notification" data-toggle="tooltip" class="more-mesg">View All</a>
                </div>
            </li>
            {{-- Notification Ends --}}


            {{-- Language Starts --}}
            @auth
                <li>
                    <a href="#" title="Languages" data-ripple=""><i class="fa fa-globe"></i><em>
                        @if(auth()->user()->lang == 'en')ENG
                        @else
                        বাংলা
                        @endif
                    </em></a>
                    <div class="dropdowns languages">
                        <div data-gutter="3" class="row">
                            <div class="col-md-12">
                                <ul class="dropdown-meganav-select-list-lang">
                                    <li class="@if(auth()->user()->lang == 'en')active @endif">
                                        <a href="{{ route('guest.change_lang', ['lang' => 'en']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_en').submit();">
                                        <img title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/US.png') }}">English(US)
                                        </a>

                                        <form id="change_lang_en" action="{{ route('guest.change_lang', ['lang' => 'en']) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                    <li  class="@if(auth()->user()->lang == 'ban')active @endif">
                                        <a href="{{ route('guest.change_lang', ['lang' => 'ban']) }}" onclick="event.preventDefault(); document.getElementById('change_lang_ban').submit();">
                                        <img title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/BAN.png') }}">বাংলা
                                        </a>

                                        <form id="change_lang_ban" action="{{ route('guest.change_lang', ['lang' => 'ban']) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            @endauth
            {{-- Language Ends --}}


            {{-- Help Starts --}}
            <li>
                <a href="#" title="Help" data-ripple=""><i class="fa fa-question-circle"></i></a>
                <div class="dropdowns helps">
                    <span>Help with this page</span>
                    <ul class="help-drop">
                        <li><a href="{{ route('guest.contact.index') }}"><i class="fa fa-map-marker"></i>Contact</a></li>
                        <li><a href="{{ route('guest.terms.index') }}"><i class="fa fa-pencil-square-o"></i>{{ __('Terms & Policy') }}</a></li>
                        <li><a href="{{ route('guest.faq.index') }}"><i class="fa fa-question-circle-o"></i>{{ __('FAQs') }}</a></li>
                        {{-- <li><a href="#"><i class="fa fa-book"></i>Community & Forum</a></li>
                        <li><a href="#"><i class="fa fa-building-o"></i>Carrers</a></li>
                        <li><a href="#"><i class="fa fa-exclamation-triangle"></i>Report a Problem</a></li> --}}
                    </ul>
                </div>
            </li>
            {{-- Help Ends --}}
        </ul>

        <div class="user-img">
            @guest
                <h5 class="text-uppercase">Contact: {{ company_get('mobile') }}</h5>
                <h5 class="devider" style="cursor: default; margin-top: -2px; font-size: 20px; font-weight: 400; color: #7d8391;"> | </h5>
                <h5 class="text-uppercase"><a href="{{ route('login') }}">Login</a></h5>
                <h5 class="devider" style="cursor: default; margin-top: -2px; font-size: 20px; font-weight: 400; color: #7d8391;"> | </h5>
                <h5 class="text-uppercase"><a href="{{ route('register') }}">Register</a></h5>
            @else
                <h5 class="text-uppercase">Contact: {{ company_get('mobile') }}</h5>
                <h5 class="devider" style="cursor: default; margin-top: -2px; font-size: 20px; font-weight: 400; color: #7d8391;"> | </h5>
                <h5>{{ auth()->user()->name }}</h5>
                @php
                    $url = get_logo(auth()->user(), 'nav');
                    if($url == null) $url = asset('vendor/images/resources/admin.jpg');
                @endphp
                <img src="{{ $url }}" alt="45 * 45 Daimention user not found">
                {{-- <span class="status f-online"></span> --}}
                <div class="user-setting">
                    <ul class="log-out">
                        <li>
                            <a href="{{ route('customer.profile.index') }}" title="">
                                <i class="ti-user"></i>
                                {{ __('My Profile') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form_02').submit();" title="">
                                <i class="ti-power-off" ></i>
                                {{ __('Log Out') }}
                            </a>
                        </li>

                        <form id="logout-form_02" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            @endguest
        </div>
        {{-- <span class="ti-settings main-menu" data-ripple=""></span> --}}
    </div>
    <nav>
        <ul class="nav-list">
            <li>
                <a class="" href="/" title="{{ __('Homepage') }}"><i class="ti-home"></i> {{ __('Homepage') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.order.index') }}" title="{{ __('My Orders') }}"><i class="ti-announcement"></i> {{ __('My Orders') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.payment.index') }}" title="{{ __('My Payments') }}"><i class="ti-credit-card"></i> {{ __('My Payments') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.cart.index') }}" title="{{ __('My Cart') }}"><i class="ti-shopping-cart"></i> {{ __('My Cart') }}</a>
            </li>

            <li>
                <a class="" href="{{ route('customer.profile.index') }}" title="{{ __('Profile') }}"><i class="ti-user"></i> {{ __('Profile') }}</a>
            </li>

            @auth
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form_03').submit();">
                        <i class="ti-power-off" ></i>
                        {{ __('Logout') }}
                    </a>
                </li>

                <form id="logout-form_03" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                <li>
                    <a class="" href="{{ route('login') }}" title="{{ __('Login') }}"><i class="ti-home"></i> {{ __('Login') }}</a>
                </li>
            @endauth
        </ul>

    </nav><!-- nav menu -->
</div><!-- topbar -->
