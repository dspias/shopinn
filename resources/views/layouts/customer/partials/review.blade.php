<div class="modal fade" id="myModalscroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title text-bold">{{ __('Product Review') }}</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <form action="{{ route('customer.order.review', ['order_id' => $review->id]) }}" method="post">
                @csrf
                {{--  <!-- Modal body -->  --}}
                <div class="modal-body product-review-section p-b-0">
                    <table class="table table-border">
                        <tbody>
                            @foreach ($review->items as $item)
                                <tr>
                                    <td>
                                        <h5 class="title-1-line text-bold text-shopinn m-b-0 text-center">
                                            {{ __($item->product->name) }}
                                        </h5>
                                        <div>
                                            <div class="rating-star text-center float_center m-b-5">
                                                <div class="rate child">
                                                    <input type="radio" id="star_5_{{ $item->id }}" name="review_rating[{{ $item->id }}][rating]" value="5" />
                                                    <label for="star_5_{{ $item->id }}" title="text">5 stars</label>
                                                    <input type="radio" id="star_4_{{ $item->id }}" name="review_rating[{{ $item->id }}][rating]" value="4" />
                                                    <label for="star_4_{{ $item->id }}" title="text">4 stars</label>
                                                    <input type="radio" id="star_3_{{ $item->id }}" name="review_rating[{{ $item->id }}][rating]" value="3" />
                                                    <label for="star_3_{{ $item->id }}" title="text">3 stars</label>
                                                    <input type="radio" id="star_2_{{ $item->id }}" name="review_rating[{{ $item->id }}][rating]" value="2" />
                                                    <label for="star_2_{{ $item->id }}" title="text">2 stars</label>
                                                    <input type="radio" id="star_1_{{ $item->id }}" name="review_rating[{{ $item->id }}][rating]" value="1" />
                                                    <label for="star_1_{{ $item->id }}" title="text">1 star</label>
                                                </div>
                                            </div>
                                            
                                            <textarea rows="2" placeholder="{{ __('Write Your Review') }}*" name="review_rating[{{ $item->id }}][review]" required></textarea>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                {{--  <!-- Modal footer -->  --}}
                <div class="modal-footer">
                    <button type="submit" class="btn btn-shopinn btn-block">{{ __('Submit Review') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
