
<div class="col-lg-12">
    <div class="gap2 no-top">
        <div class="central-meta no-margin">
            <ul class="nave-area">
                @yield('sub_sections')
            </ul>
        </div>
    </div>	
</div><!-- nave list -->