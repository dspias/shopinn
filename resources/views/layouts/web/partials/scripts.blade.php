<script src="{{ asset('vendor/js/main.min.js') }}"></script>
	<script src="{{ asset('vendor/js/jquery-stories.js') }}"></script>
	<script src="{{ asset('vendor/js/toast-notificatons.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js"></script><!-- For timeline slide show -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8c55_YHLvDHGACkQscgbGLtLRdxBDCfI"></script><!-- for location picker map -->
	<script src="{{ asset('vendor/js/locationpicker.jquery.js') }}"></script>   {{-- for loaction picker map --}} 
	<script src="{{ asset('vendor/js/map-init.js') }}"></script><!-- map initilasition -->
	{{--  <script src="{{ asset('vendor/js/page-tourintro.js') }}"></script>
	<script src="{{ asset('vendor/js/page-tour-init.js') }}"></script>  --}}
	<script src="{{ asset('vendor/js/script.js') }}"></script>
	<script>
		jQuery(document).ready(function($) {
			
			$('#us3').locationpicker({
			  location: {
			    latitude: -8.681013,
			    longitude: 115.23506410000005
			  },
			  radius: 0,
			  inputBinding: {
			    latitudeInput: $('#us3-lat'),
			    longitudeInput: $('#us3-lon'),
			    radiusInput: $('#us3-radius'),
			    locationNameInput: $('#us3-address')
			  },
			  enableAutocomplete: true,
			  onchanged: function (currentLocation, radius, isMarkerDropped) {
			    // Uncomment line below to show alert on each Location Changed event
			    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
			  }
			});

		});	
    </script>

	@yield('page_src_scripts')
	
    @yield('page_scripts')