

<link rel="stylesheet" href="{{ asset('vendor/css/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/weather-icons.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/toast-notification.css') }}"> <!-- extra from other-->
{{--  <link rel="stylesheet" href="{{ asset('vendor/css/page-tour.css') }}">	<!-- extra from other-->  --}}
<link rel="stylesheet" href="{{ asset('vendor/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/color.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/css/responsive.css') }}">

@yield('page_src_styles')
@yield('page_styles')