<div class="responsive-header">
    <div class="mh-head first Sticky">
        <span class="mh-btns-left">
            <a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
        </span>
        <span class="mh-text">
            <a href="newsfeed.html" title=""><img src="{{ asset('vendor/images/logo2.png') }}" alt=""></a>
        </span>
        <span class="mh-btns-right">
            <a class="fa fa-sliders" href="#shoppingbag"></a>
        </span>
    </div>
    <div class="mh-head second">
        <form class="mh-form">
            <input placeholder="search" />
            <a href="#/" class="fa fa-search"></a>
        </form>
    </div>
    <nav id="menu" class="res-menu">
        <ul>
            <li>
                <a class="" href="#" title="Dashboard"><i class="fa fa-home"></i> Dashboard</a>
            </li>

            <li>
                <a class="" href="#" title="Orders"><i class="fa fa-film"></i> Orders</a>
                <ul>
                    <li><a href="#" title="Running Orders">Running Orders</a></li>
                    <li><a href="#" title="Pending Orders">Pending Orders</a></li>
                    <li><a href="#" title="Completed Orders">Completed Orders</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Business Packages"><i class="fa fa-female"></i> Business Packages</a>
                <ul>
                    <li><a href="#" title="Add Package">Add Package</a></li>
                    <li><a href="#" title="Active Packages">Active Packages</a></li>
                    <li><a href="#" title="Inactive Packages">Inactive Packages</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Advertises"><i class="fa fa-female"></i> Advertises</a>
                <ul>
                    <li><a href="#" title="Advertises Types">Advertises Types</a></li>
                    <li><a href="#" title="Advertise Packages">Advertise Packages</a></li>
                    <li><a href="#" title="Advertise Logs">Advertise Logs</a></li>
                </ul>
            </li>

            <li><a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Shops</a>
                <ul>
                    <li><a href="#" title="Active Shops">Active Shops</a></li>
                    <li><a href="#" title="Inactive Shops">Inactive Shops</a></li>
                    <li><a href="#" title="Pending Shops">Pending Shops</a></li>
                </ul>
            </li>

            <li><a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Resellers</a>
                <ul>
                    <li><a href="#" title="Add Reseller">Add Reseller</a></li>
                    <li><a href="#" title="Pending Resellers">Pending Resellers</a></li>
                    <li><a href="#" title="Active Resellers">Active Resellers</a></li>
                    <li><a href="#" title="Inactive Resellers">Inactive Resellers</a></li>
                </ul>
            </li>
            
            <li>
                <a class="" href="#" title="Riders"><i class="fa fa-repeat"></i> Riders</a>
                <ul>
                    <li><a href="#" title="Add Rider">Add Rider</a></li>
                    <li><a href="#" title="Pending Riders">Pending Riders</a></li>
                    <li><a href="#" title="Active Riders">Active Riders</a></li>
                    <li><a href="#" title="Inactive Riders">Inactive Riders</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Customers"><i class="fa fa-repeat"></i> Customers</a>
                <ul>
                    <li><a href="#" title="All Customers">Customers</a></li>
                    <li><a href="#" title="Customers Reviews">Customers Reviews</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Payment Reports"><i class="fa fa-repeat"></i> Payment Reports</a>
                <ul>
                    <li><a href="#" title="Orders wise">Orders wise</a></li>
                    <li><a href="#" title="Shops wise">Shops wise</a></li>
                    <li><a href="#" title="Resellers wise">Resellers wise</a></li>
                    <li><a href="#" title="Riders wise">Riders wise</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Settings"><i class="fa fa-gears"></i> Settings</a>
                <ul>
                    <li><a href="#" title="Product Settings">Product Settings</a></li>
                    <li><a href="#" title="Company Settings">Company Settings</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <nav id="shoppingbag">
        <div>
            <div class="">
                <form method="post">
                    <div class="setting-row">
                        <span>used night mode</span>
                        <input type="checkbox" id="nightmode"/> 
                        <label for="nightmode" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Notifications</span>
                        <input type="checkbox" id="switch2"/> 
                        <label for="switch2" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Notification sound</span>
                        <input type="checkbox" id="switch3"/> 
                        <label for="switch3" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>My profile</span>
                        <input type="checkbox" id="switch4"/> 
                        <label for="switch4" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Show profile</span>
                        <input type="checkbox" id="switch5"/> 
                        <label for="switch5" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                </form>
                <h4 class="panel-title">Account Setting</h4>
                <form method="post">
                    <div class="setting-row">
                        <span>Sub users</span>
                        <input type="checkbox" id="switch6" /> 
                        <label for="switch6" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>personal account</span>
                        <input type="checkbox" id="switch7" /> 
                        <label for="switch7" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Business account</span>
                        <input type="checkbox" id="switch8" /> 
                        <label for="switch8" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Show me online</span>
                        <input type="checkbox" id="switch9" /> 
                        <label for="switch9" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Delete history</span>
                        <input type="checkbox" id="switch10" /> 
                        <label for="switch10" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <div class="setting-row">
                        <span>Expose author name</span>
                        <input type="checkbox" id="switch11" /> 
                        <label for="switch11" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                </form>
            </div>
        </div>
    </nav>
</div><!-- responsive header -->


{{-- main sectoin --}}
<div class="topbar stick">
    <div class="logo">
        <a title="" href="newsfeed.html"><img src="{{ asset('vendor/images/logo.png') }}" alt=""></a>
    </div>
    <div class="top-area">
        <div class="main-menu">
            <span>
                <i class="fa fa-braille"></i>
            </span>
        </div>  {{-- menu icon --}}
        <div class="top-search">
            <form method="post" class="">
                <input type="text" placeholder="Search People, Pages, Groups etc">
                <button data-ripple><i class="ti-search"></i></button>
            </form>
        </div> {{-- search box --}}
        <div class="page-name">
            <span>@yield('page_name')</span>
        </div> {{-- nav page title --}}

        <ul class="setting-area">

            <li>
                <a href="{{ route('admin.dashboard.index') }}" title="Home" data-ripple=""><i class="fa fa-home"></i></a>
            </li>   {{-- home button  --}}


            {{-- <li>
                <a href="" title="Friend Requests" data-ripple="">
                    <i class="fa fa-user"></i><em class="bg-red">5</em>
                </a>
                <div class="dropdowns">
                    <span>5 New Requests <a href="#" title="">View all Requests</a></span>
                    <ul class="drops-menu">
                        <li>
                            <div>
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-2.jpg') }}" alt="">
                                </figure>
                                <div class="mesg-meta">
                                    <h6><a href="#" title="">Loren</a></h6>
                                    <span><b>Amy</b> is mutule friend</span>
                                    <i>yesterday</i>
                                </div>
                                <div class="add-del-friends">
                                    <a href="#" title=""><i class="fa fa-heart"></i></a>
                                    <a href="#" title=""><i class="fa fa-trash"></i></a>
                                </div>
                            </div>	
                        </li>
                        <li>
                            <div>
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-3.jpg') }}" alt="">
                                </figure>
                                <div class="mesg-meta">
                                    <h6><a href="#" title="">Tina Trump</a></h6>
                                    <span><b>Simson</b> is mutule friend</span>
                                    <i>2 days ago</i>
                                </div>
                                <div class="add-del-friends">
                                    <a href="#" title=""><i class="fa fa-heart"></i></a>
                                    <a href="#" title=""><i class="fa fa-trash"></i></a>
                                </div>
                            </div>	
                        </li>
                        <li>
                            <div>
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-4.jpg') }}" alt="">
                                </figure>
                                <div class="mesg-meta">
                                    <h6><a href="#" title="">Andrew</a></h6>
                                    <span><b>Bikra</b> is mutule friend</span>
                                    <i>4 hours ago</i>
                                </div>
                                <div class="add-del-friends">
                                    <a href="#" title=""><i class="fa fa-heart"></i></a>
                                    <a href="#" title=""><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-5.jpg') }}" alt="">
                                </figure>
                                <div class="mesg-meta">
                                    <h6><a href="#" title="">Dasha</a></h6>
                                    <span><b>Sarah</b> is mutule friend</span>
                                    <i>9 hours ago</i>
                                </div>
                                <div class="add-del-friends">
                                    <a href="#" title=""><i class="fa fa-heart"></i></a>
                                    <a href="#" title=""><i class="fa fa-trash"></i></a>
                                </div>
                            </div>	
                        </li>
                        <li>
                            <div>
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-1.jpg') }}" alt="">
                                </figure>
                                <div class="mesg-meta">
                                    <h6><a href="#" title="">Emily</a></h6>
                                    <span><b>Amy</b> is mutule friend</span>
                                    <i>4 hours ago</i>
                                </div>
                                <div class="add-del-friends">
                                    <a href="#" title=""><i class="fa fa-heart"></i></a>
                                    <a href="#" title=""><i class="fa fa-trash"></i></a>
                                </div>
                            </div>	
                        </li>
                    </ul>
                    <a href="friend-requests.html" title="" class="more-mesg">View All</a>
                </div>
            </li>   friend request button  --}}


            <li>
                <a href="#" title="Notification" data-ripple="">
                    <i class="fa fa-bell"></i><em class="bg-purple">7</em>
                </a>
                <div class="dropdowns">
                    <span>4 New Notifications <a href="#" title="">Mark all as read</a></span>
                    <ul class="drops-menu">
                        <li>
                            <a href="notifications.html" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-1.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>sarah Loren</h6>
                                    <span>commented on your new profile status</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-2.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Jhon doe</h6>
                                    <span>Nicholas Grissom just became friends. Write on his wall.</span>
                                    <i>4 hours ago</i>
                                    <figure>
                                        <span>Today is Marina Valentine’s Birthday! wish for celebrating</span>
                                        <img src="{{ asset('vendor/images/birthday.png') }}" alt="">
                                    </figure>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-3.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Andrew</h6>
                                    <span>commented on your photo.</span>
                                    <i>Sunday</i>
                                    <figure>
                                        <span>"Celebrity looks Beautiful in that outfit! We should see each"</span>
                                        <img src="{{ asset('vendor/images/resources/admin.jpg') }}" alt="">
                                    </figure>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-4.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Tom cruse</h6>
                                    <span>nvited you to attend to his event Goo in</span>
                                    <i>May 19</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                        <li>
                            <a href="notifications.html" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-5.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Amy</h6>
                                    <span>Andrew Changed his profile picture. </span>
                                    <i>dec 18</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                    </ul>
                    <a href="notifications.html" title="" class="more-mesg">View All</a>
                </div>
            </li>   {{-- notifications button  --}}

            
            {{-- <li>
                <a href="#" title="Messages" data-ripple=""><i class="fa fa-commenting"></i><em class="bg-blue">9</em></a>
                <div class="dropdowns">
                    <span>5 New Messages <a href="#" title="">Mark all as read</a></span>
                    <ul class="drops-menu">
                        <li>
                            <a class="show-mesg" href="#" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-1.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>sarah Loren</h6>
                                    <span><i class="ti-check"></i> Hi, how r u dear ...! </span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="show-mesg" href="#" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-2.jpg') }}" alt="">
                                    <span class="status f-offline"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Jhon doe</h6>
                                    <span><i class="ti-check"></i> We’ll have to check that at the office and see if the client is on board with</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="show-mesg" href="#" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-3.jpg') }}" alt="">
                                    <span class="status f-online"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Andrew</h6>
                                    <span> <i class="fa fa-paperclip"></i>Hi Jack''s! It’s Diana, I just wanted to let you know that we have to reschedule..</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="show-mesg" href="#" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-4.jpg') }}" alt="">
                                    <span class="status f-offline"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Tom cruse</h6>
                                    <span><i class="ti-check"></i> Great, I’ll see you tomorrow!.</span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                        <li>
                            <a class="show-mesg" href="#" title="">
                                <figure>
                                    <img src="{{ asset('vendor/images/resources/thumb-5.jpg') }}" alt="">
                                    <span class="status f-away"></span>
                                </figure>
                                <div class="mesg-meta">
                                    <h6>Amy</h6>
                                    <span><i class="fa fa-paperclip"></i> Sed ut perspiciatis unde omnis iste natus error sit </span>
                                    <i>2 min ago</i>
                                </div>
                            </a>
                            <span class="tag">New</span>
                        </li>
                    </ul>
                    <a href="chat-messenger.html" title="" class="more-mesg">View All</a>
                </div>
            </li>   message button --}}


            <li>
                <a href="#" title="Languages" data-ripple=""><i class="fa fa-globe"></i><em>EN</em></a>
                <div class="dropdowns languages">
                    <div data-gutter="3" class="row">
                        <div class="col-md-12">
                            <ul class="dropdown-meganav-select-list-lang">
                                <li class="active">
                                    <a href="#">
                                    <img title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/US.png') }}">English(US)
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <img title="Image Title" alt="Image Alternative text" src="{{ asset('vendor/images/flags/BAN.png') }}">বাংলা
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>   {{-- language --}}


            <li>
                <a href="#" title="Help" data-ripple=""><i class="fa fa-question-circle"></i></a>
                <div class="dropdowns helps">
                    <span>Quick Help</span>
                    <form method="post">
                        <input type="text" placeholder="How can we help you?">
                    </form>
                    <span>Help with this page</span>
                    <ul class="help-drop">
                        <li><a href="forum.html" title=""><i class="fa fa-book"></i>Community & Forum</a></li>
                        <li><a href="faq.html" title=""><i class="fa fa-question-circle-o"></i>FAQs</a></li>
                        <li><a href="career.html" title=""><i class="fa fa-building-o"></i>Carrers</a></li>
                        <li><a href="privacy.html" title=""><i class="fa fa-pencil-square-o"></i>Terms & Policy</a></li>
                        <li><a href="#" title=""><i class="fa fa-map-marker"></i>Contact</a></li>
                        <li><a href="#" title=""><i class="fa fa-exclamation-triangle"></i>Report a Problem</a></li>
                    </ul>
                </div>
            </li>   {{-- help section --}}
        </ul>

        <div class="user-img">
            {{--  <h5>{{ auth()->user()->name }}</h5>
            @if(auth()->user()->avatar != null)
                <img src="{{ url(env('IMG_STORE').auth()->user()->avatar) }}" alt="45 * 45 Daimention user not found">
            @else
                <img src="{{ asset('vendor/images/resources/admin.jpg') }}" alt="45 * 45 Daimention user not found">
                <span class="name_logo">{{ auth()->user()->name[0] }}</span>
            @endif  --}}
            <span class="status f-online"></span>
            <div class="user-setting">
                <span class="seting-title">User setting <a href="#" title="">see all</a></span>
                <ul class="log-out">
                    <li><a href="#" title=""><i class="ti-user"></i> view profile</a></li>
                    <li><a href="#" title=""><i class="ti-pencil-alt"></i>edit profile</a></li>
                    <li><a href="#" title=""><i class="ti-settings"></i>settings</a></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title=""><i class="ti-power-off" ></i>log out</a></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
        </div>
        <span class="ti-settings main-menu" data-ripple=""></span>
    </div>
    <nav>
        <ul class="nav-list">
            <li>
                <a class="" href="#" title="Dashboard"><i class="fa fa-home"></i> Dashboard</a>
            </li>

            <li>
                <a class="" href="#" title="Orders"><i class="fa fa-film"></i> Orders</a>
                <ul>
                    <li><a href="#" title="Running Orders">Running Orders</a></li>
                    <li><a href="#" title="Pending Orders">Pending Orders</a></li>
                    <li><a href="#" title="Completed Orders">Completed Orders</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Business Packages"><i class="fa fa-female"></i> Business Packages</a>
                <ul>
                    <li><a href="#" title="Add Package">Add Package</a></li>
                    <li><a href="#" title="Active Packages">Active Packages</a></li>
                    <li><a href="#" title="Inactive Packages">Inactive Packages</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Advertises"><i class="fa fa-female"></i> Advertises</a>
                <ul>
                    <li><a href="#" title="Advertises Types">Advertises Types</a></li>
                    <li><a href="#" title="Advertise Packages">Advertise Packages</a></li>
                    <li><a href="#" title="Advertise Logs">Advertise Logs</a></li>
                </ul>
            </li>

            <li><a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Shops</a>
                <ul>
                    <li><a href="#" title="Active Shops">Active Shops</a></li>
                    <li><a href="#" title="Inactive Shops">Inactive Shops</a></li>
                    <li><a href="#" title="Pending Shops">Pending Shops</a></li>
                </ul>
            </li>

            <li><a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Resellers</a>
                <ul>
                    <li><a href="#" title="Add Reseller">Add Reseller</a></li>
                    <li><a href="#" title="Pending Resellers">Pending Resellers</a></li>
                    <li><a href="#" title="Active Resellers">Active Resellers</a></li>
                    <li><a href="#" title="Inactive Resellers">Inactive Resellers</a></li>
                </ul>
            </li>
            
            <li>
                <a class="" href="#" title="Riders"><i class="fa fa-repeat"></i> Riders</a>
                <ul>
                    <li><a href="#" title="Add Rider">Add Rider</a></li>
                    <li><a href="#" title="Pending Riders">Pending Riders</a></li>
                    <li><a href="#" title="Active Riders">Active Riders</a></li>
                    <li><a href="#" title="Inactive Riders">Inactive Riders</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Customers"><i class="fa fa-repeat"></i> Customers</a>
                <ul>
                    <li><a href="#" title="All Customers">Customers</a></li>
                    <li><a href="#" title="Customers Reviews">Customers Reviews</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Payment Reports"><i class="fa fa-repeat"></i> Payment Reports</a>
                <ul>
                    <li><a href="#" title="Orders wise">Orders wise</a></li>
                    <li><a href="#" title="Shops wise">Shops wise</a></li>
                    <li><a href="#" title="Resellers wise">Resellers wise</a></li>
                    <li><a href="#" title="Riders wise">Riders wise</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="#" title="Settings"><i class="fa fa-gears"></i> Settings</a>
                <ul>
                    <li><a href="#" title="Product Settings">Product Settings</a></li>
                    <li><a href="#" title="Company Settings">Company Settings</a></li>
                </ul>
            </li>
        </ul>
        
    </nav><!-- nav menu -->
</div><!-- topbar -->