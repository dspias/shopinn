<div class="fixed-sidebar left">
    <div class="menu-left">
        <ul class="left-menu">
            <li>
                <a class="menu-small" href="#" title="">
                    <i class="ti-menu"></i>
                </a>
            </li>
            
            <li>
                <a href="#" title="Pending Orders" data-toggle="tooltip" data-placement="right">
                    <i class="ti-magnet"></i>
                </a>
            </li>

            <li>
                <a href="#" title="Payment Reports" data-toggle="tooltip" data-placement="right">
                    <i class="ti-magnet"></i>
                </a>
            </li>

            <li>
                <a href="#" title="Pending Shops" data-toggle="tooltip" data-placement="right">
                    <i class="fa fa-forumbee"></i>
                </a>
            </li>

            <li>
                <a href="#" title="Pending Resellers" data-toggle="tooltip" data-placement="right">
                    <i class="ti-user"></i>
                </a>
            </li>

            <li>
                <a href="#" title="Pending Riders" data-toggle="tooltip" data-placement="right">
                    <i class="fa fa-star-o"></i>
                </a>
            </li>

            <li>
                <a href="#" title="Advertise Logs" data-toggle="tooltip" data-placement="right">
                    <i class="ti-comment-alt"></i>
                </a>
            </li>
            <li>
                <a href="#" title="Active Packages" data-toggle="tooltip" data-placement="right">
                    <i class="fa fa-bell-o"></i>
                </a>
            </li>
            
            <li>
                <a href="#" title="Customers Reviews" data-toggle="tooltip" data-placement="right">
                    <i class="ti-stats-up"></i>
                </a>
            </li>
        </ul>
    </div>


    {{-- main menu left side --}}
    <div class="left-menu-full">
        <ul class="menu-slide">
            <li><a class="closd-f-menu" href="#" title=""><i class="ti-close"></i> close Menu</a></li>
        </ul>
        
        <ul class="menu-slide">
            <li>
                <a class="" href="#" title="Dashboard"><i class="fa fa-home"></i> Dashboard</a>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Orders"><i class="fa fa-film"></i> Orders</a>
                <ul class="submenu">
                    <li><a href="#" title="Running Orders">Running Orders</a></li>
                    <li><a href="#" title="Pending Orders">Pending Orders</a></li>
                    <li><a href="#" title="Completed Orders">Completed Orders</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Business Packages"><i class="fa fa-female"></i> Business Packages</a>
                <ul class="submenu">
                    <li><a href="#" title="Add Package">Add Package</a></li>
                    <li><a href="#" title="Active Packages">Active Packages</a></li>
                    <li><a href="#" title="Inactive Packages">Inactive Packages</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Advertises"><i class="fa fa-female"></i> Advertises</a>
                <ul class="submenu">
                    <li><a href="#" title="Advertises Types">Advertises Types</a></li>
                    <li><a href="#" title="Advertise Packages">Advertise Packages</a></li>
                    <li><a href="#" title="Advertise Logs">Advertise Logs</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Shops</a>
                <ul class="submenu">
                    <li><a href="#" title="Active Shops">Active Shops</a></li>
                    <li><a href="#" title="Inactive Shops">Inactive Shops</a></li>
                    <li><a href="#" title="Pending Shops">Pending Shops</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title=""><i class="fa fa-graduation-cap"></i> Resellers</a>
                <ul class="submenu">
                    <li><a href="#" title="Add Reseller">Add Reseller</a></li>
                    <li><a href="#" title="Pending Resellers">Pending Resellers</a></li>
                    <li><a href="#" title="Active Resellers">Active Resellers</a></li>
                    <li><a href="#" title="Inactive Resellers">Inactive Resellers</a></li>
                </ul>
            </li>
            
            <li  class="menu-item-has-children">
                <a class="" href="#" title="Riders"><i class="fa fa-repeat"></i> Riders</a>
                <ul class="submenu">
                    <li><a href="#" title="Add Rider">Add Rider</a></li>
                    <li><a href="#" title="Pending Riders">Pending Riders</a></li>
                    <li><a href="#" title="Active Riders">Active Riders</a></li>
                    <li><a href="#" title="Inactive Riders">Inactive Riders</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Customers"><i class="fa fa-repeat"></i> Customers</a>
                <ul class="submenu">
                    <li><a href="#" title="All Customers">Customers</a></li>
                    <li><a href="#" title="Customers Reviews">Customers Reviews</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Payment Reports"><i class="fa fa-repeat"></i> Payment Reports</a>
                <ul class="submenu">
                    <li><a href="#" title="Orders wise">Orders wise</a></li>
                    <li><a href="#" title="Shops wise">Shops wise</a></li>
                    <li><a href="#" title="Resellers wise">Resellers wise</a></li>
                    <li><a href="#" title="Riders wise">Riders wise</a></li>
                </ul>
            </li>

            <li  class="menu-item-has-children">
                <a class="" href="#" title="Settings"><i class="fa fa-gears"></i> Settings</a>
                <ul class="submenu">
                    <li><a href="#" title="Product Settings">Product Settings</a></li>
                    <li><a href="#" title="Company Settings">Company Settings</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div><!-- left sidebar menu -->