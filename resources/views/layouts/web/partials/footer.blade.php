<a title="Your Cart Items" href="#" class="shopping-cart" data-toggle="tooltip">Cart <i class="fa fa-shopping-bag"></i><span>02</span></a>

<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Design By <a href="{{ 'http://shopinnbd.com/' }}" target="_blank">ShopInnbd</a></span>
                {{-- <i><img src="{{ asset('vendor/images/credit-cards.png') }}" alt=""></i> --}}
            </div>
        </div>
    </div>
</div>