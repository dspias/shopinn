@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shopinn, Shop Dashboard')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Dashboard')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Shop Dashboard') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Welcome to Shopinn BD') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gray-bg p-t-20">
			<div class="container">
				<div class="row m-b-80">
					<div class="col-lg-12">
                        <div class="row widget-page merged20" id="page-contents">
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Products') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-shopping-cart" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-success">{{ __($activeProduct) }} <sup class="text-shopinn">/{{ __($inactiveProduct) }}</sup></h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Order') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $order }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Pending Order') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $pendingOrder }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Today\'s Orders') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $todayOrder }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Sale') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($total_sale) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Commission') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($total_commission) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Paid by Shopinn') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($paid) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Payable by Shopinn') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($payable) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>



                            {{-- <div class="col-lg-3 m-t-15">
                                <aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">{{ __("Today's Sales") }}</h4>
                                        <div class="static-meta">
                                            <i class="fa fa-shopping-cart"></i>
                                            <div class="info-meta">
                                                <h4>334</h4>
                                                <i>30%</i>
                                            </div>
                                            <div class="spark">
                                                <div class="sparkline12"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget">
                                        <h4 class="widget-title">New Registers</h4>
                                        <span>This week</span>
                                        <div class="static-meta">
                                            <i class="fa fa-sign-in"></i>
                                            <div class="info-meta">
                                                <h4>66</h4>
                                                <i>10%</i>
                                            </div>
                                            <div class="spark">
                                                <div class="sparkline_three"></div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="widget stick-widget">
                                        <h4 class="widget-title">Shortcuts</h4>
                                        <ul class="naves">
                                            <li>
                                                <i class="ti-clipboard"></i>
                                                <a href="newsfeed.html" title="">News feed</a>
                                            </li>
                                            <li>
                                                <i class="ti-mouse-alt"></i>
                                                <a href="inbox.html" title="">Inbox</a>
                                            </li>
                                            <li>
                                                <i class="ti-files"></i>
                                                <a href="fav-page.html" title="">My pages</a>
                                            </li>
                                            <li>
                                                <i class="ti-user"></i>
                                                <a href="timeline-friends.html" title="">friends</a>
                                            </li>
                                            <li>
                                                <i class="ti-image"></i>
                                                <a href="timeline-photos.html" title="">images</a>
                                            </li>
                                            <li>
                                                <i class="ti-video-camera"></i>
                                                <a href="timeline-videos.html" title="">videos</a>
                                            </li>
                                            <li>
                                                <i class="ti-comments-smiley"></i>
                                                <a href="messages.html" title="">Messages</a>
                                            </li>
                                            <li>
                                                <i class="ti-bell"></i>
                                                <a href="notifications.html" title="">Notifications</a>
                                            </li>
                                            <li>
                                                <i class="ti-share"></i>
                                                <a href="people-nearby.html" title="">People Nearby</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-bar-chart-o"></i>
                                                <a href="insights.html" title="">insights</a>
                                            </li>
                                            <li>
                                                <i class="ti-power-off"></i>
                                                <a href="landing.html" title="">Logout</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Shortcuts -->
                                </aside>
                            </div>
                            <!-- sidebar widgets -->
                            <div class="col-lg-9 m-t-15">
                                <div class="central-meta">
                                    <h5 class="f-title">
                                        <i class="fa fa-line-chart"></i>Statistics <span class="more-options"><i class="fa fa-ellipsis-h"></i></span>
                                    </h5>
                                    <div class="insight-box">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Line Graph</h2>
                                                <div id="echart_line"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Line Graph</h2>
                                            </div>
                                            <ul class="toolbox">
                                                <li>
                                                    <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#">Settings 1</a></li>
                                                        <li><a href="#">Settings 2</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div class="x_content">
                                                <div id="main"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Bar Graph</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="mainb"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Mini Pie</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_mini_pie"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Pie Graph</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_pie"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Pie Area</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_pie2"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Donut Graph</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_donut"></div>
                                            </div>
                                        </div>

                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Horizontal Bar</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_bar_horizontal"></div>
                                            </div>
                                        </div>
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>World Map</h2>
                                                <ul class="toolbox">
                                                    <li>
                                                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a></li>
                                                            <li><a href="#">Settings 2</a></li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a class="close-link"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="x_content">
                                                <div id="echart_world_map"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <!-- centerl meta -->
                        </div>
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('vendor/js/echarts.min.js') }}"></script>
    <script src="{{ asset('vendor/js/world.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.sparkline.min.js') }}"></script>
	<script src="{{ asset('vendor/js/custom.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




