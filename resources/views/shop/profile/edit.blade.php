@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shopinn, Shop Profile')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Profile | Edit Profile')

{{-- page src stylesheets here --}}
@section('page_src_styles')

    <!-- font -->
    <link href="{{ asset('fileuploader/font/font-fileuploader.css') }}" media="all" rel="stylesheet">

    <!-- css -->
    <link href="{{ asset('fileuploader/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .c-form {
        display: flex;
    }

    .profile-controls{
        background: none;
    }
    .profile-menu{
        width: auto;
    }
    textarea{
        /* box-sizing: padding-box; */
        overflow:hidden;
        /* demo only: */
        font-size:14px;
        margin:50px auto;
        display:block;
    }
    .note-editor.note-frame{
        border: 1px solid #efefef;
    }
    .note-toolbar-wrapper{
        border-top: 1px solid #efefef;
    }
    .note-statusbar{
        display: none;
    }

    .attachments li.preview-btn {
        width: 100%;
    }
    /* .collapse.show{
        margin-right: 50px;
    } */
    .more-details-body{
        border: 1px solid #efefef;
        /* margin-right: 20px; */
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 14px 15px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 20px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 12px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 5px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }


    /* FileUploader  */
    .fileuploader-input .fileuploader-input-caption{
        color: #ff084e;
    }
    .fileuploader-input .fileuploader-input-button:active,
    .fileuploader-input .fileuploader-input-button:hover,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:active,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:hover{
        box-shadow: none;
        transform: none;
    }
    .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
        background: #ff084e !important;
    }

    .tag-col .chosen-container.chosen-container-multi{
        display: none !important;
    }
    .selectize-input {
        border: 1px solid #dddddd;
        padding: 15px 10px;
        box-shadow: none !important;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px;
    }
    .selectize-control.multi .selectize-input [data-value]{
        border-color: #ff084e !important;
        background: #ff084e !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row merged20" id="page-contents">

                <div class="user-profile">
                    <figure>
                        <img src="{{ asset('vendor/images/resources/profile-image.jpg') }}" alt="">
                        <ul class="profile-controls">
                            <li>
                                <a href="#" title="" data-toggle="tooltip" data-original-title="{{ __('Edit Profile Info') }}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                        </ul>
                    </figure>

                    @auth
                        <div class="profile-section">
                            <div class="row">
                                <div class="col-lg-2 col-md-3">
                                    <div class="profile-author">
                                        <a class="profile-author-thumb" href="{{ route('shop.profile.index') }}">
                                            <img alt="author" src="{{ asset('vendor/images/resources/author.jpg') }}">
                                        </a>
                                        <div class="author-content m-t-40">
                                            <a class="h3 author-name" href="{{ route('shop.profile.index') }}">{{ __('Shop_Name_Here') }}</a>
                                            {{-- <div class="country">Ontario, CA</div> --}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-10 col-md-9">
                                    <ul class="profile-menu">
                                        <li><a href="#" title="{{ __('All Orders') }}"><i class="fa fa-bullhorn"></i> {{ __('All Orders') }}</a></li>
                                    </ul>
                                    <ul class="align-right user-ben m-t-15">
                                        <li class="search-for">
                                            <a data-ripple="" class="circle-btn search-data" title="" href="#"><i class="ti-search"></i></a>
                                            <form class="searchees" method="post">
                                                <span class="cancel-search"><i class="ti-close"></i></span>
                                                <input type="text" placeholder="Search in Posts">
                                                <button type="submit"></button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endauth
                </div><!-- user profile banner  -->

                <div class="col-lg-3">
                    <form action="#" method="get">
                        @csrf
                        <aside class="sidebar static left">
                            {{-- Filter Posts --}}
                            <div class="widget">
                                <h4 class="widget-title">Filter Posts</h4>
                                <ul class="widget-body">
                                    <p class="text-dark m-b-0">{{ __('Filter Your Posts') }}</p>
                                </ul>
                            </div>
                            {{-- Filter Posts Ends --}}


                            {{-- My Categories --}}
                            <div class="widget">
                                <h4 class="widget-title">{{ __('My Categories') }}</h4>
                                <ul class="widget-body">
                                    <li>
                                        <div class="checkbox m-0">
                                            <label>
                                            <input type="checkbox" name="category[]"><i class="check-box"></i>{{ 'Category One' }}
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox m-0">
                                            <label>
                                            <input type="checkbox" name="category[]"><i class="check-box"></i>{{ 'Category Two' }}
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox m-0">
                                            <label>
                                            <input type="checkbox" name="category[]"><i class="check-box"></i>{{ 'Category Three' }}
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            {{-- My Categories Ends --}}


                            {{-- Price Range --}}
                            <div class="widget">
                                <h4 class="widget-title m-b-10">{{ __('Price Range') }}</h4>
                                <div class="widget-body c-form p-10 p-t-0">
                                    <div class="row p-b-20">
                                        <div class="col-md-6">
                                            <input type="number" name="price_min" placeholder="Min" min="0" class="bg-transparent">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" name="price_max" placeholder="Max" min="0" class="bg-transparent">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Price Range Ends --}}


                            <div class="widget stick-widget bg-transparent">
                                <div class="widget-body bg-transparent">
                                    <button class="btn btn-danger btn-shopinn btn-block text-center">{{ __('Filter') }}</button>
                                </div>
                            </div>
                        </aside>
                    </form>
                </div><!-- sidebar -->



                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-12">
                            <div class="central-meta postbox">
                                <span class="create-post">{{ __('Create Product') }}</span>
                                <form action="#" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="new-postbox">
                                        <div class="newpst-input" style="width: 100%;">
                                            <textarea class="summernote-simple @error('description') is-invalid @enderror" name="description" required>{{ old('description') }}</textarea>

                                            @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="collapse" id="more_details">
                                            <div class="card card-body more-details-body p-r-0">
                                                <div class="row c-form">
                                                    <div class="col-12 m-b-10">
                                                        <input autocomplete="off" type="text" name="name" class="border bg-transparent @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Product Name *') }}">

                                                        @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="stock" min="0" class="border bg-transparent @error('stock') is-invalid @enderror" value="{{ old('stock') }}" placeholder="{{ __('Stock Available *') }}">

                                                        @error('stock')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="buy_price" min="0" step="0.1" class="border bg-transparent @error('buy_price') is-invalid @enderror" value="{{ old('buy_price') }}" placeholder="{{ __('Purchase Price') }}">

                                                        @error('buy_price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="sell_price" min="0" step="0.1" class="border bg-transparent @error('sell_price') is-invalid @enderror" value="{{ old('sell_price') }}" placeholder="{{ __('Expected Selling Price *') }}">

                                                        @error('sell_price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-6 m-b-10">
                                                        <select data-placeholder="{{ __('Select Product Type *') }}" class="chosen-select" required name="type">
                                                            <option aria-readonly="true" aria-disabled="true">{{ __('Select Product Type *') }}</option>
                                                            <option value="Product_Type">Product_Type</option>
                                                        </select>
                                                    </div>


                                                    <div class="col-md-6 m-b-10">
                                                        <select data-placeholder="{{ __('Select Category *') }}" class="chosen-select" required name="category">
                                                            <option aria-readonly="true" aria-disabled="true">{{ __('Select Category *') }}</option>
                                                            <option value="Product_category">Product_category</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6 m-b-10">
                                                        <select data-placeholder="{{ __('Select Size\'s') }}" class="chosen-select multi" multiple required name="size">
                                                            {{-- <option aria-readonly="true" aria-disabled="true">{{ __('Select Size') }}</option> --}}
                                                            <option value="Free Size">Free Size</option>
                                                            <option value="L">L</option>
                                                            <option value="Large">Large</option>
                                                            <option value="M">M</option>
                                                            <option value="Medium">Medium</option>
                                                            <option value="None">None</option>
                                                            <option value="S">S</option>
                                                            <option value="Small">Small</option>
                                                            <option value="XL">XL</option>
                                                            <option value="XS">XS</option>
                                                            <option value="XXL">XXL</option>
                                                            <option value="XXXL">XXXL</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6 m-b-10 tag-col">
                                                        <select id="selectize-tags-1" data-placeholder="{{ __('Select Color\'s') }}" name="color" multiple class="item-info">
                                                            {{-- <option disabled selected>{{ __('Select Color\'s') }}</option> --}}
                                                            <option value="1">Adam</option>
                                                            <option value="2">Amalie</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <input type="file" name="files" class="files" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 m-b-10">
                                                <div class="attachments">
                                                    <ul>
                                                        <li class="preview-btn">
                                                            <span class="add-loc">
                                                                <small>
                                                                    <a class="btn btn-more-details btn-shopinn-outline btn-block m-t-5" data-toggle="collapse" href="#more_details" aria-expanded="false" aria-controls="more_details">
                                                                        {{-- <i class="fa fa-plus"></i> --}}
                                                                        {{ __('Advance Option') }}
                                                                    </a>
                                                                </small>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <button class="btn btn-shopinn btn-block" type="submit">{{ __('Add Product') }}</button>
                                                {{-- <button class="btn btn-shopinn btn-block" type="submit">{{ __('Send Post For Approval') }}</button> --}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- add post new box -->
                        </div>
                    </div>

                    <div class="load-more">
                        <div class="central-meta">
                            <div class="row merged20 remove-ext">
                                <div class="col-lg-3 col-md-4 col-sm-4 item">
                                    <div class="pitdate-user">
                                        <figure><img src="{{ asset('vendor/images/resources/date-user1.jpg') }}" alt="">
                                            <div class="likes heart" title="Rating"><i class="fa fa-star color-orange"></i> <span class="m-t-1"><b>4.80</b></span></div>
                                            <div class="more">
                                                <div class="more-post-optns">
                                                    <i class="ti-more-alt"></i>
                                                    <ul>
                                                        <li class="send-mesg"><a href="#"><i class="fa fa-info"></i>Details</a></li>
                                                        <li class="share-pst"><i class="fa fa-share-alt"></i>Share</li>
                                                        <li class="get-link"><i class="fa fa-link"></i>Copy Link</li>
                                                        <li class="bad-report"><i class="fa fa-trash"></i>Delete</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </figure>
                                        <h4><a href="{{ route('guest.product.show', ['product_id' => 1, 'product_name' => 'productname']) }}" title="">{{ 'Product_Name_Here' }}</a></h4>
                                        <span><i class="fa fa-map-marker"></i> Canada</span>
                                        <a href="#" title="Add Friend"><i class="fa fa-plus-circle"></i></a>
                                        {{-- <a href="#" title="Add Friend"><i class="fa fa-plus-circle"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('fileuploader/jquery.fileuploader.min.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            // enable fileuploader plugin
            $('input[name="files"]').fileuploader({
                limit: 4,
                maxSize: 50,

                extensions: ["image/*"],
                addMore: true,
                thumbnails: {
                    onItemShow: function (item) {
                        // add sorter button to the item html
                        item.html.find(".fileuploader-action-remove").after('<button type="button" class="fileuploader-action fileuploader-action-sort" title="Sort"><i class="fileuploader-icon-sort"></i></button>');
                    },
                    onImageLoaded: function (item) {
                        if (!item.html.find(".fileuploader-action-edit").length)
                            item.html.find(".fileuploader-action-remove").after('<button type="button" class="fileuploader-action fileuploader-action-popup fileuploader-action-edit" title="Edit"><i class="fileuploader-icon-edit"></i></button>');
                    },
                },
                editor: {
                    cropper: {
                        // ratio: "1:1",
                        minWidth: 100,
                        minHeight: 100,
                        showGrid: true,
                    },
                },
                sorter: {
                    selectorExclude: null,
                    placeholder: null,
                    scrollContainer: window,
                    onSort: function (list, listEl, parentEl, newInputEl, inputEl) {
                        // onSort callback
                    },
                },
            });


            $('.fileuploader-input-caption span').html('Choose or Drag & Drop Photos to Upload');
        });
    </script>

    <script>
        var textarea = document.querySelector('textarea');

        textarea.addEventListener('keydown', autosize);

        function autosize(){
        var el = this;
        setTimeout(function(){
                el.style.cssText = 'height:auto; padding:0';
                // for box-sizing other than "content-box" use:
                // el.style.cssText = '-moz-box-sizing:content-box';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
            },0);
        }
    </script>
@endsection




