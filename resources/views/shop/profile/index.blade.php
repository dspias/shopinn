@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shopinn, Shop Profile')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Profile')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/bundles/summernote/summernote-bs4.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />

    <!-- font -->
    <link href="{{ asset('fileuploader/font/font-fileuploader.css') }}" media="all" rel="stylesheet">

    <!-- css -->
    <link href="{{ asset('fileuploader/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .c-form {
        display: flex;
    }
    .c-form>div input, .c-form>div textarea, .c-form>input, .c-form>textarea {
        font-size: 13px;
        padding: 3px 15px;
    }

    .profile-controls{
        background: none;
    }
    .profile-menu{
        width: auto;
    }
    textarea{
        /* box-sizing: padding-box; */
        overflow:hidden;
        /* demo only: */
        font-size:14px;
        margin:50px auto;
        display:block;
    }
    .note-editor.note-frame{
        border: 1px solid #efefef;
    }
    .note-toolbar-wrapper{
        border-top: 1px solid #efefef;
    }
    .note-statusbar{
        display: none;
    }

    .attachments li.preview-btn {
        width: 100%;
    }
    /* .collapse.show{
        margin-right: 50px;
    } */
    .more-details-body{
        border: 1px solid #efefef;
        /* margin-right: 20px; */
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 14px 15px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 3px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 32px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 4px 14px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: -3px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }


    /* FileUploader  */
    .fileuploader-input .fileuploader-input-caption{
        color: #ff084e;
    }
    .fileuploader-input .fileuploader-input-button:active,
    .fileuploader-input .fileuploader-input-button:hover,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:active,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:hover{
        box-shadow: none;
        transform: none;
    }
    .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
        background: #ff084e !important;
    }

    .tag-col .chosen-container.chosen-container-multi{
        display: none !important;
    }
    .selectize-input {
        border: 1px solid #dddddd;
        padding: 6px 10px;
        box-shadow: none !important;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 6px 8px;
    }
    .selectize-control.multi .selectize-input [data-value]{
        border-color: #ff084e !important;
        background: #ff084e !important;
    }
    .chosen-container.chosen-container-multi{
        display: none !important;
    }
    .selectize-control.multi .selectize-input.has-items {
        padding: 4px 8px 4px;
    }
    .selectize-control.multi .selectize-input > div{
        padding: 0 6px;
        margin: 0 3px 2px 0;
    }



    .select2-container .select2-selection--single{
        height: 30px;
    }
    ..select2-container--default .select2-selection--single,
    .select2 select2-container.select2-container--default.select2-container--below.select2-container--focus,
    .select2-container--default .select2-selection--single{
        border: 1px solid #dee2e6 !important;
        outline: none !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #dee2e6 !important;
        border-radius: 4px;
        outline: none !important;
    }
    .select2 select2-container.select2-container--default.select2-container--below,
    .select2-container{
        width: 100% !important;
    }

    .chosen-container.chosen-container-single{
        display: none !important;
    }
    .select2-container--default .select2-results__option--selected,
    .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable{
        background-color: #ff084e;
        color: #ffffff;
    }
    .profile-menu>li>a.active::after{
        display: none !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row merged20" id="page-contents">

                <div class="user-profile">
                    @php
                        $logo = get_logo(auth()->user(), 'thumb');
                        if($logo == null) $logo = asset('vendor/images/resources/author.jpg');

                        $cover = get_cover(auth()->user(), 'profile');
                        if($cover == null) $cover = asset('vendor/images/resources/profile-image.jpg');
                    @endphp
                    <figure>
                        <img src="{{ $cover }}" alt="">
                        <ul class="profile-controls">
                            <li>
                                <a href="{{ route('shop.setting.shop.index') }}" title="" data-toggle="tooltip" data-original-title="{{ __('Edit Profile Info') }}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                        </ul>
                    </figure>

                    @auth
                        <div class="profile-section">
                            <div class="row">
                                <div class="col-lg-2 col-md-3">
                                    <div class="profile-author">
                                        <a class="profile-author-thumb" href="{{ route('shop.profile.index') }}">
                                            <img alt="author" src="{{ $logo }}">
                                        </a>
                                        <div class="author-content m-t-40">
                                            <a class="h3 author-name" href="{{ route('shop.profile.index') }}">{{ auth()->user()->name }}</a>
                                            {{-- <div class="country">Ontario, CA</div> --}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-10 col-md-9">
                                    <ul class="profile-menu">
                                        <li>
                                            <a href="{{ route('shop.profile.index') }}" title="{{ __('Profile') }}" class="{{ Request::is('shop/profile*') ? 'active' : '' }}">
                                                <i class="ti-user"></i> {{ __('Profile') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('shop.order.index') }}" title="{{ __('Orders') }}" class="{{ Request::is('shop/order*') ? 'active' : '' }}">
                                                <i class="fa fa-bullhorn"></i> {{ __('Orders') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('shop.product.index') }}" title="{{ __('Products') }}" class="{{ Request::is('shop/product*') ? 'active' : '' }}">
                                                <i class="ti-shopping-cart"></i> {{ __('Products') }}
                                            </a>
                                        </li>
                                    </ul>
                                    {{--  <ul class="align-right user-ben m-t-15">
                                        <li class="search-for">
                                            <a data-ripple="" class="circle-btn search-data" title="" href="#"><i class="ti-search"></i></a>
                                            <form class="searchees" method="post">
                                                <span class="cancel-search"><i class="ti-close"></i></span>
                                                <input type="text" placeholder="Search in Posts">
                                                <button type="submit"></button>
                                            </form>
                                        </li>
                                    </ul>  --}}
                                </div>
                            </div>
                        </div>
                    @endauth
                </div><!-- user profile banner  -->
                        
                <div class="col-lg-3">
                    <aside class="sidebar static left">
                        <!-- recent post-->
                        <div class="advertisment-box">
                            @php
                                $ad1 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad1))
                            <h4 class="">{{ __('Advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ image($ad1->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        <!-- ad banner -->
                        <!-- recent post-->
                        <div class="advertisment-box">
                            @php
                                $ad2 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad2))
                            <h4 class="">{{ __('Advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ image($ad2->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        <!-- ad banner -->
                        <!-- recent post-->
                        <div class="advertisment-box">
                            @php
                                $ad3 = ad(796, 485);
                            @endphp
                            @if(!is_null($ad3))
                            <h4 class="">{{ __('Advertisment') }}</h4>
                            <figure>
                                <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ image($ad3->ad_image) }}" alt=""></a>
                                {{--  <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                            </figure>
                            @endif
                        </div>
                        <!-- ad banner -->
                    </aside>
                </div>
                <!-- sidebar -->



                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-12">
                            <div class="central-meta postbox">
                                <span class="create-post">{{ __('Create Product') }}</span>
                                <form action="{{ route('shop.profile.product.store') }}" method="post" enctype="multipart/form-data" class="create_product">
                                    @csrf
                                    <div class="new-postbox">
                                        <div class="newpst-input" style="width: 100%;">
                                            <textarea class="summernote-simple @error('description') is-invalid @enderror" name="description">{{ old('description') }}</textarea>

                                            @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="details-optn">
                                        {{-- <div class="collapse" id="more_details"> --}}
                                            <div class="card card-body more-details-body p-r-0">
                                                <div class="row c-form">
                                                    <div class="col-12 m-b-10">
                                                        <input autocomplete="off" type="text" name="name" class="border bg-transparent @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Product Name *') }}">

                                                        @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="stock" min="0" class="border bg-transparent @error('stock') is-invalid @enderror" value="{{ old('stock') }}" placeholder="{{ __('Stock Available *') }}">

                                                        @error('stock')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="buy_price" min="0" step="1" class="border bg-transparent @error('buy_price') is-invalid @enderror" value="{{ old('buy_price') }}" placeholder="{{ __('Purchase Price') }}">

                                                        @error('buy_price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-4 m-b-10">
                                                        <input autocomplete="off" type="number" name="sell_price" min="0" step="1" class="border bg-transparent @error('sell_price') is-invalid @enderror" value="{{ old('sell_price') }}" placeholder="{{ __('Expected Selling Price *') }}">

                                                        @error('sell_price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>


                                                    <div class="col-md-6 m-b-10">
                                                        <select name="type_id" id="pp_type" class="form-control @error('type_id') is-invalid @enderror">
                                                            <option value="">{{ __('Select Product Type *') }}</option>
                                                            @php
                                                                $categories = array();
                                                            @endphp
                                                            @foreach($types as $type)
                                                                @php
                                                                    foreach($type->categories as $cat){
                                                                        $categories[$type->id][] = array(
                                                                                                    'id' => $cat->id,
                                                                                                    'name'=> $cat->name,
                                                                                                );
                                                                    }
                                                                @endphp
                                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('type_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-6 m-b-10">
                                                        <select name="cat_id" id="subtype"  class="form-control @error('cat_id') is-invalid @enderror">
                                                            <option value="">{{ __('Select Product Category *') }}</option>
                                                        </select>

                                                        @error('cat_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>


                                                    <div class="col-md-6 m-b-10">
                                                        <select class="selectize-tags item-info @error('sizes') is-invalid @enderror" data-placeholder="{{ __('Select Size\'s') }}" name="sizes[]" multiple>
                                                            {{-- <option aria-readonly="true" aria-disabled="true">{{ __('Select Size') }}</option> --}}
                                                            @foreach($sizes as $size)
                                                            <option value="{{ $size->name }}">{{ $size->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('sizes')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-6 m-b-10 tag-col">
                                                        <select class="selectize-tags item-info @error('colors') is-invalid @enderror" data-placeholder="{{ __('Select Color\'s') }}" name="colors[]" multiple >
                                                            {{-- <option disabled selected>{{ __('Select Color\'s') }}</option> --}}
                                                            @foreach($colors as $color)
                                                            <option value="{{ $color->name }}">{{ $color->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('colors')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-md-12 m-b-10 tag-col">
                                                        <select class="selectize-tags item-info @error('tags') is-invalid @enderror" data-placeholder="{{ __('Select Tag\'s') }}" name="tags[]" multiple >
                                                            {{-- <option disabled selected>{{ __('Select Color\'s') }}</option> --}}
                                                            @foreach($tags as $tag)
                                                            <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('tags')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>

                                                    <div class="col-md-12">
                                                        <input type="file" name="files" class="files @error('files') is-invalid @enderror" accept="image/*">

                                                        <small class="text-shopinn">({{ __('You can Upload Maximum 4 & Minimum 1 Photo.') }})</small>

                                                        @error('files')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            {{-- <div class="col-md-3 m-b-10">
                                                <div class="attachments">
                                                    <ul>
                                                        <li class="preview-btn">
                                                            <span class="add-loc">
                                                                <small>
                                                                    <a class="btn btn-more-details btn-shopinn-outline btn-block m-t-5" data-toggle="collapse" href="#more_details" aria-expanded="false" aria-controls="more_details">
                                                                        {{ __('Advance Option') }}
                                                                    </a>
                                                                </small>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> --}}
                                            <div class="col-md-12">
                                                <button class="btn btn-shopinn btn-block" type="submit">{{ __('Add Product') }}</button>
                                                {{-- <button class="btn btn-shopinn btn-block" type="submit">{{ __('Send Post For Approval') }}</button> --}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- add post new box -->
                        </div>
                    </div>

                    <div class="load-mores">
                        <div class="central-meta">
                            <div class="row merged20 remove-ext">
                                @foreach($products as $product)
                                <div class="col-md-3 col-sm-6 col-sx-6 item">
                                    <div class="pitdate-user">
                                         {{-- <figure><img src="{{ asset('vendor/images/resources/date-user1.jpg') }}" alt="">  --}}
                                            @php
                                                $url = null;
                                                $avatar = $product->getFirstMedia('product');
                                                if($avatar != null) $url = $avatar->getUrl('card');
                                                $str = str_replace(" ","-",$product->name);
                                            @endphp
                                        <figure>
                                            @if ($url != null)
                                                <img src="{{ $url }}" alt="Image Not Found">
                                            @else
                                                <img src="{{ asset('vendor/images/resources/date-user1.jpg') }}" alt="">
                                            @endif

                                            <div class="likes heart" title="Rating">
                                                <i class="fa fa-star color-orange"></i>
                                                <span class="m-t-1">
                                                    <b>{{ get_avarage_rating($product->ratings) }}</b>
                                                </span>
                                            </div>
                                            {{-- <div class="more">
                                                <div class="more-post-optns">
                                                    <i class="ti-more-alt"></i>
                                                    <ul>
                                                        <li class="send-mesg">
                                                            <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => $str]) }}">
                                                                <i class="fa fa-info"></i>
                                                                Details
                                                            </a>
                                                        </li>
                                                        <li class="get-link">
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="{{ __('Copy Share Link') }}" id="copyURL" class="js-copy">
                                                                <i class="fa fa-link"></i>Copy Link
                                                            </a>
                                                        </li>
                                                        <li class="bad-report"><i class="fa fa-trash"></i>Delete</li>
                                                    </ul>
                                                </div>
                                            </div> --}}
                                        </figure>

                                        <h4>
                                            <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => $str]) }}" class="title-1-line">{{ $product->name }}</a>
                                        </h4>

                                        <span class="text-shopinn text-bold">
                                            <i class="text-shopinn text-bold">৳</i>
                                            {{ __($product->sell_price) }}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>

    <script src="{{ asset('assets/bundles/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ asset('fileuploader/jquery.fileuploader.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function(){
            $('[data-tooltip="tooltip"]').tooltip();


            $('.selectize-tags').selectize({
                delimiter: ',',
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            // enable fileuploader plugin
            $('input[name="files"]').fileuploader({
                limit: 4,
                maxSize: 50,

                extensions: ["image/*"],
                addMore: true,
                thumbnails: {
                    onItemShow: function (item) {
                        // add sorter button to the item html
                        item.html.find(".fileuploader-action-remove").after('<button type="button" class="fileuploader-action fileuploader-action-sort" title="Sort"><i class="fileuploader-icon-sort"></i></button>');
                    },
                    onImageLoaded: function (item) {
                        if (!item.html.find(".fileuploader-action-edit").length)
                            item.html.find(".fileuploader-action-remove").after('<button type="button" class="fileuploader-action fileuploader-action-popup fileuploader-action-edit" title="Edit"><i class="fileuploader-icon-edit"></i></button>');
                    },
                },
                // editor: {
                //     cropper: {
                //         // ratio: "1:1",
                //         minWidth: 100,
                //         minHeight: 100,
                //         showGrid: true,
                //     },
                // },
                sorter: {
                    selectorExclude: null,
                    placeholder: null,
                    scrollContainer: window,
                    onSort: function (list, listEl, parentEl, newInputEl, inputEl) {
                        // onSort callback
                    },
                },
            });


            $('.fileuploader-input-caption span').html('Choose or Drag & Drop Photos to Upload');
        });
    </script>

    <script>
        $(document).ready(function() {
            // code here
            $(".summernote-simple").summernote({
                placeholder: '{{ __("Write Product Description *") }}',
                dialogsInBody: true,
                minHeight: 100,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                ]
            });
            //$('.note-codable').attr('required', true).attr('data-summernote', true);
        });
    </script>

    <script>
        var textarea = document.querySelector('textarea');

        textarea.addEventListener('keydown', autosize);

        function autosize(){
        var el = this;
        setTimeout(function(){
                el.style.cssText = 'height:auto; padding:0';
                // for box-sizing other than "content-box" use:
                // el.style.cssText = '-moz-box-sizing:content-box';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
            },0);
        }
    </script>


    <script>
        var Select2Cascade = ( function(window, $) {

            function Select2Cascade(parent, child, categories, select2Options) {
                var afterActions = [];
                var options = select2Options || {};

                // Register functions to be called after cascading data loading done
                this.then = function(callback) {
                    afterActions.push(callback);
                    return this;
                };

                parent.select2(select2Options).on("change", function (e) {

                    child.prop("disabled", true);

                    var _this = this;
                    var type = $(_this).val();
                    var newOptions = '<option value="">{{ __("Select Product Category *") }}</option>';
                    var items = [];
                    if(!isNaN(type)){
                        for(var i=0; i < categories[type].length; i++){
                            items[i] = categories[type][i].name;
                            newOptions += '<option value="'+ categories[type][i].id +'">'+ categories[type][i].name +'</option>';
                        }
                        child.select2('destroy').html(newOptions).prop("disabled", false)
                            .select2(options);

                        afterActions.forEach(function (callback) {
                            callback(parent, child, items);
                        });
                    }
                });
            }

            return Select2Cascade;

        })( window, $);

        $(document).ready(function() {
            var select2Options = { width: 'resolve' };
            let categories = JSON.parse('@php echo json_encode($categories); @endphp');

            $('#pp_type').select2(select2Options);
            $('#subtype').select2(select2Options);
            var cascadLoading = new Select2Cascade($('#pp_type'), $('#subtype'), categories, select2Options);
            cascadLoading.then( function(parent, child, items) {
                // Dump response data
                // console.log(items);
            });
        });


    </script>
@endsection




