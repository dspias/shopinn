@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Under Review Products')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 7px 10px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 10px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 37px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 0px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }
    .friend-block>figure img {
        border-radius: 0% !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Under Review products') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Under Review products of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="central-meta">
                            <div class="title-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="align-left m-t-3">
                                            <h5>{{ __('Total Products') }} <span class="m-t-m-3 bg-shopinn">{{ __($products->count()) }}</span></h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row merged20">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <form method="get" action="#">
                                                    <input type="text" id="search-products" placeholder="{{ __('Search Product') }}" />
                                                    <button type="submit"><i class="fa fa-search"></i></button>
                                                </form>
                                            </div>
                                            {{-- <div class="col-lg-5 col-md-5 col-sm-5">
                                                <div class="select-options">
                                                    <select class="select">
                                                        <option value="all">{{ __('All Products') }}</option>
                                                        <option value="review">{{ __('Under Review Products') }}</option>
                                                        <option value="featured">{{ __('Featured Products') }}</option>
                                                        <option value="active">{{ __('Active Products') }}</option>
                                                        <option value="inactive">{{ __('Inactive Products') }}</option>
                                                    </select>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- title block -->
                        <div class="central-meta padding30">
                            <div class="row merged20">
                                @foreach($products as $product)
                                <div class="col-lg-3 col-md-6 col-sm-6 product">
                                    <div class="friend-block product-data">
                                        <div class="more-opotnz">
                                            <i class="fa fa-ellipsis-h"></i>
                                            <ul>
                                                <li><a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($product->name)]) }}">{{ __('Details') }}</a></li>
                                                <li><a class="confirmation" href="{{ route('shop.product.change_featured_status', ['product_id' => encrypt($product->id)]) }}">{{ __('Change Feature') }}</a></li>
                                                <li><a href="{{ route('shop.product.edit', ['product_id' => encrypt($product->id)]) }}">{{ __('Edit Product') }}</a></li>
                                                <li>
                                                    @if($product->is_active == 1)
                                                        <a href="{{ route('shop.product.change_status', ['product_id' => encrypt($product->id)]) }}" class="text-danger confirmation">
                                                            {{ __('Deactive Product') }}
                                                        </a>
                                                    @else
                                                        <a href="{{ route('shop.product.change_status', ['product_id' => encrypt($product->id)]) }}" class="text-success confirmation">
                                                            {{ __('Active Product') }}
                                                        </a>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                        <figure>
                                            <img class="img-thumbnail rounded img-responsive" src="{{ get_image($product, 'thumb') }}" alt="Image Not Found"/>
                                            <p class="d-none">{{ $product->pid }} </p>
                                            <p class="d-none">{{ $product->category->name }} </p>
                                            <p class="d-none">{{ $product->category->type->name }} </p>
                                        </figure>

                                        <div class="frnd-meta">
                                            <div class="frnd-name">
                                                <a href="{{ route('guest.product.show', ['product_id'=> $product->id, 'product_name' => get_name($product->name)]) }}">{{ __($product->name) }}</a>
                                                @if($product->approved_at == null)
                                                    <span class="text-warning">{{ ('Under Review') }}</span>
                                                @elseif($product->is_active == 0)
                                                    <span class="text-danger">{{ ('Inactive') }}</span>
                                                @elseif($product->is_featured == 1)
                                                    <span class="text-primary">{{ ('Featured') }}</span>
                                                @else
                                                    <span class="text-success">{{ ('Active') }}</span>
                                                @endif
                                                <br><span>{{ __('Stock')." ".$product->stock }}</span><br>
                                            </div>
                                            <a class="send-mesg"><i style="font-size: 12px; font-weight: 700;">৳</i> {{ $product->sell_price }}</a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    // search
    <script>
        $(document).ready(function(){
            var searchProducts = $('#search-products'),
            products = document.querySelectorAll('.product'),
            productsData = document.querySelectorAll('.product-data'),
            searchVal;

            searchProducts.on('input', function() {
                searchVal = this.value.toLowerCase();
                
                for (var i = 0; i < products.length; i++) {
                    if(searchVal == '') {
                        products[i].style['display'] = 'flex';
                    }
                    else if (!searchVal || productsData[i].textContent.toLowerCase().indexOf(searchVal) > -1) {
                        products[i].style['display'] = 'flex';
                    }
                    else {
                        products[i].style['display'] = 'none';
                    }
                }
              });

        });
    </script>
@endsection




