<div class="modal fade" id="packageRegisterModal">
    <div class="modal-dialog" style="max-width: 60%;">
        <form action="{{ route('shop.setting.advertise.package.register.store') }}" method="post">
            @csrf
            <div class="modal-content">
                <input type="hidden" name="package_id" id="package_id" value="">
                <input type="hidden" name="package_price_original" id="package_price_original" value="">
                <input type="hidden" name="package_price_hidden" id="package_price_hidden" value="">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">
                        <span id="package_name">{{ __('Package_Name_Here') }}</span>
                        <span class="text-shopinn">(<span id="package_price">{{ __('1000') }}</span><sup class="text-bold">৳</sup>)</span>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-6 mx-auto">
                            <div class="pt_Quantity">
                                <label for="day">{{ __('How Many Days?') }} <sup class="text-shopinn"><b>*</b></sup></label>
                                <input type="number" min="1" max="30" step="1" value="1" data-inc="1" name="day" class="day-input" placeholder="Days" readonly required>
                            </div>
                        </div>
                    </div>
                    <h3 class="text-shopinn text-center">{{ __('Select Payment Method') }}</h3>
                    <div class="payment-method m-b-20">
                        <h4 class="create-post"></h4>
                        <ul class="payment-options text-center">
                            <li class="options">
                                <input type="radio" id="pay_by_bkash" name="pay_by" value="bkash" required disabled/>
                                <label class="text-capitalize m-b-0" for="pay_by_bkash">{{ __('bKash') }}</label>
                            </li>
                            <li class="options">
                                <input type="radio" id="pay_by_rocket" name="pay_by" value="rocket" required disabled/>
                                <label class="text-capitalize m-b-0" for="pay_by_rocket">{{ __('Rocket') }}</label>
                            </li>
                            <li class="options">
                                <input type="radio" id="pay_by_card" name="pay_by" value="card" required disabled/>
                                <label class="text-capitalize m-b-0" for="pay_by_card">{{ __('Card') }}</label>
                            </li>
                            <li class="options">
                                <input type="radio" id="request_shopinn" name="pay_by" value="shopinn" required checked/>
                                <label class="text-capitalize m-b-0" for="request_shopinn">{{ __('Request To Shopinn') }}</label>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-shopinn" type="submit">{{ __('Register Package') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
