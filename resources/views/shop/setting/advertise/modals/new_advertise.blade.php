<div class="modal fade" id="newAdvertise">
    <div class="modal-dialog" style="max-width: 40%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header border bottom p-b-15">
                <h4 class="modal-title">
                    <b class="text-shopinn">{{ __('Select Your Package') }}</b>
                </h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
                <!-- Modal body -->
            <div class="modal-body text-center">
                <div class="payment-method m-b-0">
                    <ul class="payment-options text-center">
                        @foreach($active_logs as $log)
                        <li class="options">
                            <a class="btn btn-shopinn-outline" href="{{ route('shop.setting.advertise.create', ['log_id' => encrypt($log->id)]) }}">{{ $log->package->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-shopinn-outline" type="button" onclick="window.location.href='{{ route('shop.setting.advertise.package.register') }}'">{{ __('Create New Package') }}</button>
            </div>
        </div>
    </div>
</div>
