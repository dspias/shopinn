@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Advertise')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Create New Advertise')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/bundles/summernote/summernote-bs4.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />

    <!-- font -->
    <link href="{{ asset('fileuploader/font/font-fileuploader.css') }}" media="all" rel="stylesheet">

    <!-- css -->
    <link href="{{ asset('fileuploader/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .c-form {
        display: flex;
    }

    .profile-controls{
        background: none;
    }
    .profile-menu{
        width: auto;
    }
    textarea{
        /* box-sizing: padding-box; */
        overflow:hidden;
        /* demo only: */
        font-size:14px;
        margin:50px auto;
        display:block;
    }
    .note-editor.note-frame{
        border: 1px solid #efefef;
    }
    .note-toolbar-wrapper{
        border-top: 1px solid #efefef;
    }
    .note-statusbar{
        display: none;
    }

    .attachments li.preview-btn {
        width: 100%;
    }
    /* .collapse.show{
        margin-right: 50px;
    } */
    .more-details-body{
        border: 1px solid #efefef;
        /* margin-right: 20px; */
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 14px 15px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 20px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 12px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 5px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }


    /* FileUploader  */
    .fileuploader-input .fileuploader-input-caption{
        color: #ff084e;
    }
    .fileuploader-input .fileuploader-input-button:active,
    .fileuploader-input .fileuploader-input-button:hover,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:active,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:hover{
        box-shadow: none;
        transform: none;
    }
    .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
        background: #ff084e !important;
    }

    .tag-col .chosen-container.chosen-container-multi{
        display: none !important;
    }
    .selectize-input {
        border: 1px solid #dddddd;
        padding: 15px 10px;
        box-shadow: none !important;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px;
    }
    .selectize-control.multi .selectize-input [data-value]{
        border-color: #ff084e !important;
        background: #ff084e !important;
    }
    .chosen-container.chosen-container-multi{
        display: none !important;
    }



    .select2-container .select2-selection--single{
        height: 30px;
    }
    ..select2-container--default .select2-selection--single,
    .select2 select2-container.select2-container--default.select2-container--below.select2-container--focus,
    .select2-container--default .select2-selection--single{
        border: 1px solid #dee2e6 !important;
        outline: none !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #dee2e6 !important;
        border-radius: 4px;
        outline: none !important;
    }
    .select2 select2-container.select2-container--default.select2-container--below,
    .select2-container{
        width: 100% !important;
    }

    .chosen-container.chosen-container-single{
        display: none !important;
    }
    .select2-container--default .select2-results__option--selected,
    .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable{
        background-color: #ff084e;
        color: #ffffff;
    }

    .selectize-control.single .selectize-input {
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        background-color: transparent !important;
        background-image: none !important;
        background-image: none !important;
        background-image: none !important;
        background-image: none !important;
        background-image: none !important;
        background-repeat: no-repeat !important;
    }
    .selectize-control.single .selectize-input, .selectize-dropdown.single {
        border-color: #e2e2e2 !important;
    }
    .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
        font-weight: 500 !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Create New Advertise') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Create New Advertise for ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">


                        <div class="col-lg-12 m-t-30">
                            {{-- Featured Post Starts --}}
                            <div class="central-meta">
                                <span class="create-post p-b-20">
                                    {{ __('Create New Advertise') }}
                                    <a class="see-all text-bold text-capitalize" href="{{ route('shop.setting.advertise.package.register') }}" style="font-size: 14px;">
                                        <b>{{ __('Register Advertise Package') }}</b>
                                    </a>
                                </span>
                                <form action="{{ route('shop.setting.advertise.store', ['log_id' => $active_log->id]) }}" method="post" enctype="multipart/form-data" class="create_product">
                                    @csrf
                                    <div>
                                        {{-- @if () --}}
                                            <div class="details-optn">
                                                <div class="card card-body more-details-body p-r-0">
                                                    <div class="row c-form">
                                                        {{-- <input type="hidden" id="hidden_height" name="hidden_height" value="">
                                                        <input type="hidden" id="hidden_width" name="hidden_width" value="">
                                                        <input type="hidden" id="hidden_min_size" name="hidden_min_size" value="">
                                                        <input type="hidden" id="hidden_max_size" name="hidden_max_size" value=""> --}}
                                                        <div class="col-md-12 m-b-10">
                                                            <label for="type_id">{{ __('Select Advertise Type') }} <b class="text-shopinn">*</b></label>
                                                            <select name="type_id" id="selectize-dropdown" required>
                                                                <option value="" disabled selected>{{ __('Select Type') }} *</option>
                                                                @foreach((array)$types as $type)
                                                                    <option value="{{ $type['type_id'] }}">{{ $type['name'] }}</option>
                                                                @endforeach
                                                            </select>

                                                            @error('type_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label for="type_id">{{ __('Advertise Title') }} <b class="text-shopinn">*</b></label>
                                                            <input autocomplete="off" type="text" name="title" class="border bg-transparent @error('title') is-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Advertise Title *') }}">

                                                            @error('title')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-md-8">
                                                            <label for="type_id">{{ __('Advertise URL') }} <b class="text-shopinn">*</b></label>
                                                            <input autocomplete="off" type="url" name="url_link" class="border bg-transparent @error('url_link') is-invalid @enderror" value="{{ old('url_link') }}" placeholder="{{ __('Advertise URL *') }}">

                                                            @error('url_link')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="col-md-12 m-t-20">
                                                            <small class="text-bold">
                                                                {{ __('Please Upload Following Size Image:') }}
                                                                <span class="text-shopinn">{{ __('Width:') }} <span id="imgWidth">0</span>PX</span> |
                                                                <span class="text-shopinn">{{ __('Height:') }} <span id="imgHeight">0</span>PX</span> |
                                                                <span class="text-shopinn">{{ __('Min Size:') }} <span id="minSize">0</span>KB</span> |
                                                                <span class="text-shopinn">{{ __('Max Size:') }} <span id="maxSize">0</span>KB</span> |
                                                                <span class="text-shopinn">{{ __('Remaining for:') }} <span id="remaining">0</span> time</span>
                                                            </small>
                                                            <input type="file" name="ad_image" class="ad_image @error('ad_image') is-invalid @enderror" accept="image/*" required>

                                                            @error('ad_image')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-shopinn btn-block p-10" type="submit">{{ __('Create Advertise') }}</button>
                                                </div>
                                            </div>
                                    </div>
                                </form>
                            </div>
                            {{-- Featured Post End --}}
                        </div><!-- centerl meta -->


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('fileuploader/jquery.fileuploader.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });
    </script>

    <script>
        // Custom Script Here
        $('#selectize-dropdown').selectize({
            create: false,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body'
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            // enable fileuploader plugin
            $('input[name="ad_image"]').fileuploader({
                limit: 1,
                // maxSize: 1,

                extensions: ["image/*"],
                addMore: true,
            });


            $('.fileuploader-input-caption span').html('Choose or Drag & Drop Your Advertise Image to Upload');
        });
    </script>

    <script>
        // type wise input change
        var types = @php echo json_encode($types); @endphp

        // console.log(types);

        $('#selectize-dropdown').on('change', function(){
            var id = $(this).val();
            var type = null;
            types.forEach(function(temp){
                    if(temp.type_id == id) type = temp;
            });

            // console.log(id);
            let input = $('#input_id');

            $('#imgWidth').html(type.width);
            $('#imgHeight').html(type.height);
            $('#minSize').html(type.min_size);
            $('#maxSize').html(type.max_size);
            $('#remaining').html(type.remaining);

            // $('#hidden_width').val(type.width)
            // $('#hidden_height').val(type.height)
            // $('#hidden_min_size').val(type.min_size)
            // $('#hidden_max_size').val(type.max_size)
        });
    </script>
@endsection
