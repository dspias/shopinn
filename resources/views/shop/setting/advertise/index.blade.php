@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Advertise')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Advertise')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    .payment-options label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .payment-options{
        display: inline;
    }
    .payment-options .options{
        display: inline-block;
        list-style: none;
    }

    .payment-options {
        display: inherit;
        padding-left: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Shop Advertises') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Advertises of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row widget-page merged20">
							<div class="col-md-12">
								<aside class="sidebar">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('All Advertises') }}
                                            <a class="see-all text-bold text-uppercase" href="javascript:void(0);" data-toggle="modal" data-target="#newAdvertise">
                                                <b  style="font-size: 12px;">{{ __('Add New Advertise') }}</b>
                                            </a>
                                        </h4>
                                        <div class="pit-tags">
                                            <div class="table-overflow">
                                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">{{ __('SL.') }}</th>
                                                            <th class="text-center">{{ __('Advertise Title') }}</th>
                                                            <th class="text-center">{{ __('Advertise Type') }}</th>
                                                            <th class="text-center">{{ __('Start Date') }}</th>
                                                            <th class="text-center">{{ __('End Date') }}</th>
                                                            <th class="text-center">{{ __('Status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($advertises as $sl => $data)
                                                        {{-- {{ dd($data) }} --}}
                                                            <tr class="row-click" data-link="{{ route('shop.setting.advertise.show', ['ad_id' => encrypt($data->id)]) }}" data-target="_self">
                                                                <td class="text-center">
                                                                    <div class="mrg-top-0">
                                                                        <span class="text-dark">
                                                                            <b>{{ $sl+1 }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mrg-top-0">
                                                                        <span>
                                                                            <b>{{ $data->title }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mrg-top-0">
                                                                        <span>
                                                                            <b>{{ $data->type->name }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>

                                                                {{-- @php
                                                                    $dateTime = new DateTime($order->created_at);
                                                                @endphp --}}

                                                                <td>
                                                                    <div class="mrg-top-0">
                                                                        <span class="text-success text-bold">{{ get_date($data->start, 'd-m-Y') }}</span>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="mrg-top-0">
                                                                        <span class="text-shopinn text-bold">{{ get_date($data->end, 'd-m-Y') }}</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="mrg-top-0">
                                                                        @if ($data->is_active == 1)
                                                                            <b class="text-success">{{ __('Acitve') }}</b>
                                                                        @else
                                                                            <b class="text-danger">{{ __('Inacitve') }}</b>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
					</div>



                {{-- =============< Modal Part Start >============== --}}
                 @include('shop.setting.advertise.modals.new_advertise')
                {{-- =============< Modal Part Ends >============== --}}
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });
    </script>
@endsection
