@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Advertise')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Create New Advertise')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .sec-heading {
        display: inline-block;
        margin-bottom: 0px;
        width: 100%;
    }


    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    .payment-options label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .payment-options{
        display: inline;
    }
    .payment-options .options{
        display: inline-block;
        list-style: none;
    }

    .payment-options {
        display: inherit;
        padding-left: 0px;
    }


    /*
    * Input Number Design
    */
    .modal-body input[type="number"] {
        -moz-appearance: textfield;
    }

    .modal-body input[type="number"]::-webkit-inner-spin-button,
    .modal-body input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .modal-body .pt_Quantity {
        display: inline-block;
        margin: 0 0 10px;
        position: relative;
    }

    .modal-body .pt_Quantity input {
        color: #443017;
        /* border-radius: 2px; */
        /* background-color: #ffffff; */
        border: solid 1px #d2d2d2;
        /* font-family: "Montserrat", sans-serif; */
        font-size: 24px;
        font-weight: 500;
        /* min-width: 50px; */
        height: 50px;
        float: left;
        display: block;
        padding: 20px;
        margin: 0;
        text-align: center;
        width: 200px;
    }

    .modal-body .pt_Quantity input:focus {
        outline: 0;
    }

    .modal-body .pt_Quantity .pt_QuantityNav {
        float: left;
        position: relative;
        height: 50px;
    }

    .modal-body .pt_Quantity .pt_QuantityNav .pt_QuantityButton {
        position: relative;
        cursor: pointer;
        border-left: 1px solid #d0d0d0;
        width: 30px;
        text-align: center;
        color: #333;
        font-size: 23px;
        /* font-family: "Trebuchet MS", Helvetica, sans-serif !important; */
        line-height: 1.2;
        -webkit-transform: translateX(-100%);
        transform: translateX(-100%);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .modal-body .pt_Quantity .pt_QuantityNav .pt_QuantityButton:active {
        background-color: #ff084e;
        color: #ffffff;
    }

    .modal-body .pt_Quantity .pt_QuantityNav .pt_QuantityButton.pt_QuantityUp {
        position: absolute;
        height: 50%;
        top: 0;
        border-bottom: 1px solid #b7b7b7;
    }

    .modal-body .pt_Quantity .pt_QuantityNav .pt_QuantityButton.pt_QuantityDown {
        position: absolute;
        bottom: 0px;
        height: 50%;
    }
    .modal-body .pt_Quantity label {
        padding: 0 !important;
        display: block !important;
        border: 0 !important;
        cursor: auto !important;
        font-size: 22px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Register Advertise Package') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Register Advertise Package for ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
    </div>
</section>

@php
    function isActive($active_packages, $package_id){
        foreach($active_packages as $package){
            if($package->id == $package_id) return 0;
        }
        return 1;
    }
@endphp

<section>
    <div class="gap">
        <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-10">
                    <div class="user-feature-info">
                        <div class="sec-heading style9 text-center">
                            <h2>{{ __('Active Packages:') }}</h2>
                            <ul class="d-inline-block p-l-0">
                                @foreach ($active_packages as $sl => $pack)
                                    <li class="d-inline-block p-6">
                                        <h6>{{ $pack->package->name }} <sup class="text-shopinn">{{ '('.get_day($pack->end).' Days Left)' }}</sup></h6>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
                @foreach($packages as $package)
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="price-box">
                            <div class="pricings">
                                <h2>{{ __($package->name) }}</h2>
                                {{-- <h6 class="text-shopinn">{{ __('FREE') }}</h6> --}}
                                <h1>{{ __($package->price) }}<sup>৳</sup> </h1>
                                <p class="title-2-line">
                                    {!! $package->details !!}
                                </p>
                                @if(isActive($active_packages, $package->id))
                                    <a href="javascript:void(0);" id="buyPackageReg" class="btn btn-shopinn btn-block" data-toggle="modal" data-target="#packageRegisterModal" data-todo="{{ $package }}">{{ __('Purchase') }}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach



                {{-- =============< Modal Part Start >============== --}}
                 @include('shop.setting.advertise.modals.register')
                {{-- =============< Modal Part Ends >============== --}}
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        // Edit Type
        // $('#buyPackageReg').on('show.bs.modal', function (e) {
        $('#packageRegisterModal').on('show.bs.modal', function (e) {
            // do something...
            var button = $(e.relatedTarget); // Button that triggered the modal
            var data = button.data('todo');

            var modal = $(this);
            // console.log(data);
            modal.find('.modal-content #package_id').val(data.id);
            modal.find('.modal-content #package_price_original').val(data.price);
            modal.find('.modal-content #package_price_hidden').val(data.price);
            modal.find('.modal-content #package_name').html(data.name);
            modal.find('.modal-content #package_price').html(data.price);
            // modal.find('.modal-content #package_price').val(data.id);
          })
    </script>

    <script>
    (function($) {
        "use strict";

        function customQuantity() {
            /** Custom Input number increment js **/
            jQuery(
                '<div class="pt_QuantityNav"><div class="pt_QuantityButton pt_QuantityUp">+</div><div class="pt_QuantityButton pt_QuantityDown">-</div></div>'
            ).insertAfter(".pt_Quantity input");
            jQuery(".pt_Quantity").each(function() {
                var spinner = jQuery(this),
                    input = spinner.find('input[type="number"]'),
                    btnUp = spinner.find(".pt_QuantityUp"),
                    btnDown = spinner.find(".pt_QuantityDown"),
                    min = input.attr("min"),
                    max = input.attr("max"),
                    valOfAmout = input.val(),
                    newVal = 1;

                btnUp.on("click", function() {
                    var oldValue = parseFloat(input.val());

                    if (oldValue >= max) {
                        var newVal = oldValue;
                    } else {
                        var newVal = oldValue + 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });
                btnDown.on("click", function() {
                    var oldValue = parseFloat(input.val());
                    if (oldValue <= min) {
                        var newVal = oldValue;
                    } else {
                        var newVal = oldValue - 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });
            });
        }
        customQuantity();
    })(jQuery);
    </script>


    {{-- Increase Price with days in modal --}}
    <script>
        $(document).ready(function(){
            var changeBtn = $('.pt_QuantityButton');
            var mainPrice = $('#package_price_original');
            var price = $('#package_price_hidden');
            var day = $('.day-input');
            var getPrice = 0;

            changeBtn.click(function(){
                getPrice = mainPrice.val() * day.val();
                $('.modal-content #package_price').html(getPrice);
                price.val(getPrice);
            });
        });
    </script>
@endsection
