@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Coupons')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Coupons')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Shop Coupons') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Coupons of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row widget-page merged20">
							<div class="col-md-12">
								<aside class="sidebar">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('All Coupons') }} 
                                            <a class="see-all text-bold text-uppercase" href="{{ route('shop.setting.coupon.create') }}">{{ __('Add New Coupon') }}</a>
                                        </h4>
                                        <div class="pit-tags">
                                            <div class="table-overflow">
                                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">{{ __('Coupon Title') }}</th>
                                                            <th class="text-center">{{ __('Coupon Code') }}</th>
                                                            <th class="text-center">
                                                                {{ __('Total Discount') }} 
                                                                <sup>{{ __('(TK)') }}</sup>
                                                            </th>
                                                            <th class="text-center">
                                                                {{ __('Min Purchase') }} 
                                                                <sup>{{ __('(TK)') }}</sup>
                                                            </th>
                                                            <th class="text-center">{{ __('Start Date') }}</th>
                                                            <th class="text-center">{{ __('End Date') }}</th>
                                                            <th class="text-center">{{ __('Status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($coupons as $sl => $data)
                                                        <tr class="row-click" data-link="{{ route('shop.setting.coupon.edit', ['id' => encrypt($data->id)]) }}" data-target="_self">
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span class="text-dark">
                                                                        <b>{{ $data->title }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b class="text-shopinn">{{ $data->code }}</b>
                                                                        {{-- <b class="text-muted">{{ 'expire_code_here' }}</b> --}}
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ $data->discount }}<sup>{{ __('TK') }}</sup></b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ $data->min_purchase }}<sup>{{ __('TK') }}</sup></b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ get_date($data->created_at, 'd M Y') }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ get_date($data->expire_date, 'd M Y') }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        @if($data->expire_date >= date('Y-m-d'))
                                                                            <b class="text-success">{{ 'Active' }}</b>
                                                                        @else
                                                                            <b class="text-danger">{{ 'Deactive' }}</b>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>                        
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection