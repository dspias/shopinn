@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Edit Offer')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', 'Edit Offer, Offer Edit')

{{-- page here --}}
@section('page_name', 'Shop || New Offer')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .c-form {
        display: flex;
    }

    .profile-controls{
        background: none;
    }
    .profile-menu{
        width: auto;
    }
    textarea{  
        /* box-sizing: padding-box; */
        overflow:hidden;
        /* demo only: */
        font-size:14px;
        margin:50px auto;
        display:block;
    }
    .note-editor.note-frame{
        border: 1px solid #efefef;
    }
    .note-toolbar-wrapper{
        border-top: 1px solid #efefef;
    }
    .note-statusbar{
        display: none;
    }

    .attachments li.preview-btn {
        width: 100%;
    }
    /* .collapse.show{
        margin-right: 50px;
    } */
    .more-details-body{
        border: 1px solid #efefef;
        /* margin-right: 20px; */
    }

    .chosen-container-active .chosen-choices{
        box-shadow: none !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type=text]{
        padding: 14px 15px !important;
        width: 100% !important;
    }
    .chosen-choices{
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 10px !important;
        background-image: none !important;
    }
    .chosen-container .chosen-results li {
        padding: 20px 6px !important;
    }
    .chosen-container .chosen-results li.active-result.highlighted {
        background-image: none !important;
        background-color: #ff084e !important;
        color: #ffffff !important;
    }

    .chosen-container-single .chosen-single{
        box-shadow: none !important;
        height: 50px !important;
        border-radius: 5px !important;
        border: 1px solid #dddddd !important;
        padding: 12px !important;
        background-image: none !important;
        background: none !important;
    }
    .chosen-container-single .chosen-single div {
        position: absolute;
        top: 5px !important;
        right: 0;
        display: block;
        width: 18px;
        height: 100%;
    }


    /* FileUploader  */
    .fileuploader-input .fileuploader-input-caption{
        color: #ff084e;
    }
    .fileuploader-input .fileuploader-input-button:active, 
    .fileuploader-input .fileuploader-input-button:hover, 
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:active,
    .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success:hover{
        box-shadow: none;
        transform: none;
    }
    .fileuploader-input .fileuploader-input-button, .fileuploader-popup .fileuploader-popup-header .fileuploader-popup-button.button-success{
        background: #ff084e !important;
    }

    /* .tag-col .chosen-container.chosen-container-multi{
        display: none !important;
    } */
    .selectize-input {
        border: 1px solid #dddddd;
        padding: 15px 10px;
        box-shadow: none !important;
    }
    .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
        padding: 15px 8px;
    }
    .selectize-control.multi .selectize-input [data-value]{
        border-color: #ff084e !important;
        background: #ff084e !important;
    }
    /* .chosen-container.chosen-container-multi{
        display: none !important;
    } */

    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom,
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-top,
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-left,
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-right{
        text-align: center !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')

<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Edit Offer') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Edit Offer for ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row merged20 justify-content-center">


                <div class="col-md-8 m-t-30">
                    {{-- Featured Post Starts --}}
                    <div class="central-meta">
                        <span class="Edit-post">{{ __('Edit Offer') }}</span>
                        

                        <form action="{{ route('shop.setting.offer.update', ['name' => $offers[0]->title]) }}" method="post">
                            @csrf
                            <div class="new-product-Edit">
                                <div class="all-details">
                                    <div class="card card-body more-details-body p-r-0">
                                        <div class="row c-form">
                                            <div class="col-md-6 m-b-10">
                                                <label>{{ __('Offer Title') }} <sup class="required">*</sup></label>
                                                <input autocomplete="off" type="text" name="title" class="border bg-transparent @error('title') is-invalid @enderror" value="{{ $offers[0]->title }}" placeholder="{{ __('Offer Title') }}" required>

                                                @error('title')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="col-md-6 m-b-10">
                                                <label>{{ __('Total Discount') }} <sup class="text-muted">{{ __('(in %)') }}</sup> <sup class="required">*</sup></label>
                                                <input autocomplete="off" type="number" name="discount" min="0" class="border bg-transparent @error('discount') is-invalid @enderror" value="{{ $offers[0]->discount }}" placeholder="{{ __('Discount %') }}" required>

                                                @error('discount')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            

                                            <div class="col-md-12 m-b-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label style="margin-bottom: 0px;">{{ __('Select Product\'s For Discount') }} <sup class="required">*</sup></label>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="checkbox m-0 float-right p-0">
                                                            <label>
                                                                <input type="checkbox" name="is_all" @if($offers[0]->is_all == 1)checked @endif>
                                                                <i class="check-box m-t-2"></i>
                                                                {{ __('Select All Products') }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    function match($offers, $id){
                                                        foreach($offers as $offer){
                                                            if($offer->product_id == $id) return 1;
                                                        }
                                                        return 0;
                                                    }
                                                @endphp

                                                <select data-placeholder="{{ __('Select Product\'s') }}" multiple class="chosen-select multi" name="products[]">
                                                    @foreach($categories as $products)
                                                    <optgroup class="text-shopinn" label="{{ $products[0]->category->name }}">
                                                        @foreach($products as $product)
                                                            <option value="{{ $product->id }}" @if(match($offers, $product->id))selected @endif)>{{ $product->name }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                    @endforeach
                                                </select>

                                                @error('products')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            
                                            <div class="col-md-12 m-b-10">
                                                <label>{{ __('Offer Expire Date') }}</label>
                                                <input autocomplete="off" type="text" name="expire_date" class="border datepicker-1 bg-transparent @error('expire_date') is-invalid @enderror" value="{{ $offers[0]->expire_date  }}" placeholder="{{ __('Offer Expire Date') }}" data-provide="datepicker">

                                                @error('expire_date')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-shopinn m-t-15" type="submit">{{ __('Update Offer') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{-- Featured Post End --}}
                </div><!-- centerl meta -->

                
            </div>	
        </div>
    </div>
</div>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>

    <script src="{{ asset('assets/bundles/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ asset('fileuploader/jquery.fileuploader.min.js') }}"></script>

    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function(){
            $('[data-tooltip="tooltip"]').tooltip();

            $('.datepicker-1').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
            });

            $('.selectize-tags').selectize({
                delimiter: ',',
                persist: false,
                Edit: function(input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });
        });
    </script>
@endsection