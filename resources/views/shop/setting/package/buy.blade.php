@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Package')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Buy Package')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .payment-options{
        display: inline;
    }
    .payment-options .options{
        display: inline-block;
        list-style: none;
    }
</style>
<style>
    .left-detail-meta {
        width: 100%;
    }
    /* input number style */
    .input-number {
        width: 80px;
        padding: 0 12px;
        vertical-align: top;
        text-align: center;
        outline: none;
        border: 1px solid #ffffff;
    }

    .input-number,
    .input-number-decrement,
    .input-number-increment {
        height: 40px;
        user-select: none;
    }

    .input-number-decrement, .input-number-increment {
        display: inline-block;
        width: 40px;
        line-height: 38px;
        background: #f1f1f1;
        color: #444;
        text-align: center;
        /* font-weight: bold; */
        cursor: pointer;
        font-size: 1.5rem;
    }
    .input-number-decrement:active,
    .input-number-increment:active {
        color: #ffffff;
        background: #ff084e;
    }

    .input-number-decrement,
    .input-number-increment {
        border: 1px solid #f1f1f1;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .st-line {
        padding-top:2px;
        margin-top: 0rem;
        margin-bottom: 0rem;
        border: 1px;
        border-top: 10px solid rgba(0,0,0,.1);
    }

    .delete-cart{
        color:#fff !important;
    }

    .total-area>ul li.order-total {
        border-top: 0px solid #ccc;
        font-weight: 500;
        margin-bottom: 20px;
        margin-top: 0px;
        max-width: 100%;
        padding-top: 0px;
    }

    .payment-options {
        display: inherit;
        padding-left: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Package') }}</h1>
            <h4 class="text-capitalize text-white">{{ __('Buy (') }}{{ __($package->name).')' }}</h4>
            @if($package->package_type == 1)
                <h6 class="text-white">{{ __('FREE') }}</h6>
                <h1 class="p-b-50  text-white">{{ __('0.00') }}<sup>৳</sup> </h1>
            @elseif($package->package_type == 2)
                <h6 class="text-white">{{ __('COMMISION BASED') }}</h6>
                <h1 class="p-b-50  text-white">{{ __($package->total_percentage) }}<sup>%</sup> </h1>
            @elseif($package->package_type == 3)
                <h6 class="text-white">{{ __('COMMISION AND PERCENTAGE BASED') }}</h6>
                <h2 class="p-b-50 text-white">{{ __($package->total_percentage) }}<sup>%</sup> + {{ __($package->total_payment) }}<sup>৳</sup> </h2>
            @endif
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row widget-page merged20">
                        <div class="col-6 mx-auto">
                            <div class="central-meta">
                                <h4 class="create-post">{{ __('Package Details') }}</h4>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="amount-areas">
                                            <div class="total-area">
                                                <ul>
                                                    <li class="order-total">
                                                        <span class="text-shopinn text-uppercase">{{ __('Package Name:') }}</span>
                                                        <i>{{ $package->name }}</i>
                                                    </li>
                                                    <li class="order-total text-center">
                                                        @if($package->package_type == 1)
                                                            <h6 class="text-shopinn">{{ __('FREE') }}</h6>
                                                            <h1>{{ __('0.00') }}<sup>৳</sup> </h1>
                                                        @elseif($package->package_type == 2)
                                                            <h6 class="text-shopinn">{{ __('COMMISION BASED') }}</h6>
                                                            <h1>{{ __($package->total_percentage) }}<sup>%</sup> </h1>
                                                        @elseif($package->package_type == 3)
                                                            <h6 class="text-shopinn">{{ __('COMMISION AND PERCENTAGE BASED') }}</h6>
                                                            <h2>{{ __($package->total_percentage) }}<sup>%</sup> + {{ __($package->total_payment) }}<sup>৳</sup> </h2>
                                                        @endif
                                                    </li>
                                                    <li class="order-total">
                                                        <span class="text-shopinn text-uppercase">{{ __('Description:') }}</span>
                                                    </li>
                                                    <li class="order-total" style="margin-top: 50px !important;">
                                                        <div class="text-shopinn">{!! $package->description !!}</div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 m-t-30">
                                        <div class="payment-method m-b-20">
                                            <h4 class="create-post">{{ __('Select Payment Method') }}</h4>
                                            <ul class="payment-options text-center">
                                                <li class="options">
                                                    <input type="radio" id="pay_by_bkash" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_bkash">{{ __('bKash') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="pay_by_rocket" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_rocket">{{ __('Rocket') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="pay_by_card" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_card">{{ __('Card') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="request_shopinn" name="pay_by" required checked/>
                                                    <label class="text-capitalize m-b-0" for="request_shopinn">{{ __('Request To Shopinn') }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="proceed text-center">
                                            <a href="{{ route('shop.setting.package.index') }}" title="" class="btn btn-shopinn-outline">{{ __('Back To ShopInn') }}</a>
                                            <a href="{{ route('shop.setting.package.activeted', ['package_id' => encrypt($package->id)]) }}" title="" class="btn btn-shopinn-outline confirmation">{{ __('Proceed to Checkout') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- price plans -->

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




