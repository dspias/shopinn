@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Package')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Package')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .sec-heading {
        display: inline-block;
        margin-bottom: 0px;
        width: 100%;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Packages') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Packages of Shopinn-BD') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap">
        <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-10">
                    <div class="user-feature-info">
                        <div class="sec-heading style9 text-center">
                            <h2>{{ __('Active Package:') }} 
                                <span>
                                    @if($activated != null)
                                        {{ $activated->package->name }}
                                    @else
                                        {{ __('Noting') }}
                                    @endif
                                </span>
                            </h2>
                        </div>
                    </div>
                </div>
                @foreach($packages as $package)
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="price-box">
                            <div class="pricings">
                                <h2>{{ __($package->name) }}</h2>
                                @if($package->package_type == 1)
                                    <h6 class="text-shopinn">{{ __('FREE') }}</h6>
                                    <h1>{{ __('0.00') }}<sup>৳</sup> </h1>
                                @elseif($package->package_type == 2)
                                    <h6 class="text-shopinn">{{ __('COMMISION BASED') }}</h6>
                                    <h1>{{ __($package->total_percentage) }}<sup>%</sup> </h1>
                                @elseif($package->package_type == 3)
                                    <h6 class="text-shopinn">{{ __('COMMISION AND PERCENTAGE BASED') }}</h6>
                                    <h2>{{ __($package->total_percentage) }}<sup>%</sup> + {{ __($package->total_payment) }}<sup>৳</sup> </h2>
                                @endif
                                <p class="title-2-line">
                                    {!! $package->description !!}
                                </p>
                                
                                @if(!(!is_null($activated) && $package->id == $activated->package_id))
                                    <a href="{{ route('shop.setting.package.buy', ['package_id' => encrypt($package->id)]) }}" class="btn btn-shopinn btn-block" >{{ __('Buy Now') }}</a>
                                @endif
                            
                        
                            </div>	
                        </div>
                    </div>
                @endforeach
                
                

                {{-- =============< Modal Part Start >============== --}}
                {{--  @include('shop.setting.package.modals.package_details')  --}}
                {{-- =============< Modal Part Ends >============== --}}
            </div>
        </div>
    </div>
</section><!-- price plans -->

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




