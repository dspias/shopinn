<div class="modal fade" id="packageDetailsModal">
    <div class="modal-dialog" style="max-width: 60%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">
                    {{ __('Package_Name_Here') }} 
                    <span class="text-shopinn">({{ __('1000') }}<sup class="text-bold">৳</sup>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                {!! __('Package_Details_here') !!}
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-shopinn">{{ __('Order Now') }}</button>
            </div>
        </div>
    </div>
</div>