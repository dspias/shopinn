@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shop, Settings')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Setting')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
      }
      .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
      }
      .avatar-upload .avatar-edit input {
        display: none;
      }
      .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
      }
      .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
      }
      .avatar-upload .avatar-edit input + label:after {
        content: "\f040";
        font-family: 'FontAwesome';
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
      }
      .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
      }
      .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
      }
      .custom-btn{
        right: 10px;
        position: absolute;
        top: 151px;
        border-radius: 100%;
        background: #ffffff;
        border-color: #d6d6d6;
    }


    .shop-cover-photo .avatar-upload {
        margin: 50px;
    }
    .shop-cover-photo .avatar-upload .avatar-preview,
    .shop-cover-photo .avatar-upload .avatar-preview > div{
        width: 300px;
        border: 0px solid #ffffff;
        border-radius: 0px;
    }
    .shop-cover-photo .avatar-upload .avatar-edit{
        position: absolute;
        right: -100px;
        z-index: 1;
        top: -5px;
    }

    .shop-cover-photo .avatar-upload .custom-btn {
        position: absolute;
        right: -100px;
        z-index: 1;
        bottom: -5px;
        padding: 10px 14px;
    }
    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: transparent !important;
        outline: 0;
        box-shadow: none !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Settings') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Update / Modify your profile informations') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
  <div class="gap gray-bg">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="central-meta">
                      <div class="about">
                          <div class="d-flex flex-row mt-2">
                              <ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left">
                                  <li class="nav-item">
                                      <a href="#gen-setting" class="nav-link active" data-toggle="tab"><i class="fa fa-gear"></i> General Setting</a>
                                  </li>
                                  <li class="nav-item">
                                      <a href="#security" class="nav-link" data-toggle="tab"><i class="fa fa-lock"></i> Security</a>
                                  </li>
                              </ul>
                              <div class="tab-content">
                                  <div class="tab-pane fade show active" id="gen-setting">
                                    <div class="set-title">
                                        <h5>General Setting</h5>
                                        <span>Change Your Shop Information here</span>
                                    </div>
                                    @php
                                        $logo = get_logo(auth()->user(), 'thumb');
                                        if($logo == null) $logo = asset('vendor/images/resources/admin2.jpg');

                                        $cover = get_cover(auth()->user(), 'card');
                                        if($cover == null) $cover = asset('vendor/images/resources/frnd-cover1.jpg');
                                    @endphp
                                    <div class="setting-meta">
                                        <div class="row">
                                            <div class="col-md-6 shop-logo">
                                                <form action="{{ route('shop.setting.shop.store.avatar') }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="change-photo">
                                                        <div class="avatar-upload">
                                                            <div class="avatar-edit">
                                                                <input type="hidden" name="type" value="logo">
                                                                <input name="shop_avatar" type='file' id="shopLogoUpload" accept=".png, .jpg, .jpeg" />
                                                                <label for="shopLogoUpload" data-toggle="tooltip" data-original-title="Upload Shop Logo"></label>
                                                            </div>
                                                            <div class="avatar-preview">
                                                                <div id="shopLogoView" style="background-image: url({{ $logo }});">
                                                                </div>
                                                            </div>

                                                            <button type="submit" class="btn btn-shopinn custom-btn" data-toggle="tooltip" data-original-title="Update Shop Logo">
                                                                <i class="fa fa-save"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="col-md-6 shop-cover-photo">
                                                <form action="{{ route('shop.setting.shop.store.avatar') }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="change-photo">
                                                        <div class="avatar-upload">
                                                            <div class="avatar-edit">
                                                                <input type="hidden" name="type" value="cover">
                                                                <input name="shop_avatar" type='file' id="shopCoverUpload" accept=".png, .jpg, .jpeg" />
                                                                <label for="shopCoverUpload" data-toggle="tooltip" data-original-title="Upload Shop Cover Photo"></label>
                                                            </div>
                                                            <div class="avatar-preview">
                                                                <div id="shopCoverView" style="background-image: url({{ $cover }});"></div>
                                                            </div>

                                                            <button type="submit" class="btn btn-shopinn custom-btn" data-toggle="tooltip" data-original-title="Update Shop Cover Photo">
                                                                <i class="fa fa-save"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @livewire('shop.setting.general')
                                  </div>
                                  <!-- general setting -->
                                  <div class="tab-pane fade" id="security" role="tabpanel">
                                    <div class="set-title">
                                        <h5>Passwod Setting</h5>
                                        <span>Change Your Password</span>
                                    </div>
                                    @livewire('shop.setting.security')
                                  </div>
                                  <!-- security -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- centerl meta -->
          </div>
      </div>
  </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        // For Logo
        function readShopLogoURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#shopLogoView').css('background-image', 'url('+e.target.result +')');
                    $('#shopLogoView').hide();
                    $('#shopLogoView').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#shopLogoUpload").change(function() {
            readShopLogoURL(this);
        });
    </script>

    <script>
        // For Cover Photo
        function readShopCoverURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#shopCoverView').css('background-image', 'url('+e.target.result +')');
                    $('#shopCoverView').hide();
                    $('#shopCoverView').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#shopCoverUpload").change(function() {
            readShopCoverURL(this);
        });
    </script>
@endsection




