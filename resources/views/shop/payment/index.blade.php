@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shopinn, Shop Dashboard')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop Dashboard')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Shop Dashboard') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Welcome to Shopinn BD') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row widget-page merged20">
							<div class="col-md-4">
								<aside class="sidebar">
                                    <div class="widget">
                                        <h4 class="widget-title">Tags:</h4>
                                        <div class="pit-tags">
                                            <a href="#" title="">News</a>
                                            <a href="#" title="">Gaming</a>
                                            <a href="#" title="">Sports</a>
                                            <a href="#" title="">Health</a>
                                            <a href="#" title="">Website</a>
                                            <a href="#" title="">Love</a>
                                            <a href="#" title="">Fashion</a>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>                        
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




