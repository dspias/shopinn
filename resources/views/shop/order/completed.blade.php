@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Completed Orders')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Completed Orders') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Completed Orders of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block p-20 p-t-20">
                                        <div class="table-overflow">
                                            <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">{{ __('Order ID') }}</th>
                                                        <th class="text-center">{{ __('Quantity') }}</th>
                                                        <th class="text-center">
                                                            {{ __('Total Amount') }}<sup class="text-shopinn">{{ __('(In TK)') }}</sup>
                                                        </th>
                                                        <th class="text-center">{{ __('Order Date') }}</th>
                                                        <th class="text-center">{{ __('Completion Date') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($orders as $sl => $order)
                                                        <tr class="row-click" data-link="{{ route('shop.order.show', ['order_type' => 'Completed Orders' , 'order_id' => encrypt($order->id)]) }}" data-target="_self">
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span class="text-dark">
                                                                        <b class="text-shopinn">{{ $order->oid }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            @php
                                                                $cost = 0;
                                                                $quantity = 0;
                                                                foreach($order->items as $item){
                                                                    $quantity += $item->quantity;
                                                                    $cost += ($item->quantity*$item->s_sell_price);
                                                                }
                                                                if(optional($order->coupon)->discount != null && $order->coupon->shop_id == auth()->user()->id){
                                                                    $cost -= optional($order->coupon)->discount;

                                                                }
                                                            @endphp
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ $quantity }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ $cost }}<sup class="text-shopinn">{{ __('TK') }}</sup></b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ get_date($order->created_at, 'd M Y') }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    @php
                                                                        if($order->status == 2) $date = get_date($order->updated_at, 'd M Y');
                                                                        else $date = '-';
                                                                    @endphp
                                                                    <span>
                                                                        <b>{{ $date }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection




