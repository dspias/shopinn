@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Order Details')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Order Details')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __($order_type) }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Order Details Of ')." ". auth()->user()->name }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="portlet">
                                    <ul class="portlet-item navbar">
                                        <li>
                                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" data-toggle="tooltip" data-original-title="{{ __('Back To Previous Page') }}">
                                                <i class="ti-arrow-left"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-heading">
                                    <h4 class="card-title">
                                        {{ __('Order ID: ') }}
                                        <span class="text-shopinn text-bold">{{ $order->oid }}</span>
                                        <sup>
                                            {{-- {{ dd($order->items[0]->status, $order->status) }} --}}
                                            (
                                            @if($order->items[0]->status == -1 || $order->status == -1)
                                                {{ __('Canceled') }}
                                            @elseif($order->items[0]->status == 1 && $order->status == 2)
                                                {{ __('Order Completed') }}
                                            @elseif($order->items[0]->status == 1 || $order->status == 1)
                                                {{ __('Confirmed') }}
                                            @else
                                                {{ __('Pending') }}
                                            @endif
                                            )
                                        </sup>
                                        @if ($order->status == -1)
                                            <sup class="text-shopinn">{{ __('(Order Canceled By ShopInn)') }}</sup>
                                        {{-- @elseif($order->status == 2)
                                            <sup class="text-success">{{ __('(Order Completed)') }}</sup> --}}
                                        @endif
                                    </h4>
                                </div>
                                <hr class="m-t-0">
                                <div class="card-body">
                                    <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                        <thead>
                                            <tr>
                                                <th class="text-center">{{ __('Sl.') }}</th>
                                                <th class="text-center">{{ __('Product ID') }}</th>
                                                <th class="text-center">{{ __('Product') }}</th>
                                                <th class="text-center">{{ __('Quantity') }}</th>
                                                <th class="text-center">{{ __('Total Price') }}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @php
                                                $grand_price = 0;
                                            @endphp
                                            @foreach ($order->items as $sl => $item)
                                                <tr>
                                                    <td class="text-center">
                                                        <div class="m-t-0">
                                                            <span class="text-dark">{{ __($sl+1) }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="m-t-0">
                                                            <span class="text-dark">{{ __($item->product->pid) }}</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="m-t-0">
                                                            <span class="text-dark">
                                                                <a href="{{ route('guest.product.show', ['product_id'=> $item->product->id, 'product_name' => get_name($item->product->name)]) }}" target="_blank" class="text-shopinn text-bold">{{ $item->product->name }}</a><br>
                                                                <small>{{ $item->size." | ".$item->color }}</small>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="m-t-0">
                                                            <span class="text-dark">{{ __($item->quantity) }}</span>
                                                        </div>
                                                    </td>                                                    
                                                    <td class="text-center">
                                                        @php
                                                            $price = $item->quantity * $item->s_sell_price;
                                                            $grand_price += $price;
                                                        @endphp
                                                        <div class="m-t-0">
                                                            <span class="text-dark">{{ __($price) }} <sup class="text-shopinn">@if($item->discount > 0)<del>{{ $item->discount }}%</del>@endif</sup></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                                <tr>
                                                    <th colspan="3" style="border: none;"></th>
                                                    <th colspan="1" class="text-center text-bold">{{ 'Sub Total' }}</th>
                                                    <td class="text-center text-bold">{{ $grand_price }} <sup class="text-shopinn">TK</sup></td>
                                                </tr>

                                                @php
                                                    $discount = 0;

                                                    if(optional($order->coupon)->discount != null && $order->coupon->shop_id == auth()->user()->id){
                                                        $grand_price -= optional($order->coupon)->discount;
                                                        $discount = optional($order->coupon)->discount;
                                                    }
                                                @endphp
                                                <tr>
                                                    <th colspan="3" style="border: none;"></th>
                                                    <th colspan="1" class="text-center text-bold">{{ 'Coupon' }}</th>
                                                    <td class="text-center text-bold">(-){{ $discount }} <sup class="text-shopinn">TK</sup></td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" style="border: none;"></th>
                                                    <th colspan="1" class="text-center text-bold">{{ 'Grand Total' }}</th>
                                                    <td class="text-center text-bold">{{ $grand_price }} <sup class="text-shopinn">TK</sup></td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @if ($order->status == 0)
                                    <div class="card-footer border top">
                                        <ul class="list-unstyled list-inline pull-right">
                                            <li class="list-inline-item">
                                                <a href="{{ route('shop.order.status', ['order_id'=> $order->id, 'value' => -1]) }}" class="btn btn-icon btn-flat btn-rounded confirmation" data-toggle="tooltip" data-original-title="{{ __('Cancel Order') }}">
                                                    <i class="ti-close text-danger"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{ route('shop.order.status', ['order_id'=> $order->id, 'value' => 1]) }}" class="btn btn-icon btn-flat btn-rounded confirmation" data-toggle="tooltip" data-original-title="{{ __('Accept Order') }}">
                                                    <i class="ti-check text-success"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection