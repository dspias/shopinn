@extends('layouts.reseller.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Shopinn, Reseller Dashboard')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Reseller Dashboard')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Reseller Dashboard') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Welcome to Shopinn BD') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gray-bg p-t-20">
			<div class="container">
				<div class="row m-b-80">
					<div class="col-lg-12">
                        <div class="row widget-page merged20" id="page-contents">
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Products') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-shopping-cart" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-success">{{ __($activeProduct) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Order') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $order }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Pending Order') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $pendingOrder }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Today\'s Orders') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i class="ti-announcement" style="font-size: 16px; font-weight: 700;"></i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ $todayOrder }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Sale') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($total_sale) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Commission') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($total_commission) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Paid by Shopinn') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($paid) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
							<div class="col-md-3">
								<aside class="sidebar static">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('Total Payable by Shopinn') }}
                                            <a class="see-all text-shopinn m-t-m-3"><i style="font-size: 16px; font-weight: 700;">৳</i></a>
                                        </h4>
                                        <div class="widget-body text-center p-b-20">
                                            <h3 class="text-shopinn">{{ __($payable) }}</h3>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('vendor/js/echarts.min.js') }}"></script>
    <script src="{{ asset('vendor/js/world.js') }}"></script>
    <script src="{{ asset('vendor/js/jquery.sparkline.min.js') }}"></script>
	<script src="{{ asset('vendor/js/custom.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




