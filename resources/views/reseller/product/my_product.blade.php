@extends('layouts.reseller.app')

{{-- page here --}}
@section('page_name', 'My Products')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('My Products') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('All Products of ') }} <span class="text-bold">{{ $reseller->name }}</span></h4>
        </div>
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block p-20 p-t-30">
                                    <div class="table-overflow">
                                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">{{ __('SL.') }}</th>
                                                    <th class="text-center">{{ __('Product ID') }}</th>
                                                    <th class="text-center">
                                                        {{ __('Price') }}<sup class="text-shopinn">{{ __('(In TK)') }}</sup>
                                                    </th>
                                                    <th class="text-center">{{ __('Stock Available') }}</th>
                                                    <th class="text-center">{{ __('Added Date') }}</th>
                                                    <th class="text-center">{{ __('Actions') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($products as $sl => $product)
                                                    {{--  {{ dd($product) }}  --}}
                                                    <tr>
                                                    {{--  <tr class="row-click" data-link="{{ route('guest.product.show', ['product_id' => $product->product->id,'product_name' => get_name($product->product->name)]) }}" data-target="_blank">  --}}
                                                        <td class="text-center">
                                                            <div class="m-t-0">
                                                                <span class="text-dark">
                                                                    <b class="text-dark">{{ $sl+1 }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="m-t-0">
                                                                <span class="text-dark">
                                                                    <b class="text-shopinn">{{ $product->product->pid }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="m-t-0">
                                                                <span>
                                                                    @php
                                                                        $price = 0;
                                                                        if($product->product->updated_price != null){
                                                                            $price = $product->product->updated_price;
                                                                        } else {
                                                                            $price = $product->product->sell_price;
                                                                        }
                                                                    @endphp
                                                                    <b>{{ __($price) }}<sup class="text-shopinn">{{ __('TK') }}</sup></b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="m-t-0">
                                                                <span>
                                                                    <b>{{ $product->product->stock }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="m-t-0">
                                                                <span>
                                                                    <b>{{ ago_time($product->updated_at) }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="mrg-top-0 dropdown">
                                                                <a href="{{ route('guest.product.show', ['product_id' => $product->product->id,'product_name' => get_name($product->product->name)]) }}" class="btn btn-shopinn-outline btn-sm m-0 border-radius-0 p-l-5 p-r-5 p-b-3 p-t-3" data-toggle="tooltip" title="{{ __('See Details') }}" data-placement="left" target="_blank">
                                                                    <i class="ti-eye"></i>
                                                                </a>
                                                                <a href="{{ route('reseller.product.download.photos', ['product_id' => $product->product->id]) }}" class="btn btn-shopinn btn-sm m-0 border-radius-0 p-l-5 p-r-5 p-b-3 p-t-3" data-toggle="tooltip" title="{{ __('Download Photos') }}" data-placement="right">
                                                                    <i class="ti-download"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection




