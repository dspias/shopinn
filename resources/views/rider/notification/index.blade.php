@extends('layouts.shop.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || All Notifications')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
    .readed{
        background-color: #f6f7fb !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('All Notifications') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('All Notifications of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block p-20 p-t-20">
                                    <div class="table-overflow">
                                        <table id="dt-opt" class="table table-lg table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">{{ __('#') }}</th>
                                                    <th class="text-center">{{ __('Notification') }}</th>
                                                    <th class="text-center">{{ __('Arrived At') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($datas as $sl => $data)
                                                    <tr class="row-click @if($data->read_at == null) readed @endif" data-link="{{ route('markasread', ['id' => $data->id]) }}" data-target="_self" role="row">
                                                        <td class="text-center">
                                                            <div class="mrg-top-15">
                                                                <span class="text-dark">
                                                                    <b>{{ __($sl+1) }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="mrg-top-15">
                                                                <span class="text-dark">
                                                                    <b>{!! $data->data['message'] !!}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="mrg-top-15">
                                                                <span class="text-dark">
                                                                    <b>{{ ago_time($data->created_at) }}</b>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection




