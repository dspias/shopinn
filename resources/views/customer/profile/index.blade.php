@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Customer Profile')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Customer || Profile')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 50px auto;
      }
      .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
      }
      .avatar-upload .avatar-edit input {
        display: none;
      }
      .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
      }
      .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
      }
      .avatar-upload .avatar-edit input + label:after {
        content: "\f040";
        font-family: 'FontAwesome';
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
      }
      .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
      }
      .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
      }
      .change-photo {
        display: inline-block;
        width: 100%;
        background: #edf2f6;
      }
      .custom-btn{
        right: 10px;
        position: absolute;
        top: 151px;
        border-radius: 100%;
        background: #ffffff;
        border-color: #d6d6d6;
    }

    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: transparent !important;
        outline: 0;
        box-shadow: none !important;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Profile') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Update / Modify your profile informations') }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="central-meta">
                        <div class="about">
                            <div class="d-flex flex-row mt-2">
                                <ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left">
                                    <li class="nav-item">
                                        <a href="#gen-setting" class="nav-link active" data-toggle="tab"><i class="fa fa-gear"></i> General Setting</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#security" class="nav-link" data-toggle="tab"><i class="fa fa-lock"></i> Security</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="gen-setting">
                                        <div class="set-title">
                                            <h5>General Setting</h5>
                                            <span>People on Pitnik will get to know you with the info below</span>
                                        </div>
                                        @php
                                            $url = get_logo(auth()->user(), 'thumb');
                                            if($url == null) $url = asset('vendor/images/resources/admin2.jpg');
                                        @endphp
                                        <div class="setting-meta">
                                            <form action="{{ route('customer.profile.store.avatar') }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="change-photo">
                                                    <div class="avatar-upload">
                                                        <div class="avatar-edit">
                                                            <input name="avatar" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                            <label for="imageUpload"></label>
                                                        </div>
                                                        <div class="avatar-preview">
                                                            <div id="imagePreview" style="background-image: url({{ $url }});">
                                                            </div>
                                                        </div>

                                                        <button type="submit" class="btn btn-outline-dark custom-btn"><i class="fa fa-save"></i></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        @livewire('customer.setting.general')
                                    </div>
                                    <!-- general setting -->

                                    <div class="tab-pane fade" id="security" role="tabpanel">

                                        @livewire('customer.setting.security')
                                    </div>
                                    <!-- security -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- centerl meta -->
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
@endsection




