@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Cart')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Customer || Order Cart')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .payment-options{
        display: inline;
    }
    .payment-options .options{
        display: inline-block;
        list-style: none;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white"><i class="ti-shopping-cart-full"></i> {{ __('Cart') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Shopinn Cart of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row widget-page merged20">
                        
                        
                        <div class="col-lg-3">
                            <aside class="sidebar static left">
                                <div class="widget">
                                    <h4 class="widget-title">
                                        {{ __('Terms & Conditions') }} 
                                        {{-- <a class="see-all" href="#" title="">see all</a> --}}
                                    </h4>
                                    <ul class="recent-photos">
                                        <p>{{ __('Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque quae atque dolorum, maiores repellat error facilis quibusdam sint iure a et dolore, inventore iste, commodi necessitatibus molestias illum perferendis veritatis cupiditate accusamus officiis harum magnam! Mollitia consectetur dolor itaque nostrum vero qui exercitationem voluptate quibusdam sit officiis? Pariatur, a incidunt?') }}</p>
                                    </ul>
                                </div>
                                <!-- recent post-->
                                <div class="advertisment-box">
                                    <h4 class="">{{ __('Advertisment') }}</h4>
                                    <figure>
                                        <a href="#" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt="" /></a>
                                    </figure>
                                </div>
                                <!-- ad banner -->
                            </aside>
                        </div>
                        <!-- sidebar -->
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <h4 class="create-post">{{ __('My Shopping Cart') }} <a class="see-all text-bold" href="#">{{ __('Add More Product') }}</a></h4>
                                <div class="cart-sec">
                                    <table class="table table-responsive">
                                        <tr>
                                            <th>{{ __('Product Name') }}</th>
                                            <th>{{ __('Price') }}</th>
                                            <th>{{ __('Quantity') }}</th>
                                            <th class="text-shopinn">{{ __('Total') }}</th>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <a href="#" class="btn btn-link text-shopinn">{{ __('shop_name_here') }}</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#"  class="delete-cart confirmation"><i class="ti-close"></i></a>
                                                <div class="cart-avatar">
                                                    <img src="{{ asset('vendor/images/resources/cart-2.jpg') }}" alt="" />
                                                </div>
                                                <div class="cart-meta">
                                                    <span>Iphone X 2020</span>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="cart-prices">
                                                    <del>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">
                                                                <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                            </span>{{ __('1200') }}
                                                        </span>
                                                    </del>
                                                    <ins>
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">
                                                                <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                            </span>{{ __('1000') }}
                                                        </span>
                                                    </ins>
                                                </span>
                                            </td>
                                            <td>
                                                <div class="cart-list-quantity">
                                                    <div class="c-input-number">
                                                        <span>
                                                            <input id="box1" type="text" class="manual-adjust" value="1" min="1" readonly/>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="total-price text-shopinn">
                                                    <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i>
                                                    {{ __('1000') }}
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <form method="post" class="coupon-code">
                                    <input type="text" placeholder="Enter your Coupon" />
                                    <button><i class="fa fa-paper-plane-o"></i></button>
                                </form>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="amount-area">
                                            <div class="total-area">
                                                <ul>
                                                    <li>
                                                        <span>{{ __('Cart Subtotal:') }}</span>
                                                        <i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __('1200') }}</i>
                                                    </li>
                                                    <li>
                                                        <span>{{ __('Discount:') }}</span>
                                                        <i>(-)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __('170') }}</i>
                                                    </li>
                                                    <li>
                                                        <span>{{ __('Delivery Charge:') }}</span>
                                                        <i>(+)</i><i style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __('50') }}</i>
                                                    </li>
                                                    <li class="order-total">
                                                        <span class="text-shopinn">{{ __('ORDER TOTAL:') }}</span>
                                                        <i class="text-shopinn" style="font-size: 14px; font-weight: 700; font-style: revert;">৳</i> <i>{{ __('170') }}</i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="payment-method m-b-20">
                                            <h4 class="create-post">{{ __('Select Payment Method') }}</h4>
                                            <ul class="payment-options">
                                                <li class="options">
                                                    <input type="radio" id="pay_by_bkash" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_bkash">{{ __('bKash') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="pay_by_rocket" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_rocket">{{ __('Rocket') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="pay_by_card" name="pay_by" required disabled/>
                                                    <label class="text-capitalize m-b-0" for="pay_by_card">{{ __('Card') }}</label>
                                                </li>
                                                <li class="options">
                                                    <input type="radio" id="cash_on_delivery" name="pay_by" required checked/>
                                                    <label class="text-capitalize m-b-0" for="cash_on_delivery">{{ __('Cash On Delivery') }}</label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="proceed">
                                            <a href="/" title="" class="btn btn-shopinn-outline">{{ __('Back To ShopInn') }}</a>
                                            <button type="submit" class="btn btn-shopinn">{{ __('Proceed to Checkout') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
	<script src="{{ asset('vendor/js/userincr.js') }}"></script><!-- Increment prodcut button-->
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection




