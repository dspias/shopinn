@php
    $layout = (Auth::check()) ? 'layouts.customer.app':'layouts.guest.app';
@endphp
@extends($layout)

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Cart')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Order Cart')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .payment-options{
        display: inline;
    }
    .payment-options .options{
        display: inline-block;
        list-style: none;
    }
</style>
<style>
    .left-detail-meta {
        width: 100%;
    }
    /* input number style */
    .input-number {
        width: 80px;
        padding: 0 12px;
        vertical-align: top;
        text-align: center;
        outline: none;
        border: 1px solid #ffffff;
    }

    .input-number,
    .input-number-decrement,
    .input-number-increment {
        height: 40px;
        user-select: none;
    }

    .input-number-decrement, .input-number-increment {
        display: inline-block;
        width: 40px;
        line-height: 38px;
        background: #f1f1f1;
        color: #444;
        text-align: center;
        /* font-weight: bold; */
        cursor: pointer;
        font-size: 1.5rem;
    }
    .input-number-decrement:active,
    .input-number-increment:active {
        color: #ffffff;
        background: #ff084e;
    }

    .input-number-decrement,
    .input-number-increment {
        border: 1px solid #f1f1f1;
    }

    /* custom radio  */
    input[type="radio"] {
        display: none;
    }
    label {
        padding: 10px;
        display: inline-block;
        border: 1px solid #f1f1f1;
        cursor: pointer;
    }

    .blank-label {
        display: none;
    }
    input[type="radio"]:checked + label {
        background: #ff084e !important;
        color: #fff;
    }

    .st-line {
        padding-top:2px;
        margin-top: 0rem;
        margin-bottom: 0rem;
        border: 1px;
        border-top: 10px solid rgba(0,0,0,.1);
    }

    .delete-cart{
        color:#fff !important;
    }

</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white"><i class="ti-shopping-cart-full"></i> {{ __('Cart') }}</h1>
            @auth
            <h4 class="text-capitalize text-white p-b-50">{{ __('Shopinn Cart of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
            @endauth
            @guest
                <h4 class="text-capitalize text-white p-b-50">{{ __('Shopinn Cart of ') }} <span class="text-bold">{{ 'Guest' }}</span></h4>
            @endguest
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row widget-page merged20">

                        <div class="col-lg-3">
                            <aside class="sidebar static left">
                                <!-- recent post-->
                                <div class="advertisment-box">
                                    @php
                                        $ad1 = ad(796, 485);
                                    @endphp
                                    @if(!is_null($ad1))
                                    <h4 class="">{{ __('Advertisment') }}</h4>
                                    <figure>
                                        <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ image($ad1->ad_image) }}" alt=""></a>
                                        {{--  <a href="{{ $ad1->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                                    </figure>
                                    @endif
                                </div>
                                <!-- ad banner -->
                                <!-- recent post-->
                                <div class="advertisment-box">
                                    @php
                                        $ad2 = ad(796, 485);
                                    @endphp
                                    @if(!is_null($ad2))
                                    <h4 class="">{{ __('Advertisment') }}</h4>
                                    <figure>
                                        <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ image($ad2->ad_image) }}" alt=""></a>
                                        {{--  <a href="{{ $ad2->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                                    </figure>
                                    @endif
                                </div>
                                <!-- ad banner -->
                                <!-- recent post-->
                                <div class="advertisment-box">
                                    @php
                                        $ad3 = ad(796, 485);
                                    @endphp
                                    @if(!is_null($ad3))
                                    <h4 class="">{{ __('Advertisment') }}</h4>
                                    <figure>
                                        <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ image($ad3->ad_image) }}" alt=""></a>
                                        {{--  <a href="{{ $ad3->url_link }}" title="Advertisment"><img src="{{ asset('vendor/images/resources/ad-widget.gif') }}" alt=""></a>  --}}
                                    </figure>
                                    @endif
                                </div>
                                <!-- ad banner -->
                            </aside>
                        </div>
                        <!-- sidebar -->
                        <div class="col-lg-9">
                            @livewire('customer.cart')
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
	<script src="{{ asset('vendor/js/userincr.js') }}"></script><!-- Increment prodcut button-->
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')


    <script>
    </script>
@endsection




