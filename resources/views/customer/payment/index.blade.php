@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Payments')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Customer || Payments')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('Payment Reports') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Payment Reports of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row widget-page merged20">
							<div class="col-md-12">
								<aside class="sidebar">
                                    <div class="widget">
                                        <h4 class="widget-title">
                                            {{ __('All Payments') }}
                                        </h4>
                                        <div class="pit-tags">
                                            <div class="table-overflow">
                                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">{{ __('Payment For') }}</th>
                                                            <th class="text-center">
                                                                {{ __('Total Payment') }} 
                                                                <sup>{{ __('in (TK)') }}</sup>
                                                            </th>
                                                            <th class="text-center">{{ __('Payment Date') }}</th>
                                                            <th class="text-center">{{ __('Payment Type') }}</th>
                                                            <th class="text-center">{{ __('Status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {{-- @foreach ($offers as $sl => $data) --}}
                                                            <tr class="row-click" data-link="#" data-target="_blank">
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">
                                                                            <b>{{ 'product_name_here' }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span>
                                                                            <b>{{ '1000' }}{{ __('TK') }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span>
                                                                            <b>{{ '12_09_2020' }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span>
                                                                            <b>{{ 'Cash_on_delivery' }}</b>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span>
                                                                            {{-- @if($data->expire_date >= date('Y-m-d')) --}}
                                                                                <b class="text-success">{{ 'Paid' }}</b>
                                                                            {{-- @else
                                                                                <b class="text-danger">{{ 'Pending' }}</b>
                                                                            @endif --}}
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        {{-- @endforeach --}}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>                        
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection




