@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description')
@endsection

{{-- meta tag keywords here --}}
@section('meta_page_keywords')
@endsection

{{-- page here --}}
@section('page_name', 'Dashboard')

{{-- page src stylesheets here --}}
@section('page_src_styles')
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
@endsection

{{-- sub secitons here --}}
@section('sub_sections')
    <li><a class="active" href="#" title=""><i class="fa fa-home"></i> Home</a></li>
    <li><a class="" href="#" title=""><i class="fa fa-toggle-off"></i> used</a></li>
    <li><a class="" href="#" title=""><i class="fa fa-heart"></i> New</a></li>
@endsection

{{-- main content here --}}
@section('content')
{{-- @include('layouts.admin.partials.userprofile') --}}

<div class="col-lg-12">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <b>{{ auth()->user()->name ." ". auth()->user()->role->name  }} </b> are logged in!
</div>
@endsection


{{-- popup here --}}
@section('page_popup')
@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom here --}}
@section('page_scripts')

<script>

</script>

@endsection




