@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn, Order Details')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Shop || Order Details')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __($order_type) }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('Order Details Of ')." ". auth()->user()->name }}</h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="portlet">
                                    <ul class="portlet-item navbar">
                                        <li>
                                            <a href="javascript:history.back();" class="btn btn-icon btn-flat btn-rounded" data-toggle="tooltip" data-original-title="{{ __('Back To Previous Page') }}">
                                                <i class="ti-arrow-left"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-heading">
                                    <h4 class="card-title">
                                        {{ __('Order ID: ') }}
                                        <span class="text-shopinn text-bold">{{ $order->oid }}</span>
                                        @if ($order->status == -1)
                                            <sup class="text-muted">{{ __('(Order Canceled By ShopInn or Customer)') }}</sup>
                                        @endif
                                    </h4>
                                </div>
                                <hr class="m-t-0">
                                @php
                                    $grand_price = 0;
                                @endphp
                                <div class="card-body">
                                    @foreach($shops as $shop)
                                        <div class="card">
                                            <div class="card-heading text-center">
                                                <h4 class="card-title text-shopinn text-bold">
                                                    {{ $shop[0]->shop->name }}
                                                </h4>
                                            </div>
                                            <hr class="m-t-0">
                                            <div class="card-body">
                                                <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">{{ __('Sl.') }}</th>
                                                            <th class="text-center">{{ __('Product ID') }}</th>
                                                            <th class="text-center">{{ __('Product') }}</th>
                                                            <th class="text-center">{{ __('Quantity') }}</th>
                                                            <th class="text-center">{{ __('Total Price') }}</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($shop as $sl => $item)
                                                            <tr>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">{{ __($sl+1) }}</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">{{ __($item->product->pid) }}</span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">
                                                                            <a href="{{ route('guest.product.show', ['product_id'=> $item->product->id, 'product_name' => get_name($item->product->name)]) }}" target="_blank" class="text-shopinn text-bold">{{ $item->product->name }}</a><br>
                                                                            <small>{{ $item->size." | ".$item->color }}</small>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td class="text-center">
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">{{ __($item->quantity) }}</span>
                                                                    </div>
                                                                </td>                                                    
                                                                <td class="text-center">
                                                                    @php
                                                                        $price = $item->quantity * $item->price;
                                                                        $grand_price += $price;
                                                                    @endphp
                                                                    <div class="m-t-0">
                                                                        <span class="text-dark">{{ __($price) }} <sup class="text-shopinn">@if($item->discount > 0)<del>{{ $item->discount }}%</del>@endif</sup></span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-lg table-hover table-bordered table-responsive-sm">
                                                <tbody>
                                                    @php
                                                        $complete = get_date($order->updated_at, 'd M Y');
                                                        if($order->status == -1){
                                                            $status = 'Canceled';
                                                        } else if($order->status == 0){
                                                            $status = 'Pending';
                                                        } else if($order->status == 1){
                                                            $status = 'Running';
                                                        }else {
                                                            $status = 'Completed';
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <th colspan="3" class="text-center text-bold">{{ 'Order Date' }}</th>
                                                        <td class="text-center text-bold">{{ get_date($order->created_at, 'd M Y') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" class="text-center text-bold">{{ 'Order Completed Date' }}</th>
                                                        <td class="text-center text-bold">{{ $complete }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" class="text-center text-bold">{{ 'Status' }}</th>
                                                        <td class="text-center text-bold text-info">{{ $status }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <table class="table table-lg table-hover table-bordered table-responsive-sm">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="3" class="text-left text-bold">{{ 'Sub Total' }}</th>
                                                        <td class="text-right text-bold">{{ $grand_price }} <sup class="text-shopinn">TK</sup></td>
                                                    </tr>

                                                    @php
                                                        $discount = 0;

                                                        if(optional($order->coupon)->discount != null){
                                                            $grand_price -= optional($order->coupon)->discount;
                                                            $discount = optional($order->coupon)->discount;
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <th colspan="3" class="text-left text-bold">{{ 'Coupon' }}</th>
                                                        <td class="text-right text-bold">(-){{ $discount }} <sup class="text-shopinn">TK</sup></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" class="text-left text-bold">{{ 'Delivery Charge' }}</th>
                                                        <td class="text-right text-bold">(+){{ $order->delivery }} <sup class="text-shopinn">TK</sup></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" class="text-left text-bold">{{ 'Grand Total' }}</th>
                                                        <td class="text-right text-bold">{{ ($grand_price+$order->delivery) }} <sup class="text-shopinn">TK</sup></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
    </script>
@endsection