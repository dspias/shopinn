@extends('layouts.customer.app')

{{-- meta tag descirption here --}}
@section('meta_page_description', 'Shopinn')

{{-- meta tag keywords here --}}
@section('meta_page_keywords', '')

{{-- page here --}}
@section('page_name', 'Customer || All Orders')

{{-- page src stylesheets here --}}
@section('page_src_styles')
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
@endsection

{{-- page stylesheets here --}}
@section('page_styles')
<style>
    .page-header {
        background: #ff084e none repeat scroll 0 0;
    }
    .gap2{
        padding-top: 0px;
    }


    .dataTables_length label{
        display: flex;
    }
    .chosen-container{
        margin: 0 10px;
    }
    .chosen-container-single .chosen-single{
        padding: 0 10px;
    }
    .chosen-container-single .chosen-single div {
        top: -7px;
    }
    .row-click{
        cursor: pointer;
    }
</style>
@endsection






{{-- ====================================================================
======================< Main Content Starts >============================
==================================================================== --}}
@section('content')
<section>
    <div class="page-header">
        <div class="header-inner">
            <h1 class="text-bold text-uppercase text-white">{{ __('All Orders') }}</h1>
            <h4 class="text-capitalize text-white p-b-50">{{ __('All Orders of ') }} <span class="text-bold">{{ auth()->user()->name }}</span></h4>
        </div>
        {{-- <figure><img src="{{ asset('vendor/images/resources/baner-forum.png') }}" alt=""></figure> --}}
    </div>
</section>


<section>
    {{-- <button type="button" class="main-btn2" data-toggle="modal" data-target="#myModalscroll">With Scroll Modal</button> --}}

		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-heading">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="card m-b-0 border">
                                                    <div class="card-block p-15">
                                                        <h3 class="no-mrg-vertical text-center font-size-35">
                                                            <sup class="text-bold text-info">{{ $pending }}</sup> 
                                                            / 
                                                            <sub class="text-bold">{{ $all }}</sub>
                                                        </h3>
                                                        <div class="mrg-top-10">
                                                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Pending Orders') }}</span>
                                                            <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%</span>
                                                            <div class="progress progress-info">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($pending*100)/$all, 2, '.', ''):0.00 }}%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="card m-b-0 border">
                                                    <div class="card-block p-15">
                                                        <h3 class="no-mrg-vertical text-center font-size-35">
                                                            <sup class="text-bold text-warning">{{ $running }}</sup> 
                                                            / 
                                                            <sub class="text-bold">{{ $all }}</sub>
                                                        </h3>
                                                        <div class="mrg-top-10">
                                                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Running Orders') }}</span>
                                                            <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($running*100)/$all, 2, '.', ''):0.00 }}%</span>
                                                            <div class="progress progress-warning">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($running*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($running*100)/$all, 2, '.', ''):0.00 }}%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="card m-b-0 border">
                                                    <div class="card-block p-15">
                                                        <h3 class="no-mrg-vertical text-center font-size-35">
                                                            <sup class="text-bold text-success">{{ $complete }}</sup> 
                                                            / 
                                                            <sub class="text-bold">{{ $all }}</sub>
                                                        </h3>
                                                        <div class="mrg-top-10">
                                                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Completed Orders') }}</span>
                                                            <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($complete*100)/$all, 2, '.', ''):0.00 }}%</span>
                                                            <div class="progress progress-success">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($complete*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($complete*100)/$all, 2, '.', ''):0.00 }}%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="card m-b-0 border">
                                                    <div class="card-block p-15">
                                                        <h3 class="no-mrg-vertical text-center font-size-35">
                                                            <sup class="text-bold text-danger">{{ $cancel }}</sup> 
                                                            / 
                                                            <sub class="text-bold">{{ $all }}</sub>
                                                        </h3>
                                                        <div class="mrg-top-10">
                                                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">{{ __('Canceled Orders') }}</span>
                                                            <span class="pull-right pdd-right-10 font-size-13">{{ ($all > 0) ? number_format(($cancel*100)/$all, 2, '.', ''):0.00 }}%</span>
                                                            <div class="progress progress-danger">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ ($all > 0) ? number_format(($cancel*100)/$all, 2, '.', ''):0.00 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($all > 0) ? number_format(($cancel*100)/$all, 2, '.', ''):0.00 }}%">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="m-t-0">
                                    <div class="card-block p-20 p-t-0">
                                        <div class="table-overflow">
                                            <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive-sm">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">{{ __('SL.') }}</th>
                                                        <th class="text-center">{{ __('Order ID') }}</th>
                                                        <th class="text-center">{{ __('Quantity') }}</th>
                                                        <th class="text-center">
                                                            {{ __('Total Amount') }}<sup class="text-shopinn">{{ __('(In TK)') }}</sup>
                                                        </th>
                                                        <th class="text-center">{{ __('Order Date') }}</th>
                                                        <th class="text-center">{{ __('Completion Date') }}</th>
                                                        <th class="text-center">{{ __('Order Status') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($orders as $sl => $order)
                                                        <tr class="row-click" data-link="{{ route('customer.order.show', ['order_type' => 'All Orders' ,'order_id' => encrypt($order->id)]) }}" data-target="_self">
                                                            
                                                        <td>{{ $sl+1 }}</td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span class="text-dark">
                                                                        <b class="text-shopinn">{{ $order->oid }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            @php
                                                                $cost = 0;
                                                                $quantity = 0;
                                                                foreach($order->items as $item){
                                                                    $quantity += $item->quantity;
                                                                    $cost += ($item->quantity*$item->price);
                                                                }
                                                                if(optional($order->coupon)->discount != null) $cost -= optional($order->coupon)->discount;
                                                            @endphp
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ $quantity }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ ($cost+$order->delivery) }}<sup class="text-shopinn">{{ __('TK') }}</sup></b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        <b>{{ get_date($order->created_at, 'd M Y') }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    @php
                                                                        if($order->status == 2) $date = get_date($order->updated_at, 'd M Y');
                                                                        else $date = '-';
                                                                    @endphp
                                                                    <span>
                                                                        <b>{{ $date }}</b>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="m-t-0">
                                                                    <span>
                                                                        @if($order->status == 0)
                                                                            <b class="text-info">{{ 'Pending' }}</b>
                                                                        @elseif($order->status == 1)
                                                                            <b class="text-warning">{{ 'Running' }}</b>
                                                                        @elseif($order->status == 2)
                                                                            <b class="text-success">{{ 'Completed' }}</b>
                                                                        @elseif($order->status == -1)
                                                                            <b class="text-danger">{{ 'Canceled' }}</b>
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
</section>

@endsection
{{-- ==================================================================
======================< Main Content Ends >============================
================================================================== --}}







{{-- popup here --}}
@section('page_popup')

@endsection

{{-- page src scripts here --}}
@section('page_src_scripts')
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

{{-- page custom scripts here --}}
@section('page_scripts')
    <script>
        $(document).ready(function($) {
            $('.row-click').each(function() {
                var $th = $(this);
                $th.on('click', function() {
                    window.open($th.attr('data-link'), $th.attr('data-target'));
                });
            });
        });        
    </script>
@endsection




