"use strict";
$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});


$(document).ready(function () {
    $('.confirmation').confirm({
        type: 'dark',
        typeAnimated: true,
        title: 'Confirm!',
        content: 'Are You Sure...?',
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        confirmButton: 'Yes',
        cancelButton: 'NO',
        animation: 'zoom',
        closeAnimation: 'scale',
        confirm: function () {
            alert('Confirmed!');
        },
        cancel: function () {
            alert('Canceled!')
        }
    });
});

// send-email-show
$(document).ready(function () {
    $('.send-email').on('input', function () {
        let email = $(this).val();
        $('.send-email-show').text(email);
    })
});
