// Data Table JavaScripts

(function($) {
    'use strict';

    // $('#dt-opt, .data-table').DataTable().order([1, 'asc'], [2, 'asc']).draw();
    $('#dt-opt, .data-table').DataTable().draw();

})(jQuery);