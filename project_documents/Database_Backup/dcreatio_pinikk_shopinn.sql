-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 15, 2020 at 02:16 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcreatio_pinikk_shopinn`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_advertises`
--

CREATE TABLE `admin_advertises` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `day` int(11) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `ad_image` blob NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_advertises`
--

INSERT INTO `admin_advertises` (`id`, `type_id`, `title`, `url_link`, `day`, `start`, `end`, `ad_image`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '50+ Shop', 'https://www.facebook.com/shopinnbd', 30, '2020-12-01', '2020-12-31', 0x41646d696e41642f4c49464469614e61344a66387635336c49364968626566347a7351494b68436f4545445278555a712e6a706567, 1, '2020-12-01 20:10:58', '2020-12-01 20:10:58', NULL),
(2, 1, 'Shopping', 'https://www.facebook.com/shopinnbd', 30, '2020-12-01', '2020-12-31', 0x41646d696e41642f5345745674333934755468484f326b50633763553942686b6b57755457376d6f53643739644866522e6a706567, 1, '2020-12-01 20:13:33', '2020-12-01 20:13:33', NULL),
(3, 1, 'freelancing', 'https://www.facebook.com/shopinnbd', 30, '2020-12-01', '2020-12-31', 0x41646d696e41642f6d54377133514f734541465839574c6857674e4f484f386e4f3546337871544c70335733565332672e6a706567, 1, '2020-12-01 20:14:40', '2020-12-01 20:14:40', NULL),
(4, 2, 'career', 'https://www.facebook.com/shopinnbd', 30, '2020-12-01', '2020-12-31', 0x41646d696e41642f455458706d4356464331784d424145614f4b64774b70376d3148684d4777535447317037737232742e6a706567, 1, '2020-12-01 20:16:42', '2020-12-01 20:16:42', NULL),
(5, 2, 'shoppingmall', 'https://www.facebook.com/shopinnbd', 30, '2020-12-01', '2020-12-31', 0x41646d696e41642f696b4c644276484b6f4674665a7a483332456875356859616573315633686557487a54644e4f73342e6a706567, 1, '2020-12-01 20:17:47', '2020-12-01 20:17:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `advertise_logs`
--

CREATE TABLE `advertise_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `day` tinyint(4) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `pay_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertise_log_details`
--

CREATE TABLE `advertise_log_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `ad_image` blob NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertise_packages`
--

CREATE TABLE `advertise_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertise_package_advertise_type`
--

CREATE TABLE `advertise_package_advertise_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `advertise_type_id` bigint(20) UNSIGNED NOT NULL,
  `advertise_package_id` bigint(20) UNSIGNED NOT NULL,
  `max_ad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertise_types`
--

CREATE TABLE `advertise_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `min_size` int(11) NOT NULL,
  `max_size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advertise_types`
--

INSERT INTO `advertise_types` (`id`, `name`, `width`, `height`, `min_size`, `max_size`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home Slider', 1920, 650, 1, 3000, '2020-12-01 20:06:49', '2020-12-01 20:06:49', NULL),
(2, 'Small Banner', 796, 485, 1, 2500, '2020-12-01 20:07:09', '2020-12-01 20:07:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_packages`
--

CREATE TABLE `business_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_type` tinyint(4) NOT NULL,
  `first_percentage` decimal(8,2) DEFAULT NULL,
  `first_day` tinyint(4) DEFAULT NULL,
  `total_percentage` decimal(8,2) DEFAULT NULL,
  `total_payment` decimal(8,2) DEFAULT NULL,
  `year` tinyint(4) DEFAULT NULL,
  `month` tinyint(4) DEFAULT NULL,
  `day` tinyint(4) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_packages`
--

INSERT INTO `business_packages` (`id`, `name`, `package_type`, `first_percentage`, `first_day`, `total_percentage`, `total_payment`, `year`, `month`, `day`, `description`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Boutique House', 2, NULL, NULL, 8.50, NULL, NULL, NULL, NULL, '<p><font face=\"Times New Roman\">8.5% commission on per sale.</font></p><p><font face=\"Times New Roman\">Reseller connection</font></p><p><font face=\"Times New Roman\">Delivery and Payment Management</font></p><div><br></div>', 1, '2020-12-01 19:01:05', '2020-12-01 22:37:26', NULL),
(2, 'Suronjona Package', 2, 5.00, 60, 7.50, NULL, NULL, NULL, NULL, '<p style=\"letter-spacing: 0.2px;\"><font face=\"Times New Roman\">7.5% commission on per sale.</font></p><p style=\"letter-spacing: 0.2px;\"><font face=\"Times New Roman\">Reseller connection</font></p>', 1, '2020-12-09 19:41:54', '2020-12-09 19:41:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `product_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Red & Yellow', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(2, 1, 'White', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(3, 1, 'Black', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(4, 1, 'Red & Black', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(5, 2, 'Blue', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(6, 2, 'White & Yellow', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(7, 2, 'Red & Black', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(8, 2, 'White & Pink', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(9, 9, 'Ash', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(10, 9, 'Blue', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(11, 9, 'Green', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(12, 9, 'Black', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(13, 10, 'Pink', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(14, 10, 'Light Pink', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(15, 10, 'Blue', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(16, 10, 'Light Blue', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(17, 11, 'Black', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(18, 11, 'Blue', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(19, 11, 'Sky Blue', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(20, 11, 'Pink', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(21, 11, 'Brown', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(22, 11, 'Navy Blue', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(23, 11, 'Off White', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(24, 12, 'Magenta', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(25, 12, 'White', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(26, 12, 'Light Pink', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(27, 12, 'Black', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(28, 13, 'Purple', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(29, 13, 'Paste', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(30, 13, 'Blue', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(31, 13, 'Black', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(32, 20, 'Lime & Blue', '2020-12-03 18:37:12', '2020-12-03 18:37:12'),
(33, 20, 'Green & Blue', '2020-12-03 18:37:12', '2020-12-03 18:37:12'),
(34, 20, 'Pink & Purple', '2020-12-03 18:37:12', '2020-12-03 18:37:12'),
(35, 20, 'Yellow & Lime', '2020-12-03 18:37:12', '2020-12-03 18:37:12'),
(36, 22, 'শিউলী', '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(37, 22, 'পদ্ম', '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(38, 47, 'Green', '2020-12-06 19:51:32', '2020-12-06 19:51:32'),
(39, 47, 'Red', '2020-12-06 19:51:32', '2020-12-06 19:51:32'),
(40, 64, 'Black', '2020-12-09 19:16:46', '2020-12-09 19:16:46'),
(41, 64, 'Blue', '2020-12-09 19:16:46', '2020-12-09 19:16:46'),
(42, 64, 'Ash', '2020-12-09 19:16:46', '2020-12-09 19:16:46'),
(43, 64, 'Pink', '2020-12-09 19:16:46', '2020-12-09 19:16:46'),
(44, 65, 'Purple', '2020-12-09 19:19:07', '2020-12-09 19:19:07'),
(45, 65, 'Green', '2020-12-09 19:19:07', '2020-12-09 19:19:07'),
(46, 65, 'Red', '2020-12-09 19:19:07', '2020-12-09 19:19:07'),
(47, 65, 'Blue', '2020-12-09 19:19:07', '2020-12-09 19:19:07'),
(48, 66, 'Ash', '2020-12-09 19:34:13', '2020-12-09 19:34:13'),
(49, 66, 'Red', '2020-12-09 19:34:13', '2020-12-09 19:34:13'),
(50, 66, 'Pink', '2020-12-09 19:34:13', '2020-12-09 19:34:13'),
(51, 66, 'Black', '2020-12-09 19:34:13', '2020-12-09 19:34:13'),
(52, 69, 'Yellow & Antique', '2020-12-11 02:17:23', '2020-12-11 02:17:23'),
(53, 79, 'Blue', '2020-12-13 21:21:21', '2020-12-13 21:21:21'),
(54, 79, 'Ash', '2020-12-13 21:21:21', '2020-12-13 21:21:21'),
(55, 80, 'Blue', '2020-12-13 21:23:59', '2020-12-13 21:23:59'),
(56, 80, 'Ash', '2020-12-13 21:23:59', '2020-12-13 21:23:59'),
(57, 81, 'Off White', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(58, 81, 'White', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(59, 81, 'Silver', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(60, 81, 'Light Pink', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(61, 82, 'Black', '2020-12-14 21:22:09', '2020-12-14 21:22:09'),
(62, 82, 'Yellow', '2020-12-14 21:22:09', '2020-12-14 21:22:09'),
(63, 88, 'Purple', '2020-12-15 17:49:27', '2020-12-15 17:49:27'),
(64, 88, 'Pink', '2020-12-15 17:49:27', '2020-12-15 17:49:27'),
(65, 88, 'Red', '2020-12-15 17:49:27', '2020-12-15 17:49:27'),
(66, 88, 'Green', '2020-12-15 17:49:27', '2020-12-15 17:49:27'),
(67, 89, 'Navy Blue', '2020-12-15 17:51:06', '2020-12-15 17:51:06'),
(68, 89, 'Pink & Navy BLue', '2020-12-15 17:51:06', '2020-12-15 17:51:06'),
(69, 90, 'Black-Yellow', '2020-12-15 18:01:33', '2020-12-15 18:01:33'),
(70, 90, 'White-Black Patterns', '2020-12-15 18:01:33', '2020-12-15 18:01:33'),
(71, 90, 'Black-White', '2020-12-15 18:01:33', '2020-12-15 18:01:33'),
(72, 90, 'White-Black Flower', '2020-12-15 18:01:34', '2020-12-15 18:01:34'),
(73, 91, 'Yellow', '2020-12-15 18:06:56', '2020-12-15 18:06:56'),
(74, 91, 'Black', '2020-12-15 18:06:56', '2020-12-15 18:06:56'),
(75, 91, 'White', '2020-12-15 18:06:56', '2020-12-15 18:06:56'),
(76, 91, 'Red', '2020-12-15 18:06:56', '2020-12-15 18:06:56'),
(77, 92, 'Black', '2020-12-15 18:09:11', '2020-12-15 18:09:11'),
(78, 92, 'Brown', '2020-12-15 18:09:11', '2020-12-15 18:09:11'),
(79, 92, 'White Flower', '2020-12-15 18:09:11', '2020-12-15 18:09:11'),
(80, 92, 'White-Black Patterns', '2020-12-15 18:09:11', '2020-12-15 18:09:11'),
(81, 93, 'Red', '2020-12-15 18:12:21', '2020-12-15 18:12:21'),
(82, 93, 'White', '2020-12-15 18:12:21', '2020-12-15 18:12:21'),
(83, 93, 'Black-script', '2020-12-15 18:12:21', '2020-12-15 18:12:21'),
(84, 93, 'Black-Brown', '2020-12-15 18:12:21', '2020-12-15 18:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compnay_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` mediumblob,
  `json_data` json DEFAULT NULL,
  `string_data` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_data` longtext COLLATE utf8mb4_unicode_ci,
  `integer_data` int(11) DEFAULT NULL,
  `double_data` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `compnay_id`, `name`, `type`, `avatar`, `json_data`, `string_data`, `text_data`, `integer_data`, `double_data`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'email', 'string', NULL, NULL, 'write@shopinnbd.com', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(2, 1, 'mobile', 'string', NULL, NULL, '+8801948819191', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(3, 1, 'address', 'text', NULL, NULL, NULL, '8 Naya Sarak, Sylhet', NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(4, 1, 'facebook_page', 'string', NULL, NULL, 'https://www.facebook.com/shopinnbd', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(5, 1, 'facebook_group', 'string', NULL, NULL, 'https://www.facebook.com/groups/ShopInnBD', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(6, 1, 'instagram', 'string', NULL, NULL, 'https://www.instagram.com/shopinnbd', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-11-08 01:13:01', NULL),
(7, 1, 'twitter', 'string', NULL, NULL, 'https://www.twitter.com/shopinnbd', NULL, NULL, NULL, '2020-11-08 01:13:01', '2020-12-01 22:01:31', NULL),
(8, 1, 'short_note', 'text', NULL, NULL, NULL, 'Shop-Inn wants to say all to its Vendors – Let\'s grow together.', NULL, NULL, '2020-11-08 01:13:01', '2020-12-08 17:55:25', NULL),
(9, 1, 'about', 'text', NULL, NULL, NULL, '<h4 style=\"margin-bottom:0in;line-height:16.5pt\"><font face=\"Times New Roman\"><a href=\"http://www.shopinnbd.com\" style=\"\" target=\"_blank\">www.shopinnbd.com</a><span style=\"font-size: 9pt; color: rgb(34, 34, 34); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b>&nbsp;</b>is an online marketplace to\r\nconnect local boutique houses with their nationwide customers.&nbsp;</span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">The platform also works with\r\nthe local high street fashion houses to grow their sales online.&nbsp;</span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">In this platform, we’ve </span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">come up with a time-consuming shopping idea\r\nwhere one can connect with all the local brands in one place. </span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">&nbsp;<br></span></font><font face=\"Times New Roman\"><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">www.shopinnbd.com also commits to serve\r\nits local vendors with quick, cost-efficient delivery and payment services to\r\nmake their life&nbsp;</span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">easier and be more\r\nfocused on their businesses.</span><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">&nbsp;<br></span></font><font face=\"Times New Roman\"><span style=\"color: rgb(34, 34, 34); font-size: 9pt; letter-spacing: 0.2px;\">Shop-Inn wants\r\nsay all to its Vendors –<b style=\"\"> Let\'s grow together.</b>&nbsp;</span></font></h4>', NULL, NULL, '2020-11-08 01:13:01', '2020-12-08 17:56:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `min_purchase` int(11) NOT NULL,
  `limit` tinyint(4) NOT NULL DEFAULT '1',
  `expire_date` date NOT NULL,
  `is_all` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 3, '0750cce1-747d-477e-8426-7fed005e24ae', 'logo', '116584171_291722115614241_6050658889930338891_n (1)', '116584171_291722115614241_6050658889930338891_n-(1).jpg', 'image/jpeg', 'public', 'public', 50631, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 1, '2020-12-01 19:03:32', '2020-12-01 19:03:33'),
(2, 'App\\User', 3, '085dd5ad-7622-48c3-b00b-4b1cf0cf38e9', 'cover', '116584171_291722115614241_6050658889930338891_n (1)', '116584171_291722115614241_6050658889930338891_n-(1).jpg', 'image/jpeg', 'public', 'public', 50631, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 2, '2020-12-01 19:03:55', '2020-12-01 19:03:56'),
(3, 'App\\Models\\Product', 1, 'ba9c185d-74c9-446f-8a3a-5d0a1636c538', 'product', '126903626_1276809159350948_8244685859078468393_n', '126903626_1276809159350948_8244685859078468393_n.jpg', 'image/jpeg', 'public', 'public', 108814, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 3, '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(4, 'App\\Models\\Product', 1, '28b796a4-0e32-4ffa-a2a8-a4c4bf992d2c', 'product', '127187240_1276808982684299_6691557025631381681_n', '127187240_1276808982684299_6691557025631381681_n.jpg', 'image/jpeg', 'public', 'public', 126865, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 4, '2020-12-01 19:49:34', '2020-12-01 19:49:35'),
(5, 'App\\Models\\Product', 1, '1fe91575-184c-4996-bb71-806314647984', 'product', '127196982_1276809082684289_1866185901298466082_n', '127196982_1276809082684289_1866185901298466082_n.jpg', 'image/jpeg', 'public', 'public', 112533, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 5, '2020-12-01 19:49:35', '2020-12-01 19:49:36'),
(6, 'App\\Models\\Product', 1, '4168fda3-02eb-49be-b9e6-15a47ba39665', 'product', '127218403_1276808936017637_3096175207436495213_n', '127218403_1276808936017637_3096175207436495213_n.jpg', 'image/jpeg', 'public', 'public', 65925, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 6, '2020-12-01 19:49:36', '2020-12-01 19:49:37'),
(7, 'App\\Models\\Product', 2, '54cb4c7f-4c9a-4c5f-8a26-0c3ae2eaabc9', 'product', '127238500_1276808659350998_6932622604226905080_n', '127238500_1276808659350998_6932622604226905080_n.jpg', 'image/jpeg', 'public', 'public', 110764, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 7, '2020-12-01 20:03:27', '2020-12-01 20:03:28'),
(8, 'App\\Models\\Product', 2, 'fdeb4de2-e935-4344-8560-b9177a4582db', 'product', '127287109_1276808606017670_8272408257168483733_n', '127287109_1276808606017670_8272408257168483733_n.jpg', 'image/jpeg', 'public', 'public', 137190, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 8, '2020-12-01 20:03:28', '2020-12-01 20:03:29'),
(9, 'App\\Models\\Product', 2, 'b44da1bd-87fe-4ee4-9505-f5e3727c2ce6', 'product', '127889724_1276808539351010_7211985813377968786_n', '127889724_1276808539351010_7211985813377968786_n.jpg', 'image/jpeg', 'public', 'public', 72928, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 9, '2020-12-01 20:03:29', '2020-12-01 20:03:30'),
(10, 'App\\Models\\Product', 2, 'b4ec5b08-33bf-45fa-945c-4f6856192384', 'product', '128207250_1276808859350978_5610530500844380157_n', '128207250_1276808859350978_5610530500844380157_n.jpg', 'image/jpeg', 'public', 'public', 116566, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 10, '2020-12-01 20:03:30', '2020-12-01 20:03:31'),
(11, 'App\\User', 4, 'e7bfaab7-88a7-4838-93d8-eb0301e10442', 'cover', '117297024_110251370788438_2149419215945550842_o', '117297024_110251370788438_2149419215945550842_o.jpg', 'image/jpeg', 'public', 'public', 49212, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 11, '2020-12-01 20:19:59', '2020-12-01 20:19:59'),
(12, 'App\\User', 4, 'f29bee04-5288-4af1-8047-0aa2eec4007b', 'logo', '117297024_110251370788438_2149419215945550842_o', '117297024_110251370788438_2149419215945550842_o.jpg', 'image/jpeg', 'public', 'public', 49212, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 12, '2020-12-01 20:20:11', '2020-12-01 20:20:11'),
(17, 'App\\Models\\Product', 6, 'd558f1e1-9bc4-4750-8a37-56fff32dec38', 'product', 'kurti', 'kurti.jpg', 'image/jpeg', 'public', 'public', 86986, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 17, '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(18, 'App\\Models\\Product', 3, '2009cd05-b499-46fd-9835-47d503d3fb64', 'product', '126625629_719203002025465_8581471033730664875_o', '126625629_719203002025465_8581471033730664875_o.jpg', 'image/jpeg', 'public', 'public', 489835, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 18, '2020-12-01 21:04:07', '2020-12-01 21:04:08'),
(19, 'App\\User', 21, '1251f60e-e307-46aa-ae87-3ada56ada6c2', 'logo', '75519351_780731375696516_797163092826914816_o', '75519351_780731375696516_797163092826914816_o.jpg', 'image/jpeg', 'public', 'public', 150963, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 19, '2020-12-01 21:41:29', '2020-12-01 21:41:29'),
(20, 'App\\User', 5, '2a99fdee-7985-4eba-a9a4-8b145e33d143', 'logo', '117108779_807779899763568_2055739318002397081_o', '117108779_807779899763568_2055739318002397081_o.jpg', 'image/jpeg', 'public', 'public', 106276, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 20, '2020-12-01 21:49:32', '2020-12-01 21:49:33'),
(21, 'App\\User', 5, '53b5d6fd-ec2d-4ff7-9196-80672cbed8d2', 'cover', '121080489_855022995039258_4655999912136193840_o', '121080489_855022995039258_4655999912136193840_o.jpg', 'image/jpeg', 'public', 'public', 633768, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 21, '2020-12-01 21:49:50', '2020-12-01 21:49:51'),
(22, 'App\\Models\\Product', 7, '4b9e42ae-6779-4aee-97b6-a1a91c2499e9', 'product', '127273719_892410307967193_7875530681741320754_o', '127273719_892410307967193_7875530681741320754_o.jpg', 'image/jpeg', 'public', 'public', 339856, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 22, '2020-12-01 22:00:21', '2020-12-01 22:00:22'),
(23, 'App\\Models\\Product', 7, 'e30f487b-44c7-472c-bc3b-45efaaf6f2e1', 'product', '128330879_892410251300532_5216413679726976315_o', '128330879_892410251300532_5216413679726976315_o.jpg', 'image/jpeg', 'public', 'public', 360998, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 23, '2020-12-01 22:00:22', '2020-12-01 22:00:24'),
(24, 'App\\Models\\Product', 8, '73194fe7-9152-44af-a815-d3f195a52db6', 'product', '125931190_888270158381208_1096455672481629810_o', '125931190_888270158381208_1096455672481629810_o.jpg', 'image/jpeg', 'public', 'public', 654257, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 24, '2020-12-01 22:29:41', '2020-12-01 22:29:43'),
(25, 'App\\Models\\Product', 8, 'c4c184f1-d62b-483b-880d-558100b6e499', 'product', '126404648_888270301714527_6635065641733220173_o', '126404648_888270301714527_6635065641733220173_o.jpg', 'image/jpeg', 'public', 'public', 968378, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 25, '2020-12-01 22:29:43', '2020-12-01 22:29:45'),
(26, 'App\\Models\\Product', 8, 'd192bb9a-15d2-4d7a-b887-e2749663494a', 'product', '126901086_888270555047835_4130259933212277261_o', '126901086_888270555047835_4130259933212277261_o.jpg', 'image/jpeg', 'public', 'public', 1024408, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 26, '2020-12-01 22:29:45', '2020-12-01 22:29:47'),
(27, 'App\\Models\\Product', 8, 'f162c2f0-fccb-4a75-a2dc-768fb65aa758', 'product', '126946268_888270435047847_3266229604300657900_o', '126946268_888270435047847_3266229604300657900_o.jpg', 'image/jpeg', 'public', 'public', 610716, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 27, '2020-12-01 22:29:47', '2020-12-01 22:29:49'),
(28, 'App\\User', 6, '7180e4ad-b44d-4dba-aa14-6f4e80c73025', 'logo', '65230184_353444952020987_6280171454466097152_o', '65230184_353444952020987_6280171454466097152_o.jpg', 'image/jpeg', 'public', 'public', 154034, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 28, '2020-12-01 22:41:30', '2020-12-01 22:41:31'),
(29, 'App\\User', 6, 'cad32485-d0b1-4281-ba2e-522c8c69c457', 'cover', '95866106_540353126663501_4239302166201761792_o', '95866106_540353126663501_4239302166201761792_o.jpg', 'image/jpeg', 'public', 'public', 83151, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 29, '2020-12-01 22:43:46', '2020-12-01 22:43:46'),
(30, 'App\\User', 19, '48ae14fa-bcde-4451-920d-4e1ceb5937e8', 'cover', 'glowing splint', 'glowing-splint.png', 'image/png', 'public', 'public', 51161, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 30, '2020-12-01 23:01:24', '2020-12-01 23:01:24'),
(31, 'App\\User', 18, '2c4f776f-4cb5-47ec-a6bc-897694757c80', 'logo', '67528041_1093364064192417_1489665370317914112_o', '67528041_1093364064192417_1489665370317914112_o.jpg', 'image/jpeg', 'public', 'public', 57373, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 31, '2020-12-02 21:07:02', '2020-12-02 21:07:02'),
(32, 'App\\Models\\Product', 9, 'a7358dc4-b9af-496b-9a4f-aff511905dc2', 'product', '117301752_1444939459034874_5603016418782987700_n', '117301752_1444939459034874_5603016418782987700_n.jpg', 'image/jpeg', 'public', 'public', 59233, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 32, '2020-12-02 21:26:53', '2020-12-02 21:26:54'),
(33, 'App\\Models\\Product', 9, '65b9fa1b-ce65-4877-b68f-41324534230c', 'product', '117607654_1444939572368196_4207735459400562168_n', '117607654_1444939572368196_4207735459400562168_n.jpg', 'image/jpeg', 'public', 'public', 63715, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 33, '2020-12-02 21:26:54', '2020-12-02 21:26:54'),
(34, 'App\\Models\\Product', 9, 'ec207812-1fc8-410b-bc29-878bbe65882f', 'product', '117755509_1444939495701537_4326625549856703444_n', '117755509_1444939495701537_4326625549856703444_n.jpg', 'image/jpeg', 'public', 'public', 66267, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 34, '2020-12-02 21:26:54', '2020-12-02 21:26:55'),
(35, 'App\\Models\\Product', 9, 'afcedde7-40fe-44eb-9659-b3b40477b212', 'product', '118075249_1444939425701544_4559414280607865100_n', '118075249_1444939425701544_4559414280607865100_n.jpg', 'image/jpeg', 'public', 'public', 62009, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 35, '2020-12-02 21:26:55', '2020-12-02 21:26:55'),
(36, 'App\\Models\\Product', 10, '00acc5bf-451f-4234-854e-2ce59ec4d301', 'product', '119744360_1480575678804585_3521550057305990840_n', '119744360_1480575678804585_3521550057305990840_n.jpg', 'image/jpeg', 'public', 'public', 90196, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 36, '2020-12-02 21:31:25', '2020-12-02 21:31:26'),
(37, 'App\\Models\\Product', 10, '59db67ff-b70b-415d-9f9b-c31a8f8a6c34', 'product', '119883404_1480575642137922_887577772623009805_n', '119883404_1480575642137922_887577772623009805_n.jpg', 'image/jpeg', 'public', 'public', 98695, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 37, '2020-12-02 21:31:26', '2020-12-02 21:31:27'),
(38, 'App\\Models\\Product', 10, '1a0cb8ab-10a7-4187-afa1-5e3501648195', 'product', '119966101_1480575615471258_6024728271552189916_n', '119966101_1480575615471258_6024728271552189916_n.jpg', 'image/jpeg', 'public', 'public', 103745, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 38, '2020-12-02 21:31:27', '2020-12-02 21:31:27'),
(39, 'App\\Models\\Product', 10, 'd7b00968-73a3-4a26-864d-c5b5a94b58bd', 'product', '120035477_1480575718804581_5449879174350286410_n', '120035477_1480575718804581_5449879174350286410_n.jpg', 'image/jpeg', 'public', 'public', 92192, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 39, '2020-12-02 21:31:27', '2020-12-02 21:31:28'),
(40, 'App\\User', 15, '2da48583-48cf-4033-9233-c31748d03f90', 'logo', '117378561_3174974879276250_4450762720942364784_n', '117378561_3174974879276250_4450762720942364784_n.jpg', 'image/jpeg', 'public', 'public', 61383, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 40, '2020-12-02 21:39:58', '2020-12-02 21:39:59'),
(41, 'App\\User', 15, '725a7ae5-de29-481e-b51b-1668aa323dd9', 'cover', '117378561_3174974879276250_4450762720942364784_n', '117378561_3174974879276250_4450762720942364784_n.jpg', 'image/jpeg', 'public', 'public', 61383, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 41, '2020-12-02 21:40:08', '2020-12-02 21:40:09'),
(42, 'App\\User', 17, '33f21ecb-e683-4f1d-9775-bfa2d03a2196', 'logo', '81039291_1519576628197343_8657306230689103872_n', '81039291_1519576628197343_8657306230689103872_n.jpg', 'image/jpeg', 'public', 'public', 70719, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 42, '2020-12-02 21:59:47', '2020-12-02 21:59:48'),
(43, 'App\\User', 17, '14fd4318-23d6-4138-931a-fe07ca2e2394', 'cover', '81039291_1519576628197343_8657306230689103872_n', '81039291_1519576628197343_8657306230689103872_n.jpg', 'image/jpeg', 'public', 'public', 70719, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 43, '2020-12-02 21:59:58', '2020-12-02 21:59:59'),
(44, 'App\\Models\\Product', 11, '909bd993-f373-4d78-af65-3616ae6b66fd', 'product', '125883412_1821695054652164_558531231077291278_n', '125883412_1821695054652164_558531231077291278_n.jpg', 'image/jpeg', 'public', 'public', 25720, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 44, '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(45, 'App\\Models\\Product', 11, '5d0c32ba-f5ff-45ac-be30-b3bbed40cd4b', 'product', '126044053_1821695184652151_7612890976574302496_n', '126044053_1821695184652151_7612890976574302496_n.jpg', 'image/jpeg', 'public', 'public', 14482, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 45, '2020-12-02 22:04:57', '2020-12-02 22:04:58'),
(46, 'App\\Models\\Product', 11, '95b2e0b9-d838-4d6e-8138-01db3aac039f', 'product', '126889028_1821695111318825_6939161138066713866_n', '126889028_1821695111318825_6939161138066713866_n.jpg', 'image/jpeg', 'public', 'public', 16098, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 46, '2020-12-02 22:04:58', '2020-12-02 22:04:58'),
(47, 'App\\Models\\Product', 12, '32460173-75dd-4944-8dfc-86d63cbec831', 'product', '123841722_1805054562982880_8845800132741911242_n', '123841722_1805054562982880_8845800132741911242_n.jpg', 'image/jpeg', 'public', 'public', 115334, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 47, '2020-12-02 22:12:07', '2020-12-02 22:12:08'),
(48, 'App\\Models\\Product', 12, '4d0aac9a-a07d-47f0-bcfb-ae59426191c3', 'product', '123926726_1805054882982848_2361602542083669805_n', '123926726_1805054882982848_2361602542083669805_n.jpg', 'image/jpeg', 'public', 'public', 97519, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 48, '2020-12-02 22:12:08', '2020-12-02 22:12:08'),
(49, 'App\\Models\\Product', 12, '200c16d8-5965-4b48-83b3-b384273dc35b', 'product', '123970654_1805054776316192_737909930619834410_n', '123970654_1805054776316192_737909930619834410_n.jpg', 'image/jpeg', 'public', 'public', 98531, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 49, '2020-12-02 22:12:08', '2020-12-02 22:12:09'),
(50, 'App\\Models\\Product', 12, '6ef1e138-34f4-4341-80d9-f40ce52c262d', 'product', '124261487_1805055642982772_1378448258679216920_n', '124261487_1805055642982772_1378448258679216920_n.jpg', 'image/jpeg', 'public', 'public', 83615, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 50, '2020-12-02 22:12:09', '2020-12-02 22:12:09'),
(51, 'App\\Models\\Product', 13, 'b72fe5a3-ee5a-4da2-b096-550cfdbefb2a', 'product', '123654683_1805055002982836_2119643970551129896_n', '123654683_1805055002982836_2119643970551129896_n.jpg', 'image/jpeg', 'public', 'public', 91056, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 51, '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(52, 'App\\Models\\Product', 13, 'aff27bbe-b730-4172-8beb-b162b566a5c8', 'product', '123723930_1805054516316218_5318395532754965100_n', '123723930_1805054516316218_5318395532754965100_n.jpg', 'image/jpeg', 'public', 'public', 74846, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 52, '2020-12-02 22:14:29', '2020-12-02 22:14:30'),
(53, 'App\\Models\\Product', 13, '8b2eb3fb-d076-401f-9d9b-f541bb7617be', 'product', '123753339_1805055779649425_538018499357692419_n', '123753339_1805055779649425_538018499357692419_n.jpg', 'image/jpeg', 'public', 'public', 80649, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 53, '2020-12-02 22:14:30', '2020-12-02 22:14:30'),
(54, 'App\\Models\\Product', 13, 'fcc8b7b4-b3cb-4683-ad11-86b6f239b1f3', 'product', '123798763_1805054846316185_8993926891359291132_n', '123798763_1805054846316185_8993926891359291132_n.jpg', 'image/jpeg', 'public', 'public', 66221, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 54, '2020-12-02 22:14:30', '2020-12-02 22:14:31'),
(55, 'App\\User', 8, '194109a8-b46e-4561-a4b3-72e02852b5cf', 'logo', '90080700_2561277337453530_3195468896723271680_n', '90080700_2561277337453530_3195468896723271680_n.jpg', 'image/jpeg', 'public', 'public', 139243, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 55, '2020-12-02 22:21:49', '2020-12-02 22:21:49'),
(56, 'App\\User', 8, '820172dd-de3d-48c5-a509-376606fb4ab8', 'cover', '120454340_2725896854324910_6468255460616511340_o', '120454340_2725896854324910_6468255460616511340_o.jpg', 'image/jpeg', 'public', 'public', 99807, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 56, '2020-12-02 22:22:01', '2020-12-02 22:22:02'),
(57, 'App\\User', 62, 'fc9195fb-b818-440b-a518-32db76ca82f6', 'logo', 'nikhil', 'nikhil.jpg', 'image/jpeg', 'public', 'public', 83684, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 57, '2020-12-03 02:26:00', '2020-12-03 02:26:00'),
(58, 'App\\Models\\Product', 14, '69e716a1-638a-4fe0-a115-d8b75c7d0c4a', 'product', '119441010_2713049972276265_7992501481118800990_n', '119441010_2713049972276265_7992501481118800990_n.jpg', 'image/jpeg', 'public', 'public', 66981, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 58, '2020-12-03 17:45:25', '2020-12-03 17:45:25'),
(59, 'App\\Models\\Product', 14, '3386cb3c-296a-4a91-bc59-d94e20b3d0fe', 'product', '119529560_2713050018942927_4871419769054989806_n', '119529560_2713050018942927_4871419769054989806_n.jpg', 'image/jpeg', 'public', 'public', 39662, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 59, '2020-12-03 17:45:25', '2020-12-03 17:45:26'),
(60, 'App\\Models\\Product', 14, '3eebbf09-09af-49ad-a620-e43400df2333', 'product', '119628126_2713049895609606_3608203510358816250_n', '119628126_2713049895609606_3608203510358816250_n.jpg', 'image/jpeg', 'public', 'public', 38901, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 60, '2020-12-03 17:45:26', '2020-12-03 17:45:27'),
(61, 'App\\Models\\Product', 15, 'f7deea75-23d4-4a56-b818-dc26db6d5884', 'product', '118817975_2702450853336177_4097957223127944753_n', '118817975_2702450853336177_4097957223127944753_n.jpg', 'image/jpeg', 'public', 'public', 38854, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 61, '2020-12-03 17:52:03', '2020-12-03 17:52:03'),
(62, 'App\\Models\\Product', 16, '8f8102d0-acbc-4dd1-95fc-e6184a55eef3', 'product', '83628513_3012680452182793_2564465166027499073_n', '83628513_3012680452182793_2564465166027499073_n.jpg', 'image/jpeg', 'public', 'public', 90306, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 62, '2020-12-03 17:56:51', '2020-12-03 17:56:51'),
(63, 'App\\Models\\Product', 16, '3f6e47c2-aca5-4133-8272-3cc35a53a93f', 'product', '103917329_3160216434039770_7131057345649304834_n', '103917329_3160216434039770_7131057345649304834_n.jpg', 'image/jpeg', 'public', 'public', 116541, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 63, '2020-12-03 17:56:51', '2020-12-03 17:56:52'),
(64, 'App\\Models\\Product', 16, '3bc97d36-def4-48c6-8f8c-17bf3d0c2127', 'product', '104054769_3318127784886538_2729266534203695471_n', '104054769_3318127784886538_2729266534203695471_n.jpg', 'image/jpeg', 'public', 'public', 73920, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 64, '2020-12-03 17:56:52', '2020-12-03 17:56:52'),
(65, 'App\\Models\\Product', 16, '57aa30c4-4f99-431a-bcff-8676889280fd', 'product', '104145691_2807804629324513_7590549062008187768_n', '104145691_2807804629324513_7590549062008187768_n.jpg', 'image/jpeg', 'public', 'public', 75780, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 65, '2020-12-03 17:56:52', '2020-12-03 17:56:53'),
(66, 'App\\Models\\Product', 17, '266609e1-960c-4ef4-beda-6045fe803ac4', 'product', '1', '1.jpg', 'image/jpeg', 'public', 'public', 52539, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 66, '2020-12-03 18:09:17', '2020-12-03 18:09:18'),
(67, 'App\\Models\\Product', 17, '1fc8b2b4-d9df-4a50-9e61-12eb921d8e58', 'product', '2', '2.jpg', 'image/jpeg', 'public', 'public', 48677, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 67, '2020-12-03 18:09:18', '2020-12-03 18:09:18'),
(68, 'App\\Models\\Product', 17, '82b3eefd-32c0-4834-9bc3-6d1806257776', 'product', '3', '3.jpg', 'image/jpeg', 'public', 'public', 34058, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 68, '2020-12-03 18:09:18', '2020-12-03 18:09:18'),
(69, 'App\\Models\\Product', 17, 'c87df63f-3992-4d98-8a27-e4e76a9bad7b', 'product', '4', '4.jpg', 'image/jpeg', 'public', 'public', 38732, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 69, '2020-12-03 18:09:18', '2020-12-03 18:09:19'),
(70, 'App\\Models\\Product', 18, 'bc72a5b6-40dc-4a73-8fd2-07d8a72c8787', 'product', '1', '1.jpg', 'image/jpeg', 'public', 'public', 38732, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 70, '2020-12-03 18:11:03', '2020-12-03 18:11:03'),
(71, 'App\\Models\\Product', 18, 'bb4e0023-2679-4164-a130-e3c2f4a1639b', 'product', '2', '2.jpg', 'image/jpeg', 'public', 'public', 48677, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 71, '2020-12-03 18:11:03', '2020-12-03 18:11:04'),
(72, 'App\\Models\\Product', 18, 'aef0dd88-281f-4735-bf19-3bd9d5ee957d', 'product', '3', '3.jpg', 'image/jpeg', 'public', 'public', 34058, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 72, '2020-12-03 18:11:04', '2020-12-03 18:11:04'),
(73, 'App\\Models\\Product', 18, '8b0dbe2c-a841-4986-8376-43dca60721b5', 'product', '4', '4.jpg', 'image/jpeg', 'public', 'public', 52539, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 73, '2020-12-03 18:11:04', '2020-12-03 18:11:05'),
(74, 'App\\Models\\Product', 19, 'b063a05c-fd74-48c7-a0a5-bc40b6ca6ef0', 'product', '117526546_2684697198444876_3079900074953622168_n', '117526546_2684697198444876_3079900074953622168_n.jpg', 'image/jpeg', 'public', 'public', 52466, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 74, '2020-12-03 18:14:13', '2020-12-03 18:14:13'),
(75, 'App\\Models\\Product', 19, 'c85567a0-1b31-4280-9e46-25197b6d79b0', 'product', '117625002_2684697238444872_4080801756740334354_o', '117625002_2684697238444872_4080801756740334354_o.jpg', 'image/jpeg', 'public', 'public', 63164, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 75, '2020-12-03 18:14:13', '2020-12-03 18:14:14'),
(76, 'App\\User', 9, 'f5106ec4-4677-4a4a-8107-2c2110905716', 'logo', '38', '38.jpg', 'image/jpeg', 'public', 'public', 111084, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 76, '2020-12-03 18:20:12', '2020-12-03 18:20:12'),
(77, 'App\\User', 9, 'cbdcda34-3068-4253-90e5-9c2855e262b2', 'cover', '107007114_135581548172184_5377029337060944700_o', '107007114_135581548172184_5377029337060944700_o.jpg', 'image/jpeg', 'public', 'public', 576108, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 77, '2020-12-03 18:20:22', '2020-12-03 18:20:22'),
(78, 'App\\Models\\Product', 20, '92823da3-44d3-431c-8630-f1a2c58eb03c', 'product', '106298993_135572678173071_2849330042490109129_o', '106298993_135572678173071_2849330042490109129_o.jpg', 'image/jpeg', 'public', 'public', 462203, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 78, '2020-12-03 18:37:12', '2020-12-03 18:37:14'),
(79, 'App\\Models\\Product', 20, '5ef01290-f45d-43d6-a1a0-84506e02f18b', 'product', '106553860_135572551506417_7289944713667365565_o', '106553860_135572551506417_7289944713667365565_o.jpg', 'image/jpeg', 'public', 'public', 487755, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 79, '2020-12-03 18:37:14', '2020-12-03 18:37:15'),
(80, 'App\\Models\\Product', 20, '87f68222-029b-4abe-ab23-6d33c89e7841', 'product', '106991683_135572484839757_2090690962282562100_o', '106991683_135572484839757_2090690962282562100_o.jpg', 'image/jpeg', 'public', 'public', 261981, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 80, '2020-12-03 18:37:15', '2020-12-03 18:37:17'),
(81, 'App\\Models\\Product', 20, 'bb7446da-f0df-4185-b818-53833e0528b3', 'product', '107641663_135572638173075_782226635687605216_o', '107641663_135572638173075_782226635687605216_o.jpg', 'image/jpeg', 'public', 'public', 301016, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 81, '2020-12-03 18:37:17', '2020-12-03 18:37:18'),
(82, 'App\\User', 10, 'ff8c6e5e-89ce-4f2a-85b2-6cf67894b0e8', 'logo', 'logo', 'logo.jpg', 'image/jpeg', 'public', 'public', 70076, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 82, '2020-12-03 18:52:27', '2020-12-03 18:52:27'),
(83, 'App\\User', 10, '9990f591-d80b-4149-9938-40dc679b2932', 'cover', '109530507_117925220002293_4989600848573863759_o', '109530507_117925220002293_4989600848573863759_o.jpg', 'image/jpeg', 'public', 'public', 1292337, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 83, '2020-12-03 18:55:29', '2020-12-03 18:55:30'),
(84, 'App\\User', 11, '678be2e7-a8a0-4e8c-b1e6-6cdeea1a1e70', 'logo', '40', '40.jpg', 'image/jpeg', 'public', 'public', 110871, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 84, '2020-12-03 19:08:42', '2020-12-03 19:08:42'),
(86, 'App\\User', 11, '2bf9105c-3b57-4bd6-a660-a366bcdfda49', 'cover', 'cover', 'cover.jpg', 'image/jpeg', 'public', 'public', 49915, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 85, '2020-12-03 19:14:16', '2020-12-03 19:14:17'),
(87, 'App\\Models\\Product', 21, '34011718-120f-4d77-a920-770940ae301f', 'product', '1', '1.jpg', 'image/jpeg', 'public', 'public', 142244, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 86, '2020-12-03 19:17:50', '2020-12-03 19:17:52'),
(88, 'App\\Models\\Product', 21, '833c5573-4435-433f-9127-eace24a834c0', 'product', '2', '2.jpg', 'image/jpeg', 'public', 'public', 91532, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 87, '2020-12-03 19:17:52', '2020-12-03 19:17:53'),
(89, 'App\\Models\\Product', 21, '4d8834ab-b84d-48c8-bf5d-17fac6d119a4', 'product', '3', '3.jpg', 'image/jpeg', 'public', 'public', 104455, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 88, '2020-12-03 19:17:53', '2020-12-03 19:17:54'),
(90, 'App\\Models\\Product', 22, 'bff7a0a4-2ded-4d87-83f4-bc7fd8618842', 'product', '119113497_129185462227116_6362886269107089060_n', '119113497_129185462227116_6362886269107089060_n.jpg', 'image/jpeg', 'public', 'public', 99615, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 89, '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(91, 'App\\Models\\Product', 22, '6bd5ebc1-8f92-4e4e-ae3a-d686d0b8e0a6', 'product', '119151952_129185478893781_6013432990736113300_n', '119151952_129185478893781_6013432990736113300_n.jpg', 'image/jpeg', 'public', 'public', 88222, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 90, '2020-12-03 19:23:30', '2020-12-03 19:23:31'),
(92, 'App\\Models\\Product', 23, '07639972-78d3-422e-99ae-06181cd3db96', 'product', '121694460_139355371210125_2105085212076891112_o (1)', '121694460_139355371210125_2105085212076891112_o-(1).jpg', 'image/jpeg', 'public', 'public', 151306, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 91, '2020-12-03 19:43:03', '2020-12-03 19:43:04'),
(93, 'App\\Models\\Product', 24, 'a5efa757-4d6b-4957-9ac2-f367144511e2', 'product', '121671763_139355401210122_5873887532057427259_o', '121671763_139355401210122_5873887532057427259_o.jpg', 'image/jpeg', 'public', 'public', 122751, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 92, '2020-12-03 19:43:48', '2020-12-03 19:43:49'),
(94, 'App\\User', 12, '94d41644-a843-4e05-941a-4edc48932e7b', 'logo', 'Untitled-1', 'Untitled-1.jpg', 'image/jpeg', 'public', 'public', 103760, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 93, '2020-12-03 20:00:32', '2020-12-03 20:00:34'),
(95, 'App\\User', 12, '787cbc17-061d-4acf-bba4-000fc3a4396d', 'cover', '118769645_204983060978050_744397795538736936_o', '118769645_204983060978050_744397795538736936_o.jpg', 'image/jpeg', 'public', 'public', 403755, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 94, '2020-12-03 20:01:53', '2020-12-03 20:01:55'),
(96, 'App\\Models\\Product', 25, 'd8da058b-1eac-4e42-ae7a-a78d27b97c43', 'product', '118769645_204983060978050_744397795538736936_o', '118769645_204983060978050_744397795538736936_o.jpg', 'image/jpeg', 'public', 'public', 403755, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 95, '2020-12-03 20:03:22', '2020-12-03 20:03:24'),
(97, 'App\\Models\\Product', 25, '46cd0438-17c1-44e9-831f-448421480615', 'product', '120837055_213528160123540_5607269178635337828_n', '120837055_213528160123540_5607269178635337828_n.jpg', 'image/jpeg', 'public', 'public', 181621, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 96, '2020-12-03 20:03:24', '2020-12-03 20:03:25'),
(98, 'App\\Models\\Product', 26, 'e75d3d65-a448-4bb0-a227-3cf571a2ca0d', 'product', '117671621_197780441698312_6409398694499605655_o', '117671621_197780441698312_6409398694499605655_o.jpg', 'image/jpeg', 'public', 'public', 783768, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 97, '2020-12-03 20:18:43', '2020-12-03 20:18:44'),
(99, 'App\\Models\\Product', 27, '9cb535f8-da5d-4775-93cb-b52bbb047c7c', 'product', '120705233_213186333491056_7434055105979515446_o', '120705233_213186333491056_7434055105979515446_o.jpg', 'image/jpeg', 'public', 'public', 637713, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 98, '2020-12-03 20:27:57', '2020-12-03 20:27:59'),
(100, 'App\\User', 13, 'f571b113-e654-4023-9fbf-1c5a034186cf', 'logo', '120346978_211479767071159_2979811570598125588_o', '120346978_211479767071159_2979811570598125588_o.png', 'image/png', 'public', 'public', 80590, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 99, '2020-12-03 20:33:33', '2020-12-03 20:33:33'),
(101, 'App\\User', 13, '140b7b64-31ae-4514-bec6-0e97f2c5b02c', 'cover', '118373325_198573025028500_4064403545122907128_o', '118373325_198573025028500_4064403545122907128_o.jpg', 'image/jpeg', 'public', 'public', 707972, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 100, '2020-12-03 20:34:40', '2020-12-03 20:34:41'),
(102, 'App\\Models\\Product', 28, '67046dbf-8d7a-4857-b532-57a883b26d4c', 'product', '121328689_215903626628773_1700022234289809060_n', '121328689_215903626628773_1700022234289809060_n.jpg', 'image/jpeg', 'public', 'public', 49358, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 101, '2020-12-03 20:47:06', '2020-12-03 20:47:07'),
(103, 'App\\Models\\Product', 28, '513671c7-450f-49e7-9285-35185ec83098', 'product', '121641256_215903643295438_8153880346369057841_n', '121641256_215903643295438_8153880346369057841_n.jpg', 'image/jpeg', 'public', 'public', 49639, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 102, '2020-12-03 20:47:07', '2020-12-03 20:47:07'),
(104, 'App\\Models\\Product', 29, '99bd98b8-66be-474f-8076-344e3b9595b0', 'product', '121369579_215901776628958_7821096453789501864_n', '121369579_215901776628958_7821096453789501864_n.jpg', 'image/jpeg', 'public', 'public', 46409, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 103, '2020-12-03 20:48:16', '2020-12-03 20:48:17'),
(105, 'App\\Models\\Product', 30, 'c22b8731-5efe-4299-ba1d-b2b189f4b9be', 'product', '118719723_202984337920702_3566797090072889554_o', '118719723_202984337920702_3566797090072889554_o.jpg', 'image/jpeg', 'public', 'public', 485677, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 104, '2020-12-03 20:50:19', '2020-12-03 20:50:21'),
(106, 'App\\Models\\Product', 30, 'b5c815eb-b2e4-4e9c-be34-fae038f08d3a', 'product', '118785587_202984614587341_613162142646138690_o', '118785587_202984614587341_613162142646138690_o.jpg', 'image/jpeg', 'public', 'public', 726506, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 105, '2020-12-03 20:50:21', '2020-12-03 20:50:22'),
(107, 'App\\Models\\Product', 30, '3523d56e-1c0c-4bff-94d4-0e2537db16ab', 'product', '118812030_202984714587331_276024148645782905_o', '118812030_202984714587331_276024148645782905_o.jpg', 'image/jpeg', 'public', 'public', 380227, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 106, '2020-12-03 20:50:22', '2020-12-03 20:50:24'),
(108, 'App\\Models\\Product', 30, '0a9edd2f-4c1b-4572-8a38-ead488ad920e', 'product', '120141725_209846187234517_884482389655466726_n', '120141725_209846187234517_884482389655466726_n.jpg', 'image/jpeg', 'public', 'public', 82740, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 107, '2020-12-03 20:50:24', '2020-12-03 20:50:24'),
(109, 'App\\Models\\Product', 31, 'd32515c9-fd61-4e80-833e-42d385c05134', 'product', '118811494_203299211222548_777691079514070345_o', '118811494_203299211222548_777691079514070345_o.jpg', 'image/jpeg', 'public', 'public', 647704, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 108, '2020-12-03 20:52:01', '2020-12-03 20:52:03'),
(110, 'App\\Models\\Product', 31, '35f423b9-d627-49a0-87bd-2c3f1bc93912', 'product', '118883529_203299417889194_1516192438024346527_o', '118883529_203299417889194_1516192438024346527_o.jpg', 'image/jpeg', 'public', 'public', 504708, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 109, '2020-12-03 20:52:03', '2020-12-03 20:52:04'),
(111, 'App\\Models\\Product', 31, 'e6c4efbd-4f5a-4c4d-b3ec-b0e874d11d3d', 'product', '118995389_203298691222600_6468741249154160730_o', '118995389_203298691222600_6468741249154160730_o.jpg', 'image/jpeg', 'public', 'public', 431553, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 110, '2020-12-03 20:52:04', '2020-12-03 20:52:06'),
(112, 'App\\Models\\Product', 31, 'aa3d62c4-a1a0-4666-b026-a6751071b2fe', 'product', '120218292_209846207234515_2633775551728793448_n', '120218292_209846207234515_2633775551728793448_n.jpg', 'image/jpeg', 'public', 'public', 72371, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 111, '2020-12-03 20:52:06', '2020-12-03 20:52:07'),
(113, 'App\\Models\\Product', 32, '52a73993-893d-4fb7-bd37-0efa51cf0b1c', 'product', '120966036_214376913448111_2629593106243921809_n', '120966036_214376913448111_2629593106243921809_n.jpg', 'image/jpeg', 'public', 'public', 32651, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 112, '2020-12-03 20:56:53', '2020-12-03 20:56:54'),
(114, 'App\\Models\\Product', 32, '95276ba0-7b2a-4e7d-b4f4-ad93a11fbe82', 'product', '121253282_214377026781433_8909188296139017524_n', '121253282_214377026781433_8909188296139017524_n.jpg', 'image/jpeg', 'public', 'public', 166860, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 113, '2020-12-03 20:56:54', '2020-12-03 20:56:54'),
(115, 'App\\Models\\Product', 32, '4593489e-52e5-4aae-9ae8-8f6058dfbc24', 'product', '121401008_214376953448107_7769806702375050097_n', '121401008_214376953448107_7769806702375050097_n.jpg', 'image/jpeg', 'public', 'public', 73466, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 114, '2020-12-03 20:56:54', '2020-12-03 20:56:55'),
(116, 'App\\Models\\Product', 33, '3b6fd61c-cfd5-485b-a535-1a65bc3f6c05', 'product', '120705814_212697523616050_3398480100422147986_o', '120705814_212697523616050_3398480100422147986_o.jpg', 'image/jpeg', 'public', 'public', 482987, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 115, '2020-12-03 20:59:32', '2020-12-03 20:59:34'),
(117, 'App\\Models\\Product', 34, '0fbc1dca-14e2-49a5-848c-79971f1fba42', 'product', '120233059_210524087166727_9058119256392289809_o', '120233059_210524087166727_9058119256392289809_o.jpg', 'image/jpeg', 'public', 'public', 453285, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 116, '2020-12-03 21:05:19', '2020-12-03 21:05:20'),
(118, 'App\\Models\\Product', 34, 'd7da22f8-868d-4ad0-a5f9-cd3efe3500f7', 'product', '120233543_210524180500051_703905017931963734_o', '120233543_210524180500051_703905017931963734_o.jpg', 'image/jpeg', 'public', 'public', 478606, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 117, '2020-12-03 21:05:20', '2020-12-03 21:05:22'),
(119, 'App\\User', 14, 'd21d5abf-c3e2-4e60-ba1c-7047775106cd', 'logo', 'Chanu', 'Chanu.jpeg', 'image/jpeg', 'public', 'public', 30952, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 118, '2020-12-03 21:28:43', '2020-12-03 21:28:43'),
(120, 'App\\User', 14, '4865fdfb-861d-4686-ad67-395085dcbb65', 'cover', 'Chanu', 'Chanu.jpeg', 'image/jpeg', 'public', 'public', 30952, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 119, '2020-12-03 21:29:05', '2020-12-03 21:29:05'),
(121, 'App\\Models\\Product', 35, 'a648770a-f6e6-43d6-a815-71720a5d55b3', 'product', '125871062_3592221797525800_9210927450226832439_o', '125871062_3592221797525800_9210927450226832439_o.jpg', 'image/jpeg', 'public', 'public', 182530, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 120, '2020-12-03 21:45:10', '2020-12-03 21:45:11'),
(122, 'App\\Models\\Product', 35, 'c909b711-a4d4-4823-b78b-940edb17fd88', 'product', '126021556_3592222084192438_8968704995803229835_o', '126021556_3592222084192438_8968704995803229835_o.jpg', 'image/jpeg', 'public', 'public', 220027, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 121, '2020-12-03 21:45:11', '2020-12-03 21:45:12'),
(123, 'App\\Models\\Product', 35, '33f661db-d4a1-4f37-ae25-7045e62c3738', 'product', '126817727_3592221940859119_3141280914588378929_o', '126817727_3592221940859119_3141280914588378929_o.jpg', 'image/jpeg', 'public', 'public', 206599, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 122, '2020-12-03 21:45:12', '2020-12-03 21:45:13'),
(124, 'App\\Models\\Product', 36, '7463676a-d825-4bfb-9afb-11c3dcf4d2a6', 'product', '126180455_3592217677526212_5456799454691748108_o', '126180455_3592217677526212_5456799454691748108_o.jpg', 'image/jpeg', 'public', 'public', 223495, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 123, '2020-12-03 21:49:12', '2020-12-03 21:49:12'),
(125, 'App\\Models\\Product', 36, '52dc64da-48ce-4801-891d-f82b79dc1df4', 'product', '126425432_3592217564192890_4950663645170005236_o', '126425432_3592217564192890_4950663645170005236_o.jpg', 'image/jpeg', 'public', 'public', 266399, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 124, '2020-12-03 21:49:12', '2020-12-03 21:49:13'),
(126, 'App\\Models\\Product', 36, '2b6ad5da-676d-438e-89c8-2cdede8167a5', 'product', '127047391_3592217394192907_8060532375569230437_o', '127047391_3592217394192907_8060532375569230437_o.jpg', 'image/jpeg', 'public', 'public', 231608, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 125, '2020-12-03 21:49:13', '2020-12-03 21:49:14'),
(128, 'App\\User', 21, '21fb5b56-b87d-4c8e-b19a-1f1097fe5040', 'cover', '119121660_1014943365608648_1915539330222463145_o', '119121660_1014943365608648_1915539330222463145_o.jpg', 'image/jpeg', 'public', 'public', 168010, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 127, '2020-12-04 21:53:19', '2020-12-04 21:53:20'),
(131, 'App\\Models\\Product', 37, '54d8e14c-5ffa-4395-91d7-cefc6d0795ab', 'product', '128430293_1078486069254377_5597452202376913441_o', '128430293_1078486069254377_5597452202376913441_o.jpg', 'image/jpeg', 'public', 'public', 597978, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 128, '2020-12-04 21:55:59', '2020-12-04 21:56:00'),
(132, 'App\\Models\\Product', 37, 'e9a0aece-8204-432e-847d-d483924159fa', 'product', '128762507_1078485999254384_5818621358240762963_o', '128762507_1078485999254384_5818621358240762963_o.jpg', 'image/jpeg', 'public', 'public', 520775, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 129, '2020-12-04 21:56:00', '2020-12-04 21:56:02'),
(133, 'App\\Models\\Product', 37, 'cbede451-5e1a-42aa-aa48-f0ffb863a37a', 'product', '129590464_2866946090247803_4043716122244853692_o', '129590464_2866946090247803_4043716122244853692_o.jpg', 'image/jpeg', 'public', 'public', 667914, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 130, '2020-12-04 21:56:02', '2020-12-04 21:56:03'),
(134, 'App\\Models\\Product', 38, '8384bc60-7ead-4ce4-8f4d-08d62ec2c6c3', 'product', '127609925_1077144036055247_6444127974737839167_o', '127609925_1077144036055247_6444127974737839167_o.jpg', 'image/jpeg', 'public', 'public', 789095, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 131, '2020-12-04 21:58:49', '2020-12-04 21:58:51'),
(135, 'App\\Models\\Product', 39, '0e230f20-590d-4168-a424-cb04e4f61d2b', 'product', '125961615_1069587540144230_9048946746359781338_o', '125961615_1069587540144230_9048946746359781338_o.jpg', 'image/jpeg', 'public', 'public', 442230, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 132, '2020-12-05 05:10:43', '2020-12-05 05:10:45'),
(136, 'App\\Models\\Product', 40, '73c89822-f368-4942-8ad5-7a43166b6986', 'product', '126602258_1069587510144233_6243098615320899322_o', '126602258_1069587510144233_6243098615320899322_o.jpg', 'image/jpeg', 'public', 'public', 269149, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 133, '2020-12-05 05:11:23', '2020-12-05 05:11:24'),
(137, 'App\\Models\\Product', 41, 'a9f6c883-dfbb-4019-964f-324f85cabbc3', 'product', '126020168_1069587643477553_791933442495780692_o', '126020168_1069587643477553_791933442495780692_o.jpg', 'image/jpeg', 'public', 'public', 518263, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 134, '2020-12-05 05:12:11', '2020-12-05 05:12:12');
INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(138, 'App\\Models\\Product', 42, 'a562e602-aa53-446d-9d56-1ee84f683f06', 'product', '126841057_1069587580144226_6466590958785790960_o', '126841057_1069587580144226_6466590958785790960_o.jpg', 'image/jpeg', 'public', 'public', 312856, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 135, '2020-12-05 05:13:08', '2020-12-05 05:13:10'),
(139, 'App\\User', 60, 'ed473c31-d28d-4121-b644-7b2905119deb', 'logo', '128743567_3480711068651118_7589616366096928181_n', '128743567_3480711068651118_7589616366096928181_n.jpg', 'image/jpeg', 'public', 'public', 26409, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 136, '2020-12-05 06:29:51', '2020-12-05 06:29:52'),
(140, 'App\\User', 60, 'd66f82ac-69d2-499c-8bb6-cd721036a2e4', 'cover', '122119807_1242787982753651_94285092281585440_o', '122119807_1242787982753651_94285092281585440_o.jpg', 'image/jpeg', 'public', 'public', 333249, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 137, '2020-12-05 06:45:53', '2020-12-05 06:45:54'),
(141, 'App\\Models\\Product', 43, 'e8d2954c-a31b-4fb2-8fe1-a584c6af3486', 'product', '129093863_1279585355740580_1821877788984465672_o', '129093863_1279585355740580_1821877788984465672_o.jpg', 'image/jpeg', 'public', 'public', 389915, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 138, '2020-12-05 06:58:49', '2020-12-05 06:58:50'),
(142, 'App\\Models\\Product', 44, '51b1100c-6b54-44dc-a419-f5880c49f6e9', 'product', '128652314_1279584649073984_4998842970632731655_o (1)', '128652314_1279584649073984_4998842970632731655_o-(1).jpg', 'image/jpeg', 'public', 'public', 334140, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 139, '2020-12-05 07:04:53', '2020-12-05 07:04:54'),
(143, 'App\\User', 24, 'add91d09-8a3b-4f1a-9901-810c8d39a0f9', 'cover', 'll', 'll.png', 'image/png', 'public', 'public', 4004399, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 140, '2020-12-05 22:34:28', '2020-12-05 22:34:29'),
(144, 'App\\User', 24, 'a8fa1b59-3d9d-44be-893e-e99c121a2085', 'logo', 'll', 'll.jpg', 'image/jpeg', 'public', 'public', 182042, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 141, '2020-12-05 22:38:29', '2020-12-05 22:38:29'),
(145, 'App\\User', 63, 'ef6a0381-b3cc-4f2c-994b-fa4d4d20f97a', 'cover', '122774750_2698100700451572_487655526896850668_o', '122774750_2698100700451572_487655526896850668_o.jpg', 'image/jpeg', 'public', 'public', 158982, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 142, '2020-12-06 19:16:28', '2020-12-06 19:16:29'),
(146, 'App\\User', 63, '85d15b88-9f89-49d8-ae74-794b49c11031', 'logo', 'Untitled-1', 'Untitled-1.jpg', 'image/jpeg', 'public', 'public', 107982, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 143, '2020-12-06 19:20:10', '2020-12-06 19:20:10'),
(147, 'App\\Models\\Product', 45, '06bf0d52-84b3-4faa-87e0-f58b3d6b471b', 'product', '129726541_2731781147083527_8947682935336646343_o', '129726541_2731781147083527_8947682935336646343_o.jpg', 'image/jpeg', 'public', 'public', 714787, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 144, '2020-12-06 19:38:52', '2020-12-06 19:38:54'),
(148, 'App\\Models\\Product', 46, 'd6fb668e-3852-4f07-a0be-64c15ed34477', 'product', '130084114_2731781610416814_2535884071072496119_o', '130084114_2731781610416814_2535884071072496119_o.jpg', 'image/jpeg', 'public', 'public', 624846, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 145, '2020-12-06 19:43:53', '2020-12-06 19:43:55'),
(149, 'App\\Models\\Product', 47, 'a6aaacef-726c-4dca-8e36-6408c5fbb2d3', 'product', '127792161_2731785273749781_4406406094732090_o', '127792161_2731785273749781_4406406094732090_o.jpg', 'image/jpeg', 'public', 'public', 541391, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 146, '2020-12-06 19:51:32', '2020-12-06 19:51:34'),
(150, 'App\\Models\\Product', 47, '6f0c3dc8-1b18-4266-b0cd-7338ac54c1ad', 'product', '128424439_2731784813749827_9016440555080331862_o', '128424439_2731784813749827_9016440555080331862_o.jpg', 'image/jpeg', 'public', 'public', 509346, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 147, '2020-12-06 19:51:34', '2020-12-06 19:51:37'),
(151, 'App\\User', 64, '71b396f7-d486-4e4d-9f45-5106ef96846e', 'logo', 'logo', 'logo.jpg', 'image/jpeg', 'public', 'public', 479177, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 148, '2020-12-06 21:18:52', '2020-12-06 21:18:52'),
(152, 'App\\User', 64, '38ad4c66-81df-4be4-8da5-f7bc3e9a32ee', 'cover', '67366476_1447234798750803_6466858332610101248_o', '67366476_1447234798750803_6466858332610101248_o.jpg', 'image/jpeg', 'public', 'public', 609906, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 149, '2020-12-06 21:22:14', '2020-12-06 21:22:15'),
(153, 'App\\User', 56, '57ecb7cd-c363-4d2b-8b02-331f82f173b6', 'logo', '72309736_1720630298071270_2734657844035977216_o', '72309736_1720630298071270_2734657844035977216_o.png', 'image/png', 'public', 'public', 21668, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 150, '2020-12-06 21:55:45', '2020-12-06 21:55:45'),
(154, 'App\\User', 56, '22091c51-f119-4089-bb53-1ba607ee9736', 'cover', '72309736_1720630298071270_2734657844035977216_o', '72309736_1720630298071270_2734657844035977216_o.png', 'image/png', 'public', 'public', 21668, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 151, '2020-12-06 21:56:14', '2020-12-06 21:56:15'),
(155, 'App\\Models\\Product', 48, '463daefa-4670-424d-bcfb-ba5d5ee060ff', 'product', '129267679_2170649029736059_2921454535320209732_o', '129267679_2170649029736059_2921454535320209732_o.jpg', 'image/jpeg', 'public', 'public', 112527, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 152, '2020-12-06 22:00:37', '2020-12-06 22:00:39'),
(156, 'App\\Models\\Product', 48, 'bb4b9700-8625-42e6-ad2e-8cd55043f98f', 'product', '129601933_2170649033069392_6753721594432278649_o', '129601933_2170649033069392_6753721594432278649_o.jpg', 'image/jpeg', 'public', 'public', 242184, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 153, '2020-12-06 22:00:39', '2020-12-06 22:00:40'),
(157, 'App\\Models\\Product', 49, 'daf53d0e-167b-451a-9c4e-a58ae922712f', 'product', 'FB_IMG_1607248380967', 'FB_IMG_1607248380967.jpg', 'image/jpeg', 'public', 'public', 299332, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 154, '2020-12-06 22:56:54', '2020-12-06 22:56:55'),
(158, 'App\\Models\\Product', 50, '02a1a252-1fbb-41a7-b2f6-acfc42a21596', 'product', '129158048_3555283287870867_275573891186206937_o', '129158048_3555283287870867_275573891186206937_o.jpg', 'image/jpeg', 'public', 'public', 162461, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 155, '2020-12-07 17:52:31', '2020-12-07 17:52:32'),
(159, 'App\\Models\\Product', 50, '77ffa861-1c92-4098-92e8-f257f522e7ec', 'product', '129407683_3555283507870845_5501635981950332273_o', '129407683_3555283507870845_5501635981950332273_o.jpg', 'image/jpeg', 'public', 'public', 185747, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 156, '2020-12-07 17:52:32', '2020-12-07 17:52:33'),
(160, 'App\\Models\\Product', 50, '67e7e41d-6ea5-4be0-a7e0-167131ed2443', 'product', '130286817_3555283394537523_3794512023824230224_o', '130286817_3555283394537523_3794512023824230224_o.jpg', 'image/jpeg', 'public', 'public', 123207, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 157, '2020-12-07 17:52:33', '2020-12-07 17:52:34'),
(161, 'App\\Models\\Product', 51, '0f873bdc-e127-4e11-b7ae-81901ffac25d', 'product', '1', '1.jpg', 'image/jpeg', 'public', 'public', 123207, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 158, '2020-12-07 17:55:41', '2020-12-07 17:55:42'),
(162, 'App\\Models\\Product', 51, '63d8dbf1-4817-46b7-9c7a-8763ad885990', 'product', '2', '2.jpg', 'image/jpeg', 'public', 'public', 185747, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 159, '2020-12-07 17:55:42', '2020-12-07 17:55:43'),
(163, 'App\\Models\\Product', 51, 'a804d693-2a8e-4b24-a6a4-ff64a4e75c5c', 'product', '3', '3.jpg', 'image/jpeg', 'public', 'public', 162461, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 160, '2020-12-07 17:55:43', '2020-12-07 17:55:43'),
(164, 'App\\Models\\Product', 52, 'a3101fc7-83c3-472a-93d7-65e8528c55b5', 'product', '129234636_3555282537870942_4383950845184909046_o', '129234636_3555282537870942_4383950845184909046_o.jpg', 'image/jpeg', 'public', 'public', 364597, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 161, '2020-12-07 17:58:34', '2020-12-07 17:58:35'),
(165, 'App\\Models\\Product', 52, '4caaccc4-25cc-4394-b43c-cc41c354e511', 'product', '129477523_3555282747870921_7861730379583624826_o', '129477523_3555282747870921_7861730379583624826_o.jpg', 'image/jpeg', 'public', 'public', 378569, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 162, '2020-12-07 17:58:35', '2020-12-07 17:58:37'),
(166, 'App\\Models\\Product', 52, 'fb438db5-7212-4d7b-b4c0-cc8807e34914', 'product', '129957087_3555282337870962_4152205735282251472_o', '129957087_3555282337870962_4152205735282251472_o.jpg', 'image/jpeg', 'public', 'public', 388825, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 163, '2020-12-07 17:58:37', '2020-12-07 17:58:38'),
(167, 'App\\Models\\Product', 53, 'da4c64e1-8623-4f28-979f-c507868dc168', 'product', '129234636_3555282537870942_4383950845184909046_o', '129234636_3555282537870942_4383950845184909046_o.jpg', 'image/jpeg', 'public', 'public', 364597, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 164, '2020-12-07 18:00:51', '2020-12-07 18:00:52'),
(168, 'App\\Models\\Product', 54, '36ed9adb-e4e6-400e-a237-67fcfa1c6ddb', 'product', '129872443_3555285527870643_5239384202264018070_o', '129872443_3555285527870643_5239384202264018070_o.jpg', 'image/jpeg', 'public', 'public', 179713, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 165, '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(169, 'App\\Models\\Product', 54, '9f6c2711-8fee-4b76-b5de-a53e5c56c8f8', 'product', '130245911_3555285441203985_3593193513274780147_o', '130245911_3555285441203985_3593193513274780147_o.jpg', 'image/jpeg', 'public', 'public', 139223, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 166, '2020-12-07 18:09:30', '2020-12-07 18:09:31'),
(170, 'App\\Models\\Product', 54, '43960080-ad78-4878-94f8-d6cdeaa9ad12', 'product', '130286817_3555285381203991_8947181857535825573_o', '130286817_3555285381203991_8947181857535825573_o.jpg', 'image/jpeg', 'public', 'public', 188315, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 167, '2020-12-07 18:09:31', '2020-12-07 18:09:32'),
(171, 'App\\Models\\Product', 55, '191bb8cc-8898-476d-8a2d-a40d08de55ef', 'product', '128643208_3552656138133582_1119959193954420915_o', '128643208_3552656138133582_1119959193954420915_o.jpg', 'image/jpeg', 'public', 'public', 569848, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 168, '2020-12-07 18:13:02', '2020-12-07 18:13:04'),
(172, 'App\\Models\\Product', 55, '707e1ed4-dcf1-46df-a302-237eda62d5d2', 'product', '129362861_3552656464800216_4834150524675923471_o', '129362861_3552656464800216_4834150524675923471_o.jpg', 'image/jpeg', 'public', 'public', 585778, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 169, '2020-12-07 18:13:04', '2020-12-07 18:13:06'),
(173, 'App\\Models\\Product', 55, 'cabb017f-50b3-4c12-9503-b7b381d9002e', 'product', '130119096_3552656631466866_4016943294438042371_o', '130119096_3552656631466866_4016943294438042371_o.jpg', 'image/jpeg', 'public', 'public', 493519, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 170, '2020-12-07 18:13:06', '2020-12-07 18:13:07'),
(174, 'App\\Models\\Product', 56, '5919c5ba-4178-45fe-b6c8-d17dcf8ae52c', 'product', '128770878_3552659664799896_228293370875314099_o', '128770878_3552659664799896_228293370875314099_o.jpg', 'image/jpeg', 'public', 'public', 488151, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 171, '2020-12-07 18:15:33', '2020-12-07 18:15:34'),
(175, 'App\\Models\\Product', 56, '7be557e6-0052-4b9a-afb9-61f6766d7d36', 'product', '129346331_3552659834799879_9135082798740708685_o', '129346331_3552659834799879_9135082798740708685_o.jpg', 'image/jpeg', 'public', 'public', 371519, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 172, '2020-12-07 18:15:34', '2020-12-07 18:15:36'),
(176, 'App\\Models\\Product', 56, 'e9c697d9-38f6-410e-a333-f26916b9bc4d', 'product', '129757240_3552660001466529_4928365296825569393_o', '129757240_3552660001466529_4928365296825569393_o.jpg', 'image/jpeg', 'public', 'public', 322978, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 173, '2020-12-07 18:15:36', '2020-12-07 18:15:37'),
(177, 'App\\User', 25, '6307d516-ae03-4a50-913c-7c3c251f05bf', 'logo', '96390597_161116672098434_5543629664653672448_n', '96390597_161116672098434_5543629664653672448_n.jpg', 'image/jpeg', 'public', 'public', 54502, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 174, '2020-12-07 19:06:55', '2020-12-07 19:06:55'),
(178, 'App\\User', 25, '21161916-a210-454d-838b-ae3a9aaf8ae4', 'cover', '106717719_178992946977473_2585146653458887343_o', '106717719_178992946977473_2585146653458887343_o.png', 'image/png', 'public', 'public', 641429, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 175, '2020-12-07 19:07:17', '2020-12-07 19:07:18'),
(179, 'App\\User', 65, 'a126e53d-3b1f-48f2-96f3-13ff09c52f10', 'cover', '55658744_1190292237798619_560522940416786432_o', '55658744_1190292237798619_560522940416786432_o.jpg', 'image/jpeg', 'public', 'public', 179496, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 176, '2020-12-08 19:04:27', '2020-12-08 19:04:27'),
(180, 'App\\User', 65, 'f10af0aa-f849-4068-97eb-de664ec9eff8', 'logo', '123486822_1714153835412454_856559771197518020_o', '123486822_1714153835412454_856559771197518020_o.jpg', 'image/jpeg', 'public', 'public', 318820, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 177, '2020-12-08 19:04:36', '2020-12-08 19:04:36'),
(181, 'App\\User', 66, '0d41427f-a5c8-420b-8dda-c2402c9bb3d4', 'logo', '117224747_220008226227331_1999310646826493548_n', '117224747_220008226227331_1999310646826493548_n.jpg', 'image/jpeg', 'public', 'public', 24915, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 178, '2020-12-08 21:19:19', '2020-12-08 21:19:20'),
(182, 'App\\User', 66, 'bb2c9485-0628-4535-84ae-71042c0830b0', 'cover', '128412984_256698859224934_1051870780580773223_o', '128412984_256698859224934_1051870780580773223_o.jpg', 'image/jpeg', 'public', 'public', 946928, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 179, '2020-12-08 21:19:31', '2020-12-08 21:19:33'),
(183, 'App\\Models\\Product', 57, '68bd6de9-dd9b-4189-89a1-8dd3477a6f12', 'product', 'FB_IMG_1607487875668', 'FB_IMG_1607487875668.jpg', 'image/jpeg', 'public', 'public', 103531, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 180, '2020-12-09 17:29:18', '2020-12-09 17:29:19'),
(184, 'App\\User', 18, 'dcdd72bb-a7b0-4b03-89a4-86b47c55d1b0', 'cover', '117870030_1443931565802330_8752660404319183258_n', '117870030_1443931565802330_8752660404319183258_n.jpg', 'image/jpeg', 'public', 'public', 85517, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 181, '2020-12-09 18:23:03', '2020-12-09 18:23:04'),
(185, 'App\\User', 59, '878905f6-5b27-4f41-b72d-76beb8742dd0', 'logo', '59947540_343530336359458_7652674549355380736_n', '59947540_343530336359458_7652674549355380736_n.jpg', 'image/jpeg', 'public', 'public', 106247, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 182, '2020-12-09 18:26:27', '2020-12-09 18:26:28'),
(186, 'App\\User', 59, 'c45b0a43-0fe5-4408-8d0c-14476ae9c24e', 'cover', 'web logo', 'web-logo.png', 'image/png', 'public', 'public', 289067, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 183, '2020-12-09 18:29:04', '2020-12-09 18:29:04'),
(187, 'App\\Models\\Product', 58, 'd8f068b6-ac2c-4635-ace7-9fd703b73e8a', 'product', '129936130_687948891917599_3222097736667026366_o', '129936130_687948891917599_3222097736667026366_o.jpg', 'image/jpeg', 'public', 'public', 153519, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 184, '2020-12-09 18:34:42', '2020-12-09 18:34:43'),
(188, 'App\\Models\\Product', 59, '4f305f70-1f08-4672-9e82-8a432f6f09c1', 'product', '129546277_687949015250920_6955593375642638477_o', '129546277_687949015250920_6955593375642638477_o.jpg', 'image/jpeg', 'public', 'public', 216422, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 185, '2020-12-09 18:39:31', '2020-12-09 18:39:32'),
(189, 'App\\Models\\Product', 60, 'd299cb00-5562-4ae9-a829-efd44d55b881', 'product', '130081296_687948848584270_1504909818434088754_o', '130081296_687948848584270_1504909818434088754_o.jpg', 'image/jpeg', 'public', 'public', 111949, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 186, '2020-12-09 18:41:18', '2020-12-09 18:41:19'),
(190, 'App\\Models\\Product', 61, '909570a6-0d1c-4b28-ad1c-2b8d87cae897', 'product', '129736208_687948985250923_6481097891304495808_o', '129736208_687948985250923_6481097891304495808_o.jpg', 'image/jpeg', 'public', 'public', 194523, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 187, '2020-12-09 18:43:22', '2020-12-09 18:43:23'),
(191, 'App\\Models\\Product', 62, 'b8e8f195-1594-4fe4-938f-7a0beb125738', 'product', '129669513_687948941917594_964805940874106349_o', '129669513_687948941917594_964805940874106349_o.jpg', 'image/jpeg', 'public', 'public', 186618, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 188, '2020-12-09 18:46:14', '2020-12-09 18:46:15'),
(192, 'App\\Models\\Product', 63, 'e83c6028-0cc5-476d-8cd4-58f29badf4ea', 'product', '129388347_687331358646019_6662171702859263354_o', '129388347_687331358646019_6662171702859263354_o.jpg', 'image/jpeg', 'public', 'public', 186390, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 189, '2020-12-09 19:02:52', '2020-12-09 19:02:53'),
(193, 'App\\Models\\Product', 63, 'b615f2d1-8d05-4db5-b3ad-bff5c3e8a6ee', 'product', '129605936_687331328646022_4311547730805984458_o', '129605936_687331328646022_4311547730805984458_o.jpg', 'image/jpeg', 'public', 'public', 168854, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 190, '2020-12-09 19:02:53', '2020-12-09 19:02:54'),
(194, 'App\\Models\\Product', 63, 'bf8ef6f1-1e58-4605-9110-8c9de09cfaef', 'product', '129904515_687331315312690_2813487129622170016_o', '129904515_687331315312690_2813487129622170016_o.jpg', 'image/jpeg', 'public', 'public', 162852, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 191, '2020-12-09 19:02:54', '2020-12-09 19:02:55'),
(195, 'App\\User', 19, '89e71819-1469-4087-a866-83c2e764ec36', 'logo', 'logo1', 'logo1.png', 'image/png', 'public', 'public', 28479, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 192, '2020-12-09 19:12:38', '2020-12-09 19:12:38'),
(196, 'App\\Models\\Product', 64, '891a2246-cc53-42fa-807b-ddec3106b253', 'product', '128304225_231893341691620_8062586123564245348_o', '128304225_231893341691620_8062586123564245348_o.jpg', 'image/jpeg', 'public', 'public', 321162, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 193, '2020-12-09 19:16:46', '2020-12-09 19:16:47'),
(197, 'App\\Models\\Product', 64, 'c95ee72c-2e2b-4d5d-80c9-72343ad0846b', 'product', '128659299_231893348358286_499621964418421057_o', '128659299_231893348358286_499621964418421057_o.jpg', 'image/jpeg', 'public', 'public', 296149, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 194, '2020-12-09 19:16:47', '2020-12-09 19:16:49'),
(198, 'App\\Models\\Product', 64, 'ed21df63-30ea-4d18-8568-07cdce6afa3b', 'product', '128704372_231893338358287_6244532177404588081_o', '128704372_231893338358287_6244532177404588081_o.jpg', 'image/jpeg', 'public', 'public', 340450, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 195, '2020-12-09 19:16:49', '2020-12-09 19:16:50'),
(199, 'App\\Models\\Product', 64, 'f13efe4c-aac3-4d65-87d0-a3ebf998de5c', 'product', '128752556_231893345024953_7055957200567813475_o', '128752556_231893345024953_7055957200567813475_o.jpg', 'image/jpeg', 'public', 'public', 288137, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 196, '2020-12-09 19:16:50', '2020-12-09 19:16:52'),
(200, 'App\\Models\\Product', 65, '3be3d6db-7241-4f5c-99bc-c744d4565e56', 'product', '128758915_231893331691621_3496167064552915967_o', '128758915_231893331691621_3496167064552915967_o.jpg', 'image/jpeg', 'public', 'public', 309850, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 197, '2020-12-09 19:19:07', '2020-12-09 19:19:08'),
(201, 'App\\Models\\Product', 65, '54c00b5b-b9f2-4b2a-b053-457076a18426', 'product', '128820031_231893351691619_2144665406764214368_o', '128820031_231893351691619_2144665406764214368_o.jpg', 'image/jpeg', 'public', 'public', 254146, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 198, '2020-12-09 19:19:09', '2020-12-09 19:19:10'),
(202, 'App\\Models\\Product', 65, '4d3316e8-968a-42de-a89d-6e717013bb41', 'product', '128846353_231893328358288_1162755780493679390_o', '128846353_231893328358288_1162755780493679390_o.jpg', 'image/jpeg', 'public', 'public', 283208, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 199, '2020-12-09 19:19:10', '2020-12-09 19:19:11'),
(203, 'App\\Models\\Product', 65, 'c625d3ef-e800-4775-9a07-d157c7c54983', 'product', '128899851_231893335024954_7995454802191932769_o', '128899851_231893335024954_7995454802191932769_o.jpg', 'image/jpeg', 'public', 'public', 308300, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 200, '2020-12-09 19:19:11', '2020-12-09 19:19:12'),
(204, 'App\\Models\\Product', 66, 'f77958e5-ab63-4608-b7e4-febf7c8bb3b8', 'product', '127541579_230234745190813_4358566846129134539_n', '127541579_230234745190813_4358566846129134539_n.jpg', 'image/jpeg', 'public', 'public', 65218, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 201, '2020-12-09 19:34:13', '2020-12-09 19:34:14'),
(205, 'App\\Models\\Product', 66, '4acaefff-c839-4ff0-b56b-b78f1741fcda', 'product', '128166991_230234758524145_4625496535537979758_n', '128166991_230234758524145_4625496535537979758_n.jpg', 'image/jpeg', 'public', 'public', 44148, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 202, '2020-12-09 19:34:14', '2020-12-09 19:34:14'),
(206, 'App\\Models\\Product', 66, '6668b333-f5de-4d0d-9aab-5271c6f1940c', 'product', '128218963_230234748524146_5862322790283716376_n', '128218963_230234748524146_5862322790283716376_n.jpg', 'image/jpeg', 'public', 'public', 61810, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 203, '2020-12-09 19:34:14', '2020-12-09 19:34:14'),
(207, 'App\\Models\\Product', 66, '8309c0b7-8d72-40a1-a31d-af43d48e26fc', 'product', '128295508_230234761857478_6594633451074951384_n', '128295508_230234761857478_6594633451074951384_n.jpg', 'image/jpeg', 'public', 'public', 64028, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 204, '2020-12-09 19:34:14', '2020-12-09 19:34:15'),
(208, 'App\\User', 58, '7177ac0c-26fe-4b24-b2d0-f3ceb06d3394', 'logo', '86970294_3422546244439095_5817259686029688832_o', '86970294_3422546244439095_5817259686029688832_o.jpg', 'image/jpeg', 'public', 'public', 79741, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 205, '2020-12-09 20:04:33', '2020-12-09 20:04:34'),
(209, 'App\\User', 58, '472d83e8-5058-446a-8e4f-b572f575f261', 'cover', '102420765_3726950890665294_7367457939184261898_o', '102420765_3726950890665294_7367457939184261898_o.jpg', 'image/jpeg', 'public', 'public', 78285, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 206, '2020-12-09 20:04:47', '2020-12-09 20:04:48'),
(210, 'App\\Models\\Product', 67, 'bb48cd6f-7fca-40a1-aa4d-5aee9294d9b6', 'product', '1', '1.jpg', 'image/jpeg', 'public', 'public', 521876, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 207, '2020-12-09 20:09:59', '2020-12-09 20:10:01'),
(211, 'App\\Models\\Product', 67, '30e01786-c110-4472-ad37-19f8fbc0cac2', 'product', '2', '2.jpg', 'image/jpeg', 'public', 'public', 301845, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 208, '2020-12-09 20:10:02', '2020-12-09 20:10:04'),
(212, 'App\\Models\\Product', 67, 'e48fbc6d-b0fb-40d8-b59f-567f2c36fd48', 'product', '3', '3.jpg', 'image/jpeg', 'public', 'public', 480106, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 209, '2020-12-09 20:10:04', '2020-12-09 20:10:06'),
(213, 'App\\Models\\Product', 68, 'ca71ef56-a28c-4872-8074-507b24410698', 'product', '127283888_4256500491043662_5756200956971571846_o', '127283888_4256500491043662_5756200956971571846_o.jpg', 'image/jpeg', 'public', 'public', 253300, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 210, '2020-12-09 20:17:50', '2020-12-09 20:17:52'),
(214, 'App\\Models\\Product', 68, '92b18eb0-a34a-40ab-89ab-c2fa88afcd0e', 'product', '128055686_4256500391043672_5244515421743558270_o', '128055686_4256500391043672_5244515421743558270_o.jpg', 'image/jpeg', 'public', 'public', 248528, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 211, '2020-12-09 20:17:52', '2020-12-09 20:17:53'),
(215, 'App\\User', 35, '64feee5b-bc04-4e41-bcb2-324d1a7feb75', 'logo', '106494894_175395184017496_5761334699545274274_o', '106494894_175395184017496_5761334699545274274_o.jpg', 'image/jpeg', 'public', 'public', 297588, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 212, '2020-12-09 21:15:00', '2020-12-09 21:15:01'),
(216, 'App\\User', 35, '1ddc6dea-00e0-48e0-bced-e748a4c53c9d', 'cover', '106990091_177048710518810_5481650728738073749_o', '106990091_177048710518810_5481650728738073749_o.jpg', 'image/jpeg', 'public', 'public', 23207, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 213, '2020-12-09 21:15:10', '2020-12-09 21:15:11'),
(217, 'App\\Models\\Product', 69, '8fde9c2c-a90a-4077-902d-bdfedc746a99', 'product', 'IMG_20201210_191318', 'IMG_20201210_191318.jpg', 'image/jpeg', 'public', 'public', 2303393, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 214, '2020-12-11 02:17:23', '2020-12-11 02:17:26'),
(218, 'App\\Models\\Product', 69, 'cd270eb0-817e-4921-a1c0-494af531aaf3', 'product', 'IMG_20201210_191259', 'IMG_20201210_191259.jpg', 'image/jpeg', 'public', 'public', 2175302, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 215, '2020-12-11 02:17:26', '2020-12-11 02:17:29'),
(219, 'App\\Models\\Product', 69, 'b82644df-23f8-4f1b-90f6-f6df30b97c77', 'product', 'IMG_20201210_191247', 'IMG_20201210_191247.jpg', 'image/jpeg', 'public', 'public', 1760950, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 216, '2020-12-11 02:17:29', '2020-12-11 02:17:31'),
(220, 'App\\User', 67, '9b344359-0cf4-41f6-9410-8a8afb953bd1', 'logo', '119124417_117117500128699_790611997416090485_n', '119124417_117117500128699_790611997416090485_n.jpg', 'image/jpeg', 'public', 'public', 107182, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 217, '2020-12-12 19:15:54', '2020-12-12 19:15:55'),
(221, 'App\\User', 67, 'a50e5fe5-749b-4998-92ff-643f5e8bf246', 'cover', '119269955_117117313462051_13992724696059490_n', '119269955_117117313462051_13992724696059490_n.jpg', 'image/jpeg', 'public', 'public', 39471, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 218, '2020-12-12 19:17:04', '2020-12-12 19:17:05'),
(222, 'App\\User', 20, 'ce8ed7a2-7645-4ece-8084-ffdf28935ab3', 'logo', 'logo', 'logo.png', 'image/png', 'public', 'public', 137929, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 219, '2020-12-12 19:46:16', '2020-12-12 19:46:16'),
(223, 'App\\User', 20, '83e5c273-d2eb-4ad7-ade5-f9438393b25c', 'cover', 'logo', 'logo.png', 'image/png', 'public', 'public', 137929, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 220, '2020-12-12 19:46:27', '2020-12-12 19:46:28'),
(224, 'App\\User', 38, '1a6f850f-b43b-410c-866d-4439c36acba5', 'logo', '117173189_133865081723438_4239694992290521492_n', '117173189_133865081723438_4239694992290521492_n.jpg', 'image/jpeg', 'public', 'public', 79942, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 221, '2020-12-12 20:04:46', '2020-12-12 20:04:47'),
(225, 'App\\User', 38, '2ecc68a6-b685-4556-8704-038ff9ed1d44', 'cover', '119709784_149944606782152_7766086266097692210_o', '119709784_149944606782152_7766086266097692210_o.jpg', 'image/jpeg', 'public', 'public', 264064, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 222, '2020-12-12 20:05:09', '2020-12-12 20:05:10'),
(226, 'App\\Models\\Product', 70, '57ca4073-6531-45e1-b256-4068174a339f', 'product', '123136902_165213398588606_492117740513703818_n', '123136902_165213398588606_492117740513703818_n.jpg', 'image/jpeg', 'public', 'public', 145617, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 223, '2020-12-12 20:24:56', '2020-12-12 20:24:57'),
(227, 'App\\Models\\Product', 70, 'c96da03a-c9ae-4add-8b96-6cb2fd47af9b', 'product', '123568299_165213371921942_756725119448238209_n', '123568299_165213371921942_756725119448238209_n.jpg', 'image/jpeg', 'public', 'public', 129416, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 224, '2020-12-12 20:24:57', '2020-12-12 20:24:58'),
(228, 'App\\User', 28, 'bce00e24-ce0c-4212-90be-f0ebfcba6408', 'cover', '23', '23.jpg', 'image/jpeg', 'public', 'public', 41123, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 225, '2020-12-12 20:37:01', '2020-12-12 20:37:01'),
(229, 'App\\User', 28, 'b1315742-a41f-4761-bf2e-b31b5e98e15c', 'logo', '23', '23.jpg', 'image/jpeg', 'public', 'public', 140715, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 226, '2020-12-12 20:53:23', '2020-12-12 20:53:23'),
(230, 'App\\User', 37, '24610745-0900-402e-b591-fb05464c8f0b', 'logo', 'logo', 'logo.png', 'image/png', 'public', 'public', 624309, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 227, '2020-12-13 18:01:58', '2020-12-13 18:01:58'),
(231, 'App\\User', 37, '5b681615-3f26-4305-9427-2c396c08db9b', 'cover', 'logo', 'logo.png', 'image/png', 'public', 'public', 624309, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 228, '2020-12-13 18:02:12', '2020-12-13 18:02:13'),
(232, 'App\\Models\\Product', 71, '8002c6e9-85a0-4820-85f1-5f76c2130b78', 'product', '120158282_164385435353019_2276930048923657065_n', '120158282_164385435353019_2276930048923657065_n.jpg', 'image/jpeg', 'public', 'public', 102407, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 229, '2020-12-13 18:03:05', '2020-12-13 18:03:06'),
(233, 'App\\Models\\Product', 71, '833c7528-ca59-48ff-96b3-e28b1d117f0c', 'product', '120194082_164385458686350_8153386252205162186_n', '120194082_164385458686350_8153386252205162186_n.jpg', 'image/jpeg', 'public', 'public', 80124, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 230, '2020-12-13 18:03:06', '2020-12-13 18:03:07'),
(234, 'App\\Models\\Product', 71, 'a9338bc3-718c-41c7-bf80-754119cf4a77', 'product', '120223673_164385478686348_2732121256954741176_o', '120223673_164385478686348_2732121256954741176_o.jpg', 'image/jpeg', 'public', 'public', 38496, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 231, '2020-12-13 18:03:07', '2020-12-13 18:03:07'),
(235, 'App\\Models\\Product', 71, '278975a1-8865-4072-8cf1-327dc71f8a8b', 'product', '120260312_164385348686361_8445591852740950340_n', '120260312_164385348686361_8445591852740950340_n.jpg', 'image/jpeg', 'public', 'public', 100424, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 232, '2020-12-13 18:03:07', '2020-12-13 18:03:08'),
(236, 'App\\Models\\Product', 72, 'd3046654-2437-470e-bfc5-4b9b0d3f97fd', 'product', '130707192_3517515368355531_3169668912537039862_o', '130707192_3517515368355531_3169668912537039862_o.jpg', 'image/jpeg', 'public', 'public', 555606, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 233, '2020-12-13 18:26:20', '2020-12-13 18:26:22'),
(237, 'App\\Models\\Product', 73, 'dcddb685-f5ec-4fea-8f18-baf5cd0af0c5', 'product', '130514124_3517515581688843_596185116818006900_o', '130514124_3517515581688843_596185116818006900_o.jpg', 'image/jpeg', 'public', 'public', 798706, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 234, '2020-12-13 18:31:16', '2020-12-13 18:31:18'),
(238, 'App\\Models\\Product', 74, 'bfea9624-c76c-43df-8b5d-4c32d386e2ce', 'product', '130299532_3517515945022140_750024956432736095_o', '130299532_3517515945022140_750024956432736095_o.jpg', 'image/jpeg', 'public', 'public', 832644, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 235, '2020-12-13 18:39:37', '2020-12-13 18:39:39'),
(239, 'App\\Models\\Product', 75, '1d043251-003a-436c-90a5-3d3709ccf862', 'product', '130311126_3517516345022100_7085602283587991355_o', '130311126_3517516345022100_7085602283587991355_o.jpg', 'image/jpeg', 'public', 'public', 149069, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 236, '2020-12-13 18:42:28', '2020-12-13 18:42:29'),
(240, 'App\\Models\\Product', 76, '2750c478-642c-46bd-97cc-8dfb59552ccf', 'product', '129898394_3517516558355412_2058027902714706271_o', '129898394_3517516558355412_2058027902714706271_o.jpg', 'image/jpeg', 'public', 'public', 187450, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 237, '2020-12-13 18:44:07', '2020-12-13 18:44:08'),
(241, 'App\\Models\\Product', 77, '8f4441f4-cb11-4534-85d2-241718f0d688', 'product', '130190154_3517517061688695_2094995972192730298_o', '130190154_3517517061688695_2094995972192730298_o.jpg', 'image/jpeg', 'public', 'public', 705304, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 238, '2020-12-13 18:45:17', '2020-12-13 18:45:19'),
(242, 'App\\Models\\Product', 78, '2693c28b-ead4-45a6-a83c-b9f44744305e', 'product', 'FB_IMG_1607840948662', 'FB_IMG_1607840948662.jpg', 'image/jpeg', 'public', 'public', 92395, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 239, '2020-12-13 20:48:29', '2020-12-13 20:48:30'),
(243, 'App\\Models\\Product', 78, '80532691-9d25-46e5-988b-4817840d79f2', 'product', 'FB_IMG_1607840948662', 'FB_IMG_1607840948662.jpg', 'image/jpeg', 'public', 'public', 92395, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 240, '2020-12-13 20:48:30', '2020-12-13 20:48:31'),
(244, 'App\\Models\\Product', 78, '59bf08e3-6672-42bb-ac6e-766c4bd441ed', 'product', 'FB_IMG_1607840950481', 'FB_IMG_1607840950481.jpg', 'image/jpeg', 'public', 'public', 80333, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 241, '2020-12-13 20:48:31', '2020-12-13 20:48:32'),
(245, 'App\\Models\\Product', 78, '142b1728-5eeb-4323-835c-8fd71fd91828', 'product', 'FB_IMG_1607840952408', 'FB_IMG_1607840952408.jpg', 'image/jpeg', 'public', 'public', 59460, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 242, '2020-12-13 20:48:32', '2020-12-13 20:48:32'),
(246, 'App\\Models\\Product', 79, 'd211f66b-4fee-472f-9df5-f6d1612a6eac', 'product', '130977072_396719875114464_2660196388839729349_n', '130977072_396719875114464_2660196388839729349_n.jpg', 'image/jpeg', 'public', 'public', 24824, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 243, '2020-12-13 21:21:21', '2020-12-13 21:21:22'),
(247, 'App\\Models\\Product', 79, 'daa2d439-be33-4692-96ec-646ed6c41df3', 'product', '131292284_396720005114451_4336933167398991746_n', '131292284_396720005114451_4336933167398991746_n.jpg', 'image/jpeg', 'public', 'public', 39338, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 244, '2020-12-13 21:21:22', '2020-12-13 21:21:23'),
(248, 'App\\Models\\Product', 79, '7806a0df-1267-4096-b2bd-bc1618f05153', 'product', '131324581_396719901781128_8113539675970573400_n', '131324581_396719901781128_8113539675970573400_n.jpg', 'image/jpeg', 'public', 'public', 30479, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 245, '2020-12-13 21:21:23', '2020-12-13 21:21:23'),
(249, 'App\\Models\\Product', 79, '17254df8-f0c5-403c-b5db-63a77ad1e219', 'product', '131331134_396719925114459_9092797455784352718_n', '131331134_396719925114459_9092797455784352718_n.jpg', 'image/jpeg', 'public', 'public', 58091, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 246, '2020-12-13 21:21:23', '2020-12-13 21:21:24'),
(250, 'App\\Models\\Product', 80, '0b3d766a-ec08-4c10-8f8f-10515960d371', 'product', '129981024_396719958447789_1047371754311873570_n', '129981024_396719958447789_1047371754311873570_n.jpg', 'image/jpeg', 'public', 'public', 29637, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 247, '2020-12-13 21:23:59', '2020-12-13 21:24:00'),
(251, 'App\\Models\\Product', 80, '9d440a26-f888-4127-81d2-1c03ad347ffb', 'product', '130245188_396719805114471_582167527221703878_n', '130245188_396719805114471_582167527221703878_n.jpg', 'image/jpeg', 'public', 'public', 32084, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 248, '2020-12-13 21:24:00', '2020-12-13 21:24:01'),
(252, 'App\\Models\\Product', 80, 'c5c1e5eb-842a-4184-b715-8e5cf0199219', 'product', '130288978_396719838447801_4250996355176293971_n', '130288978_396719838447801_4250996355176293971_n.jpg', 'image/jpeg', 'public', 'public', 28865, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 249, '2020-12-13 21:24:01', '2020-12-13 21:24:01'),
(253, 'App\\User', 68, '4b9adbb9-255f-4b45-9b30-6d9953180494', 'logo', '99110167_1538018946401786_6308093200066150400_n', '99110167_1538018946401786_6308093200066150400_n.jpg', 'image/jpeg', 'public', 'public', 76092, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 250, '2020-12-14 19:20:10', '2020-12-14 19:20:10'),
(254, 'App\\User', 68, '1cf20481-f563-47c0-8b1e-773ec359564a', 'cover', '118595489_1637522196451460_733737535906952053_o', '118595489_1637522196451460_733737535906952053_o.jpg', 'image/jpeg', 'public', 'public', 126665, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 251, '2020-12-14 19:23:13', '2020-12-14 19:23:14'),
(255, 'App\\User', 16, 'ae35df3b-85f3-414f-9d1e-6c202cf2b270', 'cover', '46458607_2024777950942006_2616190130477596672_o', '46458607_2024777950942006_2616190130477596672_o.jpg', 'image/jpeg', 'public', 'public', 304026, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 252, '2020-12-14 20:20:09', '2020-12-14 20:20:10'),
(256, 'App\\User', 16, 'f876b7ab-65d7-4286-ae62-e2ff675a7022', 'logo', 'WhatsApp Image 2020-09-30 at 7.31.09 PM', 'WhatsApp-Image-2020-09-30-at-7.31.09-PM.jpeg', 'image/jpeg', 'public', 'public', 83089, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 253, '2020-12-14 20:20:23', '2020-12-14 20:20:23'),
(257, 'App\\User', 29, 'd8926088-feb3-4101-a939-ad7ffb1ac669', 'logo', '108205532_162885528676782_8098293210563245344_n', '108205532_162885528676782_8098293210563245344_n.jpg', 'image/jpeg', 'public', 'public', 66670, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 254, '2020-12-14 20:29:44', '2020-12-14 20:29:44'),
(258, 'App\\User', 29, '5d005829-7af6-4bd1-a548-ab006821d60b', 'cover', '110126427_163169375315064_6431680770371009992_n', '110126427_163169375315064_6431680770371009992_n.jpg', 'image/jpeg', 'public', 'public', 49449, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 255, '2020-12-14 20:30:53', '2020-12-14 20:30:54'),
(259, 'App\\Models\\Product', 81, '99113b80-aae0-4019-b4b2-74f2885a8472', 'product', '125353909_196820065283328_1960419746190928083_o', '125353909_196820065283328_1960419746190928083_o.jpg', 'image/jpeg', 'public', 'public', 157513, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 256, '2020-12-14 20:42:54', '2020-12-14 20:42:55'),
(260, 'App\\Models\\Product', 81, '4996693a-a14e-4023-bbbc-5e006bb18892', 'product', '125375755_196819445283390_111725942809506985_o', '125375755_196819445283390_111725942809506985_o.jpg', 'image/jpeg', 'public', 'public', 144908, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 257, '2020-12-14 20:42:55', '2020-12-14 20:42:56'),
(261, 'App\\Models\\Product', 81, '4bff34be-30be-4855-a5e1-f5fd2cfa7f2d', 'product', '125403945_196820095283325_4323618094536812114_o', '125403945_196820095283325_4323618094536812114_o.jpg', 'image/jpeg', 'public', 'public', 152252, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 258, '2020-12-14 20:42:56', '2020-12-14 20:42:57'),
(262, 'App\\Models\\Product', 81, 'f8f62667-dcbc-4da4-9c3f-467b38c745fc', 'product', '125455234_196819301950071_5185698789115992553_o', '125455234_196819301950071_5185698789115992553_o.jpg', 'image/jpeg', 'public', 'public', 156301, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 259, '2020-12-14 20:42:57', '2020-12-14 20:42:58'),
(263, 'App\\User', 30, '6b95dfe0-7f82-43ca-8478-8c733c251c29', 'logo', '68818735_101678481209040_5469822073306087424_n', '68818735_101678481209040_5469822073306087424_n.jpg', 'image/jpeg', 'public', 'public', 39015, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 260, '2020-12-14 21:12:08', '2020-12-14 21:12:08'),
(264, 'App\\User', 30, 'c5dd9867-e35c-45a4-b76a-b726879ee951', 'cover', '69439020_101680724542149_445760031288197120_n', '69439020_101680724542149_445760031288197120_n.jpg', 'image/jpeg', 'public', 'public', 60599, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 261, '2020-12-14 21:12:39', '2020-12-14 21:12:39'),
(265, 'App\\Models\\Product', 82, 'bd06306a-f465-4ce6-bd1c-1a5d66c0ff6e', 'product', 'FB_IMG_1607933665478', 'FB_IMG_1607933665478.jpg', 'image/jpeg', 'public', 'public', 49913, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 262, '2020-12-14 21:22:09', '2020-12-14 21:22:10'),
(266, 'App\\Models\\Product', 82, '058ade97-8b7f-463c-b3ce-0fa879e901a3', 'product', 'FB_IMG_1607933664500', 'FB_IMG_1607933664500.jpg', 'image/jpeg', 'public', 'public', 39964, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 263, '2020-12-14 21:22:10', '2020-12-14 21:22:10'),
(267, 'App\\User', 46, 'f3c308eb-668a-468d-a3fc-b492cfc56b06', 'logo', 'FB_IMG_1607934443628', 'FB_IMG_1607934443628.jpg', 'image/jpeg', 'public', 'public', 9355, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 264, '2020-12-14 21:34:20', '2020-12-14 21:34:20'),
(268, 'App\\User', 46, '24954480-fd50-4230-83c2-17ca83bb55f1', 'cover', '126115543_156519456192241_4061165571603975454_o', '126115543_156519456192241_4061165571603975454_o.jpg', 'image/jpeg', 'public', 'public', 271116, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 265, '2020-12-14 21:40:43', '2020-12-14 21:40:44');
INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(269, 'App\\Models\\Product', 83, '5d2ff598-7e9f-420b-8fa9-9198f6ee9956', 'product', '123347339_148312737012913_4845612614475146012_n', '123347339_148312737012913_4845612614475146012_n.jpg', 'image/jpeg', 'public', 'public', 54519, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 266, '2020-12-14 21:44:47', '2020-12-14 21:44:48'),
(270, 'App\\Models\\Product', 83, '719f9cd2-2259-4969-81b9-40748d1c0b5d', 'product', '123378140_148312747012912_8074182516813157946_n', '123378140_148312747012912_8074182516813157946_n.jpg', 'image/jpeg', 'public', 'public', 62467, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 267, '2020-12-14 21:44:48', '2020-12-14 21:44:48'),
(271, 'App\\Models\\Product', 83, '5b1fab53-437c-4727-be3d-7bfb3b6b6fd5', 'product', '123396191_148312797012907_2566724190289525756_o', '123396191_148312797012907_2566724190289525756_o.jpg', 'image/jpeg', 'public', 'public', 160250, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 268, '2020-12-14 21:44:48', '2020-12-14 21:44:49'),
(272, 'App\\Models\\Product', 84, '6adbe7b8-0cbe-42b3-a9df-e47d242a4eca', 'product', '122151776_143203050857215_1329348421481815031_o', '122151776_143203050857215_1329348421481815031_o.jpg', 'image/jpeg', 'public', 'public', 136456, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 269, '2020-12-14 21:48:42', '2020-12-14 21:48:43'),
(273, 'App\\Models\\Product', 84, 'ed6ac47f-81dc-40f5-b6e6-c9f5463200fe', 'product', '122165239_143203010857219_2737488284882965274_o', '122165239_143203010857219_2737488284882965274_o.jpg', 'image/jpeg', 'public', 'public', 136620, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 270, '2020-12-14 21:48:43', '2020-12-14 21:48:44'),
(274, 'App\\Models\\Product', 84, '19ed5ffd-ac29-4f28-9d7e-77abf739c6da', 'product', '122434476_143203027523884_2592410392899553749_o', '122434476_143203027523884_2592410392899553749_o.jpg', 'image/jpeg', 'public', 'public', 117486, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 271, '2020-12-14 21:48:44', '2020-12-14 21:48:44'),
(275, 'App\\User', 31, 'a4ad355f-793a-4839-8866-cdc6dc4c0cc9', 'logo', '106186303_129362835485554_6696033384690254446_o', '106186303_129362835485554_6696033384690254446_o.jpg', 'image/jpeg', 'public', 'public', 116884, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 272, '2020-12-15 17:25:40', '2020-12-15 17:25:41'),
(276, 'App\\User', 31, '8692591f-c709-4fa2-928d-311224ab5d0a', 'cover', '117224334_144132984008539_7925785273402378659_o', '117224334_144132984008539_7925785273402378659_o.jpg', 'image/jpeg', 'public', 'public', 923270, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 273, '2020-12-15 17:25:54', '2020-12-15 17:25:56'),
(277, 'App\\Models\\Product', 85, '2a2912c4-43ac-46cd-aa3a-e65806b60c33', 'product', '119051573_155806239507880_2851204403278826781_o', '119051573_155806239507880_2851204403278826781_o.jpg', 'image/jpeg', 'public', 'public', 863300, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 274, '2020-12-15 17:32:37', '2020-12-15 17:32:38'),
(278, 'App\\Models\\Product', 86, '9cfd0741-a4c3-461d-8102-7f62bab2a25c', 'product', '117974909_149760990112405_3816622158180390708_o', '117974909_149760990112405_3816622158180390708_o.jpg', 'image/jpeg', 'public', 'public', 653947, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 275, '2020-12-15 17:34:07', '2020-12-15 17:34:09'),
(279, 'App\\Models\\Product', 87, 'd62972f7-678b-4a64-bc5d-7effc4a58b05', 'product', '117229735_146234783798359_630078634462161272_o', '117229735_146234783798359_630078634462161272_o.jpg', 'image/jpeg', 'public', 'public', 779374, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 276, '2020-12-15 17:35:10', '2020-12-15 17:35:12'),
(280, 'App\\User', 32, '03a27510-30ae-44b3-b86a-1e720210debe', 'logo', '118070483_188130609393448_6143327271696713475_o', '118070483_188130609393448_6143327271696713475_o.jpg', 'image/jpeg', 'public', 'public', 95962, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 277, '2020-12-15 17:45:17', '2020-12-15 17:45:17'),
(281, 'App\\User', 32, '706e2a31-e0de-4668-ba2e-060607950846', 'cover', '118329708_188130666060109_4938308457072249595_o', '118329708_188130666060109_4938308457072249595_o.jpg', 'image/jpeg', 'public', 'public', 84566, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 278, '2020-12-15 17:45:26', '2020-12-15 17:45:26'),
(282, 'App\\Models\\Product', 88, 'f8b78366-ee9a-46ec-9411-5d94275e0bda', 'product', '130828079_228521392021036_3797263262346190666_n', '130828079_228521392021036_3797263262346190666_n.jpg', 'image/jpeg', 'public', 'public', 131688, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 279, '2020-12-15 17:49:27', '2020-12-15 17:49:27'),
(283, 'App\\Models\\Product', 88, '730a2d03-0d97-4fec-939e-678e83b92bda', 'product', '130846366_228521352021040_1491218496168792091_n', '130846366_228521352021040_1491218496168792091_n.jpg', 'image/jpeg', 'public', 'public', 136213, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 280, '2020-12-15 17:49:27', '2020-12-15 17:49:28'),
(284, 'App\\Models\\Product', 88, 'd6f6fcfb-a2db-412a-9929-7d330ce7e888', 'product', '131019237_228521285354380_3147375491151225777_n', '131019237_228521285354380_3147375491151225777_n.jpg', 'image/jpeg', 'public', 'public', 136195, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 281, '2020-12-15 17:49:28', '2020-12-15 17:49:29'),
(285, 'App\\Models\\Product', 88, 'f6e7c929-c570-49e7-a2fe-113b25cf454a', 'product', '131633444_228520748687767_7237217839837023638_n', '131633444_228520748687767_7237217839837023638_n.jpg', 'image/jpeg', 'public', 'public', 145345, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 282, '2020-12-15 17:49:29', '2020-12-15 17:49:30'),
(286, 'App\\Models\\Product', 89, '3c16daa8-cee0-429d-8128-69cee1ea4b48', 'product', '130691580_228521322021043_2140481513233702352_n', '130691580_228521322021043_2140481513233702352_n.jpg', 'image/jpeg', 'public', 'public', 161660, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 283, '2020-12-15 17:51:06', '2020-12-15 17:51:06'),
(287, 'App\\Models\\Product', 89, 'd87ae6d7-137b-440d-9621-94606c8aa4a5', 'product', '130769466_228521448687697_5738693815696765759_n', '130769466_228521448687697_5738693815696765759_n.jpg', 'image/jpeg', 'public', 'public', 121730, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 284, '2020-12-15 17:51:06', '2020-12-15 17:51:07'),
(288, 'App\\Models\\Product', 90, 'b3fdd4bd-a749-4240-aa79-a9b308d7e406', 'product', '129724219_226501238889718_1558723354631173042_n', '129724219_226501238889718_1558723354631173042_n.jpg', 'image/jpeg', 'public', 'public', 93532, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 285, '2020-12-15 18:01:34', '2020-12-15 18:01:34'),
(289, 'App\\Models\\Product', 90, '1c321a0d-b598-4e61-a4ad-73725d33cb41', 'product', '129837139_226501422223033_1781032279430610725_n', '129837139_226501422223033_1781032279430610725_n.jpg', 'image/jpeg', 'public', 'public', 85547, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 286, '2020-12-15 18:01:34', '2020-12-15 18:01:35'),
(290, 'App\\Models\\Product', 90, '5d61e393-86b8-472b-8da1-5d728987255e', 'product', '129958272_226500988889743_5812905786367524317_n', '129958272_226500988889743_5812905786367524317_n.jpg', 'image/jpeg', 'public', 'public', 59759, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 287, '2020-12-15 18:01:35', '2020-12-15 18:01:35'),
(291, 'App\\Models\\Product', 90, '552edacd-6f31-4c78-80fd-1cf06ca2619a', 'product', '130101077_226501798889662_1403823722608095228_n', '130101077_226501798889662_1403823722608095228_n.jpg', 'image/jpeg', 'public', 'public', 65907, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 288, '2020-12-15 18:01:35', '2020-12-15 18:01:36'),
(292, 'App\\Models\\Product', 91, 'b322b423-cdb5-4717-b2f7-56f79db97822', 'product', '130221902_226502178889624_29180346560441591_n', '130221902_226502178889624_29180346560441591_n.jpg', 'image/jpeg', 'public', 'public', 60319, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 289, '2020-12-15 18:06:56', '2020-12-15 18:06:56'),
(293, 'App\\Models\\Product', 91, '14c04e43-7e48-4542-aa45-c4880b78de12', 'product', '130246200_226501462223029_8242725664761505388_n', '130246200_226501462223029_8242725664761505388_n.jpg', 'image/jpeg', 'public', 'public', 61614, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 290, '2020-12-15 18:06:56', '2020-12-15 18:06:57'),
(294, 'App\\Models\\Product', 91, '640a652c-9b56-42e2-a423-5b07e8d48882', 'product', '130248652_226502018889640_215590674110332237_n', '130248652_226502018889640_215590674110332237_n.jpg', 'image/jpeg', 'public', 'public', 73514, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 291, '2020-12-15 18:06:57', '2020-12-15 18:06:57'),
(295, 'App\\Models\\Product', 91, '4651284c-87cf-4ddb-aacc-fbf03469d09c', 'product', '130290392_226502145556294_8468595630525853533_n', '130290392_226502145556294_8468595630525853533_n.jpg', 'image/jpeg', 'public', 'public', 65311, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 292, '2020-12-15 18:06:57', '2020-12-15 18:06:57'),
(296, 'App\\Models\\Product', 92, 'c1131f14-b39d-4cf1-bf44-ddbddf669f85', 'product', '130534106_226501348889707_6462904108650649998_n', '130534106_226501348889707_6462904108650649998_n.jpg', 'image/jpeg', 'public', 'public', 86472, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 293, '2020-12-15 18:09:11', '2020-12-15 18:09:11'),
(297, 'App\\Models\\Product', 92, 'f5d3d1d5-20e3-4502-9833-650403437800', 'product', '130604448_226501148889727_437029915333366871_n', '130604448_226501148889727_437029915333366871_n.jpg', 'image/jpeg', 'public', 'public', 46968, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 294, '2020-12-15 18:09:11', '2020-12-15 18:09:12'),
(298, 'App\\Models\\Product', 92, 'b643423c-6897-4f8e-9a94-b568c0ef346f', 'product', '130757180_226501862222989_5958196150317012689_n', '130757180_226501862222989_5958196150317012689_n.jpg', 'image/jpeg', 'public', 'public', 88140, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 295, '2020-12-15 18:09:12', '2020-12-15 18:09:12'),
(299, 'App\\Models\\Product', 92, '2b92e011-82c0-463f-a1f9-0a8d5dc9b137', 'product', '130757950_226501075556401_651786010798934753_n', '130757950_226501075556401_651786010798934753_n.jpg', 'image/jpeg', 'public', 'public', 83827, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 296, '2020-12-15 18:09:12', '2020-12-15 18:09:13'),
(300, 'App\\Models\\Product', 93, 'bb399dfc-21f3-4945-9677-6fb2c3323f7f', 'product', '130855687_226501015556407_2141347690327196090_n', '130855687_226501015556407_2141347690327196090_n.jpg', 'image/jpeg', 'public', 'public', 53467, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 297, '2020-12-15 18:12:21', '2020-12-15 18:12:22'),
(301, 'App\\Models\\Product', 93, '631f7143-ba05-4b03-9220-6a6444650bc4', 'product', '131053003_226501595556349_5961552409193102512_n', '131053003_226501595556349_5961552409193102512_n.jpg', 'image/jpeg', 'public', 'public', 66786, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 298, '2020-12-15 18:12:22', '2020-12-15 18:12:22'),
(302, 'App\\Models\\Product', 93, '7ea89157-8911-46b1-a8d0-c49040b81865', 'product', '131101772_226501948889647_3520212556653101712_n', '131101772_226501948889647_3520212556653101712_n.jpg', 'image/jpeg', 'public', 'public', 54417, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 299, '2020-12-15 18:12:22', '2020-12-15 18:12:23'),
(303, 'App\\Models\\Product', 93, '74f2367f-5511-43d3-92a3-2e2ce00674a4', 'product', '131132464_226502105556298_1976145013419484704_n', '131132464_226502105556298_1976145013419484704_n.jpg', 'image/jpeg', 'public', 'public', 68761, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 300, '2020-12-15 18:12:23', '2020-12-15 18:12:23'),
(304, 'App\\User', 33, '7d2e464f-185c-4591-937b-bb6afd219aaf', 'logo', '101326083_100200501731961_7526238945613447168_n', '101326083_100200501731961_7526238945613447168_n.jpg', 'image/jpeg', 'public', 'public', 68799, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 301, '2020-12-15 18:30:19', '2020-12-15 18:30:20'),
(305, 'App\\User', 33, '7cd10443-df2e-4639-a02f-e16c382babd3', 'cover', '83895040_100202965065048_7197094552086249472_o', '83895040_100202965065048_7197094552086249472_o.jpg', 'image/jpeg', 'public', 'public', 45845, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 302, '2020-12-15 18:30:29', '2020-12-15 18:30:29'),
(306, 'App\\Models\\Product', 94, '8c67854f-46e4-444f-bc11-538880b4acc2', 'product', '125225722_194563088962368_5076352109323701833_o', '125225722_194563088962368_5076352109323701833_o.jpg', 'image/jpeg', 'public', 'public', 874303, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 303, '2020-12-15 18:32:28', '2020-12-15 18:32:30'),
(308, 'App\\Models\\Product', 96, 'f8c3a230-9330-4286-b768-a2689fb76125', 'product', '125796927_194563132295697_6179609204470517392_o', '125796927_194563132295697_6179609204470517392_o.jpg', 'image/jpeg', 'public', 'public', 610950, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 305, '2020-12-15 18:42:23', '2020-12-15 18:42:25'),
(309, 'App\\Models\\Product', 95, '83b2ec22-6530-4979-b388-7e67b813133a', 'product', '125888108_194563188962358_8001529881856874845_o', '125888108_194563188962358_8001529881856874845_o.jpg', 'image/jpeg', 'public', 'public', 918511, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 306, '2020-12-15 18:43:22', '2020-12-15 18:43:23'),
(310, 'App\\Models\\Product', 97, '780a89de-fe56-4ee8-9868-8f7bd143ce2c', 'product', '125765359_194563218962355_2887292975161111699_o', '125765359_194563218962355_2887292975161111699_o.jpg', 'image/jpeg', 'public', 'public', 825689, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 307, '2020-12-15 18:44:53', '2020-12-15 18:44:55'),
(311, 'App\\Models\\Product', 98, 'd7a983f4-42ea-4ab3-b993-83853a7d58f5', 'product', '125283947_194563248962352_3953471206246732239_o', '125283947_194563248962352_3953471206246732239_o.jpg', 'image/jpeg', 'public', 'public', 717591, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 308, '2020-12-15 18:45:53', '2020-12-15 18:45:55'),
(312, 'App\\Models\\Product', 99, '1c6452e5-4d29-4673-affb-a9840806196d', 'product', '125223895_194563265629017_4561970210180591669_o', '125223895_194563265629017_4561970210180591669_o.jpg', 'image/jpeg', 'public', 'public', 677372, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 309, '2020-12-15 18:46:53', '2020-12-15 18:46:55'),
(313, 'App\\User', 34, '94ffa972-dc2e-40f3-9156-cc341eeb42f7', 'logo', '106726013_293646905323886_556486748740746372_o', '106726013_293646905323886_556486748740746372_o.jpg', 'image/jpeg', 'public', 'public', 535571, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 310, '2020-12-15 19:22:47', '2020-12-15 19:22:48'),
(314, 'App\\User', 34, 'e30d7ea4-5904-4361-aa7d-37c8a74be7bd', 'cover', '67742587_107041610651084_1476826093867499520_n', '67742587_107041610651084_1476826093867499520_n.jpg', 'image/jpeg', 'public', 'public', 86229, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 311, '2020-12-15 19:23:02', '2020-12-15 19:23:03'),
(315, 'App\\Models\\Product', 100, 'f0813827-51e0-42ff-8817-2549cb52418a', 'product', '106726013_293646905323886_556486748740746372_o', '106726013_293646905323886_556486748740746372_o.jpg', 'image/jpeg', 'public', 'public', 535571, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 312, '2020-12-15 19:25:51', '2020-12-15 19:25:53'),
(316, 'App\\Models\\Product', 100, '8c22af66-1b68-4f9b-b957-7b22a0cccf40', 'product', '106740178_293646991990544_7726935026751746870_o', '106740178_293646991990544_7726935026751746870_o.jpg', 'image/jpeg', 'public', 'public', 412649, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 313, '2020-12-15 19:25:53', '2020-12-15 19:25:55'),
(317, 'App\\Models\\Product', 101, 'ff957e6b-aa67-4065-8f9e-dc5e7088d90e', 'product', '106506864_293643861990857_5091024365364638533_o', '106506864_293643861990857_5091024365364638533_o.jpg', 'image/jpeg', 'public', 'public', 419636, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 314, '2020-12-15 19:31:48', '2020-12-15 19:31:50'),
(318, 'App\\Models\\Product', 101, 'f6e80477-5d3f-43b7-a2df-6f75a4091602', 'product', '107420487_293646701990573_230519969381787170_o', '107420487_293646701990573_230519969381787170_o.jpg', 'image/jpeg', 'public', 'public', 383338, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 315, '2020-12-15 19:31:50', '2020-12-15 19:31:52'),
(319, 'App\\Models\\Product', 101, '72574da7-c3de-4c07-b0e6-a3f0e60d6270', 'product', '107495428_293646478657262_3589218793681262678_o', '107495428_293646478657262_3589218793681262678_o.jpg', 'image/jpeg', 'public', 'public', 488553, '[]', '{\"generated_conversions\": {\"card\": true, \"thumb\": true, \"slider\": true, \"details\": true, \"timeline\": true}}', '[]', 316, '2020-12-15 19:31:52', '2020-12-15 19:31:54'),
(320, 'App\\User', 36, '9a304cd0-52d0-4658-a473-d3dae0549e7f', 'logo', 'logo', 'logo.png', 'image/png', 'public', 'public', 108512, '[]', '{\"generated_conversions\": {\"nav\": true, \"thumb\": true}}', '[]', 317, '2020-12-15 19:39:45', '2020-12-15 19:39:45'),
(321, 'App\\User', 36, 'c784037f-93c5-4a2c-ac15-3c00edebc11a', 'cover', 'logo', 'logo.png', 'image/png', 'public', 'public', 108512, '[]', '{\"generated_conversions\": {\"card\": true, \"profile\": true}}', '[]', 318, '2020-12-15 19:42:17', '2020-12-15 19:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_04_16_035325_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2020_04_19_082503_create_companies_table', 1),
(6, '2020_04_19_085755_create_shops_table', 1),
(7, '2020_04_19_090107_create_resellers_table', 1),
(8, '2020_04_19_090127_create_customers_table', 1),
(9, '2020_04_19_090143_create_riders_table', 1),
(10, '2020_04_19_090609_create_product_types_table', 1),
(11, '2020_04_19_090742_create_product_categories_table', 1),
(12, '2020_04_19_090840_create_business_packages_table', 1),
(13, '2020_04_19_090933_create_package_logs_table', 1),
(14, '2020_04_19_091155_create_advertise_types_table', 1),
(15, '2020_04_19_091800_create_advertise_packages_table', 1),
(16, '2020_04_19_092901_create_advertise_logs_table', 1),
(17, '2020_04_19_092953_create_products_table', 1),
(18, '2020_04_19_093116_create_reseller_products_table', 1),
(19, '2020_04_19_093151_create_product_ratings_table', 1),
(20, '2020_04_19_093217_create_product_reviews_table', 1),
(21, '2020_04_19_093242_create_offers_table', 1),
(22, '2020_04_19_135302_create_coupons_table', 1),
(23, '2020_04_19_135326_create_orders_table', 1),
(24, '2020_04_19_135347_create_order_items_table', 1),
(25, '2020_04_19_135416_create_used_coupons_table', 1),
(26, '2020_04_19_135454_create_payment_reports_table', 1),
(27, '2020_04_19_135542_create_payment_order_lists_table', 1),
(28, '2020_04_19_141124_create_rider_delivery_reports_table', 1),
(29, '2020_04_19_141416_create_rider_delivery_order_lists_table', 1),
(30, '2020_08_27_165235_create_sizes_table', 1),
(31, '2020_08_27_165255_create_colors_table', 1),
(32, '2020_08_27_165304_create_tags_table', 1),
(33, '2020_08_28_041836_create_media_table', 1),
(34, '2020_08_30_183915_create_codes_table', 1),
(35, '2020_10_11_170128_create_product_comments_table', 1),
(36, '2020_10_11_170331_create_product_replies_table', 1),
(37, '2020_10_28_150322_create_notifications_table', 1),
(38, '2020_10_29_193009_advertise_package_advertise_type', 1),
(39, '2020_10_30_114845_create_admin_advertises_table', 1),
(40, '2020_10_30_203832_create_advertise_log_details_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('0060c61b-c2bd-4d09-9325-9924dea4314d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkRuZ2V3bm1tVHhZbzZtRjU3S1lncUE9PSIsInZhbHVlIjoicDRhU2V5Tnlpemo2S0RHbjNmSGpBdz09IiwibWFjIjoiYWZhOGFmYTBmNTgyYTFkMGUyYmIwY2NhNDBmNTdkNTE4ZDMxZDBlZjM0YTFhM2NiMDFhMmJmMDI4N2IxYjg5OSJ9\"}', NULL, '2020-12-03 20:27:59', '2020-12-03 20:27:59'),
('00980d04-4edf-462f-93ab-8ebbbbfa588c', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InZIUGtTb09vZVpXNWRPNDVWMzdPWnc9PSIsInZhbHVlIjoiMlJpTHlSUmw4L21KbXgyY1dGaUVKdz09IiwibWFjIjoiYWNjNDUzNDYyODlkOGQ4YjYyNjgyMjg3YzQ2MTY5YTlkNmVkNWY0M2YyMjY4NDU3NmJhYmJmNDU0ZDc1NjRhYyJ9\"}', '2020-12-03 02:00:44', '2020-12-02 22:19:58', '2020-12-03 02:00:44'),
('00abe215-a27c-4fd7-b24d-9f0d66e9f842', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InhmcitDT0NmcEozRDJBQW9lRHhsZEE9PSIsInZhbHVlIjoic0lQblF6ZUkyTW90VFZwQmEyaGY2Zz09IiwibWFjIjoiYzNmZjhhYjFmZjExMWFjNDdkMmUzNTU2YTk3ZTA5NzJjYTYxMzZkMjhhOGE4NGYwYTU0ODgwMDhlMzJjNjhiNCJ9\"}', NULL, '2020-12-03 19:43:49', '2020-12-03 19:43:49'),
('013b0f3f-8d7c-43d9-90df-5a75855bbac4', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Jahan\'s Collections<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImxjaXJNMmZra2ZPbGltUEloejB3MFE9PSIsInZhbHVlIjoiOHNuVzZ2Z04zSTV2cnp1R2E5NVhaUT09IiwibWFjIjoiNTk2Y2JjOGMyNDE5MjNiNzAyYTdjMjBjZmEyNzhiYWE1MWMwNzNlZjBkZmZjZDY3YjZiNWEyOWNkZDVhODI1NiJ9\"}', NULL, '2020-12-15 17:38:05', '2020-12-15 17:38:05'),
('018611c2-03bb-4757-8831-69ee7c32f9be', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IllWYjVJUWxUMFluZlNFUHl4dGRhOEE9PSIsInZhbHVlIjoiTTVMNVMybHJNYWhUc3RLZUVlZmFJdz09IiwibWFjIjoiNThiZTYxNjFjMGIzOWYzODNlZWQxZjZhNjdlYjk5YjQwNWEzMzMwODlkYjRiYmU2MWRjOTk5OWYwOGFlMzAyNiJ9\"}', NULL, '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
('01de7d50-7eb8-4fc1-8a7d-bf376c5332b2', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Monforing \\u09ae\\u09a8\\u09ab\\u09dc\\u09bf\\u0982<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/www.shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InNoL0w3cDdNcjRaT2ZwVVRaOVJZc1E9PSIsInZhbHVlIjoiZVM2eGpWMWhRdWcwRzRSRzIwVThkZz09IiwibWFjIjoiZTBhNzlkNTlkMDFiYWE4MWFmY2E3YTk2OGFmYjI4NTlhNWVkMDA5MDJjZDcxNzE0NTExZWM5NGE2NWJhZDcwOCJ9\"}', '2020-12-11 21:22:01', '2020-12-11 02:17:31', '2020-12-11 21:22:01'),
('025c2bd0-0d10-4330-8c3e-b9c323bb1dd5', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Sultan\\u2019s Shop<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkdvZWxWbUk4cDBhSHRNS1cyRE11S1E9PSIsInZhbHVlIjoiZTl0anBPUlFZRkk0RTUxSmRXTTI1UT09IiwibWFjIjoiNTg4YWY3NTcyODliNmM5ODUzYWNhZjM2MWZlNjc1MjM5MjU2N2IzM2Y1ZWU2ZWE3YjRmOWQzZGVjM2JiZjkyMiJ9\"}', '2020-12-01 19:01:33', '2020-12-01 19:01:26', '2020-12-01 19:01:33'),
('02936f21-fef6-4e79-9fb9-376b66f9e8da', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImlxcmdYdm1WVGdyaWlESGRvSzlnUnc9PSIsInZhbHVlIjoiUlRGWFJpMFNBZ2RlQ0tib21QRzRlZz09IiwibWFjIjoiZmE3N2Y1MDk5ODRkNDg5OWVlMjI0MTkzMjE4NTEyMDQwNzU2YjI4MmE4YmY2ZTQxMzk4NWNkMTVlMzk5MTY2NiJ9\"}', NULL, '2020-12-13 21:21:24', '2020-12-13 21:21:24'),
('04151436-f5ad-4a37-a7c7-5f007e466910', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im52QVRLNGVTYlI3VmsraVlueTRQTnc9PSIsInZhbHVlIjoiSzhaYnhLcnc4QmwzUVh5NTcrTmY5QT09IiwibWFjIjoiZTZlNThiOTAxOTVmOTVjYTUxNzdkMzQ1YzBlNmM5NGExODBmMjhkMmMwZDZjY2JkMmNmZGFiYTMxNzJlZDE3OCJ9\"}', NULL, '2020-12-03 20:27:59', '2020-12-03 20:27:59'),
('078b64fd-b2e3-4fb1-9b0a-06b952867ffe', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nisa\'s Wardrobe<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImZjWWtaeWRQbEFqOEFoc21CY2NuUEE9PSIsInZhbHVlIjoiMHNEUVh2TytWSnp5S1BOSmRMb21Ddz09IiwibWFjIjoiN2M0Nzg5OTk3MzNiNTAyNGEyZDI4OTcxZTM4YmE1NWVlZmFiZDgwMTA0NzA1OTdiYzRhNWU0MjljZTM1OGZjZiJ9\"}', NULL, '2020-12-14 20:42:58', '2020-12-14 20:42:58'),
('085755e9-9533-4cf7-a521-25e048608cde', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjdXaEtDMnRIZDNhL2dtYW9pSlRad2c9PSIsInZhbHVlIjoiMG43dzJvcE9CeWRTZzVwZHBVL1NQdz09IiwibWFjIjoiZjM4N2ExNmRiMTkzODRlYWNmY2ZiMDEwMmIwZTYwN2JjZDI3YjQ1NTc4NmU1NzEyODFlZWE3MmMyYjQ4NWU1ZiJ9\"}', NULL, '2020-12-03 18:09:19', '2020-12-03 18:09:19'),
('08588818-721c-4bce-8805-d018bd1e0412', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ild4UlN6UVFhb3R4QStYUGNGUXNZZFE9PSIsInZhbHVlIjoiTm9uVmd4RHBtZHY2VnFMZTJiRklPUT09IiwibWFjIjoiMmViOWVkYTkwYThiNmU1MDIzYzNkYmYwYTI2NzZjZTFmYzkzZDE4YTdkNTRjMTc4NzhkYzg0ODY1N2ZlNmJiNCJ9\"}', NULL, '2020-12-02 21:31:28', '2020-12-02 21:31:28'),
('096f0ce9-4e3b-4b47-abbb-d0412b94905f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Inc3TGZZQzExSkxUcWtXSytNK01Mb1E9PSIsInZhbHVlIjoiU2JUUGJWTmRtUlZvL0xBVTdLa3F0Zz09IiwibWFjIjoiNmVjZTU5ZDk0ZDczNWMxM2QxNmE2MDQ0NWI5ZDMyZThmMWI0MDMzZWY5YWEyNzYyNDc2M2E0OGZmZThjMGQ0YiJ9\"}', NULL, '2020-12-06 22:00:40', '2020-12-06 22:00:40'),
('09a9db94-5ebc-456e-8615-ae6bfdb4e763', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nisa\'s Wardrobe<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkhoYWtHOFdnZm5tZWhWUVhKYnd2OWc9PSIsInZhbHVlIjoiTmdIZ2NTVFIxaytPZmRDQ2JKUC9PZz09IiwibWFjIjoiMzI1ODM5ODE1NjJiNzc2ZjJkNzAxZGRlNTNmYTY5Mjc1YWMzYzFmOTE4Y2M0YzcwNzhkZjliODE4N2ExNDZhZCJ9\"}', NULL, '2020-12-14 20:42:58', '2020-12-14 20:42:58'),
('0a59bdb7-04a2-4bf4-869c-ebaf6351f1cc', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Suronjona Package<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkpXY3dORWJ5TzhzcGl1WitzeG1ITXc9PSIsInZhbHVlIjoiUXR5dFF3eG9NZzg5MkJ3TFM3MTFMZz09IiwibWFjIjoiN2U5YzE2YjYwZTY2ZjgxYjA2YWEyM2Q5OTA4ODkxMDA2ZjAxM2UwZTM2MjVmNDdhNzUxNzM2OTg5MmZhMTM3YSJ9\"}', NULL, '2020-12-09 19:52:56', '2020-12-09 19:52:56'),
('0addfa70-85a6-4e3a-bab2-06f839c8f64e', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Nisa\'s Wardrobe<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkRsMm5tbXBUN1UybHphNHgyR3NENkE9PSIsInZhbHVlIjoiVHJ4RE5TcElLY2Ntb1J1aTkvdjRiZz09IiwibWFjIjoiY2Y3YTI4NTIzMDdmMWI2YWJmYzdjMjBhNzFjYWZmN2IzMGJjYjUxYjU3NjhhMjljOGVlYWM1YTA5NTQzODE4ZSJ9\"}', NULL, '2020-12-14 20:25:47', '2020-12-14 20:25:47'),
('0aeaaf62-3d66-4566-b270-ba4c7ca95bbe', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkhaS1ZjV3pBb0tuNDF1K3B0UFRqMnc9PSIsInZhbHVlIjoiL1FsTDF4TkZIZEEwWnhNcXVvSklpdz09IiwibWFjIjoiNmJkZDUxY2FhODZmMmFkNDVhM2QxN2RiYTkyNTE4ZmU1NmJhNWU5YTA4NWFhNTI0M2Q2YTQ5ZmQ3NTk1MDEyZCJ9\"}', NULL, '2020-12-14 21:44:49', '2020-12-14 21:44:49'),
('0c3bf823-140f-4c68-9ccc-8c540ae72123', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Women\'s Craft<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InRrZ0RFcXVhZGFlaU5mTE1VdXc5NVE9PSIsInZhbHVlIjoiNURLQm0zdXhHREFkOGdGVUgrQldqZz09IiwibWFjIjoiNDExYWFmMjQ1ZDBjMDU4ZTNmZGYzMWU3ZTg2MDJkY2ZkYTM4NzJiOTg5MGVlOTgzMWYwNmZkNzllNTYxMzFkOCJ9\"}', '2020-12-11 21:22:10', '2020-12-09 21:12:32', '2020-12-11 21:22:10'),
('0cbb34fe-98a8-44e5-a16b-43363df6fbbf', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InM3UXljOGpycDRnTkVRR1IvamtVbFE9PSIsInZhbHVlIjoiRUhpaW1yTFo5TjR5Ti9tczAzRjV2dz09IiwibWFjIjoiZmVmYWFhMTMyZDVmN2U5NzYxODc5OGI5ODU5OTg2ZDVjMDFlN2IxY2EzODcwMDI3ZjFmZDBjNjdmMjJjYjc2NSJ9\"}', NULL, '2020-12-01 21:41:04', '2020-12-01 21:41:04'),
('0d08bd17-eedc-49c2-986c-e76cde86aa73', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkNNcjYrYytLWkFUa2JPTUliUWNnRnc9PSIsInZhbHVlIjoiUHZ3N2QrRmt1OXhOa3FvY1lUaWJsUT09IiwibWFjIjoiMDRhZDMwMTU2NDFkNWExMjVjY2ZjNmM1NGUxYzhiMWNhMTdkZDA1ZjNlNTA2MzU0Y2QwMTc5ZGU1ZjgzNzMyZiJ9\"}', NULL, '2020-12-13 18:39:39', '2020-12-13 18:39:39'),
('0d659dbf-328c-41b6-93b2-fd213462905a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ii81SEQrSHJLTHhpbG1mcnJTN3duY1E9PSIsInZhbHVlIjoiT3psdGVKZUVFOXB0Ulh1QmtBMUtLUT09IiwibWFjIjoiOTg3MmY3NWRjYjY2MjA0NjkzNDcwZjUzM2YxNTIwNjc1ZjI4ODJhYTFlN2FkMzYwYTEyZGFmY2M1MTFmMDQyMiJ9\"}', NULL, '2020-12-09 19:19:12', '2020-12-09 19:19:12'),
('0d84ff99-aea0-496d-87a1-80e0ee2fc0c2', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjliRElXd0UxR3FrTkZoYmFrTWg0dlE9PSIsInZhbHVlIjoiUXk2OVdNaUJPbmpMSEMzTWF1aGZxdz09IiwibWFjIjoiYjU0OTU0MGVjMWExNWNmNjM4ZDk3ZDMyMTAwYTNiOTQ0NDBlYTgzNTI1MTc1ZWUwMzAxMGM5OThkMTE1NDcwZSJ9\"}', NULL, '2020-12-13 20:48:32', '2020-12-13 20:48:32'),
('0fc4125a-05d4-4766-916a-50ca12ce1d27', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjZTSXpFdHRON3ZUcS9OcWp3OWFWQlE9PSIsInZhbHVlIjoicmgrOVBYNHZJc09oTjc3QTVuc2ppUT09IiwibWFjIjoiYzVhYWRiNmI0OWI1MGIxNjBkNjc0M2UxNDQwNmY0YWQ3MmZlYWIwMDhhNDBjNDM3YmNkZDVkODljYjUwYmZiYSJ9\"}', NULL, '2020-12-14 21:44:49', '2020-12-14 21:44:49'),
('10d054be-5db8-4aa5-9d5d-416d17bd1d4d', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Moushumi\'s Loft<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Inl0L1MvdlBoaWxsd0c2Sm44aDhOMlE9PSIsInZhbHVlIjoiUVZsZ1pJQWNUVXoxZUlmZndEVGE3dz09IiwibWFjIjoiNzc4YzRlNzJmYWI0ZDBmOWMzNWFkN2VhMmQ3OWMzMmYwZDAyODNlZTcwZmNkZjIzZWRiYjI2ZTViZWZkOWI1MSJ9\"}', NULL, '2020-12-06 19:11:19', '2020-12-06 19:11:19'),
('117a0baf-7680-4069-8930-51b4a9902a2b', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099f\\u09bf\\u09df\\u09be\\u09b0 \\u09b8\\u0996\\u09c7\\u09b0 \\u09a6\\u09cb\\u0995\\u09be\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlhSVTEyTS9OQ2V0TCswY2d5RVQwbkE9PSIsInZhbHVlIjoiTGtwU3RJa1hXbFFRcE5oUVRhVG5xQT09IiwibWFjIjoiYWYyMWUzNjFmMTU2YjhmMGYzNGM5OTk1NzQ5OTAzNzQwMDA3Y2U0ZmQ0OTYzZWM0NmQ2YzdlZWY0YTJmMzY3NSJ9\"}', NULL, '2020-12-13 17:58:57', '2020-12-13 17:58:57'),
('11f24415-e345-4705-900b-8f94de2b90bc', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InVvL0hSRXdDMTFGRnZwTkRuc3ZGb0E9PSIsInZhbHVlIjoiaXBISU5TRkFjcm5XczJxRktBSGh6QT09IiwibWFjIjoiYjgwNWIxZGU3Y2M2Y2E2ZmRhMmMyMjFkMzc0YTMwYmUyMzNjMTA3OGNhOGIyZGE0MDhkZTZmNGMzMzcwNWQ2MyJ9\"}', NULL, '2020-12-13 20:48:32', '2020-12-13 20:48:32'),
('127b85a3-a290-45ae-8bd2-dded282b4439', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjNya0V5a3d6VU1jbnNZY09FLysxR0E9PSIsInZhbHVlIjoiVVBoVldLWTZlTWxsNGl5L2g5SVMyUT09IiwibWFjIjoiM2NjNjIzYWM5N2VmOWQ3ZDA5MGMxNGQxNDQ4YTMxMDgyYjIwYTAyOGJmOTlmNDgxY2U0YTBjNzVlZTExZTBmZiJ9\"}', NULL, '2020-12-02 22:12:09', '2020-12-02 22:12:09'),
('1641f910-811d-4c33-8306-862e437421e1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZGTi9VY1dXanZTNGpkdFFxMUJTTkE9PSIsInZhbHVlIjoiSXF0eW0xMnBGUkNWcHU1QnQrT2twUT09IiwibWFjIjoiM2RjNGYzNTI1Yjc2M2ZmYzQyOWZlZGZkMzUyZmZjN2Q2OTI1YmU1NGZmOGI5MGIzNTk3OTQwNmIwMDliZDI2MSJ9\"}', NULL, '2020-12-15 18:46:55', '2020-12-15 18:46:55'),
('184ddacc-52c8-40f2-ae7f-c41d527dc74e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkJ3SUhEenJUaFF2ZEhWYUpra0VMV3c9PSIsInZhbHVlIjoiK2F4bDhNQXFPL0lKeU15MCt6ZDhTdz09IiwibWFjIjoiOTlhMDE4MWFkZTk0NGQyOGFmYjQ4YjU1ZWE1YWM1YTQxYzM5OWU4ZTM5NTdmODI5NjZjNjJkZDg4ZTY5NThiMSJ9\"}', NULL, '2020-12-15 17:34:09', '2020-12-15 17:34:09'),
('19b1123e-53e7-4057-935f-f2fed8d461b8', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImYzdW9WZFNwTjBiV09GZjlpbVpIZlE9PSIsInZhbHVlIjoiN09VcUs5ZjVydjUwaS9kazN1Y25oQT09IiwibWFjIjoiMDgxZWZiZDZhNmJiNWU3OWE3OWFkYmM0Njg0NGY2ZDA5YzZkODUwZmFhYThkMWMwMzVlYzk1OTY5MWY0M2JkYSJ9\"}', NULL, '2020-12-13 18:44:08', '2020-12-13 18:44:08'),
('1a0bbe77-a20f-49aa-bd84-8bf619ebbbc5', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InhtczdRditsZFd2RXMzaU5ib21pZVE9PSIsInZhbHVlIjoiNFdWVW5uZnprWkU1OVB2OW1qQWVJUT09IiwibWFjIjoiZmZlZjc0MzA5ZTY5NDU0YmExMGIxMzIxZGI2MWRmOWI2NTIyNGU1NjVhNDMzMjY0NGVhOGZiMDZlYWYxNWU5MiJ9\"}', NULL, '2020-12-03 17:45:27', '2020-12-03 17:45:27'),
('1b76f907-b80b-4726-83ec-112e357b3cb4', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InBXTitjWWl6cGRSdjkvSmlkTnIrUVE9PSIsInZhbHVlIjoieFdYQ0dNZCs5Q3l4UW5PaFJKTkRGdz09IiwibWFjIjoiMzA4NTllYmFjNTExOGIyNGVkMzYxNjVhNzdiZTczNWZjODU1ZTM2MDNiOWE4ZTYzNjhlODM2OWE1ZWIwMjlkNCJ9\"}', NULL, '2020-12-15 18:09:13', '2020-12-15 18:09:13'),
('1ba59740-c6d2-4683-90f8-2d964fe08c57', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImVYdGhreG9vditydVFMYWYwSlNkZFE9PSIsInZhbHVlIjoiM2FPU0lJbVczRzNmenZWSklrRXJLUT09IiwibWFjIjoiNWE4NjVhOTIxYzRlYWVmNmY3MmM2Y2JlNDNmZTM1NTFjNzcxNDE0Y2NmZjAxNDgxNjAyYWVmNDlmYTNjOWUxOSJ9\"}', NULL, '2020-12-13 21:24:01', '2020-12-13 21:24:01'),
('1c4dcfad-1b94-4011-b439-9e0fdcce431b', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InlEOGM5NEYzT1lKalpzcVN2b3Rnb3c9PSIsInZhbHVlIjoiWkE3VzNxMEFqY05HRDV4QWc4enBTUT09IiwibWFjIjoiNTY4MzFkNjBmODYwMzlkY2FiNTNhZGFiYjU2NjFmOGM4NmU1MmI2NTE4NWM0MDA5YTMxODVlMTU5ZTY5NDVlZiJ9\"}', NULL, '2020-12-07 18:15:37', '2020-12-07 18:15:37'),
('1d1703b8-43c0-4288-9d69-da8863c45c16', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlNKdVhnNDh0SGg2emp4cDNYY1RIZ2c9PSIsInZhbHVlIjoiSWVnS01Oa3p6UmNRQUdyRjNYc0ZYQT09IiwibWFjIjoiMGI4NzQ2MTJiZmQ5ZjhhMmUyYjU2NDMxOTFiMGZhNDI4M2U3N2M3OWE3MDI3YWVmMmE4MWRmOWQyNTVmMjM3ZCJ9\"}', NULL, '2020-12-02 21:26:55', '2020-12-02 21:26:55'),
('1e6b8a08-8f16-43d9-872c-b91a48e65f17', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjFuajk3QnEvWkNnTXdvT0lmb1loMnc9PSIsInZhbHVlIjoiY1FkcFVEclF3TWRJTDJsNmZib0NGUT09IiwibWFjIjoiMTM0NDViMTczNDcwNWQzMjljZjM5Y2Q2MWU0MmY5MTkzODgzZTRlMGQ3M2RlZTZiYmJhMDAyYThmYTZmN2IzYyJ9\"}', NULL, '2020-12-09 19:02:55', '2020-12-09 19:02:55'),
('1f33de67-2579-4ac7-b144-a321edfba1a0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImpwQmMrSHgvMWxpSTUxNE02MzR0bVE9PSIsInZhbHVlIjoiSFlsUDRFNk9TczVVTTllRDNwRU9Hdz09IiwibWFjIjoiZjI2Y2I1ZWI0ZmViZjM0ZWZjMDBlNjA4NWMxNTczNmM3OGYxNjkzNDhlZTY1MTgyY2UxYmEwMWM0NTZhMWVmOCJ9\"}', NULL, '2020-12-03 20:52:07', '2020-12-03 20:52:07'),
('215ffde8-c495-4305-aba4-9aed4080cc42', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InFTN0pXdER1WCtGNndsU2VicEhxelE9PSIsInZhbHVlIjoiSlY1NmoxM1JEUjZZdmQrbDk1TWgvdz09IiwibWFjIjoiZDY0Y2ZiMDU5YzdiYjVjM2RlZGNiNDkyODE4N2QyZWNhNzlkMDA1OTM3MTdjMDdiZDljMDlhZWI1MzU2OTJjYyJ9\"}', NULL, '2020-12-09 18:43:23', '2020-12-09 18:43:23'),
('238a58ef-bad0-4789-bbe3-411a9f4998cf', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkwwdTI5WStPWTExblJoUUlNemhjOWc9PSIsInZhbHVlIjoic0xLMnI5QndUOEJvYjNra1kxNWpGZz09IiwibWFjIjoiMmJiYjFjYjExZDRiMTUyY2UyOTA0NDVkNzQ2M2I4YWM5NmVlYzZlZTA5YjJjNTA4ZTlmYWJlYjRjNzljNzA4ZiJ9\"}', NULL, '2020-12-15 18:55:12', '2020-12-15 18:55:12'),
('245c8818-2474-4679-a1e1-2d7e4c592f7b', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImlHcVpTOFBYMTdnYUFxTnVEMWlJOHc9PSIsInZhbHVlIjoiY0N4OVhQTjEvNHFLVzF4N2xPR2lrUT09IiwibWFjIjoiMGJiMjM4MjVlNTRiZmE3ZDkxMmJmNDY1YmNlZmUzNDNhYzUwMWE4ZTY4Zjg4OTExMGNlYTBmODYwMjFjZjU2YyJ9\"}', NULL, '2020-12-09 18:39:32', '2020-12-09 18:39:32'),
('249f6865-16a6-4498-b739-910116d8b627', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZYLzJKNVI4bUI2b2Fja1c1cVk2dXc9PSIsInZhbHVlIjoialBRZ0ZOUktYanJVRmsweFd6U295dz09IiwibWFjIjoiZmQ0ZDExZTMyZjJlNmZjNjFkOWY4NDNjYjQwMWM0Y2UxNjVjZGQ0NWUwMmQ1NDY2YmU3MWU5MDk3ZDk5YWY1ZSJ9\"}', NULL, '2020-12-01 20:03:31', '2020-12-01 20:03:31'),
('2514c7f9-b3aa-435d-86fe-cbc7e4851bf9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImFKM0pVZ0gyS3ZJS3hxMkhkZ0Zkb0E9PSIsInZhbHVlIjoiT1JSbFRhdUUyMTZaR0dvMzhvTW5Jdz09IiwibWFjIjoiMmE0OGE3MDQ4NWRhYTJiYzZlNzNiOGU1N2ViOTBhMDJhYjRjMWYxYThkNzE0MTk2MmU1YWEwMjhjYmFhNjFhMyJ9\"}', NULL, '2020-12-03 20:50:24', '2020-12-03 20:50:24'),
('25508161-13d4-41ee-8593-9ace04c3a524', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjNFa004REMwVzdEeU5tVzVxNDlVRUE9PSIsInZhbHVlIjoiZ2ZSRmJ5b3VMVjJQMEVGMG0rOWI2UT09IiwibWFjIjoiNjViOWNmZWEwMmM3ZmUzOTA1YTE1NjNjZmU3YjVhNjQyNmM0MjkzYjkwMzc1MGFhYTMwNWUwY2Y2MTdlNDdjNyJ9\"}', NULL, '2020-12-03 18:14:14', '2020-12-03 18:14:14'),
('25930196-3289-40ce-a276-7ffd2f3fab2a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Imcrci9RcWVWUkp1M1dWNTZrRUhZaVE9PSIsInZhbHVlIjoiRG0zaFFZMFVGTFhyamVrTGRnSE1HZz09IiwibWFjIjoiNzkzNzQ3ZmY0NjgzOWM2MzE0NGJiNjFiMjI0MTE2NjQwOGJlOWRmM2M5ODMzZmM5OWUzZDZlNGVlY2I2NjZmYiJ9\"}', NULL, '2020-12-07 17:52:34', '2020-12-07 17:52:34'),
('26777e7d-3286-470b-851e-352e7264d212', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImdMZzFyL2pOa2ErQjlUdUQ1bjFiQ3c9PSIsInZhbHVlIjoiQ3JaUmFmeFlJRUpBUlU4QVhiLzVPZz09IiwibWFjIjoiMDY0ODllYzAwNWFlYjgwZTQxYjAzNDhiZTUxMWQwMTIxNDE5OTJjMTQ0Zjc1YmM3OWU5OWZlNWQzZGZiMzNjYiJ9\"}', NULL, '2020-12-09 18:41:19', '2020-12-09 18:41:19'),
('296012a1-666c-405b-8881-e2ebbc8e5ad4', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Story of Stich<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImE0Qk1COWk2Mk42TzBWdnhCdkhGYVE9PSIsInZhbHVlIjoiM1d4WGR4R2pBcndqdWh0cUJydVlYdz09IiwibWFjIjoiMWJhYzdkNWI1ZDljOTNmNmZiYWNhMjE4ZTJjOTYxZTJkYjRhYTgyMWRmNzZmOTA3ZDc1NTZlMzQ4MmQ3ODRmZCJ9\"}', NULL, '2020-12-12 19:51:30', '2020-12-12 19:51:30'),
('29dea68e-4ee2-49f6-b5c8-d0e0dc3ff3c5', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImdWWjRNQnV1OThWRVkxSkZBQVFPeFE9PSIsInZhbHVlIjoiZEpJVS8rT2x2VkNGcWVqQUNlekNGdz09IiwibWFjIjoiZTgyZWNhOTliYTYwNTQ3ZDAwYjI1MGVlNzFjNjE2YTY3OGM4ZjdmNmJiYzUyOGFkMmNlOTkyMDk3NWQxYjk3YiJ9\"}', NULL, '2020-12-13 21:24:01', '2020-12-13 21:24:01'),
('2a72fc4a-c43e-4552-a2f6-007599fa5658', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkwxOCtkN2pQOGhteW5IR0R6NCtGV2c9PSIsInZhbHVlIjoiU1M4b1pJcS9yT0QxUTIzUDZ2SmZsZz09IiwibWFjIjoiNmNlOTdiMTFhODk3ZGI0NTg3MDJhMmEwOWVlNGZiOGY2M2QyZmRhNTRhOTE0NGRmYjZiZTdiYjU2MzA0NDNjOCJ9\"}', NULL, '2020-12-13 18:31:18', '2020-12-13 18:31:18'),
('2b168331-c8a0-4a9b-a979-57490263e567', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Nisa\'s Wardrobe<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InNZVkIvRnQ2MHQ1NnplQWpMNnI3SHc9PSIsInZhbHVlIjoiUVJDYURheXlFNTRwUjJRbWpYa1hqdz09IiwibWFjIjoiYzdkMjQxOTMyYzc5M2YyZWU0Mzc1MTFkYjg2NTQxOWZlZWYyMDk0ODNlM2E2MDNlMjIwNDRmZThlZjE3MjZmMCJ9\"}', NULL, '2020-12-14 20:25:47', '2020-12-14 20:25:47'),
('2b4656d0-7a33-4d6b-aee3-b099d7191ee7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlpKNVdKWUZibE02WFFSQm5oZzM1T3c9PSIsInZhbHVlIjoiNkFucVloditRSHR0NFFwRDBMN3k3QT09IiwibWFjIjoiZDQ1ZWYwYWNlYjEwNTkwMTBiNjdhZmQ5MzcwNzEzODYyODMyYTc2YmJiNzI0M2MxZWU5Njc5MmE3YjE2M2ZmNiJ9\"}', NULL, '2020-12-03 19:17:54', '2020-12-03 19:17:54'),
('2bd6de58-a7bd-40b0-988b-da0433333cc9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkFycEpRS2JwQXdYSEVZaEIyVi9ZN1E9PSIsInZhbHVlIjoicHRHeVhSUkRsV2NrV1FJNTZKbnNsQT09IiwibWFjIjoiMmUxMzgzOTAxMjg0YTdhMmM3OWM2NDgyM2VkZjM0YjQ1MzU2YjY3NGNjMzBiZDI4NDRhYjc1NGYyMjI2NjcwNCJ9\"}', NULL, '2020-12-15 19:25:55', '2020-12-15 19:25:55'),
('30e79fac-7427-4278-981f-6d360dc22580', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ad\\u09c8\\u09b0\\u09ac\\u09c0\\u09a4\\u09be\\u09a8-Bhairabitan<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im8vVFc5aEtsQmRRejNzOWsrbTAzaWc9PSIsInZhbHVlIjoiL0cydjNpa2RCcll4V1RQbXNEVWZNdz09IiwibWFjIjoiNzJiM2I0MDk2MmM4N2QwNjJlOTUwMzRkMTgwYWY5NzM2MmY0NzBhY2IxNzI5Mzc3ZmJhMmE3NTVhNjU5Mzg3MSJ9\"}', NULL, '2020-12-03 18:37:18', '2020-12-03 18:37:18'),
('31578d48-b43e-41dc-8c7c-482de273f60a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlNpbUllemdJRG81SjJhVHAwNG0rNEE9PSIsInZhbHVlIjoiYXFjeTNSNkg3TURJK3VNT0NTTnRRdz09IiwibWFjIjoiMjAzODkzNGE5ZTYxZmExYWNlZDk3MjM1OGYwZDlhOGI5MTY4ZjkyYzkzYWUyMDEwMDYwNzg0Mjk1NGRmNDg3MSJ9\"}', NULL, '2020-12-09 20:17:53', '2020-12-09 20:17:53'),
('3180b319-1ab2-465f-bdcc-8553bbd715b4', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Zhaapi Sharee<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IisxVUE0cUJyYjVnR3dha1lkTEpVL3c9PSIsInZhbHVlIjoiMDdyM1dGTjA4eEM3V3JHOG80K09iUT09IiwibWFjIjoiYmZhZmM5NmI0Nzg0MmFlNDExZTM0NGI5ZTJmMzVmNjkyMmE4OWU0ODY2YmM0M2MwYTc1MjNlNGJlMzRlMGNhOSJ9\"}', NULL, '2020-12-06 21:48:21', '2020-12-06 21:48:21'),
('3231b368-6a08-4cb5-b54e-b47d2844ea8f', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>CHanU<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Im9aQXBVSFN0VTZWMTZka0hTZTl0dnc9PSIsInZhbHVlIjoiM3d0OWlQWDFCMUE4OFFoUUtPbXpmQT09IiwibWFjIjoiMGVhYWRjZjUwNzg3NTRhNGY5MjVjODUwODNkZTI4OWI4M2VlMWIzYjYwMmEzOWYyMDgwNjgxYTU0MjA1OThkMyJ9\"}', '2020-12-04 23:04:58', '2020-12-03 21:28:15', '2020-12-04 23:04:58'),
('32c936bf-0423-45ad-afd8-384bc11eeafa', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImZsNWFITHRGL3UyM25oM0Z5V0k0bnc9PSIsInZhbHVlIjoiM05LY2Z3NjQ3MFpkYm9TOHJIdVdpQT09IiwibWFjIjoiYzBiMTFmNTQyOWY3YzU4NDExZmI2Y2VmNjRhODQ0NjEzYjczZDhiYjhjZGI4YThmNzM1NWUyNWUyNjlhM2QwMyJ9\"}', NULL, '2020-12-09 19:02:55', '2020-12-09 19:02:55'),
('32fd0d01-b97e-4c65-915d-cd2bd8558781', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Trinoyoni : \\u09a4\\u09cd\\u09b0\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlM1RTRkcjVwd29rVjRnbEtpUGtQdEE9PSIsInZhbHVlIjoiUURDWkZuV0NoN2JwdzVicDFkRjJxZz09IiwibWFjIjoiYmU2MWYzMjdmNGQxYzQ0N2ZhZTk5ODVkNmY5MWJmZWQzNTkyYzhiMWZhZjcyYTlkMzlhNjQ5YzQ0MGQwZTM1MiJ9\"}', NULL, '2020-12-14 18:53:15', '2020-12-14 18:53:15'),
('33b15938-4434-4ae5-8897-0fe1e951225d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlNwdzZkdkVaVmRyYTN6Mk5FV0ZnUWc9PSIsInZhbHVlIjoiNW1FMVBubUdjWnhTdU9mZGVsMFNGZz09IiwibWFjIjoiYTZiMDY1NzI4YTJiYzYxN2MzMjUzOTBkODgwNmRiMjk3NGY0YTgwODkzYjNhOTM2NDU1NzI2ZTE5MjhkNDdhMSJ9\"}', NULL, '2020-12-15 18:42:25', '2020-12-15 18:42:25'),
('34e3887c-1b03-4863-bf9d-0b92740a3218', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjRwWWlXUGNueGhZREhWQnpmZ251WHc9PSIsInZhbHVlIjoicHltTFhMa3RoVGtmaWg2UWtrMXRnQT09IiwibWFjIjoiZjhlODg4ZWFhOGJiMTEyMDY1NTY4YTkxYjY3Y2RjYzczNjBmNDU0MmU5YzAyYzEwMTNjNDIxODRlYmQ2OTk2ZiJ9\"}', NULL, '2020-12-15 18:42:25', '2020-12-15 18:42:25'),
('34ff0547-e698-4fe7-a04d-d391a7ba7b62', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ijdyc0lrQXNNWU5RL1M0VHFuU21LbVE9PSIsInZhbHVlIjoiWjF5bzRkczluR2dac1Foc2JqU0pNUT09IiwibWFjIjoiMDE3Yjk5ZThjNGJkZmE2N2U1NTgyMTcwMWM5YmM5Njc5YTg4ZjY4OTBmOTQ0M2E4ZDFiN2JjNzhmN2JhMjRhNCJ9\"}', NULL, '2020-12-15 18:29:53', '2020-12-15 18:29:53'),
('35dc36a8-db3a-4626-9161-a71114020043', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjdCM25ySWFaRUZab1RzcitEZXd1TEE9PSIsInZhbHVlIjoiak41d0FEY0hKOXI4RmJDbjFWeVRwQT09IiwibWFjIjoiMmJhYWNkZjg1YzI0NTdkODQ3NjQ5Zjk0YzEwZTAxZmI0ZTkyOTE3YTZjM2Y2YmRlMzY1NDJjOGE3NjQ1YzQ5ZCJ9\"}', NULL, '2020-12-05 05:12:12', '2020-12-05 05:12:12'),
('368e00d1-f44a-482f-a178-85edb8eaeb8e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik9IZjJZZ21DcGdmZEIyVEdGR3BvUWc9PSIsInZhbHVlIjoicklYYUxxanVFYUxXaDlhNC9aV1Nxdz09IiwibWFjIjoiMjVmMWQxOTI5ZTYxOTRlZTFjNmIyZjk3ZDYyOTQ1ODAxY2RlY2VlNTc3NGNlODRhZDE3MWNlZDVlNjEyNjcwMiJ9\"}', NULL, '2020-12-09 18:39:32', '2020-12-09 18:39:32'),
('374454dc-6b80-4e48-9173-a5ecab21d644', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZ4Mjhob0VEQ2M0VVF0TlphTzFYR1E9PSIsInZhbHVlIjoiNVE2b0VNOU84ZXA5NndrNHlXRnEwUT09IiwibWFjIjoiMGYzZTEyOGQ3MjFjYzYxODJkNzE1M2FlN2FjODYwMDI1ZTg2Zjk3NDlmMGMxMjE4ZDQ3NmRjNTBiMjFlYWU5NiJ9\"}', NULL, '2020-12-13 18:26:22', '2020-12-13 18:26:22'),
('381a8fa8-5890-4921-85f3-fa417fffbc74', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkhGY0NFRm1abGZyc3VtYXRFQitsdUE9PSIsInZhbHVlIjoiVmpzcS9ON2ZBY2JCQTVScjVxYkdtdz09IiwibWFjIjoiNTk2ZjNiNjM0MmJiNWUzYWE0YTY5MDdhNGRjNzZjYjc4MjJlNzQ0YmQ3YTkzMmExN2E4NWM4OTRhNjNmZTU4MSJ9\"}', NULL, '2020-12-03 19:08:15', '2020-12-03 19:08:15'),
('3a24a252-b919-47b0-9fa0-20d65a0bf20a', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InJzNHA0N3h2eThNRjk2bnJyMU9PTEE9PSIsInZhbHVlIjoiUzA3b0RYUTA3WDV5YmQvWGFBb2xmdz09IiwibWFjIjoiODUyMWM0Y2Q3NTVkMDJlNmJjZmRmZjI0YWRhY2MzOGI3YWEzMmRjNjU2OTRjNzQ1M2E2ZGQwNWJiMzdiNzRhMSJ9\"}', NULL, '2020-12-03 20:31:59', '2020-12-03 20:31:59'),
('3ae88550-7e81-4666-b762-d60fbaab0317', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlFvRmpiQjBSUmV0Z2R6TXgwRjdEOHc9PSIsInZhbHVlIjoianozVDhTMjhielZzZldKaFZuc3Yzdz09IiwibWFjIjoiMTBhODNmZTVhYWQ5M2NlYmUxMGZlODZhYjE1MDE1YTcxZDJjOWY5NTViYTMxMmRjZTY3Mzg4NDgzMDEzODBiZiJ9\"}', NULL, '2020-12-09 18:46:15', '2020-12-09 18:46:15'),
('3bf63a89-88bc-4357-b952-e94b4eb1841e', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Nobonita<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjFVMTNwSHQyWUpUSXh1bDdxeW82K2c9PSIsInZhbHVlIjoiZVBIa2M1YVMwNmVEL3h2a0xTcGNSdz09IiwibWFjIjoiMmNlZTdjMzk4NDdhNzI4NTFiN2Y2ZThhNTE4N2U3OTE1OTJiMTY2YjFiYWQ2ZDZlNGFlZTFiOGZiNzIxOTgwZiJ9\"}', NULL, '2020-12-14 20:16:41', '2020-12-14 20:16:41'),
('3cb82cad-d8d9-458a-a048-1c01e952abdb', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlhiTHNnQ1NuYVRlWHFnRjVESmdudHc9PSIsInZhbHVlIjoiRzluQVNsL2luNFJLcDAvTmJ1eVFGUT09IiwibWFjIjoiZjU5YWUzZmQ1MDAyZmMxY2M2ZjJjZWZiZTQxOWE2MWEzNWNkY2U1ZGQ5ZGE4NjQyZWEwOTI3NDMwZmM3OGQ2MyJ9\"}', NULL, '2020-12-01 21:48:48', '2020-12-01 21:48:48'),
('3f91bb26-6548-4e9c-9fd9-10cbd8b60852', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjZ3bHBxM2R6UXF1Yy9NSzB2bkxsVGc9PSIsInZhbHVlIjoiZHRwcSt6M1psMWlvUVZxeS92amFYQT09IiwibWFjIjoiZTViNTYyYTIwOWViN2I5YjRkMTBiZTBhMDFiZjFmYzU3ZWQzODMwZDUwZDIwNjQyYjNlYzA5NzNlNmNjODE0MiJ9\"}', NULL, '2020-12-07 18:09:32', '2020-12-07 18:09:32'),
('417e4b23-d6b0-40f5-bfbc-71ba547ccbb2', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>CHanU<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjdGWXp1Q2JOTkt6anlvQ0tIZjk5cHc9PSIsInZhbHVlIjoia29TdkdYMFlkT2ViY3NBSllwLzVwdz09IiwibWFjIjoiYjVkYjMzNmY5NTNkNjRkZTgxY2JlNjI3NjNkNjJiYzgxNjliNWY2YjQ2YjhhOTBlYmJkNGZjNTU1ODkzMWQyNCJ9\"}', NULL, '2020-12-03 21:28:15', '2020-12-03 21:28:15'),
('41b1b16a-a4a2-461d-bb09-bcfea8ebd949', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Gaandhari-\\u0997\\u09be\\u09a8\\u09cd\\u09a7\\u09be\\u09b0\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlJCdXlZWGlWTnFLZUpjREIvT1JjTVE9PSIsInZhbHVlIjoiZ0NjRGZtZG1VV2tBejVQYmMwTG5SUT09IiwibWFjIjoiMDQ4NWU1MTg2YTJhMTM3YWRhMTdlYjhjNzM1M2I5Njg1ODVlNDQwMzMxZjc1Njk5OTQwZDBlNzFiNzNkODJmOSJ9\"}', NULL, '2020-12-01 22:36:02', '2020-12-01 22:36:02'),
('4245b539-4a02-4127-a41c-5e655de4ba6f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkVEdCtFaWQrOFdqVXlVcnBYUlg2Vmc9PSIsInZhbHVlIjoiNXkya0dZYU5zMmgvS2dQclRhQnVSdz09IiwibWFjIjoiMWFjODA0ZmQxZGYxNzVmMWQ3NjE2ZjU3MmRkNWRkZGZjMzVkMWJjMDBkNzU2ZjIwM2Q3NWExOGRlNWZhZTBmYyJ9\"}', NULL, '2020-12-13 18:45:19', '2020-12-13 18:45:19'),
('42c6c527-8892-4618-a240-dfa7f403547d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjJWUUFSSlYwZWo4Y1NrUzVTM3d5cnc9PSIsInZhbHVlIjoiQnlhL3hhNmxJaStnQk9MbTFyQnNrdz09IiwibWFjIjoiNWI3Yjc4Y2JlMWM4NDAzOGQwMGFmMmExYTI3ODhkMTY2ODIxNzU1YjhjNjJlMTY4YjRmNmZhZjk2MDg1MWIyNSJ9\"}', NULL, '2020-12-01 22:29:49', '2020-12-01 22:29:49'),
('443f230b-8eb6-46f2-9229-03327ac8513f', 'App\\Notifications\\Shop\\Product\\Status', 'App\\User', 56, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09bf\\u09a3\\u09a8 \\/ \\u09a5\\u09be\\u09ae\\u09bf \\u09b6\\u09be\\u09dc\\u09c0<\\/b>\' has changed status\",\"link\":\"http:\\/\\/shopinnbd.com\\/product\\/48\\/%E0%A6%AA%E0%A6%BF%E0%A6%A3%E0%A6%A8-\\/-%E0%A6%A5%E0%A6%BE%E0%A6%AE%E0%A6%BF-%E0%A6%B6%E0%A6%BE%E0%A7%9C%E0%A7%80\"}', NULL, '2020-12-07 01:49:56', '2020-12-07 01:49:56'),
('45d6b2f9-1463-4049-80bf-bccf832eb0cb', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkZBS2pRWWxQenBkVEdqZ1d3N3p5UGc9PSIsInZhbHVlIjoiYjd6L3NrMjdhT0pTc1VKUzNvcGF2Zz09IiwibWFjIjoiNjUyM2RiNGFhYTk5N2I4MmU3OGZkNDFhMDkwN2Q4MGE1MjE4OWIxYmYwYjU5NWUwZGFlNzNkNTBkZTk4ZjhkYSJ9\"}', NULL, '2020-12-09 19:16:52', '2020-12-09 19:16:52'),
('470d158d-16e5-4734-aad4-30d18467bd0f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkthR1VldnpXd0M1Z240OHAwZ1diNEE9PSIsInZhbHVlIjoicFdMc1ovYXNrTXJJN2JvNWl4dFdjUT09IiwibWFjIjoiODZiOGI5MGFhNzUyOTFhNWY5NTk1Y2FlMDhhZDZmNjQ4NDM2Yjg0NjRhZWQ2YjU5ODY1OWYzMjUzOGE1YTg1YiJ9\"}', NULL, '2020-12-09 20:10:06', '2020-12-09 20:10:06'),
('482cccc3-067f-44ae-93c4-4d98e3b57ecd', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IituSkVPVGZjQTd2STNDNER0YVgwa2c9PSIsInZhbHVlIjoiaGpDK1NNU0wxKzZBY2F6SXY5N2g2dz09IiwibWFjIjoiNDU3MzgzYzc4M2QxODJmYmNhMGM2ODY0ZmE5MmRmYTJmNTNmOTgyMDYxN2NlNWFhMzNkODVkM2Y2MjgxNmYxOCJ9\"}', NULL, '2020-12-03 20:56:55', '2020-12-03 20:56:55'),
('487e771e-b7dd-40c8-9a46-5eeaba4b5769', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Story of Stich<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InNSejFVM0lBemhwWWZFdzJFUDVLN3c9PSIsInZhbHVlIjoiazltRGtFaHNQcVUwbkVqbWpBQkhjUT09IiwibWFjIjoiZGJkZGI4ZWI2M2RmZjI5YTg2MTlhNzhiODhjYzgzYTQyYzM1NTJlNDYxNmIyMmZkYjhiZDJiZThkZGNmY2M2NSJ9\"}', NULL, '2020-12-12 20:24:58', '2020-12-12 20:24:58'),
('48dde9ba-cdb5-48be-93e6-e6b16d2a3e45', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlRBeWdibDd0SkRjbFF0UkdYUnd1aVE9PSIsInZhbHVlIjoiZlh6MFhQVnlraENoQjRUR2N1eEJidz09IiwibWFjIjoiYWZiNjM4OWZkMTNjYmJhMWJkNjQyZTU3ZmEwNjliMDk0OWQ4MDc1ZTY5MmU2ZjlmNzY2YmY2MzQxMGVlNjIzZiJ9\"}', NULL, '2020-12-15 18:01:36', '2020-12-15 18:01:36'),
('49dde6fd-2db9-4f67-b5ff-3aeafccdea75', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InhBclFWaU1oeFVtZjhjTzM0dE4yb3c9PSIsInZhbHVlIjoieHczZVBObDJ1ZC94RHNJNzRlTW1EQT09IiwibWFjIjoiM2NhMjZmYmVhNGYxNGU2ZDJjN2UwZTcyM2NjODdmY2YyMTZkMzcwOGQ1MTA0NTliNjAzZjY2YjdmZWQ4YWVmYiJ9\"}', NULL, '2020-12-15 18:55:12', '2020-12-15 18:55:12'),
('4a8154ca-d1f2-44a4-9fec-4e494bf0b5f1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ijh1L1o1dzdsSng2Q3Fpb0lMUFV3Zmc9PSIsInZhbHVlIjoid3gzcHo5ZW81QVRja1JWSFRDcVkxUT09IiwibWFjIjoiYWEwYzM2NjhhNWRmYzRiZDFkNmQ0YzA4ODY1MTdmNjI2ZmEwYjFkM2M1M2Y0NGQ4N2NmNDRkNjA0MjNmN2NlNiJ9\"}', NULL, '2020-12-15 17:35:12', '2020-12-15 17:35:12'),
('4b07d4ed-09a8-4dce-ae0e-86939378963e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099f\\u09bf\\u09df\\u09be\\u09b0 \\u09b8\\u0996\\u09c7\\u09b0 \\u09a6\\u09cb\\u0995\\u09be\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InNHbE1JV092OEUycTFLQzFIMTdJcEE9PSIsInZhbHVlIjoiM1BZTXdQak9HcS9YNVFraVdnV1g1Zz09IiwibWFjIjoiNTZjNzk4ZGE0NDBjM2JmMThiMTAxMzAxYjE4ZjE0OGY1MDlmZGNmMTY5OWE4MTUzN2NlZjQzZmJiODllYWIxNCJ9\"}', NULL, '2020-12-13 18:03:08', '2020-12-13 18:03:08'),
('4b41f43d-88e7-4790-9c63-4db7de52dc65', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Imkra1g5cTF1emNrcnNCajA1b2RuYWc9PSIsInZhbHVlIjoianUvdktaNlNiZlRtcmlNM3NrWEZtdz09IiwibWFjIjoiMDcyYTNiMmU2NzM5MzVmZWQ0ZjY1MTBkZmE4MzVhNTEwOTMyOGZlZGQ0OGQ4ZmNhNTE0OWI3NTVmMzBiNjkyZiJ9\"}', NULL, '2020-12-15 18:44:55', '2020-12-15 18:44:55'),
('4d033205-f473-484e-8099-74f9f7680db0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InVxdWwxN1Q1OVBqeW9ObStpR2xSa0E9PSIsInZhbHVlIjoiRkZ1N0J2NTRSRU5oYk5xbkFNMXRqdz09IiwibWFjIjoiNmQxZjFhMmJjNmE4M2QyYTQ5Yjk5Y2M1M2JhNmEyZmYxYjFkYzBkZGE0ODFhMTc0M2FjNzZjMWQ0Y2IzYWYxMyJ9\"}', NULL, '2020-12-15 17:51:07', '2020-12-15 17:51:07'),
('4ec5db37-2e8d-4345-8056-b254b4fb8378', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik81alNlL0E1YlRtSU12WnR2bzAwWnc9PSIsInZhbHVlIjoieTVLYTV0NEdWUG90S0lTS1g4R3dYZz09IiwibWFjIjoiOGNlMDE0NTM2NmY1MDE5ZjhhODg5NTIyYzMwN2I1MTRjOWFkM2JkNzEyNmYxZTYyMjM2YTU5MzAzZWQ2ZmMyYiJ9\"}', NULL, '2020-12-06 19:51:37', '2020-12-06 19:51:37'),
('4f16913a-dd0d-406e-9dd7-eff00a37b882', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjFpeENWQ05YRGhzOWdlYVgvSVZCaWc9PSIsInZhbHVlIjoibVFFQzcvMjJKSnZkWTN1TTFmRGFWQT09IiwibWFjIjoiMjkwZTY0OTRlOTE2YjIyOGYxMzk4YWM5NTkzYzAxOThkYjJkZWVkMzBmYzNmOGIwM2MxYThlZWI4ZDZiOTRhYyJ9\"}', NULL, '2020-12-15 18:32:30', '2020-12-15 18:32:30'),
('4f686d34-7501-4820-bc5f-59c9eaf3cacf', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlBNS0lqSmhKelAzaUQ1K2JMcS81S2c9PSIsInZhbHVlIjoiT0ZFYUpQYzhLZUVpS09NdzVTY051Zz09IiwibWFjIjoiYjg3ZjAxNDQwZmU3OTlhMGJjOWM3YzQ1NzZmYWUxMDhhOTU0OWZhZDQ4NzUzZGUxMmMyZjFkMTkwZjVjMmE2ZCJ9\"}', NULL, '2020-12-09 18:25:45', '2020-12-09 18:25:45');
INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('4fa11cb2-b7d6-43e1-af02-efae36e253ed', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a4\\u09be\\u09b0\\u09c1\\u09aa\\u09be\\u099f-Tarupaat<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImZqODIvSlFKVnhRYko2NHZxaWRrcUE9PSIsInZhbHVlIjoiVXdjbE1KL0Z4QzBoOEF3VGtpVldoUT09IiwibWFjIjoiMmI1ZTI5Yzk3MTc1OWJmYzQ2NTI5Zjc2MzEzNzk3OGNhZmIxNGUzZGMyMWQ1YjNlMzBhN2EzNWNkNTM2YzliYiJ9\"}', NULL, '2020-12-07 19:01:08', '2020-12-07 19:01:08'),
('50912e4a-0202-4de7-ae5e-6182c54a7e3a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IldwbWVvUTlGQjdLUG14OUJNaTNTcHc9PSIsInZhbHVlIjoiWXVsdGt6c1NSY3o1VnhDMDhaM2hiQT09IiwibWFjIjoiM2M0ZmUzYTMwZDQyOTdhZjY3YzYwNTg5ZjhjMTBkNmZmYmQ3NzVkZTZmOTlkNWZhNDg1NzhmNTFkMzI0MDU2ZSJ9\"}', NULL, '2020-12-06 19:51:37', '2020-12-06 19:51:37'),
('51535440-7a13-427c-8d2f-d7c0f93ce7dd', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkdvdFYvZkcyeDJ2TTJWaS9kc3pId2c9PSIsInZhbHVlIjoiYU90WTBWVEM4KzNIQXE1c0lMVkp5QT09IiwibWFjIjoiNWFjNmUzN2ViODEyNjhlYWE0Yjc4NjZhOGE0YjI3Njg2YTY2YTY4MWE5MTExYzBjZDM4NTFiNTgyYjE5NmJiOCJ9\"}', '2020-12-04 23:04:52', '2020-12-04 21:52:25', '2020-12-04 23:04:52'),
('540f2653-2a98-48b2-8740-e9840e79ec67', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Nobonita<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlhkSDFTWlMrL3lqVGs2b1lyanVTTHc9PSIsInZhbHVlIjoib3JIajlKMWFlNjh2WGlqeEttZFMvdz09IiwibWFjIjoiMWI5NTBkMzgyNzVjMTMyNTRhNjhjYzk2NzNlZmZlNzY1OTU4OWRkNzgyODFiMzY3ZTQ0MmE1MjkzYTdkYjQ2NCJ9\"}', NULL, '2020-12-14 20:16:41', '2020-12-14 20:16:41'),
('5506bb1f-21d9-4b06-a493-8479117f475a', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Crafty Fire<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IndTTXpGN0ZWZjRHWlVJRWNkU2dxQ2c9PSIsInZhbHVlIjoiT2pVYnQ2cVZtL2NGQ3RkN1hIamVXQT09IiwibWFjIjoiNjI5ZjhhNDBkY2ExZWEwMDk4N2Y2NjNiOTRlOTY2NzYxZDY0YjZkMGY3NzlhMGIyMzBhYzgzMTEzMjZhMGFiMyJ9\"}', NULL, '2020-12-14 21:06:26', '2020-12-14 21:06:26'),
('55e4b90b-29b2-40d1-be69-a3353fa26014', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InduTFgyL29YM1N1SUVyN1ZoNnBYMkE9PSIsInZhbHVlIjoiYTNIeGFHUVFSWlpKSERsOW9BU2FwZz09IiwibWFjIjoiZGYzMWRkYzEwMmIyZjMzOTI2MDg5Yzg4NWJkMzQ1MzExZGNjZTdiM2JiMjJmNjAxODEyNjY4NmVhYjYxMmUyZSJ9\"}', NULL, '2020-12-15 18:46:55', '2020-12-15 18:46:55'),
('56149d67-e7c8-4d6b-951b-5e4f3068a2c4', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImN1TjdPbFRMcGc4bkFHVFVYNmJqNVE9PSIsInZhbHVlIjoiOGVEd3dadUlVRXZ0Ym9VUUtSMHkrQT09IiwibWFjIjoiYmYwOWZlM2ZkZGIwOGRkYjhhMDg4ZjBkYjI4MzNiY2ExNDcxZjI1MTZjYTU5M2MzZGUxNDk3ODA5Y2QyYWJhMSJ9\"}', NULL, '2020-12-15 17:49:30', '2020-12-15 17:49:30'),
('565c8037-87eb-4f43-9468-5b4b15c7eda6', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImhIZ0V4ZUlLN28yREVsOHhGaFYzNnc9PSIsInZhbHVlIjoiMFNkdy9seFNLV1c3Vk0yYUpnSGxldz09IiwibWFjIjoiMGI2ZjFkZGZhNTc3M2Q3NTY5YjA4YjFkZWZiM2RhZDY2OTE0ZmIxZjA0MzVjNzY2NmQ5MzJmNmZlM2M0Njc3MSJ9\"}', NULL, '2020-12-07 18:15:37', '2020-12-07 18:15:37'),
('56716429-5e95-4d4d-8311-84fca6f29ed1', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Moushumi\'s Loft<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImtHNDl2K0YwTEtaK2VUQU93UjRVNWc9PSIsInZhbHVlIjoiVGhpOWNuM04wSFNnZkF6dzc1ZGJNUT09IiwibWFjIjoiMWEyNTcyZjAxNTJiNmQ0YmJjZTkyOGU1YmQ1MWYwN2UxNGY3MTg3ZWE0YmFlNmE2MGI0MjZmYzAyYzVmYjg5ZCJ9\"}', NULL, '2020-12-06 19:11:19', '2020-12-06 19:11:19'),
('56ee46a3-7d42-4bf3-aecd-a31eb37e0e59', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InM2Y3ZhcWkxcE9Fa2hnWkN0bnNKQ0E9PSIsInZhbHVlIjoiazlFM3VxN3NlZk0zUUFVYVo1UDBGZz09IiwibWFjIjoiYWNiMDFmMmExMjkxNTc2NWEwNzYwNzhiNDMxN2U0ODlmOWFiZjljMTY3MmMwMzAwODNhYzlhOTk0Y2UzZDk1NiJ9\"}', NULL, '2020-12-15 18:01:36', '2020-12-15 18:01:36'),
('57f0a79e-34f3-430f-a0e7-b2c04a75991a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im53YUJKODJDdlNKRTl0aEg2ajlQSWc9PSIsInZhbHVlIjoic01wdWVCNDRBc0h1NnlxMTlYN3Fsdz09IiwibWFjIjoiYmQzNTk5NTlhMjdjYzQ1N2JmMDRlNjBiODFkZTBiZTJiOTA4YWVlNjM4NmFlYjQzMDZjNmYwYjMzMWEzNmJlOSJ9\"}', NULL, '2020-12-03 20:56:55', '2020-12-03 20:56:55'),
('584c4f25-0bd5-41db-8f95-8706d296584a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjJIYzUzS09Rd2hXbnlJOEF4MDZJM1E9PSIsInZhbHVlIjoiMnlWM2J0YUt6a2pRK3ZydEowV3VrZz09IiwibWFjIjoiNjMwNjQ0YTE5YWM2MTVmMjM0ZDJmZDYwMGU3MWUxNmJlNjMzYTQ3ZjMyYTVkNzQ2MDI4N2Q2N2E0YmI3MDNjOSJ9\"}', NULL, '2020-12-03 17:56:53', '2020-12-03 17:56:53'),
('58a94e22-622b-41f2-b3ad-8b494b7f7967', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkF1Z1Y1Y1AzS1lTcjl1aHZQcDczTlE9PSIsInZhbHVlIjoiQ1V2NGxYK01sOGVZQzFpbGl1RWpOdz09IiwibWFjIjoiZjNlYmE4OTVmNDcyNTdhZDVkN2YxOGVhMTczMDk0YWIxMTQxZjhhMjdjMzE0NjgyZTYyZWE0NzYxYWE3MDFiNiJ9\"}', NULL, '2020-12-03 18:11:05', '2020-12-03 18:11:05'),
('58b5fb80-292b-4b76-963c-b152c4d9e788', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ik1temNWblB0YSt6a2VMZDhmQnRUaEE9PSIsInZhbHVlIjoiNGt3TGQyelBUWkI3QzRrWHlFdGYxQT09IiwibWFjIjoiYWI4ZDhhNDk1YjU2MDE2ZWE2MDIwNGYxZjY5YWUyOWJiNmY5YjRiYjRhMmQ5ZDNiZDU2ZjI0NzU0MzNhNzJmZCJ9\"}', NULL, '2020-12-05 05:15:59', '2020-12-05 05:15:59'),
('590c0fd7-25ae-425f-a7b7-98508c9a1389', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlJ2S0prL0U3d3dzMytTc2hXM0c0bFE9PSIsInZhbHVlIjoiNzNVSUxKVWJ4RkxTSFhmRjlndUlzQT09IiwibWFjIjoiZTQ5NWIzMzRiNTNlMTkxNTU1MTM3MWE2ZWJmNmM1NGExY2MwMjdlY2JhZjNkMWMzYmE5N2NlZTFiNTE5NTQxZiJ9\"}', NULL, '2020-12-06 19:38:54', '2020-12-06 19:38:54'),
('594da1ca-0497-47a4-b31a-970aad34493e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ii9YYUtaY3NWR2xvenFDU09NODhzd3c9PSIsInZhbHVlIjoieVdnKy9BbUQ5ZGV4ZkppeTlpQnhXUT09IiwibWFjIjoiOWJmNjk2YmJjMTc5MTVkNjY1OTRhYTVmMjI2NGI1MzQ3OTVlYmJmZTIxYzg2Njg2MDI0ZmI5OTQ1YTA0M2NlMCJ9\"}', NULL, '2020-12-09 17:29:19', '2020-12-09 17:29:19'),
('5a1e61e8-f5a7-491f-9303-f9c61b2ce3ee', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IndabkhjYkJNNCt6VW1KL2ZpMG40UUE9PSIsInZhbHVlIjoiOTNyUVNyRHVyTEE1eEhzbnlsUjFoQT09IiwibWFjIjoiNGJhYTg5NDk2MjIwZTEyMmNjNjdiYTNlNTBiMTlmMWVlN2FlMjZiZjJiMTljNjJkODZiMzg0MWJmYTU4NzJmOSJ9\"}', NULL, '2020-12-13 18:26:22', '2020-12-13 18:26:22'),
('5b536ec5-61c0-4189-821d-3c0d4a781980', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ijh5K1JqRjNGU05JanRHSDljRSs1VkE9PSIsInZhbHVlIjoidDRWR1FZZTR1NzJCYnl5NFAzZmJsZz09IiwibWFjIjoiZGYyMWRmN2NiNTA0YzBiMjFjNWNjOThiOWYxYzY0MTA3MWMxZDUyZWIyNDllY2Y5MDM5NWI5YzBhODJmYjA5MyJ9\"}', NULL, '2020-12-09 20:10:06', '2020-12-09 20:10:06'),
('5bb4466d-eac3-4ee9-b1f6-bd3574055895', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Glowing_splint<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjF5bXo4cVEyUmdtTjQrejY4Y3B4L3c9PSIsInZhbHVlIjoiMlRkdGFJVTc4L3JJQlVuaDdrUWZmQT09IiwibWFjIjoiYmM1OTdlM2VhNzc1ZjY0MmY0NzZhZjY3NmU2YTY5N2U2NTlhZWQzMzI5NTE0MmI0NTRkNTU4Y2UzMTU2Mzc2ZSJ9\"}', NULL, '2020-12-01 22:54:53', '2020-12-01 22:54:53'),
('5bd3c77e-a9da-47b5-899e-862915c33658', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a4\\u09be\\u09b0\\u09c1\\u09aa\\u09be\\u099f-Tarupaat<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IisyeTIvVlk1Vi92SHM0aVpkQWtBc0E9PSIsInZhbHVlIjoiWTZLdEZ4Rzl3QTdUaE5oSVlRSEZXQT09IiwibWFjIjoiZWVlYmYwYWFmNzg5MTg5MmYxOTk2MGJlN2M0NTM3ZDNiNTI0MmE1YWQ0NGIwZWJlNWZiNWY1Yjg1YWY3YmY2MSJ9\"}', NULL, '2020-12-07 19:01:08', '2020-12-07 19:01:08'),
('5d15aaac-1688-4421-bf09-591962142850', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InZ0WW90c0ZFdzY2Mmp1dUUrQWlPM1E9PSIsInZhbHVlIjoiY3lLWUVMZFYzMWJiVlFyQTZxS2wzQT09IiwibWFjIjoiYzNiMGU5NDI0YWY4MjQ2OWVjZjJkNjlkYWIyMjBkOTY0MGQwYjQyYjE2MjAwNzBlNzIyMDRhNjQ2NTQ0YTQ1YiJ9\"}', NULL, '2020-12-05 05:15:59', '2020-12-05 05:15:59'),
('5f2fdc87-ce32-41df-80ef-528c62a2b117', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Monforing \\u09ae\\u09a8\\u09ab\\u09dc\\u09bf\\u0982<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InNQRDdheXlvdnlwbDlkb3Jyck5UbWc9PSIsInZhbHVlIjoidjdHWDRBRGRZZm9qcjhId0sxYmVZdz09IiwibWFjIjoiYzM4ZTdlYjc5YjE0OWU4ODU3YTI4MzczYWFmNmNiZDcwODM3ZTkxMWY3NGU3NDNjMWM2MjlkY2MxMWU5MDI4MSJ9\"}', NULL, '2020-12-06 21:17:08', '2020-12-06 21:17:08'),
('5fccb4ba-e5c9-41c0-b8eb-4f2dde253461', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InRWeUJQMDg2SnRRa2h2RGVmek92K1E9PSIsInZhbHVlIjoiWWI2S2NvbmlvVWNTa3pnRERXWFdWdz09IiwibWFjIjoiMTQ0ZGY0OTlmZmM0N2JmZTlmMmEwMmRmOTQ3ZjZlMzliYzYxOWQ1YjM1MmU3ZjZjZDI4ODAwMWI5Mjg2MTczMyJ9\"}', NULL, '2020-12-09 19:34:15', '2020-12-09 19:34:15'),
('5fe38cf3-d4de-419b-b34f-540ebdaa4a68', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im1tRTFselJmN3JZZ3U4RWwrZ0NJQnc9PSIsInZhbHVlIjoiYW11RFdOZzllQUQySWFPemFWY1FyQT09IiwibWFjIjoiZGVkMWUwNGY5YjRmMzM5MWVhNWI0OWFlZjYxZDhiNmRmYmNiZGY5Nzg0ZWVhZmUzYTgyMzhiYjI3NjYwMGIwYyJ9\"}', NULL, '2020-12-03 20:47:07', '2020-12-03 20:47:07'),
('606c8f94-ff88-44cf-9993-beb58621bbea', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlNmMExEaVVURnhmNWJwd3hpbkVZTWc9PSIsInZhbHVlIjoiL2VvU2F2S0t5ZnBIdlZjTk40S3ZoZz09IiwibWFjIjoiZTRjOTYzOGFmYTc0NmZiZmNiODliNjlhZDhkNDRiMTZkMjM0NTVhZTE5MWViOGZjM2JmZDZhNDRiNGQzMWVjYSJ9\"}', NULL, '2020-12-06 22:00:40', '2020-12-06 22:00:40'),
('612f984a-3710-4593-a305-148fff62d514', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Monforing \\u09ae\\u09a8\\u09ab\\u09dc\\u09bf\\u0982<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkVXS2hhaEtSZXRSYkJxMGdWYWJaM1E9PSIsInZhbHVlIjoidWFjNjd1T3RUem9waTNDZ0Nhci9QZz09IiwibWFjIjoiMzUzODZjNjE5M2M2MWE5NmQwNGU2YWQyZTU3ZjZhNWVlMmYzNDI4ZDRkYzA4MmY0YjkxODI3MDhhOWQ3MzRiNCJ9\"}', NULL, '2020-12-06 21:17:08', '2020-12-06 21:17:08'),
('6326430a-d30a-4000-aa2b-be72d897caa2', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImtNU0NWNlBVWWVDWnV4THhLdVBRQ3c9PSIsInZhbHVlIjoid01RY0ExL2V4bnZxU0tYTWJyS3JWQT09IiwibWFjIjoiODAxN2E3MDdmZWRlNzNhNGVlODZiYmQ2ZDY0ZmM4ZTk1NDlmYzgwYTJjNDhmYmM0NjA4MTg3ZmZhMjAwZmUyYyJ9\"}', '2020-12-03 02:01:02', '2020-12-02 22:04:58', '2020-12-03 02:01:02'),
('63e2bf09-4d4f-4fb8-ae76-de4fcb03666e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImIwcktiWkkyTi90bit6b2cvSzVxbWc9PSIsInZhbHVlIjoiWFdmRFpac0FQV2ZjZnZTcDBuL2xZUT09IiwibWFjIjoiYTMyM2I1MzFkNjEzNjRhYWQyZTc2NzU1Njg2NWE1NzYyMWZkMTNlNTUzMjcxN2M2OTkzOTQ1MTlmYTVjZjE4NyJ9\"}', NULL, '2020-12-05 07:04:54', '2020-12-05 07:04:54'),
('640e4a36-d684-45ae-a35a-9f778e16d728', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IktqMkZ0N0N0dVZjVkZ3ckIvVzREZFE9PSIsInZhbHVlIjoiNHFwYzY1S2lRU2pHTjJpbmlFYm5QUT09IiwibWFjIjoiZmU3ZTc5MWQ4MTdmYjA3OGE5ZjBhNWE1ZGY0ZDExYmYyMGJjMjBjOGI3NWIyMDFiMTFiZWY1ODhkNjMwODgyMiJ9\"}', NULL, '2020-12-05 05:10:45', '2020-12-05 05:10:45'),
('64595b6f-485c-4c74-9ea8-8393d62792c1', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b6\\u09c8\\u09b2\\u09bf By \\u09b8\\u09be\\u09ae\\u09bf\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Inh2YUFuUEZUOENRcWtveDJTZDltWGc9PSIsInZhbHVlIjoieWgySkVWVk5SWWRRVGVkQ1BxQ3VMdz09IiwibWFjIjoiZThjZTI3Y2I3ZDJhNzMwZjM2MDRkMjU0ZWEzZTFkZjAxYTNiZDIxM2U0NDFjNDI5M2NmNjM5OTAzOWRjZDliNiJ9\"}', NULL, '2020-12-15 19:39:05', '2020-12-15 19:39:05'),
('64f1800c-7dcf-4b2c-8d99-7da3524b7c90', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im9XTUx5VTJURzZteERTNGQwSnRVanc9PSIsInZhbHVlIjoiKzNEK0U3cUp1Zy9UWGVRRDBrNWkrUT09IiwibWFjIjoiYzE5OWRiOWMyYzE4ZTJhZTRjZmYwNDcwMzY4ZDI1ZDdhMjY2NzczMTQ3ZTkwYzM4NWQwMjliYWRjYjdmMWFjMyJ9\"}', NULL, '2020-12-06 22:56:55', '2020-12-06 22:56:55'),
('66530df4-2f78-4a58-af70-ea563966feb3', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ad\\u09c8\\u09b0\\u09ac\\u09c0\\u09a4\\u09be\\u09a8-Bhairabitan<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InpOZzA3QjE5SmwrT3JoUnNZbkFHY0E9PSIsInZhbHVlIjoiUzRIZVdndkNvTGM1dEhpOW5OUWY4QT09IiwibWFjIjoiNzg1NjZkYzc0OTIzMWIwZTVjMzlmZDQ4NGJiODNjNjNiNGY1NTg1YTRjYzZlYjQ2YzA0OTZkOWUwNTdlNTg3ZSJ9\"}', NULL, '2020-12-03 18:15:53', '2020-12-03 18:15:53'),
('66aa9718-029a-4797-9a4b-265fb231792f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Story of Stich<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjBYSS80WDFicVpQV0FPbTVjUnNJK3c9PSIsInZhbHVlIjoiUmtoc0FQUGxiTXRtcGZXOGtlTE4rdz09IiwibWFjIjoiMzU2YzNkMWQ0ZGZkOTMzYzlkNDVmZGViMDBiOTgwNWU3MTY4ZjZiMTMyMjc4YmQyNGFjM2NlNDI2ZjA2YzhiMCJ9\"}', NULL, '2020-12-12 20:24:58', '2020-12-12 20:24:58'),
('6796b4b9-d880-4d83-93e6-2f7b19d92e1d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Crafty Fire<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkMwZkMwcXVyRXViMFhobTlYSHRMM2c9PSIsInZhbHVlIjoiSnpCTUFFaCtiSmVNMVp0T3hpQWZSQT09IiwibWFjIjoiYWY3MzBlYTdlZjZmOThmN2FiNThhOGZhZDk4ZTc5NjI5MmRiOGNhMzBkYmU4ZGJjYzJhNDU5MDI5NmE5ZDczNyJ9\"}', NULL, '2020-12-14 21:22:10', '2020-12-14 21:22:10'),
('6821640f-d03f-4684-a1bc-0bec63af132c', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u0985\\u09b2\\u0995\\u09be\\u09a8\\u09a8\\u09cd\\u09a6\\u09be -Alokanonda<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImkvQXVyWHFvUXR5M3I5ZGZUUXBkbFE9PSIsInZhbHVlIjoiTU03Vml1T05kUVFpTmN6SC8wcGt3UT09IiwibWFjIjoiY2UzNzYwYTc2NWUxM2JkMGYwZjk1YjA2MzVjNDE3ZDAwMDk2MzFmMWI4NzY0ZjE0YmZlMzVmZDU2Njk2Mzc1YyJ9\"}', NULL, '2020-12-03 18:41:39', '2020-12-03 18:41:39'),
('68ee3083-6380-47c9-91fe-53019503bff7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ino4c204YUpCUUtFelFTY1VHUFFpQlE9PSIsInZhbHVlIjoicCt1K2V1ZmlydGdjMDlycGNZOHdDdz09IiwibWFjIjoiY2U2ZWMyMjQ0NTkyM2RiZDIxNmFlN2ZmZjcwZmZiNzgwMGIyOTUzYTkzZDQyMjBiYTRhOTBmNTJmYWNhMjY2NSJ9\"}', NULL, '2020-12-13 18:39:39', '2020-12-13 18:39:39'),
('694396ca-338c-4e62-beec-30ec1677dedd', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlpZbEp1YjQ4eDFDTnU5bW5SVGxYZWc9PSIsInZhbHVlIjoiam5iUERrVzlsNUpMbUc3ajcrT3RkUT09IiwibWFjIjoiYTFiZGExZTM3NGNlYTM2ZTMzZWYxNjMzNjMzNjYzZWI5YmI2ZWI5ZDIxZjQ5ZTYwYTQ0MDZmYjg2OTUyMzE5ZSJ9\"}', NULL, '2020-12-15 17:32:38', '2020-12-15 17:32:38'),
('6c3741d2-e391-4d59-89a4-bf0bc1cfe7c7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InVKbzArdnl6Zkl3NDFnYzFiSUpFK0E9PSIsInZhbHVlIjoiS0dpWHdXeFh4SnNPMDVOc0xGWUJCdz09IiwibWFjIjoiYjZmODkxN2YwNTJkZDg4YjIyYTk5MmMwMjM5ZDRiZjQ2NGE1YzgwOTQ0NGE0MjY5ZDI5ZDE0NDdiMDFjMjgxNyJ9\"}', NULL, '2020-12-01 22:00:24', '2020-12-01 22:00:24'),
('6c4aeadc-8fdd-4c27-aaf2-d4ff9dc85d30', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b2\\u09c0\\u09b2\\u09be\\u09ac\\u09a4\\u09c0 \\u09b9\\u09cd\\u09af\\u09be\\u09a8\\u09cd\\u09a1\\u09bf \\u0995\\u09cd\\u09b0\\u09be\\u09ab\\u099f\\u09b8-Lilaboti handi crafts<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImJERGkxOHpVVk5qdzVmUlM2SjROTmc9PSIsInZhbHVlIjoieFZmL3hGTVljTW41dFUvWUp5bmkvdz09IiwibWFjIjoiOTg1OWNmNWUxNzgxMjdlYTJlNjI0YTc0ZTQ0YTA1MDI2ZTBkZThmYTk4YjJjYzRkMWJlY2QyZGEwYzQ4Mjg0YSJ9\"}', NULL, '2020-12-05 22:08:33', '2020-12-05 22:08:33'),
('6d1edf7e-c0ea-48ba-9435-dfe7f43d1323', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InZqZk92ZnZYQWF0Mno2dHRpVDVNQ0E9PSIsInZhbHVlIjoidXN0MEtEc1RzWWlta0p3UStIK2VGZz09IiwibWFjIjoiZGU3ZWI3YzNhODM2YzBiYjBiYzY0ZmU3ZWFhNWUyYzI5ZTQzOTBjYjZlMjQ3Nzc0NDZkNGYxNTA1NzJkMmY5OSJ9\"}', NULL, '2020-12-02 20:35:26', '2020-12-02 20:35:26'),
('6d201cf4-d567-4548-a008-a3493adaa0da', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlNvQVlLL3JJTk9WLzgrWUk5L1pzeXc9PSIsInZhbHVlIjoiLzVwdzNIQUEydzNFa3d3Q09KMGtUUT09IiwibWFjIjoiZWU2MzEyYzA1YTlkNjdmYjdlMWNlZmQ0MjY1NDM2ZTIzMjEyYzJjMGNlM2RmMjhmZGNmNDA3MWI2MGVhZjViNiJ9\"}', NULL, '2020-12-03 21:45:13', '2020-12-03 21:45:13'),
('6dde93d9-847e-4f78-bcb3-d446e168a42e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImV0RWRqbGd4VHJmamVmZnlnb3BHRVE9PSIsInZhbHVlIjoibXhMRitBRnFZYXB0NUZpcVNhb0RHUT09IiwibWFjIjoiMzVmNmUyNGRlYTczY2QwYTEwYjAzMThmOWRmMzg1YjBkYzQwMWJkYTI1ODk4ODA2MmVkYjk4YzdhZDVmMGEzZiJ9\"}', NULL, '2020-12-03 18:11:05', '2020-12-03 18:11:05'),
('6fa12e33-e14c-4210-bb82-c73fb6a02100', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Dane Shunno-\\u09a1\\u09be\\u09a8\\u09c7 \\u09b6\\u09c2\\u09a8\\u09cd\\u09af<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InJyNmpncDUwR3UrUXg3VkxxQWx3TkE9PSIsInZhbHVlIjoiZG9ZTmc0NWN2T1d2NWphSVZ4THdmdz09IiwibWFjIjoiYmY4OGExYjMwNmM4Yjc2MzgyZjk0N2EyMjY1MWMxMjU5MDk1ZjM3ZWVmNmQ4YWU0NDJkYTMzMzdkM2Q1ZjVjMCJ9\"}', NULL, '2020-12-12 19:08:52', '2020-12-12 19:08:52'),
('6fb115a8-7e39-4b0e-8f29-3f2858a8c49c', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Mim\'s Exclusive Wearhouse<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IngyRGFvZnVMb0EvYTZuUzdLbk9UMFE9PSIsInZhbHVlIjoiU1RjQ0Zta3V6TjBSb2t1SUZEUk0vdz09IiwibWFjIjoiNjViMWU1MmFlMGU1MWE4Mjk2MWE5N2IyMmUzZDY5MzRmOTAxMjZhMGUyZDgxZTUyYThiNDcxM2RkN2QwZmI5ZCJ9\"}', NULL, '2020-12-07 19:21:21', '2020-12-07 19:21:21'),
('71bedce1-86b7-4805-8a10-66b7b63eee4a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkVtZDlwQWVzKzhkYjVEV0ZQMldmMnc9PSIsInZhbHVlIjoicGVGb1FmMFJxb3VYWGx5RGYxamFqZz09IiwibWFjIjoiOWY3OTVjYWRiNWI0OTQ3YmU4ZTcwOGQ0OGNhYjgzYjRiNjU1OTRhMjA3MjQ2YjZmODU1OWEwOWI5NjhkNGFlNiJ9\"}', NULL, '2020-12-14 21:48:45', '2020-12-14 21:48:45'),
('724b3831-b574-4ced-b44c-3e924f8d9c7a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlJnMTlFNlZMT29WYnRTWFF3ZExpV3c9PSIsInZhbHVlIjoiWHFMS3ZCTkNtRmJtYnpZWFBxOC9Fdz09IiwibWFjIjoiY2YyNjEwODAwN2U2MWQ1MGMyNjViYmNlYTFjYTc1MWYzZTI0ZTk1ZWQ3NzE2MDA5YzZkNjA2MWVkNDA2MTE4YSJ9\"}', '2020-12-03 02:01:24', '2020-12-02 21:31:28', '2020-12-03 02:01:24'),
('734f314a-0858-4817-8538-b5862852bdf0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ijl4eU13c0Fhd2wva0oxZFlkUklRdkE9PSIsInZhbHVlIjoiZHROMnU5Uk96eU5yWm9sYnNzMEZ5QT09IiwibWFjIjoiMjA3N2FkYWJkYzdjNzhjNzBiMTI4ODVlNzlhMjYyODRlZmM5YjY2YTE2NDcyMTdlZjI3ZDcwOGRiMGE5OTEyYSJ9\"}', NULL, '2020-12-03 20:03:25', '2020-12-03 20:03:25'),
('746df051-3ced-48c2-860a-3a0cadfc656f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im4yTGlKbDRoRGoxeHptM29yMmdWOFE9PSIsInZhbHVlIjoiU2xDV2luZ20wYjNtYlRyQ1FKY3RSQT09IiwibWFjIjoiNDRkMGYxYjVjYTI5OGU4NzMzZThlYzgxN2UwN2MxMjM0MDhiYTU0M2NjYjlkYjkzNTc3YWQwNGQ2ZmZiNmI3YSJ9\"}', NULL, '2020-12-04 21:58:51', '2020-12-04 21:58:51'),
('74872c92-33f2-4af3-8509-b1617fea65ca', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InNlR0hOQ29ZVjRRdGdZaGlYYlVTNlE9PSIsInZhbHVlIjoiZE1EcU9wV2ViWERwWGpOQnA0UnZYUT09IiwibWFjIjoiNjlkYTM1OTYxM2NkZGIyM2IzODQ3ZTQ4NTRjZTU3OGY1ZmE2NTA2ZTY3NmZiNzFiNzgxODY2NmE3YWE2NzlkYyJ9\"}', '2020-12-04 23:05:06', '2020-12-03 21:05:22', '2020-12-04 23:05:06'),
('75b9c704-ec52-4c6a-932a-b3809cb01495', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ild3MWcrZUplaUkyZ3pBR1V1U3gySmc9PSIsInZhbHVlIjoicEloU0Z5OVZxUlIvQy9wQ01valZydz09IiwibWFjIjoiM2I0OTRjZDUwZmVhMzRlYzU3Yzc4MzIxMjY2ZWI3Yjc1YjllODMzNmQzODE5NjY2ZGEwZDU0NTc2M2IzOWQxMCJ9\"}', NULL, '2020-12-09 20:17:53', '2020-12-09 20:17:53'),
('75cccb30-b56c-48d4-a879-1084233e2133', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik4yZ0RTelN2UmEzU2RBVHBJMi9SekE9PSIsInZhbHVlIjoiL1JxQ3lSait4QWI1aTFEWWVvWGIyUT09IiwibWFjIjoiNDE1ODNkZmQyZTk4YjY1MzlmMzhjYTI2ODJlZDVlOTRlMTY4OWVkZGU1OWE4ZDFlNzgyY2Q4MzkzYWY2MjI5NiJ9\"}', '2020-12-04 23:04:14', '2020-12-04 21:58:51', '2020-12-04 23:04:14'),
('75e36a0b-bca9-4836-92ba-099cb56605ee', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImVFZHQ1Ni9kTVovNUJHM1h5UWVPWUE9PSIsInZhbHVlIjoiaUp3akt4M3BGYXdKMlV5MlZhUGw2dz09IiwibWFjIjoiN2YyMWI0ZGViOTcxNTU2ODEzMTZhYzMyMjFiMmMzODFkZGI0NjUwMGZkN2IxMGZhZGFlYzcyZDkyYTVlMzA5ZiJ9\"}', '2020-12-03 02:01:08', '2020-12-02 21:57:19', '2020-12-03 02:01:08'),
('76343311-a3ec-429a-affe-683a5214f656', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InpTeGlyZnNxdmdLN21wNFVCY1RpZ0E9PSIsInZhbHVlIjoidE4vQk1GSnJqQ3dXNHNLVDFKZk5Odz09IiwibWFjIjoiNzZlNjczNGQzZGFkMmQ1ZmY1OWI1YzcwMTE0YTc3OTlhYjc5ZDg4OTdlNjAxNmVmMDE3MzI3M2U4YTQ2NWVlZSJ9\"}', NULL, '2020-12-03 17:52:03', '2020-12-03 17:52:03'),
('76505b1a-ec13-4cb7-be4b-aa86eb389c5b', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImFONkRoZW1NZWFjT05qb2MzcWJqYkE9PSIsInZhbHVlIjoiZkNpcnRuZ25FVHRBb0QraWVDSDNOZz09IiwibWFjIjoiMzQ1MTA5ZjQ0NTk3ODlmODk0MTQ3NGU3ZTI2MzVkYzYxNzIzY2NiY2IzNTM3YWUyN2ExMzg2ZjM0ODk0MDA0OSJ9\"}', NULL, '2020-12-03 17:56:53', '2020-12-03 17:56:53'),
('77385bf9-84a0-4e93-a9bc-cba55268e62d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ikc5b1dqZ0NwZzJnSlB4SEEzb0xZMXc9PSIsInZhbHVlIjoiK3RkYlY3LzNsTTZ6M1RpS3lMZzNXQT09IiwibWFjIjoiNTVlNGJiZjY3YTkzMjMyODI0YjhkYjExOGJmZGY3YzAyZjIwYTAyMmNlNWU4N2YwZGNiMWZkYTk3ZWI0MDFjZCJ9\"}', '2020-12-03 02:00:56', '2020-12-02 22:12:09', '2020-12-03 02:00:56'),
('77eea10c-41cc-439c-b833-e9c226190674', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Mim\'s Exclusive Wearhouse<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InFhOHBacFo0bjVrK2dBblFoQWZ6Vnc9PSIsInZhbHVlIjoiUzFiUzJEWWlGbzN1aU8yOU1iam1ldz09IiwibWFjIjoiNjA2NTEzYjhhNTg3YzM4MmFhMzRkMjlhZDJkZmIxNDNiYmFjOWQ2ODM3MWJlNTk0YWZjYzUwYjMwZDQyNzA5ZCJ9\"}', NULL, '2020-12-07 19:21:21', '2020-12-07 19:21:21'),
('78f30745-b096-496d-adc1-77e00fdef5cb', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Trinoyoni : \\u09a4\\u09cd\\u09b0\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkdzOGRycFVMVmZ4bWs0RFZxazdVS2c9PSIsInZhbHVlIjoiQ2Exb21jV0VSQTRvMWNRaVZXV200UT09IiwibWFjIjoiMzIyZjI1ZDZhZWZkYzQ5NzQ3ZGJhMjNhMWE1ZjRjM2E2NjY0M2IxYmZlNjVjZDJjNjRhMTExMzRhMWM2MTYwYyJ9\"}', NULL, '2020-12-14 18:53:15', '2020-12-14 18:53:15'),
('7aa13c3e-aa67-4929-8427-ae92e88c4be9', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IklnOEpGM3FqdU5iMkFBZ3BqWDE1a1E9PSIsInZhbHVlIjoiU3BJTUpuWWtNaktBY2VoVGEzNmlXdz09IiwibWFjIjoiNWQ2NTYzZGM5NmE1MmQ1ZjIzODU2ZWM4YTkwOGMxM2QzOWE1ZjM5M2JkODY2Y2NlZDllNDlkYmY2MzE5MzRkMSJ9\"}', '2020-12-01 20:29:06', '2020-12-01 20:18:56', '2020-12-01 20:29:06'),
('7b42fed6-1b17-4c72-abe9-419ae4d67497', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Debii-\\u09a6\\u09c7\\u09ac\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkxqNUxBYTJlSHNrSUdSTVdTOG5hdGc9PSIsInZhbHVlIjoiVFZzNTdKelVMWW16RTM2RHJ5ZjRjZz09IiwibWFjIjoiNmRmMDJjOGFlNDhlMzljNjFhZDIyYzE4Y2E4NTVlNjliYjg1MGFjZGU0Y2FiMDhlODJkMjkzYjEwM2RjOTVlNiJ9\"}', NULL, '2020-12-08 21:15:47', '2020-12-08 21:15:47'),
('7c470993-b2cb-4fc0-a135-f3b086595b33', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Suronjona : \\u09b8\\u09c1\\u09b0\\u099e\\u09cd\\u099c\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Suronjona Package<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InVGc3g0Vy9OYjBsdUF1K25taURzcGc9PSIsInZhbHVlIjoiWjFVY1lnVDVsUmQvUG0zTldsZzlyUT09IiwibWFjIjoiYmU5NDFkOTdhYTAyYmYwYTcxODRlOGUwY2IwNDc5YWVjY2IzMDUyYWFhNTdjM2Y0ODMxNmJmOGQzNjE0OTJiZCJ9\"}', NULL, '2020-12-09 19:52:56', '2020-12-09 19:52:56'),
('7c820995-60cc-4971-b7ab-14b9092565ac', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkhVZUtUZVhyWFloM0JZVWdEbVg2Y0E9PSIsInZhbHVlIjoiQ242UnAvelE1RjNEc0dOQWFKZ1A1dz09IiwibWFjIjoiNWY4ZmRlNmIwNDdiOWZhZjQ1OGY4YTY3ZmNlODI3MzM5NTE5ZDIxOTkyMzU0MGIxY2IzMWNmZTE3YjNmNmQxMyJ9\"}', '2020-12-03 02:01:36', '2020-12-02 20:35:26', '2020-12-03 02:01:36'),
('7f280565-2c2a-4494-acce-fb6f41853c1d', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkJLZmI4ZDRzTm1GU1B2em5Rc0Y5b2c9PSIsInZhbHVlIjoiVkZ6YkdwblFDZEhUWHZ5Y0lXZXZWQT09IiwibWFjIjoiN2IzZTYyNzI4MGU3MTNhYjViNzQzYjUyZmQyM2ZjYjYzMzZlNGQxNmMzOGVlNmY3MWEzZjY0NzgwOTJmNjE0MSJ9\"}', NULL, '2020-12-09 18:43:23', '2020-12-09 18:43:23'),
('80517c14-e1c9-4d60-99d9-b918e8e0cae4', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImNlNGtsV1FRUGFNK3BNbVh0ZWpYNHc9PSIsInZhbHVlIjoidUpHS3hTcmlBVVdlaTFVY1NwUjdkQT09IiwibWFjIjoiZTA3NjRlMmRiMjJhNWFkNWEyZTg1NjRkZmQ3ZTk1OTIxOWM1MTljMjM2M2M4MDE4YmFlMjJmMzQxNmYwMzdkNyJ9\"}', NULL, '2020-12-07 18:00:52', '2020-12-07 18:00:52'),
('8061d8d2-aa17-4921-b85e-1ee6834cc9b6', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b6\\u09c8\\u09b2\\u09bf By \\u09b8\\u09be\\u09ae\\u09bf\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImNIOHJVS3pPdEZVYjNlaW1SeDV3Y3c9PSIsInZhbHVlIjoiYmorbWw3eVl4ZklqOTBIZW9hMFVLZz09IiwibWFjIjoiYjIxYzdmODQzOWI1YWMzMzFkZGUxOTRhNTQ4YTBlYjBmZTg2NDRlYzI3ZWUyYmI2NWYwOTQxMzE0NmVlMzg1MyJ9\"}', NULL, '2020-12-15 19:39:05', '2020-12-15 19:39:05'),
('80f19252-4131-4b5e-b261-a43b2a2f056e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImJ5OU9KSGVTRDl1M3lJMG9CYVVlUnc9PSIsInZhbHVlIjoibDFHSlpBMWpYUmtZQjl0QWluMjYyUT09IiwibWFjIjoiODUwOTQ5ZWIyNzQ3NjljMTA4ZGZiNzNiYWYxY2M4ZjNlOWQyNjczZThlMGNiOGJjYTQ4ODU3Yjg2MjliNmFlMyJ9\"}', NULL, '2020-12-03 17:45:27', '2020-12-03 17:45:27'),
('814e192a-07a6-4f8c-8b78-aa782fb9cf34', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlVnOGNSaEtEWXVGME5rVmJBK0RCSFE9PSIsInZhbHVlIjoiT2RrckFLalhYVWJxdVF3UUFSRWI2Zz09IiwibWFjIjoiYWY4MTFkZmVkZTdjOTE5NjhmMjJhODViNTk3M2RjZTJjZmExMjNhOWE5MjRhNWFjNmIxMzI3NzM4YWVhMGQ0ZSJ9\"}', NULL, '2020-12-06 19:43:55', '2020-12-06 19:43:55'),
('8188ea3b-9c97-4fa8-b381-6ecea1991b33', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImVMNU1PQmQ4cjhHMkoxSVpqa2FLdXc9PSIsInZhbHVlIjoiUllsbjNCNWpia1AreWtXVUl0YmNLdz09IiwibWFjIjoiMDMxZWExYzQ1MmMzZDBiNDg0NjFhYjk3NzJiYmJmYWI4ODI0MzkzMjM0NjkxMmEzNTFlNWZlNjJlNjhmZDE1ZSJ9\"}', NULL, '2020-12-13 18:31:18', '2020-12-13 18:31:18'),
('827651e0-57e9-4bd8-9f13-359844d3e2fc', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IldaUGdRN3I2MFhLTm5aTWtPQ2ZwT0E9PSIsInZhbHVlIjoiQUlCNlBaM2o1VEtkalZkZEVTUTJSUT09IiwibWFjIjoiM2IwODQ3YzU0MGZmZDA1MWVkMWNlMDg4MTc5MjI3MTIxYjg4MjYzMTM3Mzc1MzFjMzkyZDY0OGI0YjYwZjljOCJ9\"}', '2020-12-01 20:32:22', '2020-12-01 20:03:31', '2020-12-01 20:32:22'),
('842aebee-2ff7-48b5-ad2e-58c5cc2502df', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkE5QlhzWmt1WEZkRHB1aExSSWloQXc9PSIsInZhbHVlIjoiS0dWRFZzdHZzK2lsYXpveFdyWVc4QT09IiwibWFjIjoiZjg2YzFjYTUzNDY2NmM4OTJkNjBmMDlhYTk2ZjE3MGRhMzM1Zjc3ODdiM2RjMzA2NzVlODU4NWY1MjBlNDU0MiJ9\"}', NULL, '2020-12-02 21:33:54', '2020-12-02 21:33:54'),
('84dbc364-3c17-4645-a769-889d925369c0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InRLUjZiZEM5dlJXaFJHaU9xMVhjSUE9PSIsInZhbHVlIjoiSHdralN4clJvTnVoL0VsbFpMeHV2UT09IiwibWFjIjoiNjViOTc1ZGY3NjJjZTE5MDBhYjJiNjM5YzAwZjZiZThiMTdhNjc3ZjgyMTRjMzM5MjliODlhMjUzYzMzMjg0MyJ9\"}', NULL, '2020-12-15 18:06:57', '2020-12-15 18:06:57'),
('85a1136d-142f-4c7a-8ee4-47f82b619290', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImoyK3hweHRnd2h5ODBqZDVKMTRsRFE9PSIsInZhbHVlIjoiZUREWFhvMGJvWlV1cmJ4ZmVpQzQzdz09IiwibWFjIjoiNWZjNTQ5YzJkZWZkNWQ0YzdlOWY3YjgwMDM5ZTk3ZDUwNWIwNjM2ZjRkNjJhZTYxNGRiOWY3MDhmOTY2ZjZhMiJ9\"}', NULL, '2020-12-03 20:18:44', '2020-12-03 20:18:44'),
('86596f2a-839e-4c21-b775-49539e74ee2a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImtLWmxOUDA4aU9MODVhYjdsS21pWHc9PSIsInZhbHVlIjoiMExuRWR4RUJVODVDSHBkUkhXOTNhQT09IiwibWFjIjoiYTlhYWFhYTFkNjU0Y2M2ZTU1YTEzZGFjY2RhZGQyNjVjZmIzMzFjNmFjYjRjN2E3MWY5ZTdjOWUzNDUyNGU2NSJ9\"}', NULL, '2020-12-15 17:35:12', '2020-12-15 17:35:12'),
('874bfd6d-26be-4612-b0d0-7bfbfc8f70f3', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkZtanU1d1o3clB4U1FrNG1jYWNSeVE9PSIsInZhbHVlIjoiWksyWUhGTnZIV3dxeUo2M2REK1BhUT09IiwibWFjIjoiMzcxODZmMTUxMjFiYzdlMDZlZTkzYWY4NWEwODQ1YWY4YzAwZDg2YmFmM2FmNmE2MTFiN2JiYjYzNDY4YThjZCJ9\"}', NULL, '2020-12-06 19:38:54', '2020-12-06 19:38:54'),
('8782986c-f30c-42b1-b4f2-94c109f34a9f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZVaWFUZnU5SWQ4ZnNtVVRrWmVna0E9PSIsInZhbHVlIjoiSkYwUnRWSzZvcHlsQXBKTDFsa2gzUT09IiwibWFjIjoiYzJlZmYwNTQ4ODkzNWIwZTVkZjI0NDE3NmZhOWUzOWI0YWRkNDdhMWZhMDEzMTA5YzUwN2E3NTJiNWIyMGUyYSJ9\"}', NULL, '2020-12-03 19:23:31', '2020-12-03 19:23:31'),
('87e3ae6b-e46a-4cab-ada2-ab3a1e8a6de1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InF3alZ5WmpiZlVjbUJNekdVemtBNVE9PSIsInZhbHVlIjoiZXA0OVo1SGhONGhZeUpLSnkzdXVNdz09IiwibWFjIjoiNWYwNWRlZTRiZjZjMmViZWVkMDI4ZGE4Njk5ZWJmNTE5MjI3ZTNiMWM1YzI3NzE2OWEyZTdkOWY5YTI3MzJkMSJ9\"}', NULL, '2020-12-03 19:17:54', '2020-12-03 19:17:54'),
('8999ab99-4bb3-44c0-b36c-ac6bef4b4f89', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IktiOUNjZzVOdmZaaENFY3dsMmpaQ1E9PSIsInZhbHVlIjoiaVM3Unk4Zkg0WXplT2VYNzF3YWRxQT09IiwibWFjIjoiMjJlMWM1NGRiNmVlODM1NGFkM2YxYjMzNjI4NWQyMTg0ODViM2E5MGYyYzQ5OGU3MGE4OTQ0YTdmYTIzZmYzOCJ9\"}', NULL, '2020-12-03 20:59:34', '2020-12-03 20:59:34'),
('8a685b5b-922a-46fa-bd1b-1c6aab71892a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjJlUklhcUNPNTZVK0xyVENFbDJSWWc9PSIsInZhbHVlIjoiQTQzdzJkUzhHU1VLcTBLUDNGVDY4QT09IiwibWFjIjoiODg0ZmM4MzFjZTQ0YzgzOWNlMDU5NDU5MGQ1ZjFlMTg2ZTA0N2NhNGY3NmIyNDhkZjdhMTJkMDNiM2NhN2QwMyJ9\"}', NULL, '2020-12-07 17:52:34', '2020-12-07 17:52:34'),
('8abbf05e-8350-4e20-9542-ad00aa5b298f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik43dmw5bkswaHo2cTlxMUJWYzlVTFE9PSIsInZhbHVlIjoiUmFiU2ZoWUZzb3FGUEh5TE9HOXpOQT09IiwibWFjIjoiZTcxNWFjMmZhZTA2ZWRhN2U1MWVmNjQwZDZiNjcwMzI1MGYxMjBiNzJhYjU2MDIyZDgzYmVkN2IyMTgyOTExYyJ9\"}', NULL, '2020-12-07 17:58:38', '2020-12-07 17:58:38'),
('8c35ff2f-fc5d-46df-9045-8d27c22b4708', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IndNZTNDWWwxS3h0SzhzY3UxT24xcFE9PSIsInZhbHVlIjoiTU9PS0FKaGRnQ1A1Q3NiUzA3U3FaQT09IiwibWFjIjoiZjJhNGE5YmU0YjM3ZDhlMGQwZWRkZDkwOWMyYjA3Zjk1YTYxMGJmMzI0YzI3ZjNjM2M3YTM3NjFmN2RkOGQ0ZCJ9\"}', NULL, '2020-12-03 19:23:31', '2020-12-03 19:23:31'),
('8d174f87-eb4d-4a03-b3bb-b89feec330c6', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ilg1bUI1Tkh3MHhsTktiRjJ5azYxekE9PSIsInZhbHVlIjoiZDhJUVdsMXk3am52cWd4NVB6Ky9mZz09IiwibWFjIjoiODE5NWY0NTRhYzdjNDRjZTAyNGNkZmVhMTBmMzAzZTA5ZmU5Nzc5NzkzODU5NGMxMjg1ZjUxNmM3ODdjYzI5NiJ9\"}', NULL, '2020-12-14 21:33:52', '2020-12-14 21:33:52'),
('8d4ba92c-1b6a-4d05-9068-74514e40e96c', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlR4cTlySnpMUGZaUVU3eFFJN2lNWEE9PSIsInZhbHVlIjoiRDlKSnRBdmF2azVyWm9ubXcvUEh6UT09IiwibWFjIjoiMTBmNDhkZDZjOTJkNDg5MjY0YmJjYmJlNTk4NDhlYmU2YWE0ZGRiMDg3MmE0YTZhYmE2NzRjMDk2NjkwNDViNyJ9\"}', NULL, '2020-12-09 18:46:15', '2020-12-09 18:46:15'),
('8d5bee75-cf66-4094-936b-c2bb022bdeee', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InQ5M2RvQmFIYzhKaVh2VmxQVVZCdHc9PSIsInZhbHVlIjoiRWJrbGxQN2ZDMU1seTBWc1hGZmZpUT09IiwibWFjIjoiN2EzNWZlM2NlNjU5NjRhM2I3ZTMxYzUwMTZhZGJlZGZiMzg2MGUxYmVhNGVjYzA2MzM2OGUzMTc1NDYyNzUxMCJ9\"}', NULL, '2020-12-03 18:14:14', '2020-12-03 18:14:14'),
('8d8f3688-976a-449a-ac31-4108b4983c69', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlREVSs5ei8yekQzQVNEMGswLzFwa0E9PSIsInZhbHVlIjoidUJsMVIvSXFicHB3VDFwNzNBNEpFQT09IiwibWFjIjoiZGE5M2E2MjAwY2Q0ZTlkNGE2Mzg5ZjIwNWI5ODg3Y2Q2YjZhMjQxYTRjM2ZhN2NhNGZmMWQ5YWEwN2M0Y2JkNyJ9\"}', NULL, '2020-12-15 17:34:09', '2020-12-15 17:34:09'),
('8e902a21-1570-4e24-a2d9-eb24ef241b03', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjFFY1A0ZnZWUm1lN2paWjlsRVIxNFE9PSIsInZhbHVlIjoiOHNTaisxYzExdnpjS1FkRFBYc3F6Zz09IiwibWFjIjoiMmY5YWUzNzMyNDZlYmI2N2NhMjRiOTY5MzJiYmZmY2RlYzVhMDg5MGYxMDQ1NzBhYjk1NjQxNjBjNzRiZTFjMiJ9\"}', NULL, '2020-12-13 18:42:29', '2020-12-13 18:42:29'),
('90239dc3-92c9-4f68-8241-d6734c18edb1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im4zRklTUUl5ZkJwdHBWdk4xS0Ivb2c9PSIsInZhbHVlIjoiSUF4VDRrdUdScGdscUE3ZkxKbWJKUT09IiwibWFjIjoiZDIyNDk2OTdiMGNmMDkwNWFiODM1ZWJkN2E2ZjQ4MGJiOTRiODM1NzQ0ZDg3OTczNDMwMWQwYTZiYWE0MGRhNCJ9\"}', NULL, '2020-12-03 21:05:22', '2020-12-03 21:05:22'),
('9196dc48-6ae4-4d37-8b23-a8d2ab0e1948', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u0985\\u09b2\\u0995\\u09be\\u09a8\\u09a8\\u09cd\\u09a6\\u09be -Alokanonda<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjlXM1lqMVpWRWxsTVdvdjJIeUFuNmc9PSIsInZhbHVlIjoiNU96eFhaWWNJeHYzcytMTTNOM1N5dz09IiwibWFjIjoiZmQwZjdkNDAwNTQxYWRiZDRjMDQ0ODBlNjliZmM2ZTZkODhjNDY2MWViOTc3YjY4OWM3YzMxMmMyZWEyMjA2MCJ9\"}', NULL, '2020-12-03 18:41:39', '2020-12-03 18:41:39');
INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('91df6892-a331-4d96-b430-3eefc8dc72e0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InloSDB6U1EwVnhGbU5CWTlCb2JXVkE9PSIsInZhbHVlIjoidUx5THVrekpyKzVPZjdsOGhkdjNNQT09IiwibWFjIjoiMDc3OTdhMWZmZThlZWRkNjQ5YzM1NmYwMDMzZmVlNjZmYzM2ODlmYWNmNTAzM2M1OTQ4YmFkZTdkMTc2MDQ1MiJ9\"}', NULL, '2020-12-03 21:49:14', '2020-12-03 21:49:14'),
('926ba91f-d000-476e-b821-b35a28b8549f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik5xNldhN2lvYmFKc2NVQm4wTE1pb3c9PSIsInZhbHVlIjoiaVhDaEZRblJ3aERLaTJvbHpwZlpmdz09IiwibWFjIjoiYTI0ZTY5MmU2NWQ2ZjdkOTMyNDc3OGQ0MjM0YTNiMGI2MmZiMWNjMjRjNWYwMjU4YzIyYTU0OTBiZGY3ZTdiYiJ9\"}', NULL, '2020-12-05 05:13:10', '2020-12-05 05:13:10'),
('942022c4-78be-47b3-b83d-c1ea05a786b7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkR5SDVONWV6TkdUMXNwMERhUlMwTVE9PSIsInZhbHVlIjoiVG55aktWb0w0QWlMNmdha2d5OTRQQT09IiwibWFjIjoiMThiMmE3MmEwNjdjNmI3YTlmZDU5OTE0NzZiY2I3NzM3ZTM1NDUyZmYzMDE1Yjc0NjFlYjBiOWZhNzliZDg0OCJ9\"}', NULL, '2020-12-03 19:43:04', '2020-12-03 19:43:04'),
('948c7525-fdc6-4da8-b377-850945d4d963', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IktTa1RtQ1BCR2g0WTlTODB3VGQyeFE9PSIsInZhbHVlIjoiWUNuVExwVlVOMEhnK29qYm1qNDNXZz09IiwibWFjIjoiZjBjZjU3OGUyNmVlNTFhOTZjODFhNWJiNDNjMjQzMzNkMThkZjk5YmY0NzQ4YTc3MWQwNTVjZWRkYzY0ZGFkNyJ9\"}', NULL, '2020-12-05 06:58:50', '2020-12-05 06:58:50'),
('95963f00-d787-46ce-a5dd-c4f6c813734d', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImRIcVFiMk9HWXd2T09DeFNwYUVzSVE9PSIsInZhbHVlIjoiVXVDMFEzQVlnakkzL2RxNGNUTXkydz09IiwibWFjIjoiOGY3YzE4YzNhNzEwZTZlNWFiY2U1MDg4ODY5YTFmZGZiMzFhN2JkZTEyZjlkYjM3NjVhYjliZmFlZDFkMGZmOCJ9\"}', NULL, '2020-12-15 18:29:53', '2020-12-15 18:29:53'),
('97bdb20f-de3e-4690-b52b-d77be564df27', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ijl4QTRYNlUrLzFzMjRPdm9WdjFyYVE9PSIsInZhbHVlIjoiZHI1V0duWkpQWVJlM0ZweFZrempGUT09IiwibWFjIjoiM2IzODI4YjY1ZTMzNDZkMTNiM2U1N2JhNDU3ODYxNzFmODAxOGFmNTQxNTFkNzM3OGQwYTQzMTAxZDVlMjc5NSJ9\"}', NULL, '2020-12-05 05:13:10', '2020-12-05 05:13:10'),
('98807c1f-42c6-4d0c-8fe1-408a213025f0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ikp6WGNsbGY2SmQwTnZFNCtGNTFuWHc9PSIsInZhbHVlIjoiS3hIYUdDQW1YYjNVQzlXY01IZ3U4QT09IiwibWFjIjoiYmQ0MTM1ZmRlZTM4ZmQ0ZGJjYTFkMDNiZmU4YmUwOWExMGY1YzU3OWE3NzVjNTJjYzE2OWIyOGJkNDA1ZDU5OCJ9\"}', '2020-12-05 10:53:48', '2020-12-05 07:04:54', '2020-12-05 10:53:48'),
('98d0adce-126d-4b31-93bd-84aa9dbaf84b', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImsyYmplWDlZMGNUcGJhS2hmaE9KQnc9PSIsInZhbHVlIjoib2VrWTZ2d0x4Znlza2lLaTNvS0Nndz09IiwibWFjIjoiODczMGZkYThjYWRkMjRlNDJlYjljYjRjMjY2OTYyZjk4ZTZlMTgwMTBlYTBkMTgzNjQ5ODMyMWNlYzA1OGRhMCJ9\"}', '2020-12-02 07:46:37', '2020-12-01 21:41:04', '2020-12-02 07:46:37'),
('99aedcab-74d4-4da6-82cf-9959e8eea20e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Crafty Fire<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im9Zc1kvdXptTlVIUVVkV2h4UGpVeEE9PSIsInZhbHVlIjoiWU1YbnlLWVFka0pVOVlPa3QyZ0JWQT09IiwibWFjIjoiZTM4YmZlMzViOTU5MmI5ODZkMDVmZDU2MDIxMDk1MTFhYzJjZDZjMjIxZTQ1NmI1OGYyZDk3MzdjMGY1YjVmOCJ9\"}', NULL, '2020-12-14 21:22:10', '2020-12-14 21:22:10'),
('99c54ec3-c4b2-4cb3-944f-6de7986d2af7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im13Z3hPZnMwbWpLdzlGQlQ2TWZSYXc9PSIsInZhbHVlIjoiN1JoT1hLc0tIZjByTjF2ZTZkUzd6QT09IiwibWFjIjoiNDJjNjBhY2Q0MThlYjU5MmM0MTdjZjNkNzM1NWM2ZDRhNjNiYzMyYjdmODRjNzI3YjAzNmY2YTBmNmQxZWIxNiJ9\"}', '2020-12-02 07:46:44', '2020-12-01 21:02:38', '2020-12-02 07:46:44'),
('9b22fb38-e9f5-4a4d-a8a8-dd7dc00ae20a', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Debii-\\u09a6\\u09c7\\u09ac\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjZpZHZaZHdDTjRlNUI5WWlUb2xUUlE9PSIsInZhbHVlIjoiOWpJSFAyazNFSVhwTXlWTFA5QkJYdz09IiwibWFjIjoiNmU1YmM5MGM1N2JhYTAyODY2ZjNkMzliMTAxY2MyMDRhNDVkMzUwMWYwYjU3YjQ2Mzk1Mzg0YmIxN2JlNDEyMSJ9\"}', NULL, '2020-12-08 21:15:47', '2020-12-08 21:15:47'),
('9c101394-3664-4043-89e2-4fb685365249', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InhJMXphU3BTNkdyVmR2T0NFZHBNYXc9PSIsInZhbHVlIjoiRElHOEd4VGRjeGwvM3JaVFFLUHFHdz09IiwibWFjIjoiM2Q2YjY4YjU3MmY0N2RlZDFhNzk2ZDEzOTdmYzI5MDRlODE3ZjgzYWQ5YmVhYzJjNmE2NTk3ODYzODRhNDM2ZiJ9\"}', '2020-12-03 02:00:50', '2020-12-02 22:14:31', '2020-12-03 02:00:50'),
('9cee0c42-6612-4236-8498-0e689068bf21', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik5vL1IzYjh4UzM2bUpEQllxamhFOVE9PSIsInZhbHVlIjoieTV0K09KM3lnaXVlQ2l4eDJVOVh5QT09IiwibWFjIjoiOGM5ZjNiNWNiMThkMjI4M2M5ZmM5YjAzZTVmOTlmMjU3MGMwYjU4MTgwNTU5NDAwNDZjMjIyMGNiNDU2YWIyNiJ9\"}', NULL, '2020-12-05 05:10:45', '2020-12-05 05:10:45'),
('9d43dc49-3c3f-4641-99f3-fe8cfa7d963e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InppYnBybU9RdG5veXprbUh0QlR3T1E9PSIsInZhbHVlIjoieXkycWRkOGdURU1Ld0dER28rTWdQZz09IiwibWFjIjoiMWNkNTYwZGIxNzA5ODJjZjdiNWE2ZGJhNTg4YzZhNzUzNDcxZDg3ZWNiMDUwYWZmYjFjMmU2ODFjZmY2NmIzNSJ9\"}', NULL, '2020-12-03 20:03:25', '2020-12-03 20:03:25'),
('a11d9d99-d186-4d4a-9dcd-6bfbfb1a05ed', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik5NVVVpdUdSLzNMR0d3S2J5azgvZVE9PSIsInZhbHVlIjoiK2p3Lzd4VkN1NWlDU0FYY0NUYTdUZz09IiwibWFjIjoiNjI1YzMxOWZiZmI1OTkyOWQ1ZmFlZDgxNDIyNjc4NTRjNTQyYTQ2MGMzZGQ3YTJiM2I1YWZjMWY2NjAxNTk0YyJ9\"}', NULL, '2020-12-09 18:41:19', '2020-12-09 18:41:19'),
('a29bdb3a-4694-4404-b208-2434bda84860', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Art by Soumitro<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik1WQzZTazcvbFpOUWR5bjdKVUJrNWc9PSIsInZhbHVlIjoicUozU2xYMDFRQzJTWEk1ZkNjT3VsUT09IiwibWFjIjoiOTkxYTg0NzU5ZWYxYzdmNDg4MjVjZjIyMzVhNjhhZGU4YjU3ZTkwZDY1NzRlMmZlOWJkMmQ4MjZmZGQwZTRhYiJ9\"}', NULL, '2020-12-15 17:32:38', '2020-12-15 17:32:38'),
('a2a44d0d-dc54-403b-8076-589f79e2e8e5', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InB0S2UwaWF4RDJDcm9OckNQMUZmT0E9PSIsInZhbHVlIjoiSElxcWlIK2pFWXpYSnQvbFZ2Vko1QT09IiwibWFjIjoiZjBhMGY0MDg1ZTc5YzdmYTQ1MGYyZTU2ZjI4Y2QzZDJiY2Q1ZmFhZTY3YjQ0NDEyMWE1MDE3MmFlODdhYzcyNSJ9\"}', NULL, '2020-12-06 22:56:55', '2020-12-06 22:56:55'),
('a7bb118f-8473-4ed8-8bc7-751b9b5f4144', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Story of Stich<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ii9ZV3NZNWxaMmdmOHZRSDluZ1lacVE9PSIsInZhbHVlIjoiMFlkTmgvUnRJVGlMQXJEY2d2RHJQQT09IiwibWFjIjoiNGZmZDgxMjI2MDkxOTIxMTk1Y2Y3NjJiZGQwYThjZjFlZmVhYjI1Y2M5YmE3ZmMwMzE5YzU3MTM1NmEzNGYxMyJ9\"}', NULL, '2020-12-12 19:51:30', '2020-12-12 19:51:30'),
('a803f7cc-96b6-4ce7-91a1-fbf903d37a60', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IldSVlpmSGFGNzRJcXdqRzlxNVZJTGc9PSIsInZhbHVlIjoiM2YwR3NnUzZ0Qi9mV1NBY0NCVE5wZz09IiwibWFjIjoiOGE0N2VmMDkyZGU5MzkyMTUyNjEyOWJmOTg3Y2Q2ZTI1MDQ5MWM2MjlmNTVkMTVhNDhhZGE5ZjllZjlmZTI5YyJ9\"}', NULL, '2020-12-15 18:32:30', '2020-12-15 18:32:30'),
('a94aca94-4c3d-41d7-a21d-0065614c2d4c', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099f\\u09bf\\u09df\\u09be\\u09b0 \\u09b8\\u0996\\u09c7\\u09b0 \\u09a6\\u09cb\\u0995\\u09be\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImJJcjVsVGR0TnF5UEN6OVlmVWpvVnc9PSIsInZhbHVlIjoiSVBCZlZob3VXNUlFcXFjVDczblJVZz09IiwibWFjIjoiZjgzNmJhZGY3NDQxMDk1ZjAwOWVhOGRkN2Q5NGI4ZTI3YjFmNDA0NzNjNjhjNTFjZTZkYjliZjI3MzZiYjhkMiJ9\"}', NULL, '2020-12-13 18:03:08', '2020-12-13 18:03:08'),
('a978f2e4-ece2-4ce0-ae51-e47b272ccacb', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InMxMFk3NjJFOFR4dUVNOGJoczlzU1E9PSIsInZhbHVlIjoiMFY0MWs2TmVxQndtcDlGQUZ1c0wyQT09IiwibWFjIjoiNmExYWM5OTc3ODFhOGVlNzVmODA2Mjk0YWMzNjFiNzhhNDgyZGZkODQwZTllMmI3OWM1ZTc2ZTRkMjY4YjZmOCJ9\"}', NULL, '2020-12-03 20:59:34', '2020-12-03 20:59:34'),
('aa06d95d-f6a5-466a-bf57-449788cf5b58', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099a\\u09be\\u09b0\\u09be\\u0997\\u09be\\u099b - Caragach<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IllMRjJmVUcrVTZ6Y2JXTVRaKzlTZkE9PSIsInZhbHVlIjoiR0dLZVp6THZ1N3hWdnpkSCtxZDdEZz09IiwibWFjIjoiZjI2Y2ZlYWMyY2Q4MWQ0MzZjNzRkMjUxZDhjZTNkOTI4NmI5YmFmOTE4OTgzNGQ4NmZhOGRjYTQ2MDI4ZWM4ZCJ9\"}', NULL, '2020-12-08 18:58:51', '2020-12-08 18:58:51'),
('aa34333a-c712-4973-9419-880b045780fc', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImJsMFd0b3l2cm42WkRVZVlSeE1yeUE9PSIsInZhbHVlIjoiTmtCKzZaaHBWUlFnMzJ3VCtDdFo5QT09IiwibWFjIjoiZWZiNGEzMWNlZTU3OWRiMzNkY2IxYmQwNDA1YTkyNTk3MWM4MWEwMmNkNWFhZjdiZWQ2YWExOTMxNDdiODkxMSJ9\"}', NULL, '2020-12-03 20:52:07', '2020-12-03 20:52:07'),
('abd49f0f-b8eb-4c2f-8ef5-6ab3fbb4ec44', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Dane Shunno-\\u09a1\\u09be\\u09a8\\u09c7 \\u09b6\\u09c2\\u09a8\\u09cd\\u09af<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InZRbUdyZGpkbkJyaFJKZVNFUHkrTFE9PSIsInZhbHVlIjoidExYVkl0WndhS0t3UHp2VG9XSkVLZz09IiwibWFjIjoiZTg4ZTUxNzc5MDAyMTVhZWI3NWU4MDEwNjYyN2YyZWQxZDdiMzUxOGZjM2ZhYTFiZmU0ZDQyYjgzYjQzOGVlZiJ9\"}', NULL, '2020-12-12 19:08:52', '2020-12-12 19:08:52'),
('ad2f99be-efa6-4bdd-9b51-f6136f35b6d4', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099a\\u09be\\u09b0\\u09be\\u0997\\u09be\\u099b - Caragach<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InFxd1J5V3JUQXhTSS9tQ0NxdXlnWmc9PSIsInZhbHVlIjoicEFoK3FoaVcybEYxVm4wcWwyRWUyZz09IiwibWFjIjoiYzllZDNjYzYzOTRkYmEyNWZkOWU1MjZjOWUyZTE2ZTNjOTJjNzRmZDJjODAzNjQ0MWE4YzNkMTMyODcxMjIxYSJ9\"}', NULL, '2020-12-08 18:58:51', '2020-12-08 18:58:51'),
('ae45c561-bc33-4a3e-886e-5adcff13f923', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkZFc2pjVDBEaWk1dzJiOVZwZVRScmc9PSIsInZhbHVlIjoiY1hqZUpCRjh0SDZ1bjRZWEUyWGp1UT09IiwibWFjIjoiODYzZTQ5ZDhmOWE4NWM1OGMwZjVkM2I5OTQ0YjI4N2Y3MmIwOWFlNmI3MjJmZTgxZmU1OTNkODBkZWRmM2Q0OSJ9\"}', NULL, '2020-12-07 17:58:38', '2020-12-07 17:58:38'),
('af5b1bf9-d38a-4a29-af45-edff3317c5d9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InVYdHV0dWZXdFpLUXlzaGlla2tKdEE9PSIsInZhbHVlIjoicDdQOW1tVkJuQjhydFBRdVhza1lGUT09IiwibWFjIjoiN2E2MzM0MjRiY2Y0ZDZlYjY3Y2VmOTc4NmRmMDMwNzE3ZTk1YjEwNzI4ZGM2YTI3NDg1MjY1Y2Q2ODM1ODI2ZiJ9\"}', '2020-12-02 07:46:14', '2020-12-01 22:29:49', '2020-12-02 07:46:14'),
('af615d6f-f0be-4109-8633-5fd4a4e13f6e', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Glowing_splint<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6InEwNlc5WFBUOVJPeEZzaG4wcEp5ZXc9PSIsInZhbHVlIjoiNTY2RU00cS9aN1V2a2ZRTzNKaUJYUT09IiwibWFjIjoiYjAyMzIxMWJiNjg5MDU5OThlY2NiOTViNDM3OTFlZWZlZGNiNjk5ZmNiMjZjNzQ3MTA5MGJjMGUyYTY3NTQwNiJ9\"}', '2020-12-02 07:45:47', '2020-12-01 22:54:53', '2020-12-02 07:45:47'),
('afd16c1d-4882-4198-be14-ec69280fb618', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ii9MaDI3ckdvaHRZYVJOcnFNWFpRQWc9PSIsInZhbHVlIjoiTzNJQmk0Yk1tS2hHV1RXY2QvLzd3QT09IiwibWFjIjoiNjI4ZDg4YjZkZDVmNDIxMzdiZjlhM2E1ODE3ZjQ0OTQ3ODY2ZTgzOTIxNTcyOWU3NmQ3YzU4OWI5ZTZhODg4ZiJ9\"}', NULL, '2020-12-03 20:50:25', '2020-12-03 20:50:25'),
('b17297e6-78ab-4dd4-a706-e7ddec1784da', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik95K1FHS0FZbFlLYnpsVDkzazZBR2c9PSIsInZhbHVlIjoiMG0wK2JmdHRFenhNTzNkenVxOVJaZz09IiwibWFjIjoiOTk1ZDA3ODI1MTE5MTlhNjgzMWU2ZTdhZWVlMTc5YjkzNTA2ZjQzYzA5OGZjZGQxOGQxOGRmYWJjZWFjYjY0MCJ9\"}', NULL, '2020-12-15 19:31:54', '2020-12-15 19:31:54'),
('b18d11ef-e527-4dab-a8cc-7c77d5fdf41c', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ad\\u09c8\\u09b0\\u09ac\\u09c0\\u09a4\\u09be\\u09a8-Bhairabitan<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkNqTmRaZ3VHY0Q0NkFYUWZMZkFwTnc9PSIsInZhbHVlIjoiVzdMK1RDSHQxMjVlVDZXTFAzVmlEUT09IiwibWFjIjoiYmMyNDkwNDgzZDRkMjI4MzJjYTEyYWExOTA4ODg0Zjc4YzkyNzFmOGY0MTgyMjRhM2IyYzFiYTU5NzdlYzg4MyJ9\"}', NULL, '2020-12-03 18:15:53', '2020-12-03 18:15:53'),
('b1eb9146-0649-4170-9b83-9e116f375c24', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ad\\u09c8\\u09b0\\u09ac\\u09c0\\u09a4\\u09be\\u09a8-Bhairabitan<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkdDUmYxRkh0ai9iSjhxMlp2dU9Oc0E9PSIsInZhbHVlIjoiR0o0QmhTYlZSRDJERFBzSHhQVWhjQT09IiwibWFjIjoiYmQ4YTA2ODQwNzY1YmYyZDUzZWZjMDdhZDNjNWQ2NzY5YjY2YjlmZjVmNDQxNjE4MGFjMjhlYzdlMDY0MWMwNyJ9\"}', NULL, '2020-12-03 18:37:18', '2020-12-03 18:37:18'),
('b2ada6db-f4be-41f6-9e63-fd71f01c81dd', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09aa\\u09cd\\u09b0\\u09a4\\u09bf\\u09b7\\u09cd\\u09a0\\u09be \\u0995\\u09be\\u09b2\\u09c7\\u0995\\u09b6\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlpibHc3VTNsYzVCeG44TjlvUnVXcWc9PSIsInZhbHVlIjoiWHJlQ01saTJYSzhtRmN3N0dwaFYzUT09IiwibWFjIjoiZWU1N2ExZDA3ZDEzNDlhYzljYWUzMWJlOGZiOWMxNjFiOWY3MmM4NGZjMDk2M2M1NDljYjZlNTI5MzI4ZGZiMyJ9\"}', NULL, '2020-12-12 19:45:53', '2020-12-12 19:45:53'),
('b2d29248-b2b4-479a-8114-edad01493cb6', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im04STN5S2pwbWFCYTZicVlQdHJFWEE9PSIsInZhbHVlIjoiV3R1UUhpVFFrc28xLzZURXBMNm9XZz09IiwibWFjIjoiODE3ZTg0OTI3YzJkZjJjZWQxNjNkMjZhMTBiOWU2YzY3MWQ5NWVmYzE5YWM2YmEzNWZjZGM4ZjY5YmJhNzgxYiJ9\"}', NULL, '2020-12-09 19:16:52', '2020-12-09 19:16:52'),
('b2d38526-3f0e-4d59-9554-93bf1fe24f7e', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Jahan\'s Collections<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IndoMUJCWUJTTngxTFlpYzA4dEw2ZkE9PSIsInZhbHVlIjoiYUFIQWdkZ1I0TXYrMGxlT0I1Q1g3UT09IiwibWFjIjoiNDlmMzZiMjUyY2IzN2QyYzhlMDZjZTA3NWQ5ZTA0NDA1ZTNjOGRlODg4NDFlZTdlNzM1ZDY1ZDk3NjFhZGVkMCJ9\"}', NULL, '2020-12-15 17:38:05', '2020-12-15 17:38:05'),
('b360362f-23a3-47f3-a921-c286717dc3f3', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlBDMGNsaHNaeit4cjVCcDNJMG9CdVE9PSIsInZhbHVlIjoiVWRXVVY3VHRGOGZMcnZ6bFljMFNRZz09IiwibWFjIjoiMDUwN2QyYjQ3YThlY2VjOWM1NzM3ZjRkYWQ4OTNiYzlhMTg4YmI5N2U0MTFiYmVhNWE3N2QzM2MzZGI0NzVjMCJ9\"}', NULL, '2020-12-13 18:44:08', '2020-12-13 18:44:08'),
('b371cb6b-69d4-476a-b4e1-22afabb61cc9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImRJeDZpYzJ2NHAzS3BEbFAzWnEvdHc9PSIsInZhbHVlIjoib3dRUDQreW9iUlZ4NHYzWGhSUFVWZz09IiwibWFjIjoiYjZkMDBhMzYxODEwMWE5N2YwYTIyMWVjZThkN2JiNTkwMzBjMjFkMGI1NmRlM2Q4NDY4MDcyM2Q0ZGI3YjAyZSJ9\"}', '2020-12-03 22:09:47', '2020-12-03 21:45:13', '2020-12-03 22:09:47'),
('b4631e24-cb38-4579-916b-2e318aa00031', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImZzK1FUYUhnN1daYUxMbVZ3cnpaaXc9PSIsInZhbHVlIjoiZjRuTXhnSzVxWmNRcE03bUlpR1lSdz09IiwibWFjIjoiODY3MWRjYTgzN2NmMTcyZjcwZTE3NDY4ZjQ5OTAwMGVkYzc1ZDNlMjMzZTU2ZGNjOWEwNDYwNTk4ZjIzNTJhYyJ9\"}', NULL, '2020-12-02 22:14:31', '2020-12-02 22:14:31'),
('b59ce095-ccf7-438d-bfac-704ecf1a63ce', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjF1ZFU0UmJFWXpjSEhNRCsrOG91UHc9PSIsInZhbHVlIjoiamxxZnRIZ1U5NFJUVHVvYTlBZGF6dz09IiwibWFjIjoiZjM4NzI4ZmQ5NmQxODA0OWNkOTA1YzM4NTM3MTJhMjI5NDVhNTg4YjdjNmM4YjJhOWQ5ZDA5OWRiNDhjZmY3NCJ9\"}', NULL, '2020-12-01 19:49:37', '2020-12-01 19:49:37'),
('b5dbcad6-e7d8-4a45-884e-c8355bfc9d30', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Sultan\\u2019s Shop<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjN3UEtPaE9BTnFJL3NHMFBKOWxkK3c9PSIsInZhbHVlIjoiRmdKTXhSMnh0aTJKWVkzRGl6Nmtwdz09IiwibWFjIjoiMDI4ZWM2Njk3NWE3MzNjYzQ2MGE1NjNlODAzNmQ2OWZlYTFkOTAzNTA3YjU4Y2E3NmMyMjhhMGM1MTI0ZTU3MyJ9\"}', '2020-12-01 20:32:00', '2020-12-01 19:01:26', '2020-12-01 20:32:00'),
('b67c1b7a-38d5-4071-be19-5b6df58634f1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkRlK1RZcGlmVjBtN281ekRlTTcvS0E9PSIsInZhbHVlIjoib1A4N01hcTFNVnJIRG01YmlGZ25kQT09IiwibWFjIjoiNWEyMGQ0ZGMxMTIzMzliNWVjNzUzZTE3NzE1MWM3MTk3MzE3ZjBkZTNhZmM1N2FkMGU0YWNmN2EyMGMwYWYwMCJ9\"}', NULL, '2020-12-07 17:55:43', '2020-12-07 17:55:43'),
('b7aae8ab-b067-4ded-a742-0de53a59faf7', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ingwc3UxbnFIeXlWUmxWWENTYVdUU1E9PSIsInZhbHVlIjoiUTgrNWRlMEhvbkJGR25NMjF6ajkwUT09IiwibWFjIjoiNTQ5ZDg2MmZkMjFiNjgwOWIxMmFkNzZjZDAxNTg3MDBkY2E3NDVkYjNjMjNmNDJmZWY0ZWZiNzBiNjE4YzRkMCJ9\"}', NULL, '2020-12-03 19:49:03', '2020-12-03 19:49:03'),
('b8d6820f-aa3e-45a6-a318-254edf0065a0', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlBYa3U5Ykd1Um84NGtvTzFmRlFqZXc9PSIsInZhbHVlIjoiTFVoVE1WWjRPdnNtVDNCVVNNZzZMUT09IiwibWFjIjoiZDY1MzIxODFhZWNmYmI0ZGZhNDJiMDQ4MzgwMTAxZTNlNjc5NGMxZjliYWEyODUyNTcxMDQxNjVkMzZhYjEzYyJ9\"}', NULL, '2020-12-15 18:12:23', '2020-12-15 18:12:23'),
('b99cd9ad-95f5-40b3-be4a-563306db912c', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik1HM2lWMW90SjgyWlI3NE9IT3NmNVE9PSIsInZhbHVlIjoiZXJYeEJ1NkpiTVNQMVdMT1l3VUdFUT09IiwibWFjIjoiZmMwZTc3YTIwYTQ2OGUzN2JjZjFkMjBjNTdkOGYwNTJlNWU0MDMxNDQ4NmEwMWFjNmI5NTI2Y2I5Y2UwOWM3MiJ9\"}', NULL, '2020-12-09 19:19:12', '2020-12-09 19:19:12'),
('ba193ea5-820c-43ad-9b29-3ab8b2f0339a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik9CZFUzcm91djliemJtS2Z0OStDeHc9PSIsInZhbHVlIjoiSmUxQVllWk1WWmNVVGUzT0dDQXJBUT09IiwibWFjIjoiN2U0MmUxYWI3ODNhNmViMmQ3OGM3ZWRiNTIzZDM0MmVmYWQxYmY2YzUwZDIzN2QyNWNiNzcxYzU0NzRkNTJmYyJ9\"}', NULL, '2020-12-03 17:52:03', '2020-12-03 17:52:03'),
('ba89ba16-5cf2-4baa-bc76-ee41ce35ac29', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Im40dTd6TVEycG9Wc1R3M2lXSzFuZ2c9PSIsInZhbHVlIjoiRXQxQUkwdUVaMG4zSU1WWk5rdTNxZz09IiwibWFjIjoiZThiYWM2NjA1NTQ5ZjljNmNiMTU1ODJjNzY0MmU1MmNkZDBlNjIwZTg5NWIwNGVmNGQwNDg2ODI2NmJlZWRmZCJ9\"}', '2020-12-03 02:01:15', '2020-12-02 21:33:54', '2020-12-03 02:01:15'),
('bb412789-58b8-4b98-9665-d57be27b57a7', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ik1YU1hvYUNkeDRGVU1XU2RNUGpoVlE9PSIsInZhbHVlIjoiY2FTUk1KUjZzaGQwRVkwVTA3UEJ4dz09IiwibWFjIjoiNTU5MjdhZjY3ZWE0Yjc3YzQ1NzFhMjQ4ZjI4MGZmMmQ2OTY3NGQyY2IxMmMwZDE3MGRhYTcwNjA2Mjk5ZjY5NCJ9\"}', '2020-12-01 20:19:09', '2020-12-01 20:18:56', '2020-12-01 20:19:09'),
('bb417811-aeb7-45e1-976a-3bc47a9290c7', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Women\'s Craft<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ik9oZHpSV0M5c3ZOQTVpOENrcjlMUXc9PSIsInZhbHVlIjoiQ053RG1DMW1qdVNsSGVzMElLSzRRUT09IiwibWFjIjoiODU4MzJkMWJmYjcxNzljZDA1NDQzYjgzOWU3OGZjOGZjZjBhYWQ1MmQ3YWZhYjFhNzM5N2E4ZGMxNDhmNGRhYiJ9\"}', '2020-12-09 21:12:49', '2020-12-09 21:12:32', '2020-12-09 21:12:49'),
('bb4b7306-6ceb-4a44-9b52-c61beecd82fb', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ijh2Uk8xNGdTRGZtdFYvd2QvdnBmU2c9PSIsInZhbHVlIjoiYTM4T0V0RzArVXF6M2J0b281Tmdydz09IiwibWFjIjoiNGRkNzkzZDQ1MzNhODhlZmE4MGM3ZWI1MjkzZDBlMDYwYmVkZTVhYTQ0NDY3OTQ0N2U0NWNhMDA3OTMzMmIyMSJ9\"}', NULL, '2020-12-15 18:45:55', '2020-12-15 18:45:55'),
('bca9dff2-9460-45a9-bb2a-a862292091de', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImlSbGZ4SVUydGdsVkIwYm9JamZJeUE9PSIsInZhbHVlIjoiZ0g1T2NNNCtCMXpWS2xoUUNqVzNXdz09IiwibWFjIjoiOWFkYjQwOGJjNGY4YWVhOTk1MDdmMjQ1NDlkMWNiNzE1YTg0OGU0NWM1YjcwOTNhYTYzMDYyNGMzYjdiYTI1ZCJ9\"}', NULL, '2020-12-03 19:08:15', '2020-12-03 19:08:15'),
('bcfaa6a0-5d72-4d3b-95d7-1f5b1a8d9e88', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImhwbnRVRGNmM0F2a25oMHN3OTIrRlE9PSIsInZhbHVlIjoicXp1Yk5JdlJyeDh5UWluUFNkQ0svZz09IiwibWFjIjoiMjZkMTZlNTQ3MDRkNzdjYjQzMzAzMWQzZmI5OTk1OTM4MGFmYThkNTMyMWNiNjMzZDZjMTIwNmYwNTBmOWM3YyJ9\"}', NULL, '2020-12-05 05:12:12', '2020-12-05 05:12:12'),
('be3ca80d-1a32-41d9-87dd-f558118f9260', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlF6NmN4ejZBMjRkazF3bmJuK0lGaWc9PSIsInZhbHVlIjoiMGtCVGdGM0tvak41aWpGWjY3Rzl2Zz09IiwibWFjIjoiODQyOGNmNDdjOWNkODYyYTQ0NzdjOTc3YjliMDM4NzU1NDlmNDVlYTE4ODM0MWY5MzNkNzIzNGY3NjViMWNhMCJ9\"}', '2020-12-03 22:09:30', '2020-12-03 21:49:14', '2020-12-03 22:09:30'),
('bfc1d051-96ab-462d-94b6-35321d94e2f1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InBsb1prNDlLUXFCaVpVTWhkdE1DZkE9PSIsInZhbHVlIjoiQWNFOURHbDZPS01RSHhKblpwWWdEZz09IiwibWFjIjoiNmRmOWMwY2JkMmVlNGE4MWFmNDY3ZmZhZTczNjgxN2JmNDk0MjZjY2QyNTgzYTY5MTdiNjBhY2EyMmNkNjZlZiJ9\"}', '2020-12-01 20:32:11', '2020-12-01 20:30:36', '2020-12-01 20:32:11'),
('c1dafe6d-9982-4404-90ed-ced62f051a29', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjJWK0FPMDFCdlVKY0xOb1hIWGpiNFE9PSIsInZhbHVlIjoiNFFybzBBdndaWG9udWtURzlTcDFCZz09IiwibWFjIjoiZmJkNDcwN2RmNWVkYTkyYWU5MTA4OGU0MTQwM2M3ZDM2OWFmZWY2NmU2OWI5ZTYxMTJhZjhmMzlmMmViMGQ3OSJ9\"}', NULL, '2020-12-07 18:13:07', '2020-12-07 18:13:07'),
('c2a73bdb-65ac-43a7-9e59-05a53e306e90', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkdzY3ZZcmNpbVBkRjl6SkNCQ0xLWWc9PSIsInZhbHVlIjoicDVFRFF0SUZCSzdVdGZaVEV3QTFRdz09IiwibWFjIjoiNjlhYTc3ZGNhZjQ2YTY2MzU5N2VhMzVlZjcxOTUwZGU4NWQ1NmEyOGQ2ODhkZDgyOGZkMjVjYjk2ZjRiM2U1YiJ9\"}', NULL, '2020-12-07 18:13:07', '2020-12-07 18:13:07'),
('c4adb8bd-348f-4ec3-9c8c-f28a66d28ec4', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099f\\u09bf\\u09df\\u09be\\u09b0 \\u09b8\\u0996\\u09c7\\u09b0 \\u09a6\\u09cb\\u0995\\u09be\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImxFdEcvb2NSUW40TjRxazJiQTRhMVE9PSIsInZhbHVlIjoianFvaE4rSlZMSkdydXhpVzEzQWFXQT09IiwibWFjIjoiODdjMmRiMmQyNDA5NThhMzMxZGExNWYyZmVhZjg5MWVjZWZhNDFjMmY5OWEwMzM2NDkzYmYwODBhZGY3ODA3OSJ9\"}', NULL, '2020-12-13 17:58:57', '2020-12-13 17:58:57'),
('c4b5f656-fe81-4fd3-aa65-a97e3bc2e058', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Art by Soumitro<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ik5QdC9MY2F2czZGdlFoYXpyWnUrRmc9PSIsInZhbHVlIjoiV3VoRHBhbGRXTzVPQzREOGd3allkZz09IiwibWFjIjoiMGNjYTc4NjYyMmY2ZWE1ZjNhZGIwOWQ2ODQ2NGQ0ZTA0NDRjN2FiYzA4N2YxZDM1OTU1NzRmNTY2NTgxYjQ1OCJ9\"}', NULL, '2020-12-15 17:20:47', '2020-12-15 17:20:47'),
('c4c7717a-15ed-4106-bd1d-874baa015f19', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InhleXh6anZRVjRrelRsWTJQWmVPSEE9PSIsInZhbHVlIjoiaklvTm4yVnBWUjFFV2NnSWZiTWZUUT09IiwibWFjIjoiM2I5ZjI1MTE1YzcxMzg0M2ZlOTc2NGMyOWY4ZTc5YWYwZDljMDhiNmFiODI0YmY1NmVkMTRkM2UzN2MyNzg0MiJ9\"}', NULL, '2020-12-14 21:48:45', '2020-12-14 21:48:45'),
('c6116e96-1904-4799-9923-1329dcb1ef68', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Gaandhari-\\u0997\\u09be\\u09a8\\u09cd\\u09a7\\u09be\\u09b0\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImFuaDdlYzB3ckRoU292YkZ6VzBCN2c9PSIsInZhbHVlIjoiVGVVbXFwYVQ2MjE0d05FajlRQXFaQT09IiwibWFjIjoiNWI2NWY2NTEzMzk4MDRiZTkzNzY5MGY5Y2JhZTA4MDJiMDg2YWE4NjJkN2IxOTgwZjczZTI0MzkyMzE0NTc2ZiJ9\"}', '2020-12-02 07:46:09', '2020-12-01 22:36:02', '2020-12-02 07:46:09'),
('c71ce036-54fd-4359-bb0e-7238402d9e93', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlpvenhKYzdseUFmWi9BNWRlUURLd2c9PSIsInZhbHVlIjoib0VQRHpGdUEwRUwyRWh6M05oNVp3UT09IiwibWFjIjoiZTFjZjRiOTBjM2EyMWI1ZGM0ZTM1M2JlYzg1MTEzM2E2ZDM5MDI2YjIxMWJmMjQ4MjgwNmNiNzg1NjZiZDFkNSJ9\"}', '2020-12-02 07:46:22', '2020-12-01 22:00:24', '2020-12-02 07:46:22'),
('c7a3fa74-c071-42f8-972b-fbf2162608d5', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjF3cGRwTUkybDB5MjZJdDM3aW9tZ3c9PSIsInZhbHVlIjoiSmZ0RWJwb0dMRnJKM09LVEFPbXJTZz09IiwibWFjIjoiZGI2OTZjZWNiODMxODJhMDAwNjdjNGE5MTAzZWJhMGFlNzE3ZDM3OWU0MjQ3MGRlYzk0ZDg1ZWE5OGY5ZmRkYyJ9\"}', NULL, '2020-12-03 19:43:49', '2020-12-03 19:43:49'),
('c8db4b34-ff9c-4501-9cf0-33fede63952b', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Moushumi\'s Loft<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImVTai9HVTVqM0JvcnZoOU9uaE9Ob3c9PSIsInZhbHVlIjoiUXFuQVRpUmtrcEUraWhtNkV2T2ZJQT09IiwibWFjIjoiY2I0NTFlZjcyZDQzMWIwZGRlOTcyYzVkMWQ0OTViMzhkYjNiNDUxN2NhZjYzMzgwMWM5MjZlYjZiMjIzMzZlOSJ9\"}', NULL, '2020-12-06 19:43:55', '2020-12-06 19:43:55'),
('ca4c0bef-c289-46c1-8732-f592e30f6fdb', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IklsQmY5a29LTHhUOHVIY3JDTlRCVlE9PSIsInZhbHVlIjoiNGtqa2JzRnBDYWtLVEc4RWg5RVZ6QT09IiwibWFjIjoiYjBjYmFjM2FmMWUzYzA2ZmRjZDYyM2E4OTE2Yjk5OTVkNTE0NGYxMGJmZjRkMWZhMjFmYjQ5MGI4MjRmNDBjZCJ9\"}', NULL, '2020-12-15 18:45:55', '2020-12-15 18:45:55'),
('cc184933-c9ca-4a07-af78-88c7092c67f9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Nilavrita - \\u09a8\\u09c0\\u09b2\\u09be\\u09ad\\u09c3\\u09a4\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImRzUGJyZVJIcnlDckJxbWJEUE1TTnc9PSIsInZhbHVlIjoicncvdExUekJBMVlsWjlscllEa3ZNUT09IiwibWFjIjoiYzQxOWJlNjY5NTI4ZGU2NGExNGU5Y2I3NDUyYzdlMDk1Njc3YWZjNGE4ZjEzODkyM2JkNzllN2ZhOWNiMGRiMyJ9\"}', NULL, '2020-12-05 06:58:50', '2020-12-05 06:58:50'),
('cc570183-d6e6-425f-9e69-df1040a3cf69', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Imc2YStJSjl4YjRVbyt6ZWV0ZEYyc1E9PSIsInZhbHVlIjoiblNWN3RGQ2p0ZmJCODErY2pJbHdJQT09IiwibWFjIjoiOGVhNjg4MDkyMzkzNjhjODlhNGIwYjAyN2FmMWQyNzRkZGRkMGQxN2Y0OTgwNmRkZDVhZGRiNTE2NDM1MTBiNyJ9\"}', NULL, '2020-12-15 18:42:18', '2020-12-15 18:42:18'),
('cd19229d-2515-485e-b563-c06cfac1ae45', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkdtOUFIMFJ3L3ZCSEZNN1hpWFJEQ3c9PSIsInZhbHVlIjoiTGlhMWhrRzVtRkYvbS8wMk40eEFNZz09IiwibWFjIjoiMTNlYThhZDI2NTllNDMwMjBkMTFjOTI5M2VjZDZjZmViNmIzN2ExMWQ1YzdlZGVjNTNjODA4M2M0MTQ1ZWMzNCJ9\"}', NULL, '2020-12-03 20:48:17', '2020-12-03 20:48:17'),
('cd7a405a-9bc8-4d05-8067-e6c512239a29', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6InhNRmRJT2toMHFrV2ZPOWRyZWFqNXc9PSIsInZhbHVlIjoieXl5d3paQW1FRjg4SVRJNVNxV084Zz09IiwibWFjIjoiZjUyYTEyOTQ4NGI2ZjFkMDYxMjgzZmQ1MGE3YjQ5OTZlMmQ3NDQ4YmNjODVhNjljZGM3M2JmNDk5M2NkNTgwMSJ9\"}', NULL, '2020-12-15 18:09:13', '2020-12-15 18:09:13'),
('ced8b156-0b0f-479c-a8a9-d5820fe8d80c', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09a8\\u09c0-\\u09ac\\u09cd\\u09af\\u099e\\u09cd\\u099c\\u09a8\\u09be-NeeBenjona<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ikk4aDM2cmQyWUlLd3JraGcrRnpDaHc9PSIsInZhbHVlIjoiU3VQNnZOdlV6VGJsdHNqUnJDRTlLUT09IiwibWFjIjoiNGUxNGE5MmM5ZmIwY2FiYjkxYjcwZDQ5NGU3MzIyYzEwOTc3ZTdmNjM0MWRmNmRmZDZkNjQwMzMyNjUwMzY2YiJ9\"}', '2020-12-02 07:46:29', '2020-12-01 21:48:48', '2020-12-02 07:46:29'),
('cf5db0f8-1a79-4d43-805f-740366517e14', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjI5Vko1Mm1BV0p4WmI0eG9BWkUzQkE9PSIsInZhbHVlIjoicUtmNWRDdzJwallkcVk1c29ZRXBVdz09IiwibWFjIjoiYzBhZmM2ZTAyZjc3OWEyNjZmMzc3MmE3MTAwNDA4ODc3ZWMyOTY2NjFmMzk3ODE3N2FkMTQ1Yzk1Y2E5Mzk2MyJ9\"}', NULL, '2020-12-15 19:31:54', '2020-12-15 19:31:54'),
('d0bd7311-1789-48ae-a892-b63d0f95d4fd', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik9IMm4zREIvMVdCeFVhb0FZdjJTL3c9PSIsInZhbHVlIjoiczBmNjdjN0d4R3l6K2VJUXZDZUlWUT09IiwibWFjIjoiMmRkMGEwNzhjODJmYjdiZWUxNDNiODY5NzU5MmIzNDY0ZDU1MmMyZjUyYzc4MzA1Y2ViNjdiZjhiOTVjNjU0ZSJ9\"}', '2020-12-01 20:32:30', '2020-12-01 19:49:37', '2020-12-01 20:32:30'),
('d2b5613a-e54b-4e4f-85e8-f1d08e4cc23b', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09a4\\u09cd\\u09b0\\u09df\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkZzU2RmTGU4b3NXSnRabWd1OWVYSGc9PSIsInZhbHVlIjoidXdvcmxhUXZRU0VkcXNqKzNJOTlodz09IiwibWFjIjoiYjc4YWIxMTc5NmRhMGRkODMzMzBmMDQzYWNhNjNjZmFmOTQzNmNhYzcyMDViNjA2MDYwN2I3ZjQwOTk5MDQ1MSJ9\"}', '2020-12-03 02:01:30', '2020-12-02 21:26:55', '2020-12-03 02:01:30'),
('d304e4ea-ed7b-40d3-8849-5346a413138e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Monforing \\u09ae\\u09a8\\u09ab\\u09dc\\u09bf\\u0982<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/www.shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im1PZ0Y4cXJEZ0pURHlZQk8xck5XSXc9PSIsInZhbHVlIjoiL1B2K2w5SjlrSmpIeHV5dFVYd1pjZz09IiwibWFjIjoiN2I2M2ZjYTBhMTRmMTkwNjExODg3ZmQzMDlkNTY1ODVjNmJjMTFjOTM3NjhlZjUxMzRkMzIzNGM2YmQyYjY5MiJ9\"}', NULL, '2020-12-11 02:17:31', '2020-12-11 02:17:31'),
('d4a28a46-c51c-4935-99ee-824bde47dfc7', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkxEakNVM1JjeWpVMy9LUzB2VlBHNFE9PSIsInZhbHVlIjoickkrTVVidDNsb29uUUhaWDNDMXZGdz09IiwibWFjIjoiMGNlOWQ4M2M3N2U2NmU3ZDM3OGU3NDViZDk5YmZiY2RmZTQyOTQzYmJlY2U5NWNmNzM5OGJmMjFlYmU4ZjYyNiJ9\"}', NULL, '2020-12-03 20:48:17', '2020-12-03 20:48:17'),
('d4c439d0-dbd2-4fdf-8f03-51c1cc7d301f', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Glowing_splint<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlBzSFp6aVFBbDNBcWs3TDMzR0xHRlE9PSIsInZhbHVlIjoiU1ArRUhuK0drS0E0R3ZHaDQ1N2xpQT09IiwibWFjIjoiMDFkM2MwZTMwMmE0NmEzMDE2OTllNmQ1NDc3YWRiZDI5MzU4OGYwMmEzMTIzMjUwZGUzZTVjMmNjY2RlNTBiNiJ9\"}', NULL, '2020-12-09 19:34:15', '2020-12-09 19:34:15'),
('d5d103c3-0829-442c-9428-674133fa229a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjlURmZscjFyNEp3cGtkdER6UTh0d1E9PSIsInZhbHVlIjoiN3ZtZlZ0ZDZ3KzVweTVNL2o4SUVhdz09IiwibWFjIjoiZDA4NTE3OGJiZTUyNTBjNmU4NTg1ZTZlNDNjZDY0MjRiZTBmNGFlMzIyNGI2ZjZjYjAxYzY2MGY3MWQ2MzU0NSJ9\"}', NULL, '2020-12-15 18:06:57', '2020-12-15 18:06:57'),
('d69aad30-2a82-4aa0-ad71-c52230b38a94', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IldsaG5YZTdsNThvMFdzeElmMlVTNnc9PSIsInZhbHVlIjoiVFlIMXVzOWZkdTNuY0o0VE96dnNuQT09IiwibWFjIjoiZjc2ZjRiMjNmMzE1ZGRhNzFkZmEyY2ExZDBlOTU2NWJhMDE4NmFjMzdmZGM0MzFjOWE0ODcyODg3YzlmNzk4ZiJ9\"}', NULL, '2020-12-03 20:18:44', '2020-12-03 20:18:44'),
('da9cea3e-516c-4239-9dc3-a2d7505d4ae3', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099a\\u09a8\\u09cd\\u09a6\\u09cd\\u09b0\\u09ae\\u09cc\\u09b2\\u09bf - Chondromouli<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IldJWGdMWktkVVJVZWZ6RWZoMG85UHc9PSIsInZhbHVlIjoiVUQ0aFk5MzRVeFVFUlBnMkc2L2w1QT09IiwibWFjIjoiZjIwOTgwMjE5ODc3NGQ5NGJmZDk5NzIzZTA0YjdmNDU4NmIzNDA3N2RiZGQ2ZDZmMDcxNmZmMmNkMDFhMTEwNCJ9\"}', NULL, '2020-12-03 19:49:03', '2020-12-03 19:49:03'),
('dc278b61-fea8-40a2-8bf9-d7aab6ec23e1', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlBFN1ZDSW1uT2MxMk1sU0JwejczUXc9PSIsInZhbHVlIjoiUS9kWW83N2hPQjlZbkxRNWxINkdtQT09IiwibWFjIjoiOTY5MTNlOWU0MDIwYzE5ZDk2MTVlNTM1NzJiNjM5ZGZmZDQwODg3ZDdkMmZlN2VlYzdjMGUyYmViNDg1NDkyMSJ9\"}', NULL, '2020-12-03 18:09:19', '2020-12-03 18:09:19'),
('de8c399d-7b2f-4cbd-a0a2-d2d13d3c100a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik1mUExKOW1pNmZERTR6UTRTR1d0MEE9PSIsInZhbHVlIjoiY1Z1ejZnVjdLYVpuNmRzenRsMnhzZz09IiwibWFjIjoiNzMwZDdkOTUyNGJmMDUyOGNlNTkxMGJhMWNjMTMwNTg5OTRiOTRjYTZmNWM4ZjZhMGVlMzE1OTMyNzk5ZGM2ZCJ9\"}', NULL, '2020-12-13 18:45:19', '2020-12-13 18:45:19'),
('def1a66d-fadf-4001-81c9-254d359c80a2', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IlZQckMzcmpBUjF1UDczZFNjMTNGTmc9PSIsInZhbHVlIjoiczdvKzhwTDRpbTc1ekdHWGZRcTRNUT09IiwibWFjIjoiZGYyZTdhODNlNWY3MmIzZjdiMGExODgxNWIwZDllNzA2ZTJkYTY3NWU1Y2E1ZWQ2ODljMmJlMjBjNDA1MjM4NCJ9\"}', NULL, '2020-12-02 21:57:19', '2020-12-02 21:57:19'),
('df1f176c-96e3-4256-9c15-02474b500940', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Art by Soumitro<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ikl5NDhLVWs0TU9KdHBQUXRnZ1dSdnc9PSIsInZhbHVlIjoiaXkzYURvTnlzaTNiWmNCVWd3Q01wQT09IiwibWFjIjoiYTY0MzU1YThhNGNkNDIzZGI1OGQxOTM4YjljYTg5ZmNmOThlYzY1MDY4OTFkY2M2Mzg4ZjM0MjFmZDlmNDc0OCJ9\"}', NULL, '2020-12-15 17:20:47', '2020-12-15 17:20:47'),
('e1fb8966-6aee-44cd-9c07-47b99e09f1eb', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09ae\\u09c3\\u09ce-mreet<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IkdxMFA1ZFYvNE1QYWtBSFMrS2FaOGc9PSIsInZhbHVlIjoibFNSUWpJU2NkbXlDWWNyWUVwS1kvQT09IiwibWFjIjoiZThhNWM2NTNhOTIzODJkNTM1MmI0OWM5YjM5Y2IwYmMwOGVlYzNkNDg1NmJjZGVkNzMzYzc3ZjM1NTc4YmU1NCJ9\"}', NULL, '2020-12-02 22:19:58', '2020-12-02 22:19:58'),
('e2f58dd6-2ef2-4884-8c1e-ed2a1457d999', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Zhaapi Sharee<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjZrK1IzZitncUNvbTNmNGVFRDhYWHc9PSIsInZhbHVlIjoiaE01UDJTMUw1aXdRUWhCRGNabUltUT09IiwibWFjIjoiYjNjMTZmY2NjMWZlZDc0NzBhN2Q4MzFlY2JjYTk4ZTgwNjM5YjRhZTFjN2MyZjMyNzNhNDFlYjZjODQ1NmExYiJ9\"}', NULL, '2020-12-09 17:29:19', '2020-12-09 17:29:19'),
('e3200ee7-64bd-439b-bb9e-4c0c61bae254', 'App\\Notifications\\Shop\\Product\\Status', 'App\\User', 56, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09bf\\u09a3\\u09a8 \\/ \\u09a5\\u09be\\u09ae\\u09bf \\u09b6\\u09be\\u09dc\\u09c0<\\/b>\' has changed status\",\"link\":\"http:\\/\\/shopinnbd.com\\/product\\/48\\/%E0%A6%AA%E0%A6%BF%E0%A6%A3%E0%A6%A8-\\/-%E0%A6%A5%E0%A6%BE%E0%A6%AE%E0%A6%BF-%E0%A6%B6%E0%A6%BE%E0%A7%9C%E0%A7%80\"}', NULL, '2020-12-07 01:49:49', '2020-12-07 01:49:49'),
('e3f15358-2ecb-4d37-9d76-3bebca6f026a', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkNEM1JNamNRLzhJZXdhWm03TnBHMUE9PSIsInZhbHVlIjoiSzJqdEtQU3YyUjBWMGtkNytvK1FqZz09IiwibWFjIjoiYzViNmEzZmI0YmRjMTFhYWNmYzRjYzE3NTgyYWZkYTMwNGNkM2ZkMDkzYjQyZmY5Y2U3ZGVjYTEwYTllZTllMSJ9\"}', NULL, '2020-12-07 18:09:32', '2020-12-07 18:09:32'),
('e40a50b4-14f4-4d56-9327-f7671bdcc81c', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Imx1aU5jTFV4UVdJL0RveS9MdzdNRlE9PSIsInZhbHVlIjoiRlN4ZUZCdVV1czVrYWExRlpRUnhnQT09IiwibWFjIjoiOTE5ZjI5NjQ5OTZmY2IwNzNlODVjYzFiZGQ2OWNhYjA0MTYyYzU5MzhmMDIzY2ExNTNjMjU5NDM4NWJiZGQ1YiJ9\"}', NULL, '2020-12-03 20:47:07', '2020-12-03 20:47:07');
INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('e430e89c-4b72-4572-8f7d-a11a6420e12c', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Sultan\\u2019s Shop<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkRGOXJQUVpPL2VqWE5LOHd6UUxVY2c9PSIsInZhbHVlIjoiVndvb3pDdGIyUjlUMVlsa0padm9UUT09IiwibWFjIjoiNjNiYTcwN2E0ZDhmY2IxMjI5ZGFmYTVlMGVjYzkyMDE4NGE0NjU1OGJhZmQ1MWY3ZTQ3MzI2Njg4NWFmNzM2MyJ9\"}', NULL, '2020-12-13 21:21:24', '2020-12-13 21:21:24'),
('e4503a45-cdfd-4aff-a645-51e5efb62624', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImJ0SFYvb1p4K1VCZDVFV29XbjNsZHc9PSIsInZhbHVlIjoiaFVIam9SL2diOWlGS2gzbHZmODZYdz09IiwibWFjIjoiYTA3MzVmNDMzNmUwNjYwYTM1MmFiODFiNWZmZmVhZGM5Nzg0ZmM3ZTQyZTE4ZGQ3ZGI0NWU1MWI3YjdkY2NhMSJ9\"}', NULL, '2020-12-04 21:52:25', '2020-12-04 21:52:25'),
('e5112b9f-9974-411a-bcf8-b07e11653f33', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ae\\u09c3\\u09a8\\u09cd\\u09ae\\u09df\\u09c0 \\u09ae\\u09c3\\u0993\\u09bf\\u0995\\u09be- Mrinmoyee Mrittika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlpKZ1owcitqSXZiNFozZ20rMC9seHc9PSIsInZhbHVlIjoiaDREY09laFU5MkppKzk5K2VJZ2MwUT09IiwibWFjIjoiMGFiNDE2MTEyYmRiYmNlZWE3YmZlN2Q1M2QyNGM4MGYxZmY2MzE1YTI1MWY2MmJkMThhN2FhMTFkYWNiNzdkMSJ9\"}', NULL, '2020-12-13 18:42:29', '2020-12-13 18:42:29'),
('e766da10-615d-4231-8a0b-6f868e580ff9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZ1K0F4Wmx4QXBBOHRqcCtNbytRNlE9PSIsInZhbHVlIjoiOUtDREV4SU5UcTBCby9QTXNLS1M2dz09IiwibWFjIjoiMzQ4ZDQwZWZjOGY2YTFkODFjNDNiZTkzYjE4YjdmMjhiZDg4MDc2MDhmNmE0YjcxMWIxOGMyM2UwOTY2YTM1NSJ9\"}', NULL, '2020-12-15 17:51:07', '2020-12-15 17:51:07'),
('e7791318-3ee5-481d-99d9-640e5a03206d', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09b2\\u09c0\\u09b2\\u09be\\u09ac\\u09a4\\u09c0 \\u09b9\\u09cd\\u09af\\u09be\\u09a8\\u09cd\\u09a1\\u09bf \\u0995\\u09cd\\u09b0\\u09be\\u09ab\\u099f\\u09b8-Lilaboti handi crafts<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ii9JbXFabEhURUNadTQvakI3dGpZOGc9PSIsInZhbHVlIjoiT3AwQW9SWEQ3amlKY2tKNjlXTjJNUT09IiwibWFjIjoiNTVkOTRhMTEzNmI2NjJmYmI1MWI0MWFiZDUyZmZlMjlmNDJiYzE2MjczNWRiODE5NDY2NmExNDY0NDE3YWY3OSJ9\"}', NULL, '2020-12-05 22:08:33', '2020-12-05 22:08:33'),
('e8598416-4795-440d-aa05-914ca5f4bfdb', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jamini-\\u09af\\u09be\\u09ae\\u09bf\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkRVb1ZPREdHZnptemR2QWg1em9KTUE9PSIsInZhbHVlIjoib1dWMWRyKy9idE1lamVBRjNVajZhZz09IiwibWFjIjoiM2Q0ZTQyYmU1NGNiM2YxZDZlYzVmZGM2NjVmZjBhYThmMDRkYTZjNzhkYmVkODY3N2Y0OGRjYzYxNTViMjhmZiJ9\"}', NULL, '2020-12-15 19:25:55', '2020-12-15 19:25:55'),
('e8ec0c5e-0ba3-40f0-a359-125ea3911b4e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZ3V01Da1lWZ1R1Skx0R0VmQlBteFE9PSIsInZhbHVlIjoicmJyazFDWlFpMXg4YnlIc2NOOUp2UT09IiwibWFjIjoiYTQ4MmE5YzM2MTMxNzA1MWI4Mjk2MjM2YzMzMDJmYTA2NTUxZTY4NTllNTQyYWMyNjI1Y2QxYmZlZDFmNWVlNiJ9\"}', NULL, '2020-12-15 17:49:30', '2020-12-15 17:49:30'),
('e97f0fda-b248-4029-af83-b66ec0492830', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ijh1b214UnNWb256OFhRdkZhR1FGeFE9PSIsInZhbHVlIjoiNUVMSVhod2wrMm1vZUFjSm9IUWNOUT09IiwibWFjIjoiNDIxODA0ZGVkZTA5YjM3OWViNWI0MDc5NDIwNTY2NzMxMWI4NjU0OTMyYmU5YTY5OGQ1Njc2MTI5ZmEzMzdlNCJ9\"}', NULL, '2020-12-09 18:25:45', '2020-12-09 18:25:45'),
('ea5581da-6d63-46ab-b6b3-411992314bac', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Crafty Fire<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ikc5akxlWW05eTdCZ1VIWFU0TDhUclE9PSIsInZhbHVlIjoiY0JDcWNvRENYNGRaSTNuTVZabUFYQT09IiwibWFjIjoiYjJiYWQyZWI3MjVjOWFhZDc0OTgxYWQ3N2RhM2E1YmU5MWNlNjc4ZGViMjMwN2UyMmIzMWYxYTNhZmUxYTlhNiJ9\"}', NULL, '2020-12-14 21:06:26', '2020-12-14 21:06:26'),
('eac012de-1c06-440d-9472-d8cbcaddd6fa', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6ImZSR1Z1TEhYd2N3eWtNaVRVTTJkOEE9PSIsInZhbHVlIjoiQ3RpdUkydXcxNmo2d3lGQVVEeDhyUT09IiwibWFjIjoiZmE0OGY2OWE3N2RjYWQ3NzFhNjJlMTI0OTA1ZDNhNzQ5Mjk4NmM5OWU1NTEwN2U3ZDVlYzkyN2VhZGFiYWE4NyJ9\"}', NULL, '2020-12-07 17:55:43', '2020-12-07 17:55:43'),
('eb746ac2-5de2-4662-ae7b-790556212981', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u09aa\\u09cd\\u09b0\\u09a4\\u09bf\\u09b7\\u09cd\\u09a0\\u09be \\u0995\\u09be\\u09b2\\u09c7\\u0995\\u09b6\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6IjFDN0YraEYxRnpESktqSGNNWEFSdnc9PSIsInZhbHVlIjoiOFNoK2J6Ry8xSTFnV0xJNXNzU01SUT09IiwibWFjIjoiZGQxYjExZDRkMWFiOGQ1NzhiZjI2YjI1MzRiYjRhNDQ1OTI5ZDI0ZGZhOGE3MWVmZDQyZmI3NjM4YmQ0OGIzNiJ9\"}', NULL, '2020-12-12 19:45:53', '2020-12-12 19:45:53'),
('ee3ddec2-e19f-4892-8416-4a4c87b86d95', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ikc3NzBPUkUxa2NScWM4d3dPWVpZWlE9PSIsInZhbHVlIjoiSVpzZy84ckNXcFlnaDc2WVAxQlA4Zz09IiwibWFjIjoiY2ZkZWI2NGJmYmQ0ODEwOWUxMTI0YWM3YWNiMzkzNWYyMWYzNDAzNDE4NTQwZjk1N2Y1ZWNiZjNkOTQzZGJhNiJ9\"}', NULL, '2020-12-09 18:34:43', '2020-12-09 18:34:43'),
('ee9a4f5f-8fb9-483a-9b14-3ce792a6d9d9', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b7\\u09cb\\u09b2 \\u0986\\u09a8\\u09be \\u09ac\\u09be\\u0999\\u09cd\\u0997\\u09be\\u09b2\\u09c0\\u09df\\u09be\\u09a8\\u09be<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ilc0YnZqZkNlV2I2b3J1dG1ZSlRjT0E9PSIsInZhbHVlIjoidndZRkRMTEFEeG00R1BBcDZFbUpxUT09IiwibWFjIjoiZDU1ZDQ5ZDdlY2MxNzI1ZDA5ZTM3NGIxNjFiOGEyZTA2ZGNkNDliZjI3YzNmYTg5ZmE3NjcxOGQyYzgyNjZmZSJ9\"}', NULL, '2020-12-01 20:30:36', '2020-12-01 20:30:36'),
('ef84db6b-6230-4e89-8d85-c60fe3cc03ae', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>Jahan\'s Collections<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjFZamdSSE5GZVNTMWFwRW9aaW9zK3c9PSIsInZhbHVlIjoiSEFtTEk1RGdrZExjc2tYTXp5c2pYdz09IiwibWFjIjoiMDVmZmY2MmI1YjE2MDQ2ZmIyYjY5YWQ1OTRjYmQ3MTJlOWQwNWJlNmMwNjFhOTgzNjY0NDA3YTkzMzFjYjY2NyJ9\"}', NULL, '2020-12-15 18:12:23', '2020-12-15 18:12:23'),
('f1e21b1b-f4a7-4e61-887c-fb394a543469', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik1nd3N3T3VKNUV6c2dpNVBlY2RmYUE9PSIsInZhbHVlIjoiVTNWa3pQZGkxdzBqVlN2cFlaZVVQQT09IiwibWFjIjoiOTE4YzA1MDc4YWZjNjFmN2VhYjlmNjdiNjdmYmE4YTE0NWE4YmJkZjYxZDQyNjNjNDE0ZDRmZTczOGEzYzE2MiJ9\"}', NULL, '2020-12-05 05:11:24', '2020-12-05 05:11:24'),
('f3086395-a7b3-45f2-a85f-3ede68105c67', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-loginn\'>Zhaapi Sharee<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImpBMkhRejEzUkhkQVh6VDlxUDFBaVE9PSIsInZhbHVlIjoidVl5ZlljOEdSdnF5UXNEZGZjaGJFUT09IiwibWFjIjoiYWU0MzRiMWExZmIyM2YzYTg3YTJkMjA2NzMyNWQzZTgzZTM0NzE3MDU4ZWY2MWQxOTFmMGYxNDhkMDAxYzU1NiJ9\"}', NULL, '2020-12-06 21:48:21', '2020-12-06 21:48:21'),
('f40292c6-40a2-46a6-bb8f-e6620e5ece71', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09aa\\u09be\\u09b0\\u09bf\\u099c\\u09be\\u09a4-Parijat<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlExQVhnaHNmai8rOWIrY2JNQXN1dXc9PSIsInZhbHVlIjoickdjSnBwT3N2TTQyZlJyZmJ4TmRGQT09IiwibWFjIjoiMmFmNWIxNDJlOTYyMGFlNjUyZTk3NjZjMGIwZGU2YjJiMmU1YThiOGQwZTEzNDczZDllNjI0MzkzMzY0YjJmZiJ9\"}', NULL, '2020-12-09 18:34:43', '2020-12-09 18:34:43'),
('f814fd60-c4e6-491d-bbc0-f92a5981ce72', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>CHanU<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Im9DQ3htZzlVeTVUSjd3cEF5dEVKSGc9PSIsInZhbHVlIjoic0kyVStteGhqQnZvQ3d4S1lsZUc3QT09IiwibWFjIjoiNDAyZTk1YmUyNzkyNTJhZDcxNDJmM2QyMzVjM2Y3Y2MwODM0YzZiY2Y1MjQ4MmJhZmM4NzI3MzllNzM4ZTZhOCJ9\"}', NULL, '2020-12-07 18:00:52', '2020-12-07 18:00:52'),
('f89967d4-563a-4511-9a7b-b27a2eb24e69', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>\\u099c\\u09be\\u09ae\\u09a6\\u09be\\u09a8\\u09bf \\u09ac\\u09b8\\u09a8<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6ImNhODdLeFJPL2h1TERoYlhLeWdaaEE9PSIsInZhbHVlIjoiYWlkL0NYZm9aTHhtSExuQ2lvQ3E3UT09IiwibWFjIjoiZTJjOWNhNmZkMGNmMDY0MTU4YmFiZGQ5NmIxNmRiMWU5Y2Q5MmMxMDBkYTUxOWYyMmM4ZDdjMDIyMjllYTAwMCJ9\"}', NULL, '2020-12-14 21:33:52', '2020-12-14 21:33:52'),
('f9559d01-cad0-4433-9eb0-c34335f2feb4', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u0987\\u09b6\\u0995\\u09be\\u09aa\\u09a8- Ishkapon<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZFc2xOSmU3QnUxNFRBYWYyWWQxUEE9PSIsInZhbHVlIjoiYTBjb0Ivdmphc1JKbUxHSUMrdWczQT09IiwibWFjIjoiZDRlYmQyNTYxNDJhZDhlMmYwNjQ1MTM1NTM0ODQ1MDNjMjRmMzJhNWI3OTYxMTJiNjRjOWI5MzRkYzU5YWFhMyJ9\"}', NULL, '2020-12-05 05:11:24', '2020-12-05 05:11:24'),
('f961cfbb-df2c-4993-b5e8-c43b99ad3125', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09b2\\u09cb\\u0995\\u099c<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IlZxb0JnMktoeFJ6Mi9RQXhLRUh1aFE9PSIsInZhbHVlIjoicmc0akJoOXhHTjBoN3NHaFl6WC9ZZz09IiwibWFjIjoiNzhkNzU2NTNiNmNjZTg4NGU5YjAyNDhmZTNhNTliN2VjNjM1ODkwZjI3MWUzMjcwZTJiY2RmYTUzMDIxNzAxNyJ9\"}', NULL, '2020-12-03 19:43:04', '2020-12-03 19:43:04'),
('fc3df7c0-bad2-4477-844c-649667959400', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u098f\\u09bf\\u09a8\\u09df\\u09a8\\u09c0<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IjJJVDJKTGNuOGk2NWtHanlXNWsrQWc9PSIsInZhbHVlIjoiSUZvOUxRVXdWR3ZEUlYxMnBUeG1hdz09IiwibWFjIjoiZjQ1MGFhMmM5NWIxZjIwYWIyZGU0MzI3ZGU3OWZhM2E4ZTMxN2U1OTlhNWIwZDAzZDZiMWJiNDAwYThhN2EwNSJ9\"}', NULL, '2020-12-02 22:04:58', '2020-12-02 22:04:58'),
('fe54e1e5-cf33-4c99-b900-0c29a64042ed', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6IkhzTzVpWElSMDhGbCs1VnNKT2VDOEE9PSIsInZhbHVlIjoic21udkpCT2RmNUtHMCsrcnpNTWVHUT09IiwibWFjIjoiZjNiMGYzMDcwZmY0ZTIzZTlmMzA0YTkxNTk2ZTUwZmU4ZDZlMTIzMGZmMTgxYmE1YWJkM2Q2Y2RkYzYwN2UwMCJ9\"}', NULL, '2020-12-15 18:44:55', '2020-12-15 18:44:55'),
('fe628f13-9e30-4630-9028-8198b7370f1e', 'App\\Notifications\\Admin\\Product\\NewProduct', 'App\\User', 1, '{\"message\":\"\'<b class=\'text-shopinn text-uppercase\'>\\u09ac\\u09c7\\u09a7\\u09a8\\u09bf\\u0995\\u09be :: Bedhonika<\\/b>\' Added a new Product\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/products\\/all\\/details\\/eyJpdiI6Ik1RZk0rZFBpQ09qY091b2hYTm5jaFE9PSIsInZhbHVlIjoiMC9mY25kb0d3QmRzODNwTlB1cXJhUT09IiwibWFjIjoiZTBjNjQ3Nzc0OTdkNGEyZGRlMzk2YzU0ZTY2ZDk5NjdiMjQ0OWViODIzYWM2MjQ4NjllOGIyZjVlNTdhNWNkNSJ9\"}', NULL, '2020-12-15 18:42:18', '2020-12-15 18:42:18'),
('ffe44884-1672-4ecb-8601-736a4a7e55f0', 'App\\Notifications\\Admin\\NewPackage', 'App\\User', 2, '{\"message\":\"\'<b class=\'text-loginn\'>Amarelo- \\u0985\\u09cd\\u09af\\u09be\\u09ae\\u09be\\u09b0\\u09c7\\u09b2\\u09cb<\\/b>\' has purchased a new package: \'<b class=\'text-loginn\'>Boutique House<\\/b>\'\",\"link\":\"http:\\/\\/shopinnbd.com\\/admin\\/shops\\/all\\/details\\/eyJpdiI6Ii9WY0tldy8wTldONW1aY0ZWTVJkSlE9PSIsInZhbHVlIjoiMlpocVpkaG1oMktOM295VzRMQnlMdz09IiwibWFjIjoiZmRjYTEyYWVkMzY2Njg4YTA1M2UzNmQwZWJlMzM5OGMyZTEzOWRlNWI2MTc3ODMwMzE5MjViNzA3YWVmN2U5YSJ9\"}', NULL, '2020-12-03 20:31:59', '2020-12-03 20:31:59');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` tinyint(4) NOT NULL,
  `expire_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `shop_id`, `product_id`, `title`, `discount`, `expire_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 56, 48, 'Pinnon Sharee', 6, '2020-12-31', '2020-12-06 22:32:50', '2020-12-06 22:32:50', NULL),
(7, 59, 58, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(8, 59, 59, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(9, 59, 60, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(10, 59, 61, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(11, 59, 62, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(12, 59, 63, 'বিজয় দিবস অফার', 10, '2020-12-15', '2020-12-09 19:03:34', '2020-12-09 19:03:34', NULL),
(13, 15, 77, 'সিলভার মালা', 9, '2020-12-17', '2020-12-13 18:47:57', '2020-12-13 18:47:57', NULL),
(15, 15, 76, 'সিলভার চোকার', 7, '2020-12-17', '2020-12-13 18:50:53', '2020-12-13 18:50:53', NULL),
(16, 15, 75, 'সিলভার চোকার', 7, '2020-12-17', '2020-12-13 18:50:53', '2020-12-13 18:50:53', NULL),
(17, 15, 74, 'Jewellery Set', 8, '2020-12-17', '2020-12-13 18:52:42', '2020-12-13 18:52:42', NULL),
(18, 15, 73, 'গহনা', 21, '2020-12-17', '2020-12-13 18:54:20', '2020-12-13 18:54:20', NULL),
(19, 3, 79, 'WInter Offer', 13, '2021-01-30', '2020-12-13 21:29:37', '2020-12-13 21:29:37', NULL),
(20, 3, 80, 'WInter Offer', 13, '2021-01-30', '2020-12-13 21:29:37', '2020-12-13 21:29:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `oid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `is_reseller` tinyint(1) NOT NULL DEFAULT '0',
  `reseller_earn` double(9,4) NOT NULL DEFAULT '0.0000',
  `reseller_paid` tinyint(1) NOT NULL DEFAULT '0',
  `rider_id` bigint(20) UNSIGNED DEFAULT NULL,
  `rider_earn` double(9,4) NOT NULL DEFAULT '0.0000',
  `rider_paid` tinyint(1) NOT NULL DEFAULT '0',
  `extra_cost` double(9,4) NOT NULL DEFAULT '0.0000',
  `coupon_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `delivery` int(11) NOT NULL DEFAULT '0',
  `has_reviewed` tinyint(4) NOT NULL DEFAULT '0',
  `status_at` timestamp NULL DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `s_sell_price` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `shop_earn` double(9,4) NOT NULL DEFAULT '0.0000',
  `admin_earn` double(9,4) NOT NULL DEFAULT '0.0000',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_logs`
--

CREATE TABLE `package_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `start` date NOT NULL,
  `end` date DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `paid` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_logs`
--

INSERT INTO `package_logs` (`id`, `shop_id`, `package_id`, `start`, `end`, `is_active`, `paid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 19:01:26', '2020-12-01 19:01:26', NULL),
(2, 4, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 20:18:56', '2020-12-01 20:18:56', NULL),
(3, 21, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 21:41:04', '2020-12-01 21:41:04', NULL),
(4, 5, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 21:48:48', '2020-12-01 21:48:48', NULL),
(5, 6, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 22:36:02', '2020-12-01 22:36:02', NULL),
(6, 19, 1, '2020-12-01', NULL, 1, 0, '2020-12-01 22:54:53', '2020-12-01 22:54:53', NULL),
(7, 18, 1, '2020-12-02', NULL, 1, 0, '2020-12-02 20:35:26', '2020-12-02 20:35:26', NULL),
(8, 15, 1, '2020-12-02', NULL, 1, 0, '2020-12-02 21:33:54', '2020-12-02 21:33:54', NULL),
(9, 17, 1, '2020-12-02', NULL, 1, 0, '2020-12-02 21:57:19', '2020-12-02 21:57:19', NULL),
(10, 8, 1, '2020-12-02', NULL, 1, 0, '2020-12-02 22:19:58', '2020-12-02 22:19:58', NULL),
(11, 9, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 18:15:53', '2020-12-03 18:15:53', NULL),
(12, 10, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 18:41:39', '2020-12-03 18:41:39', NULL),
(13, 11, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 19:08:15', '2020-12-03 19:08:15', NULL),
(14, 12, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 19:49:03', '2020-12-03 19:49:03', NULL),
(15, 13, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 20:31:59', '2020-12-03 20:31:59', NULL),
(16, 14, 1, '2020-12-03', NULL, 1, 0, '2020-12-03 21:28:15', '2020-12-03 21:28:15', NULL),
(17, 60, 1, '2020-12-04', NULL, 1, 0, '2020-12-05 05:15:59', '2020-12-05 05:15:59', NULL),
(18, 24, 1, '2020-12-05', NULL, 1, 0, '2020-12-05 22:08:33', '2020-12-05 22:08:33', NULL),
(19, 63, 1, '2020-12-06', NULL, 1, 0, '2020-12-06 19:11:19', '2020-12-06 19:11:19', NULL),
(20, 64, 1, '2020-12-06', NULL, 1, 0, '2020-12-06 21:17:08', '2020-12-06 21:17:08', NULL),
(21, 56, 1, '2020-12-06', NULL, 1, 0, '2020-12-06 21:48:21', '2020-12-06 21:48:21', NULL),
(22, 25, 1, '2020-12-07', NULL, 1, 0, '2020-12-07 19:01:08', '2020-12-07 19:01:08', NULL),
(23, 28, 1, '2020-12-07', NULL, 1, 0, '2020-12-07 19:21:21', '2020-12-07 19:21:21', NULL),
(24, 65, 1, '2020-12-08', NULL, 1, 0, '2020-12-08 18:58:51', '2020-12-08 18:58:51', NULL),
(25, 66, 1, '2020-12-08', NULL, 1, 0, '2020-12-08 21:15:47', '2020-12-08 21:15:47', NULL),
(26, 59, 1, '2020-12-09', NULL, 1, 0, '2020-12-09 18:25:45', '2020-12-09 18:25:45', NULL),
(27, 58, 2, '2020-12-09', NULL, 1, 0, '2020-12-09 19:52:56', '2020-12-09 19:52:56', NULL),
(28, 35, 1, '2020-12-09', NULL, 1, 0, '2020-12-09 21:12:32', '2020-12-09 21:12:32', NULL),
(29, 67, 1, '2020-12-12', NULL, 1, 0, '2020-12-12 19:08:52', '2020-12-12 19:08:52', NULL),
(30, 20, 1, '2020-12-12', NULL, 1, 0, '2020-12-12 19:45:53', '2020-12-12 19:45:53', NULL),
(31, 38, 1, '2020-12-12', NULL, 1, 0, '2020-12-12 19:51:30', '2020-12-12 19:51:30', NULL),
(32, 37, 1, '2020-12-13', NULL, 1, 0, '2020-12-13 17:58:57', '2020-12-13 17:58:57', NULL),
(33, 68, 1, '2020-12-14', NULL, 1, 0, '2020-12-14 18:53:15', '2020-12-14 18:53:15', NULL),
(34, 16, 1, '2020-12-14', NULL, 1, 0, '2020-12-14 20:16:41', '2020-12-14 20:16:41', NULL),
(35, 29, 1, '2020-12-14', NULL, 1, 0, '2020-12-14 20:25:47', '2020-12-14 20:25:47', NULL),
(36, 30, 1, '2020-12-14', NULL, 1, 0, '2020-12-14 21:06:26', '2020-12-14 21:06:26', NULL),
(37, 46, 1, '2020-12-14', NULL, 1, 0, '2020-12-14 21:33:52', '2020-12-14 21:33:52', NULL),
(38, 31, 1, '2020-12-15', NULL, 1, 0, '2020-12-15 17:20:47', '2020-12-15 17:20:47', NULL),
(39, 32, 1, '2020-12-15', NULL, 1, 0, '2020-12-15 17:38:05', '2020-12-15 17:38:05', NULL),
(40, 33, 1, '2020-12-15', NULL, 1, 0, '2020-12-15 18:29:53', '2020-12-15 18:29:53', NULL),
(41, 34, 1, '2020-12-15', NULL, 1, 0, '2020-12-15 18:55:12', '2020-12-15 18:55:12', NULL),
(42, 36, 1, '2020-12-15', NULL, 1, 0, '2020-12-15 19:39:05', '2020-12-15 19:39:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('tulitarafdar250@gmail.com', '$2y$10$2OFyzf0mx/g1jBnmOARSu.mAKYufihQiXdBws3H7IM6gBcz.UvtaK', '2020-12-05 22:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `payment_order_lists`
--

CREATE TABLE `payment_order_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pay_rep_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_reports`
--

CREATE TABLE `payment_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `invoice_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `buy_price` double(9,2) DEFAULT NULL,
  `sell_price` double(9,2) NOT NULL,
  `updated_price` double(9,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `reward` int(11) NOT NULL DEFAULT '0',
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pid`, `shop_id`, `category_id`, `description`, `name`, `stock`, `buy_price`, `sell_price`, `updated_price`, `is_active`, `is_featured`, `reward`, `approved_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '316068053741', 3, 1, '<p>❄️ Free Home Delivery ❄️</p><p>❄️Description-</p><ul><li>Material - Cotton, Block Print&nbsp;</li><li>5 hat long</li><li>2 hat length</li></ul><p>❄️ Delivery Time:&nbsp;</p><ul><li>Inside Dhaka within 3/4 days&nbsp;</li><li>outside Dhaka within 7/8 days&nbsp;</li></ul><p>✅Disclaimer :</p><p>👉 Product color may slightly vary due to photographic lighting sources or your monitor settings.</p><p>👉❗Please Read The Products Description &amp; Order Rules Before Placing Your Order❗</p>', 'Shawl', 10, NULL, 799.00, NULL, 1, 0, 0, '2020-12-01 19:49:34', '2020-12-01 19:49:34', '2020-12-01 19:49:34', NULL),
(2, '316068062071', 3, 1, '<p>❄️ Free Home Delivery ❄️</p><p>❄️Description-</p><ul><li>Material - Cotton, Block Print </li><li>5 hat long</li><li>2 hat length</li></ul><p>❄️ Delivery Time:</p><ul><li>Inside Dhaka within 3/4 days </li><li>outside Dhaka within 7/8 days </li></ul><p>✅Disclaimer :</p><p>👉 Product color may slightly vary due to photographic lighting sources or your monitor settings.</p><p>👉❗Please Read The Products Description & Order Rules Before Placing Your Order❗</p>', 'Shawl', 10, NULL, 799.00, NULL, 1, 0, 0, '2020-12-01 20:03:27', '2020-12-01 20:03:27', '2020-12-01 20:05:21', NULL),
(3, '416068078292', 4, 2, '<div dir=\"auto\" style=\"font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">বিয়ের সিজনে এই লাল জুম শাড়ির ডিমান্ড যেনো আকাশচুম্বী <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🥰\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tea/1/16/1f970.png\" style=\"border: 0px;\"></span></div><div dir=\"auto\" style=\"font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">যেকোনো অনুষ্ঠান হোক কিংবা রেগুলার অফিসের স্টাইল সকল জায়গায় ক্লাসিক সাজ এনে দিতে পারে এই শাড়িটি। </div>', 'টেম্পল জুম শাড়ি', 4, NULL, 960.00, NULL, 1, 0, 0, '2020-12-01 20:30:29', '2020-12-01 20:30:29', '2020-12-01 21:04:08', NULL),
(4, '416068090192', 4, 2, '<p>কুর্তি&nbsp;</p><p>সুতি কাপড়&nbsp;</p><p>কালো কাপড়ে সুন্দর স্কীন প্রিন্ট করা কুর্তি।&nbsp;</p><p>গোল ডিজাইন এর সুন্দর কুর্তি এটি।</p>', 'কুর্তি', 10, NULL, 900.00, NULL, 0, 0, 0, '2020-12-01 20:50:19', '2020-12-01 20:50:19', '2020-12-01 21:03:00', NULL),
(5, '416068093353', 4, 3, '<p>কুর্তি&nbsp;</p><p>সুতি কাপড়&nbsp;</p><p>কালো কাপড়ে সুন্দর স্কীন প্রিন্ট করা কুর্তি।&nbsp;</p><p>গোল ডিজাইন এর সুন্দর কুর্তি এটি।</p>', 'কুর্তি', 10, NULL, 900.00, NULL, 0, 0, 0, '2020-12-01 20:55:35', '2020-12-01 20:55:35', '2020-12-01 21:02:54', NULL),
(6, '416068097583', 4, 3, '<p>কুর্তি&nbsp;</p><p>সুতি কাপড়&nbsp;</p><p>কালো কাপড়ে সুন্দর স্কীন প্রিন্ট করা কুর্তি।&nbsp;</p><p>গোল ডিজাইন এর সুন্দর কুর্তি এটি।</p>', 'কুর্তি', 2, NULL, 900.00, NULL, 1, 0, 0, '2020-12-01 21:02:38', '2020-12-01 21:02:38', '2020-12-01 21:02:38', NULL),
(7, '516068132214', 5, 4, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">চায়েএএএ গরম!!!!<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"😌\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t81/1/16/1f60c.png\" style=\"border: 0px;\"></span></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ছোট্ট কেটলি চার্ম, গ্লাস বিডস,কাঠপুতির মিশেলে একটা মালা।<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💙\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/1f499.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💙\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/1f499.png\" style=\"border: 0px;\"></span></div>', 'কেটলি মালা', 4, NULL, 95.00, NULL, 1, 0, 0, '2020-12-01 22:00:21', '2020-12-01 22:00:21', '2020-12-01 22:00:21', NULL),
(8, '516068149814', 5, 4, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍀\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t87/1/16/1f340.png\" style=\"border: 0px;\"></span>অম্বিক<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍀\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t87/1/16/1f340.png\" style=\"border: 0px;\"></span></div><div dir=\"auto\" style=\"font-family: inherit;\">বোহেমিয়ান ধাঁচের বাজেট ফ্রেন্ডলি একটা মালা।আদিম যুগের পয়সা,মেটালের পাতা ও ফুলের সাথে কাঠপুতি,নেপালি বিডস,সুতোর বিডসের কম্বিনেশনে।</div></div>', 'অম্বিক', 5, NULL, 105.00, NULL, 1, 0, 0, '2020-12-01 22:29:41', '2020-12-01 22:29:41', '2020-12-01 22:29:41', NULL),
(9, '1816068976135', 18, 5, '<p>সুতি আনস্টিচ ড্রেস</p><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ওড়না জর্জেট</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">আলাদা হাতা আছে</div>', 'সুতি আনস্টিচ ড্রেস', 1, NULL, 730.00, NULL, 1, 0, 0, '2020-12-02 21:26:53', '2020-12-02 21:26:53', '2020-12-02 21:26:53', NULL),
(10, '1816068978855', 18, 5, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">এই গর্জিয়াস কাপড় গুলো বাইরে পড়ার জন্যে একদম পার্ফেক্ট! মানিয়ে যাবে যেকোন কারো সাথেই</span><br></p>', 'সুতি আনস্টিচ ড্রেস', 1, NULL, 1450.00, NULL, 1, 0, 0, '2020-12-02 21:31:25', '2020-12-02 21:31:25', '2020-12-02 21:31:25', NULL),
(11, '1716068998976', 17, 6, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Size:72/30 </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Medium coverage</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Price -280৳</div>', 'Diamond  georgette hijab', 2, NULL, 280.00, NULL, 1, 0, 0, '2020-12-02 22:04:57', '2020-12-02 22:04:57', '2020-12-02 22:04:57', NULL),
(12, '1716069003272', 17, 2, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ব্লকের শাড়ি  উইথ ডলার ওয়ার্ক </span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><img height=\"16\" width=\"16\" alt=\"🥰\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tea/1/16/1f970.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">  কয়েকটা শাড়ির আচলে  টাসেল লাগানো আছে </span><br></p>', 'ব্লক শাড়ি', 1, NULL, 999.00, NULL, 1, 0, 0, '2020-12-02 22:12:07', '2020-12-02 22:12:07', '2020-12-02 22:12:07', NULL),
(13, '1716069004692', 17, 2, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ব্লকের শাড়ি  উইথ ডলার ওয়ার্ক </span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><img height=\"16\" width=\"16\" alt=\"🥰\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tea/1/16/1f970.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">  কয়েকটা শাড়ির আচলে  টাসেল লাগানো আছে </span><br></p>', 'ব্লক শাড়ি', 1, NULL, 999.00, NULL, 1, 0, 0, '2020-12-02 22:14:29', '2020-12-02 22:14:29', '2020-12-02 22:14:29', NULL),
(14, '816069707257', 8, 7, '<p>টেপা পুতুল এর কানের দুল শখ করে কালো দুই জোড়া বানিয়েছি। সাথে জুড়ে দিয়েছি রুদ্রাক্ষ আর কাঠের পুতি পুতুলকে সাজিয়েছি পিতলের পুতি দিয়ে । অনেকেই টেপা পুতুল এর কানের দুল চেয়েছিলেন কালো মাত্র দুই জোড়া আছে সবার পছন্দ হলে সামনে আরো আসবে।&nbsp;</p><p>মূল্য 320 টাকা +পার্সেল।</p><p>কালো টেপা</p><p>মাটির কানের_দুল</p><p>হাতে বানানো_গয়না&nbsp;</p>', 'টেপা পুতুল দুল', 2, NULL, 320.00, NULL, 1, 0, 0, '2020-12-03 17:45:25', '2020-12-03 17:45:25', '2020-12-03 17:45:25', NULL),
(15, '816069711234', 8, 4, '<p>এলোকেশী মা</p><p>কাঠের_গয়না</p><p>মৃৎ</p>', 'এলোকেশী মা মালা', 2, NULL, 350.00, NULL, 1, 0, 0, '2020-12-03 17:52:03', '2020-12-03 17:52:03', '2020-12-03 17:52:03', NULL),
(16, '816069714114', 8, 4, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">এরকম একটা মালা গলায় থাকলে আমার মনে হয়না বেশি কিছু আর পড়তে হবে।বিয়ে বাড়ি কিংবা সাংস্কৃতিক অনুষ্ঠান, এমন গয়নায় নজর সবার যাবেই একবার। গাড় রঙের কাপড়ের উপর ফুটে উঠবে বেশি সুন্দর করে। দেশী হাতে বানানো ভিন্নধর্মী গয়নায় এখন থেকে সাজি,মন্দ কি !!</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"> খুব যত্ন করে সময় নিয়ে বেনী পাকিয়ে পাকিয়ে কড়ি, সাথে সুতা তার সাথে পিতলের ঘন্টা জুরে বানানো হয়েছে এ-ই গয়না।</span><br></p>', 'লক্ষ্মীছড়া', 2, NULL, 570.00, NULL, 1, 0, 0, '2020-12-03 17:56:51', '2020-12-03 17:56:51', '2020-12-03 17:56:51', NULL),
(17, '816069721573', 8, 3, '<p>আল্পনা</p><p>হাতে আঁকা জামা</p><p>এই জামাটার সুন্দর দিক গলাটা খালি, গলায় যেই গয়নাই পড়া হয় মেরুন রং এর উপর ভাসে সুন্দর আর এন্ডি সিল্কের কটির মত করে পেস্ট করে তাতে হাতে আঁকা আলপনা।। আল্পনা আঁকা পূজার আরেক সংস্কৃতি, সেটাকে মাথায় রেখেই এই নকশা। খুব সাধারন&nbsp; নকশা কিন্তু সেটার রং আর নকশা মিশে অসাধারণ এক রুপ এসেছে।</p><p>কাপড়ের মাঝে আছে তাতের সুতার কাজ করা কটন আর কটির কাপড় এন্ডি সিল্ক&nbsp;</p><p>এই জামাটা স্টিচ বানিয়ে দিচ্ছি এবার অনেকেই বানিয়ে দেওয়ায় খুশি হয়েছেন।তাই এইবার বানিয়ে দিবো&nbsp;</p><p>মূল্য স্টিচ ২১৫০টাকা+পারসেল&nbsp;</p><p>আনস্টিচ ২০০০ টাকা+ পারসেল</p>', 'আলপনা জামা', 2, NULL, 2150.00, NULL, 1, 0, 0, '2020-12-03 18:09:17', '2020-12-03 18:09:17', '2020-12-03 18:09:17', NULL),
(18, '816069722635', 8, 5, '<p>আল্পনা</p><p>হাতে_আঁকা_জামা</p><p>এই জামাটার সুন্দর দিক গলাটা খালি, গলায় যেই গয়নাই পড়া হয় মেরুন রং এর উপর ভাসে সুন্দর আর এন্ডি সিল্কের কটির মত করে পেস্ট করে তাতে হাতে আঁকা আলপনা।। আল্পনা আঁকা পূজার আরেক সংস্কৃতি, সেটাকে মাথায় রেখেই এই নকশা। খুব সাধারন&nbsp; নকশা কিন্তু সেটার রং আর নকশা মিশে অসাধারণ এক রুপ এসেছে।</p><p>কাপড়ের মাঝে আছে তাতের সুতার কাজ করা কটন আর কটির কাপড় এন্ডি সিল্ক&nbsp;</p><p>এই জামাটা স্টিচ বানিয়ে দিচ্ছি এবার অনেকেই বানিয়ে দেওয়ায় খুশি হয়েছেন।তাই এইবার বানিয়ে দিবো&nbsp;</p><p>মূল্য স্টিচ ২১৫০টাকা+পারসেল&nbsp;</p><p>আনস্টিচ ২০০০ টাকা+ পারসেল</p>', 'হাতে আঁকা জামা', 2, NULL, 2000.00, NULL, 1, 0, 0, '2020-12-03 18:11:03', '2020-12-03 18:11:03', '2020-12-03 18:11:03', NULL),
(19, '816069724534', 8, 4, '<p>কালো মুখোশ</p><p>মাটির গয়না</p><p>মাটির এই কালো রং টা একদম প্রাকৃতিক, বিশেষভাবে পোড়ানোতে এই রং এসেছে।আমার খুব প্রিয়, কারণ এক একটা এক এক রং। কারো সাথে কারো মিল নেই। ছবি পোস্টের আগেই ২ টা দখল হয়ে গেছে।</p><p>আর বাকি যা আছে তা এখন নিশ্চিত করলে কবে আসবে বলা যাচ্ছে না।</p><p>ইনবক্সে যোগাযোগ করুন</p><p>বিনিময় ৩৫০টাকা+পারসেল</p>', 'কালো মুখোশ মালা', 2, NULL, 350.00, NULL, 1, 0, 0, '2020-12-03 18:14:13', '2020-12-03 18:14:13', '2020-12-03 18:14:13', NULL),
(20, '916069738322', 9, 2, '<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">  <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t8/1/16/1f341.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t8/1/16/1f341.png\" style=\"border: 0px;\"></span>আড়ং হাফসিল্ক ব্লক পিন্ট শাড়ি<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t8/1/16/1f341.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t8/1/16/1f341.png\" style=\"border: 0px;\"></span></div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍀\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t87/1/16/1f340.png\" style=\"border: 0px;\"></span>এটি খুবই  আরামদায়ক এবং যে-কোনো জায়গায় নিত্য নতুন বিভিন্ন আয়োজনে সহজেই ক্যারি করা যায়।</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"👉\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t51/1/16/1f449.png\" style=\"border: 0px;\"></span>ঢাকার ভেতর কুরিয়া চার্জ: ৮০/-</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"👉\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t51/1/16/1f449.png\" style=\"border: 0px;\"></span> ঢাকার বাইরে কুরিয়ার চার্জ: ১২০/</div></div>', 'আড়ং হাফসিল্ক ব্লক প্রিন্ট শাড়ি', 2, NULL, 1350.00, NULL, 1, 0, 0, '2020-12-03 18:37:12', '2020-12-03 18:37:12', '2020-12-03 18:37:12', NULL),
(21, '1116069762708', 11, 8, '<p>মালা:</p><ul><li>মালা \'সাইজ : প্রায় ১৪/১৮সে:মি</li></ul><p>কানের দুল</p><ul><li>ব্যাস :৪সে:মি</li></ul><p>আংটি :</p><ul><li>৪.৫সে:মি</li></ul><p>চুড়ি:</p><ul><li>চুড়ি \'র বাহিরের ব্যাস : ৯.৫ সে:মি</li><li>চুড়ি\'র ভিতরের ব্যাস : ৬সে:মি</li><li>পুরুত্ব :১সে:মি</li></ul>', 'শিউলী সেট', 4, NULL, 590.00, NULL, 1, 0, 0, '2020-12-03 19:17:50', '2020-12-03 19:17:50', '2020-12-03 19:17:50', NULL),
(22, '1116069766107', 11, 7, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">পদ্ম ও শিউলি কানের দুল</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">বিনিময় - ৪৫/- ( ১জোড়া)</span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><br></span><br></p>', 'পদ্ম-শিউলি কানের দুল', 4, NULL, 45.00, NULL, 1, 0, 0, '2020-12-03 19:23:30', '2020-12-03 19:23:30', '2020-12-03 19:23:30', NULL),
(23, '1116069777824', 11, 4, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">পাটলাবতী -১</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">দেবী দুর্গার অনেক নাম আছে। বাংলায় প্রচলিত পাঁচালী ও ছড়া গানে এই \"পাটলাবতী \" নাম টি উল্লেখ আছে। এই গয়না টি মাটি, পাট, কড়ি, রুদ্রাক্ষ ও বিভিন্ন উপকরণ দিযে তৈরী।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">এখানে ব্যবহৃত দুর্গার মুখমণ্ডল টি মাটি দিয়ে তৈরী করা।</span><br></p>', 'পাটলাবতী -১', 1, NULL, 590.00, NULL, 1, 0, 0, '2020-12-03 19:43:03', '2020-12-03 19:43:02', '2020-12-03 19:43:03', NULL),
(24, '1116069778284', 11, 4, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">পাটলাবতী -২</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">দেবী দুর্গার অনেক নাম আছে। বাংলায় প্রচলিত পাঁচালী ও ছড়া গানে এই \"পাটলাবতী \" নাম টি উল্লেখ আছে। এই গয়না টি মাটি, পাট, কড়ি, রুদ্রাক্ষ ও বিভিন্ন উপকরণ দিযে তৈরী।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">এখানে ব্যবহৃত দুর্গার মুখমণ্ডল টি মাটি দিয়ে তৈরী করা।</span><br></p>', 'পাটলাবতী -২', 1, NULL, 549.00, NULL, 1, 0, 0, '2020-12-03 19:43:48', '2020-12-03 19:43:48', '2020-12-03 19:43:48', NULL),
(25, '1216069790024', 12, 4, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">\" মঙ্গল ঘট \"</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">একেবারে ইউনিক একটি সেট ৷ ম্যাটেরিয়াল ফেব্রিক এর কিন্তু কাঠের মতোই শক্ত ৷ অনেক বড় এটা ৷ একটি মাত্র সেট বানিয়েছি ৷ ঘট হয়তো অন্য ডিজাইন এ আরো বানাবো ৷ কিন্তু এটা এক পিস ই থাকবে ৷ মানে যার ঝুলিতে যাবেন এবারে পূজোতে সে থাকবেন সবার থেকে আলাদা ৷</span><br></p>', 'মঙ্গল ঘট', 1, NULL, 450.00, NULL, 1, 0, 0, '2020-12-03 20:03:22', '2020-12-03 20:03:22', '2020-12-03 20:03:22', NULL),
(26, '1216069799238', 12, 8, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">এটাকে অনেক রঙে রাঙিয়েছি ৷ কারণ কিছু জামদানি শাড়ি আছে যা কয়েকটা রঙের মিশ্রণে থাকে ৷ তাই এরকম করেই এই জামদানি গয়নার সেটা টা তৈরি করলাম ৷</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">কানের দুল, আংটি, গলার মালা<br></span><br></p>', 'রঙ্গিলা জামদানি', 2, NULL, 370.00, NULL, 1, 0, 0, '2020-12-03 20:18:43', '2020-12-03 20:18:43', '2020-12-03 20:18:43', NULL),
(27, '1216069804774', 12, 4, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">এটা টেরাকোটা দূর্গা মুখ ৷খুব সুন্দর মুকুট এর কারুকার্য আর মায়ের মুখ টা দেখতে ভীষণ মিষ্টি ৷ পাটের বেইজের উপর বসানো ৷ চারদিকে পমপম দিয়ে ডিজাইন করা আর মায়ের মুখের নিচে কয়েকটা কড়ি ৷ এখানে মাতৃ মুখ টাই এতো সুন্দর যে আর এটাতে বেশি কিছু এড করার প্রয়োজন পড়ে নি ৷</span><br></p>', 'টেরাকোটা দুর্গা পেন্ডেন্ট', 1, NULL, 470.00, NULL, 1, 0, 0, '2020-12-03 20:27:57', '2020-12-03 20:27:57', '2020-12-03 20:27:57', NULL),
(28, '1316069816264', 13, 4, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">পঞ্চমী </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য : ১৭০/</div></div>', 'পঞ্চমী', 3, NULL, 170.00, NULL, 1, 0, 0, '2020-12-03 20:47:06', '2020-12-03 20:47:06', '2020-12-03 20:47:06', NULL),
(29, '1316069816964', 13, 4, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">ত্রিমূর্তি </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য : ১৬০/-</div></div>', 'ত্রিমূর্তি', 3, NULL, 160.00, NULL, 1, 0, 0, '2020-12-03 20:48:16', '2020-12-03 20:48:16', '2020-12-03 20:48:16', NULL),
(30, '1316069818197', 13, 7, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মৃন্ময়ী</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">ঝুমকা প্রেমীদের জন্য</div><div dir=\"auto\" style=\"font-family: inherit;\">ছোট্ট এক জোড়া ঝুমকা কি সুন্দর বদলে দিতে পারে পুরো সাজটাই। </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য  :  ২৮০/- </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">*স্টকে খুব সীমিত পরিমাণে  আছে </div><div dir=\"auto\" style=\"font-family: inherit;\">** অনুমতি ছাড়া ছবি অন্যত্র ব্যবহার নিষেধ!   </div></div>', 'মৃন্ময়ী ঝুমকা', 2, NULL, 280.00, NULL, 1, 0, 0, '2020-12-03 20:50:19', '2020-12-03 20:50:19', '2020-12-03 20:50:19', NULL),
(31, '1316069819217', 13, 7, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">রঙ্গিলা </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য : ২৮০/- </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">পূজার যেকোন সাজের সাথে মানিয়ে যাবে এই ছোট্ট ঝুমকা জোড়া<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"😊\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t7f/1/16/1f60a.png\" style=\"border: 0px;\"></span></div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"> *এই সুন্দরী ঝুমকা আমার কাছে মাত্র এবং কেবলমাত্র ৪জোড়া আছে।তারমধ্যে একজোড়া আমার  <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"😶\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tb1/1/16/1f636.png\" style=\"border: 0px;\"></span>।  </div><div dir=\"auto\" style=\"font-family: inherit;\">**তবে চিন্তার কিছু নেই, প্রি-অর্ডার করা যাবে। </div><div dir=\"auto\" style=\"font-family: inherit;\">***আলোর কারণে রং এর সামান্য তারতম্য  হতে পারে।  </div></div>', 'রঙ্গিলা', 4, NULL, 280.00, NULL, 1, 0, 0, '2020-12-03 20:52:01', '2020-12-03 20:52:01', '2020-12-03 20:52:01', NULL),
(32, '1316069822134', 13, 4, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মৎস</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">শেল কাট মৎস আকৃতির পেনডেন্ট আর একুয়া রংয়ের বিডস এ এবার এলো মৎস।  </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য : ৫২০/- </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">*লেংথ কাস্টমাইজ করা যাবে</div></div>', 'মৎস মালা', 3, NULL, 520.00, NULL, 1, 0, 0, '2020-12-03 20:56:53', '2020-12-03 20:56:53', '2020-12-03 20:56:53', NULL),
(33, '1316069823727', 13, 7, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">সবার পছন্দের এই দুলটি পূজা উপলক্ষে আবার এনেছি... ইনবক্সে অনেকেই দুলটা চাচ্ছিলেন দিতে পারি নি। রং কাস্টমাইজেরও অনুরোধ ছিলো। তাই আবার নিয়ে আসলাম।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">মূল্য : ২৫০/-</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">*রং কাস্টমাইজ করা যাবে যদি আমাদের কাছে থাকে।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">*কাস্টমাইজের ক্ষেত্রে মূল্যের তারতম্য হতে পারে কাজের উপর ভিত্তি করে। ইনবক্সে কথা বলে নিবেন।</span><br></p>', 'এম্ব্রোয়ডারি দুল', 2, NULL, 250.00, NULL, 1, 0, 0, '2020-12-03 20:59:32', '2020-12-03 20:59:32', '2020-12-03 20:59:32', NULL),
(34, '1316069827197', 13, 7, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">কড়িদুল </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">কড়ি, রুদ্রাক্ষ আর ঘুঙ্গুরের মিলনে এলো কড়িদুল। যেকোন রঙের যেকোন পোশাকের সাথে মানিয়ে যাবে অনায়াসে। </div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">মূল্য : ২২০</div></div>', 'কড়িদুল', 3, NULL, 220.00, NULL, 1, 0, 0, '2020-12-03 21:05:19', '2020-12-03 21:05:19', '2020-12-03 21:05:19', NULL),
(35, '1416069851102', 14, 2, '<p><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">&nbsp;শাড়ি</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">&nbsp;হাফসিল্ক কাপড়ের উপর হ্যান্ডপেইন্টের কাজ করা।।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">যেকোন কাপড়ে হ্যান্ডপেইন্ট করা যাবে।।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">ডিজাইন কাস্টমাইজ করা যাবে।।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">তোসর কাপড় দিয়ে পার দেয়া।।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">&nbsp;শাড়ির সাথে রানিং ব্লাউজ পিস নেই।। মেচিং ব্লাউজ পিস দেয়া আছে।। ব্লাউজ পিসে হ্যান্ডপেইন্টের কাজ করা।।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">মূল্যঃ শাড়ি- ২৪০০ টাকা</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">ব্লাউজ পিস - ৫০০ টাকা (আনরেডি)</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">প্রিঅর্ডার<br></span><br></p>', 'শাড়ি', 4, NULL, 2400.00, NULL, 1, 0, 0, '2020-12-03 21:45:10', '2020-12-03 21:45:10', '2020-12-03 21:45:10', NULL),
(36, '1416069853522', 14, 2, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span> শাড়ি</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span> হাফসিল্ক  কাপড়ের উপর হ্যান্ডপেইন্টের  কাজ করা।। </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span>যেকোন কাপড়ে হ্যান্ডপেইন্ট করা যাবে।।</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span>ডিজাইন কাস্টমাইজ করা যাবে।।</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🍂\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/1f342.png\" style=\"border: 0px;\"></span>রিবন দিয়ে পার দেয়া।।</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌿\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t1e/1/16/1f33f.png\" style=\"border: 0px;\"></span> শাড়ির সাথে রানিং ব্লাউজ পিস নেই।। মেচিং ব্লাউজ পিস দেয়া আছে।। ব্লাউজ পিসে স্টিচের  কাজ করা।।</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">মূল্যঃ শাড়ি-২৫৬০ টাকা</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ব্লাউজ পিস - ৬০০ টাকা (আনরেডি)</div>', 'হাফসিল্ক শাড়ি', 3, NULL, 2560.00, NULL, 1, 0, 0, '2020-12-03 21:49:12', '2020-12-03 21:49:12', '2020-12-03 21:49:12', NULL);
INSERT INTO `products` (`id`, `pid`, `shop_id`, `category_id`, `description`, `name`, `stock`, `buy_price`, `sell_price`, `updated_price`, `is_active`, `is_featured`, `reward`, `approved_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(37, '2116070719432', 21, 2, '<p><span style=\"color: rgb(5, 5, 5); font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">গাঢ় সিঁদুর লাল তসর শাড়িতে শাড়িতে কালো রঙে নিজস্ব ডিজাইনে ব্লকপ্রিন্ট করা হয়েছে,</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">গ্লোসি একটা বুনন শাড়িটায়</span><br style=\"color: rgb(5, 5, 5); font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: \"Segoe UI Historic\", \"Segoe UI\", Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">সাড়ে ১৩ হাত শাড়ি (রানিং ব্লাউজ পিস)</span><br></p>', 'লাল তসর শাড়ি', 5, NULL, 1590.00, NULL, 1, 0, 0, '2020-12-04 21:52:23', '2020-12-04 21:52:23', '2020-12-04 21:55:20', NULL),
(38, '2116070723293', 21, 3, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">প্রি-অর্ডার</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">ফেব্রিক- সফট খাদি কটন কাপড়ে নিজস্ব ডিজাইনে ব্লকপ্রিন্ট করা হয়েছে</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">সাইজ ৩৪-৪২ (৪৪/৪৬ ও দেয়া যাবে প্রাইস বাড়বে)</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">বিনিময় মূল্য ১১২০ টাকা</span><br></p>', 'সূর্যমুখী কুর্তি', 4, NULL, 1120.00, NULL, 1, 0, 0, '2020-12-04 21:58:49', '2020-12-04 21:58:49', '2020-12-04 21:58:49', NULL),
(39, '2116070982431', 21, 1, '<p>শীতের চাদর</p><p>ফেব্রিকঃ ভিসকস (আড়ং কোয়ালিটি)&nbsp;</p><p>সূর্যমুখী ফুলের ডিজাইনে ব্লকপ্রিন্ট করা</p><p>সাইজ ৮৪/৩২ ইঞ্চি&nbsp;</p>', 'শীতের চাদর', 6, NULL, 790.00, NULL, 1, 0, 0, '2020-12-05 05:10:43', '2020-12-05 05:10:43', '2020-12-05 05:10:43', NULL),
(40, '2116070982831', 21, 1, '<p>শীতের চাদর</p><p>ফেব্রিকঃ ভিসকস (আড়ং কোয়ালিটি)&nbsp;</p><p>সূর্যমুখী ফুলের ডিজাইনে ব্লকপ্রিন্ট করা</p><p>সাইজ ৮৪/৩২ ইঞ্চি&nbsp;</p>', 'শীতের চাদর', 5, NULL, 790.00, NULL, 1, 0, 0, '2020-12-05 05:11:23', '2020-12-05 05:11:23', '2020-12-05 05:11:23', NULL),
(41, '2116070983311', 21, 1, '<p>শীতের চাদর</p><p>ফেব্রিকঃ ভিসকস (আড়ং কোয়ালিটি)&nbsp;</p><p>সূর্যমুখী ফুলের ডিজাইনে ব্লকপ্রিন্ট করা</p><p>সাইজ ৮৪/৩২ ইঞ্চি&nbsp;</p>', 'শীতের চাদর', 5, NULL, 790.00, NULL, 1, 0, 0, '2020-12-05 05:12:11', '2020-12-05 05:12:11', '2020-12-05 05:12:11', NULL),
(42, '2116070983881', 21, 1, '<p>শীতের চাদর</p><p>ফেব্রিকঃ ভিসকস (আড়ং কোয়ালিটি)&nbsp;</p><p>সূর্যমুখী ফুলের ডিজাইনে ব্লকপ্রিন্ট করা</p><p>সাইজ ৮৪/৩২ ইঞ্চি&nbsp;</p>', 'শীতের চাদর', 5, NULL, 790.00, NULL, 1, 0, 0, '2020-12-05 05:13:08', '2020-12-05 05:13:08', '2020-12-05 05:13:08', NULL),
(43, '6016071047297', 60, 7, '<p><span style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><font face=\"inherit\"><span style=\"cursor: pointer; list-style: none; border-style: initial; border-color: initial; touch-action: manipulation; text-align: inherit; -webkit-tap-highlight-color: transparent;\"><b>শিউলিবন</b></span></font></span><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">&nbsp;</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">কাঠের ডাইসে তৈরি<br></span><br></p>', 'শিউলিবন দুল', 2, NULL, 180.00, NULL, 1, 0, 0, '2020-12-05 06:58:49', '2020-12-05 06:58:49', '2020-12-05 06:58:49', NULL),
(44, '6016071050937', 60, 7, '<p>পাখি দুল</p><p>সিলভার মেটাল এর তৈরি</p>', 'পাখি দুল', 2, NULL, 150.00, NULL, 1, 0, 0, '2020-12-05 07:04:53', '2020-12-05 07:04:53', '2020-12-05 07:04:53', NULL),
(45, '6316072367328', 63, 8, '<p>সিলভার গলা-কানের সেট&nbsp;&nbsp;&nbsp;&nbsp;</p>', 'সিলভার গহনা', 2, NULL, 450.00, NULL, 1, 1, 0, '2020-12-06 19:38:52', '2020-12-06 19:38:52', '2020-12-06 19:40:43', NULL),
(46, '6316072370338', 63, 8, '<p>সিলভার মেটাল আর সুতার কাজ করা গহনা&nbsp;&nbsp;&nbsp;&nbsp;</p>', 'গহনা', 3, NULL, 500.00, NULL, 1, 0, 0, '2020-12-06 19:43:53', '2020-12-06 19:43:53', '2020-12-06 19:43:53', NULL),
(47, '6316072374928', 63, 8, '<p>সুতা আর সিলভার মেটালের মিশ্রণে তৈরি এই মালাটি।</p><p>সেই সাথে মেটালের কানের দুল</p>', 'গহনা', 4, NULL, 350.00, NULL, 1, 0, 0, '2020-12-06 19:51:32', '2020-12-06 19:51:32', '2020-12-06 19:51:32', NULL),
(48, '5616072452372', 56, 2, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">পিণন / থামি শাড়ীতেও রয়েছে ডিস্কাউন্ট <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❤️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png\" style=\"border: 0px;\"></span></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ডেলিভারি চার্জ ঢাকার মধ্যে ৮০ টাকা । ঢাকার বাইরে ১৫০ টাকা ।</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">রেগুলার প্রাইস ১৬০০৳</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ডিস্কাউন্ট প্রাইস ১৫০০৳</div>', 'পিণন / থামি শাড়ী', 4, NULL, 1600.00, NULL, 1, 0, 0, '2020-12-06 22:00:37', '2020-12-06 22:00:37', '2020-12-07 01:49:56', NULL),
(49, '5616072486142', 56, 2, '<p>পিণন / থামি হাফ সিল্ক শাড়ী&nbsp;</p><p>রানিং ব্লাউজ পিস সহ ১৩.৫ হাতের শাড়ী&nbsp;</p><p>প্রাইস ১৫০০৳</p>', 'পিণন / থামি শাড়ী', 5, NULL, 1500.00, NULL, 1, 0, 0, '2020-12-06 22:56:54', '2020-12-06 22:56:54', '2020-12-06 22:56:54', NULL),
(50, '1416073167513', 14, 3, '<p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">ওয়ানপিস</span></font></p><p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">জর্জেট কাপড়ের উপর স্টিচের কাজ করা।</span></font></p><p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">ডিজাইন কাস্টমাইজ করা যাবে।।।</span></font></p><p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">কাপড়ের রঙ পরিবর্তন করা যাবে।।</span></font></p><p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">মূল্যঃ&nbsp;</span></font></p><ul><li><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">১২৮০ টাকা (আনরেডি)</span></font></li><li><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">১৪৮০ টাকা (রেডি)</span></font></li></ul><p><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"></span><br></p>', 'ওয়ানপিস', 6, NULL, 1480.00, NULL, 1, 0, 0, '2020-12-07 17:52:31', '2020-12-07 17:52:31', '2020-12-07 17:52:31', NULL),
(51, '1416073169415', 14, 5, '<p>ওয়ানপিস</p><p>জর্জেট কাপড়ের উপর স্টিচের কাজ করা।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।।</p><p>কাপড়ের রঙ পরিবর্তন করা যাবে।।</p><p>মূল্যঃ&nbsp;</p><ul><li>১২৮০ টাকা (আনরেডি)</li><li>১৪৮০ টাকা (রেডি)</li></ul>', 'আনরেডি ওয়ানপিস', 4, NULL, 1280.00, NULL, 1, 0, 0, '2020-12-07 17:55:41', '2020-12-07 17:55:41', '2020-12-07 17:55:41', NULL),
(52, '1416073171133', 14, 3, '<p>ওয়ানপিস</p><p>জর্জেট কাপড়ের উপর স্টিচের কাজ করা।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।।</p><p>কাপড়ের রঙ পরিবর্তন করা যাবে।।</p><p>মূল্যঃ&nbsp;</p><ul><li>১২৮০ টাকা (আনরেডি)</li><li>১৪৮০ টাকা (রেডি)</li></ul>', 'ওয়ানপিস', 4, NULL, 1480.00, NULL, 1, 0, 0, '2020-12-07 17:58:33', '2020-12-07 17:58:33', '2020-12-07 17:58:33', NULL),
(53, '1416073172515', 14, 5, '<p>ওয়ানপিস</p><p>জর্জেট কাপড়ের উপর স্টিচের কাজ করা।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।।</p><p>কাপড়ের রঙ পরিবর্তন করা যাবে।।</p><p>মূল্যঃ </p><ul><li>১২৮০ টাকা (আনরেডি)</li><li>১৪৮০ টাকা (রেডি)</li></ul>', 'আনরেডি ওয়ানপিস', 3, NULL, 1280.00, NULL, 1, 0, 0, '2020-12-07 18:00:51', '2020-12-07 18:00:51', '2020-12-07 18:02:16', NULL),
(54, '1416073177693', 14, 3, '<p>ওয়ানপিস</p><p>সুতি কাপড়ের উপর হ্যান্ডপেইন্টের কাজ করা।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।।</p><p>কাপড়ের রঙ পরিবর্তন করা যাবে।।</p><p>মূল্যঃ&nbsp;</p><ul><li>৮৫০ টাকা (আনরেডি)</li><li>১১৮০ টাকা (রেডি)</li></ul>', 'ওয়ানপিস', 5, NULL, 1180.00, NULL, 1, 0, 0, '2020-12-07 18:09:30', '2020-12-07 18:09:29', '2020-12-07 18:09:30', NULL),
(55, '1416073179822', 14, 2, '<p>শাড়ি</p><p>হাফসিল্ক কাপড়ের উপর হ্যান্ডপেইন্টের কাজ করা।।</p><p>যেকোন কাপড়ে হ্যান্ডপেইন্ট করা যাবে।।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।</p><p>তোসর দিয়ে পার দেয়া যাবে৷</p><p>শাড়ির সাথে রানিং ব্লাউজ পিস নেই।।&nbsp;</p><p>মেচিং ব্লাউজ পিস দেয়া যাবে।।</p>', 'হাফসিল্ক শাড়ি', 5, NULL, 2200.00, NULL, 1, 0, 0, '2020-12-07 18:13:02', '2020-12-07 18:13:02', '2020-12-07 18:13:02', NULL),
(56, '1416073181332', 14, 2, '<p>শাড়ি</p><p>হাফসিল্ক&nbsp; কাপড়ের উপর ব্লকপ্রিন্টের&nbsp; কাজ করা।।&nbsp;</p><p>যেকোন কাপড়ে ব্লকপ্রিন্ট করা যাবে।।</p><p>ডিজাইন কাস্টমাইজ করা যাবে।।</p><p>তোসর দিয়ে পার দেয়া যাবে৷&nbsp;</p><p>শাড়ির সাথে রানিং ব্লাউজ পিস নেই।। মেচিং ব্লাউজ পিস দেয়া যাবে।।</p><p>ব্লাউজ পিসে স্টিচের কাজ করা।।</p>', 'শাড়ি', 6, NULL, 1740.00, NULL, 1, 0, 0, '2020-12-07 18:15:33', '2020-12-07 18:15:33', '2020-12-07 18:15:33', NULL),
(57, '5616074881582', 56, 2, '<p>তাঁতের সুতি শাড়ী&nbsp;</p><p>রানিং ব্লাউজ পিস সহ ১৪ হাতের শাড়ী&nbsp;</p><p>ডিস্কাউন্ট প্রাইস ১৮৫০৳</p>', 'তাঁতের সুতি শাড়ী', 8, NULL, 1850.00, NULL, 1, 0, 0, '2020-12-09 17:29:18', '2020-12-09 17:29:18', '2020-12-09 17:29:18', NULL),
(58, '5916074920828', 59, 8, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">(পেন্ডেন্ট,  কানের টপ) </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div>', 'সেট - ১', 4, NULL, 160.00, NULL, 1, 0, 0, '2020-12-09 18:34:42', '2020-12-09 18:34:42', '2020-12-09 18:34:42', NULL),
(59, '5916074923718', 59, 8, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">(পেন্ডেন্ট,  কানের টপ)</span><br></p>', 'সেট - ২', 5, NULL, 140.00, NULL, 1, 0, 0, '2020-12-09 18:39:31', '2020-12-09 18:39:31', '2020-12-09 18:39:31', NULL),
(60, '5916074924788', 59, 8, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">সেট-৩ </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">পছন্দমতো রাউন্ড/ ট্রায়াংগেল টপ দেয়া যাবে, সেক্ষেত্রে প্রাইসিং আলাদা) </div>', 'সেট-৩', 5, NULL, 120.00, NULL, 1, 0, 0, '2020-12-09 18:41:18', '2020-12-09 18:41:18', '2020-12-09 18:41:18', NULL),
(61, '5916074926028', 59, 8, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">সেট-৪ </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">জবা আংটি, প্রজাপতি পেন্ডেন্ট</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">( আলাদা নেয়া যাবে </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">জবা রিং - ৪৫/- </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">প্রজাপতি পেন্ডেন্ট - ৯০/-)</div>', 'সেট - ৪', 5, NULL, 150.00, NULL, 1, 0, 0, '2020-12-09 18:43:22', '2020-12-09 18:43:22', '2020-12-09 18:43:22', NULL),
(62, '5916074927748', 59, 8, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">সেট-৫ </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">( ফ্লোরাল আংটি, ফ্লোরাল কানের টপ) </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">( আলাদা নেয়া যাবে সেক্ষেত্রে </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">আংটি- ৪৫/- </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">টপ-৩৬/-)  </div>', 'সেট-৫', 5, NULL, 90.00, NULL, 1, 0, 0, '2020-12-09 18:46:14', '2020-12-09 18:46:14', '2020-12-09 18:46:14', NULL),
(63, '5916074937728', 59, 8, '<p>কয়েন নেকপিস ( চোকার হিসেবে বা ঝুলিয়ে পড়া যাবে)&nbsp;&nbsp;</p><p>মেরুন কালারের সিরামিকের বিডস এবং এন্টিকের কয়েনের মিশেলে তৈরী খুবই সুন্দর একটি নেকপিস! সাথে আছে কয়েনের দুল!&nbsp;&nbsp;</p><p><br></p>', 'কয়েন নেকপিস', 6, NULL, 170.00, NULL, 1, 0, 0, '2020-12-09 19:02:52', '2020-12-09 19:02:52', '2020-12-09 19:02:52', NULL),
(64, '1916074946065', 19, 5, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">Bihar silk 3 piece</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">Price 550tk</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">Delivery Charge :80/150</div></div>', 'Bihar Silk Dress', 5, NULL, 550.00, NULL, 1, 0, 0, '2020-12-09 19:16:46', '2020-12-09 19:16:46', '2020-12-09 19:16:46', NULL),
(65, '1916074947475', 19, 5, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">Bihar Silk 3 piece</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">Price 550tk</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><br></div></div>', 'Bihar silk', 5, NULL, 550.00, NULL, 1, 0, 0, '2020-12-09 19:19:07', '2020-12-09 19:19:07', '2020-12-09 19:19:07', NULL),
(66, '1916074956532', 19, 2, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">জুম শাড়ি  </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ব্লাউজ পিস নেই</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">এক কালার</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div>', 'জুম শাড়ি', 10, NULL, 750.00, NULL, 1, 0, 0, '2020-12-09 19:34:13', '2020-12-09 19:34:13', '2020-12-09 19:34:13', NULL),
(67, '5816074977992', 58, 2, '<p><font color=\"#050505\" face=\"Segoe UI Historic, Segoe UI, Helvetica, Arial, sans-serif\"><span style=\"font-size: 15px; letter-spacing: normal;\">সিল্ক শাড়ি</span></font><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">নরম আরামদায়ক এ সিল্ক শাড়ির মূল্য ২৬৫০৳</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">বারো হাত শাড়ি ।</span><br></p>', 'সিল্ক শাড়ি', 5, NULL, 2650.00, NULL, 1, 0, 0, '2020-12-09 20:09:59', '2020-12-09 20:09:59', '2020-12-09 20:09:59', NULL),
(68, '5816074982702', 58, 2, '<p>Silk Sharee&nbsp;</p><p>Screen print&nbsp;</p><p>Silk শাড়ি এবার শীতে করবো বলেছিলাম, সেই ধারাবাহিকতায় পোস্ট করছি ছবি ।&nbsp;</p><p>নরম ,আরামদায়ক ও বারো হাতের শাড়িগুলো উৎসবের মৌসুমকেই চিন্তা করে তৈরি করেছি ।&nbsp;</p><p>আলো ও ডিভাইসের জন্য রঙের হালকা তারতম্য লাগতে পারে।</p>', 'সিল্ক শাড়ি', 5, NULL, 2800.00, NULL, 1, 0, 0, '2020-12-09 20:17:50', '2020-12-09 20:17:50', '2020-12-09 20:17:50', NULL),
(69, '6416076062438', 64, 8, '<p>The necklace is made with antique colour beads and plastic yellow crystal beads in the first layer. The second layer contains breaded silk thread. And the third layer containing medium size antique colour beads with small golden metal beads and antique bail beads. It has a tassel behind so that it can be used both as choker and long necklace. It will look perfect with saree or kurtie.<br></p>', 'Three Layered Yellow Thread Braided Necklace with Beads and Metal Bail Beads', 1, NULL, 390.00, NULL, 1, 0, 0, '2020-12-11 02:17:23', '2020-12-11 02:17:23', '2020-12-11 02:17:23', NULL),
(70, '3816077578968', 38, 8, '<p>পাটের গয়না</p>', 'পাটের গয়না', 10, NULL, 450.00, NULL, 1, 0, 0, '2020-12-12 20:24:56', '2020-12-12 20:24:56', '2020-12-12 20:24:56', NULL),
(71, '3716078357858', 37, 8, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">পাটের তৈরি আরেকটি গয়নার সেট।লাল আর কালোর মিশ্রণে তৈরি।</div><div dir=\"auto\" style=\"font-family: inherit;\">এরকম একেকটা গয়না বানানোর পিছনে কত গল্প,কত ঘটনা,কত ভাললাগা,মন্দলাগা,কত স্বপ্ন থাকে তা শুধু একজন কারিগরই জানে।</div><div dir=\"auto\" style=\"font-family: inherit;\">হ্যা, আমিই এই গয়নার কারিগর।আবার আমার গয়নার মডেল ও আমি।</div><div dir=\"auto\" style=\"font-family: inherit;\">লাল কালোর মিশ্রনে তৈরি বলেই এই গয়নাটির নাম \" কাজল ভোমরা\"</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌺\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t99/1/16/1f33a.png\" style=\"border: 0px;\"></span>কাজল ভোমরা<span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🌺\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t99/1/16/1f33a.png\" style=\"border: 0px;\"></span>  </div></div>', 'কাজল ভোমরা', 5, NULL, 350.00, NULL, 1, 0, 0, '2020-12-13 18:03:05', '2020-12-13 18:03:05', '2020-12-13 18:03:05', NULL),
(72, '1516078371804', 15, 4, 'সিলভার মেটাল পেন্ডেন্ট', 'সিলভার পেন্ডেন্ট', 2, NULL, 500.00, NULL, 1, 0, 0, '2020-12-13 18:26:20', '2020-12-13 18:26:20', '2020-12-13 18:26:20', NULL),
(73, '1516078374768', 15, 8, 'বেগুনী-গোল্ডেন কালারের সুন্দর জুয়েলার্স সেট&nbsp;', 'গহনা', 2, NULL, 1400.00, NULL, 1, 0, 0, '2020-12-13 18:31:16', '2020-12-13 18:31:16', '2020-12-13 18:31:16', NULL),
(74, '1516078379778', 15, 8, '<p>Red Silver combo Jewellery </p>', 'Jewellery Set', 2, NULL, 600.00, NULL, 1, 0, 0, '2020-12-13 18:39:37', '2020-12-13 18:39:37', '2020-12-13 18:51:53', NULL),
(75, '1516078381489', 15, 9, '<p>সিলভার চোকার<br></p>', 'সিলভার চোকার', 2, NULL, 450.00, NULL, 1, 0, 0, '2020-12-13 18:42:28', '2020-12-13 18:42:28', '2020-12-13 18:42:28', NULL),
(76, '1516078382478', 15, 8, '<p>সিলভার চোকার<br></p>', 'সিলভার চোকার', 3, NULL, 650.00, NULL, 1, 1, 0, '2020-12-13 18:44:07', '2020-12-13 18:44:07', '2020-12-13 18:45:49', NULL),
(77, '1516078383178', 15, 8, '<p>সিলভার মেটাল মালা<br></p>', 'সিলভার মালা', 2, NULL, 550.00, NULL, 1, 0, 0, '2020-12-13 18:45:17', '2020-12-13 18:45:17', '2020-12-13 18:45:17', NULL),
(78, '6016078457098', 60, 8, '<p>ময়ূরাক্ষী</p><p>🍁সিলভার কালারের ময়ূর বেইজ আর রাউন্ড বেইজ এবং পেন্ডেন্ট এর অসাধারণ একটা কম্বিনেশন করা নেকপিস সেট।&nbsp;</p><p>🍁 এখন বিয়ের মৌসুম চলছে।&nbsp; নিজেকে অন্যরকম সাজানোর জন্য এটা একদম পারফেক্ট।</p>', 'ময়ূরাক্ষী গয়না', 2, NULL, 580.00, NULL, 1, 0, 0, '2020-12-13 20:48:29', '2020-12-13 20:48:29', '2020-12-13 20:48:29', NULL),
(79, '3160784768110', 3, 10, '<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span> Winter Offer <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span></div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"✅\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png\" style=\"border: 0px;\"></span>Product Type:</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Winter Jacket</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Material:Cotton</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Denim</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"✅\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png\" style=\"border: 0px;\"></span>Size:</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>XL (Chest - 41,Long - 24 )</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>L (Chest - 39 ,Long - 22 )</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>M (Chest - 37 ,Long - 20 )</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"*️⃣\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/2a_20e3.png\" style=\"border: 0px;\"></span>Delivery Time: Inside Dhaka within 3/4 days and outside Dhaka within 7/8 days </div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"*️⃣\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/2a_20e3.png\" style=\"border: 0px;\"></span>Stock Limited <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tc5/1/16/1f481.png\" style=\"border: 0px;\"></span></div></div>', 'Winter Jacket', 10, NULL, 1500.00, NULL, 1, 0, 0, '2020-12-13 21:21:21', '2020-12-13 21:21:21', '2020-12-13 21:21:21', NULL),
(80, '3160784783910', 3, 10, '<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span> Winter Offer <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"❄️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t2e/1/16/2744.png\" style=\"border: 0px;\"></span></div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"✅\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png\" style=\"border: 0px;\"></span>Product Type:</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Winter Jacket</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Material:Cotton</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>Denim</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"✅\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t33/1/16/2705.png\" style=\"border: 0px;\"></span>Size:</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>XL (Chest - 41,Long - 24 )</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>L (Chest - 39 ,Long - 22 )</div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"▪️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/25aa.png\" style=\"border: 0px;\"></span>M (Chest - 37 ,Long - 20 )</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"*️⃣\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/2a_20e3.png\" style=\"border: 0px;\"></span>Delivery Time: Inside Dhaka within 3/4 days and outside Dhaka within 7/8 days </div><div dir=\"auto\" style=\"font-family: inherit;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"*️⃣\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t89/1/16/2a_20e3.png\" style=\"border: 0px;\"></span>Stock Limited <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💁\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tc5/1/16/1f481.png\" style=\"border: 0px;\"></span></div></div>', 'Winter Jacket', 10, NULL, 1500.00, NULL, 1, 0, 0, '2020-12-13 21:23:59', '2020-12-13 21:23:59', '2020-12-13 21:23:59', NULL),
(81, '29160793177311', 29, 11, '<div class=\"kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">নতুন গুলজি উপস্থাপন করছি</div></div><div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\" style=\"overflow-wrap: break-word; margin: 0.5em 0px 0px; white-space: pre-wrap; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><div dir=\"auto\" style=\"font-family: inherit;\">3 পিস সম্পূর্ণ সেলাই করা</div><div dir=\"auto\" style=\"font-family: inherit;\">ডেলিভারি 30 দিন</div></div>', 'গুলজি সেট', 2, NULL, 5250.00, NULL, 1, 0, 0, '2020-12-14 20:42:53', '2020-12-14 20:42:53', '2020-12-14 20:42:53', NULL),
(82, '3016079341294', 30, 4, '<p>রিজিন বেজ লম্বামালা</p><p>৩০০টাকা</p><p>আরও কালার আছে।&nbsp;</p><p>হিজাবি আপুরা দেখতে পারেন,</p><p>নিলে প্রি-অর্ডার করতে হবে।</p>', 'রিজিন বেল মালা', 4, NULL, 300.00, NULL, 1, 0, 0, '2020-12-14 21:22:09', '2020-12-14 21:22:09', '2020-12-14 21:22:09', NULL),
(83, '4616079354872', 46, 2, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💥\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png\" style=\"border: 0px;\"></span>ঢাকাই ঐতিহ্যবাহী  তাঁতের জামদানী </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💥\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png\" style=\"border: 0px;\"></span>সম্পুর্ন হাতে বোনা</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💥\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png\" style=\"border: 0px;\"></span>অল ওভার কাজ করা</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💥\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png\" style=\"border: 0px;\"></span>রেশম সুতার জামদানী </div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"💥\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png\" style=\"border: 0px;\"></span>নিজস্ব তাঁতে বোনা</div>', 'জামদানী শাড়ি', 5, NULL, 6500.00, NULL, 1, 0, 0, '2020-12-14 21:44:47', '2020-12-14 21:44:47', '2020-12-14 21:44:47', NULL),
(84, '4616079357225', 46, 5, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">এক্সক্লুসিভ জামদানী টুপিছ।</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">ফুল কটন।</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">পানিতে ওয়াশেবল।</span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">আনস্টিচ সম্পূর্ণ হাতে বোনা</span><br></p>', 'জামদানী টুপিছ', 5, NULL, 2500.00, NULL, 1, 0, 0, '2020-12-14 21:48:42', '2020-12-14 21:48:42', '2020-12-14 21:48:42', NULL),
(85, '31160800675712', 31, 12, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">Jennifer Aniston Sketch Portrait</span></p>', 'Sketch Portrait', 2, NULL, 800.00, NULL, 1, 0, 0, '2020-12-15 17:32:37', '2020-12-15 17:32:37', '2020-12-15 17:32:37', NULL);
INSERT INTO `products` (`id`, `pid`, `shop_id`, `category_id`, `description`, `name`, `stock`, `buy_price`, `sell_price`, `updated_price`, `is_active`, `is_featured`, `reward`, `approved_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(86, '31160800684612', 31, 12, '<div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Billie Eilish <span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: inherit;\"><img height=\"16\" width=\"16\" alt=\"🥰\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tea/1/16/1f970.png\" style=\"border: 0px;\"></span></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Medium: Graphite pencil on paper</div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\"><br></div><div dir=\"auto\" style=\"font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal; white-space: pre-wrap;\">Can order customize portrait sketch. Price will vary. </div>', 'Sketch Portrait', 2, NULL, 800.00, NULL, 1, 0, 0, '2020-12-15 17:34:06', '2020-12-15 17:34:06', '2020-12-15 17:34:06', NULL),
(87, '31160800691012', 31, 12, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">Portrait of a true politician.</span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"❤️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png\" style=\"border: 0px;\"></span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">Father of nation-</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">Bangabandhu Sheikh Mujibur Rahman&nbsp;</span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"❤️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png\" style=\"border: 0px;\"></span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">You will always be in our heart&nbsp;</span><span class=\"pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu\" style=\"margin: 0px 1px; height: 16px; width: 16px; display: inline-flex; vertical-align: middle; font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; color: rgb(5, 5, 5); font-size: 15px; letter-spacing: normal;\"><img height=\"16\" width=\"16\" alt=\"❤️\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/2764.png\" style=\"border: 0px;\"></span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">Medium-Pencil on paper.</span><br></p>', 'Sheikh Mujibur Rahman Sketch', 2, NULL, 910.00, NULL, 1, 0, 0, '2020-12-15 17:35:10', '2020-12-15 17:35:10', '2020-12-15 17:35:10', NULL),
(88, '3216080077675', 32, 5, '<p>কাতান থ্রি-পিস ড্রেস</p><p>আনস্টিচ</p>', 'কাতান ড্রেস', 8, NULL, 650.00, NULL, 1, 0, 0, '2020-12-15 17:49:27', '2020-12-15 17:49:27', '2020-12-15 17:49:27', NULL),
(89, '3216080078665', 32, 5, '<p>কাতান থ্রি-পিস ড্রেস</p><p>আনস্টিচ</p>', 'কাতান ড্রেস', 8, NULL, 650.00, NULL, 1, 0, 0, '2020-12-15 17:51:06', '2020-12-15 17:51:06', '2020-12-15 17:51:06', NULL),
(90, '3216080084931', 32, 1, '<p>যারা একটু reasonable প্রাইস এর মধ্যে চাদর চাচ্ছেন তাদের জন্য 🥰</p><p>আড়ং প্রিন্ট চাদর&nbsp;</p><div><br></div>', 'প্রিন্ট চাদর', 10, NULL, 500.00, NULL, 1, 0, 0, '2020-12-15 18:01:33', '2020-12-15 18:01:33', '2020-12-15 18:01:33', NULL),
(91, '3216080088161', 32, 1, '<p>যারা একটু reasonable প্রাইস এর মধ্যে চাদর চাচ্ছেন তাদের জন্য 🥰</p><p>আড়ং প্রিন্ট চাদর&nbsp;</p>', 'আড়ং প্রিন্ট চাদর', 10, NULL, 500.00, NULL, 1, 0, 0, '2020-12-15 18:06:56', '2020-12-15 18:06:56', '2020-12-15 18:06:56', NULL),
(92, '3216080089511', 32, 1, '<p>যারা একটু reasonable প্রাইস এর মধ্যে চাদর চাচ্ছেন তাদের জন্য 🥰</p><p>আড়ং প্রিন্ট চাদর&nbsp;</p>', 'আড়ং প্রিন্ট চাদর', 10, NULL, 500.00, NULL, 1, 0, 0, '2020-12-15 18:09:11', '2020-12-15 18:09:11', '2020-12-15 18:09:11', NULL),
(93, '3216080091411', 32, 1, '<p>যারা একটু reasonable প্রাইস এর মধ্যে চাদর চাচ্ছেন তাদের জন্য 🥰</p><p>আড়ং প্রিন্ট চাদর&nbsp;</p>', 'আড়ং প্রিন্ট চাদর', 10, NULL, 500.00, NULL, 1, 0, 0, '2020-12-15 18:12:21', '2020-12-15 18:12:21', '2020-12-15 18:12:21', NULL),
(94, '3316080103482', 33, 2, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">নাগরঙ্গ।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">সুতি শাড়িতে ব্লক ছাপা।</span><br></p>', 'নাগরঙ্গ শাড়ি', 5, NULL, 1299.00, NULL, 1, 0, 0, '2020-12-15 18:32:28', '2020-12-15 18:32:28', '2020-12-15 18:32:28', NULL),
(95, '3316080109362', 33, 2, '<p><b>প্রি-অর্ডার</b></p><p>মেঘ&nbsp;শাড়ি</p><p>সুতি শাড়িতে ব্লক ছাপা।</p>', 'মেঘ শাড়ি', 5, NULL, 1299.00, NULL, 1, 0, 0, '2020-12-15 18:42:16', '2020-12-15 18:42:16', '2020-12-15 18:43:23', NULL),
(96, '3316080109432', 33, 2, '<p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><b>প্রি-অর্ডার</b></span></p><p><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">হরিত।</span><br style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\"><span style=\"color: rgb(5, 5, 5); font-family: &quot;Segoe UI Historic&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif; font-size: 15px; letter-spacing: normal;\">সুতি শাড়িতে ব্লক ছাপা।</span></p><p><br></p>', 'হরিত শাড়ি', 5, NULL, 1299.00, NULL, 1, 0, 0, '2020-12-15 18:42:23', '2020-12-15 18:42:23', '2020-12-15 18:42:23', NULL),
(97, '3316080110932', 33, 2, '<p><b>প্রি-অর্ডার</b></p><p>শুক্ল শাড়ি</p><p>হাফসিল্ক মসলিন শাড়িতে ব্লক ছাপা।</p>', 'শুক্ল শাড়ি', 5, NULL, 1399.00, NULL, 1, 0, 0, '2020-12-15 18:44:53', '2020-12-15 18:44:53', '2020-12-15 18:44:53', NULL),
(98, '3316080111532', 33, 2, '<p><b>প্রি-অর্ডার</b></p><p>শুভ্র শাড়ি</p><p>সুতি শাড়িতে ব্লক ছাপা।</p>', 'শুভ্র শাড়ি', 5, NULL, 1299.00, NULL, 1, 0, 0, '2020-12-15 18:45:53', '2020-12-15 18:45:53', '2020-12-15 18:45:53', NULL),
(99, '3316080112132', 33, 2, '<p><b>প্রি-অর্ডার</b></p><p>পলাকা শাড়ি</p><p>সুতি শাড়িতে ব্লক ছাপা।</p>', 'পলাকা শাড়ি', 5, NULL, 1299.00, NULL, 1, 0, 0, '2020-12-15 18:46:53', '2020-12-15 18:46:53', '2020-12-15 18:46:53', NULL),
(100, '3416080135514', 34, 4, '<p>কাঠের ডাইস</p><p>রাউন্ড আকৃতি</p>', 'পোলকা মালা', 5, NULL, 200.00, NULL, 1, 0, 0, '2020-12-15 19:25:51', '2020-12-15 19:25:51', '2020-12-15 19:25:51', NULL),
(101, '3416080139088', 34, 8, '<p>ড্রিম কেচার পেন্ডেন্ট</p><p>কাঠের ডাইস</p>', 'ড্রিম কেচার পেন্ডেন্ট', 2, NULL, 250.00, NULL, 1, 0, 0, '2020-12-15 19:31:48', '2020-12-15 19:31:48', '2020-12-15 19:31:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `type_id`, `name`, `note`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Shawl', NULL, 1, '2020-12-01 19:44:22', '2020-12-01 19:46:26', NULL),
(2, 1, 'Sharee', NULL, 1, '2020-12-01 20:27:24', '2020-12-01 20:27:24', NULL),
(3, 1, 'Kurti/Tops', NULL, 1, '2020-12-01 20:45:40', '2020-12-01 20:45:40', NULL),
(4, 4, 'Pendent', NULL, 1, '2020-12-01 21:58:57', '2020-12-01 21:58:57', NULL),
(5, 1, 'Unstich Dress', NULL, 1, '2020-12-02 21:22:42', '2020-12-02 21:22:42', NULL),
(6, 1, 'Scarf', NULL, 1, '2020-12-02 22:02:52', '2020-12-02 22:02:52', NULL),
(7, 4, 'Earrings', NULL, 1, '2020-12-03 17:44:04', '2020-12-03 17:44:04', NULL),
(8, 4, 'Jewellery Set', NULL, 1, '2020-12-03 19:16:11', '2020-12-03 19:16:11', NULL),
(9, 4, 'Chokar', NULL, 1, '2020-12-13 18:41:48', '2020-12-13 18:41:48', NULL),
(10, 3, 'Jacket', NULL, 1, '2020-12-13 21:16:55', '2020-12-13 21:16:55', NULL),
(11, 1, 'Salwar Kameez', NULL, 1, '2020-12-14 20:40:50', '2020-12-14 20:40:50', NULL),
(12, 5, 'Sketch', NULL, 1, '2020-12-15 17:32:01', '2020-12-15 17:32:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_comments`
--

CREATE TABLE `product_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_replies`
--

CREATE TABLE `product_replies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL,
  `reply` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `name`, `note`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cloths', NULL, 1, '2020-12-01 19:39:09', '2020-12-01 19:39:09', NULL),
(2, 'Shoes', NULL, 1, '2020-12-01 19:39:19', '2020-12-01 19:39:19', NULL),
(3, 'Winter Collection', NULL, 1, '2020-12-01 19:41:54', '2020-12-01 19:41:54', NULL),
(4, 'Jewellery', NULL, 1, '2020-12-01 21:57:58', '2020-12-01 21:57:58', NULL),
(5, 'Painting', NULL, 1, '2020-12-15 17:31:25', '2020-12-15 17:31:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resellers`
--

CREATE TABLE `resellers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reseller_id` bigint(20) UNSIGNED NOT NULL,
  `business_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reseller_products`
--

CREATE TABLE `reseller_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reseller_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `riders`
--

CREATE TABLE `riders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rider_id` bigint(20) UNSIGNED NOT NULL,
  `rider_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rider_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rider_delivery_order_lists`
--

CREATE TABLE `rider_delivery_order_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rider_delivery_reports`
--

CREATE TABLE `rider_delivery_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rider_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', '2020-12-01 03:41:40', '2020-12-01 03:41:40', NULL),
(2, 'Shop', 'shop', '2020-12-01 03:41:40', '2020-12-01 03:41:40', NULL),
(3, 'Reseller', 'reseller', '2020-12-01 03:41:40', '2020-12-01 03:41:40', NULL),
(4, 'Rider', 'rider', '2020-12-01 03:41:40', '2020-12-01 03:41:40', NULL),
(5, 'Customer', 'customer', '2020-12-01 03:41:40', '2020-12-01 03:41:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) UNSIGNED NOT NULL,
  `shop_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inside_city` int(11) DEFAULT NULL,
  `outside_city` int(11) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `shop_id`, `shop_mobile`, `shop_email`, `shop_address`, `owner_name`, `owner_mobile`, `owner_email`, `owner_address`, `facebook`, `instagram`, `twitter`, `inside_city`, `outside_city`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '01716387653', 'sultansshop27@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;124, West Pirmoholla, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">124, West Pirmoholla, Sylhet</span><br></p>', 'Sultan Shadman', '01716387653', 'sultansshop27@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;124, West Pirmoholla, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">124, West Pirmoholla, Sylhet</span><br></p>', NULL, NULL, NULL, 0, 0, NULL, '2020-11-08 00:57:54', '2020-12-01 19:01:49', NULL),
(2, 4, '01955842528', 'sholoanabangaliwana@gmail.com', 'Shibgonj, Sylhet', 'Prothima Toma', '01955842528', 'sholoanabangaliwana@gmail.com', 'Shibgonj, Sylhet', NULL, NULL, NULL, 50, 130, NULL, '2020-11-08 02:34:19', '2020-12-01 20:19:25', NULL),
(3, 5, '01624924559', 'labonee4@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Bypass road,Habiganj sadar,sylhet,Bangladesh&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Bypass road, Habiganj Sadar, Sylhet, Bangladesh</span><br></p>', 'Labonee Banik', '01624924559', 'labonee4@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Bypass road,Habiganj sadar,sylhet,Bangladesh&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Bypass road, Habiganj Sadar, Sylhet, Bangladesh</span><br></p>', 'https://www.facebook.com/WWW.NEEBENJONA.COM.BD', NULL, NULL, NULL, NULL, NULL, '2020-11-08 03:32:08', '2020-12-01 21:50:13', NULL),
(4, 6, '01760631834', 'rupaag97@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mohona B, 40/1, Sotish chandra saroni&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Mohona B, 40/1, Sotish chandra saroni</span><br></p>', 'Rupali Das', '01760631834', 'rupaag97@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mohona B, 40/1, Sotish chandra saroni&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Mohona B, 40/1, Sotish chandra saroni</span><br></p>', '2020-11-08 05:04:15', '2020-11-08 05:04:15', NULL),
(5, 7, '01628428232', 'modhuprm@gmail.com', '<p>sylhet</p>', 'test', '01628428232', 'modhuprm@gmail.com', '<p><span style=\"letter-spacing: 0.2px;\">sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-11 01:42:30', '2020-11-11 01:42:30', NULL),
(6, 8, '01755856211', 'madhurideby@gmail.com', '<p>Permanent:Niloy-117,21;West Notun para;Sunamgonj.</p><p>Present :Dariapara,Mirjajangal,Sylhet</p>', 'Madhuri Deby Rumi', '01755856211', 'madhurideby@gmail.com', '<p style=\"letter-spacing: 0.2px;\">Permanent:Niloy-117,21;West Notun para;Sunamgonj.</p><p style=\"letter-spacing: 0.2px;\">Present :Dariapara,Mirjajangal,Sylhet</p>', NULL, NULL, NULL, 50, 120, NULL, '2020-11-12 04:18:45', '2020-11-23 01:14:17', NULL),
(7, 9, '01748467281', 'mahmudasarker@gmail.com', '<p><font face=\"Times New Roman\">ঠিকানাঃ&nbsp;ভৈরব, কিশোরগঞ্জ। বর্তমান অবস্তানঃ Elephant Road ,Dhaka</font></p>', 'মাহমুদা সরকার', '01748467281', 'mahmudasarker@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">ঠিকানাঃ&nbsp;ভৈরব, কিশোরগঞ্জ। বর্তমান অবস্তানঃ Elephant Road ,Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, '<p>ok</p>', '2020-11-12 12:09:52', '2020-11-12 12:09:52', NULL),
(8, 10, '01744222860', 'bushrapromi@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">গ্রামঃগৌরাঙের চক,</span><br style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\"><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">পোঃশরিফাবাদ</span><br style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\"><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">থানা ও জেলাঃ হবিগঞ্জ</span><br></p>', 'Tahmina Akther Bushra Promi', '01744222860', 'bushrapromi@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">গ্রামঃগৌরাঙের চক,</span><br style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\"><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">পোঃশরিফাবাদ</span><br style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\"><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">থানা ও জেলাঃ হবিগঞ্জ</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-12 12:15:58', '2020-11-12 12:15:58', NULL),
(9, 11, '01788495870', 'pritomtalukder4133@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">সিলেট, শাহপরান, বাহুবল আ/এ</span><br></p>', 'Pritom Taklukder', '01788495870', 'pritomtalukder4133@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">সিলেট, শাহপরান, বাহুবল আ/এ</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-12 12:24:29', '2020-11-12 12:24:29', NULL),
(10, 12, '01755140071', 'pronomishutradhar@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">Amu Tea Estate, Moulvibazar</span><br></p>', 'Pronomi Tumpa', '01755140071', 'pronomishutradhar@gmail.com', '<p><span style=\"color: rgb(34, 34, 34); font-family: Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size: small; letter-spacing: normal;\">Amu Tea Estate, Moulvibazar</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-12 12:29:15', '2020-11-12 12:29:15', NULL),
(11, 13, '01913585023', 'tasnim.archi@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">92,Mousumi,agpara,Mirabazar,Sylhet</span><br></p>', 'Tasnim Kamal Mim', '01913585023', 'tasnim.archi@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">92,Mousumi,agpara,Mirabazar,Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-13 01:41:48', '2020-11-13 01:41:48', NULL),
(12, 14, '01749776655', '1206thbouli@gmail.com', '<p><font face=\"Times New Roman\">Borobazar,Amborkhana.</font></p>', 'Thoudam Bouli/Thoudam Mouli/Labonti Chanu', '01749776655', '1206thbouli@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">Borobazar,Amborkhana</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-13 01:55:34', '2020-11-13 01:55:34', NULL),
(13, 15, '01742244744', 'mmrittika7423@gmail.com', '<p><font face=\"Times New Roman\">23/2 Soptodipa,Jamtola,Sylhet</font></p>', 'Shafi Ullah,Paromita Das Trisha,Suchona Devi,Papia Dash', '01742244744', 'mmrittika7423@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">23/2 Soptodipa,Jamtola,Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-13 02:09:05', '2020-11-13 02:09:05', NULL),
(14, 16, '01314662269', 'nobonitasarees2019@gmail.com', '<p><font face=\"Times New Roman\">Dowarabazar,Sylhet.&nbsp;</font></p>', 'Partha Sarathi Roy', '01314662269', 'nobonitasarees2019@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">Dowarabazar,Sylhet.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-13 02:16:13', '2020-11-13 02:16:13', NULL),
(15, 17, '01788063456', 'harrydee767@gmail.com', '<p><font face=\"Times New Roman\">&nbsp;97,Pushpayon,Senpara,Shibgonj,Sylhet.&nbsp;</font></p>', 'Sorna Das', '01788063456', 'harrydee767@gmail.com', '<p><span style=\"font-family: &quot;Times New Roman&quot;; letter-spacing: 0.2px;\">97,Pushpayon,Senpara,Shibgonj,Sylhet.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-13 02:21:06', '2020-11-13 02:21:06', NULL),
(16, 18, '01632506078', 'jisanahmed1995@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;29/A, Sagor Dighir Par, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">29/A, Sagor Dighir Par, Sylhet</span><br></p>', '1. Nahiyan Ahmed Moumi 2. Nusrat Alam Nadia 3. Wasif Ahmed Jisan', '01632506078', 'jisanahmed1995@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13.3333px; letter-spacing: normal;\">29/A, Sagor Dighir Par, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\"><b><u>Owner</u></b>: </span></p><p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">1. Nahiyan Ahmed Moumi\r\n2. Nusrat Alam Nadia\r\n3. Wasif Ahmed Jisan</span><br></p>', '2020-11-23 02:05:57', '2020-11-23 02:05:57', NULL),
(17, 19, '01799695640', 'suzanarahman705@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sornali D/3, Terminal Road, Barthokola, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sornali D/3, Terminal Road, Barthokola, Sylhet</span><br></p>', 'Suzana Rahman', '01799695640', 'suzanarahman705@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sornali D/3, Terminal Road, Barthokola, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sornali D/3, Terminal Road, Barthokola, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, '<p>Shop: Glowing_splint</p><p><span style=\"letter-spacing: 0.2px;\">suzanarahman705@gmail.com</span></p><p>01799695640</p><p>pass: 12345678</p>', '2020-11-23 02:15:21', '2020-11-23 02:15:21', NULL),
(18, 20, '01646005321', 'debnondini@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Moulvibazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Moulvibazar, Sylhet</span>&nbsp;&nbsp;&nbsp;&nbsp;<br></p>', 'Nimmi Das', '01646005321', 'debnondini@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Moulvibazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Moulvibazar, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-23 02:22:15', '2020-11-23 02:22:15', NULL),
(19, 21, '01858231019', 'ishkapon.bd@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mirpur dohs, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Mirpur dohs, Dhaka</span><br></p>', '1. Amatul Bushra  2. Mashtura shikder khushboo', '01858231019', 'aishiamatul@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mirpur dohs, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Mirpur dohs, Dhaka</span><br></p>', 'https://www.facebook.com/ishkapon.KA', NULL, NULL, NULL, NULL, '১৬ আনা বাঙালিয়ানা ধরে রাখার চেষ্টা করছি আমরা।\nআমাদের সাথেই থাকুন।', '2020-12-01 21:36:11', '2020-12-01 21:43:33', NULL),
(20, 24, '01720313876', 'mahmudulhassan01720@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Shamshernagar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Shamshernagar, Sylhet</span><br></p>', 'Mahmudul Hassan', '01720313876', 'mahmudulhassan01720@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Shamshernagar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Shamshernagar, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 05:04:25', '2020-12-02 05:04:25', NULL),
(21, 25, '01631015098', 'tamannajahan123456789@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Adress: Mirpur, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Adress: Mirpur, Dhaka</span><br></p>', 'অরিন্দিতা আইরি', '01631015098', 'tamannajahan123456789@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Adress: Mirpur, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Adress: Mirpur, Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 17:49:56', '2020-12-02 17:49:56', NULL),
(22, 28, '01750053950', 'mimjannat811@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Rangpur&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Rangpur</span><br></p>', 'Jannatul Ferdoush Mim', '01750053950', 'mimjannat811@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Rangpur&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Rangpur</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:06:05', '2020-12-02 18:06:05', NULL),
(23, 29, '01717132976', 'alisamiranur@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Antaranga 54 Kazitula Uchasarak Sylhet &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Antaranga 54 Kazitula Uchasarak Sylhet</span><br></p>', 'Samira Mridula', '01717132976', 'alisamiranur@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Antaranga 54 Kazitula Uchasarak Sylhet &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Antaranga 54 Kazitula Uchasarak Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:11:08', '2020-12-02 18:11:08', NULL),
(24, 30, '01912674462', 'opayel32@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dhanmondi, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Dhanmondi, Dhaka</span><br></p>', 'Oindrila Payel', '01912674462', 'opayel32@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dhanmondi, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:11139,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;14&quot;:{&quot;1&quot;:2,&quot;2&quot;:0},&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Dhanmondi, Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:15:35', '2020-12-02 18:15:35', NULL),
(25, 31, '01776516669', 'soumitrosomu1@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">52 r/A Amberkhana Borobazar,Sylhet.</span>&nbsp;&nbsp;&nbsp;&nbsp;<br></p>', 'Soumitro Singha', '01776516669', 'soumitrosomu1@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">52 r/A Amberkhana Borobazar,Sylhet.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:24:34', '2020-12-02 18:24:34', NULL),
(26, 32, '01644167073', 'eshitapoly96@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', 'Eshita Jahan Poly', '01644167073', 'eshitapoly96@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:29:08', '2020-12-02 18:29:08', NULL),
(27, 33, '01516342465', 'sabihaanjumasha2001@gmail.com', '৩২, চামেলিবাগ, শান্তিনগর, ঢাকা', 'Sabiha Anjum', '01516342465', 'sabihaanjumasha2001@gmail.com', '৩২, চামেলিবাগ, শান্তিনগর, ঢাকা', 'https://www.facebook.com/bedhonika', NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:30:32', '2020-12-15 18:31:13', NULL),
(28, 34, '01715720329', 'shuveccha75@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9091,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', 'Adhara Das Ratri', '01715720329', 'shuveccha75@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9091,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16776960},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:32:59', '2020-12-02 18:32:59', NULL),
(29, 35, '01701078883', 'naher.pinkey@gmail.com', '<p>Sylhet</p>', 'Samsoon Naher Pinkey', '01701078883', 'naher.pinkey@gmail.com', '<p><span style=\"letter-spacing: 0.2px;\">Sylhet</span><br></p>', NULL, NULL, NULL, 50, 130, '<p><br></p>', '2020-12-02 18:34:13', '2020-12-09 21:13:03', NULL),
(30, 36, '01680100992', 'saminaarna14023@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;রাসোস-৩২, সোনারপারা, সিলেট। &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9091,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16777215},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">রাসোস-৩২, সোনারপারা, সিলেট।</span><br></p>', 'সামিনা ভূঁইয়া অর্না', '01680100992', 'saminaarna14023@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;রাসোস-৩২, সোনারপারা, সিলেট। &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9091,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:16777215},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">রাসোস-৩২, সোনারপারা, সিলেট।</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:40:15', '2020-12-02 18:40:15', NULL),
(31, 37, '01717931910', 'lunaroy505@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">মোহনা ব্লক/ বি-৮৫\r\nকরেরপাড়া,পাটানটুলা,সিলেট।</span><br></p>', 'লুনা রায়', '01717931910', 'lunaroy505@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">মোহনা ব্লক/ বি-৮৫\r\nকরেরপাড়া,পাটানটুলা,সিলেট।</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:46:59', '2020-12-02 18:46:59', NULL),
(32, 38, '01681022942', 'rezwanahabibparly@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">House no-85 Len-7/A Housing Estate. Ambarkhana. Sylhet.</span><br></p>', 'Rezwana Habib', '01681022942', 'rezwanahabibparly@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">House no-85 Len-7/A Housing Estate. Ambarkhana. Sylhet.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:48:07', '2020-12-02 18:48:07', NULL),
(33, 39, '01789789436', 'swaroniikachakroborty@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', 'Swaronika Chakroborty', '01789789436', 'swaroniikachakroborty@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 18:55:22', '2020-12-02 18:55:22', NULL),
(34, 40, '01752212941', 'tulitarafdar250@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;ছাতক ,বাগবাড়ি&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">ছাতক ,বাগবাড়ি</span><br></p>', 'শ্রাবন্তি তরফদার তুলি', '01752212941', 'tulitarafdar250@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;ছাতক ,বাগবাড়ি&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">ছাতক ,বাগবাড়ি</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:13:41', '2020-12-02 19:13:41', NULL),
(35, 41, '01784188889', 'avinandamukti@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Upatyaka-84/1, Hasan Nagar</span><br></p><p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">3000 Sunamganj, Bangladesh</span><br></p>', 'Avinanda Mukti', '01784188889', 'avinandamukti@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Upatyaka-84/1, Hasan Nagar</span><br></p><p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">3000 Sunamganj, Bangladesh</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:16:37', '2020-12-02 19:16:37', NULL),
(36, 42, '01798768840', 'arporoy790@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', 'মনিষা রায়', '01798768840', 'arporoy790@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:17:56', '2020-12-02 19:17:56', NULL),
(37, 43, '01711335249', 'kalnirbake@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Lamabazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Lamabazar, Sylhet</span><br></p>', 'Humayun Kabir Jewel', '01711335249', 'kalnirbake@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Lamabazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Lamabazar, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:21:51', '2020-12-02 19:21:51', NULL),
(38, 44, '01750035337', 'azafori@gmail.com', '<p>Sreemongol</p>', 'Abdullah Al Zafori', '01750035337', 'azafori@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '<p><span style=\"letter-spacing: 0.2px;\">Sreemongol</span><br></p>', '2020-12-02 19:30:42', '2020-12-02 19:30:42', NULL),
(39, 45, '01611906101', 'aparnaranidas10@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dolia, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Dolia, Sylhet</span><br></p>', '1. Aparna Das 2. Sapna Akter', '01611906101', 'aparnaranidas10@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Dolia, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Dolia, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:34:18', '2020-12-02 19:34:18', NULL),
(40, 46, '01740402246', 'mirpannu2@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Demra, Dhaka-1360&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Demra, Dhaka-1360</span><br></p>', 'Mir Pannu', '01740402246', 'mirpannu2@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Demra, Dhaka-1360&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Demra, Dhaka-1360</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:39:21', '2020-12-02 19:39:21', NULL),
(41, 47, '01798009370', 'shutoshoili@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', 'Anik Chandra Paul', '01798009370', 'shutoshoili@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:41:21', '2020-12-02 19:41:21', NULL),
(42, 48, '01998878809', 'deepadev500@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Shibganj, Senpara, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Shibganj, Senpara, Sylhet</span><br></p>', 'Kritideepa Deb', '01998878809', 'deepadev500@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Shibganj, Senpara, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Shibganj, Senpara, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:50:24', '2020-12-02 19:50:24', NULL),
(43, 49, '01713811165', 'ananyadasm@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Seikhghat,luminous tower,sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Seikhghat,luminous tower,sylhet</span><br></p>', 'Ananya Das', '01713811165', 'ananyadasm@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Seikhghat,luminous tower,sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Seikhghat,luminous tower,sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:51:28', '2020-12-02 19:51:28', NULL),
(44, 50, '01796414482', 'troyeepurnasreepurkayastha@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;18/1, Islampur mejortila Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">18/1, Islampur mejortila Sylhet</span><br></p>', 'Purnasree purkayastha Troyee', '01796414482', 'troyeepurnasreepurkayastha@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;18/1, Islampur mejortila Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">18/1, Islampur mejortila Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:54:02', '2020-12-02 19:54:02', NULL),
(45, 51, '01679484366', 'charu22lata@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Jamtola, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Jamtola, Sylhet</span><br></p>', '1. Paromita Das Trisha 2. Sahanara Begum', '01679484366', 'charu22lata@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Jamtola, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Jamtola, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 19:56:06', '2020-12-02 19:56:06', NULL),
(46, 52, '01688562271', 'juismodak4@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;32/2 Somota, Chalibondor, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">32/2 Somota, Chalibondor, Sylhet</span><br></p>', 'জ্যোতিকা রাণী মোদক জুঁই', '01688562271', 'juismodak4@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;32/2 Somota, Chalibondor, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">32/2 Somota, Chalibondor, Sylhet</span><br></p>', '2020-12-02 19:56:59', '2020-12-02 19:56:59', NULL),
(47, 53, '01760497359', 'abhijayeeni@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Medical, Sylhet, Sylhet Division, Bangladesh\r\n</span></p><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\"><br></span></div>', 'Sushmita Dhar', '01760497359', 'abhijayeeni@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Tajpur, Osmaninagar, Sylhet\r\n</span></p><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\"><br></span></div>', '2020-12-02 20:00:31', '2020-12-02 20:00:31', NULL),
(48, 54, '01709366538', 'supriadabi199@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Sreemangal, Moulvibazar. </span><br></p>', 'Supriya Debnath', '01709366538', 'supriadabi199@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">Sreemangal, Moulvibazar. </span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:03:45', '2020-12-02 20:03:45', NULL),
(49, 55, '01770313434', 'sayem611@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">1. Miah Bari 60, Nobab Road Sylhet 3100\r\n2.  Dorgah mohollah,payra 13/2, Sylhet 3100</span><br></p>', '1. Sayem Ahmad Fahim 2. Nuzhat Sadar Chy', '01770313434', 'sayem611@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; letter-spacing: normal; white-space: pre-wrap;\">1. Miah Bari 60, Nobab Road Sylhet 3100\r\n2.  Dorgah mohollah,payra 13/2, Sylhet 3100</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:05:47', '2020-12-02 20:05:47', NULL),
(50, 56, '01634203910', 'tasmeha@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;House #02, Road #16, Sector #10, Uttara, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">House #02, Road #16, Sector #10, Uttara, Dhaka</span><br></p>', 'Tasmeha Sultana', '01634203910', 'tasmeha@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;House #02, Road #16, Sector #10, Uttara, Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">House #02, Road #16, Sector #10, Uttara, Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:06:44', '2020-12-02 20:06:44', NULL),
(51, 57, '01628140510', 'cherrypick06@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;kamalbazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">kamalbazar, Sylhet</span><br></p>', 'Humayra Akter Aysha', '01628140510', 'humayraaysha9906@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;kamalbazar, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">kamalbazar, Sylhet</span><br></p>', 'https://www.facebook.com/cherrypick.closet/', 'https://instagram.com/cherrypick26?igshid=13y5bcb61kfsn', NULL, NULL, NULL, 'Cherrypick Closet brings you an sublime collection of admirable products for ladies. We believe in only superior products & gratify our customers at a very reasonable price. Stay tuned with us & spread love ❤️. ', '2020-12-02 20:09:39', '2020-12-04 02:28:44', NULL),
(52, 58, '01711145927', 'suronjona2017@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Flat B3 , House 22, Road 20, Sector 4, Uttara,  Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:897,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Flat B3 , House 22, Road 20, Sector 4, Uttara, Dhaka</span><br></p>', 'Nur Nahar Tripty', '01711145927', 'suronjona2017@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Flat B3 , House 22, Road 20, Sector 4, Uttara,  Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:897,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Flat B3 , House 22, Road 20, Sector 4, Uttara, Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:12:08', '2020-12-02 20:12:08', NULL),
(53, 59, '01761783429', 'milialam30@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Habigonj Sadar, Habigonj&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Habigonj Sadar, Habigonj</span><br></p>', 'Mili Alom', '01761783429', 'milialam30@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Habigonj Sadar, Habigonj&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Habigonj Sadar, Habigonj</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:19:30', '2020-12-02 20:19:30', NULL),
(54, 60, '01719773729', 'priyankachakrobortypinkey@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;P.T.I. Road Habiganj.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">P.T.I. Road Habiganj.</span><br></p>', 'Priyanka Chakroborty Pinkey', '01719773729', 'priyankachakrobortypinkey@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;P.T.I. Road Habiganj.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9088,&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">P.T.I. Road Habiganj.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:24:29', '2020-12-02 20:24:29', NULL),
(55, 61, '01757557632', 'omitrony.99@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Adampur,Kamalganj,Moulvibazar &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Adampur,Kamalganj,Moulvibazar</span><br></p>', 'Omit Hasan Rony', '01757557632', 'omitrony.99@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Adampur,Kamalganj,Moulvibazar &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Adampur,Kamalganj,Moulvibazar</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-02 20:27:41', '2020-12-02 20:27:41', NULL),
(56, 63, '01316417046', 'moushumiloft@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Narayanganj, &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Narayanganj</span><br></p>', 'Moushumi Akter Bristi', '01316417046', 'moushumiloft@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Narayanganj, &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Narayanganj</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-06 19:09:20', '2020-12-06 19:09:20', NULL),
(57, 64, '01785880318', 'fariha.metro@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Nilay 29, Chowhatta, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Nilay 29, Chowhatta, Sylhet</span><br></p>', 'Momotaz Islam Chowdhury Fariha', '01785880318', 'fariha.metro@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Nilay 29, Chowhatta, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Nilay 29, Chowhatta, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-06 21:16:09', '2020-12-06 21:16:09', NULL),
(58, 65, '01627261626', 'symumnoor@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;শুভ ভবন,বি-১২০, সিডিএ,রোড নং : ১৭,আগ্রাবাদ,চট্টগ্রাম। &quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">শুভ ভবন,বি-১২০, সিডিএ,রোড নং : ১৭,আগ্রাবাদ,চট্টগ্রাম।</span><br></p>', 'জান্নাতুন নূর সাইমুম', '01627261626', 'symumnoor@gmail.com', '<p><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13.3333px; letter-spacing: normal;\">শুভ ভবন,বি-১২০, সিডিএ,রোড নং : ১৭,আগ্রাবাদ,চট্টগ্রাম।</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-08 18:56:51', '2020-12-08 18:56:51', NULL),
(59, 66, '01839501401', 'iamtonudebi@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Tongi College, bonomala road (haydarabad), jonota bazar.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Tongi College, bonomala road (haydarabad), jonota bazar.</span><br></p>', 'Tonu debi dey', '01839501401', 'iamtonudebi@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Tongi College, bonomala road (haydarabad), jonota bazar.&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Tongi College, bonomala road (haydarabad), jonota bazar.</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-08 21:13:33', '2020-12-08 21:13:33', NULL);
INSERT INTO `shops` (`id`, `shop_id`, `shop_mobile`, `shop_email`, `shop_address`, `owner_name`, `owner_mobile`, `owner_email`, `owner_address`, `facebook`, `instagram`, `twitter`, `inside_city`, `outside_city`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(60, 67, '01712242401', 'tonnyskisc@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Torongo - 5/6, Mojumdari, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Torongo - 5/6, Mojumdari, Sylhet</span><br></p>', 'Farzana khan', '01712242401', 'tonnyskisc@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Torongo - 5/6, Mojumdari, Sylhet&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">Torongo - 5/6, Mojumdari, Sylhet</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-12 19:07:24', '2020-12-12 19:07:24', NULL),
(61, 68, '01953957490', 'konika89@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;House 389, itadir mor, pulpar, jafrabad, Mohammadpur. Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">House 389, itadir mor, pulpar, jafrabad, Mohammadpur. Dhaka</span><br></p>', 'Konika Bhattacharya', '01953957490', 'konika89@gmail.com', '<p><span data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;House 389, itadir mor, pulpar, jafrabad, Mohammadpur. Dhaka&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:9089,&quot;3&quot;:{&quot;1&quot;:0},&quot;10&quot;:1,&quot;11&quot;:4,&quot;12&quot;:0,&quot;16&quot;:10}\" style=\"color: rgb(0, 0, 0); letter-spacing: normal; font-size: 10pt; font-family: Arial;\">House 389, itadir mor, pulpar, jafrabad, Mohammadpur. Dhaka</span><br></p>', NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-14 18:49:44', '2020-12-14 18:49:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `product_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 4, '38', '2020-12-01 20:50:19', '2020-12-01 20:50:19'),
(2, 4, '40', '2020-12-01 20:50:19', '2020-12-01 20:50:19'),
(3, 5, '38', '2020-12-01 20:55:35', '2020-12-01 20:55:35'),
(4, 5, '40', '2020-12-01 20:55:35', '2020-12-01 20:55:35'),
(5, 6, '38', '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(6, 6, '40', '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(7, 17, '32', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(8, 17, '34', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(9, 17, '36', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(10, 17, '38', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(11, 17, '40', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(12, 17, '42', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(13, 38, '34', '2020-12-04 21:58:49', '2020-12-04 21:58:49'),
(14, 38, '36', '2020-12-04 21:58:49', '2020-12-04 21:58:49'),
(15, 38, '38', '2020-12-04 21:58:49', '2020-12-04 21:58:49'),
(16, 38, '40', '2020-12-04 21:58:49', '2020-12-04 21:58:49'),
(17, 38, '42', '2020-12-04 21:58:49', '2020-12-04 21:58:49'),
(18, 50, '32', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(19, 50, '34', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(20, 50, '36', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(21, 50, '38', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(22, 50, '40', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(23, 50, '42', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(24, 52, '32', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(25, 52, '34', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(26, 52, '36', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(27, 52, '38', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(28, 52, '42', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(29, 52, '40', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(30, 54, '32', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(31, 54, '34', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(32, 54, '36', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(33, 54, '38', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(34, 54, '40', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(35, 54, '42', '2020-12-07 18:09:30', '2020-12-07 18:09:30'),
(36, 69, 'Choker', '2020-12-11 02:17:23', '2020-12-11 02:17:23'),
(37, 79, 'XL', '2020-12-13 21:21:21', '2020-12-13 21:21:21'),
(38, 79, 'L', '2020-12-13 21:21:21', '2020-12-13 21:21:21'),
(39, 79, 'M', '2020-12-13 21:21:21', '2020-12-13 21:21:21'),
(40, 80, 'XL', '2020-12-13 21:23:59', '2020-12-13 21:23:59'),
(41, 80, 'M', '2020-12-13 21:23:59', '2020-12-13 21:23:59'),
(42, 80, 'L', '2020-12-13 21:23:59', '2020-12-13 21:23:59'),
(43, 81, '34', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(44, 81, '36', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(45, 81, '38', '2020-12-14 20:42:54', '2020-12-14 20:42:54'),
(46, 81, '40', '2020-12-14 20:42:54', '2020-12-14 20:42:54');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `product_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'winter collection', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(2, 1, 'shawl', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(3, 1, 'চাদর', '2020-12-01 19:49:34', '2020-12-01 19:49:34'),
(4, 2, 'winter collection', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(5, 2, 'Shawl', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(6, 2, 'চাদর', '2020-12-01 20:05:21', '2020-12-01 20:05:21'),
(9, 4, 'kurti', '2020-12-01 20:50:19', '2020-12-01 20:50:19'),
(10, 4, 'Tops', '2020-12-01 20:50:19', '2020-12-01 20:50:19'),
(11, 4, 'One Piece', '2020-12-01 20:50:19', '2020-12-01 20:50:19'),
(12, 5, 'Tops', '2020-12-01 20:55:35', '2020-12-01 20:55:35'),
(13, 5, 'Kurti', '2020-12-01 20:55:35', '2020-12-01 20:55:35'),
(14, 5, 'One Piece', '2020-12-01 20:55:35', '2020-12-01 20:55:35'),
(15, 6, 'Tops', '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(16, 6, 'Kurti', '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(17, 6, 'One Piece', '2020-12-01 21:02:38', '2020-12-01 21:02:38'),
(18, 3, 'Sharee', '2020-12-01 21:04:07', '2020-12-01 21:04:07'),
(19, 3, 'শাড়ি', '2020-12-01 21:04:07', '2020-12-01 21:04:07'),
(20, 7, 'Jewellery', '2020-12-01 22:00:21', '2020-12-01 22:00:21'),
(21, 7, 'Pendent', '2020-12-01 22:00:21', '2020-12-01 22:00:21'),
(22, 7, 'মালা', '2020-12-01 22:00:21', '2020-12-01 22:00:21'),
(23, 8, 'Pendent', '2020-12-01 22:29:41', '2020-12-01 22:29:41'),
(24, 8, 'মালা', '2020-12-01 22:29:41', '2020-12-01 22:29:41'),
(25, 9, 'Unstich', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(26, 9, 'Three Piece', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(27, 9, 'Salwar Kameez', '2020-12-02 21:26:53', '2020-12-02 21:26:53'),
(28, 10, 'Three Piece', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(29, 10, 'Salwar Kameez', '2020-12-02 21:31:25', '2020-12-02 21:31:25'),
(30, 11, 'Scarf', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(31, 11, 'Orna', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(32, 11, 'Chunri', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(33, 11, 'ওড়না', '2020-12-02 22:04:57', '2020-12-02 22:04:57'),
(34, 12, 'Sharee', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(35, 12, 'শাড়ি', '2020-12-02 22:12:07', '2020-12-02 22:12:07'),
(36, 13, 'শাড়ি', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(37, 13, 'Sharee', '2020-12-02 22:14:29', '2020-12-02 22:14:29'),
(38, 14, 'Earrings', '2020-12-03 17:45:25', '2020-12-03 17:45:25'),
(39, 14, 'Ear Rings', '2020-12-03 17:45:25', '2020-12-03 17:45:25'),
(40, 14, 'কানের দুল', '2020-12-03 17:45:25', '2020-12-03 17:45:25'),
(41, 14, 'দুল', '2020-12-03 17:45:25', '2020-12-03 17:45:25'),
(42, 15, 'Pendent', '2020-12-03 17:52:03', '2020-12-03 17:52:03'),
(43, 15, 'Crafted Jewellery', '2020-12-03 17:52:03', '2020-12-03 17:52:03'),
(44, 16, 'Pendent', '2020-12-03 17:56:51', '2020-12-03 17:56:51'),
(45, 16, 'লক্ষ্মীছড়া', '2020-12-03 17:56:51', '2020-12-03 17:56:51'),
(46, 16, 'মালা', '2020-12-03 17:56:51', '2020-12-03 17:56:51'),
(47, 16, 'কড়ির মালা', '2020-12-03 17:56:51', '2020-12-03 17:56:51'),
(48, 17, 'kurti', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(49, 17, 'মৃৎ', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(50, 17, 'mreet', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(51, 17, 'ড্রেস', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(52, 17, 'Tops', '2020-12-03 18:09:17', '2020-12-03 18:09:17'),
(53, 21, 'Jewellery', '2020-12-03 19:17:50', '2020-12-03 19:17:50'),
(54, 21, 'শিউলী', '2020-12-03 19:17:50', '2020-12-03 19:17:50'),
(55, 22, 'Earrings', '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(56, 22, 'শিউলী', '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(57, 22, 'পদ্ম', '2020-12-03 19:23:30', '2020-12-03 19:23:30'),
(58, 50, 'Tops', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(59, 50, 'kurti', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(60, 50, 'One Piece', '2020-12-07 17:52:31', '2020-12-07 17:52:31'),
(61, 52, 'kurti', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(62, 52, 'Tops', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(63, 52, 'One Piece', '2020-12-07 17:58:33', '2020-12-07 17:58:33'),
(64, 57, 'Sharee', '2020-12-09 17:29:18', '2020-12-09 17:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `used_coupons`
--

CREATE TABLE `used_coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` blob,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `reward` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `mobile`, `mobile_verified_at`, `password`, `avatar`, `lang`, `remember_token`, `approved_at`, `is_active`, `city`, `address`, `note`, `reward`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Shopinn Developer', 'dev.pinikk@gmail.com', '2020-12-01 03:41:40', '01763803118', '2020-12-01 03:41:40', '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', 0x41646d696e4176617461722f367634796f32644c735948535954364d67514d465462547168704832326c337173386d396c4957692e6a706567, 'en', NULL, '2020-12-01 03:41:40', 1, NULL, NULL, NULL, 0, '2020-12-01 03:41:40', '2020-12-01 04:39:53', NULL),
(2, 1, 'ShopInn-BD', 'admin@shopinnbd.com', NULL, '01948819191', NULL, '$2y$10$PToysff35bM4oeQKx5Cs3Ot7WK2DkYnU4vC/j0FQNAWRJSqqFaeSy', 0x61646d696e2f6b64584a6f66764d5a6f4174364f6457636d3870336e4e306d64536f776a566e6d7276414a596a772e6a706567, 'en', 'WeU1DsXGrHqsKVUXoyESgfhE7O94GQQQRJFteStHVBF88NTWFBEjo07cmt8E', '2020-12-01 03:57:04', 1, NULL, '<p>Admin Address</p>', NULL, 0, '2020-12-01 03:57:04', '2020-12-01 03:57:04', NULL),
(3, 2, 'Sultan’s Shop', 'sultansshop27@gmail.com', NULL, '01716387653', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', 0x73686f704c6f676f2f6f6f6f51423850685238654c594e47595344307070634363776a4b48394d354c7255494d656c68792e6a706567, 'en', NULL, '2020-11-08 00:57:54', 1, 'Sylhet', NULL, NULL, 0, '2020-11-08 00:57:54', '2020-11-17 02:54:13', NULL),
(4, 2, 'ষোল আনা বাঙ্গালীয়ানা', 'sholoanabangaliwana@gmail.com', NULL, '01955842528', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', NULL, 'en', NULL, '2020-11-08 02:35:28', 1, 'Sylhet', NULL, NULL, 0, '2020-11-08 02:12:02', '2020-11-08 02:35:29', NULL),
(5, 2, 'নী-ব্যঞ্জনা-NeeBenjona', 'labonee4@gmail.com', NULL, '01624924559', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', 0x73686f704c6f676f2f785749566e374e463533724d33456873644577496c3530486b62446670685270515853444257714b2e6a706567, 'en', NULL, '2020-11-08 03:32:08', 1, 'Habiganj', NULL, NULL, 0, '2020-11-08 03:32:08', '2020-11-08 03:32:08', NULL),
(6, 2, 'Gaandhari-গান্ধারী', 'rupaag97@gmail.com', NULL, '01760631834', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', NULL, 'en', 'NdsAsAv0RaGmYypnoWSqpDEA8AD70XvdAO79yFiE0e39Tr4SuCgR6iB5Nwtb', '2020-11-08 05:04:15', 1, 'Sylhet', NULL, NULL, 0, '2020-11-08 05:04:15', '2020-11-08 05:04:15', NULL),
(7, 2, 'test', 'modhuprm@gmail.com', NULL, '01628428232', NULL, '$2y$10$Dz5HusXu4wcuGREXPAgd9.tWf7AF5Pg7.zPGiG2HBNev2zl8u3EDG', NULL, 'en', NULL, '2020-11-11 01:42:30', 0, 'Sylhet', NULL, NULL, 0, '2020-11-11 01:42:30', '2020-11-23 01:48:06', NULL),
(8, 2, 'মৃৎ-mreet', 'madhurideby@gmail.com', NULL, '01755856211', NULL, '$2y$10$mXS.vHe8FzfNU5Ywo/fvHuvPHHz5pN9p5TxOzuT0tT0nqkm2b4rsO', NULL, 'en', NULL, '2020-11-12 04:18:45', 1, 'Sylhet', NULL, NULL, 0, '2020-11-12 04:18:45', '2020-11-12 04:18:45', NULL),
(9, 2, 'ভৈরবীতান-Bhairabitan', 'mahmudasarker@gmail.com', NULL, '01748467281', NULL, '$2y$10$pPs6s5zM7WCRREgzdW6UM.QTWJG68cpG1w3rSbaJzPSnKqV2szWau', NULL, 'en', NULL, '2020-11-12 12:09:52', 1, 'Sylhet', NULL, NULL, 0, '2020-11-12 12:09:52', '2020-11-12 12:09:52', NULL),
(10, 2, 'অলকানন্দা -Alokanonda', 'bushrapromi@gmail.com', NULL, '01744222860', NULL, '$2y$10$pPs6s5zM7WCRREgzdW6UM.QTWJG68cpG1w3rSbaJzPSnKqV2szWau', NULL, 'en', NULL, '2020-11-12 12:15:58', 1, 'Habiganj', NULL, NULL, 0, '2020-11-12 12:15:58', '2020-11-12 12:15:58', NULL),
(11, 2, 'লোকজ', 'pritomtalukder4133@gmail.com', NULL, '01788495870', NULL, '$2y$10$NPcNh5NN/Bi7d8qmmopKAOn6Z/dKIZiSa9q1cAOewLD3AdA2fr9l6', NULL, 'en', NULL, '2020-11-12 12:24:29', 1, 'Sylhet', NULL, NULL, 0, '2020-11-12 12:24:29', '2020-11-12 12:24:29', NULL),
(12, 2, 'চন্দ্রমৌলি - Chondromouli', 'pronomishutradhar@gmail.com', NULL, '01755140071', NULL, '$2y$10$czoYELd00vZ6Osrbafx2EevxLr1m.TsD81a5zSGfp7zuumHDd57IC', NULL, 'en', NULL, '2020-11-12 12:29:15', 1, 'Sylhet', NULL, NULL, 0, '2020-11-12 12:29:15', '2020-11-12 12:29:15', NULL),
(13, 2, 'Amarelo- অ্যামারেলো', 'tasnim.archi@gmail.com', NULL, '01913585023', NULL, '$2y$10$6jSePxW7FqDO44cZJbko2uiY8di56JIEswBT3RO4jlbox1bqP7C7C', NULL, 'en', NULL, '2020-11-13 01:41:48', 1, 'Sylhet', NULL, NULL, 0, '2020-11-13 01:41:48', '2020-11-13 01:41:48', NULL),
(14, 2, 'CHanU', '1206thbouli@gmail.com', NULL, '01749776655', NULL, '$2y$10$V1XNVxOf0PqYVRVbgP2KQ.PVbxrfMzvL9VXE4utz5fHHkYieWJ/qm', NULL, 'en', NULL, '2020-11-13 01:55:34', 1, 'Sylhet', NULL, NULL, 0, '2020-11-13 01:55:34', '2020-11-13 01:55:34', NULL),
(15, 2, 'মৃন্ময়ী মৃওিকা- Mrinmoyee Mrittika', 'mmrittika7423@gmail.com', NULL, '01742244744', NULL, '$2y$10$hkmw.O4jzgB6Gcy0aSpU7uuQ/9byjt6YL.yseW5zd6NxFjga53pOe', NULL, 'en', NULL, '2020-11-13 02:09:05', 1, 'Sylhet', NULL, NULL, 0, '2020-11-13 02:09:05', '2020-11-13 02:09:05', NULL),
(16, 2, 'Nobonita', 'nobonitasarees2019@gmail.com', NULL, '01314662269', NULL, '$2y$10$YWn3qJWwGpf6lPoBERj4ju1UYyGysOOmUjHArKCUGC7MlX9Pzxfem', NULL, 'en', NULL, '2020-11-13 02:16:13', 1, 'Sylhet', NULL, NULL, 0, '2020-11-13 02:16:13', '2020-11-13 02:16:13', NULL),
(17, 2, 'এিনয়নী', 'harrydee767@gmail.com', NULL, '01788063456', NULL, '$2y$10$aHYsGPKZBQw.MQ3lIH.38uwZT7WzDVV9tdy4rYEapZ03QYrxNSJq2', NULL, 'en', NULL, '2020-11-13 02:21:06', 1, 'Sylhet', NULL, NULL, 0, '2020-11-13 02:21:06', '2020-11-13 02:21:06', NULL),
(18, 2, 'ত্রয়ী', 'jisanahmed1995@gmail.com', NULL, '01632506078', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', 0x73686f704c6f676f2f54446d446955394d616b6650754b515a727244444c42546d393152725a686e5669423576684539682e6a706567, 'en', NULL, '2020-11-23 02:05:57', 1, 'Sylhet', NULL, NULL, 0, '2020-11-23 02:05:57', '2020-11-23 02:05:57', NULL),
(19, 2, 'Glowing_splint', 'suzanarahman705@gmail.com', NULL, '01799695640', NULL, '$2y$10$7Zty.54aknSUpicdUqnQ7uAjXjqjCSqvSII2cSSTyFsmwkwdfXV3a', NULL, 'en', NULL, '2020-11-23 02:15:21', 1, 'Sylhet', NULL, NULL, 0, '2020-11-23 02:15:21', '2020-11-23 02:15:21', NULL),
(20, 2, 'প্রতিষ্ঠা কালেকশন', 'debnondini@gmail.com', NULL, '01646005321', NULL, '$2y$10$z7.gHZB5x.NoVYY46JkKHunVv7JUzVL38gfAIhOSe3LFo2lwhBysu', NULL, 'en', NULL, '2020-11-23 02:22:15', 1, 'Moulvibazar', NULL, NULL, 0, '2020-11-23 02:22:15', '2020-11-23 02:22:15', NULL),
(21, 2, 'ইশকাপন- Ishkapon', 'ishkapon.bd@gmail.com', NULL, '01858231019', NULL, '$2y$10$x0gm7DYmK9ZxHcd/HArxbeNillzckjrwBZDVLgv1YegITGsSpV4ES', NULL, 'en', NULL, '2020-12-01 21:36:11', 1, 'Dhaka', NULL, NULL, 0, '2020-12-01 21:36:11', '2020-12-01 21:36:11', NULL),
(22, 2, 'লীলাবতী হ্যান্ডি ক্রাফটস-Lilaboti handi crafts', NULL, NULL, NULL, NULL, '$2y$10$4jXgwBMJkmB2VT5uXXm1/OHQPEdCUnuTyf.N5gI08cU35xxc4Iy6O', NULL, 'en', NULL, NULL, 1, NULL, NULL, NULL, 0, '2020-12-02 04:59:29', '2020-12-02 04:59:29', NULL),
(23, 2, 'লীলাবতী হ্যান্ডি ক্রাফটস-Lilaboti handi crafts', NULL, NULL, NULL, NULL, '$2y$10$0hMtJOFaP8YJyhhfjY7Co.6ISHCt0AQkTm5JyKwTzPl/ozxQtWPs.', NULL, 'en', NULL, NULL, 1, NULL, NULL, NULL, 0, '2020-12-02 05:00:13', '2020-12-02 05:00:13', NULL),
(24, 2, 'লীলাবতী হ্যান্ডি ক্রাফটস-Lilaboti handi crafts', 'mahmudulhassan01720@gmail.com', NULL, '01720313876', NULL, '$2y$10$Ct1ebMlY5VvTjytuFRTAnuCbi7T1.qoinDF8w/NKyxiza7w17LqIu', NULL, 'en', NULL, '2020-12-02 05:04:25', 1, 'Moulvibazar', NULL, NULL, 0, '2020-12-02 05:04:25', '2020-12-02 05:04:25', NULL),
(25, 2, 'তারুপাট-Tarupaat', 'tamannajahan123456789@gmail.com', NULL, '01631015098', NULL, '$2y$10$bn1VnZyYSHCkGS/lALkuk.TnpxK3eLNWJcxEWkxMGaFFwspCqPMWq', 0x73686f704c6f676f2f5463754967554f42684d62344e4932696b3358664b5230687442315846566853397a7a6a475047372e6a706567, 'en', NULL, '2020-12-02 17:49:56', 1, 'Dhaka', NULL, NULL, 0, '2020-12-02 17:49:56', '2020-12-02 17:49:56', NULL),
(26, 2, 'Mim\'s Exclusive Wearhouse', NULL, NULL, NULL, NULL, '$2y$10$Q2wmhxRNpqKVUY/1veFq3Omwiwd7dnbbULypytx1dlv8iOVSVG8WS', NULL, 'en', NULL, NULL, 1, NULL, NULL, NULL, 0, '2020-12-02 18:02:14', '2020-12-02 18:02:14', NULL),
(27, 2, 'Mim\'s Exclusive Wearhouse', NULL, NULL, NULL, NULL, '$2y$10$KE4pZzReRy/NFc1va2/PxOuzDoRLPClXlrCmneVt2jvZtmm10YRfy', NULL, 'en', NULL, NULL, 1, NULL, NULL, NULL, 0, '2020-12-02 18:03:35', '2020-12-02 18:03:35', NULL),
(28, 2, 'Mim\'s Exclusive Wearhouse', 'mimjannat811@gmail.com', NULL, '01750053950', NULL, '$2y$10$TDeUbteeKMZ9dMAYRunigOzr29Tf.AX3TGRoVaPSVymyUM6ZVOgei', 0x73686f704c6f676f2f6a7564485338726743556c545131423644586a707168644a50647a6c4c6953593953426e497337392e706e67, 'en', NULL, '2020-12-02 18:06:05', 1, 'Rangpur', NULL, NULL, 0, '2020-12-02 18:06:05', '2020-12-02 18:06:05', NULL),
(29, 2, 'Nisa\'s Wardrobe', 'alisamiranur@gmail.com', NULL, '01717132976', NULL, '$2y$10$lT6HAKDSoE0lGUTI3LZNkesAQ194iKpWgPlVhTPV6TcJxesPFPCye', 0x73686f704c6f676f2f686c30447a6a444d724668565038366e54425a6e506b72456b6b78374b6b4f3741314d32636531622e6a706567, 'en', NULL, '2020-12-02 18:11:08', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:11:08', '2020-12-02 18:11:08', NULL),
(30, 2, 'Crafty Fire', 'opayel32@gmail.com', NULL, '01912674462', NULL, '$2y$10$r0aVehm3KNR/7PiQY4f6q.vx3yISvUCjdFZDkI3quz386d9WEsity', 0x73686f704c6f676f2f574130436653714c6941426231626f664d637057356666595976705653733973547763666f4178662e6a706567, 'en', 'tJGaGnwaFQEi3EdN3CIptFNPmGuPiabD5vWgs7ncy8WbRiSL80uqqOhAyCVb', '2020-12-02 18:15:35', 1, 'Dhaka', NULL, NULL, 0, '2020-12-02 18:15:35', '2020-12-02 18:15:35', NULL),
(31, 2, 'Art by Soumitro', 'soumitrosomu1@gmail.com', NULL, '01776516669', NULL, '$2y$10$xHcvLdYROpmD8XOkaPYSMeorWJ5sjLh4uncZTzpFVe4Ppq19AutO2', 0x73686f704c6f676f2f72517a306d717a4f6849545757324d7a69433251575a6a546a484962653453674a78475854576f4f2e6a706567, 'en', NULL, '2020-12-02 18:24:34', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:24:34', '2020-12-02 18:24:34', NULL),
(32, 2, 'Jahan\'s Collections', 'eshitapoly96@gmail.com', NULL, '01644167073', NULL, '$2y$10$ZMri5ERyzbZmadeLgc/rluKeXrI98yFP6EpwvR/4Z1E2VzizUY3QK', NULL, 'en', NULL, '2020-12-02 18:29:08', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:29:08', '2020-12-02 18:29:08', NULL),
(33, 2, 'বেধনিকা :: Bedhonika', 'sabihaanjumasha2001@gmail.com', NULL, '01516342465', NULL, '$2y$10$A7i0tydCSqhYQYXcD.ZJMuwqeXm2povdXOTxJODj8wdwLWdW6WHf.', NULL, 'en', NULL, '2020-12-02 18:30:32', 1, 'Dhaka', NULL, NULL, 0, '2020-12-02 18:30:32', '2020-12-02 18:30:32', NULL),
(34, 2, 'Jamini-যামিনী', 'shuveccha75@gmail.com', NULL, '01715720329', NULL, '$2y$10$GxaAqUwmERP.dCdmoOYJpOZ6c2Mbw8pK2Ulte0RcApoqjEQAneQiO', NULL, 'en', NULL, '2020-12-02 18:32:59', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:32:59', '2020-12-02 18:32:59', NULL),
(35, 2, 'Women\'s Craft', 'naher.pinkey@gmail.com', NULL, '01701078883', NULL, '$2y$10$PirKI6jYKk6xJhyl0OfI2u.J4bqOOfXCgbDlQtvn8opuha/YFfj/m', NULL, 'en', NULL, '2020-12-02 18:34:13', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:34:13', '2020-12-02 18:34:13', NULL),
(36, 2, 'শৈলি By সামিনা', 'saminaarna14023@gmail.com', NULL, '01680100992', NULL, '$2y$10$gh3rC8olAZLY8evmNuUH.eHmUTJl7RbEc5zxt42AQZqESe9LePFSK', NULL, 'en', NULL, '2020-12-02 18:40:15', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:40:15', '2020-12-02 18:40:15', NULL),
(37, 2, 'টিয়ার সখের দোকান', 'lunaroy505@gmail.com', NULL, '01717931910', NULL, '$2y$10$vEQKezFKMCreHnMj79duXOrQ41owMBfwOi1ismfUoVRwpyIlEKkMq', NULL, 'en', NULL, '2020-12-02 18:46:59', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:46:59', '2020-12-02 18:46:59', NULL),
(38, 2, 'Story of Stich', 'rezwanahabibparly@gmail.com', NULL, '01681022942', NULL, '$2y$10$AQIo6jEOL4D9oMt8e63rNe7HvEIFFkL7awnR/XA0gWe8JsN7bofkS', NULL, 'en', 'wykcYV6zs7m4tdCL1KfOV5pYlMLKiPW08Tgye7DHXyJAOdhoYtixRWYhpefF', '2020-12-02 18:48:07', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:48:07', '2020-12-02 18:48:07', NULL),
(39, 2, 'চিত্রাঙ্গদা', 'swaroniikachakroborty@gmail.com', NULL, '01789789436', NULL, '$2y$10$cjgegHEdw8A0/bKHoJO1D.6Tkj/dplKHHC/zVZY6XASr9gO.zcekO', NULL, 'en', NULL, '2020-12-02 18:55:22', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 18:55:22', '2020-12-02 18:55:22', NULL),
(40, 2, 'রং তুলি', 'tulitarafdar250@gmail.com', NULL, '01752212941', NULL, '$2y$10$lE6fZW71V5WS.BzvO484F.acAwzeo79TrN64H80SGlDi0z4wQjh6u', NULL, 'en', NULL, '2020-12-02 19:13:41', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:13:41', '2020-12-02 19:13:41', NULL),
(41, 2, 'Dream To Draw', 'avinandamukti@gmail.com', NULL, '01784188889', NULL, '$2y$10$wM97dQ1m5dUtLSW/D4cCJuaoA48tzD9/JeS0UuFpo6k2Vny38tanG', NULL, 'en', NULL, '2020-12-02 19:16:37', 1, 'Sunamganj', NULL, NULL, 0, '2020-12-02 19:16:37', '2020-12-02 19:16:37', NULL),
(42, 2, 'অলংকার', 'arporoy790@gmail.com', NULL, '01798768840', NULL, '$2y$10$R.DavoT7s9r4Gbyw1bjxEuTjr0HL5rMBsbMd6N3IUX5K59JXJoVzG', NULL, 'en', NULL, '2020-12-02 19:17:56', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:17:56', '2020-12-02 19:17:56', NULL),
(43, 2, 'ষড়ঋতু', 'kalnirbake@gmail.com', NULL, '01711335249', NULL, '$2y$10$XEeYODwpX4eg18xBjKtFLuF5shE6e2AT9wDC.XVVIULcuk8rDfyjy', NULL, 'en', NULL, '2020-12-02 19:21:51', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:21:51', '2020-12-02 19:21:51', NULL),
(44, 2, 'Deshi Express BD - দেশি এক্সপ্রেস বিডি', 'azafori@gmail.com', NULL, '01750035337', NULL, '$2y$10$IGc4IVW.J.oVNPeY709dC.QnC47wUbFgzVAA0PUvThtmc4MeYPyuO', NULL, 'en', NULL, '2020-12-02 19:30:42', 1, 'Moulvibazar', NULL, NULL, 0, '2020-12-02 19:30:42', '2020-12-02 19:30:42', NULL),
(45, 2, 'Shuvrota_শুভ্রতা', 'aparnaranidas10@gmail.com', NULL, '01611906101', NULL, '$2y$10$5Jw35DDKmrqFOV6Ax7Zs4OBAoJg7Nw7WBAAvB/xXTbfj9Huin1EWi', NULL, 'en', NULL, '2020-12-02 19:34:18', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:34:18', '2020-12-02 19:34:18', NULL),
(46, 2, 'জামদানি বসন', 'mirpannu2@gmail.com', NULL, '01740402246', NULL, '$2y$10$ifIg4cq7cETpkPVfWZC.Zux2wkn1hdPihHpklfzCj1dPHPLmNaHHK', NULL, 'en', NULL, '2020-12-02 19:39:21', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:39:21', '2020-12-02 19:39:21', NULL),
(47, 2, 'সুতো শৈলী', 'shutoshoili@gmail.com', NULL, '01798009370', NULL, '$2y$10$4vLFyETynPqL0mSXQyfLsuq3h5xc/3VZBuPiD359tKuBiZOCqckze', NULL, 'en', NULL, '2020-12-02 19:41:21', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:41:21', '2020-12-02 19:41:21', NULL),
(48, 2, 'খুনসুটি - khunsuti', 'deepadev500@gmail.com', NULL, '01998878809', NULL, '$2y$10$dhpqo9XcBMNr1S1OuhbY5edc7L8lYsKaoc.s36..T5BPaXzG8ugWK', NULL, 'en', NULL, '2020-12-02 19:50:24', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:50:24', '2020-12-02 19:50:24', NULL),
(49, 2, 'অনন্যা - Ananya', 'ananyadasm@gmail.com', NULL, '01713811165', NULL, '$2y$10$U9In.RXCFqXe/z6GUPlzP.ItN.V.nUxquBQ1qpaOpTm9rifDILWlS', NULL, 'en', NULL, '2020-12-02 19:51:28', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:51:28', '2020-12-02 19:51:28', NULL),
(50, 2, 'ত্রিয়ানীল art', 'troyeepurnasreepurkayastha@gmail.com', NULL, '01796414482', NULL, '$2y$10$icUjJULrzz8RmUMgueKX8.UQVVgFk6fi8XSXEkC.fxurqfopmjUD2', NULL, 'en', NULL, '2020-12-02 19:54:02', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:54:02', '2020-12-02 19:54:02', NULL),
(51, 2, 'চারুলতা - Charulata', 'charu22lata@gmail.com', NULL, '01679484366', NULL, '$2y$10$NuUMynQc.5wBOccJQwTc8..1Dii7UzGCdt2GGezQ2tKq0Rtv.U7se', NULL, 'en', NULL, '2020-12-02 19:56:06', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:56:06', '2020-12-02 19:56:06', NULL),
(52, 2, 'N. Anando gift corner', 'juismodak4@gmail.com', NULL, '01688562271', NULL, '$2y$10$xl4Xb6IY/a6kUT4JdO3ahut8EkjFBfTdAdL7rF4xDHONFp99RTOaq', NULL, 'en', NULL, '2020-12-02 19:56:59', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 19:56:59', '2020-12-02 19:56:59', NULL),
(53, 2, 'অভিজয়ীনি-Abhijayeeni', 'abhijayeeni@gmail.com', NULL, '01760497359', NULL, '$2y$10$kEfj.RBMk3u01DO3ARntNepuz4jAI9bnrstnAgaCCO0bqmjwfcEo2', NULL, 'en', NULL, '2020-12-02 20:00:31', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 20:00:31', '2020-12-02 20:00:31', NULL),
(54, 2, 'Okkhora-অক্ষরা', 'supriadabi199@gmail.com', NULL, '01709366538', NULL, '$2y$10$M.jSY.rsJYP8tB/57lBERenNJxtSZIvMAvgrRh1LrXxoeySA2fz8q', NULL, 'en', NULL, '2020-12-02 20:03:45', 1, 'Moulvibazar', NULL, NULL, 0, '2020-12-02 20:03:45', '2020-12-02 20:03:45', NULL),
(55, 2, '<> Dream Catcher <>', 'sayem611@gmail.com', NULL, '01770313434', NULL, '$2y$10$4Ag0WGLddrl9.Q9eJpCkOeWHZVVxEGW.Ozy8K4NU79nPhKQMeTEzy', NULL, 'en', NULL, '2020-12-02 20:05:47', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 20:05:46', '2020-12-02 20:05:47', NULL),
(56, 2, 'Zhaapi Sharee', 'tasmeha@gmail.com', NULL, '01634203910', NULL, '$2y$10$zVfGK2LEia0wpNs0fOL1ueUboxZhGjGxV7NOvMWmuR6ZxwfD7H1Vi', NULL, 'en', '07DqFwhPdgswDpt6JcZMkiQlop1GZdhFe57yVPtfaZJFGHig8lQQyfJGlQsW', '2020-12-02 20:06:44', 1, 'Dhaka', NULL, NULL, 0, '2020-12-02 20:06:43', '2020-12-02 20:06:44', NULL),
(57, 2, 'Cherrypick Closet', 'cherrypick06@gmail.com', NULL, '01628140510', NULL, '$2y$10$weGyWfdhuomH.ZpXhfk6hOCHdpcY6SgRIy8HIO1vrxBdKQD/0NxF.', NULL, 'en', NULL, '2020-12-02 20:09:39', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 20:09:39', '2020-12-04 02:28:44', NULL),
(58, 2, 'Suronjona : সুরঞ্জনা', 'suronjona2017@gmail.com', NULL, '01711145927', NULL, '$2y$10$prSFsJDwIx0vrR4qbtBIiOm3I.CXScKnmv3uygOR5TzMP8d9CFS5i', NULL, 'en', NULL, '2020-12-02 20:12:08', 1, 'Sylhet', NULL, NULL, 0, '2020-12-02 20:12:08', '2020-12-02 20:12:08', NULL),
(59, 2, 'পারিজাত-Parijat', 'milialam30@gmail.com', NULL, '01761783429', NULL, '$2y$10$yu20NWRvqfWJgs4ubc7DXuqRnucSL8z8.kv8krvm9Z9ys9hFaaigS', NULL, 'en', NULL, '2020-12-02 20:19:30', 1, 'Habiganj', NULL, NULL, 0, '2020-12-02 20:19:30', '2020-12-02 20:19:30', NULL),
(60, 2, 'Nilavrita - নীলাভৃতা', 'priyankachakrobortypinkey@gmail.com', NULL, '01719773729', NULL, '$2y$10$L3tR4sqBbUjScaNnBaNOZ.zaPSEDLW5dpNqZ10qNJIokDyHyxpztq', NULL, 'en', NULL, '2020-12-02 20:24:29', 1, 'Habiganj', NULL, NULL, 0, '2020-12-02 20:24:29', '2020-12-02 20:24:29', NULL),
(61, 2, 'Ayesh', 'omitrony.99@gmail.com', NULL, '01757557632', NULL, '$2y$10$cIvv8whXOZxge42kF14wH.jK6dwrxZfO.EPDhU/FJxCEePpPnCDWS', NULL, 'en', NULL, '2020-12-02 20:27:41', 1, 'Moulvibazar', NULL, NULL, 0, '2020-12-02 20:27:41', '2020-12-02 20:27:41', NULL),
(62, 5, 'NIKHIL KURMI', 'itzzmee3224@gmail.com', NULL, '01521235559', NULL, '$2y$10$eJ0eaa9MD7sUlGk/KGv.Du6zF6S8Cd/kA41dHISVq0gH2meHdsvEm', NULL, 'en', NULL, NULL, 1, 'Sylhet', 'Inclusive School Road, Sylhet', NULL, 0, '2020-12-03 02:25:39', '2020-12-03 02:26:43', NULL),
(63, 2, 'Moushumi\'s Loft', 'moushumiloft@gmail.com', NULL, '01316417046', NULL, '$2y$10$SfIiH2oN7W8mRxQhIyVj0u3SlFvQAYUC7NqlZ51.7kaHF4gBxAESm', NULL, 'en', NULL, '2020-12-06 19:09:20', 1, 'Narayanganj', NULL, NULL, 0, '2020-12-06 19:09:20', '2020-12-06 19:09:20', NULL),
(64, 2, 'Monforing মনফড়িং', 'fariha.metro@gmail.com', NULL, '01785880318', NULL, '$2y$10$bBpHrUZdE6f.UxUXFLv.ne3A2W2nq36wXETrYOnM0kthnu7hrKMSa', NULL, 'en', NULL, '2020-12-06 21:16:09', 1, 'Sylhet', NULL, NULL, 0, '2020-12-06 21:16:09', '2020-12-06 21:16:09', NULL),
(65, 2, 'চারাগাছ - Caragach', 'symumnoor@gmail.com', NULL, '01627261626', NULL, '$2y$10$1HjfHcChcwHID4qAxPyEsuBEQWMoPXzxBLxK09hRYC8/PGZoPakty', NULL, 'en', 'd7fGjNvDSqkvT5UrtY1O1oTeS5phYaAeIRp1cDdh0PlpfhUxKbKQjzoMdklm', '2020-12-08 18:56:51', 1, 'Chittagong', NULL, NULL, 0, '2020-12-08 18:56:51', '2020-12-08 18:56:51', NULL),
(66, 2, 'Debii-দেবী', 'iamtonudebi@gmail.com', NULL, '01839501401', NULL, '$2y$10$6NGgJdV5TlFmN5A0fWoaPeTJ2.LVoBekr3yY8pLtb.HZV465Ja/om', NULL, 'en', 'PpXRfP1UvxIegGm4xZzORqTRPsUPoOy9bebO1bxPPZ2OjSRFBMEvE5sBqpMp', '2020-12-08 21:13:33', 1, 'Gazipur', NULL, NULL, 0, '2020-12-08 21:13:33', '2020-12-08 21:13:33', NULL),
(67, 2, 'Dane Shunno-ডানে শূন্য', 'tonnyskisc@gmail.com', NULL, '01712242401', NULL, '$2y$10$uF0FL6kDym234g6EfyUVy.1KwIHOQ.hVoI70vY58u89IA6F2A.iiq', NULL, 'en', 'oWHTt5aJvzaRufdkhy80nkVgp7bKT8rPSpzyLnYi8bOaIVfR0pX0tVc384mb', '2020-12-12 19:07:24', 1, 'Sylhet', NULL, NULL, 0, '2020-12-12 19:07:24', '2020-12-12 19:07:24', NULL),
(68, 2, 'Trinoyoni : ত্রিনয়নী', 'konika89@gmail.com', NULL, '01953957490', NULL, '$2y$10$FoZQ3wmnDTrA8TC2InmHneLIkzQlsLYGqbgKXMpufqsFRLc2beX/e', NULL, 'en', NULL, '2020-12-14 18:49:44', 1, 'Dhaka', NULL, NULL, 0, '2020-12-14 18:49:44', '2020-12-14 18:49:44', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_advertises`
--
ALTER TABLE `admin_advertises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_advertises_type_id_foreign` (`type_id`);

--
-- Indexes for table `advertise_logs`
--
ALTER TABLE `advertise_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertise_logs_shop_id_foreign` (`shop_id`),
  ADD KEY `advertise_logs_package_id_foreign` (`package_id`);

--
-- Indexes for table `advertise_log_details`
--
ALTER TABLE `advertise_log_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertise_log_details_log_id_foreign` (`log_id`),
  ADD KEY `advertise_log_details_type_id_foreign` (`type_id`);

--
-- Indexes for table `advertise_packages`
--
ALTER TABLE `advertise_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertise_package_advertise_type`
--
ALTER TABLE `advertise_package_advertise_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertise_package_advertise_type_advertise_type_id_foreign` (`advertise_type_id`),
  ADD KEY `advertise_package_advertise_type_advertise_package_id_foreign` (`advertise_package_id`);

--
-- Indexes for table `advertise_types`
--
ALTER TABLE `advertise_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_packages`
--
ALTER TABLE `business_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codes_mobile_unique` (`mobile`),
  ADD UNIQUE KEY `codes_email_unique` (`email`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colors_product_id_foreign` (`product_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_name_unique` (`name`),
  ADD KEY `companies_compnay_id_foreign` (`compnay_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`),
  ADD KEY `coupons_shop_id_foreign` (`shop_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_shop_id_foreign` (`shop_id`),
  ADD KEY `offers_product_id_foreign` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_oid_unique` (`oid`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`),
  ADD KEY `orders_rider_id_foreign` (`rider_id`),
  ADD KEY `orders_coupon_id_foreign` (`coupon_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`),
  ADD KEY `order_items_shop_id_foreign` (`shop_id`);

--
-- Indexes for table `package_logs`
--
ALTER TABLE `package_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_logs_shop_id_foreign` (`shop_id`),
  ADD KEY `package_logs_package_id_foreign` (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_order_lists`
--
ALTER TABLE `payment_order_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_order_lists_pay_rep_id_foreign` (`pay_rep_id`),
  ADD KEY `payment_order_lists_order_id_foreign` (`order_id`);

--
-- Indexes for table `payment_reports`
--
ALTER TABLE `payment_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_reports_user_id_foreign` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_pid_unique` (`pid`),
  ADD KEY `products_shop_id_foreign` (`shop_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories_type_id_foreign` (`type_id`);

--
-- Indexes for table `product_comments`
--
ALTER TABLE `product_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_comments_product_id_foreign` (`product_id`),
  ADD KEY `product_comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_ratings_customer_id_foreign` (`customer_id`),
  ADD KEY `product_ratings_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_replies`
--
ALTER TABLE `product_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_replies_user_id_foreign` (`user_id`),
  ADD KEY `product_replies_comment_id_foreign` (`comment_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_customer_id_foreign` (`customer_id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resellers`
--
ALTER TABLE `resellers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resellers_reseller_id_foreign` (`reseller_id`);

--
-- Indexes for table `reseller_products`
--
ALTER TABLE `reseller_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reseller_products_reseller_id_foreign` (`reseller_id`),
  ADD KEY `reseller_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `riders`
--
ALTER TABLE `riders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `riders_rider_id_foreign` (`rider_id`);

--
-- Indexes for table `rider_delivery_order_lists`
--
ALTER TABLE `rider_delivery_order_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rider_delivery_order_lists_report_id_foreign` (`report_id`),
  ADD KEY `rider_delivery_order_lists_order_id_foreign` (`order_id`);

--
-- Indexes for table `rider_delivery_reports`
--
ALTER TABLE `rider_delivery_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rider_delivery_reports_rider_id_foreign` (`rider_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shops_shop_id_foreign` (`shop_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sizes_product_id_foreign` (`product_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_product_id_foreign` (`product_id`);

--
-- Indexes for table `used_coupons`
--
ALTER TABLE `used_coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `used_coupons_customer_id_foreign` (`customer_id`),
  ADD KEY `used_coupons_coupon_id_foreign` (`coupon_id`),
  ADD KEY `used_coupons_order_id_foreign` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_advertises`
--
ALTER TABLE `admin_advertises`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `advertise_logs`
--
ALTER TABLE `advertise_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advertise_log_details`
--
ALTER TABLE `advertise_log_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advertise_packages`
--
ALTER TABLE `advertise_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advertise_package_advertise_type`
--
ALTER TABLE `advertise_package_advertise_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advertise_types`
--
ALTER TABLE `advertise_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `business_packages`
--
ALTER TABLE `business_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=322;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_logs`
--
ALTER TABLE `package_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `payment_order_lists`
--
ALTER TABLE `payment_order_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_reports`
--
ALTER TABLE `payment_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_comments`
--
ALTER TABLE `product_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_replies`
--
ALTER TABLE `product_replies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `resellers`
--
ALTER TABLE `resellers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reseller_products`
--
ALTER TABLE `reseller_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `riders`
--
ALTER TABLE `riders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rider_delivery_order_lists`
--
ALTER TABLE `rider_delivery_order_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rider_delivery_reports`
--
ALTER TABLE `rider_delivery_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `used_coupons`
--
ALTER TABLE `used_coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_advertises`
--
ALTER TABLE `admin_advertises`
  ADD CONSTRAINT `admin_advertises_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `advertise_types` (`id`);

--
-- Constraints for table `advertise_logs`
--
ALTER TABLE `advertise_logs`
  ADD CONSTRAINT `advertise_logs_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `advertise_packages` (`id`),
  ADD CONSTRAINT `advertise_logs_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `advertise_log_details`
--
ALTER TABLE `advertise_log_details`
  ADD CONSTRAINT `advertise_log_details_log_id_foreign` FOREIGN KEY (`log_id`) REFERENCES `advertise_logs` (`id`),
  ADD CONSTRAINT `advertise_log_details_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `advertise_types` (`id`);

--
-- Constraints for table `advertise_package_advertise_type`
--
ALTER TABLE `advertise_package_advertise_type`
  ADD CONSTRAINT `advertise_package_advertise_type_advertise_package_id_foreign` FOREIGN KEY (`advertise_package_id`) REFERENCES `advertise_packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `advertise_package_advertise_type_advertise_type_id_foreign` FOREIGN KEY (`advertise_type_id`) REFERENCES `advertise_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `colors`
--
ALTER TABLE `colors`
  ADD CONSTRAINT `colors_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_compnay_id_foreign` FOREIGN KEY (`compnay_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `offers_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`),
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `order_items_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `package_logs`
--
ALTER TABLE `package_logs`
  ADD CONSTRAINT `package_logs_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `business_packages` (`id`),
  ADD CONSTRAINT `package_logs_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payment_order_lists`
--
ALTER TABLE `payment_order_lists`
  ADD CONSTRAINT `payment_order_lists_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `payment_order_lists_pay_rep_id_foreign` FOREIGN KEY (`pay_rep_id`) REFERENCES `payment_reports` (`id`);

--
-- Constraints for table `payment_reports`
--
ALTER TABLE `payment_reports`
  ADD CONSTRAINT `payment_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `products_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `product_types` (`id`);

--
-- Constraints for table `product_comments`
--
ALTER TABLE `product_comments`
  ADD CONSTRAINT `product_comments_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD CONSTRAINT `product_ratings_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_ratings_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_replies`
--
ALTER TABLE `product_replies`
  ADD CONSTRAINT `product_replies_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `product_comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_replies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `resellers`
--
ALTER TABLE `resellers`
  ADD CONSTRAINT `resellers_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reseller_products`
--
ALTER TABLE `reseller_products`
  ADD CONSTRAINT `reseller_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `reseller_products_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `riders`
--
ALTER TABLE `riders`
  ADD CONSTRAINT `riders_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `rider_delivery_order_lists`
--
ALTER TABLE `rider_delivery_order_lists`
  ADD CONSTRAINT `rider_delivery_order_lists_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `rider_delivery_order_lists_report_id_foreign` FOREIGN KEY (`report_id`) REFERENCES `rider_delivery_reports` (`id`);

--
-- Constraints for table `rider_delivery_reports`
--
ALTER TABLE `rider_delivery_reports`
  ADD CONSTRAINT `rider_delivery_reports_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `shops`
--
ALTER TABLE `shops`
  ADD CONSTRAINT `shops_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `used_coupons`
--
ALTER TABLE `used_coupons`
  ADD CONSTRAINT `used_coupons_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`),
  ADD CONSTRAINT `used_coupons_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `used_coupons_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
