-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table shopinn.advertise_logs
DROP TABLE IF EXISTS `advertise_logs`;
CREATE TABLE IF NOT EXISTS `advertise_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `advertise_logs_shop_id_foreign` (`shop_id`),
  KEY `advertise_logs_package_id_foreign` (`package_id`),
  CONSTRAINT `advertise_logs_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `advertise_packages` (`id`),
  CONSTRAINT `advertise_logs_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.advertise_logs: ~0 rows (approximately)
DELETE FROM `advertise_logs`;
/*!40000 ALTER TABLE `advertise_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertise_logs` ENABLE KEYS */;

-- Dumping structure for table shopinn.advertise_packages
DROP TABLE IF EXISTS `advertise_packages`;
CREATE TABLE IF NOT EXISTS `advertise_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `advertise_packages_type_id_foreign` (`type_id`),
  CONSTRAINT `advertise_packages_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `advertise_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.advertise_packages: ~0 rows (approximately)
DELETE FROM `advertise_packages`;
/*!40000 ALTER TABLE `advertise_packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertise_packages` ENABLE KEYS */;

-- Dumping structure for table shopinn.advertise_types
DROP TABLE IF EXISTS `advertise_types`;
CREATE TABLE IF NOT EXISTS `advertise_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.advertise_types: ~0 rows (approximately)
DELETE FROM `advertise_types`;
/*!40000 ALTER TABLE `advertise_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertise_types` ENABLE KEYS */;

-- Dumping structure for table shopinn.business_packages
DROP TABLE IF EXISTS `business_packages`;
CREATE TABLE IF NOT EXISTS `business_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_type` tinyint(4) NOT NULL,
  `first_percentage` decimal(8,2) DEFAULT NULL,
  `first_day` tinyint(4) DEFAULT NULL,
  `total_percentage` decimal(8,2) DEFAULT NULL,
  `total_payment` decimal(8,2) DEFAULT NULL,
  `year` tinyint(4) DEFAULT NULL,
  `month` tinyint(4) DEFAULT NULL,
  `day` tinyint(4) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.business_packages: ~0 rows (approximately)
DELETE FROM `business_packages`;
/*!40000 ALTER TABLE `business_packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_packages` ENABLE KEYS */;

-- Dumping structure for table shopinn.codes
DROP TABLE IF EXISTS `codes`;
CREATE TABLE IF NOT EXISTS `codes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codes_mobile_unique` (`mobile`),
  UNIQUE KEY `codes_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.codes: ~0 rows (approximately)
DELETE FROM `codes`;
/*!40000 ALTER TABLE `codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `codes` ENABLE KEYS */;

-- Dumping structure for table shopinn.colors
DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `colors_product_id_foreign` (`product_id`),
  CONSTRAINT `colors_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.colors: ~0 rows (approximately)
DELETE FROM `colors`;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;

-- Dumping structure for table shopinn.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `compnay_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` mediumblob,
  `json_data` json DEFAULT NULL,
  `string_data` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_data` longtext COLLATE utf8mb4_unicode_ci,
  `integer_data` int(11) DEFAULT NULL,
  `double_data` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_name_unique` (`name`),
  KEY `companies_compnay_id_foreign` (`compnay_id`),
  CONSTRAINT `companies_compnay_id_foreign` FOREIGN KEY (`compnay_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.companies: ~0 rows (approximately)
DELETE FROM `companies`;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Dumping structure for table shopinn.coupons
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `min_purchase` int(11) NOT NULL,
  `limit` tinyint(4) NOT NULL DEFAULT '1',
  `expire_date` date NOT NULL,
  `is_all` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupons_code_unique` (`code`),
  KEY `coupons_shop_id_foreign` (`shop_id`),
  CONSTRAINT `coupons_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.coupons: ~0 rows (approximately)
DELETE FROM `coupons`;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;

-- Dumping structure for table shopinn.customers
DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.customers: ~0 rows (approximately)
DELETE FROM `customers`;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table shopinn.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table shopinn.media
DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.media: ~0 rows (approximately)
DELETE FROM `media`;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;

-- Dumping structure for table shopinn.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.migrations: ~34 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2013_04_16_035325_create_roles_table', 1),
	(2, '2014_10_12_000000_create_users_table', 1),
	(3, '2014_10_12_100000_create_password_resets_table', 1),
	(4, '2019_08_19_000000_create_failed_jobs_table', 1),
	(5, '2020_04_19_082503_create_companies_table', 1),
	(6, '2020_04_19_085755_create_shops_table', 1),
	(7, '2020_04_19_090107_create_resellers_table', 1),
	(8, '2020_04_19_090127_create_customers_table', 1),
	(9, '2020_04_19_090143_create_riders_table', 1),
	(10, '2020_04_19_090609_create_product_types_table', 1),
	(11, '2020_04_19_090742_create_product_categories_table', 1),
	(12, '2020_04_19_090840_create_business_packages_table', 1),
	(13, '2020_04_19_090933_create_package_logs_table', 1),
	(14, '2020_04_19_091155_create_advertise_types_table', 1),
	(15, '2020_04_19_091800_create_advertise_packages_table', 1),
	(16, '2020_04_19_092901_create_advertise_logs_table', 1),
	(17, '2020_04_19_092953_create_products_table', 1),
	(18, '2020_04_19_093116_create_reseller_products_table', 1),
	(19, '2020_04_19_093151_create_product_ratings_table', 1),
	(20, '2020_04_19_093217_create_product_reviews_table', 1),
	(21, '2020_04_19_093242_create_offers_table', 1),
	(22, '2020_04_19_135302_create_coupons_table', 1),
	(23, '2020_04_19_135326_create_orders_table', 1),
	(24, '2020_04_19_135347_create_order_items_table', 1),
	(25, '2020_04_19_135416_create_used_coupons_table', 1),
	(26, '2020_04_19_135454_create_payment_reports_table', 1),
	(27, '2020_04_19_135542_create_payment_order_lists_table', 1),
	(28, '2020_04_19_141124_create_rider_delivery_reports_table', 1),
	(29, '2020_04_19_141416_create_rider_delivery_order_lists_table', 1),
	(30, '2020_08_27_165235_create_sizes_table', 1),
	(31, '2020_08_27_165255_create_colors_table', 1),
	(32, '2020_08_27_165304_create_tags_table', 1),
	(33, '2020_08_28_041836_create_media_table', 1),
	(34, '2020_08_30_183915_create_codes_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table shopinn.offers
DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` tinyint(4) NOT NULL,
  `expire_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offers_shop_id_foreign` (`shop_id`),
  KEY `offers_product_id_foreign` (`product_id`),
  CONSTRAINT `offers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `offers_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.offers: ~0 rows (approximately)
DELETE FROM `offers`;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;

-- Dumping structure for table shopinn.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `oid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `rider_id` bigint(20) unsigned DEFAULT NULL,
  `coupon_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `delivery` int(11) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_oid_unique` (`oid`),
  KEY `orders_customer_id_foreign` (`customer_id`),
  KEY `orders_rider_id_foreign` (`rider_id`),
  KEY `orders_coupon_id_foreign` (`coupon_id`),
  CONSTRAINT `orders_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`),
  CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.orders: ~0 rows (approximately)
DELETE FROM `orders`;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table shopinn.order_items
DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_id` bigint(20) unsigned NOT NULL,
  `reseller_id` bigint(20) unsigned DEFAULT NULL,
  `price` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  KEY `order_items_product_id_foreign` (`product_id`),
  KEY `order_items_shop_id_foreign` (`shop_id`),
  KEY `order_items_reseller_id_foreign` (`reseller_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `order_items_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`),
  CONSTRAINT `order_items_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.order_items: ~0 rows (approximately)
DELETE FROM `order_items`;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

-- Dumping structure for table shopinn.package_logs
DROP TABLE IF EXISTS `package_logs`;
CREATE TABLE IF NOT EXISTS `package_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `start` date NOT NULL,
  `end` date DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `package_logs_shop_id_foreign` (`shop_id`),
  KEY `package_logs_package_id_foreign` (`package_id`),
  CONSTRAINT `package_logs_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `business_packages` (`id`),
  CONSTRAINT `package_logs_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.package_logs: ~0 rows (approximately)
DELETE FROM `package_logs`;
/*!40000 ALTER TABLE `package_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `package_logs` ENABLE KEYS */;

-- Dumping structure for table shopinn.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table shopinn.payment_order_lists
DROP TABLE IF EXISTS `payment_order_lists`;
CREATE TABLE IF NOT EXISTS `payment_order_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pay_rep_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_order_lists_pay_rep_id_foreign` (`pay_rep_id`),
  KEY `payment_order_lists_order_id_foreign` (`order_id`),
  CONSTRAINT `payment_order_lists_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `payment_order_lists_pay_rep_id_foreign` FOREIGN KEY (`pay_rep_id`) REFERENCES `payment_reports` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.payment_order_lists: ~0 rows (approximately)
DELETE FROM `payment_order_lists`;
/*!40000 ALTER TABLE `payment_order_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_order_lists` ENABLE KEYS */;

-- Dumping structure for table shopinn.payment_reports
DROP TABLE IF EXISTS `payment_reports`;
CREATE TABLE IF NOT EXISTS `payment_reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned DEFAULT NULL,
  `reseller_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_reports_shop_id_foreign` (`shop_id`),
  KEY `payment_reports_reseller_id_foreign` (`reseller_id`),
  CONSTRAINT `payment_reports_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`),
  CONSTRAINT `payment_reports_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.payment_reports: ~0 rows (approximately)
DELETE FROM `payment_reports`;
/*!40000 ALTER TABLE `payment_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_reports` ENABLE KEYS */;

-- Dumping structure for table shopinn.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `buy_price` double(9,2) NOT NULL,
  `sell_price` double(9,2) NOT NULL,
  `updated_price` double(9,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_pid_unique` (`pid`),
  KEY `products_shop_id_foreign` (`shop_id`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`),
  CONSTRAINT `products_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.products: ~0 rows (approximately)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table shopinn.product_categories
DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_categories_type_id_foreign` (`type_id`),
  CONSTRAINT `product_categories_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `product_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.product_categories: ~10 rows (approximately)
DELETE FROM `product_categories`;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` (`id`, `type_id`, `name`, `note`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'category_1_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(2, 1, 'category_1_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(3, 2, 'category_2_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(4, 2, 'category_2_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(5, 3, 'category_3_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(6, 3, 'category_3_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(7, 4, 'category_4_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(8, 4, 'category_4_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(9, 5, 'category_5_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(10, 5, 'category_5_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL);
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;

-- Dumping structure for table shopinn.product_ratings
DROP TABLE IF EXISTS `product_ratings`;
CREATE TABLE IF NOT EXISTS `product_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_ratings_customer_id_foreign` (`customer_id`),
  KEY `product_ratings_product_id_foreign` (`product_id`),
  CONSTRAINT `product_ratings_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `product_ratings_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.product_ratings: ~0 rows (approximately)
DELETE FROM `product_ratings`;
/*!40000 ALTER TABLE `product_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_ratings` ENABLE KEYS */;

-- Dumping structure for table shopinn.product_reviews
DROP TABLE IF EXISTS `product_reviews`;
CREATE TABLE IF NOT EXISTS `product_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_reviews_customer_id_foreign` (`customer_id`),
  KEY `product_reviews_product_id_foreign` (`product_id`),
  CONSTRAINT `product_reviews_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.product_reviews: ~0 rows (approximately)
DELETE FROM `product_reviews`;
/*!40000 ALTER TABLE `product_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_reviews` ENABLE KEYS */;

-- Dumping structure for table shopinn.product_types
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.product_types: ~5 rows (approximately)
DELETE FROM `product_types`;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
INSERT INTO `product_types` (`id`, `name`, `note`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'type_1', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(2, 'type_2', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(3, 'type_3', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(4, 'type_4', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(5, 'type_5', NULL, 1, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL);
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;

-- Dumping structure for table shopinn.resellers
DROP TABLE IF EXISTS `resellers`;
CREATE TABLE IF NOT EXISTS `resellers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reseller_id` bigint(20) unsigned NOT NULL,
  `business_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resellers_reseller_id_foreign` (`reseller_id`),
  CONSTRAINT `resellers_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.resellers: ~0 rows (approximately)
DELETE FROM `resellers`;
/*!40000 ALTER TABLE `resellers` DISABLE KEYS */;
/*!40000 ALTER TABLE `resellers` ENABLE KEYS */;

-- Dumping structure for table shopinn.reseller_products
DROP TABLE IF EXISTS `reseller_products`;
CREATE TABLE IF NOT EXISTS `reseller_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reseller_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reseller_products_reseller_id_foreign` (`reseller_id`),
  KEY `reseller_products_product_id_foreign` (`product_id`),
  CONSTRAINT `reseller_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `reseller_products_reseller_id_foreign` FOREIGN KEY (`reseller_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.reseller_products: ~0 rows (approximately)
DELETE FROM `reseller_products`;
/*!40000 ALTER TABLE `reseller_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `reseller_products` ENABLE KEYS */;

-- Dumping structure for table shopinn.riders
DROP TABLE IF EXISTS `riders`;
CREATE TABLE IF NOT EXISTS `riders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rider_id` bigint(20) unsigned NOT NULL,
  `rider_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rider_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `riders_rider_id_foreign` (`rider_id`),
  CONSTRAINT `riders_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.riders: ~0 rows (approximately)
DELETE FROM `riders`;
/*!40000 ALTER TABLE `riders` DISABLE KEYS */;
/*!40000 ALTER TABLE `riders` ENABLE KEYS */;

-- Dumping structure for table shopinn.rider_delivery_order_lists
DROP TABLE IF EXISTS `rider_delivery_order_lists`;
CREATE TABLE IF NOT EXISTS `rider_delivery_order_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rider_delivery_order_lists_report_id_foreign` (`report_id`),
  KEY `rider_delivery_order_lists_order_id_foreign` (`order_id`),
  CONSTRAINT `rider_delivery_order_lists_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `rider_delivery_order_lists_report_id_foreign` FOREIGN KEY (`report_id`) REFERENCES `rider_delivery_reports` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.rider_delivery_order_lists: ~0 rows (approximately)
DELETE FROM `rider_delivery_order_lists`;
/*!40000 ALTER TABLE `rider_delivery_order_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `rider_delivery_order_lists` ENABLE KEYS */;

-- Dumping structure for table shopinn.rider_delivery_reports
DROP TABLE IF EXISTS `rider_delivery_reports`;
CREATE TABLE IF NOT EXISTS `rider_delivery_reports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rider_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rider_delivery_reports_rider_id_foreign` (`rider_id`),
  CONSTRAINT `rider_delivery_reports_rider_id_foreign` FOREIGN KEY (`rider_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.rider_delivery_reports: ~0 rows (approximately)
DELETE FROM `rider_delivery_reports`;
/*!40000 ALTER TABLE `rider_delivery_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `rider_delivery_reports` ENABLE KEYS */;

-- Dumping structure for table shopinn.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.roles: ~5 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Admin', 'admin', '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(2, 'Shop', 'shop', '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(3, 'Reseller', 'reseller', '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(4, 'Rider', 'rider', '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(5, 'Customer', 'customer', '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table shopinn.shops
DROP TABLE IF EXISTS `shops`;
CREATE TABLE IF NOT EXISTS `shops` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `shop_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_address` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shops_shop_id_foreign` (`shop_id`),
  CONSTRAINT `shops_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.shops: ~0 rows (approximately)
DELETE FROM `shops`;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table shopinn.sizes
DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sizes_product_id_foreign` (`product_id`),
  CONSTRAINT `sizes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.sizes: ~0 rows (approximately)
DELETE FROM `sizes`;
/*!40000 ALTER TABLE `sizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sizes` ENABLE KEYS */;

-- Dumping structure for table shopinn.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tags_product_id_foreign` (`product_id`),
  CONSTRAINT `tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.tags: ~0 rows (approximately)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table shopinn.used_coupons
DROP TABLE IF EXISTS `used_coupons`;
CREATE TABLE IF NOT EXISTS `used_coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `coupon_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `used_coupons_customer_id_foreign` (`customer_id`),
  KEY `used_coupons_coupon_id_foreign` (`coupon_id`),
  KEY `used_coupons_order_id_foreign` (`order_id`),
  CONSTRAINT `used_coupons_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`),
  CONSTRAINT `used_coupons_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `used_coupons_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.used_coupons: ~0 rows (approximately)
DELETE FROM `used_coupons`;
/*!40000 ALTER TABLE `used_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `used_coupons` ENABLE KEYS */;

-- Dumping structure for table shopinn.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` blob,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `address` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_unique` (`mobile`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table shopinn.users: ~5 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `mobile`, `mobile_verified_at`, `password`, `avatar`, `lang`, `remember_token`, `approved_at`, `is_active`, `address`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Demo Admin', 'admin@mail.com', '2020-09-08 04:53:54', '01712345678', '2020-09-08 04:53:54', '$2y$10$vQYrRlB5DuSR3xZqkI0ob.sCEgzC7sTLL36bEjpD7yXLptRZ1cNgK', NULL, 'en', NULL, '2020-09-08 04:53:54', 1, NULL, NULL, '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(2, 2, 'Demo Shop', 'shop@mail.com', '2020-09-08 04:53:54', '01713345678', '2020-09-08 04:53:54', '$2y$10$vPm8DeADsASDPVTQnLx0tusfu/Pc2sdx6OSamhfI233ts5mGiroge', NULL, 'en', NULL, '2020-09-08 04:53:54', 1, NULL, NULL, '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(3, 3, 'Demo Reseller', 'reseller@mail.com', '2020-09-08 04:53:54', '01714345678', '2020-09-08 04:53:54', '$2y$10$hX2oQ.z8iMWxIPQ5xzY9EOEcjuYyX1TDOHSHlyFrSh.tEvqVtdOx.', NULL, 'en', NULL, '2020-09-08 04:53:54', 1, NULL, NULL, '2020-09-08 04:53:54', '2020-09-08 04:53:54', NULL),
	(4, 4, 'Demo Rider', 'rider@mail.com', '2020-09-08 04:53:55', '01715345678', '2020-09-08 04:53:55', '$2y$10$buET7WUGRQKFk4MG.3K.lO1zzi27n20ulmlKm8xNRxUiARVuobO4O', NULL, 'en', NULL, '2020-09-08 04:53:55', 1, NULL, NULL, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL),
	(5, 5, 'Demo Customer', 'customer@mail.com', '2020-09-08 04:53:55', '01716345678', '2020-09-08 04:53:55', '$2y$10$OLFFJXLv7venXciGYPQRa.326/tas4z5RBjXpzTzHb2rO3vSJBWzS', NULL, 'en', NULL, '2020-09-08 04:53:55', 1, NULL, NULL, '2020-09-08 04:53:55', '2020-09-08 04:53:55', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
