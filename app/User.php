<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, SoftDeletes, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'name', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    // all relation below

    //relation with Role many to one relationship
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    //relation with Company
    public function companies()
    {
        return $this->hasMany('App\Models\Company', 'compnay_id', 'id');
    }

    //relation with shop
    public function shop()
    {
        return $this->hasOne('App\Models\Shop', 'shop_id', 'id');
    }

    //relation with reseller
    public function reseller()
    {
        return $this->hasOne('App\Models\Reseller', 'reseller_id', 'id');
    }

    //relation with customer
    // public function customer(){
    //     return $this->hasOne('App\Models\Customer', 'customer_id', 'id');
    // }

    //relation with rider
    public function rider()
    {
        return $this->hasOne('App\Models\Rider', 'rider_id', 'id');
    }

    //relation with packgeLog
    public function packgeLogs()
    {
        return $this->hasMany('App\Models\PackageLog', 'shop_id', 'id');
    }

    //relation with advertiseLog
    public function advertiseLogs()
    {
        return $this->hasMany('App\Models\AdvertiseLog', 'shop_id', 'id');
    }

    //relation with product for shop
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'shop_id', 'id');
    }

    //relation with order customer
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id', 'id');
    }

    //relation with orderRider for riders
    public function orderRiders()
    {
        return $this->hasMany('App\Models\Order', 'rider_id', 'id');
    }

    //relation with orderItem for shop
    public function shopOrderItems()
    {
        return $this->hasMany('App\Models\Order', 'shop_id', 'id');
    }

    //relation with reseller Products
    public function resellerProducts()
    {
        return $this->hasMany('App\Models\ResellerProduct', 'reseller_id', 'id');
    }

    //relation with user payment reports
    public function paymentReports()
    {
        return $this->hasMany('App\Models\PaymentReport', 'user_id', 'id');
    }

    //relation with rider payment reporst
    public function riderDeliveryReports()
    {
        return $this->hasMany('App\Models\RiderDeliveryReport', 'rider_id', 'id');
    }

    //relation with used coupons
    public function usedCoupns()
    {
        return $this->hasMany('App\Models\UsedCoupon', 'customer_id', 'id');
    }

    //relation with shop offters
    public function Offers()
    {
        return $this->hasMany('App\Models\Offer', 'shop_id', 'id');
    }

    //relation with product ratings
    public function ratings()
    {
        return $this->hasMany('App\Models\ProductRating', 'customer_id', 'id');
    }

    //relation with product reviews
    public function reviews()
    {
        return $this->hasMany('App\Models\ProductReview', 'customer_id', 'id');
    }

    //relation with product comments
    public function comments()
    {
        return $this->hasMany('App\Models\ProductComment', 'user_id', 'id');
    }

    //relation with product replies
    public function replies()
    {
        return $this->hasMany('App\Models\ProductReply', 'user_id', 'id');
    }





    /**
     * !`contain`, `max`, `fill`, `stretch`, `crop`
     *
     * Handling Media Collections
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('logo')
            ->registerMediaConversions(function (Media $media = null) {
                $this->addMediaConversion('thumb')
                    ->fit('crop', 160, 160);
                $this->addMediaConversion('nav')
                    ->fit('crop', 45, 45);
            });
        $this->addMediaCollection('cover')
            ->registerMediaConversions(function (Media $media = null) {
                $this->addMediaConversion('profile')
                    ->fit('crop', 1920, 640);
                $this->addMediaConversion('card')
                    ->fit('crop', 448, 224);
            });
    }
}
