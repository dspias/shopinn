<?php

namespace App\Notifications\User\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\Product;

class Comment extends Notification
{
    use Queueable;

    private $product;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->product->name."</b>' a new Comment";
        $link = route('guest.product.show', ['product_id'=> $this->product->id, 'product_name' => get_name($this->product->name)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->product->name."</b>' a new Comment";
        $link = route('guest.product.show', ['product_id'=> $this->product->id, 'product_name' => get_name($this->product->name)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->product->name."</b>' a new Comment";
        $link = route('guest.product.show', ['product_id'=> $this->product->id, 'product_name' => get_name($this->product->name)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
