<?php

namespace App\Notifications\Admin\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\User;
use App\Models\Product;

class NewProduct extends Notification
{
    use Queueable;

    private $shop;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $shop, Product $product)
    {
        $this->shop = $shop;
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->shop->name."</b>' Added a new Product";
        $link = route('admin.product.details', ['page' => 'all', 'product_id' => encrypt($this->product->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->shop->name."</b>' Added a new Product";
        $link = route('admin.product.details', ['page' => 'all', 'product_id' => encrypt($this->product->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $msg = "'<b class='text-shopinn text-uppercase'>".$this->shop->name."</b>' Added a new Product";
        $link = route('admin.product.details', ['page' => 'all', 'product_id' => encrypt($this->product->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
