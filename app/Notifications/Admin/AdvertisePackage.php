<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\AdvertiseLog;
use App\Models\AdvertisePackage as ModelsAdvertisePackage;
use App\User;

class AdvertisePackage extends Notification
{
    use Queueable;

    private $log;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AdvertiseLog $log)
    {
        $this->log = $log;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $shop = User::findOrFail($this->log->shop_id);
        $package = ModelsAdvertisePackage::findOrFail($this->log->package_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has purchased AD package: '<b class='text-loginn'>".$package->name."</b>'";
        $link = route('admin.advertise.shop.ad.pack.show', ['pack_id' => encrypt($this->log->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $shop = User::findOrFail($this->log->shop_id);
        $package = ModelsAdvertisePackage::findOrFail($this->log->package_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has purchased AD package: '<b class='text-loginn'>".$package->name."</b>'";
        $link = route('admin.advertise.shop.ad.pack.show', ['pack_id' => encrypt($this->log->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $shop = User::findOrFail($this->log->shop_id);
        $package = ModelsAdvertisePackage::findOrFail($this->log->package_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has purchased AD package: '<b class='text-loginn'>".$package->name."</b>'";
        $link = route('admin.advertise.shop.ad.pack.show', ['pack_id' => encrypt($this->log->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
