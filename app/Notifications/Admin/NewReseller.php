<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewReseller extends Notification
{
    use Queueable;

    private $reseller;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $reseller)
    {
        $this->reseller = $reseller;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $msg = "new reseller '<b class='text-shopinn'>".$this->reseller->name."</b>' registered";
        $link = route('admin.reseller.details', ['page' => 'all', 'reseller_id' => encrypt($this->reseller->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $msg = "new reseller '<b class='text-shopinn'>".$this->reseller->name."</b>' registered";
        $link = route('admin.reseller.details', ['page' => 'pending', 'reseller_id' => encrypt($this->reseller->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $msg = "new reseller '<b class='text-shopinn'>".$this->reseller->name."</b>' registered";
        $link = route('admin.reseller.details', ['page' => 'pending', 'reseller_id' => encrypt($this->reseller->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
