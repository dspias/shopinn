<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\AdvertiseLog;
use App\Models\AdvertiseLogDetails;
use App\User;

class NewAd extends Notification
{
    use Queueable;

    private $ad;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AdvertiseLogDetails $ad)
    {
        $this->ad = $ad;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $log = AdvertiseLog::findOrfail($this->ad->log_id);
        $shop = User::findOrFail($log->shop_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has created new Advertise";
        $link = route('admin.advertise.view', ['ad_id' => encrypt($this->ad->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $log = AdvertiseLog::findOrfail($this->ad->log_id);
        $shop = User::findOrFail($log->shop_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has created new Advertise";
        $link = route('admin.advertise.view', ['ad_id' => encrypt($this->ad->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $log = AdvertiseLog::findOrfail($this->ad->log_id);
        $shop = User::findOrFail($log->shop_id);
        $msg = "'<b class='text-loginn'>".$shop->name."</b>'" . " has created new Advertise";
        $link = route('admin.advertise.view', ['ad_id' => encrypt($this->ad->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
