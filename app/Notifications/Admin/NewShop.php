<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\User;
class NewShop extends Notification
{
    use Queueable;

    private $shop;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'broadcast', 'mail];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $msg = "new shop '<b class='text-shopinn'>".$this->shop->name."</b>' registered";
        $link = route('admin.shop.details', ['page' => 'all', 'shop_id' => encrypt($this->shop->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $msg = "new shop '<b class='text-shopinn'>".$this->shop->name."</b>' registered";
        $link = route('admin.shop.details', ['page' => 'pending', 'shop_id' => encrypt($this->shop->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toBroadcast($notifiable)
    {
        $msg = "new shop '<b class='text-shopinn'>".$this->shop->name."</b>' registered";
        $link = route('admin.shop.details', ['page' => 'pending', 'shop_id' => encrypt($this->shop->id)]);
        return [
            'message' => $msg,
            'link' => $link,
            // 'admin' => $notifiable,
        ];
    }
}
