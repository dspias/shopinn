<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetShopPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $data, $password;

    public function __construct($data, $password)
    {
        $this->data = $data;
        $this->password = $password;
    }

    public function build()
    {
        $data = $this->data;
        $password = $this->password;
        $address = env('MAIL_FROM_ADDRESS', 'write@shopinnbd.com');
        $subject = 'Password Reseted by SHOPINN-BD';
        $name = env('APP_NAME', 'ShopInn');

        return $this->view('emails.reset_shop_password', compact(['data', 'password']))
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject);
    }
}
