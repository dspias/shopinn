<?php

use App\Models\AdminAdvertise;
use App\Models\AdvertiseLogDetails;
use Illuminate\Support\Carbon;
use App\Models\Company;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\PackageLog;
use App\Models\ResellerProduct;
use App\Models\Shop;
use App\User;
use Illuminate\Support\Facades\DB;

/**
 * Get Images
 */

if (!function_exists('get_images')) {
    function get_images($item, $type = 'default')
    {
        $urls = array();
        $avatars = $item->getMedia('product');
        foreach ($avatars as $avatar) {
            if ($avatar != null) $urls[] = $avatar->getUrl($type);
        }
        return $urls;
    }
}


/**
 * Get Image
 */

if (!function_exists('get_image')) {
    function get_image($item, $type = 'default')
    {
        $url = null;
        $avatar = $item->getFirstMedia('product');
        if ($avatar != null) $url = $avatar->getUrl($type);
        return $url;
    }
}
if (!function_exists('get_logo')) {
    function get_logo($item, $type = 'default')
    {
        $url = null;
        $avatar = $item->getFirstMedia('logo');
        if ($avatar != null) $url = $avatar->getUrl($type);
        return $url;
    }
}
if (!function_exists('get_cover')) {
    function get_cover($item, $type = 'default')
    {
        $url = null;
        $avatar = $item->getFirstMedia('cover');
        if ($avatar != null) $url = $avatar->getUrl($type);
        return $url;
    }
}

/**
 * Get Name
 */

if (!function_exists('get_name')) {
    function get_name($name)
    {
        $name = str_replace(" ", "-", $name);
        $name = str_replace("/", "_", $name);
        $name = str_replace("'", "", $name);
        $name = preg_replace('/[^\p{L}\p{N}]/u', '_', $name);
        return $name;
    }
}

/**
 * Get Date
 */

if (!function_exists('get_date')) {
    function get_date($date, $format)
    {
        $dd = new DateTime($date);
        return $dd->format($format);
    }
}

/**
 * Get Price
 */

if (!function_exists('get_price')) {
    function get_price($product)
    {
        return ($product->updated_price != null) ? $product->updated_price : $product->sell_price;
    }
}
if (!function_exists('offer_price')) {
    function offer_price($product)
    {
        $data = array();
        $price = ($product->updated_price != null) ? $product->updated_price : $product->sell_price;
        $data['main'] = $price;
        $offer = $product->offers->last();
        if (!is_null($offer) && $offer->expire_date >= date('Y-m-d')) {
            $data['isOffer'] = 1;
            $data['offer'] = $offer->discount;
            $data['price'] = floor((float)((100 - $offer->discount) * $price) / 100);
        } else {
            $data['isOffer'] = 0;
            $data['offer'] = null;
            $data['price'] = null;
        }
        return (array) $data;
    }
}
if (!function_exists('offer_sell_price')) {
    function offer_sell_price($product)
    {
        $data = array();
        $price = $product->sell_price;
        $data['main'] = $price;
        $offer = $product->offers->last();
        if (!is_null($offer) && $offer->expire_date >= date('Y-m-d')) {
            $data['isOffer'] = 1;
            $data['offer'] = $offer->discount;
            $data['price'] = floor((float)((100 - $offer->discount) * $price) / 100);
        } else {
            $data['isOffer'] = 0;
            $data['offer'] = null;
            $data['price'] = null;
        }
        return (array) $data;
    }
}

/**
 * Get Added Time
 *
 * For Setting the TimeZone for Carbon diffForHumans();
 * in the AppServiceProvider.php you can add the php functionality to alter the timestamp for the whole project
 *
 *! public function boot()
 *! {
 *     Schema::defaultStringLength(191);
 *!    date_default_timezone_set('Asia/Aden');
 *! }
 */

if (!function_exists('ago_time')) {
    function ago_time($date)
    {
        return Carbon::parse($date)->diffForHumans();
    }
}

/**
 * avarage rating
 */

if (!function_exists('get_avarage_rating')) {
    function get_avarage_rating($ratings)
    {
        $sl = 0;
        $rate = 0;
        foreach ($ratings as $rating) {
            $sl++;
            $rate += $rating->rate;
        }
        $result = ($sl > 0) ? number_format($rate / $sl, 1, '.', '') : 0.0;
        return $result;
    }
}


/**
 * company Info
 */

if (!function_exists('company_set')) {
    function company_set($volume, $type, $value)
    {
        $data = Company::where('name', $volume)->first();
        if ($data == null) {
            $data = new Company();
            $data->compnay_id = auth()->user()->id;
            $data->name = $volume;
            $data->type = $type;
        } else {
            $type = $data->type;
        }

        if ($type == 'avatar') {
            $path = storage_path('app/public/' . optional($data)->avatar);
            if (file_exists($path)) {
                @unlink($path);
            }
            $data->avatar = $value->store('CompanyAvatar', 'public');
        } elseif ($type == 'json') {
            $en_value = json_encode($value);
            $data->json_data = $en_value;
        } elseif ($type == 'string') {
            $data->string_data = $value;
        } elseif ($type == 'integer') {
            $data->integer_data = $value;
        } elseif ($type == 'double') {
            $data->double_data = $value;
        } else {
            $data->text_data = $value;
        }
        $data->save();
    }
}


if (!function_exists('company_get')) {
    function company_get($volume)
    {
        $data = Company::where('name', $volume)->first();
        $value = null;
        if ($data != null) {
            $type = $data->type;
            if ($type == 'avatar') {
                $value = $data->avatar;
            } elseif ($type == 'json') {
                $value = json_decode($data->json_data, true);
                $value = (array) $value;
            } elseif ($type == 'string') {
                $value = $data->string_data;
            } elseif ($type == 'integer') {
                $value = $data->integer_data;
            } elseif ($type == 'double') {
                $value = $data->double_data;
            } else {
                $value = $data->text_data;
            }
        }
        return $value;
    }
}



/**
 * Notify to admin
 */

if (!function_exists('admin_notify')) {
    function admin_notify($data1, $data2 = null)
    {
        // find all admins
        $admins = User::where('role_id', 1)->get();
        foreach ($admins as $admin) {
            if ($data2 == null)  $admin->notify($data1);
            else $admin->notify($data1, $data2);
        }
    }
}

/**
 * Notify to admin
 */

if (!function_exists('user_notify')) {
    function user_notify($user_id, $data1, $data2 = null)
    {
        $user = User::find($user_id);
        if (!is_null($user)) {
            if ($data2 == null)  $user->notify($data1);
            else $user->notify($data1, $data2);
        }
    }
}

/**
 * checking shop is subsribed any package or not
 */

if (!function_exists('subsribed')) {
    function subsribed($user_id)
    {
        $activated = PackageLog::with('package')->where('shop_id', $user_id)->where('is_active', 1)->where('end', '>=', date('Y-m-d'))->orderBy('id', 'desc')->first();
        if (is_null($activated)) {
            $activated = PackageLog::with('package')->where('shop_id', $user_id)->where('is_active', 1)->WhereNull('end')->orderBy('id', 'desc')->first();
            if (is_null($activated)) return false;
        }
        return $activated;
    }
}


/**
 * get day from date
 */

if (!function_exists('get_day')) {
    function get_day($end_date, $start_date  = null)
    {
        if ($start_date == null) $start_date = date('Y-m-d');
        $endDate = strtotime($end_date);
        $startDate = strtotime($start_date);

        $day = floor(($endDate - $startDate) / (60 * 60 * 24));

        return $day;
    }
}


/**
 * ad show big
 */

if (!function_exists('ads')) {
    function ads($width, $height)
    {
        $data = array($width, $height);

        $admins = AdminAdvertise::inRandomOrder()->whereHas('type', function ($query) use ($data) {
            $query->where('width', $data[0])->where('height', $data[1]);
        })->where('is_active', 1)->where('end', '>=', date('Y-m-d'))->get();

        $shops = AdvertiseLogDetails::inRandomOrder()->whereHas('type', function ($query) use ($data) {
            $query->where('width', $data[0])->where('height', $data[1]);
        })->where('is_active', 1)->where('end', '>=', date('Y-m-d'))->get();

        $ads = array();
        foreach ($admins as $ad) $ads[] = $ad;
        foreach ($shops as $ad) $ads[] = $ad;
        shuffle($ads);
        return $ads;
    }
}


/**
 * ad show big
 */

if (!function_exists('ad')) {
    function ad($width, $height)
    {
        $data = array($width, $height);

        $admin = AdminAdvertise::inRandomOrder()->whereHas('type', function ($query) use ($data) {
            $query->where('width', $data[0])->where('height', $data[1]);
        })->where('is_active', 1)->where('end', '>=', date('Y-m-d'))->first();

        $shop = AdvertiseLogDetails::inRandomOrder()->whereHas('type', function ($query) use ($data) {
            $query->where('width', $data[0])->where('height', $data[1]);
        })->where('is_active', 1)->where('end', '>=', date('Y-m-d'))->first();

        $ads = array($admin, $shop);
        shuffle($ads);
        return $ads[0];
    }
}


/**
 * ad show big
 */

if (!function_exists('image')) {
    function image($url)
    {
        return url(env('IMG_STORE', 'storage/') . $url);
    }
}



/**
 * product is added or not for reseller
 */

if (!function_exists('isAdded')) {
    function isAdded($product_id, $reseller_id)
    {
        $product = ResellerProduct::where('product_id', $product_id)
            ->where('reseller_id', $reseller_id)
            ->with(['product' => function ($query) {
                $query->where('is_active', 1);
            }])->first();
        return (is_null($product)) ? false : true;
    }
}

/**
 * Shopinn's (Admin) Earning Calculation
 */

if (!function_exists('get_admin_commision')) {
    function get_admin_commision($shop_id)
    {
        $packageLog = subsribed($shop_id);
        if ($packageLog) {
            $package = $packageLog->package;
            if (!is_null($package)) {
                $start = get_date($packageLog->start, 'Y-m-d');
                $days = !is_null($package->first_day) ? $package->first_day : 0;
                $first = strtotime($start . ' + ' . $days . ' days');
                if (!is_null($package->first_day) && $first <= date('Y-m-d')) {
                    return $package->first_percentage;
                } else return $package->total_percentage;
            }
        }
        return 0;
    }
}


/**
 * Calculate percentage
 */

if (!function_exists('get_percent')) {
    function get_percent($total, $dependent)
    {
        return ($total > 0) ? number_format(($dependent * 100) / $total, 2, '.', '') : 0.00;
    }
}


/**
 * Shop Earn Per Order Calculation
 */

if (!function_exists('get_shop_earn_per_order')) {
    function get_shop_earn_per_order($order_id, $shop_id)
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('shop_id', $shop_id)->where('status', '!=', -1)->whereHas('order', function ($query) use ($order_id) {
            $query->where('id', $order_id)->where('status', 2);
        })->where('paid', 0)->select(DB::raw('sum(shop_earn*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('id', $order_id)->where('status', 2)->whereHas('items', function ($query) use ($shop_id) {
            $query->where('status', '!=', -1)->where('shop_id', $shop_id)->where('paid', 1);
        })->whereHas('coupon', function ($query) use ($shop_id) {
            $query->where('shop_id', $shop_id);
        })->get();

        $vouchers = 0;
        foreach ($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn - $vouchers);
    }
}


/**
 * Shop Earn Per Order Calculation
 */

if (!function_exists('get_shop_earn_per_order_paid')) {
    function get_shop_earn_per_order_paid($order_id, $shop_id)
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('shop_id', $shop_id)->where('status', '!=', -1)->whereHas('order', function ($query) use ($order_id) {
            $query->where('id', $order_id)->where('status', 2);
        })->where('paid', 1)->select(DB::raw('sum(shop_earn*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('id', $order_id)->where('status', 2)->whereHas('items', function ($query) use ($shop_id) {
            $query->where('status', '!=', -1)->where('shop_id', $shop_id)->where('paid', 1);
        })->whereHas('coupon', function ($query) use ($shop_id) {
            $query->where('shop_id', $shop_id);
        })->get();

        $vouchers = 0;
        foreach ($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn - $vouchers);
    }
}
