<?php

namespace App\Helpers;

use App\Models\Product;
use App\User;

class Cart
{
    public function __construct()
    {
        if ($this->get() === null){
            $this->set($this->empty());
            $this->set_code($this->empty());
            $this->set_dcharge($this->empty());

        }
    }

    public function add(Product $product, int $quantity = 1, array $details = array()): void
    {
        $cart = $this->get();
        $is_match = $this->match($product->id, $details);
        if($is_match != -1){
            $key = $is_match;
        }
        $quantity = ($is_match != -1) ? ($quantity + $cart[$key]['quantity']) : $quantity;
        $quantity = ($product->stock < $quantity) ? $product->stock : $quantity;
        $quantity = (1 > $quantity) ? 1 : $quantity;
        $details = ($is_match != -1) ? $cart[$key]['details']:$details;
        $item = array(
            'product_id'  => $product->id,
            'shop_id'  => $product->shop->id,
            'details'   => $details,
            'quantity'  => $quantity,
        );
        if($is_match != -1){
            $cart[$key] =  $item;
        } else{
            $cart[] =  $item;
        }
        $this->set($cart);
        $this->set_code($this->empty());
    }

    public function match($id, $details): int
    {
        $cart = $this->get();
        if ($cart === null) return -1;
        foreach((array) $cart as $key => $item){
            if($item['product_id'] == $id){
                if(empty($details)) return $key;

                if(!empty($details) && array_key_exists('color',$details) && array_key_exists('size',$details)){
                    if($item['details']['color'] == $details['color'] && $item['details']['size'] == $details['size']) return $key;
                }
                elseif(!empty($details) && array_key_exists('color',$details)){
                    if($item['details']['color'] == $details['color']) return $key;
                }
                elseif(!empty($details) && array_key_exists('size',$details)){
                    if($item['details']['size'] == $details['size']) return $key;
                }
            }
        }
        return -1;
    }

    public function remove(int $key): void
    {
        $cart = $this->get();
        // array_splice($cart, $key, 1);
        unset($cart[$key]);
        $this->set($cart);
        $this->set_code($this->empty());
    }

    public function clear(): void
    {
        $this->set($this->empty());
        $this->set_code($this->empty());
        $this->set_dcharge($this->empty());
    }

    public function empty(): array
    {
        return [];
    }

    public function get(): ?array
    {
        return request()->session()->get('cart');
    }

    private function set($cart): void
    {
        request()->session()->put('cart', $cart);
    }


    public function get_code(): ?array
    {
        return request()->session()->get('code');
    }

    public function set_code($code): void
    {
        request()->session()->put('code', $code);
    }



    public function get_dcharge(): ?array
    {
        return request()->session()->get('dcharge');
    }

    public function set_dcharge($dcharge): void
    {
        request()->session()->put('dcharge', $dcharge);
    }


    public function delivery_charge($city = null): void
    {

        $this->set_dcharge($this->empty());
        $cart = $this->get();
        $city = (!is_null($city)) ? $city:'Sylhet';
        $charge = array();
        $shops = array();
        foreach((array)$cart as $aa => $item){
            $shops[$item['shop_id']][] = array(
                'key'   => $aa,
                'item'  => $item
            );
        }
        foreach((array)$shops as $temp){
            $shop = User::with('shop')->where('id', $temp[0]['item']['shop_id'])->first();
            // dd($shop);
            if($shop->city == $city){
                $charge[] = array(
                    'shop_id'   => $shop->id,
                    'charge'    => $shop->shop->inside_city
                );
            } else{
                $charge[] = array(
                    'shop_id'   => $shop->id,
                    'charge'    => $shop->shop->outside_city
                );
            }
        }

        $this->set_dcharge($charge);



    }
}
