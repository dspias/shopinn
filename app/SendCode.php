<?php

namespace App;

use Exception;
use Nexmo\Laravel\Facade\Nexmo;

use App\Mail\VerificationCode;
use Mail;
use OnnoRokomSMS;

class SendCode
{
    public static function sendCode($medium, $type, $code = null)
    {
        $code = ($code == null) ? rand(000000, 999999) : $code;
        $message = 'Thank you for registering on ShopInn. Your Verification Code is: ';

        if ($type == 'mobile') {
            
            try {
                // Nexmo::message()->send([
                //     // $nexmo = app('Nexmo\Client');
                //     // $nexmo->message()->send([
                //     'to'   => '+880' . (int)$medium,
                //     'from' => 'Shopinn',
                //     'text' => $message . $code,
                // ]);
                $status = onnorokom_sms([
                                    'message' => $message . $code,
                                    'mobile_number' => '+880' . (int)$medium,
                                ]);
                if(SendCode::checkStatus($status) == false) return false;
                
            } catch (Exception $e) {
                return false;
            }
        } else {
            try {
                Mail::to($medium)->send(new VerificationCode(array('code' => $code, 'message' => $message)));
            } catch (Exception $e) {
                return false;
            }
        }
        return $code;
    }

    private static function checkStatus($status):int
    {
        if($status[0] != 1900) return false;
        return true;
    }
}
