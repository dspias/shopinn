<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountCalculation extends Controller
{
    public function distribution($order_id)
    {
        $order = Order::with('items')->where('id', $order_id)->first();
        $items = $order->items;

        foreach($items as $item){
            if($item->status != -1){
                $commission = get_admin_commision($item->shop_id);
                $item->shop_earn = $this->shopEarn($item, $commission);
                $item->admin_earn = $this->adminEarn($item, $commission);
                $item->save();
            }
        }
        //add reseller earn here
        if($order->is_reseller == 1){
            $order->reseller_earn = $this->resellerEarn($order->id);
            $order->save();
        }
    }

    private function shopEarn($item, $commission){
        $c_total = ($item->s_sell_price*$commission)/100;   //get total commission for this product
        return ($item->s_sell_price - $c_total);
    }

    private function adminEarn($item, $commission){
        return (($item->price - $item->s_sell_price) + ($item->s_sell_price*$commission)/100);   //get total commission for this product
    }
    private function resellerEarn($order_id){
        $commission = company_get('reseller_commision');
        $total = 0.00;
        $total = OrderItem::with('order')->where('status', '!=', -1)->whereHas('order', function($query) use ($order_id){
            $query->where('id', $order_id);
        })->select(DB::raw('sum(price*quantity) as total'))->get()->sum('total');

        return (($total*$commission)/100);   //get total commission for this product
    }
}
