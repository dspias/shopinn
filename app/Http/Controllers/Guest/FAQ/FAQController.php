<?php

namespace App\Http\Controllers\Guest\FAQ;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class FAQController extends Controller
{
    public function __construct()
    {
    }

    /**
     * FAQ Index
     */
    public function index()
    {
        $faqs =(array) company_get('faq');
        return view('guest.faq.index', compact(['faqs']));
    }
}
