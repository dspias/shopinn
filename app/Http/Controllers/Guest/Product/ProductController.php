<?php

namespace App\Http\Controllers\Guest\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductComment as Comment;
use App\Models\ProductReply as Reply;
use App\User;

use App\Notifications\User\Product\Comment as Ncomment;
use App\Notifications\User\Product\Reply as Nreply;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    /**
     * product featured
     */
    public function featured()
    {
        $products = Product::with('ratings')->whereNotNull('approved_at')->where('is_active', 1)->where('is_featured', 1)->get();
        return view('guest.product.featured', compact(['products']));
    }

    /**
     * product details
     */
    public function show($product_id, $product_name)
    {
        $product = Product::with([
            'shop' => function ($query) {
                $query->select('id', 'name', 'avatar');
            },
            'category.type' => function ($query) {
                $query->select('id', 'name');
            },
            'ratings' => function ($query) {
                $query->select('id', 'product_id', 'rate');
            },
            'reviews.customer',
            'comments',
            'offers',
            // 'orders' => function ($query) {
            //     $query->select('id', 'name', 'avatar');
            // },
            'sizes' => function ($query) {
                $query->select('id', 'name', 'product_id');
            },
            'colors' => function ($query) {
                $query->select('id', 'name', 'product_id');
            },
            'tags' => function ($query) {
                $query->select('id', 'name', 'product_id');
            },
        ])->where('id', $product_id)->firstOrFail();

        if($product->is_active != 1 && !auth()->check()){
            return abort(404);
        }
        elseif($product->is_active != 1 && auth()->check() && $product->shop_id != auth()->user()->id){
            return abort(404);
        }

        // dd($product);
        $matches = Product::inRandomOrder()->with(['shop', 'ratings'])->where('category_id', $product->category_id)->where('id', '!=', $product->id)->limit(4)->get();
        return view('guest.product.show', compact(['product', 'matches']));
    }




    /**
     * Product Comments
     */
    public function productComment(Request $request, $product_id)
    {
        $this->validate($request, array(
            'comment'   =>  'required | string | max:500'
        ));

        $comment = new Comment();
        $comment->product_id = $product_id;
        $comment->user_id = auth()->user()->id;
        $comment->comment = $request->comment;

        $product = Product::find($product_id);
        if(!is_null($product) && $comment->user_id != $product->shop_id){
            user_notify($product->shop_id, new Ncomment($product));
        }
        if ($comment->save()) {
            toast('Commented.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Sorry! Something Wrong!.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * Product Replies
     */
    public function productReply(Request $request, $comment_id)
    {
        $this->validate($request, array(
            'reply'   =>  'required | string | max:500'
        ));

        $reply = new Reply();
        $reply->user_id = auth()->user()->id;
        $reply->comment_id = $comment_id;
        $reply->reply = $request->reply;

        $comment = Comment::with('product')->where('id', $comment_id)->first();
        $product = optional($comment)->product;
        if(!is_null($product) && $reply->user_id != $product->shop_id){
            user_notify($product->shop_id, new Nreply($product));
        }
        if(!is_null($product) && $reply->user_id != $comment->user_id){
            user_notify($comment->user_id, new Nreply($product));
        }
        if ($reply->save()) {
            toast('Replied.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Sorry! Something Wrong!.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
