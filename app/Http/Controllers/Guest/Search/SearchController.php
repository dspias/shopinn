<?php

namespace App\Http\Controllers\Guest\Search;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;


class SearchController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Result
     */
    public function find(Request $request)
    {
        $this->validate($request, array(
            'search'    =>  'required | string'
        ));

        $key = $request->search;

        return redirect()->route('guest.search.index', ['search_keyword' => $key]);
    }

    /**
     * Homepage
     */
    public function index($search_keyword)
    {
        $key = $search_keyword;
        return view('guest.search.index', compact(['key']));
    }
}
