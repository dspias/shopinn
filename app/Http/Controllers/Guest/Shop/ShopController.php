<?php

namespace App\Http\Controllers\Guest\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Models\Shop;

class ShopController extends Controller
{
    public function __construct()
    {
    }

    /**
     * All Shops
     */
    public function index()
    {
        $shops = User::inRandomOrder()->with(['shop'])->whereNotNull('approved_at')->where('is_active', 1)->where('role_id', 2)->get();
        return view('guest.shop.index', compact(['shops']));
    }

    /**
     * Shop Details
     */
    public function show($shop_id, $shop_name)
    {
        $shop = User::with(['shop', 'products.ratings'])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where('role_id', 2)
            ->where('id', $shop_id)
            ->firstOrFail();
        return view('guest.shop.show', compact(['shop']));
    }

    /**
     * Shop search Details
     */
    public function search($shop_id)
    {
        $this->validate($request, array(
            'key' => 'required | string',
        ));
        $key = $request->key;

        $shop = User::with(['shop', 'products.ratings'])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where('role_id', 2)
            ->where('id', $shop_id)
            ->whereHas('products', function ($q) use ($key) {
                $q->where("name", "LIKE", "%$key%");
            })
            ->whereHas('products.category', function ($q) use ($key) {
                $q->where("name", "LIKE", "%$key%");
            })
            ->firstOrFail();
        return view('guest.shop.search', compact(['shop']));
    }
}
