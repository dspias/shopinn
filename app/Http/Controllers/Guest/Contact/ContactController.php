<?php

namespace App\Http\Controllers\Guest\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Mail;
use App\Mail\Contact;
use Exception;


class ContactController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Contact Index
     */
    public function index()
    {
        return view('guest.contact.index');
    }
    /**
     * Contact Index
     */
    public function sendmail(Request $request)
    {
        $this->validate($request, array(
            'name'  =>  'required | string',
            'email'  =>  'required | string | email',
            'mobile'  =>  'required | regex:/(01)[0-9]{9}/ |min:11|max:14',
            'subject'  =>  'required | string',
            'message'  =>  'required | string',
            'g-recaptcha-response' => 'required|captcha',
        ));
        try {
            Mail::to(env('TO_MAIL'))->send(new Contact($request));
        } catch (Exception $e) {
            toast('Does not sent Your Mail, please try again.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
        toast('Sent Your Mail', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
