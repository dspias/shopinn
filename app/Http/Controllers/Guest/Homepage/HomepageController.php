<?php

namespace App\Http\Controllers\Guest\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductType;
use App\User;


class HomepageController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Homepage
     */
    public function index()
    {
        $products = Product::inRandomOrder()
            ->with([
                'shop' => function ($query) {
                    $query->select('id', 'name');
                },
                'ratings',
                'offers',
                'sizes',
                'colors'
            ])->whereNotNull('approved_at')->where('is_active', 1)->whereHas('category', function($nested){
                $nested->where('name', '!=', 'Lingerie');
            })->orderBy('id', 'desc')->paginate(20)->onEachSide(1);

        $types = ProductType::with(['categories' => function ($query) {
            $query->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        // dd($types);

        $latest = Product::select('id', 'name')->whereNotNull('approved_at')->orderBy('id', 'desc')->where('is_active', 1)->limit(6)->get();

        $shops = User::where('role_id', 2)
            ->whereNotNull('approved_at')
            ->orderBy('id', 'desc')
            ->where('is_active', 1)
            ->limit(6)->get();
        $resellers = User::select('id', 'name')
            ->where('role_id', 3)
            ->whereNotNull('approved_at')
            ->orderBy('id', 'desc')
            ->where('is_active', 1)
            ->limit(6)->get();

        $features = Product::select('id', 'name')->where('is_featured', 1)->where('is_active', 1)->whereNotNull('approved_at')->get();

        // dd($products);
        return view('guest.homepage.index', compact(['products', 'features', 'latest', 'types', 'shops', 'resellers']));
    }

    /**
     * search
     */
    public function category(Request $request)
    {
        // dd($request);
        $cat_id = decrypt($request->cat_id);
        $products = Product::with([
            'shop' => function ($query) {
                $query->select('id', 'name', 'avatar');
            },
            'ratings',
            'offers'
        ])->whereNotNull('approved_at')->where('is_active', 1)->where('category_id', $cat_id)->orderBy('id', 'desc')->paginate(20);

        $types = ProductType::with(['categories' => function ($query) {
            $query->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        // dd($types);

        $latest = Product::select('id', 'name')->whereNotNull('approved_at')->orderBy('id', 'desc')->where('is_active', 1)->limit(6)->get();

        $shops = User::select('id', 'name')
            ->where('role_id', 2)
            ->whereNotNull('approved_at')
            ->orderBy('id', 'desc')
            ->where('is_active', 1)
            ->limit(6)->get();

        $features = Product::select('id', 'name')->where('is_featured', 1)->where('is_active', 1)->whereNotNull('approved_at')->get();

        // dd($products);
        return view('guest.homepage.index', compact('products', 'features', 'latest', 'shops', 'types'));
    }

    /**
     * search
     */
    public function search(Request $request)
    {
        // dd($request);
        $keyword = $request->keyword;
        $products = Product::with([
            'shop' => function ($query) {
                $query->select('id', 'name', 'avatar');
            },
            'ratings',
            'offers'
        ])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where("name", "LIKE", "%$keyword%")
            ->orWhere("description", "LIKE", "%$keyword%")
            ->orderBy('id', 'desc')
            ->paginate(20);

        $types = ProductType::with(['categories' => function ($query) {
            $query->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        $latest = Product::select('id', 'name')->whereNotNull('approved_at')->orderBy('id', 'desc')->limit(6)->get();


        $shops = User::select('id', 'name')
            ->where('role_id', 2)
            ->whereNotNull('approved_at')
            ->orderBy('id', 'desc')
            ->limit(6)->get();
        $resellers = User::select('id', 'name')
            ->where('role_id', 3)
            ->whereNotNull('approved_at')
            ->orderBy('id', 'desc')
            ->limit(6)->get();

        $features = Product::select('id', 'name')->where('is_featured', 1)->where('is_active', 1)->whereNotNull('approved_at')->get();

        // dd($products);
        return view('guest.homepage.index', compact(['products', 'features', 'latest', 'types', 'shops', 'resellers']));
    }


    /**
     * changeLang
     */
    public function changeLang(Request $request, $lang)
    {
        $user = User::findOrFail(auth()->user()->id);
        $user->lang = $lang;

        if($user->save()){
            toast('Language Changed Successfully', 'success')->autoClose(2000)->timerProgressBar();
        } else{
            toast('Language Does not Changed', 'error')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }






    // !About Section
    public function aboutShopinn()
    {
        return view('guest.about.index');
    }
}
