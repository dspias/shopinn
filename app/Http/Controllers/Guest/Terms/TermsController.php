<?php

namespace App\Http\Controllers\Guest\Terms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class TermsController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Terms $ Policy Index
     */
    public function index()
    {
        $terms =(array) company_get('term');
        $policies =(array) company_get('privacy');
        return view('guest.terms.index', compact(['terms', 'policies']));
    }
}
