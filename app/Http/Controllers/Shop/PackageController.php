<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\BusinessPackage;
use App\Models\PackageLog;
use App\Notifications\Admin\NewPackage;

class PackageController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Package Index
     */
    public function index()
    {
        $activated = subsribed(auth()->user()->id);
        if(!$activated) $activated = null;
        $packages = BusinessPackage::where('is_active', 1)->get();
        return view('shop.setting.package.index', compact(['packages', 'activated']));
    }

    public function buy($package_id)
    {
        $id = decrypt($package_id);
        $package = BusinessPackage::findOrFail($id);

        return view('shop.setting.package.buy', compact(['package']));
    }

    public function activeted($package_id)
    {
        $id = decrypt($package_id);
        $package = BusinessPackage::findOrFail($id);

        $activateds = PackageLog::with('package')->where('shop_id', auth()->user()->id)->where('is_active', 1)->orderBy('id', 'desc')->get();

        foreach($activateds as $activated){
            $activated->is_active = 0;
            $activated->end = date('Y-m-d');
            $activated->save();
        }

        $log = new PackageLog();
        $log->shop_id = auth()->user()->id;
        $log->package_id = $package->id;

        //current time not paid
        $log->paid = 0;
        $log->start = date('Y-m-d');
        if($package->package_type == 1){
            $log->end = date('Y-m-d', strtotime($log->start. ' + 30 days'));
        }
        elseif($package->package_type == 3){
            $day = ($package->year*365) + ($package->month*30) + $package->day;
            $log->end = date('Y-m-d', strtotime($log->start. ' + '.$day.' days'));
        }
        // dd($package, $log);

        if ($log->save()) {
            // Generate new NOTIFICATION for order Confirm from Shop
            admin_notify(new NewPackage($log));
            toast('Subsribed new Package.', 'success')->autoClose(1000)->timerProgressBar();
            return redirect()->route('shop.setting.package.index');
        } else {
            toast('Does not subsribe.', 'warning')->autoClose(1000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
