<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct(){
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeProduct = Product::where('shop_id', auth()->user()->id)->where('is_active', 1)->count();
        $inactiveProduct = Product::where('shop_id', auth()->user()->id)->where('is_active', 0)->count();
        $order = Order::where('status', '!=', -1)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id);
        })->count();
        $todayOrder = Order::where('status', '!=', -1)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id);
        })->whereDate('created_at', date('Y-m-d'))->count();
        $pendingOrder = Order::where('status', '!=', -1)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id);
        })->where('status', '!=', 2)->count();

        $total_sale = $this->totalSale();
        $total_commission = $this->totalCommision();
        $payable = $this->calculatePayable();
        $paid = $this->calculatePaid();

        return view('shop.dashboard.index',
                compact([
                    'activeProduct',
                    'inactiveProduct',
                    'order',
                    'pendingOrder',
                    'todayOrder',
                    'total_sale',
                    'total_commission',
                    'payable',
                    'paid',
                ]));
    }


    private function totalSale()
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('shop_id', auth()->user()->id)->where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->select(DB::raw('sum(shop_earn*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('status', 2)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id);
        })->whereHas('coupon', function($query){
            $query->where('shop_id', auth()->user()->id);
        })->get();

        $vouchers = 0;
        foreach($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn-$vouchers);
    }


    private function totalCommision()
    {
        $total_admin_earn = 0.00;
        $total_admin_earn = OrderItem::with('order')->where('shop_id', auth()->user()->id)->where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->select(DB::raw('sum(admin_earn*quantity) as total'))->get()->sum('total');

        return $total_admin_earn;
    }


    private function calculatePayable()
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('shop_id', auth()->user()->id)->where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->where('paid', 0)->select(DB::raw('sum(shop_earn*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('status', 2)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id)->where('paid', 0);
        })->whereHas('coupon', function($query){
            $query->where('shop_id', auth()->user()->id);
        })->get();

        $vouchers = 0;
        foreach($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn-$vouchers);
    }
    private function calculatePaid()
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('shop_id', auth()->user()->id)->where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->where('paid', 1)->select(DB::raw('sum(shop_earn*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('status', 2)->whereHas('items', function($query){
            $query->where('status', '!=', -1)->where('shop_id', auth()->user()->id)->where('paid', 1);
        })->whereHas('coupon', function($query){
            $query->where('shop_id', auth()->user()->id);
        })->get();

        $vouchers = 0;
        foreach($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn-$vouchers);
    }
}
