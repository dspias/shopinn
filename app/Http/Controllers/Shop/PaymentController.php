<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop Payment Index
     */
    public function index()
    {
        return view('shop.payment.index');
    }
}
