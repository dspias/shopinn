<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;

use App\Models\ProductType as Type;

use App\Models\Color;
use App\Models\Size;
use App\Models\Tag;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop Product Index
     */
    public function index()
    {
        $products = Product::with('ratings')->where('shop_id', auth()->user()->id)->get();
        return view('shop.product.index', compact(['products']));
    }

    /**
     * Shop Product review
     */
    public function review()
    {
        $products = Product::with('ratings')->where('shop_id', auth()->user()->id)->whereNull('approved_at')->get();
        return view('shop.product.review', compact(['products']));
    }

    /**
     * Shop Product featured
     */
    public function featured()
    {
        $products = Product::with('ratings')->where('shop_id', auth()->user()->id)->whereNotNull('approved_at')->where('is_active', 1)->where('is_featured', 1)->get();
        return view('shop.product.featured', compact(['products']));
    }

    /**
     * Shop Product active
     */
    public function active()
    {
        $products = Product::with('ratings')->where('shop_id', auth()->user()->id)->whereNotNull('approved_at')->where('is_active', 1)->get();
        return view('shop.product.active', compact(['products']));
    }

    /**
     * Shop Product inactive
     */
    public function inactive()
    {
        $products = Product::with('ratings')->where('shop_id', auth()->user()->id)->whereNotNull('approved_at')->where('is_active', 0)->get();
        return view('shop.product.inactive', compact(['products']));
    }



    /**
     * Shop Product create
     */
    public function create()
    {
        $types = Type::with(['categories' => function ($q) {
            $q->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        $colors = Color::select('name')->distinct('name')->get();
        $sizes = Size::select('name')->distinct('name')->get();
        $tags = Tag::select('name')->distinct('name')->get();
        return view('shop.product.create', compact(['colors', 'sizes', 'tags', 'types']));
    }



    /**
     * Shop Product changeFeaturedStatus
     */
    public function changeFeaturedStatus($product_id)
    {
        $id = decrypt($product_id);
        $prodcut = Product::findOrFail($id);

        if ($prodcut->approved_at == null) {
            toast('Product did not approved', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        $prodcut->is_featured = ($prodcut->is_featured == 1) ? 0 : 1;

        if ($prodcut->save()) {
            toast('Prodcut featured status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Prodcut featured status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * change_status prodcuts
     */
    public function change_status($prodcut_id)
    {
        $id = decrypt($prodcut_id);
        $product = Product::findOrFail($id);

        if ($product->approved_at == null) {
            toast('Product did not approved', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
        
        $product->is_active = ($product->is_active == 1) ? 0 : 1;

        if ($product->save()) {
            toast('Product status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Product status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * edit prodcuts
     */
    public function edit($prodcut_id)
    {
        // dd($prodcut_id);
        $id = decrypt($prodcut_id);
        $product = Product::with(['category.type', 'colors', 'tags',])->where('id', $id)->firstOrFail();


        $types = Type::with(['categories' => function ($q) {
            $q->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        return view('shop.product.edit', compact(['types', 'product']));
    }
}
