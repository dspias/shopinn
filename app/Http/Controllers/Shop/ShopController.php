<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop Shop Index
     */
    public function index()
    {
        return view('shop.setting.shop.index');
    }


    /**
     * Profile Index
     */
    public function storeAvatar(Request $request)
    {
        // dd($request);

        if ($request->type == 'cover') {
            $this->validate($request, array(
                'shop_avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096'
            ));
        } else {
            $this->validate($request, array(
                'shop_avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
            ));
        }


        try {
            $user = auth()->user();
            $mediaItems = $user->getMedia($request->type);
            foreach ($mediaItems as $item)
                $item->delete();

            $user->addMedia($request->shop_avatar)->toMediaCollection($request->type);
        } catch (Exception $e) {
            toast('Image does not update. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        toast('Image changed.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
