<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProductType as Type;
use App\Models\ProductCategory as Category;
use App\Models\Product as MProduct;

use App\Models\Color;
use App\Models\Size;
use App\Models\Tag;

use App\Http\Controllers\Backend\Product as Product;

use App\Notifications\Admin\Product\NewProduct;

class ProfileController extends Controller
{
    private $storage_path;

    public function __construct()
    {
        $this->middleware(['auth', 'shop']);
        // $this->storage_path = 'app/public/products/';
        // $this->storage_path = 'app\public\products\\';

    }

    /**
     * Shop Profile Index
     */
    public function index()
    {
        $types = Type::with(['categories' => function ($q) {
            $q->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();

        $colors = Color::select('name')->distinct('name')->get();
        $sizes = Size::select('name')->distinct('name')->get();
        $tags = Tag::select('name')->distinct('name')->get();

        $products = MProduct::with('ratings')->where('shop_id', auth()->user()->id)->get();
        return view('shop.profile.index', compact(['colors', 'sizes', 'tags', 'types', 'products']));
    }

    /**
     * Shop Profile Edit
     */
    public function edit()
    {
        return view('shop.profile.edit');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'description'   => 'required | string',
            'name'          => 'required | string | max:255',
            'stock'         => 'required | integer | min:0',
            // 'buy_price'         => 'required | integer | min:0',
            'sell_price'         => 'required | integer | min:0',
            'type_id'         => 'required | integer | min:0',
            'cat_id'         => 'required | integer | min:0',
            // 'sizes'         => 'required',
            // 'colors'         => 'required',

            'files' => 'required',
            'files.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ));

        $data = Product::store($request);

        if ($data != null) {
            // Generate new NOTIFICATION for creating new Product
            $shop = auth()->user();
            admin_notify(new NewProduct($shop, $data));

            toast('Product Created Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Opp! Product does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $this->validate($request, array(
            'description'   => 'required | string',
            'name'          => 'required | string | max:255',
            'stock'         => 'required | integer | min:0',
            // 'buy_price'         => 'required | integer | min:0',
            'sell_price'         => 'required | integer | min:0',
            // 'sizes'         => 'required',
            // 'colors'         => 'required',
        ));

        if ($request->hasFile('files')) {
            $this->validate($request, array(
                'files' => 'required',
                'files.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ));
        }

        $id = decrypt($id);
        $data = Product::update($request, $id);

        if ($data != null) {
            toast('Product Updated Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Opp! Product does not Updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
