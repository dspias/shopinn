<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdvertiseLog;
use App\Models\AdvertiseLogDetails;
use App\Models\AdvertisePackage;
use App\Models\AdvertiseType;

use App\Notifications\Admin\AdvertisePackage as AdPackage;
use App\Notifications\Admin\NewAd;

class AdvertiseController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop Advertise Index
     */
    public function index()
    {
        $checks = AdvertiseLogDetails::where('end', '<', date('Y-m-d'))->where('is_active', 1)->get();
        foreach($checks as $check){ $check->is_active = 0; $check->save(); }

        $advertises = AdvertiseLogDetails::with('ad_log')->where('is_active', 1)->get();
        $active_logs = AdvertiseLog::select(['id', 'package_id', 'start', 'end'])
            ->with(['package' => function ($query) {
                $query->select(['id', 'name']);
            }])
            ->where('is_active', 1)
            ->where('end', '>=', date('Y-m-d'))
            ->orWhereNull('end')
            ->orderBy('id', 'asc')
            ->where('shop_id', auth()->user()->id)
            ->get();
        return view('shop.setting.advertise.index', compact(['advertises', 'active_logs']));
    }

    /**
     * Shop Advertise Package Register
     */
    public function package_register()
    {
        $packages = AdvertisePackage::where('is_active', 1)->get();
        $shop_id = auth()->user()->id;
        $active_packages = AdvertiseLog::select(['id', 'package_id', 'start', 'end'])
            ->with(['package' => function ($query) {
                $query->select(['id', 'name']);
            }])
            ->where('is_active', 1)
            ->where('end', '>=', date('Y-m-d'))
            ->orWhereNull('end')
            ->orderBy('id', 'desc')
            ->where('shop_id', $shop_id)
            ->get();

        return view('shop.setting.advertise.package_register', compact(['packages', 'active_packages']));
    }

    /**
     * Shop Advertise Register
     */
    public function register(Request $request)
    {
        $this->validate($request, array(
            'package_id' => 'required | integer | exists:advertise_packages,id',
            // 'package_id.exists' => 'No Package Available With This ID',
            // 'package_price_hidden' => 'required | integer',
            'day' => 'required | integer',
            'pay_by' => 'required | string',
        ));
        $package = AdvertisePackage::select(['price'])->where('id', $request->package_id)->firstOrFail();
        // dd($request, $package->price);

        $register = new AdvertiseLog();
        $register->shop_id = auth()->user()->id;
        $register->package_id = $request->package_id;
        $register->day = $request->day;
        $register->start = date('Y-m-d');
        $register->end = date('Y-m-d', strtotime($register->start . ' + ' . $request->day . ' days'));
        $register->pay_by = $request->pay_by;
        $register->paid = false;
        $register->price = $package->price * $request->day;
        $register->is_active = true;

        if ($register->save()) {
            // Generate new NOTIFICATION for order Confirm from Shop
            admin_notify(new AdPackage($register));
            if ($request->pay_by == 'shopinn') {
                toast('Your Advertise Package Registration Successful. Shopinn Will Contact You Soon.', 'info')->autoClose(false)->timerProgressBar();
            } else {
                toast('Your Advertise Package Registration Successful..', 'success')->autoClose(2000)->timerProgressBar();
            }
            return redirect()->back();
        } else {
            toast('Your Advertise Package Registration Has Been Declined. There is some error. Please try again or contact with shopinn.', 'error')->autoClose(false)->timerProgressBar();
            return redirect()->back();
        }
    }



    /**
     * Shop Advertise create
     */
    public function create($log_id)
    {
        $log_id = decrypt($log_id);
        $active_log = AdvertiseLog::select(['id', 'package_id', 'start', 'end'])
            ->with(['package' => function ($query) {
                $query->select(['id', 'name']);
            }, 'ad_details'])
            ->where('id', $log_id)
            ->where('is_active', 1)
            ->where('end', '>=', date('Y-m-d'))
            ->orWhereNull('end')
            ->orderBy('id', 'desc')
            ->where('shop_id', auth()->user()->id)
            ->first();
        $types = array();
        // dd($active_packages[0]->package->types);
        foreach ($active_log->package->types as $type) {
            $used = (!empty($active_log->ad_details)) ? $active_log->ad_details->where('type_id', $type->id)->count() : 0;
            if (($type->pivot->max_ad - $used) > 0) {
                $types[] = array(
                    'name' => $type->name,
                    'type_id' => $type->id,
                    'width' => $type->width,
                    'height' => $type->height,
                    'min_size' => $type->min_size,
                    'max_size' => $type->max_size,
                    'remaining' => $type->pivot->max_ad - $used,
                );
            }
        }
        return view('shop.setting.advertise.create', compact(['types', 'active_log']));
    }

    /**
     * Shop Advertise store
     */
    public function store(Request $request, $log_id)
    {
        // dd($request, $log_id);
        //validation here
        $this->validate($request, array(
            'type_id'       =>  'required | integer',
            'title'         =>  'required | string | max:191',
            'url_link'      =>  'required | url',
        ));
        $type = AdvertiseType::findOrFail($request->type_id);
        $rule = 'required | image | mimes:jpeg,png,jpg,gif,svg | max:' . $type->max_size . ' | min:' . $type->min_size . ' | dimensions:min_width=' . $type->width . ',min_height=' . $type->height . ', max_width=' . $type->width . ',max_height=' . $type->height;
        $this->validate($request, array(
            'ad_image'      =>  $rule,
        ));

        $ad = new AdvertiseLogDetails();
        $ad->log_id = $log_id;
        $ad->type_id = $request->type_id;
        $ad->title = $request->title;
        $ad->url_link = $request->url_link;

        $log = AdvertiseLog::findOrFail($log_id);
        $ad->start = date('Y-m-d');
        $ad->end = $log->end;

        $ad->ad_image = $request->file('ad_image')->store('ShopAd', 'public');

        if ($ad->save()) {
            // Generate new NOTIFICATION for order Confirm from Shop
            admin_notify(new NewAd($ad));
            toast('Advertise Created Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('shop.setting.advertise.index');
        } else {
            toast('Advertise Does Not Created. There is some error.', 'error')->autoClose(5000)->timerProgressBar();
            return redirect()->back();
        }
    }


    public function show($ad_id)
    {
        $ad_id = decrypt($ad_id);
        $ad = AdvertiseLogDetails::findOrFail($ad_id);
        return view('shop.setting.advertise.show', compact(['ad']));
    }

    
    /**
     * Shop Advertise store
     */
    public function update(Request $request, $ad_id)
    {
        $ad = AdvertiseLogDetails::findOrfail($ad_id);
        $type = $ad->type;

        $rule = 'required | image | mimes:jpeg,png,jpg,gif,svg | max:' . $type->max_size . ' | min:' . $type->min_size . ' | dimensions:min_width=' . $type->width . ',min_height=' . $type->height . ', max_width=' . $type->width . ',max_height=' . $type->height;
        $this->validate($request, array(
            'ad_image'      =>  $rule,
        ));

        $path = storage_path('app/public/'.$ad->ad_image);
        if(file_exists($path)){
            @unlink($path);
        }
        $ad->ad_image = $request->file('ad_image')->store('ShopAd', 'public');

        if ($ad->save()) {
            toast('Advertise Updated Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('shop.setting.advertise.index');
        } else {
            toast('Advertise Does Not Updated. There is some error.', 'error')->autoClose(5000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
