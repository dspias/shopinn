<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Offer;
use App\Models\Coupon;

class OfferCouponController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop offer Index
     */
    public function offerIndex()
    {
        $offers = Offer::select('shop_id', 'title', 'discount', 'expire_date', 'created_at')->where('shop_id', auth()->user()->id)->distinct('title')->get();
        return view('shop.setting.offer.index', compact(['offers']));
    }

    /**
     * Shop offer Create
     */
    public function offerCreate()
    {
        $categories = Product::with('category')->where('shop_id', auth()->user()->id)->where('is_active', 1)->whereNotNUll('approved_at')->get()->groupBy('category_id');
        return view('shop.setting.offer.create', compact(['categories']));
    }

    /**
     * Shop offer edit
     */
    public function offeredit($name)
    {
        $offers = Offer::select('product_id', 'title', 'discount', 'expire_date', 'created_at')->where('shop_id', auth()->user()->id)->where('title', $name)->get();

        $categories = Product::with('category')->where('shop_id', auth()->user()->id)->where('is_active', 1)->whereNotNUll('approved_at')->get()->groupBy('category_id');

        return view('shop.setting.offer.edit', compact(['categories', 'offers']));
    }

    /**
     * Shop offer Store
     */
    public function offerStore(Request $request)
    {
        $this->validate($request, array(
            'title'     => 'required | string | max:191',
            'discount'  => 'required | integer',
            'expire_date'  => 'required',
        ));
        if (!isset($request->is_all)) {
            $this->validate($request, array(
                'products'     => 'required',
            ));
        }
        if (isset($request->is_all)) {
            $products = Product::select('id')->where('shop_id', auth()->user()->id)->where('is_active', 1)->whereNotNUll('approved_at')->get();
            foreach ($products as $product) {
                $tt = Offer::where('product_id', $product->id)->get();
                if (count($tt) > 0) {
                    foreach ($tt as $t) $t->forceDelete();
                }

                $offer = new Offer();
                $offer->title = $request->title;
                $offer->discount = $request->discount;
                $offer->expire_date = $request->expire_date;
                $offer->product_id = $product->id;
                $offer->shop_id = auth()->user()->id;
                $offer->save();
            }
        } else {
            foreach ($request->products as $product) {
                $tt = Offer::where('product_id', $product)->get();
                if (count($tt) > 0) {
                    foreach ($tt as $t) $t->forceDelete();
                }

                $offer = new Offer();
                $offer->title = $request->title;
                $offer->discount = $request->discount;
                $offer->expire_date = $request->expire_date;
                $offer->product_id = $product;
                $offer->shop_id = auth()->user()->id;
                $offer->save();
            }
        }
        toast('New Offer Created', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->route('shop.setting.offer.index');
    }

    /**
     * Shop offer Update
     */
    public function offerUpdate(Request $request, $name)
    {
        $this->validate($request, array(
            'title'     => 'required | string | max:191',
            'discount'  => 'required | integer',
            'expire_date'  => 'required',
        ));
        if (!isset($request->is_all)) {
            $this->validate($request, array(
                'products'     => 'required',
            ));
        }
        if (isset($request->is_all)) {
            $products = Product::select('id')->where('shop_id', auth()->user()->id)->where('is_active', 1)->whereNotNUll('approved_at')->get();
            foreach ($products as $product) {
                $tt = Offer::where('product_id', $product->id)->get();
                if (count($tt) > 0) {
                    foreach ($tt as $t) $t->forceDelete();
                }

                $offer = new Offer();
                $offer->title = $request->title;
                $offer->discount = $request->discount;
                $offer->expire_date = $request->expire_date;
                $offer->product_id = $product->id;
                $offer->shop_id = auth()->user()->id;
                $offer->save();
            }
        } else {
            $dofs = Offer::where('title', $name)->get();
            foreach ($dofs as $of) $of->forceDelete();

            foreach ($request->products as $product) {
                $tt = Offer::where('product_id', $product)->get();
                if (count($tt) > 0) {
                    foreach ($tt as $t) $t->forceDelete();
                }

                $offer = new Offer();
                $offer->title = $request->title;
                $offer->discount = $request->discount;
                $offer->expire_date = $request->expire_date;
                $offer->product_id = $product;
                $offer->shop_id = auth()->user()->id;
                $offer->save();
            }
        }
        toast('Offer updated', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->route('shop.setting.offer.index');
    }








    /**
     * ============================================================================
     * ===================================< Coupons >==============================
     * ============================================================================
     */


    /**
     * Shop coupon Index
     */
    public function couponIndex()
    {
        $coupons = Coupon::where('shop_id', auth()->user()->id)->get();
        return view('shop.setting.coupon.index', compact(['coupons']));
    }


    /**
     * Shop coupon Create
     */
    public function couponCreate()
    {
        return view('shop.setting.coupon.create');
    }

    /**
     * Shop coupon Edot
     */
    public function couponEdit($id)
    {
        $id = decrypt($id);
        $coupon = Coupon::findOrFail($id);
        return view('shop.setting.coupon.edit', compact(['coupon']));
    }


    /**
     * Shop coupon Store
     */
    public function couponStore(Request $request)
    {
        $this->validate($request, array(
            "title"         => "required | string",
            "code"          => "required | string | unique:coupons",
            "discount"      => "required | integer",
            "min_purchase"  => "required | integer",
            "limit"         => "required | integer",
            "expire_date"   => "required",
        ));

        $coupon = new Coupon();

        $coupon->title         = $request->title;
        $coupon->code          = $request->code;
        $coupon->discount      = $request->discount;
        $coupon->min_purchase  = $request->min_purchase;
        $coupon->limit         = $request->limit;
        $coupon->expire_date   = $request->expire_date;
        $coupon->shop_id       = auth()->user()->id;

        if ($coupon->save()) {
            toast('New coupon created', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('shop.setting.coupon.index');
        } else {
            toast('New coupon does not created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * Shop coupon Update
     */
    public function couponUpdate(Request $request, $id)
    {

        $this->validate($request, array(
            "title"         => "required | string",
            "discount"      => "required | integer",
            "min_purchase"  => "required | integer",
            "limit"         => "required | integer",
            "expire_date"   => "required",
        ));


        $id = decrypt($id);
        $coupon = Coupon::findOrFail($id);
        if ($coupon->code != $request->code) {
            $this->validate($request, array(
                "code"          => "required | string | unique:coupons",
            ));
        }

        $coupon->title         = $request->title;
        $coupon->code          = $request->code;
        $coupon->discount      = $request->discount;
        $coupon->min_purchase  = $request->min_purchase;
        $coupon->limit         = $request->limit;
        $coupon->expire_date   = $request->expire_date;

        if ($coupon->save()) {
            toast('Coupon updated', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('shop.setting.coupon.index');
        } else {
            toast('New coupon does not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
