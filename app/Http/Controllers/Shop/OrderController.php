<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

use App\Notifications\Admin\Order\Cancel;
use App\Notifications\Admin\Order\Confirm;
use App\Notifications\Customer\Order\Canceled;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Shop Order Index
     */
    public function index()
    {
        $id = auth()->user()->id;
        $orders = Order::with([
            'items' => function ($query) use ($id) {
                $query->where('shop_id', $id);
            },
            'coupon'
        ])
            ->whereHas('items', function ($query) use ($id) {
                $query->where('shop_id', $id);
            })
            ->get();
        // dd($orders);

        $all = $orders->count();
        $cancel = Order::whereHas('items', function ($query) use ($id) {
            $query->where('shop_id', $id)->where('status', -1);
        })->orWhere('status', -1)->count();
        // dd($cancel);
        $pending = Order::whereHas('items', function ($query) use ($id) {
            $query->where('shop_id', $id)->where('status','!=', -1);
        })->where('status', 0)->count();
        $running = Order::whereHas('items', function ($query) use ($id) {
            $query->where('shop_id', $id)->where('status','!=', -1);
        })->where('status', 1)->count();;
        $complete = Order::whereHas('items', function ($query) use ($id) {
            $query->where('shop_id', $id)->where('status','!=', -1);
        })->where('status', 2)->count();;
        // dd($complete);

        return view('shop.order.index', compact(['orders', 'all', 'cancel', 'pending', 'running', 'complete']));
    }

    /**
     * Shop Order new
     */
    public function new()
    {
        $id = auth()->user()->id;
        $orders = Order::with([
            'items' => function ($query) use ($id) {
                $query->where('shop_id', $id);
            },
            'coupon'
        ])
            ->whereHas('items', function ($query) use ($id) {
                $query->where('shop_id', $id);
            })
            ->where('status', 0)
            ->get();

        return view('shop.order.new', compact(['orders']));
    }

    /**
     * Shop Order running
     */
    public function running()
    {
        $id = auth()->user()->id;
        $orders = Order::with([
            'items' => function ($query) use ($id) {
                $query->where('shop_id', $id);
            },
            'coupon'
        ])
            ->whereHas('items', function ($query) use ($id) {
                $query->where('shop_id', $id);
            })
            ->where('status', 1)
            ->get();

        return view('shop.order.running', compact(['orders']));
    }

    /**
     * Shop Order completed
     */
    public function completed()
    {
        $id = auth()->user()->id;
        $orders = Order::with([
            'items' => function ($query) use ($id) {
                $query->where('shop_id', $id);
            },
            'coupon'
        ])
            ->whereHas('items', function ($query) use ($id) {
                $query->where('shop_id', $id);
            })
            ->where('status', 2)
            ->get();

        return view('shop.order.completed', compact(['orders']));
    }

    /**
     * Shop Order canceled
     */
    public function canceled()
    {
        $id = auth()->user()->id;
        $orders = Order::with([
            'items' => function ($query) use ($id) {
                $query->where('shop_id', $id);
            },
            'coupon'
        ])
            ->whereHas('items', function ($query) use ($id) {
                $query->where('shop_id', $id);
            })
            ->where('status', -1)
            ->get();

        return view('shop.order.canceled', compact(['orders']));
    }


    /**
     * Order Details
     */
    public function show($order_type, $order_id)
    {
        $id = decrypt($order_id);
        $shopId = auth()->user()->id;

        $order = Order::with([
            'items' => function ($query) use ($shopId) {
                $query->where('shop_id', $shopId);
            },
            'coupon'
        ])->where('id', $id)->first();
        // dd($order);
        return view('shop.order.show', compact(['order', 'order_type']));
    }

    /**
     * Order Status
     */
    public function status($order_id, $value)
    {
        $items = OrderItem::with('order')->where('order_id', $order_id)->where('shop_id', auth()->user()->id)->get();

        if($items[0]->order->status != 0){
            toast('Order already Confirmed', 'warning')->autoClose(1000)->timerProgressBar();
        }

        foreach ($items as $item) {
            $item->status = ($value == 1) ? 1 : -1;
            $item->save();
        }

        
        $order = Order::with([
            'items',
            'coupon'
        ])->where('id', $order_id)->firstOrFail();
        $flag = true;
        foreach($order->items as $item){
            if($item->status == 0 || $item->status == 1) $flag =  false;
        }
        
        if($flag){
            $order->status = -1;
            $order->save();
            
            /**
             * ! make Notifaition for new order to customer
             */
            user_notify($order->customer_id, new Canceled($order));
        }

        $shop = auth()->user();
        if ($value == 1) {
            // Generate new NOTIFICATION for order Confirm from Shop
            admin_notify(new Confirm($order, $shop));

            toast('Order Approved.', 'success')->autoClose(1000)->timerProgressBar();
        } else {
            // Generate new NOTIFICATION for order cancel from Shop
            admin_notify(new Cancel($order, $shop));

            toast('Order Canceled.', 'warning')->autoClose(1000)->timerProgressBar();
        }

        return redirect()->back();                                                          
    }
}
