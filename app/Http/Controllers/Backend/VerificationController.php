<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Code;
use App\SendCode;
use Illuminate\Http\Request;
use DB;

class VerificationController extends Controller
{
    public function sendcode(Request $request)
    {
        $email = null;
        $mobile = null;
        $code = null;
        $sendCode = null;

        if (isset($request->email)) {
            $email = $request->email;
            $code = Code::where('email', $email)->first();
        }

        if (isset($request->mobile)) {
            $mobile = $request->mobile;
            $code = Code::where('mobile', $mobile)->first();
        }

        if ($code == null) {
            $sendCode = ($mobile != null) ? SendCode::sendCode($mobile, 'mobile') : SendCode::sendCode($email, 'email');
        } else {
            $sendCode = ($mobile != null) ? SendCode::sendCode($mobile, 'mobile', $code->code) : SendCode::sendCode($email, 'email', $code->code);
        }

        if ($sendCode != false) {
            if ($code == null) {
                $code = new Code();
                $code->email = $email;
                $code->mobile = $mobile;

                $code->code = $sendCode;

                $code->save();
            }
            return response('Code Sent Successfully!');
        }
        return response('Code Sent Failed!');
    }


    public function checkcode(Request $request)
    {
        // dd($request);
        $requestCode = $request->code;
        $email = null;
        $mobile = null;
        $code = null;


        if (isset($request->email)) {
            $email = $request->email;
            $code = Code::where('email', $email)->first();
        }

        if (isset($request->mobile) && $code == null) {
            $mobile = $request->mobile;
            $code = Code::where('mobile', $mobile)->first();
        }

        if ($code != null) {
            if ($code->code == $requestCode) {
                return response(true);
            } else {
                return response(false);
            }
        } else {
            return response(false);
        }
    }


    /**
     * notification mark as read
     */
    public function markasread($id)
    {

        $notification = auth()->user()->notifications()->where('id', $id)->first();
        $notification->markAsRead();
        return redirect()->to($notification->data['link']);
    }
}
