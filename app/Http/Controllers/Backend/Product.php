<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product as MoProduct;
use App\Models\Color;
use App\Models\Size;
use App\Models\Tag;
use App\Http\Controllers\FileUploader as FileUpload;


class Product extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {
        // dd($request);
        $pid = auth()->user()->id . strtotime(now()) . $request->cat_id;

        $data = MoProduct::create([
            'shop_id'          => auth()->user()->id,
            'pid'              => $pid,
            'description'      => $request->description,
            'name'             => $request->name,
            'stock'            => $request->stock,
            'buy_price'        => $request->buy_price,
            'sell_price'       => $request->sell_price,
            'category_id'      => $request->cat_id,
        ]);
        $data->approved_at = now();
        $data->save();

        foreach ((array)$request->colors as $clr) {
            $color = new Color();

            $color->product_id = $data->id;
            $color->name = $clr;

            if (!$color->save()) {
                $dcolors = Color::where('product_id', $data->id)->get();
                foreach ($dcolors as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }

        foreach ((array)$request->sizes as $sz) {
            $size = new Size();

            $size->product_id = $data->id;
            $size->name = $sz;

            if (!$size->save()) {
                $dsizes = Size::where('product_id', $data->id)->get();
                foreach ($dsizes as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }

        foreach ((array)$request->tags as $tg) {
            $tag = new Tag();

            $tag->product_id = $data->id;
            $tag->name = $tg;

            if (!$tag->save()) {
                $dtags = Tag::where('product_id', $data->id)->get();
                foreach ($dtags as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }
        foreach ($request->files as $files) {
            foreach ((array)$files as $file) {
                $data->addMedia($file)->toMediaCollection('product');
            }
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function update(Request $request, $id)
    {
        // dd($request);

        $data = MoProduct::with(['tags', 'colors', 'sizes',])->where('id', $id)->first();

        $data->description      = $request->description;
        $data->name             = $request->name;
        $data->stock            = $request->stock;
        $data->buy_price        = $request->buy_price;
        $data->sell_price       = $request->sell_price;
        if ($request->cat_id != null) $data->category_id = $request->cat_id;

        foreach ($data->colors as $tmp) $tmp->delete();
        foreach ($data->sizes as $tmp) $tmp->delete();
        foreach ($data->tags as $tmp) $tmp->delete();

        if ($request->hasFile('files')) {
            $mediaItems = $data->getMedia('product');
            foreach ($mediaItems as $item) $item->delete();
        }

        foreach ((array)$request->colors as $clr) {
            $color = new Color();

            $color->product_id = $data->id;
            $color->name = $clr;

            if (!$color->save()) {
                $dcolors = Color::where('product_id', $data->id)->get();
                foreach ($dcolors as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }

        foreach ((array)$request->sizes as $sz) {
            $size = new Size();

            $size->product_id = $data->id;
            $size->name = $sz;

            if (!$size->save()) {
                $dsizes = Size::where('product_id', $data->id)->get();
                foreach ($dsizes as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }

        foreach ((array)$request->tags as $tg) {
            $tag = new Tag();

            $tag->product_id = $data->id;
            $tag->name = $tg;

            if (!$tag->save()) {
                $dtags = Tag::where('product_id', $data->id)->get();
                foreach ($dtags as $tmp) $tmp->delete();
                $data->forceDelete();
                return null;
            }
        }
        foreach ($request->files as $files) {
            foreach ((array)$files as $file) {
                $data->addMedia($file)->toMediaCollection('product');
            }
        }

        $data->save();

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
