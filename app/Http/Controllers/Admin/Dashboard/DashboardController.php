<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Models\ProductReview;
use App\Http\Controllers\Controller;
use App\Models\AdminAdvertise;
use App\Models\AdvertiseLogDetails;
use App\Models\BusinessPackage;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct(){
        // constructors here
    }

    /**
     * Dashboard
     */
    public function index()
    {
        $total_sale = $this->totalSale();

        $profit_and_cost = $this->adminProfit();
        $total_shopinn_earn = $profit_and_cost['result'];
        $extra_cost = $profit_and_cost['extra_cost'];
        $rider_cost = $profit_and_cost['rider_cost'];
        $reseller_cost = $profit_and_cost['reseller_cost'];

        $chart = $this->makeChartValue();

        $orders = $this->getOrders();
        $shops = $this->getShops();
        $packages = $this->getPackages();
        $riders = $this->getRiders();
        $advertises = $this->getAdvertises();
        $products = $this->getProducts();
        $resellers = $this->getResellers();
        $customers = $this->getCustomers();

        $reviews = $this->getReviews();

        return view('admin.dashboard.index',
            compact([
                'total_sale',
                'total_shopinn_earn',
                'extra_cost',
                'rider_cost',
                'reseller_cost',
                'chart',
                'orders',
                'shops',
                'packages',
                'riders',
                'advertises',
                'products',
                'resellers',
                'customers',
                'reviews',
            ]));
    }

    private function totalSale()
    {
        $total_sale = 0.00;
        $total_sale = OrderItem::where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->select(DB::raw('sum(price*quantity) as total'))->get()->sum('total');
        return $total_sale;
    }

    private function adminProfit()
    {
        $total_shopinn_earn = OrderItem::where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2);
        })->select(DB::raw('sum(admin_earn*quantity) as total'))->get()->sum('total');

        $total_d_charge = Order::where('status', 2)->sum('delivery');
        $rider_cost = Order::where('status', 2)->sum('rider_earn');
        $extra_cost = Order::where('status', 2)->sum('extra_cost');
        $reseller_cost = Order::where('status', 2)->sum('reseller_earn');

        $result = ($total_shopinn_earn + $total_d_charge) - ($rider_cost + $reseller_cost + $extra_cost);

        $data = array(
            'result' => $result,
            'rider_cost' => $rider_cost,
            'extra_cost' => $extra_cost,
            'reseller_cost' => $reseller_cost,
        );
        return $data;
    }

    private function makeChartValue()
    {
        $data = array();
        $calculation = array();
        for($i=0; $i<=11; $i++) {
            $calculation[$i] = $this->getProfitInDate( $this->mdate($i+1));
        }
        for($i=0; $i<=11; $i++){
            $data['earn'][$i] = $calculation[$i]['earn'];
            $data['cost'][$i] = $calculation[$i]['cost'];
            $data['sale'][$i] = $calculation[$i]['sale'];
        }
        return $data;
    }

    private function mdate($m)
    {
        $year = date('Y');
        $datestr = $year."-".$m."-01";
        date_default_timezone_set (date_default_timezone_get());
        $dt = strtotime ($datestr);
        return array (
            "start" => date ('Y-m-d', strtotime ('first day of this month', $dt)),
            "end" => date ('Y-m-d', strtotime ('last day of this month', $dt))
        );
    }

    private function getProfitInDate($daterange)
    {
        $start = $daterange['start'];
        $end = $daterange['end'];

        $sale = OrderItem::where('status', '!=', -1)->whereHas('order', function($query) use ($start, $end){
            $query->where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end);
        })->select(DB::raw('sum(price*quantity) as total'))->get()->sum('total');

        $total_shopinn_earn = OrderItem::where('status', '!=', -1)->whereHas('order', function($query) use ($start, $end){
            $query->where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end);
        })->select(DB::raw('sum(admin_earn*quantity) as total'))->get()->sum('total');

        $total_d_charge = Order::where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end)->sum('delivery');
        $rider_cost = Order::where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end)->sum('rider_earn');
        $extra_cost = Order::where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end)->sum('extra_cost');
        $reseller_cost = Order::where('status', 2)->where('status_at', '>=', $start)->where('status_at', '<=', $end)->sum('reseller_earn');

        $total_cost = $rider_cost + $reseller_cost + $extra_cost;
        $result = ($total_shopinn_earn + $total_d_charge) - $total_cost;

        return array(
            'sale' => $sale,
            'earn' => $result,
            'cost' => $total_cost,
        );
    }





    private function getOrders()
    {
        $orders = Order::select('id', 'status')->get();
        $all = $orders->count();
        $cancel = $orders->where('status', -1)->count();
        $pending = $orders->where('status', 0)->count();
        $running = $orders->where('status', 1)->count();
        $complete = $orders->where('status', 2)->count();
        return array(
            'all' => $all,
            'cancel' => $cancel,
            'pending' => $pending,
            'running' => $running,
            'complete' => $complete,
        );
    }



    /**
     * Get Shops
     */
    private function getShops()
    {
        $shops = User::where('role_id', 2)->get();
        $all = $shops->count();
        $active = $shops->whereNotNull('approved_at')->where('is_active', 1)->count();
        $pending = $shops->whereNull('approved_at')->count();
        $inactive = $shops->whereNotNull('approved_at')->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'pending' => $pending,
            'inactive' => $inactive,
        );
    }



    /**
     * Get Packages
     */
    private function getPackages()
    {
        $packages = BusinessPackage::all();
        $all = $packages->count();
        $active = $packages->where('is_active', 1)->count();
        $inactive = $packages->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'inactive' => $inactive,
        );
    }



    /**
     * Get Riders
     */
    private function getRiders()
    {
        $riders = User::where('role_id', 4)->get();
        $all = $riders->count();
        $active = $riders->whereNotNull('approved_at')->where('is_active', 1)->count();
        $pending = $riders->whereNull('approved_at')->count();
        $inactive = $riders->whereNotNull('approved_at')->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'pending' => $pending,
            'inactive' => $inactive,
        );
    }



    /**
     * Get Advertises
     */
    private function getAdvertises()
    {
        $advertises = AdvertiseLogDetails::all();
        $all = $advertises->count();
        $active = $advertises->where('is_active', 1)->count();

        $adminAds = AdminAdvertise::all();
        $all += $adminAds->count();
        $active += $adminAds->where('is_active', 1)->count();
        return array(
            'all' => $all,
            'active' => $active,
        );
    }



    /**
     * Get Products
     */
    private function getProducts()
    {
        $products = Product::all();
        $all = $products->count();
        $active = $products->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive = $products->whereNotNull('approved_at')->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'inactive' => $inactive,
        );
    }



    /**
     * Get Resellers
     */
    private function getResellers()
    {
        $resellers = User::where('role_id', 3)->get();
        $all = $resellers->count();
        $active = $resellers->whereNotNull('approved_at')->where('is_active', 1)->count();
        $pending = $resellers->whereNull('approved_at')->count();
        $inactive = $resellers->whereNotNull('approved_at')->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'pending' => $pending,
            'inactive' => $inactive,
        );
    }



    /**
     * Get Customers
     */
    private function getCustomers()
    {
        $customers = User::where('role_id', 5)->get();
        $all = $customers->count();
        $active = $customers->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive = $customers->whereNotNull('approved_at')->where('is_active', 0)->count();
        return array(
            'all' => $all,
            'active' => $active,
            'inactive' => $inactive,
        );
    }


    private function getReviews($limit = 5){
        $reviews = ProductReview::limit($limit)->get();
        return $reviews;
    }
}
