<?php

namespace App\Http\Controllers\Admin\Shop;

use App\User;
use Exception;

use App\Models\Shop;
use App\Mail\LoginInfo;
use App\Models\PackageLog;
use Illuminate\Http\Request;
use App\Models\BusinessPackage;
use App\Http\Controllers\Controller;
use App\Mail\ResetShopPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;


class ShopController extends Controller
{
    public function __construct()
    {
        // constructors here
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All shops
     */
    public function all()
    {
        // calcualte
        $all = User::where('role_id', 2)->count();
        $active = User::where('role_id', 2)->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive = User::where('role_id', 2)->whereNotNull('approved_at')->where('is_active', 0)->count();
        $pending = User::where('role_id', 2)->whereNull('approved_at')->count();

        $shops = User::with(['shop'])->where('role_id', 2)->get();
        return view('admin.shop.all', compact(['shops', 'all', 'active', 'inactive', 'pending']));
    }

    /**
     * pending shops
     */
    public function pending()
    {
        // calcualte
        $all = User::where('role_id', 2)->count();
        $pending = User::where('role_id', 2)->whereNull('approved_at')->count();

        $shops = User::with(['shop'])->where('role_id', 2)->whereNull('approved_at')->get();
        return view('admin.shop.pending', compact(['shops', 'all', 'pending']));
    }

    /**
     * active shops
     */
    public function active()
    {
        // calcualte
        $all = User::where('role_id', 2)->count();
        $active = User::where('role_id', 2)->whereNotNull('approved_at')->where('is_active', 1)->count();

        $shops = User::with(['shop'])->whereNotNull('approved_at')->where('is_active', 1)->where('role_id', 2)->get();
        return view('admin.shop.active', compact(['shops', 'all', 'active']));
    }

    /**
     * inactive shops
     */
    public function inactive()
    {
        // calcualte
        $all = User::where('role_id', 2)->count();
        $inactive = User::where('role_id', 2)->whereNotNull('approved_at')->where('is_active', 0)->count();

        $shops = User::with(['shop'])->whereNotNull('approved_at')->where('is_active', 0)->where('role_id', 2)->get();
        return view('admin.shop.inactive', compact(['shops', 'all', 'inactive']));
    }

    /**
     * create shops
     */
    public function create()
    {
        return view('admin.shop.create');
    }

    /**
     * store shops
     */
    public function store(Request $request)
    {
        //validation here
        $this->validate($request, array(
            'email'         =>  'required | string | email | max:255 | unique:users',
            'mobile'        =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11 | unique:users',
            'password'      =>  'required | string  | min:8',
            'name'          =>  'required | string',
            'shop_mobile'   =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11',
            'shop_email'    =>  'required | string | email | max:255',
            'city'          =>  'required | string',
            'shop_address'  =>  'required | string',
            'owner_name'    =>  'required | string',
            'owner_mobile'  =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11',
        ));

        if ($request->owner_email != null) {
            $this->validate($request, array(
                'owner_email'   =>  'required | string | email | max:255',
            ));
        }
        if ($request->owner_address != null) {
            $this->validate($request, array(
                'owner_address'   =>  'required | string',
            ));
        }
        if ($request->note != null) {
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }


        // store in user table
        $user = User::create(array(
            'role_id'   =>  2,
            'name'      =>  $request->name,
            'password'  =>  Hash::make($request->password)
        ));
        $user->approved_at = now();
        if ($request->hasfile('avatar')) {
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));
            $user->avatar = $request->file('avatar')->store('shopLogo', 'public');
        }
        $user->city = $request->city;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        if (!isset($request->is_active)) $user->is_active = 0;

        // store shop info
        $shop = new Shop();
        $shop->shop_id = $user->id;
        $shop->shop_mobile = $request->shop_mobile;
        $shop->shop_email = $request->shop_email;
        $shop->shop_address = $request->shop_address;

        $shop->owner_name = $request->owner_name;
        $shop->owner_mobile = $request->owner_mobile;
        $shop->owner_email = $request->owner_email;
        $shop->owner_address = $request->owner_address;
        $shop->note = $request->note;

        if ($shop->save() && $user->save()) {

            if (isset($request->is_email_info)) {
                try {
                    Mail::to($request->email)->send(new LoginInfo($request));
                } catch (Exception $e) {
                }
            }

            toast('Shop created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.shop.all');
        } else {
            toast('Shop does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * details shops
     */
    public function details($page, $shop_id)
    {
        $id = decrypt($shop_id);
        $shop = User::with(['shop', 'products.shop', 'paymentReports'])->where('id', $id)->where('role_id', 2)->firstOrFail();
        if (is_null($shop->shop)) {
            toast('Shop does not Updated there profile.', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        switch ($page) {
            case 'pending':
                $page = 'Pending Shops';
                $route = 'admin.shop.pending';
                break;
            case 'active':
                $page = 'Active Shops';
                $route = 'admin.shop.active';
                break;
            case 'inactive':
                $page = 'Inactive Shops';
                $route = 'admin.shop.inactive';
                break;
            default:
                $page = 'All Shops';
                $route = 'admin.shop.all';
                break;
        }

        $packages = PackageLog::where('shop_id', $id)->orderBy('id', 'desc')->get();

        $all_packages = BusinessPackage::select(['id', 'name'])->where('is_active', 1)->orderBy('id', 'asc')->get();

        return view('admin.shop.details', compact(['shop', 'page', 'route', 'packages', 'all_packages']));
    }

    /**
     * change_status shops
     */
    public function change_status($shop_id)
    {
        $id = decrypt($shop_id);
        $shop = User::findOrFail($id);

        $shop->is_active = ($shop->is_active == 1) ? 0 : 1;

        if ($shop->save()) {
            toast('Shop status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Shop status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * update_delivery shops
     */
    public function update_delivery(Request $request, $shop_id)
    {
        $id = decrypt($shop_id);
        $shop = Shop::where('shop_id', $id)->firstOrFail();

        $this->validate($request, array(
            'inside_city'   =>  'required | integer',
            'outside_city'  =>  'required | integer',
        ));

        $shop->inside_city = $request->inside_city;
        $shop->outside_city = $request->outside_city;

        if ($shop->save()) {
            toast('Shop delivery charge changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Shop delivery charge does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * approve shops
     */
    public function approve(Request $request, $shop_id)
    {
        $this->validate($request, array(
            'inside_city'   =>  'required | integer | min:0',
            'outside_city'  =>  'required | integer | min:0',
        ));

        $id = decrypt($shop_id);
        $user = User::findOrFail($id);

        $user->approved_at = ($user->approved_at == null) ? now() : $user->approved_at;

        $shop = Shop::where('shop_id', $user->id)->first();

        if ($shop != NULL) {
            $shop->inside_city = $request->inside_city;
            $shop->outside_city = $request->outside_city;
            $shop->save();
        } else {
            toast('This shop has not filled their proper informations.', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        if ($user->save()) {
            toast('Shop Approved.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Shop does not approved. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }



    /**
     * Package Assign
     */
    public function assignPackage(Request $request, $shop_id)
    {
        // dd($shop_id, $request);
        $shop_id = decrypt($shop_id);
        $package = BusinessPackage::findOrFail($request->package_id);

        $activateds = PackageLog::with('package')->where('shop_id', $shop_id)->where('is_active', 1)->orderBy('id', 'desc')->get();

        foreach($activateds as $activated){
            $activated->is_active = 0;
            $activated->end = date('Y-m-d');
            $activated->save();
        }

        $log = new PackageLog();
        $log->shop_id = $shop_id;
        $log->package_id = $package->id;

        //current time not paid
        $log->paid = 0;
        $log->start = date('Y-m-d');
        if ($package->package_type == 1) {
            $log->end = date('Y-m-d', strtotime($log->start . ' + 30 days'));
        } elseif ($package->package_type == 3) {
            $day = ($package->year * 365) + ($package->month * 30) + $package->day;
            $log->end = date('Y-m-d', strtotime($log->start . ' + ' . $day . ' days'));
        }
        // dd($package, $log);

        if ($log->save()) {
            toast('Subsribed new Package.', 'success')->autoClose(3000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Does not subsribe.', 'warning')->autoClose(3000)->timerProgressBar();
            return redirect()->back();
        }
    }




    /**
     * Reset Password For Shop
     */
    public function resetPassword($shop_id)
    {
        $shop_id = decrypt($shop_id);
        $shop = User::where('id', $shop_id)->firstOrFail();
        // $shop = User::findOrFail($shop_id);
        // dd($shop);

        $password = 'ShopinnP@ssword';
        
        Mail::to($shop->email)->send(new ResetShopPassword($shop, $password));
        Mail::to(env('TO_MAIL', 'write@shopinnbd.com'))->send(new ResetShopPassword($shop, $password));

        $shop->password = Hash::make($password);
        $shop->save();

        toast('Password Reseted Successfully.', 'success')->autoClose(3000)->timerProgressBar();
        return redirect()->back();
    }
}
