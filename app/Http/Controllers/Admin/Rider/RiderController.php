<?php

namespace App\Http\Controllers\Admin\Rider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Rider;
use Mail;
use App\Mail\LoginInfo;
use Exception;

class RiderController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All Riders
     */
    public function all()
    {
        // calcualte
        $all= User::where('role_id', 4)->count();
        $active= User::where('role_id', 4)->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive= User::where('role_id', 4)->whereNotNull('approved_at')->where('is_active', 0)->count();
        $pending= User::where('role_id', 4)->whereNull('approved_at')->count();

        $riders = User::with(['rider'])->where('role_id', 4)->get();
        return view('admin.rider.all', compact(['riders', 'all', 'active', 'inactive', 'pending']));
    }

    /**
     * pending Riders
     */
    public function pending()
    {
        // calcualte
        $all= User::where('role_id', 4)->count();
        $pending= User::where('role_id', 4)->whereNull('approved_at')->count();

        $riders = User::with(['rider'])->where('role_id', 4)->whereNull('approved_at')->get();
        return view('admin.rider.pending', compact(['riders', 'all','pending']));
    }

    /**
     * active Riders
     */
    public function active()
    {
        // calcualte
        $all= User::where('role_id', 4)->count();
        $active= User::where('role_id', 4)->whereNotNull('approved_at')->where('is_active', 1)->count();

        $riders = User::with(['rider'])->whereNotNull('approved_at')->where('is_active', 1)->where('role_id', 4)->get();
        return view('admin.rider.active', compact(['riders', 'all', 'active']));
    }

    /**
     * inactive Riders
     */
    public function inactive()
    {
        // calcualte
        $all= User::where('role_id', 4)->count();
        $inactive= User::where('role_id', 4)->whereNotNull('approved_at')->where('is_active', 0)->count();

        $riders = User::with(['rider'])->whereNotNull('approved_at')->where('is_active', 0)->where('role_id', 4)->get();
        return view('admin.rider.inactive', compact(['riders', 'all', 'inactive']));
    }

    /**
     * create new rider
     */
    public function create()
    {
        return view('admin.rider.create');
    }

    /**
     * store rider
     */
    public function store(Request $request)
    {
        // dd($request);
        //validation here
        $this->validate($request, array(
            'email'         =>  'required | string | email | max:255 | unique:users',
            'mobile'        =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11 | unique:users',
            'password'      =>  'required | string  | min:8',
            'name'          =>  'required | string',
            'address'   =>  'required | string',
            'city'   =>  'required | string',
        ));

        if($request->rider_email != null){
            $this->validate($request, array(
                'rider_email'   =>  'required | string | email | max:255',
            ));
        }
        if($request->rider_mobile != null){
            $this->validate($request, array(
                'rider_mobile'   =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11',
            ));
        }
        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }


        // store in user table
        $user = User::create(array(
            'role_id'   =>  4,
            'name'      =>  $request->name,
            'password'  =>  Hash::make($request->password)
        ));
        $user->approved_at = now();
        if($request->hasfile('avatar')){
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));
            $user->avatar = $request->file('avatar')->store('rider', 'public');
        }
        $user->city = $request->city;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        if(!isset($request->is_active)) $user->is_active = 0;

        // store rider info
        $rider = new Rider();
        $rider->rider_id = $user->id;

        $rider->rider_email = $request->rider_email;
        $rider->rider_mobile = $request->rider_mobile;
        $rider->address = $request->address;
        $rider->note = $request->note;

        if($rider->save() && $user->save()){

            if(isset($request->is_email_info)){
                try{
                    Mail::to($request->email)->send(new LoginInfo($request));
                } catch (Exception $e){

                }
            }

            toast('Rider created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.rider.all');
        } else{
            toast('Rider does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * details riders
     */
    public function details($page, $rider_id)
    {
        $id = decrypt($rider_id);
        $rider = User::with(['rider', 'orderRiders', 'paymentReports', 'riderDeliveryReports'])->where('id', $id)->where('role_id', 4)->firstOrFail();

        switch ($page) {
            case 'pending':
                $page = 'Pending Riders';
                $route = 'admin.rider.pending';
                break;
            case 'active':
                $page = 'Active Riders';
                $route = 'admin.rider.active';
                break;
            case 'inactive':
                $page = 'Inactive Riders';
                $route = 'admin.rider.inactive';
                break;
            default:
                $page = 'All Riders';
                $route = 'admin.rider.all';
                break;
        }

        return view('admin.rider.details', compact(['rider', 'page', 'route']));
    }

    /**
     * change_status riders
     */
    public function change_status($rider_id)
    {
        $id = decrypt($rider_id);
        $rider = User::findOrFail($id);

        $rider->is_active = ($rider->is_active == 1) ? 0:1;

        if($rider->save()){
            toast('Rider status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Rider status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * approve riders
     */
    public function approve($rider_id)
    {
        $id = decrypt($rider_id);
        $rider = User::findOrFail($id);

        $rider->approved_at = ($rider->approved_at == null) ? now():$rider->approved_at;

        if($rider->save()){
            toast('Rider Approved.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Rider does not approved. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }




}
