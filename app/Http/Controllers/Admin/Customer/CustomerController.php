<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Hash;
use App\User;
use Mail;
use App\Mail\LoginInfo;
use Exception;

class CustomerController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }
    
    /**
     * All Customers
     */
    public function all()
    {
        
        // calcualte
        $all= User::where('role_id', 5)->count();
        $active= User::where('role_id', 5)->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive= User::where('role_id', 5)->whereNotNull('approved_at')->where('is_active', 0)->count();

        $customers = User::where('role_id', 5)->get();
        return view('admin.customer.all', compact(['customers', 'all', 'active', 'inactive']));
    }
    
    /**
     * banned Customers
     */
    public function banned()
    {
        
        // calcualte
        $all= User::where('role_id', 5)->count();
        $inactive= User::where('role_id', 5)->whereNotNull('approved_at')->where('is_active', 0)->count();

        $customers = User::where('role_id', 5)->where('is_active', 0)->get();
        return view('admin.customer.banned', compact(['customers', 'all', 'inactive']));
    }
    
    /**
     * create Customer
     */
    public function create()
    {
        return view('admin.customer.create');
    }


    /**
     * store customer
     */
    public function store(Request $request)
    {
        // dd($request);
        //validation here
        $this->validate($request, array(
            'email'         =>  'required | string | email | max:255 | unique:users',
            'mobile'        =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11 | unique:users',
            'password'      =>  'required | string  | min:8 | max:255',
            'name'          =>  'required | string | max:255',
            'city'          =>  'required | string | max:255',
        ));

        if($request->address != null){
            $this->validate($request, array(
                'address'   =>  'required | string',
            ));
        }
        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }

        
        // store in user table
        $user = User::create(array(
            'role_id'   =>  5,
            'name'      =>  $request->name,
            'password'  =>  Hash::make($request->password)
        ));
        $user->approved_at = now();
        if($request->hasfile('avatar')){
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));
            $user->avatar = $request->file('avatar')->store('customer', 'public');
        }
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->city = $request->city;
        $user->address = $request->address;
        $user->note = $request->note;
        if(!isset($request->is_active)) $user->is_active = 0;

        if($user->save()){
            
            if(isset($request->is_email_info)){
                try{
                    Mail::to($request->email)->send(new LoginInfo($request));
                } catch (Exception $e){
                    
                }
            }
            
            toast('Customer created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.customer.all');
        } else{
            toast('Customer does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    
    
    /**
     * details customers
     */
    public function details($page, $customer_id)
    {
        $id = decrypt($customer_id);
        $customer = User::where('id', $id)->where('role_id', 5)->firstOrFail();

        return view('admin.customer.details', compact(['customer', 'page']));
    }
    
    /**
     * change_status customers
     */
    public function change_status($customer_id)
    {
        $id = decrypt($customer_id);
        $customer = User::findOrFail($id);

        $customer->is_active = ($customer->is_active == 1) ? 0:1;

        if($customer->save()){
            toast('Customer status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Customer status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

}
