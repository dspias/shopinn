<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\PaymentOrderList;
use App\Models\PaymentReport;
use App\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * All Payments
     */
    public function all()
    {
        $payments = PaymentReport::orderBy('id', 'desc')->get();
        return view('admin.payment.all', compact(['payments']));
    }

    /**
     * Create New Payment
     */
    public function create()
    {
        $shops = User::where('role_id', 2)->get();
        $resellers = User::where('role_id', 3)->get();
        $riders = User::where('role_id', 4)->get();
        return view('admin.payment.create', compact(['shops', 'resellers', 'riders']));
    }

    /**
     * createInvoice
     */
    public function createInvoice(Request $request)
    {
        $user_id = $request->user_id;
        $user = User::where('id', $user_id)->firstOrFail();
        $role = $user->role_id;
        if ($role == 2) {
            $orders = Order::where('status', 2)->whereHas('items', function ($query) use ($user_id) {
                $query->where('status', '!=', -1)->where('shop_id', $user_id)->where('paid', 0);
            })->get();
        } elseif ($role == 3) {
            $orders = Order::where('status', 2)->where('is_reseller', 1)->where('customer_id', $user_id)->where('reseller_paid', 0)->get();
        } elseif ($role == 4) {
            $orders = Order::where('status', 2)->where('rider_id', $user_id)->where('rider_paid', 0)->get();
        } else $orders = array();
        return view('admin.payment.invoice', compact(['user', 'orders']));
    }

    /**
     * invoice
     */
    public function invoice(Request $request, $user_id)
    {
        $this->validate($request, array(
            'orders' => 'required|array|min:1',
            'orders.*' => 'required',
        ));
        $user = User::findOrFail($user_id);
        if (!is_null($user)) {
            $pay_id = 'SHOPINN' . $user_id . strtotime(now());
            $payment  = PaymentReport::create(array(
                'user_id' => $user_id,
                'invoice_id' => $pay_id,
            ));

            foreach ((array) $request->orders as $order) {
                $order = Order::with('items')->where('id', $order)->first();
                if (!is_null($order)) {
                    if ($user->role_id == 2) {
                        foreach ($order->items as $item) {
                            if ($item->shop_id == $user_id) {
                                $item->paid = 1;
                                $item->save();
                            }
                        }
                    } elseif ($user->role_id == 3) {
                        $order->reseller_paid = 1;
                        $order->save();
                    } elseif ($user->role_id == 4) {
                        $order->rider_paid = 1;
                        $order->save();
                    }
                    $payOrder = new PaymentOrderList();
                    $payOrder->pay_rep_id = $payment->id;
                    $payOrder->order_id = $order->id;
                    $payOrder->save();
                }
            }
            toast('Payment Completed', 'success')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->route('admin.payment.show', ['payment_id' => $payment->id]);
    }



    /**
     * show New Payment
     */
    public function show($payment_id)
    {
        $payment = PaymentReport::findOrFail($payment_id);
        return view('admin.payment.show', compact(['payment']));
    }
}
