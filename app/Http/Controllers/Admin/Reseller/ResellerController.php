<?php

namespace App\Http\Controllers\Admin\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Reseller;
use Mail;
use App\Mail\LoginInfo;
use Exception;

class ResellerController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All Resellers
     */
    public function all()
    {
        // calcualte
        $all= User::where('role_id', 3)->count();
        $active= User::where('role_id', 3)->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive= User::where('role_id', 3)->whereNotNull('approved_at')->where('is_active', 0)->count();
        $pending= User::where('role_id', 3)->whereNull('approved_at')->count();

        $resellers = User::with(['reseller'])->where('role_id', 3)->get();
        return view('admin.reseller.all', compact(['resellers', 'all', 'active', 'inactive', 'pending']));
    }

    /**
     * pending Resellers
     */
    public function pending()
    {
        // calcualte
        $all= User::where('role_id', 3)->count();
        $pending= User::where('role_id', 3)->whereNull('approved_at')->count();

        $resellers = User::with(['reseller'])->where('role_id', 3)->whereNull('approved_at')->get();
        return view('admin.reseller.pending', compact(['resellers', 'all','pending']));
    }

    /**
     * active Resellers
     */
    public function active()
    {
        // calcualte
        $all= User::where('role_id', 3)->count();
        $active= User::where('role_id', 3)->whereNotNull('approved_at')->where('is_active', 1)->count();

        $resellers = User::with(['reseller'])->whereNotNull('approved_at')->where('is_active', 1)->where('role_id', 3)->get();
        return view('admin.reseller.active', compact(['resellers', 'all', 'active']));
    }

    /**
     * inactive Resellers
     */
    public function inactive()
    {
        // calcualte
        $all= User::where('role_id', 3)->count();
        $inactive= User::where('role_id', 3)->whereNotNull('approved_at')->where('is_active', 0)->count();

        $resellers = User::with(['reseller'])->whereNotNull('approved_at')->where('is_active', 0)->where('role_id', 3)->get();
        return view('admin.reseller.inactive', compact(['resellers', 'all', 'inactive']));
    }

    /**
     * create Resellers
     */
    public function create()
    {
        return view('admin.reseller.create');
    }

    /**
     * store Reseller
     */
    public function store(Request $request)
    {
        // dd($request);
        //validation here
        $this->validate($request, array(
            'email'         =>  'required | string | email | max:255 | unique:users',
            'mobile'        =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11 | unique:users',
            'password'      =>  'required | string  | min:8 | max:255',
            'owner_name'    =>  'required | string | max:255',
            'owner_address'   =>  'required | string',
            'city'   =>  'required | string',
        ));


        // store in user table
        $user = User::create(array(
            'role_id'   =>  3,
            'name'      =>  $request->owner_name,
            'password'  =>  Hash::make($request->password)
        ));
        $user->approved_at = now();
        if($request->hasfile('avatar')){
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));
            $user->avatar = $request->file('avatar')->store('reseller', 'public');
        }
        $user->email = $request->email;
        $user->city = $request->city;
        $user->address = $request->owner_address;
        $user->mobile = $request->mobile;
        if(!isset($request->is_active)) $user->is_active = 0;

        if($user->save()){

            if(isset($request->is_email_info)){
                try{
                    Mail::to($request->email)->send(new LoginInfo($request));
                } catch (Exception $e){

                }
            }

            toast('Reseller created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.reseller.all');
        } else{
            toast('Reseller does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * details resellers
     */
    public function details($page, $reseller_id)
    {
        $id = decrypt($reseller_id);
        $reseller = User::with(['reseller', 'paymentReports'])->where('id', $id)->where('role_id', 3)->firstOrFail();

        switch ($page) {
            case 'pending':
                $page = 'Pending Resellers';
                $route = 'admin.reseller.pending';
                break;
            case 'active':
                $page = 'Active Resellers';
                $route = 'admin.reseller.active';
                break;
            case 'inactive':
                $page = 'Inactive Resellers';
                $route = 'admin.reseller.inactive';
                break;
            default:
                $page = 'All Resellers';
                $route = 'admin.reseller.all';
                break;
        }

        return view('admin.reseller.details', compact(['reseller', 'page', 'route']));
    }

    /**
     * change_status resellers
     */
    public function change_status($reseller_id)
    {
        $id = decrypt($reseller_id);
        $reseller = User::findOrFail($id);

        $reseller->is_active = ($reseller->is_active == 1) ? 0:1;

        if($reseller->save()){
            toast('Reseller status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Reseller status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * approve resellers
     */
    public function approve($reseller_id)
    {
        $id = decrypt($reseller_id);
        $reseller = User::findOrFail($id);

        $reseller->approved_at = ($reseller->approved_at == null) ? now():$reseller->approved_at;

        if($reseller->save()){
            toast('reseller Approved.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('reseller does not approved. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * approve resellers
     */
    public function commission()
    {
        return view('admin.reseller.commission');
    }

    /**
     * approve resellers
     */
    public function commissionStore(Request $request)
    {
        $this->validate($request, array(
            'commission' => 'required'
        ));
        company_set('reseller_commision', 'double', $request->commission);

        toast('reseller Approved.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }









}
