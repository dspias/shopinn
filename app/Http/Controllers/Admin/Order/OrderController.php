<?php

namespace App\Http\Controllers\Admin\Order;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

use App\Notifications\Shop\Order\CancelOrder;
use App\Notifications\Shop\Order\ConfirmOrder;
use App\Notifications\Shop\Order\CompleteOrder;

use App\Notifications\Customer\Order\Canceled;
use App\Notifications\Customer\Order\Confirmed;
use App\Notifications\Customer\Order\Completed;
use App\User;

use App\Http\Controllers\AccountCalculation as Account;

class OrderController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * All Orders
     */
    public function all()
    {
        $orders = Order::with(['items.product', 'coupon'])->get();
        $all = $orders->count();
        $cancel = $orders->where('status', -1)->count();
        $pending = $orders->where('status', 0)->count();
        $running = $orders->where('status', 1)->count();
        $complete = $orders->where('status', 2)->count();
        // dd($orders);
        return view('admin.order.all', compact(['orders', 'all', 'cancel', 'pending', 'running', 'complete']));
    }

    /**
     * pending Orders
     */
    public function pending()
    {
        $orders = Order::with(['items.product', 'coupon'])->where('status', 0)->get();
        $all = $orders->count();
        $pending = $orders->where('status', 0)->count();
        // dd($orders);
        return view('admin.order.pending', compact(['orders', 'all',  'pending']));
    }

    /**
     * running Orders
     */
    public function running()
    {
        $orders = Order::with(['items.product', 'coupon'])->where('status', 1)->get();
        $all = $orders->count();
        $running = $orders->where('status', 1)->count();
        // dd($orders);
        return view('admin.order.running', compact(['orders', 'all', 'running']));
    }

    /**
     * completed Orders
     */
    public function completed()
    {
        $orders = Order::with(['items.product', 'coupon'])->where('status', 2)->get();
        $all = $orders->count();
        $complete = $orders->where('status', 2)->count();
        // dd($orders);
        return view('admin.order.completed', compact(['orders', 'all', 'complete']));
    }

    /**
     * canceled Orders
     */
    public function canceled()
    {
        $orders = Order::with(['items.product', 'coupon'])->where('status', -1)->get();
        $all = $orders->count();
        $cancel = $orders->where('status', -1)->count();
        // dd($orders);
        return view('admin.order.canceled', compact(['orders', 'all', 'cancel']));
    }

    /**
     * create new Order
     */
    // public function create()
    // {
    //     return view('admin.order.create');
    // }

    /**
     * Order Details
     */
    public function show($order_type, $order_id)
    {
        switch ($order_type) {
            case 'All Orders':
                $route = 'admin.order.all';
                break;
            case 'Pending Orders':
                $route = 'admin.order.pending';
                break;
            case 'Running Orders':
                $route = 'admin.order.running';
                break;
            case 'Completed Orders':
                $route = 'admin.order.completed';
                break;
            default:
                $route = 'admin.order.canceled';
                break;
        }

        $id = decrypt($order_id);

        $order = Order::with([
            'items',
            'coupon'
        ])->where('id', $id)->firstOrFail();
        $shops = $order->items->groupBy('shop_id');

        $riders = User::select('id', 'name')->where('role_id', 4)->where('is_active', 1)->get();

        return view('admin.order.show', compact(['order', 'shops', 'order_type', 'route', 'riders']));
    }

    /**
     * Order Status
     */
    public function status($order_id, $value, Request $request)
    {
        // dd($order_id, $value, $request);
        $order = Order::with(['items'])->where('id', $order_id)->firstOrFail();

        $shopList = array();
        foreach ($order->items as $item) {
            $product = Product::find($item->product_id);
            if(!in_array($product->shop_id, $shopList)) $shopList[] = $product->shop_id;
        }

        $prev = $order->status;
        if ($value == -1) {
            $order->status = -1;
            $order->status_at = now();
            $order->save();
            if ($prev == 1 || $prev == 0) {
                foreach ($order->items as $item) {
                    $product = Product::find($item->product_id);
                    $product->stock += $item->quantity;
                    $product->save();
                }
            }

            /**
             * ! make Notifaition for new order to shops
             */
            foreach((array)$shopList as $shop){
                user_notify($shop, new CancelOrder($order));
            }
            /**
             * ! make Notifaition for new order to customer
             */
             user_notify($order->customer_id, new Canceled($order));

        } elseif ($value == 1) {
            if($request->has('rider_id')){
                $order->rider_id = $request->rider_id;
                $order->rider_earn = $request->rider_charge;
                $order->extra_cost = $request->extra_cost;
            }
            $order->status = 1;
            $order->status_at = now();
            $order->save();

            $account = new Account();
            $account->distribution($order->id);      // Distribute amount

            if ($prev == -1) {
                foreach ($order->items as $item) {
                    $product = Product::find($item->product_id);
                    $product->stock -= $item->quantity;
                    $product->save();
                }
            }
            /**
             * ! make Notifaition for new order to shops
             */
            foreach((array)$shopList as $shop){
                user_notify($shop, new ConfirmOrder($order));
            }
            /**
             * ! make Notifaition for new order to customer
             */
             user_notify($order->customer_id, new Confirmed($order));

        } elseif ($value == 2 && $prev == 1) {
            $order->status = 2;
            $order->status_at = now();
            $order->save();
            /**
             * ! make Notifaition for new order to shops
             */
            foreach((array)$shopList as $shop){
                user_notify($shop, new CompleteOrder($order));
            }
            /**
             * ! make Notifaition for new order to customer
             */
             user_notify($order->customer_id, new Completed($order));
        }

        if ($value == 1) {
            toast('Order Approved.', 'info')->autoClose(2000)->timerProgressBar();
        } elseif ($value == 2) {
            toast('Order Completed.', 'success')->autoClose(2000)->timerProgressBar();
        } elseif ($value == -1) {
            toast('Order Canceled.', 'warning')->autoClose(2000)->timerProgressBar();
        }
        return redirect()->back();
    }

}
