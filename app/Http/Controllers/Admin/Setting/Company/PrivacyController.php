<?php

namespace App\Http\Controllers\Admin\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrivacyController extends Controller
{
    public function __construct(){
        // constructors here
    }
    
    /**
     * privacy
     */
    public function index()
    {
        $policies = (array) company_get('privacy');
        return view('admin.setting.company.privacy.index', compact(['policies']));
    }
    
    /**
     * privacy store
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $privacy = (array) company_get('privacy');
        $ques = array(
            'title' => $request->title,
            'description' => $request->description,
            'created_at' => now(),
            'updated_at' => now(),
        );
        $privacy[] = $ques;

        company_set('privacy', 'json', $privacy);

        toast('Stored new policy successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * privacy update
     */
    public function update(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $privacy = (array) company_get('privacy');

        $ques = $privacy[$request->key];
        $ques['title'] = $request->title;
        $ques['description'] = $request->description;
        $ques['updated_at'] = now();

        $privacy[$request->key] = $ques;

        company_set('privacy', 'json', $privacy);

        toast('Update policy successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * privacy delete
     */
    public function delete($key)
    {
        $privacy = (array) company_get('privacy');
        unset($privacy[$key]);
        company_set('privacy', 'json', $privacy);
        toast('Delete policy successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
