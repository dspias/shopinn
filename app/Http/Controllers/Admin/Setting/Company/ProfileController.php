<?php

namespace App\Http\Controllers\Admin\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct(){
        // constructors here
    }
    
    /**
     * profile
     */
    public function index()
    {
        return view('admin.setting.company.profile.index');
    }
}
