<?php

namespace App\Http\Controllers\Admin\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TermController extends Controller
{
    public function __construct(){
        // constructors here
    }
    
    /**
     * term
     */
    public function index()
    {
        $terms = (array) company_get('term');
        return view('admin.setting.company.term.index', compact(['terms']));
    }
    
    /**
     * term store
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $term = (array) company_get('term');
        $ques = array(
            'title' => $request->title,
            'description' => $request->description,
            'created_at' => now(),
            'updated_at' => now(),
        );
        $term[] = $ques;

        company_set('term', 'json', $term);

        toast('Stored new term successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * term update
     */
    public function update(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $term = (array) company_get('term');

        $ques = $term[$request->key];
        $ques['title'] = $request->title;
        $ques['description'] = $request->description;
        $ques['updated_at'] = now();

        $term[$request->key] = $ques;

        company_set('term', 'json', $term);

        toast('Update term successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * term delete
     */
    public function delete($key)
    {
        $term = (array) company_get('term');
        unset($term[$key]);
        company_set('term', 'json', $term);
        toast('Delete term successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
