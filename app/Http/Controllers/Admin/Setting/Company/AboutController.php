<?php

namespace App\Http\Controllers\Admin\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;

class AboutController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * About
     */
    public function index()
    {
        return view('admin.setting.company.about.index');
    }

    /**
     * About
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required | string | email | max:191',
            'mobile' => 'required | numeric ',
            'address' => 'required | string | max:191',
            // 'facebook_page' => 'required | string',
            // 'facebook_group' => 'required | string',
            // 'instagram' => 'required | string',
            // 'twitter' => 'required | string',
            'short_note' => 'required | string',
            'about' => 'required | string',
        ));

        company_set('email', 'string', $request->email);

        company_set('mobile', 'string', $request->mobile);

        company_set('address', 'text', $request->address);

        // if(!is_null($request->facebook_page))
        company_set('facebook_page', 'string', $request->facebook_page);

        // if(!is_null($request->facebook_group))
        company_set('facebook_group', 'string', $request->facebook_group);

        // if(!is_null($request->instagram))
        company_set('instagram', 'string', $request->instagram);

        // if(!is_null($request->twitter))
        company_set('twitter', 'string', $request->twitter);

        company_set('short_note', 'text', $request->short_note);

        company_set('about', 'text', $request->about);

        // if($request->hasfile('logo')){
        //     $this->validate($request, [
        //         'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        //     ]);
        //     company_set('logo', 'avatar', $request->file('logo'));
        // }

        toast('Updated Company Information successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
