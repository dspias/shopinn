<?php

namespace App\Http\Controllers\Admin\Setting\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function __construct(){
        // constructors here
    }
    
    /**
     * faq
     */
    public function index()
    {
        $faqs = (array) company_get('faq');
        return view('admin.setting.company.faq.index', compact(['faqs']));
    }
    
    /**
     * faq store
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $faq = (array) company_get('faq');
        $ques = array(
            'title' => $request->title,
            'description' => $request->description,
            'created_at' => now(),
            'updated_at' => now(),
        );
        $faq[] = $ques;

        company_set('faq', 'json', $faq);

        toast('Stored new faq successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * faq update
     */
    public function update(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required | string | max: 191',
            'description' => 'required | string',
        ));
        
        $faq = (array) company_get('faq');

        $ques = $faq[$request->key];
        $ques['title'] = $request->title;
        $ques['description'] = $request->description;
        $ques['updated_at'] = now();

        $faq[$request->key] = $ques;

        company_set('faq', 'json', $faq);

        toast('Update faq successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
    
    /**
     * faq delete
     */
    public function delete($key)
    {
        $faq = (array) company_get('faq');
        unset($faq[$key]);
        company_set('faq', 'json', $faq);
        toast('Delete faq successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
