<?php

namespace App\Http\Controllers\Admin\Setting\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProductType;

class TypeController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }
    
    /**
     * type
     */
    public function index()
    {
        $types = ProductType::with(['categories' => function($q){
            $q->select('id', 'type_id', 'name', 'is_active')->where('is_active', 1);
        }])->get();
        return view('admin.setting.product.type.index', compact(['types']));
    }
    
    /**
     * type store
     */
    public function store(Request $request)
    {
        //validation here
        $this->validate($request, array(
            'name'          =>  'required | string | max:255',
        ));

        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }

        $type = new ProductType();

        $type->name = $request->name;
        $type->note = $request->note;
        
        if(!isset($request->is_active)) $type->is_active = 0;

        if($type->save()){

            toast('Product Type created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.setting.product.type.index');
        } else{
            toast('Product Type does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * type update
     */
    public function update(Request $request)
    {
        $type = ProductType::findOrFail($request->id);
        //validation here
        $this->validate($request, array(
            'name'          =>  'required | string | max:255',
        ));

        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }


        $type->name = $request->name;
        $type->note = $request->note;
        
        $type->is_active = (isset($request->is_active)) ? 1:0;

        if($type->save()){

            toast('Product Type updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.setting.product.type.index');
        } else{
            toast('Product Type does not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * type change_status
     */
    public function change_status($type_id)
    {
        $id = decrypt($type_id);
        $type = ProductType::findOrFail($id);

        $type->is_active = ($type->is_active == 1) ? 0:1;

        if($type->save()){
            toast('Product Type status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Product Type status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


}
