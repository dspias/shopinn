<?php

namespace App\Http\Controllers\Admin\Setting\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProductType;
use App\Models\ProductCategory;

class CategoryController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }
    
    /**
     * category
     */
    public function index()
    {
        $types = ProductType::where('is_active', 1)->get();
        $categories = ProductCategory::with(['type' => function($q){
            $q->select('id', 'name', 'is_active');
        }, 'products' => function($q){
            $q->select('id', 'category_id', 'is_active')->where('is_active', 1);
        }])->get();
        return view('admin.setting.product.category.index', compact(['types', 'categories']));
    }
    
    /**
     * type store
     */
    public function store(Request $request)
    {
        //validation here
        $this->validate($request, array(
            'name'          =>  'required | string | max:255',
            'type_id'          =>  'required',
        ));

        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }

        $cat = new ProductCategory();

        $cat->name = $request->name;
        $cat->type_id = $request->type_id;
        $cat->note = $request->note;
        
        if(!isset($request->is_active)) $cat->is_active = 0;

        if($cat->save()){

            toast('Product Category created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.setting.product.category.index');
        } else{
            toast('Product Category does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * type update
     */
    public function update(Request $request)
    {
        $cat = ProductCategory::findOrFail($request->id);
        //validation here
        $this->validate($request, array(
            'name'          =>  'required | string | max:255',
            'type_id'          =>  'required',
        ));

        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }

        $cat->name = $request->name;
        $cat->type_id = $request->type_id;
        $cat->note = $request->note;
        
        $cat->is_active = (isset($request->is_active)) ? 1:0;

        if($cat->save()){

            toast('Product Category updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.setting.product.category.index');
        } else{
            toast('Product Category does not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * type change_status
     */
    public function change_status($cat_id)
    {
        $id = decrypt($cat_id);
        $type = ProductCategory::findOrFail($id);

        $type->is_active = ($type->is_active == 1) ? 0:1;

        if($type->save()){
            toast('Product Type status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Product Type status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
