<?php

namespace App\Http\Controllers\Admin\Setting\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\User;
use Mail;
use App\Mail\LoginInfo;


class AdministrationController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All Admins
     */
    public function all()
    {
        // calcualte
        $all= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->count();
        $active= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->whereNotNull('approved_at')->where('is_active', 0)->count();

        $admins = User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->get();
        return view('admin.setting.administration.all', compact(['admins', 'all', 'active', 'inactive']));
    }

    /**
     * active Admins
     */
    public function active()
    {

        // calcualte
        $all= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->count();
        $active= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->whereNotNull('approved_at')->where('is_active', 1)->count();

        $admins = User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->where('is_active', 1)->get();
        return view('admin.setting.administration.active', compact(['admins', 'all', 'active']));
    }

    /**
     * inactive Admins
     */
    public function inactive()
    {

        // calcualte
        $all= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->count();
        $inactive= User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->whereNotNull('approved_at')->where('is_active', 0)->count();

        $admins = User::where('role_id', 1)->where('email', '!=', 'dev.pinikk@gmail.com')->where('is_active', 0)->get();
        return view('admin.setting.administration.inactive', compact(['admins', 'all', 'inactive']));
    }

    /**
     * create New Admin
     */
    public function create()
    {
        return view('admin.setting.administration.create');
    }


    /**
     * store admin
     */
    public function store(Request $request)
    {
        // dd($request);
        //validation here
        $this->validate($request, array(
            'email'         =>  'required | string | email | max:255 | unique:users',
            'mobile'        =>  'required | regex:/^([0-9\s\-\+\(\)]*)$/ | min:11 | unique:users',
            'password'      =>  'required | string  | min:8 | max:255',
            'name'          =>  'required | string | max:255',
            'address'   =>  'required | string',
        ));

        if($request->note != null){
            $this->validate($request, array(
                'note'   =>  'required | string',
            ));
        }


        // store in user table
        $user = User::create(array(
            'role_id'   =>  1,
            'name'      =>  $request->name,
            'password'  =>  Hash::make($request->password)
        ));
        $user->approved_at = now();
        if($request->hasfile('avatar')){
            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));
            $user->avatar = $request->file('avatar')->store('admin', 'public');
        }
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->note = $request->note;
        if(!isset($request->is_active)) $user->is_active = 0;

        if($user->save()){

            if(isset($request->is_email_info)){
                Mail::to($request->email)->send(new LoginInfo($request));
            }

            toast('Admin created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.setting.administration.all');
        } else{
            toast('Admin does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }



    /**
     * details admins
     */
    public function details($page, $admin_id)
    {
        $id = decrypt($admin_id);
        $admin = User::where('id', $id)->where('role_id', 1)->firstOrFail();

        return view('admin.setting.administration.details', compact(['admin', 'page']));
    }

    /**
     * change_status admins
     */
    public function change_status($admin_id)
    {
        $id = decrypt($admin_id);
        $admin = User::findOrFail($id);

        $admin->is_active = ($admin->is_active == 1) ? 0:1;

        if($admin->save()){
            toast('admin status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('admin status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
