<?php

namespace App\Http\Controllers\Admin\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Profile Index
     */
    public function index()
    {
        return view('admin.profile.profile');
    }

    //Change Password
    public function change_password(Request $request){
        // dd($request);
        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            toast('Your Old Password Does not Match.', 'error')->autoClose(5000)->timerProgressBar();
            return redirect()->back();
        }
        elseif($request->old_password == $request->new_password){
            toast('Old Password and New Password must be Different.', 'error')->autoClose(5000)->timerProgressBar();
            return redirect()->back();
        }
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);

            $super->save();
            toast('Your Password has been Updated Succesfully.', 'success')->autoClose(5000)->timerProgressBar();

            return redirect()->back();
        }
    }

    /**
     * Profile update
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, array(
            'name'  =>  'required | string | max:255',
            'mobile'  =>  'required | string'
        ));

        if($user->email != $request->email){
            $this->validate($request, array(
                'email'  =>  'required | string | email | max:255 | unique:users'
            ));
        }


        if($request->hasfile('avatar')){
            $this->validate($request, [
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
            ]);
            $path = storage_path('app/public/'.$user->avatar);
            if(file_exists($path)){
                @unlink($path);
            }
            $user->avatar = $request->file('avatar')->store('AdminAvatar', 'public');
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;

        if($user->save()){
            toast('Profile Updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Profile does not Updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


}
