<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;


use App\Notifications\Shop\Product\Status;

class ProductController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }
    
    /**
     * All products
     */
    public function all()
    {
        $all= Product::count();
        $pending= Product::whereNull('approved_at')->count();
        $active= Product::whereNotNull('approved_at')->where('is_active', 1)->count();
        $inactive= Product::whereNotNull('approved_at')->where('is_active', 0)->count();
        $products = Product::with('shop.shop')->get();
        // dd($products);
        return view('admin.product.all', compact(['products', 'all', 'active', 'inactive', 'pending']));
    }
    

    
    /**
     * pending products
     */
    public function pending()
    {
        $all= Product::count();
        $pending= Product::whereNull('approved_at')->count();
        $products = Product::with('shop.shop')->whereNull('approved_at')->get();
        // dd($products);
        return view('admin.product.pending', compact(['products', 'all', 'pending']));
    }
    


    /**
     * active products
     */
    public function active()
    {
        $all= Product::count();
        $active= Product::whereNotNull('approved_at')->where('is_active', 1)->count();
        $products = Product::with('shop.shop')->whereNotNull('approved_at')->where('is_active', 1)->get();
        // dd($products);
        return view('admin.product.active', compact(['products', 'all', 'active']));
    }
    


    /**
     * inactive products
     */
    public function inactive()
    {
        $all= Product::count();
        $inactive= Product::whereNotNull('approved_at')->where('is_active', 0)->count();
        $products = Product::with('shop.shop')->whereNotNull('approved_at')->where('is_active', 0)->get();
        // dd($products);
        return view('admin.product.inactive', compact(['products', 'all','inactive']));
    }
    


    /**
     * create products
     */
    public function create()
    {
        return view('admin.product.create');
    }
    
    
    /**
     * details products
     */
    public function details($page, $product_id)
    {
        $id = decrypt($product_id);
        $product = Product::with(['shop.shop', 'colors', 'tags', 'category.type', 'sizes'])->where('id', $id)->firstOrFail();

        switch ($page) {
            case 'pending':
                $page = 'Pending Products';
                $route = 'admin.product.pending';
                break;
            case 'active':
                $page = 'Active Products';
                $route = 'admin.product.active';
                break;
            case 'inactive':
                $page = 'Inactive Products';
                $route = 'admin.product.inactive';
                break;
            default:
                $page = 'All Products';
                $route = 'admin.product.all';
                break;
        }

        return view('admin.product.details', compact(['product', 'page', 'route']));
    }

    
    
    /**
     * approve prodcuut
     */
    public function approve($product_id)
    {
        $id = decrypt($product_id);
        $product = Product::findOrFail($id);

        $product->approved_at = ($product->approved_at == null) ? now():$product->approved_at;

        user_notify($product->shop_id, new Status($product));

        if($product->save()){
            toast('Product Approved.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Product does not approved. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * change_status prodcuts
     */
    public function change_status($prodcut_id)
    {
        $id = decrypt($prodcut_id);
        $product = Product::findOrFail($id);

        $product->is_active = ($product->is_active == 1) ? 0:1;

        user_notify($product->shop_id, new Status($product));

        if($product->save()){
            toast('Prodcut status changed.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Prodcut status does not change. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
    
    /**
     * UpdatePrice prodcuts
     */
    public function updatePrice(Request $request, $prodcut_id)
    {
        $id = decrypt($prodcut_id);
        $prodcut = Product::findOrFail($id);

        $prodcut->updated_price = $request->updated_price;

        if($prodcut->save()){
            toast('Prodcut updated new price.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else{
            toast('Prodcut new price does not Updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

}