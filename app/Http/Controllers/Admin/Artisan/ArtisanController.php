<?php

namespace App\Http\Controllers\Admin\Artisan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ArtisanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'dev']);
    }

    /**
     * Artisan Index
     */
    public function index()
    {
        return view('admin.artisan.index');
    }

    /**
     * Artisan command
     */
    public function command($command)
    {
        Artisan::call($command);

        $alert = $command. ' Command Executed';
        toast($alert, 'success')->autoClose(2000)->timerProgressBar();

        return redirect()->route('admin.artisan.index');
    }
}
