<?php

namespace App\Http\Controllers\Admin\Advertise;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdvertiseType as Type;
use App\Models\AdvertiseType;
use App\Models\AdminAdvertise;
use App\Models\AdvertiseLog;
use App\Models\AdvertiseLogDetails;

class AdvertiseController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * All Admin Advertises
     */
    public function index()
    {
        $advertises = AdminAdvertise::with('type')->get();
        return view('admin.advertise.index', compact(['advertises']));
    }


    /**
     * store Advertises
     */
    public function store(Request $request)
    {
        //validation here
        $this->validate($request, array(
            'type_id'       =>  'required | integer',
            'title'         =>  'required | string | max:191',
            'url_link'      =>  'required | url',
            'day'           =>  'required | integer',
        ));

        $type = AdvertiseType::findOrFail($request->type_id);

        $rule = 'required | image | mimes:jpeg,png,jpg,gif,svg | max:' . $type->max_size . ' | min:' . $type->min_size . ' | dimensions:min_width=' . $type->width . ',min_height=' . $type->height . ', max_width=' . $type->width . ',max_height=' . $type->height;
        $this->validate($request, array(
            'ad_image'      =>  $rule,
        ));

        $ad = new AdminAdvertise();
        $ad->title = $request->title;
        $ad->type_id = $request->type_id;
        $ad->url_link = $request->url_link;
        $ad->day = $request->day;
        $ad->start = date('Y-m-d');
        $ad->end = date('Y-m-d', strtotime($ad->start . ' + ' . $request->day . ' days'));
        $ad->ad_image = $request->file('ad_image')->store('AdminAd', 'public');

        if ($request->is_active == 'on') {
            $ad->is_active = true;
        } else {
            $ad->is_active = false;
        }


        if ($ad->save()) {
            toast('Added new advertise.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Does not added new advertise. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * Show Admin Advertises
     */
    public function show($ad_id)
    {
        $id = decrypt($ad_id);
        $advertise = AdminAdvertise::with('type')->where('id', $id)->firstOrFail();
        // dd($advertise);
        return view('admin.advertise.show', compact(['advertise']));
    }

    /**
     * edit Admin Advertises
     */
    public function edit($ad_id)
    {
        $id = decrypt($ad_id);
        $advertise = AdminAdvertise::with('type')->where('id', $id)->firstOrFail();
        $types = Type::all();
        // dd($types);
        return view('admin.advertise.edit', compact(['advertise', 'types']));
    }

    /**
     * update Admin Advertises
     */
    public function update(Request $request, $ad_id)
    {
        $id = decrypt($ad_id);
        // dd($id, $request);
        //validation here
        $this->validate($request, array(
            'type_id'       =>  'required | integer',
            'title'         =>  'required | string | max:191',
            'url_link'      =>  'required | url',
            'day'           =>  'required | integer',
        ));

        $type = AdvertiseType::findOrFail($request->type_id);

        $rule = 'required | image | mimes:jpeg,png,jpg,gif,svg | max:' . $type->max_size . ' | min:' . $type->min_size . ' | dimensions:min_width=' . $type->width . ',min_height=' . $type->height . ', max_width=' . $type->width . ',max_height=' . $type->height;
        if ($request->hasFile('ad_image')) {
            $this->validate($request, array(
                'ad_image'      =>  $rule,
            ));
        }

        $ad = AdminAdvertise::findOrFail($id);
        $ad->title = $request->title;
        $ad->type_id = $request->type_id;
        $ad->url_link = $request->url_link;
        $ad->day = $request->day;
        $ad->start = date('Y-m-d');
        $ad->end = date('Y-m-d', strtotime($ad->start . ' + ' . $request->day . ' days'));

        if ($request->hasfile('ad_image')) {
            $path = storage_path('app/public/' . $ad->ad_image);
            if (file_exists($path)) {
                @unlink($path);
            }
            $ad->ad_image = $request->file('ad_image')->store('AdminAd', 'public');
        }

        if ($request->is_active == 'on') {
            $ad->is_active = true;
        } else {
            $ad->is_active = false;
        }


        if ($ad->save()) {
            toast('Advertise Updated Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Advertise Does Not Updated Successfully. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * shopAdPack
     */
    public function shopAdPack()
    {
        $checks = AdvertiseLog::where('is_active', 1)->where('end', '<', date('Y-m-d'))->get();
        foreach($checks as $check) { $check->is_active = 0; $check->save(); }
        
        $logs = AdvertiseLog::all();
        return view('admin.advertise.shop_package', compact(['logs']));
    }

    /**
     * shopAdPackShow
     */
    public function shopAdPackShow($pack_id)
    {
        $pack_id = decrypt($pack_id);
        $log = AdvertiseLog::findOrFail($pack_id);
        return view('admin.advertise.shop_package_show', compact(['log']));
    }

    /**
     * All Advertises
     */
    public function all()
    {
        $ads = AdvertiseLogDetails::all();
        return view('admin.advertise.all', compact(['ads']));
    }

    /**
     * active Advertises
     */
    public function active()
    {
        $ads = AdvertiseLogDetails::where('is_active', 1)->where('end', '>=', date('Y-m-d'))->get();
        return view('admin.advertise.active', compact(['ads']));
    }

    /**
     * inactive Advertises
     */
    public function inactive()
    {
        $ads = AdvertiseLogDetails::where('is_active', 0)->orWhere('end', '<', date('Y-m-d'))->get();
        return view('admin.advertise.inactive', compact(['ads']));
    }

    /**
     * create Advertises
     */
    public function create()
    {
        $types = Type::all();
        return view('admin.advertise.create', compact(['types']));
    }

    /**
     * Show shop Advertises
     */
    public function view($ad_id)
    {
        $id = decrypt($ad_id);
        $advertise = AdvertiseLogDetails::with('type')->where('id', $id)->firstOrFail();
        // dd($advertise);
        return view('admin.advertise.view', compact(['advertise']));
    }

    /**
     * Show shop Advertises
     */
    public function change_status($ad_id)
    {
        $id = decrypt($ad_id);
        $advertise = AdvertiseLogDetails::with('type')->where('id', $id)->firstOrFail();
        $advertise->is_active = ($advertise->is_active == 0) ? 1:0;

        if ($advertise->save()) {
            toast('Advertise Status Updated Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Advertise Status Does Not Updated Successfully. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * change status for package selected log
     */
    public function change_log_status($log_id)
    {
        $id = decrypt($log_id);
        $log = AdvertiseLog::with('ad_details')->where('id', $id)->firstOrFail();
        $log->is_active = ($log->is_active == 0) ? 1:0;

        if ($log->save()) {
            foreach($log->ad_details as $ad){
                $ad->is_active = ($log->is_active == 1) ? 1:0;
                $ad->save();
            }
            toast('Advertise Status Updated Successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Advertise Status Does Not Updated Successfully. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
