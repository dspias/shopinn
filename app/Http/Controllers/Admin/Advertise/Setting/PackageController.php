<?php

namespace App\Http\Controllers\Admin\Advertise\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdvertiseType as Type;
use App\Models\AdvertisePackage as Package;

class PackageController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * Advertise Packages
     */
    public function all()
    {
        $packages = Package::with(['types', 'logs' => function ($query) {
            $query->where('is_active', 1)->where('end', '>=', date('Y-m-d'));
        }])->get();
        return view('admin.advertise.setting.package.all', compact(['packages']));
    }

    /**
     * Create Advertise Packages
     */
    public function create()
    {
        $types = Type::select('id', 'name', 'width', 'height')->get();
        return view('admin.advertise.setting.package.create', compact(['types']));
    }

    /**
     * store Advertise Packages
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, array(
            'name'         =>  'required | string | max:255',
            'price'         =>  'required | integer | min:0',
            'details'         =>  'required | string',
            'type'         =>  'array',
            'type.*'         =>  'required',
            'max_ad'         =>  'array',
            'max_ad.*'         =>  'required',
        ));



        if ($request->is_active == 'on') {
            $is_active = 1;
        } else {
            $is_active = 0;
        }

        $package = Package::create(array(
            'name' => $request->name,
            'price' => $request->price,
            'details' => $request->details,
            'is_active' => $is_active,
        ));
        foreach ((array)$request->type as $sl => $type) {
            // dd($type, $request->max_ad[$sl]);
            $package->types()->attach($type, ['max_ad' => $request->max_ad[$sl], 'created_at' => now(), 'updated_at' => now()]);
            // $package->types()->sync($request->input('type_id', []));
        }

        if (!is_null($package)) {
            toast('Package created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.advertise.setting.package.all');
        } else {
            toast('Package does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * show Advertise Packages
     */
    public function show($package_id)
    {
        $id = decrypt($package_id);
        $package = Package::with(['types', 'logs' => function ($query) {
            $query->where('is_active', 1)->where('end', '>=', date('Y-m-d'));
        }])->where('id', $id)->firstOrFail();
        // dd($package);
        return view('admin.advertise.setting.package.show', compact(['package']));
    }

    /**
     * edit Advertise Packages
     */
    public function edit($package_id)
    {
        $id = decrypt($package_id);
        $types = Type::select('id', 'name')->get();
        $package = Package::with(['types', 'logs' => function ($query) {
            $query->where('is_active', 1)->where('end', '>=', date('Y-m-d'));
        }])->where('id', $id)->firstOrFail();
        // dd($package);
        return view('admin.advertise.setting.package.edit', compact(['package', 'types']));
    }

    /**
     * update Advertise Packages
     */
    public function update(Request $request, $package_id)
    {
        $pack_id = decrypt($package_id);

        $this->validate($request, array(
            'name'         =>  'required | string | max:255',
            'price'         =>  'required | integer | min:0',
            'details'         =>  'required | string',
            'type'         =>  'array',
            'type.*'         =>  'required',
            'max_ad'         =>  'array',
            'max_ad.*'         =>  'required',
        ));



        if ($request->is_active == 'on') {
            $is_active = 1;
        } else {
            $is_active = 0;
        }

        $package = Package::findOrFail($pack_id);

        $package->name = $request->name;
        $package->price = $request->price;
        $package->details = $request->details;
        $package->is_active = $is_active;

        $package->save();
        $package->types()->detach();
        foreach ((array)$request->type as $sl => $type) {
            // dd($type, $request->max_ad[$sl]);
            $package->types()->attach($type, ['max_ad' => $request->max_ad[$sl], 'created_at' => now(), 'updated_at' => now()]);
            // $package->types()->sync($request->input('type_id', []));
        }

        if (!is_null($package)) {
            toast('Package created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.advertise.setting.package.all');
        } else {
            toast('Package does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
