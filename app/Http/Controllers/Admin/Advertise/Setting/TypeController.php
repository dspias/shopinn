<?php

namespace App\Http\Controllers\Admin\Advertise\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdvertiseType as Type;

class TypeController extends Controller
{
    public function __construct()
    {
        // constructors here
    }

    /**
     * Advertise types
     */
    public function all()
    {
        $types = Type::all();
        return view('admin.advertise.setting.type.all', compact(['types']));
    }

    /**
     * Store Advertise types
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'              =>  'required | string | max:255',
            // 'width'             =>  'required | integer | min:0',
            // 'height'            =>  'required | integer | min:0',
            'min_size'          =>  'required | integer | min:0',
            'max_size'          =>  'required | integer | min:0',
        ));
        // dd($request);

        $pieces = explode("-", $request->width_height);
        $width = (int)$pieces[0];
        $height = (int)$pieces[1];

        // dd($width, $height);

        $type = new Type();

        $type->name     = $request->name;
        // $type->width    = $request->width;
        // $type->height    = $request->height;
        $type->width    = $width;
        $type->height   = $height;
        $type->min_size = $request->min_size;
        $type->max_size = $request->max_size;

        if ($type->save()) {
            toast('New Type created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('New Type does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }


    /**
     * update Advertise types
     */
    public function update(Request $request)
    {
        $type = Type::findOrFail($request->id);

        $this->validate($request, array(
            'name'              =>  'required | string | max:255',
            // 'width'             =>  'required | integer | min:0',
            // 'height'            =>  'required | integer | min:0',
            'min_size'          =>  'required | integer | min:0',
            'max_size'          =>  'required | integer | min:0',
        ));
        // dd($request, $type);

        $pieces = explode("-", $request->width_height);
        dd($pieces);
        $width = (int)$pieces[0];
        $height = (int)$pieces[1];

        $type->name     = $request->name;
        // $type->width    = $request->width;
        // $type->height    = $request->height;
        $type->width    = $width;
        $type->height   = $height;
        $type->min_size = $request->min_size;
        $type->max_size = $request->max_size;

        if ($type->save()) {
            toast('Type Updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Type does not Updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }
}
