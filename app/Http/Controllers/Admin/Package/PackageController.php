<?php

namespace App\Http\Controllers\Admin\Package;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\BusinessPackage as Package;

class PackageController extends Controller
{
    public function __construct(){
        // constructors here
        $this->middleware(['auth', 'admin']);
    }

    /**
     * All Packages
     */
    public function all()
    {
        // calcualte
        $all= Package::count();
        $active= Package::where('is_active', 1)->count();
        $inactive= Package::where('is_active', 0)->count();

        $packages = Package::with(['logs' => function($q){
            $q->select('id', 'package_id', 'is_active')->where('is_active', 1);
        }])->get();
        return view('admin.package.all', compact(['all', 'active', 'inactive', 'packages']));
    }

    /**
     * active Packages
     */
    public function active()
    {
        // calcualte
        $all= Package::count();
        $active= Package::where('is_active', 1)->count();
        $inactive= Package::where('is_active', 0)->count();

        $packages = Package::with(['logs' => function($q){
            $q->select('id', 'package_id', 'is_active')->where('is_active', 1);
        }])->where('is_active', 1)->get();
        return view('admin.package.active', compact(['all', 'active', 'packages']));
    }

    /**
     * inactive Packages
     */
    public function inactive()
    {
        // calcualte
        $all= Package::count();
        $active= Package::where('is_active', 1)->count();
        $inactive= Package::where('is_active', 0)->count();

        $packages = Package::with(['logs' => function($q){
            $q->select('id', 'package_id', 'is_active')->where('is_active', 1);
        }])->where('is_active', 0)->get();
        return view('admin.package.inactive', compact(['all', 'inactive', 'packages']));
    }

    /**
     * create Packages
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * store Packages
     */
    public function store(Request $request)
    {
        //validation here
        $this->validate($request, array(
            'name'              =>  'required | string | max:255',
            'package_type'      =>  'required',
            'description'      =>  'required | string',
        ));

        if($request->package_type == 2){
            $this->validate($request, array(
                'total_percentage'  =>  'required',
            ));
        }

        if($request->package_type == 3){
            $this->validate($request, array(
                'total_percentage'  =>  'required',
                'total_payment'  =>  'required',
                'year'  =>  'required',
                'month'  =>  'required',
                'day'  =>  'required',
            ));
        }

        // store packge data
        $data = new Package();
        $data->name = $request->name;
        $data->package_type = $request->package_type;
        $data->total_percentage = $request->total_percentage;
        $data->total_payment = $request->total_payment;
        $data->first_percentage = $request->first_percentage;
        $data->first_day = $request->first_day;
        $data->year = $request->year;
        $data->month = $request->month;
        $data->day = $request->day;
        $data->description = $request->description;
        if(!isset($request->is_active)) $data->is_active = 0;

        if($data->save()){
            toast('Business package created successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.package.all');
        } else{
            toast('Business package does not Created. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * update Packages
     */
    public function update(Request $request, $package_id)
    {
        $id = decrypt($package_id);
        $data = Package::findOrFail($id);
        //validation here
        $this->validate($request, array(
            'name'              =>  'required | string | max:255',
            'package_type'      =>  'required',
            'description'      =>  'required | string',
        ));

        if($request->package_type == 2){
            $this->validate($request, array(
                'total_percentage'  =>  'required',
            ));
        }

        if($request->package_type == 3){
            $this->validate($request, array(
                'total_percentage'  =>  'required',
                'total_payment'  =>  'required',
                'year'  =>  'required',
                'month'  =>  'required',
                'day'  =>  'required',
            ));
        }

        // store packge data
        $data->name = $request->name;
        $data->package_type = $request->package_type;
        $data->total_percentage = $request->total_percentage;
        $data->total_payment = $request->total_payment;
        $data->first_percentage = $request->first_percentage;
        $data->first_day = $request->first_day;
        $data->year = $request->year;
        $data->month = $request->month;
        $data->day = $request->day;
        $data->description = $request->description;
        if(!isset($request->is_active)) $data->is_active = 0;

        if($data->save()){
            toast('Business package updated successfully.', 'success')->autoClose(2000)->timerProgressBar();
            return redirect()->route('admin.package.all');
        } else{
            toast('Business package does not updated. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }
    }



    /**
     * edit Packages
     */
    public function edit($package_id)
    {
        $id = decrypt($package_id);
        $package = Package::findOrFail($id);
        return view('admin.package.edit', compact(['package']));
    }


    /**
     * show Packages
     */
    public function show($package_id)
    {
        $id = decrypt($package_id);


        $data = Package::with(['logs.shop.shop'])->where('id', $id)->firstOrFail();
        return view('admin.package.show', compact('data'));
    }
}
