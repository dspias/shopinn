<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductRating;
use App\Models\ProductReview;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Order Index
     */
    public function index()
    {
        $orders = Order::with(['items.product', 'coupon'])->where('customer_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        $all = $orders->count();
        $cancel = $orders->where('status', -1)->count();
        $pending = $orders->where('status', 0)->count();
        $running = $orders->where('status', 1)->count();
        $complete = $orders->where('status', 2)->count();
        // dd($orders);
        return view('customer.order.index', compact(['orders', 'all', 'cancel', 'pending', 'running', 'complete']));
    }


    /**
     * Order Details
     */
    public function show($order_type, $order_id)
    {
        $id = decrypt($order_id);

        $order = Order::with([
            'items',
            'coupon'
        ])->where('id', $id)->first();
        $shops = $order->items->groupBy('shop_id');
        // dd($shops);
        return view('customer.order.show', compact(['order', 'shops', 'order_type']));
    }


    /**
     * Review Rating
     */
    public function reviewRating(Request $request, $order_id)
    {
        // dd($request, $order_id);

        $this->validate($request, array(
            'review_rating' => 'array',
            'review_rating.*' => 'array',
            'review_rating.*.*' => 'required',
        ));

        $order = Order::find($order_id);

        foreach($request->review_rating as $key => $item){
            $temp = OrderItem::find($key);
            // dd($temp, $item, $key);

            if($temp != null){
                $rating = new ProductRating();
                $rating->customer_id = auth()->user()->id;
                $rating->product_id = $temp->product_id;
                $rating->rate = $item['rating'];
                $rating->save();

                $review = new ProductReview();
                $review->customer_id = auth()->user()->id;
                $review->product_id = $temp->product_id;
                $review->review = $item['review'];
                $review->save();
            }
        }

        $order->has_reviewed = 1;
        $order->save();

        toast('Review Done.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
