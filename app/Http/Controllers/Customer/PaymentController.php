<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Payment Index
     */
    public function index()
    {
        // return view('customer.payment.index');
        return redirect()->route('guest.homepage.index');
    }
}
