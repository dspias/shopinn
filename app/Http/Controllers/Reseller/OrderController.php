<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    /**
     * All Order
     */
    public function allOrder()
    {
        $orders = Order::with(['items.product', 'coupon'])
                        ->where('customer_id', auth()->user()->id)
                        ->where('is_reseller', 1)
                        ->orderBy('id', 'desc')
                        ->get();

        return view('reseller.order.all_order', compact(['orders']));
    }

    /**
     * Order Details
     */
    public function show($order_id)
    {
        $id = decrypt($order_id);

        $order = Order::with([
            'items',
            'coupon'
        ])->where('id', $id)->first();
        $shops = $order->items->groupBy('shop_id');

        return view('reseller.order.show', compact(['order', 'shops']));
    }
}
