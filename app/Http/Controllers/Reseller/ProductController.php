<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ResellerProduct;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Support\MediaStream;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Add Product
     */
    public function addProduct($product_id, $reseller_id)
    {
        // dd($product_id, $reseller_id);
        $data = ResellerProduct::where('reseller_id', $reseller_id)
                            ->where('product_id', $product_id)
                            ->onlyTrashed()
                            ->first();
        if(is_null($data)){
            ResellerProduct::create([
                'reseller_id'   =>  $reseller_id,
                'product_id'   =>  $product_id
            ]);
        } else {
            $data->restore();
        }
        
        toast('Product Added to your Favourite.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }

    /**
     * Remove Product
     */
    public function removeProduct($product_id, $reseller_id)
    {
        // dd($product_id, $reseller_id);
        $data = ResellerProduct::where('reseller_id', $reseller_id)->where('product_id', $product_id)->firstOrFail();
        if($data->delete()){
            toast('Product removed from your Favourite.', 'warning')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            toast('Product did not remove from your Favourite. There is some error. Please try again.', 'error')->autoClose(5000)->timerProgressBar();
            return redirect()->back();
        }
    }

    /**
     * My Products
     */
    public function myProduct()
    {
        $reseller = auth()->user();
        $products = ResellerProduct::with(['reseller', 'product'])->where('reseller_id', $reseller->id)->get();
        // dd($products);
        return view('reseller.product.my_product', compact(['products', 'reseller']));
    }



    /**
     * downloadPhotos
     */
    public function downloadPhotos($product_id)
    {
        $product = Product::findOrFail($product_id);
        // Let's get some media.
        $downloads = $product->getMedia('product');

        // Download the files associated with the media in a streamed way.
        // No prob if your files are very large.
        $name = $product->pid.'('.$product->name.').zip';
        return MediaStream::create($name)->addMedia($downloads);
    }
}
