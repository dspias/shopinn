<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ResellerProduct;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct(){
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeProduct = ResellerProduct::where('reseller_id', auth()->user()->id)->count();
        $order = Order::where('status', '!=', -1)->where('is_reseller', 1)->where('customer_id', auth()->user()->id)->count();
        $todayOrder = Order::where('status', '!=', -1)->where('is_reseller', 1)->where('customer_id', auth()->user()->id)->whereDate('created_at', date('Y-m-d'))->count();
        $pendingOrder = Order::where('status', '!=', -1)->where('is_reseller', 1)->where('customer_id', auth()->user()->id)->whereDate('created_at', date('Y-m-d'))->count();

        $total_sale = $this->totalSale();
        $total_commission = $this->totalCommision();
        $payable = $this->calculatePayable();
        $paid = $this->calculatePaid();

        return view('reseller.dashboard.index',
                compact([
                    'activeProduct',
                    'order',
                    'pendingOrder',
                    'todayOrder',
                    'total_sale',
                    'total_commission',
                    'payable',
                    'paid',
                ]));
    }


    private function totalSale()
    {
        $total_shop_earn = 0.00;
        $total_shop_earn = OrderItem::with('order')->where('status', '!=', -1)->whereHas('order', function($query){
            $query->where('status', 2)->where('is_reseller', 1)->where('customer_id', auth()->user()->id);
        })->select(DB::raw('sum(price*quantity) as total'))->get()->sum('total');

        $coupons = Order::where('status', 2)->whereHas('items', function($query){
            $query->where('status', '!=', -1);
        })->where('is_reseller', 1)->where('customer_id', auth()->user()->id)->get();

        $vouchers = 0;
        foreach($coupons as $coupon) $vouchers += optional($coupon->coupon)->discount ?? 0.00;

        return ($total_shop_earn-$vouchers);
    }


    private function totalCommision()
    {
        $total_reseller_earn = 0.00;
        $total_reseller_earn = Order::where('is_reseller', 1)->where('customer_id', auth()->user()->id)->where('status', 2)->sum('reseller_earn');

        return $total_reseller_earn;
    }


    private function calculatePayable()
    {
        $total_resller_earn = 0.00;
        $total_resller_earn = Order::where('is_reseller', 1)->where('customer_id', auth()->user()->id)->where('status', 2)->where('reseller_paid', 0)->sum('reseller_earn');

        return $total_resller_earn;
    }
    private function calculatePaid()
    {

        $total_resller_earn = 0.00;
        $total_resller_earn = Order::where('is_reseller', 1)->where('customer_id', auth()->user()->id)->where('status', 2)->where('reseller_paid', 1)->sum('reseller_earn');

        return $total_resller_earn;
    }
}
