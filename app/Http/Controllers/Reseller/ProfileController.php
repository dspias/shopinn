<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'reseller']);

    }

    /**
     * Shop Profile Index
     */
    public function index()
    {
        return view('reseller.profile.index');
    }
    /**
     * Profile Index
     */
    public function storeAvatar(Request $request)
    {
        // dd($request);

        $this->validate($request, array(
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        ));

        try {
            $user = auth()->user();
            $mediaItems = $user->getMedia('logo');
            foreach ($mediaItems as $item)
                $item->delete();

            $user->addMedia($request->avatar)->toMediaCollection('logo');
        } catch (Exception $e) {
            toast('Image does not update. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        }

        toast('Image changed.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->back();
    }
}
