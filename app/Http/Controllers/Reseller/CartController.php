<?php

namespace App\Http\Controllers\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Coupon;
use App\Facades\Cart as CartFacade;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UsedCoupon;

use App\Notifications\Admin\Order\NewOrder;
use App\Notifications\Shop\Order\NewOrder as ShopOrder;
use App\Notifications\Shop\Product\Stock;

class CartController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Cart Index
     */
    public function index()
    {
        return view('reseller.cart.index');
    }

    /**
     * Cart checkout
     */
    public function checkout()
    {
        return view('reseller.cart.checkout');
    }

    /**
     * Cart confirmation 
     */
    public function confirmation(Request $request)
    {
        $customer = auth()->user();
        $cart = CartFacade::get();
        $couponcode = CartFacade::get_code();
        $delivery = $this->ddcharge();

        $this->validate($request, array(
            'name'                  => 'required | string',
            'email'                 => 'required | email | string',
            'mobile'                => 'required | string | min:11 | max:11',
            'customer_city'         => 'required | string',
            'address'               => 'required | string',
        ));

        $oid = 'SHOPINN' . $customer->id . strtotime(now());
        $order = Order::create(array(
            'oid'           => $oid,
            'email'         => $request->email,
            'mobile'        => $request->mobile,
            'city'          => $request->customer_city,
            'address'       => $request->address,
            'customer_id'   => $customer->id,
        ));
        $order->name = $request->name;
        $order->delivery = $delivery;
        if($customer->role_id == 3) $order->is_reseller = true;
        $order->save();

        $shopList = array();
        foreach ((array)$cart as $item) {
            $orderItem  = new OrderItem();

            $product = Product::find($item['product_id']);
            if (is_null($product) || $product->stock < $item['quantity']) {
                $msg = "Sorry " . $product->name . " Stock out. please remove it";

                $temps = OrderItem::where('order_id', $order->id)->get();
                foreach ($temps as $temp) $temp->forceDelete();

                $order->forceDelete();

                toast($msg, 'error')->autoClose(false)->timerProgressBar();
                return redirect()->back();
            }
            // Product stock balance
            $product->stock -= $item['quantity'];
            $product->save();

            //listing shop for sending notificaiton
            if(!in_array($product->shop_id, $shopList)) $shopList[] = $product->shop_id;

            //if product stock lessthen 5 make notification
            if($product->stock < 2){
                user_notify($product->shop_id, new Stock($product));
            }

            $offer = offer_price($product);
            if ($offer['isOffer'] == 1) {
                $orderItem->discount = $offer['offer'];
                $orderItem->price = $offer['price'];
            } else {
                $orderItem->price = get_price($product);
            }

            $offer_sell = offer_sell_price($product);
            if ($offer_sell['isOffer'] == 1) {
                $orderItem->discount = $offer_sell['offer'];
                $orderItem->s_sell_price = $offer_sell['price'];
            } else {
                $orderItem->s_sell_price = get_price($product);
            }

            $orderItem->order_id    =   $order->id;
            $orderItem->product_id    =   $item['product_id'];
            $orderItem->shop_id    =   $item['shop_id'];
            if (!empty($item['details'])) {
                if (array_key_exists("color", $item['details'])) $orderItem->color    =   $item['details']['color'];
                if (array_key_exists("size", $item['details'])) $orderItem->size    =   $item['details']['size'];
            }
            $orderItem->quantity    =   $item['quantity'];

            if (!$orderItem->save()) {
                toast('Please Try Again. There is some errors.', 'error')->autoClose(2000)->timerProgressBar();
                return redirect()->back();
            }
        }

        if (!empty($couponcode)) {
            $coupon = Coupon::with(['uses' => function ($query) use ($customer) {
                $query->where('customer_id', $customer->id);
            }])->where('code', $couponcode['code'])
                ->where('expire_date', '>=', date('Y-m-d'))
                ->first();

            if ($coupon != null && $coupon->uses->count() < $coupon->limit) {
                $order->coupon_id   =   $coupon->id;
                $order->save();

                $usedCoupon = new UsedCoupon();

                $usedCoupon->coupon_id      =   $coupon->id;
                $usedCoupon->customer_id    =   $customer->id;
                $usedCoupon->order_id       =   $order->id;

                $usedCoupon->save();
            }
        }

        /**
         * ! make Notifaition for new order to admin
         */
        admin_notify(new NewOrder($order));

        /**
         * ! make Notifaition for new order to shops
         */
        foreach((array)$shopList as $shop){
            user_notify($shop, new ShopOrder($order));
        }

        CartFacade::clear();

        toast('Order Placed successfully.', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->route('reseller.order.all');
    }

    
    private function ddcharge()
    {
        $value = 0;
        $charge = CartFacade::get_dcharge();
        foreach((array) $charge as $temp){
            $value += $temp['charge'];
        }
        return $value;
    }
}
