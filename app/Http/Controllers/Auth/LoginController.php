<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (Auth::check() && Auth::user()->role->id == 1) {
            $this->redirectTo = route('admin.dashboard.index');
        } elseif (Auth::check() && Auth::user()->role->id == 2) {
            $this->redirectTo = route('shop.dashboard.index');
        } elseif (Auth::check() && Auth::user()->role->id == 3) {
            $this->redirectTo = route('reseller.dashboard.index');
        } elseif (Auth::check() && Auth::user()->role->id == 4) {
            $this->redirectTo = route('rider.dashboard.index');
        } else {
            $this->redirectTo = route('guest.homepage.index');
        }

        $this->middleware('guest')->except('logout');
    }



    // After login where to go
    public function redirectAuth()
    {
        if (Auth::check() && Auth::user()->role->id == 1) {
            $redirectTo = 'admin.dashboard.index';
        } elseif (Auth::check() && Auth::user()->role->id == 2) {
            $redirectTo = 'shop.dashboard.index';
        } elseif (Auth::check() && Auth::user()->role->id == 3) {
            $redirectTo = 'reseller.dashboard.index';
        } elseif (Auth::check() && Auth::user()->role->id == 4) {
            $redirectTo = 'rider.dashboard.index';
        } else {
            $redirectTo = 'guest.homepage.index';
        }

        $this->middleware('guest')->except('logout');
        return $redirectTo;
    }

    // Login with Mobile or email
    public function loginWithEmailOrMobile(Request $request)
    {
        // dd($request);
        $this->validate($request, array(
            'email' => 'required | string | max:191',
            'password' => 'required | string | min:8',
        ));

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1], $request->remember)) {
            return redirect()->route($this->redirectAuth());
            // return "logged in email";
        } elseif (Auth::attempt(['mobile' => $request->email, 'password' => $request->password, 'is_active' => 1], $request->remember)) {

            return redirect()->route($this->redirectAuth());
            // return "logged in by Mobile";

        } else {
            // dd($request);
            toast('Does not match login Credentials', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
            // return "not logged in";
        }
    }


    /**
     * Redirect the user to the provider authentication page.
     *
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        // dd($user);
        // Find existing user.
        $existingUser = User::where('email', $user->getEmail())->first();
        if ($existingUser) {
            Auth::login($existingUser);
            return redirect()->route($this->redirectAuth());
            // if (Auth::attempt(['email' => $existingUser->email, 'is_active' => 1])) {
            // }
        } else {
            // Create new user.
            $newUser = User::create([
                'name' => $user->getName(),
                'role_id' => 5,
                'password' => Hash::make('@test@pinikk@2021#sust^leading*gotoHell'),
            ]);
            $newUser->email = $user->getEmail();
            $newUser->mobile = '01' . rand(10000000000, 99999999999);
            $newUser->email_verified_at = now();
            $newUser->save();
            // upload images
            if ($user->getAvatar()) {
                // $newUser->addMediaFromUrl($user->getAvatar())->toMediaCollection('avatar');
                $newUser->addMediaFromUrl($user->getAvatar())->toMediaCollection('logo');
            }
            // Auth::login($newUser);
            if (Auth::attempt(['email' => $user->getEmail(), 'password' => '@test@pinikk@2021#sust^leading*gotoHell', 'is_active' => 1])) {
                return redirect()->route($this->redirectAuth());
            }
        }
        toast('You have successfully logged in with ' . ucfirst($provider) . '!', 'success')->autoClose(2000)->timerProgressBar();
        return redirect()->route('guest.homepage.index');
    }
}
