<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Models\Code;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Notifications\Admin\NewShop;
use App\Notifications\Admin\NewReseller;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (isset($data['mobile'])) {

            Validator::make($data, [
                'mobile' => ['required', 'regex:/(01)[0-9]{9}/', 'max:14', 'min:11', 'unique:users'],
            ])->validate();
        }
        // dd($data);
        if (isset($data['email'])) {
            $email = $data['email'];
            $code = Code::where('email', $email)->first();
        }

        if (isset($data['mobile']) && $code == null) {
            $mobile = $data['mobile'];
            $code = Code::where('mobile', $mobile)->first();
        }

        if ($code == null) {
            toast('Verification Code does not', 'error')->autoClose(2000)->timerProgressBar();
            return redirect()->back();
        } else {
            $code->delete();
        }

        $role_id = (!isset($data['register_as'])) ? 5 : (($data['register_as'] == 'shop') ? 2 : 3);
        if ($role_id == 2) {
            $this->redirectTo = '/shop/dashboard';
        } elseif ($role_id == 3) {
            $this->redirectTo = '/reseller/dashboard';
        }
        $user =  User::create([
            'name' => $data['name'],
            'role_id' => $role_id,
            'password' => Hash::make($data['password']),
        ]);

        $user->email = $data['email'];
        $user->mobile = $data['mobile'];
        $user->save();

        /**
         * make Notification for new registered user
         */
        if ($role_id == 2) {
            admin_notify(new NewShop($user));
        } else if ($role_id == 2) {
            admin_notify(new NewReseller($user));
        }
        return $user;
    }
}
