<?php

namespace App\Http\Livewire\Shop;

use Livewire\Component;

use App\Models\Shop;
use App\Models\Product;
use App\Models\ProductCategory as Category;
use App\User;
use DB;

class ShopDetails extends Component
{
    public $colors, $sizes, $tags, $types, $products, $categories;
    public $shop, $key, $categoylists;

    public $min, $max, $cat_ids = array();

    public function mount($colors, $sizes, $tags, $types, $products, $categories)
    {
        $this->shop = auth()->user();
        $this->key = null;
        $this->colors = $colors;
        $this->sizes = $sizes;
        $this->tags = $tags;
        $this->types = $types;
        $this->products = $products;
        $this->categories = $categories;
        $this->categoylists = Category::where('is_active', 1)->get();
    }

    public function render()
    {
        return view('livewire.shop.shop-details');
    }

    public function submit()
    {
        $key = $this->key;
        if($key == "") $key = null;

        $this->products = Product::with(['shop.shop', 'ratings'])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where('shop_id', $this->shop->id)
            ->where("name", "LIKE", "%$key%")
            ->whereHas('category', function ($q) use ($key) {
                $q->orWhere("name", "LIKE", "%$key%");
            })
            ->get();
    }

    public function filter()
    {
        $this->products = $this->pri_filter();
    }


    private function pri_filter(){
        $key = $this->key;
        $products = Product::with(['shop.shop', 'ratings'])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where('shop_id', $this->shop->id)
            ->where("name", "LIKE", "%$key%")
            ->whereHas('category', function ($q) use ($key) {
                $q->orWhere("name", "LIKE", "%$key%");
            });
        if(!empty($this->cat_ids)){
            foreach($this->cat_ids as $sl => $cat){
                if($sl == 0) $products->where('category_id', $cat);
                else $products->orWhere('category_id', $cat);
            }
        }

        if($this->min != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '>=', $this->min);
        }
        if($this->max != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '<=', $this->max);
        }
        return $products->get();
    }
}
