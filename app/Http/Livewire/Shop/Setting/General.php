<?php

namespace App\Http\Livewire\Shop\Setting;

use App\Models\Shop;
use Livewire\Component;
use App\User;

class General extends Component
{
    public $user;

    public $shopName, $email, $mobile, $shopEmail, $shopMobile, $city, $shopAddress, $shopFacebook, $shopTwitter, $shopInstagram, $ownerName, $ownerEmail, $ownerMobile, $ownerAddress, $note, $lang;


    public function mount()
    {
        $this->user = User::with(['role', 'shop'])->where('id', auth()->user()->id)->first();
        $this->shopName = $this->user->name;
        $this->email = $this->user->email;
        $this->mobile = $this->user->mobile;
        $this->lang    = $this->user->lang;
        $this->shopEmail    = optional($this->user->shop)->shop_email;
        $this->shopMobile    = optional($this->user->shop)->shop_mobile;
        $this->city    = $this->user->city;
        $this->shopAddress    = optional($this->user->shop)->shop_address;
        $this->ownerName    = optional($this->user->shop)->owner_name;
        $this->ownerEmail    = optional($this->user->shop)->owner_email;
        $this->ownerMobile    = optional($this->user->shop)->owner_mobile;
        $this->ownerAddress    = optional($this->user->shop)->owner_address;
        $this->shopFacebook    = optional($this->user->shop)->facebook;
        $this->shopInstagram    = optional($this->user->shop)->instagram;
        $this->shopTwitter    = optional($this->user->shop)->twitter;
        $this->note    = optional($this->user->shop)->note;
    }


    public function render()
    {
        return view('livewire.shop.setting.general');
    }

    public function saveInformation()
    {
        $this->validate([
            'shopName' => 'required | string',
            'city' => 'required | string',
            'shopAddress' => 'required | string',
            'shopMobile' => 'required | string | min:11 | max:11',
            'shopEmail' => 'required | string',
            'ownerName' => 'required | string',
            'ownerMobile' => 'required | string',
            'lang' => 'required',
        ]);
        if ($this->user->email != $this->email) {
            $this->validate([
                'email' => 'required | string | email | unique:users',
            ]);
        }
        if ($this->user->mobile != $this->mobile) {
            $this->validate([
                'mobile' => 'required | string | min:11 | unique:users',
            ]);
        }

        $data = User::find($this->user->id);
        $data->name = $this->shopName;
        $data->email = $this->email;
        $data->mobile = $this->mobile;
        $data->city = $this->city;
        $data->lang = $this->lang;
        $data->save();
        if ($this->user->shop == null) {
            $shop = new Shop();
            $shop->shop_id = $this->user->id;
        } else {
            $shop = Shop::find($this->user->shop->id);
        }
        $shop->shop_mobile = $this->shopMobile;
        $shop->shop_email = $this->shopEmail;
        $shop->shop_address = $this->shopAddress;
        $shop->owner_name = $this->ownerName;
        $shop->owner_mobile = $this->ownerMobile;
        $shop->owner_email = $this->ownerEmail;
        $shop->owner_address = $this->ownerAddress;
        $shop->facebook = $this->shopFacebook;
        $shop->instagram = $this->shopInstagram;
        $shop->twitter = $this->shopTwitter;
        $shop->note = $this->note;

        $shop->save();

        $this->user = User::with(['role', 'shop'])->where('id', auth()->user()->id)->first();
        session()->flash('success', 'Profile Updated successfully !');
    }
}
