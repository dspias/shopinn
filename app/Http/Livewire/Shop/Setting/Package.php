<?php

namespace App\Http\Livewire\Shop\Setting;

use Livewire\Component;

class Package extends Component
{
    public function render()
    {
        return view('livewire.shop.setting.package');
    }
}
