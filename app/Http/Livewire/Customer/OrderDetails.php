<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use Illuminate\Support\Facades\Redirect;
use App\Models\Product;
use App\Models\Coupon;
use App\Facades\Cart as CartFacade;

class OrderDetails extends Component
{
    public $cart;
    public $totalCost = 0;
    public $couponDiscount = 0;
    public $deliveryCharge = 0;
    public $customer;

    public $customer_city;

    //coupon code
    public $code;
    public $couponcode;


    public function mount(): void
    {
        $this->customer = auth()->user();
        $this->customer_city = $this->customer->city;
        $this->cart = CartFacade::get();
        $this->couponcode = CartFacade::get_code();
        $this->totalCost = 0;
        if (count($this->couponcode) > 0) {
            $this->couponDiscount = $this->couponcode['discount'];
        }
        
        $this->charge = CartFacade::get_dcharge();
        $this->ddcharge();
    }
    private function ddcharge(): void
    {
        $this->deliveryCharge = 0;
        foreach((array) $this->charge as $temp){
            $this->deliveryCharge += $temp['charge'];
        }
    }
    public function render()
    {
        if(!is_null($this->customer_city)){
            CartFacade::delivery_charge($this->customer_city);
            $this->charge = CartFacade::get_dcharge();
            $this->ddcharge();
        }
        return view('livewire.customer.order-details');
    }
}
