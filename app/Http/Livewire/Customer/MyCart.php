<?php

namespace App\Http\Livewire\Customer;

use Illuminate\View\View;
use Livewire\Component;
use App\Facades\Cart;

class MyCart extends Component
{
    public $cartTotal = 0;

    protected $listeners = [
        'productAdded' => 'updateCartTotal',
        'productRemoved' => 'updateCartTotal',
        'clearCart' => 'updateCartTotal'
    ];

    public function mount(): void
    {
        $this->cartTotal = count(Cart::get());
    }
    public function render()
    {
        return view('livewire.customer.my-cart');
    }

    public function updateCartTotal(): void
    {
        $this->cartTotal = count(Cart::get());
    }
}
