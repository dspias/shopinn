<?php

namespace App\Http\Livewire\Customer\Setting;

use Livewire\Component;

use App\User;
use Hash;

class Security extends Component
{
    public $user;
    public $current_password, $password, $password_confirmation;

    public function mount(): void
    {
        $this->user = auth()->user();
    }

    public function render()
    {
        return view('livewire.customer.setting.security');
    }


    public function changePassword()
    {

        $this->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:8|confirmed',
        ]);

        if (!(Hash::check($this->current_password, $this->user->password))) {
            // The passwords matches
            return session()->flash('error', 'Your current password does not matches with the password you provided. Please try again.');
        }

        if(strcmp($this->current_password, $this->password) == 0){
            //Current password and new password are same
            return session()->flash('error', 'New Password cannot be same as your current password. Please choose a different password.');

        }

        //Change Password
        $user = auth()->user();
        $user->password = Hash::make($this->password);
        $user->save();

        session()->flash('success', 'Password changed successfully !');

    }
}
