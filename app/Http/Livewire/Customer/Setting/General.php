<?php

namespace App\Http\Livewire\Customer\Setting;

use Livewire\Component;

use App\User;

class General extends Component
{

    public $user;
    public $name, $email, $mobile, $city, $address, $lang, $avatar;
    

    public function mount(): void
    {
        $this->user = auth()->user();
        $this->name = $this->user->name;
        $this->email = $this->user->email;
        $this->mobile = $this->user->mobile;
        $this->address = $this->user->address;
        $this->lang = $this->user->lang;
        $this->city = $this->user->city;
        $this->avatar = null;
    }

    public function saveInformation(){
        $this->validate([
            'name' => 'required | string',
            'city' => 'required | string',
            'address' => 'required | string',
            'lang' => 'required',
        ]);
        if($this->user->email != $this->email){
            $this->validate([
                'email' => 'required | string | email | unique:users',
            ]);
        }
        if($this->user->mobile != $this->mobile){
            $this->validate([
                'mobile' => 'required | string | min:11 | unique:users',
            ]);
        }
        $data = User::find($this->user->id);
        $data->name = $this->name;
        $data->email = $this->email;
        $data->mobile = $this->mobile;
        $data->city = $this->city;
        $data->address = $this->address;
        $data->lang = $this->lang;
        $data->save();

        $this->user = auth()->user();
        session()->flash('success', 'Profile Updated successfully !');
    }

    public function render()
    {
        return view('livewire.customer.setting.general');
    }
}
