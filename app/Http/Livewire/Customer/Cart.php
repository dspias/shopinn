<?php

namespace App\Http\Livewire\Customer;

use Illuminate\Support\Facades\Redirect;
use App\Models\Product;
use App\Models\Coupon;
use Livewire\Component;
use App\Facades\Cart as CartFacade;

class Cart extends Component
{
    public $cart, $charge;
    public $totalCost = 0;
    public $couponDiscount = 0;
    public $deliveryCharge = 0;

    //coupon code
    public $code, $couponCode, $couponAlert;


    public function mount(): void
    {
        $this->cart = CartFacade::get();
        $this->couponCode = CartFacade::get_code();
        $this->totalCost = 0;
        $this->couponDiscount = (!empty($this->couponCode))?$this->couponCode['discount']:0;
        $this->deliveryCharge = 0;
        $this->couponAlert = null;
        CartFacade::delivery_charge();
        $this->charge = CartFacade::get_dcharge();
        $this->ddcharge();
    }
    private function ddcharge(): void
    {
        foreach((array) $this->charge as $temp){
            $this->deliveryCharge += $temp['charge'];
        }
    }
    public function render()
    {
        return view('livewire.customer.cart');
    }
    public function removeFromCart($productId): void
    {
        CartFacade::remove($productId);
        CartFacade::delivery_charge();
        $this->cart = CartFacade::get();
        $this->emit('productRemoved');
        $this->couponDiscount = 0;
    }
    public function checkout()
    {
        return redirect()->to('customer/cart/checkout');
    }


    public function submit()
    {
        $validatedData = $this->validate([
            'code' => 'required',
        ]);

        $coupon = Coupon::with(['uses' => function ($q) {
            $q->where('customer_id', auth()->user()->id);
        }])
            ->where('code', $this->code)
            ->where('expire_date', '>=', date('Y-m-d'))
            ->first();
        if ($coupon != null && $coupon->uses->count() < $coupon->limit) {
            $this->discount($coupon);
        } else {
            $this->couponAlert = 'This coupon is not available';
        }
        
        $this->code = NULL;
        // dd($this->couponAlert);
    }

    public function discount($coupon)
    {
        $cost = 0;
        foreach ($this->cart as $item) {
            if ($item['shop_id'] == $coupon->shop_id) {
                $offer = offer_price(Product::where('id', $item['product_id'])->first());
                if ($offer['isOffer'] == 1) {
                    $sub = ($item['quantity'] * $offer['price']);
                    $cost += $sub;
                } else {
                    $sub = ($item['quantity'] * $offer['main']);
                    $cost += $sub;
                }
            }
        }
        if ($coupon->min_purchase <= $cost) {
            $this->couponDiscount = $coupon->discount;
            CartFacade::set_code(array('code' => $this->code, 'shop_id' => $coupon->shop_id, 'discount' => $coupon->discount));
        } 
    }



    public function updateCart(int $productId, $quantity, $key): void
    {
        $temp = CartFacade::get();
        $details = $temp[$key]['details'];
        CartFacade::add(Product::where('id', $productId)->first(), $quantity, $details);
        $this->cart = CartFacade::get();
        CartFacade::delivery_charge();
        $this->couponDiscount = 0;
    }
}
