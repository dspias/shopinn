<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use App\Models\Product;
use App\Facades\Cart;

class AddToCart extends Component
{
    public $product;

    public function mount($product)
    {
        $this->product = $product;
    }

    public function render()
    {
        return view('livewire.customer.add-to-cart');
    }


    public function addToCart(int $productId): void
    {
        Cart::add(Product::where('id', $productId)->first());
        $this->emit('productAdded');
    }
}
