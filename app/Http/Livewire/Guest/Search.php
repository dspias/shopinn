<?php

namespace App\Http\Livewire\Guest;

use Livewire\Component;
use App\Models\Product;
use App\Models\ProductType;
use Livewire\WithPagination;
use DB;
class Search extends Component
{
    use WithPagination;
    public $key;
    public $products;
    public $types;

    public $title, $min, $max, $cat_id;

    public function mount($key)
    {
        $this->key = $key;

        $this->types = ProductType::with(['categories' => function ($q) {
            $q->where('is_active', 1);
        }])->where('is_active', 1)->get();
        $this->title = $key;

        $this->min = null;
        $this->max = null;

        $this->search();
    }

    public function search()
    {
        $key = $this->key;
        $this->products = Product::with([
            'shop' => function ($query) {
                $query->select('id', 'name');
            },
            'ratings',
            'offers',
            'sizes',
            'colors'
        ])
            ->where("name", "LIKE", "%$key%")
            ->whereHas('shop', function ($q) use ($key) {
                $q->orWhere("name", "LIKE", "%$key%");
            })
            ->orwhere(DB::raw('ifnull(updated_price, sell_price)'), $key)
            ->where('is_active', 1)
            // ->paginate(30);
            ->get();
    }


    public function render()
    {
        return view('livewire.guest.search');
    }

    public function filter()
    {
        $this->products = $this->pri_filter($this->title);
    }

    private function pri_filter($key){
        $products = Product::with([
            'shop' => function ($query) {
                $query->select('id', 'name');
            },
            'ratings',
            'offers',
            'sizes',
            'colors'
        ])
        ->where("name", "LIKE", "%$key%")
        ->whereHas('shop', function ($q) use ($key) {
            $q->orWhere("name", "LIKE", "%$key%");
        })
        ->where('is_active', 1);

        if($this->min != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '>=', $this->min);
        }
        if($this->max != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '<=', $this->max);
        }
        if($this->cat_id != null){
            $products->where('category_id', $this->cat_id);
        }
        return $products->get();
    }
}
