<?php

namespace App\Http\Livewire\Guest;

use Illuminate\Support\Facades\Redirect;
use Livewire\Component;
use App\Models\Product;
use App\Facades\Cart as CartFacade;

class ProductDetails extends Component
{
    public $product;

    public $color;
    public $size;
    public $quantity = 1;

    public function mount($product)
    {
        $this->product = $product;
    }

    public function submit()
    {
        if (count($this->product->colors) > 0) {
            $this->validate([
                'color' => 'required',
            ]);
        }
        if (count($this->product->sizes) > 0) {
            $this->validate([
                'size' => 'required',
            ]);
        }
        $this->validate([
            'quantity' => 'required',
        ]);

        $details = array(
            'color' => $this->color,
            'size' => $this->size,
        );
        $this->addToCart($details, $this->product->id);

        return redirect()->to('/');
    }

    public function render()
    {
        return view('livewire.guest.product-details');
    }

    public function increase()
    {
        $this->quantity += 1;
        if ($this->product->stock < $this->quantity) {
            $this->quantity = $this->product->stock;
        }
    }

    public function decrease()
    {
        $this->quantity -= 1;
        if (1 > $this->quantity) {
            $this->quantity = 1;
        }
    }

    public function addToCart(array $item, $productId)
    {
        CartFacade::add(Product::where('id', $productId)->first(), $this->quantity, $item);
        $this->emit('productAdded');
    }

    public function buynow()
    {
        
        if (count($this->product->colors) > 0) {
            $this->validate([
                'color' => 'required',
            ]);
        }
        if (count($this->product->sizes) > 0) {
            $this->validate([
                'size' => 'required',
            ]);
        }
        $this->validate([
            'quantity' => 'required',
        ]);

        $details = array(
            'color' => $this->color,
            'size' => $this->size,
        );
        $this->addToCart($details, $this->product->id);

        return redirect()->to('/customer/cart');
    }
}
