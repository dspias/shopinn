<?php

namespace App\Http\Livewire\Guest;

use Livewire\Component;
use App\Models\Product;
use App\Facades\Cart;

class AddToCart extends Component
{
    public $product;

    public function mount($product)
    {
        $this->product = $product;
    }

    public function render()
    {
        return view('livewire.guest.add-to-cart');
    }


    public function addToCart(int $productId): void
    {
        Cart::add(Product::where('id', $productId)->first());
        $this->emit('productAdded');
    }
}
