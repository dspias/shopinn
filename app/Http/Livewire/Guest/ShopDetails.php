<?php

namespace App\Http\Livewire\Guest;

use Livewire\Component;

use App\Models\Shop;
use App\Models\Product;
use App\Models\ProductCategory as Category;
use App\User;
use DB;

class ShopDetails extends Component
{
    public $shop, $key, $products, $categories;

    public $min, $max, $cat_ids = array();

    public function mount($shop)
    {
        $this->shop = $shop;
        $this->key = null;
        $this->products = $this->shop->products;
        $this->categories = Category::where('is_active', 1)->get();
    }

    public function render()
    {
        return view('livewire.guest.shop-details');
    }

    public function submit()
    {
        $key = $this->key;
        if($key == "") $key = null;

        $this->products = Product::with(['shop.shop', 'ratings'])->whereNotNull('approved_at')
            ->where('is_active', 1)
            ->where('shop_id', $this->shop->id)
            ->where("name", "LIKE", "%$key%")
            ->whereHas('category', function ($q) use ($key) {
                $q->orWhere("name", "LIKE", "%$key%");
            })
            ->get();
    }

    public function filter()
    {
        $products = $this->pri_filter();
        $this->products = $products->where('shop_id', $this->shop->id);
    }


    private function pri_filter(){
        $key = $this->key;
        $products = Product::with(['shop.shop', 'ratings'])
            ->where("name", "LIKE", "%$key%")
            ->whereHas('category', function ($q) use ($key) {
                $q->orWhere("name", "LIKE", "%$key%");
            });
        if(!empty($this->cat_ids)){
            foreach($this->cat_ids as $sl => $cat){
                if($sl == 0) $products->where('category_id', $cat);
                else $products->orWhere('category_id', $cat);
            }
        }

        if($this->min != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '>=', $this->min);
        }
        if($this->max != null){
            $products->where(DB::raw('ifnull(updated_price, sell_price)'), '<=', $this->max);
        }
        $products->whereNotNull('approved_at')
        ->where('is_active', 1)
        ->where('shop_id', $this->shop->id);
        return $products->get();
    }
}
