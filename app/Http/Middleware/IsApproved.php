<?php

namespace App\Http\Middleware;

use Closure;

use RealRashid\SweetAlert\Facades\Alert;

class IsApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->approved_at) {
            if (auth()->user()->role_id == 2) {
                if (is_null(auth()->user()->shop)) {
                    $message = '<h4>You have to update your valid information first.</h4>';
                } else {
                    $message = '<h4>Shopinn didn\'t approve your shop yet.</h4> <h5>Please update your information properly or contact with <span class="text-shopinn text-bold">Shopinn-BD</span></h5>';
                }
                Alert::html('Shopinn Alert', $message, 'warning')->autoClose(false)->timerProgressBar();
                $path = 'shop.setting.shop.index';
            } elseif (auth()->user()->role_id == 3) {
                if (is_null(auth()->user()->shop)) {
                    $message = '<h4>You have to update your valid information first.</h4>';
                } else {
                    $message = '<h4>Shopinn didn\'t approve your account yet.</h4> <h5>Please update your information properly or contact with <span class="text-shopinn text-bold">Shopinn-BD</span></h5>';
                }
                Alert::html('Shopinn Alert', $message, 'warning')->autoClose(false)->timerProgressBar();
                $path = 'reseller.profile.index';
            }
            return redirect()->route($path);
        }
        return $next($request);
    }
}
