<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsDev
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dev = Auth::user()->email;
        if (Auth::check() && Auth::user()->role->id == 1 && $dev == 'dev.pinikk@gmail.com') {
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
}
