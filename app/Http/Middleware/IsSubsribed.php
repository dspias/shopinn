<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsSubsribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(!subsribed(auth()->user()->id)) return redirect()->route('shop.setting.package.index');
        }

        return $next($request);
    }
}
