<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentReport extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'invoice_id'
    ];

    //relation with shop
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    //relation with orders
    public function orders(){
        return $this->hasMany('App\Models\PaymentOrderList', 'pay_rep_id', 'id');
    }
}
