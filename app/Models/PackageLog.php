<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageLog extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];    

    //relation with shop
    public function shop(){
        return $this->belongsTo('App\User', 'shop_id', 'id');
    }

    //relation with package
    public function package(){
        return $this->belongsTo('App\Models\BusinessPackage', 'package_id', 'id');
    }
}
