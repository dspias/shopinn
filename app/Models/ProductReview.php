<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductReview extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];
    
    //relation with product
    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    //relation with customer
    public function customer(){
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }
}
