<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{

    // Use Relations and Scopes
    use SoftDeletes, InteractsWithMedia;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pid', 'shop_id', 'description', 'name', 'stock', 'sell_price', 'category_id'];

    //relation with shop alia
    public function shop()
    {
        return $this->belongsTo('App\User', 'shop_id', 'id');
    }

    //relation with category
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    //relation with ratings
    public function ratings()
    {
        return $this->hasMany('App\Models\ProductRating', 'product_id', 'id');
    }

    //relation with reviews
    public function reviews()
    {
        return $this->hasMany('App\Models\ProductReview', 'product_id', 'id');
    }

    //relation with offers
    public function offers()
    {
        return $this->hasMany('App\Models\Offer', 'product_id', 'id');
    }

    //relation with orders
    public function orders()
    {
        return $this->hasMany('App\Models\OrderItem', 'product_id', 'id');
    }

    //relation with orderitems
    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem', 'product_id', 'id');
    }


    //relation with resellerProducts
    public function resellerProducts()
    {
        return $this->hasMany('App\Models\ResellerProduct', 'product_id', 'id');
    }

    //relation with sizes
    public function sizes()
    {
        return $this->hasMany('App\Models\Size', 'product_id', 'id');
    }

    //relation with colors
    public function colors()
    {
        return $this->hasMany('App\Models\Color', 'product_id', 'id');
    }

    //relation with tags
    public function tags()
    {
        return $this->hasMany('App\Models\Tag', 'product_id', 'id');
    }

    //relation with product comments
    public function comments()
    {
        return $this->hasMany('App\Models\ProductComment', 'product_id', 'id');
    }






    /**
     * !`contain`, `max`, `fill`, `stretch`, `crop`
     *
     * Handling Media Collections
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('product')
            ->registerMediaConversions(function (Media $media = null) {
                $this->addMediaConversion('thumb')
                    ->fit('crop', 70, 70);
                $this->addMediaConversion('slider')
                    ->fit('fill', 600, 600);
                $this->addMediaConversion('timeline');
                // ->fit('crop', 448, 224);
                $this->addMediaConversion('details')
                    ->fit('fill', 1000, 1000);
                $this->addMediaConversion('card')
                    ->fit('crop', 450, 328);
            });
    }
}
