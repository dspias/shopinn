<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResellerProduct extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'reseller_id'
    ];
    
    //relation with reseller
    public function reseller(){
        return $this->belongsTo('App\User', 'reseller_id', 'id');
    }    
    
    //relation with product
    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}
