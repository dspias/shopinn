<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    //relation with orders
    public function orders(){
        return $this->hasMany('App\Models\Order', 'coupon_id', 'id');
    }

    //relation with uses
    public function uses(){
        return $this->hasMany('App\Models\UsedCoupon', 'coupon_id', 'id');
    }
}
