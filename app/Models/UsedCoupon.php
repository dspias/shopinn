<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsedCoupon extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    //relation with User
    public function user(){
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    //relation with coupon
    public function coupon(){
        return $this->belongsTo('App\Models\Coupon', 'coupon_id', 'id');
    }
       
    //relation with order
    public function order(){
        return $this->hasOne('App\Models\Order', 'order_id', 'id');
    }
}
