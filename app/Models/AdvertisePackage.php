<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertisePackage extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'details', 'status'];

    // //relation with type
    // public function type()
    // {
    //     return $this->belongsTo('App\Models\AdvertiseType', 'type_id', 'id');
    // }

    /**
     * Relation with types table
     */
    public function types()
    {
        return $this->belongsToMany('App\Models\AdvertiseType')->withPivot('max_ad');
    }

    //relation with logs
    public function logs()
    {
        return $this->hasMany('App\Models\AdvertiseLog', 'package_id', 'id');
    }
}
