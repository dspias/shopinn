<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminAdvertise extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url_link', 'day', 'ad_image'];

    //relation with advertise_types
    public function type()
    {
        return $this->belongsTo('App\Models\AdvertiseType', 'type_id', 'id');
    }
}
