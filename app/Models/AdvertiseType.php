<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertiseType extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    // //relation with packages
    // public function packages(){
    //     return $this->hasMany('App\Models\AdvertisePackage', 'type_id', 'id');
    // }

    /**
     * Relation with packages table
     */
    public function packages()
    {
        return $this->belongsToMany('App\Models\AdvertisePackage');
    }

    /**
     * Relation with admin_advertises table
     */
    public function advertises()
    {
        return $this->hasMany('App\Models\AdminAdvertise', 'type_id', 'id');
    }

    /**
     * Relation with admin_advertises table
     */
    public function logdetails()
    {
        return $this->hasMany('App\Models\AdvertiseLogDetails', 'type_id', 'id');
    }
}
