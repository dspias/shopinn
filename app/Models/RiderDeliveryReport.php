<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiderDeliveryReport extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];
    
    //relation with rider
    public function rider(){
        return $this->belongsTo('App\User', 'rider_id', 'id');
    }

    //relation with orders
    public function orders(){
        return $this->hasMany('App\Models\RiderDeliveryOrderList', 'report_id', 'id');
    }
}
