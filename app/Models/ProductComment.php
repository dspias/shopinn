<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{

    //relation with product
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    
    //relation with users
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    //relation with product replies
    public function replies()
    {
        return $this->hasMany('App\Models\ProductReply', 'comment_id', 'id');
    }
}
