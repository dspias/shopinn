<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['oid', 'email', 'mobile', 'city', 'address', 'customer_id'];

    //relation with customer alia
    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    //relation with rider alia
    public function rider()
    {
        return $this->belongsTo('App\User', 'rider_id', 'id');
    }

    //relation with coupon
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id', 'id');
    }

    //relation with items
    public function items()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id', 'id');
    }

    //relation with usedCoupon
    public function usedCoupon()
    {
        return $this->hasOne('App\Models\OrderItem', 'order_id', 'id');
    }



    //relation with paymentReport
    public function paymentReport()
    {
        return $this->hasOne('App\Models\PaymentOrderList', 'order_id', 'id');
    }

    //relation with riderReports
    public function riderReports()
    {
        return $this->hasOne('App\Models\RiderDeliveryOrderList', 'order_id', 'id');
    }
}
