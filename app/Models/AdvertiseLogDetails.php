<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertiseLogDetails extends Model
{

    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    //relation with advertise_log table
    public function ad_log()
    {
        return $this->belongsTo('App\Models\AdvertiseLog', 'log_id', 'id');
    }

    //relation with types table
    public function type()
    {
        return $this->belongsTo('App\Models\AdvertiseType', 'type_id', 'id');
    }
}
