<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    //relation with type
    public function type(){
        return $this->belongsTo('App\Models\ProductType', 'type_id', 'id');
    }

    //relation with products
    public function products(){
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }
}
