<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReply extends Model
{

    //relation with product comment
    public function comment()
    {
        return $this->belongsTo('App\Models\ProductComment', 'comment_id', 'id');
    }

    //relation with users
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
