<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiderDeliveryOrderList extends Model
{
    
    // Use Relations and Scopes
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];    

    //relation with order
    public function order(){
        return $this->hasOne('App\Models\Order', 'order_id', 'id');
    }

    //relation with report
    public function report(){
        return $this->belongsTo('App\Models\RiderDeliveryReport', 'report_id', 'id');
    }
}
