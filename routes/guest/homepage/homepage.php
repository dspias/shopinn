<?php

// Homepage
Route::group([
    'prefix'        =>      '', // URL
    'namespace'     =>      'Homepage', // Controller Path
    'as'            =>      'homepage.', // Route
], function () {
    Route::get('/', 'HomepageController@index')->name('index'); // Homepage
    Route::get('/category/{cat_id}', 'HomepageController@category')->name('category'); // Homepage
    Route::get('/search', 'HomepageController@search')->name('search'); // Homepage

    // !About
    Route::get('/about', 'HomepageController@aboutShopinn')->name('about'); // Homepage
});
