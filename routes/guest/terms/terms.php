<?php

// Termas & Policy
Route::group([
    'prefix'        =>      'termsandpolicy', // URL
    'namespace'     =>      'Terms', // Controller Path
    'as'            =>      'terms.', // Route
], function () {
    Route::get('/', 'TermsController@index')->name('index'); // Terms index
});
