<?php

// search
Route::group([
    'prefix'        =>      'search', // URL
    'namespace'     =>      'Search', // Controller Path
    'as'            =>      'search.', // Route
], function () {
    Route::post('/find', 'SearchController@find')->name('find'); // product
    Route::get('/{search_keyword}', 'SearchController@index')->name('index'); // product
});
