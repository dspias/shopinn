<?php

// Customer Routes
Route::group(
    [
        'prefix'        =>      '', // URL
        'namespace'     =>      'Guest', // Controller Path
        'as'            =>      'guest.', // Route
    ],
    function () {
        include_once 'homepage/homepage.php';
        include_once 'product/product.php';
        include_once 'shop/shop.php';
        include_once 'search/search.php';
        include_once 'faq/faq.php';
        include_once 'terms/terms.php';
        include_once 'contact/contact.php';
        
        Route::post('/change_lang/{lang}', 'Homepage\HomepageController@changeLang')->name('change_lang'); // Terms index
    }
);
