<?php

// shop
Route::group([
    'prefix'        =>      'shop', // URL
    'namespace'     =>      'Shop', // Controller Path
    'as'            =>      'shop.', // Route
], function () {
    Route::get('/all', 'ShopController@index')->name('index'); // product
    Route::get('/{shop_id}/{shop_name}', 'ShopController@show')->name('show'); // product
});
