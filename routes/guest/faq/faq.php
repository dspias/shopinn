<?php

// FAQ
Route::group([
    'prefix'        =>      'faq', // URL
    'namespace'     =>      'FAQ', // Controller Path
    'as'            =>      'faq.', // Route
], function () {
    Route::get('/', 'FAQController@index')->name('index'); // faq index
});
