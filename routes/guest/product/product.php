<?php

// product
Route::group([
    'prefix'        =>      'product', // URL
    'namespace'     =>      'Product', // Controller Path
    'as'            =>      'product.', // Route
], function () {
    Route::get('/featured', 'ProductController@featured')->name('featured'); // product
    Route::get('/{product_id}/{product_name}', 'ProductController@show')->name('show'); // product
    
    Route::post('/comment/{product_id}', 'ProductController@productComment')->name('comment'); // comment
    Route::post('/reply/{comment_id}', 'ProductController@productReply')->name('reply'); // reply
});
