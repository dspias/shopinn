<?php

// Contact
Route::group([
    'prefix'        =>      'contact', // URL
    'namespace'     =>      'Contact', // Controller Path
    'as'            =>      'contact.', // Route
], function () {
    Route::get('/', 'ContactController@index')->name('index'); // contact index
    Route::post('/sendmail', 'ContactController@sendmail')->name('sendmail'); // contact index
});
