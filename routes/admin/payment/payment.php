<?php

// Payment Routes
Route::group([
    'prefix'        =>      'payments', // URL
    'namespace'     =>      'Payment', // Controller Path
    'as'            =>      'payment.', // Route
],
    function(){
        Route::get('/all', 'PaymentController@all')->name('all'); // All Payments
        Route::get('/create', 'PaymentController@create')->name('create'); // Make New Payment
        Route::get('/create/make_payment', 'PaymentController@createInvoice')->name('create.invoice'); // Make New Invoice
        Route::post('/create/invoice/{user_id}', 'PaymentController@invoice')->name('invoice'); // Make New Invoice
        Route::get('/show/{payment_id}', 'PaymentController@show')->name('show'); // Make New Payment
    }
);
