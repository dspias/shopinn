<?php

// Order Routes
Route::group(
    [
        'prefix'        =>      'orders', // URL
        'namespace'     =>      'Order', // Controller Path
        'as'            =>      'order.', // Route
    ],
    function () {
        Route::get('/all', 'OrderController@all')->name('all'); // All Orders
        Route::get('/pending', 'OrderController@pending')->name('pending'); // Pending Orders
        Route::get('/running', 'OrderController@running')->name('running'); // Running Orders
        Route::get('/completed', 'OrderController@completed')->name('completed'); // Completed Orders
        Route::get('/canceled', 'OrderController@canceled')->name('canceled'); // Canceled Orders
        // Route::get('/create', 'OrderController@create')->name('create'); // Create New Order

        Route::get('/details/{order_type}/{order_id}', 'OrderController@show')->name('show'); // Show Order Details

        Route::post('/orders/status/{order_id}/{value}', 'OrderController@status')->name('status');
        Route::get('/orders/status/{order_id}/{value}', 'OrderController@status')->name('status');
    }
);
