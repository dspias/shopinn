<?php
// Profile
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::get('', 'ProfileController@index')->name('index');
        Route::post('/update', 'ProfileController@update')->name('update');
    }
);
