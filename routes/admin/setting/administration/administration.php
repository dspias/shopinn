<?php

// Administration
Route::group([
    'prefix'        =>      'administration', // URL
    'namespace'     =>      'Administration', // Controller Path
    'as'            =>      'administration.', // Route
],
    function(){
        Route::get('/all', 'AdministrationController@all')->name('all'); // All Admins
        Route::get('/{page}/details/{admin_id}', 'AdministrationController@details')->name('details'); // details admin
        Route::get('/active', 'AdministrationController@active')->name('active'); // active Admins
        Route::get('/inactive', 'AdministrationController@inactive')->name('inactive'); // inactive Admins
        Route::get('/create', 'AdministrationController@create')->name('create'); // create Admins
        Route::get('/change_status/{admin_id}', 'AdministrationController@change_status')->name('change_status'); // change_status admin

        Route::post('/store', 'AdministrationController@store')->name('store'); // Store New admin
    }
);
