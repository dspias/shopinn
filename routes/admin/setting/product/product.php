<?php

// Product Routes
Route::group([
    'prefix'        =>      'products', // URL
    'namespace'     =>      'Product', // Controller Path
    'as'            =>      'product.', // Route
],
    function(){
        include_once 'type.php';
        include_once 'category.php';
    }
);
