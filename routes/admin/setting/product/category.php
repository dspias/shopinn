<?php

// Product Categories
Route::group([
    'prefix'        =>      'categories', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'category.', // Route
],
    function(){
        Route::get('/', 'CategoryController@index')->name('index'); // Product Categories

        Route::post('/store', 'CategoryController@store')->name('store'); // store Product Categories
        Route::post('/update', 'CategoryController@update')->name('update'); // update Product Categories
        Route::get('/change_status/{cat_id}', 'CategoryController@change_status')->name('change_status'); // change_status Product 
    }
);
