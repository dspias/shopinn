<?php

// Product Types
Route::group([
    'prefix'        =>      'types', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'type.', // Route
],
    function(){
        Route::get('/', 'TypeController@index')->name('index'); // Product Types

        Route::post('/store', 'TypeController@store')->name('store'); // store Product Types
        Route::post('/update', 'TypeController@update')->name('update'); // update Product Types
        Route::get('/change_status/{type_id}', 'TypeController@change_status')->name('change_status'); // change_status Product Types
    }
);
