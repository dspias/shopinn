<?php

// Company privacy
Route::group([
    'prefix'        =>      'privacy', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'privacy.', // Route
],
    function(){
        Route::get('/', 'PrivacyController@index')->name('index'); // Company privacy
        Route::post('/store', 'PrivacyController@store')->name('store'); // Company privacy
        Route::post('/update', 'PrivacyController@update')->name('update'); // Company privacy
        Route::get('/delete/{key}', 'PrivacyController@delete')->name('delete'); // Company privacy
    }
);
