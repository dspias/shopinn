<?php

// Company term
Route::group([
    'prefix'        =>      'term', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'term.', // Route
],
    function(){
        Route::get('/', 'TermController@index')->name('index'); // Company term
        Route::post('/store', 'TermController@store')->name('store'); // Company term
        Route::post('/update', 'TermController@update')->name('update'); // Company term
        Route::get('/delete/{key}', 'TermController@delete')->name('delete'); // Company term
    }
);
