<?php

// Company term
Route::group([
    'prefix'        =>      'faq', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'faq.', // Route
],
    function(){
        Route::get('/', 'FaqController@index')->name('index'); // Company FAQ
        Route::post('/store', 'FaqController@store')->name('store'); // Company FAQ
        Route::post('/update', 'FaqController@update')->name('update'); // Company FAQ
        Route::get('/delete/{key}', 'FaqController@delete')->name('delete'); // Company FAQ
    }
);
