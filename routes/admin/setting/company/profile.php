<?php

// Company profile
Route::group([
    'prefix'        =>      'profile', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'profile.', // Route
],
    function(){
        Route::get('/', 'ProfileController@index')->name('index'); // Company profile
    }
);
