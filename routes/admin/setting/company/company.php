<?php

// Company Routes
Route::group([
    'prefix'        =>      'companies', // URL
    'namespace'     =>      'Company', // Controller Path
    'as'            =>      'company.', // Route
],
    function(){
        include_once 'profile.php';
        include_once 'about.php';
        include_once 'privacy.php';
        include_once 'term.php';
        include_once 'faq.php';
    }
);
