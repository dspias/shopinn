<?php

// Company About
Route::group([
    'prefix'        =>      'about', // URL
    // 'namespace'     =>      '', // Controller Path
    'as'            =>      'about.', // Route
],
    function(){
        Route::get('/', 'AboutController@index')->name('index'); // Company About
        Route::post('/store', 'AboutController@store')->name('store'); // Company About
    }
);
