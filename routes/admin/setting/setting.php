<?php

// Setting Routes
Route::group([
    'prefix'        =>      'settings', // URL
    'namespace'     =>      'Setting', // Controller Path
    'as'            =>      'setting.', // Route
],
    function(){
        include_once 'administration/administration.php';
        include_once 'product/product.php';
        include_once 'company/company.php';
    }
);
