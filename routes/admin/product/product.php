<?php

// Product Routes
Route::group([
    'prefix'        =>      'products', // URL
    'namespace'     =>      'Product', // Controller Path
    'as'            =>      'product.', // Route
],
    function(){
        Route::get('/all', 'ProductController@all')->name('all'); // All Products
        Route::get('/pending', 'ProductController@pending')->name('pending'); // Pending Products
        Route::get('/active', 'ProductController@active')->name('active'); // active Products
        Route::get('/inactive', 'ProductController@inactive')->name('inactive'); // inactive Products
        Route::get('/create', 'ProductController@create')->name('create'); // Create New Product
        
        Route::get('/{page}/details/{product_id}', 'ProductController@details')->name('details'); // details Product
        Route::get('/change_status/{product_id}', 'ProductController@change_status')->name('change_status'); // change_status product
        Route::get('/approve/{product_id}', 'ProductController@approve')->name('approve'); // change_status product
        Route::post('/updatePrice/{product_id}', 'ProductController@updatePrice')->name('update_price'); // change_status product

        // Route::get('/{page}/details/{product_id}', 'ProductController@details')->name('details'); // details Product
    }
);
