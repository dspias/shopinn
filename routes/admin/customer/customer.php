<?php

// Customer Routes
Route::group([
    'prefix'        =>      'customers', // URL
    'namespace'     =>      'Customer', // Controller Path
    'as'            =>      'customer.', // Route
],
    function(){
        Route::get('/all', 'CustomerController@all')->name('all'); // All Customers
        Route::get('/{page}/details/{customer_id}', 'CustomerController@details')->name('details'); // details customer
        Route::get('/banned', 'CustomerController@banned')->name('banned'); // banned New Customer
        Route::get('/create', 'CustomerController@create')->name('create'); // banned New Customer
        Route::get('/change_status/{customer_id}', 'CustomerController@change_status')->name('change_status'); // change_status customer

        Route::post('/store', 'CustomerController@store')->name('store'); // Store New customer
    }
);
