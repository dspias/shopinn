<?php

// Artisan Routes
Route::group([
        'prefix'        =>      'pinikk/artisan', // URL
        'namespace'     =>      'Artisan', // Controller Path
        'as'            =>      'artisan.', // Route
    ],
    function () {
        Route::get('/', 'ArtisanController@index')->name('index'); // index Artisans
        Route::get('/{command}', 'ArtisanController@command')->name('command'); // command Artisans
    }
);
