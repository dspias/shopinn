<?php

// Shop Routes
Route::group(
    [
        'prefix'        =>      'shops', // URL
        'namespace'     =>      'Shop', // Controller Path
        'as'            =>      'shop.', // Route
    ],
    function () {
        Route::get('/all', 'ShopController@all')->name('all'); // All Shops
        Route::get('/{page}/details/{shop_id}', 'ShopController@details')->name('details'); // details Shop
        Route::get('/pending', 'ShopController@pending')->name('pending'); // Pending Shops
        Route::get('/active', 'ShopController@active')->name('active'); // active Shops
        Route::get('/inactive', 'ShopController@inactive')->name('inactive'); // inactive Shops
        Route::get('/create', 'ShopController@create')->name('create'); // Create New Shop
        Route::get('/change_status/{shop_id}', 'ShopController@change_status')->name('change_status'); // change_status Shop
        Route::post('/approve/{shop_id}', 'ShopController@approve')->name('approve'); // change_status Shop

        Route::post('/store', 'ShopController@store')->name('store'); // Store New Shop

        Route::post('/update_delivery/{shop_id}', 'ShopController@update_delivery')->name('update_delivery'); // Store New Shop

        Route::post('/{shop_id}/assign_pack', 'ShopController@assignPackage')->name('package.assign'); // Package Assign

        Route::get('/{shop_id}/reset_password', 'ShopController@resetPassword')->name('reset.password'); // Reset Password
    }
);
