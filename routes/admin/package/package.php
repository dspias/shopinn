<?php

// Package Routes
Route::group([
    'prefix'        =>      'packages', // URL
    'namespace'     =>      'Package', // Controller Path
    'as'            =>      'package.', // Route
],
    function(){
        Route::get('/all', 'PackageController@all')->name('all'); // All Packages
        Route::get('/active', 'PackageController@active')->name('active'); // active Packages
        Route::get('/inactive', 'PackageController@inactive')->name('inactive'); // inactive Packages
        Route::get('/create', 'PackageController@create')->name('create'); // Create New Package

        Route::get('/edit/{package_id}', 'PackageController@edit')->name('edit'); // edit New Package
        Route::get('/show/{package_id}', 'PackageController@show')->name('show'); // show New Package

        Route::post('/store', 'PackageController@store')->name('store'); // store New Package
        Route::post('/update/{package_id}', 'PackageController@update')->name('update'); // update New Package
    }
);
