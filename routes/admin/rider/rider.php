<?php

// Rider Routes
Route::group([
    'prefix'        =>      'riders', // URL
    'namespace'     =>      'Rider', // Controller Path
    'as'            =>      'rider.', // Route
],
    function(){
        Route::get('/all', 'RiderController@all')->name('all'); // All Riders
        Route::get('/{page}/details/{rider_id}', 'RiderController@details')->name('details'); // details Rider
        Route::get('/pending', 'RiderController@pending')->name('pending'); // Pending Riders
        Route::get('/active', 'RiderController@active')->name('active'); // active Riders
        Route::get('/inactive', 'RiderController@inactive')->name('inactive'); // inactive Riders
        Route::get('/create', 'RiderController@create')->name('create'); // Create New Rider
        Route::get('/change_status/{rider_id}', 'RiderController@change_status')->name('change_status'); // change_status Rider
        Route::get('/approve/{rider_id}', 'RiderController@approve')->name('approve'); // change_status Rider
        
        Route::post('/store', 'RiderController@store')->name('store'); // store New Rider
    }
);
