<?php

// Customer Routes
Route::group([
    'prefix' => 'dashboard', // URL
    'namespace' => 'Dashboard', // Controller Path
    'as' => 'dashboard.', // Route
],
    function(){
        Route::get('/', 'DashboardController@index')->name('index');
    }
);
