<?php

// Advertise Routes
Route::group(
    [
        'prefix'        =>      'advertises', // URL
        'namespace'     =>      'Advertise', // Controller Path
        'as'            =>      'advertise.', // Route
    ],
    function () {
        Route::get('/admin', 'AdvertiseController@index')->name('index'); // All Admin Advertises
        Route::get('/admin/show/{ad_id}', 'AdvertiseController@show')->name('show'); // Show Admin Advertises
        Route::get('/admin/edit/{ad_id}', 'AdvertiseController@edit')->name('edit'); // edit Admin Advertises
        Route::post('/admin/update/{ad_id}', 'AdvertiseController@update')->name('update'); // update Admin Advertises


        Route::get('/shop_ad_pack_log', 'AdvertiseController@shopAdPack')->name('shop.ad.pack'); // All pack log
        Route::get('/shop_ad_pack_log/show/{pack_id}', 'AdvertiseController@shopAdPackShow')->name('shop.ad.pack.show'); // All pack log
        Route::get('/change_log_status/{ad_id}', 'AdvertiseController@change_log_status')->name('change_log_status'); // Create New Advertise

        Route::get('/all', 'AdvertiseController@all')->name('all'); // All Advertises
        Route::get('/active', 'AdvertiseController@active')->name('active'); // active Advertises
        Route::get('/inactive', 'AdvertiseController@inactive')->name('inactive'); // inactive Advertises
        Route::get('/create', 'AdvertiseController@create')->name('create'); // Create New Advertise
        Route::post('/store', 'AdvertiseController@store')->name('store'); // store New Advertise
        Route::get('/all/view/{ad_id}', 'AdvertiseController@view')->name('view'); // Create New Advertise
        Route::get('/change_status/{ad_id}', 'AdvertiseController@change_status')->name('change_status'); // Create New Advertise


        // Settings
        include_once 'setting/type.php';
        include_once 'setting/package.php';
    }
);
