<?php

// Advertise Type
Route::group(
    [
        'prefix'        =>      'settings/types', // URL
        'namespace'     =>      'Setting', // Controller Path
        'as'            =>      'setting.type.', // Route
    ],
    function () {
        Route::get('/all', 'TypeController@all')->name('all'); // All Advertise Types
        Route::post('/store', 'TypeController@store')->name('store'); // Create Advertise Types
        Route::post('/update', 'TypeController@update')->name('update'); // Create Advertise Types
    }
);
