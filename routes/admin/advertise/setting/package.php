<?php

// Advertise Package
Route::group(
    [
        'prefix'        =>      'settings/packages', // URL
        'namespace'     =>      'Setting', // Controller Path
        'as'            =>      'setting.package.', // Route
    ],
    function () {
        Route::get('/all', 'PackageController@all')->name('all'); // All Advertise Packages

        Route::get('/create', 'PackageController@create')->name('create'); // Create Advertise Packages
        Route::post('/store', 'PackageController@store')->name('store'); // store Advertise Packages

        Route::get('/show/{package_id}', 'PackageController@show')->name('show'); // Show Advertise Packages

        Route::get('/edit/{package_id}', 'PackageController@edit')->name('edit'); // edit Advertise Packages
        Route::post('/update/{package_id}', 'PackageController@update')->name('update'); // update Advertise Packages

    }
);
