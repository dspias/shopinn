<?php

// Notification Routes
Route::group([
    'prefix'        =>      'notifications', // URL
    'namespace'     =>      'Notification', // Controller Path
    'as'            =>      'notification.', // Route
],
    function(){
        Route::get('/index', 'NotificationController@index')->name('index'); // All Shops
    }
);
