<?php

// Customer Routes
Route::group([
    'as' => 'admin.',
    'namespace' => 'Admin',
    'prefix' => 'admin',
],
    function(){
        include_once 'dashboard/dashboard.php';
        include_once 'order/order.php';
        include_once 'product/product.php';
        include_once 'shop/shop.php';
        include_once 'reseller/reseller.php';
        include_once 'rider/rider.php';
        include_once 'package/package.php';
        include_once 'advertise/advertise.php';
        include_once 'payment/payment.php';
        include_once 'customer/customer.php';
        include_once 'setting/setting.php';
        include_once 'profile/profile.php';
        include_once 'notificaitons/notificaitons.php';
        include_once 'artisan/artisan.php';
    }
);
