<?php

// Reseller Routes
Route::group([
    'prefix'        =>      'resellers', // URL
    'namespace'     =>      'Reseller', // Controller Path
    'as'            =>      'reseller.', // Route
],
    function(){
        Route::get('/all', 'ResellerController@all')->name('all'); // All Resellers
        Route::get('/{page}/details/{reseller_id}', 'ResellerController@details')->name('details'); // details Reseller
        Route::get('/pending', 'ResellerController@pending')->name('pending'); // Pending Resellers
        Route::get('/active', 'ResellerController@active')->name('active'); // active Resellers
        Route::get('/inactive', 'ResellerController@inactive')->name('inactive'); // inactive Resellers
        Route::get('/create', 'ResellerController@create')->name('create'); // Create New Reseller
        Route::get('/change_status/{reseller_id}', 'ResellerController@change_status')->name('change_status'); // change_status Reseller
        Route::get('/approve/{reseller_id}', 'ResellerController@approve')->name('approve'); // change_status Reseller

        Route::post('/store', 'ResellerController@store')->name('store'); // Store New Reseller

        Route::get('/commission', 'ResellerController@commission')->name('commission'); // reseller Commission
        Route::post('/commission', 'ResellerController@commissionStore')->name('commission.store'); // reseller Commission
    }
);
