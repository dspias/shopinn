<?php

// Customer Routes
Route::group([
    'prefix'        =>      '', // URL
    'namespace'     =>      'Backend', // Controller Path
    'as'            =>      'product.', // Route
],
    function(){
        

        Route::post('/store', 'Product@store')->name('store'); // Store New product
    }
);