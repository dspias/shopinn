<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::post('/login_with_email_or_mobile', 'Auth\LoginController@loginWithEmailOrMobile')->name('login_with_email_or_mobile');

// Socialite routes
Route::group(['as' => 'login.', 'prefix' => 'login', 'namespace' => 'Auth'], function () {
    Route::get('/{provider}', [LoginController::class, 'redirectToProvider'])->name('provider');
    Route::get('/{provider}/callback', [LoginController::class, 'handleProviderCallback'])->name('callback');
});


// Route::get('/home', 'HomeController@index')->name('home');
Route::post('/sendcode', 'Backend\VerificationController@sendcode')->name('sendcode');
Route::post('/checkcode', 'Backend\VerificationController@checkcode')->name('checkcode');

Route::get('/markasread/{id}', 'Backend\VerificationController@markasread')->name('markasread');


Route::group(
    [
        'middleware' => ['auth'],
    ],
    function () {
        Route::post('/change_password', 'Admin\Profile\ProfileController@change_password')->name('change_password');
    }
);


/*===================================
========< admin Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'admin'],
    ],
    function () {
        include_once 'admin/all.php';
    }
);




/*===================================
========< shop Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'shop'],
    ],
    function () {
        include_once 'shop/all.php';
    }
);




/*===================================
========< reseller Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'reseller'],
    ],
    function () {
        include_once 'reseller/all.php';
    }
);




/*===================================
========< rider Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'rider'],
    ],
    function () {
        include_once 'rider/all.php';
    }
);



/*===================================
========< customer Routes >=======
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'customer'],
    ],
    function () {
        include_once 'customer/all.php';
    }
);



/*===================================
========< web Routes >=======
===================================*/
Route::group(
    [
        // 'middleware' => ['auth', 'web'],
    ],
    function () {

        Route::get('customer/cart/', 'Customer\CartController@index')->name('customer.cart.index');
        include_once 'guest/all.php';
    }
);


/*===================================
========< api Routes >=======
===================================*/
// Route::group(
//     [
//         'middleware' => ['auth', 'web'],
//         'as'         => 'backend.', // Route
//     ],
//     function () {
//         include_once 'api/backend.php';
//     }
// );


/*===================================
========< Artisan routes >=======
===================================*/
// Route::group(
//     [
//     ],
//     function () {
//         include_once 'artisan/artisan.php';
//     }
// );
