<?php

// Customer Routes
Route::group(
    [
        'as' => 'order.',
        'prefix' => 'order',
    ],
    function () {
        Route::get('/', 'OrderController@index')->name('index');
        Route::get('/details/{order_type}/{order_id}', 'OrderController@show')->name('show');

        Route::post('/review/{order_id}', 'OrderController@reviewRating')->name('review');
    }
);
