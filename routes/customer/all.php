<?php

// Customer Routes
Route::group(
    [
        'as' => 'customer.',
        'namespace' => 'Customer',
        'prefix' => 'customer',
    ],
    function () {

        include_once 'dashboard/dashboard.php';
        include_once 'order/order.php';
        include_once 'payment/payment.php';
        include_once 'cart/cart.php';
        include_once 'profile/profile.php';
        include_once 'notificaitons/notificaitons.php';
    }
);
