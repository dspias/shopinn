<?php

// Customer Routes
Route::group(
    [
        'as' => 'payment.',
        'prefix' => 'payment',
    ],
    function () {
        Route::get('/', 'PaymentController@index')->name('index');
    }
);
