<?php

// Notification Routes
Route::group([
    'prefix'        =>      'notifications', // URL
    'as'            =>      'notification.', // Route
],
    function(){
        Route::get('/index', 'NotificationController@index')->name('index'); // All Shops
    }
);
