<?php

// Reseller Routes
Route::group(
    [
        'as' => 'product.',
        'prefix' => 'product',
    ],
    function () {
        Route::get('/add_product/{product_id}/{reseller_id}', 'ProductController@addProduct')->name('add');
        Route::get('/remove_product/{product_id}/{reseller_id}', 'ProductController@removeProduct')->name('remove');
        Route::get('/download/photos/{product_id}', 'ProductController@downloadPhotos')->name('download.photos');
        Route::get('/myProducts', 'ProductController@myProduct')->name('my');
    }
);
