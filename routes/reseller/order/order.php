<?php

// Reseller Routes
Route::group(
    [
        'as' => 'order.',
        'prefix' => 'order',
    ],
    function () {
        Route::get('/allOrder', 'OrderController@allOrder')->name('all');

        Route::get('/show/{order_id}', 'OrderController@show')->name('show');
    }
);
