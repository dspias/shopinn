<?php

// Customer Routes
Route::group([
    'as' => 'reseller.',
    'namespace' => 'Reseller',
    'prefix' => 'reseller',
    'middleware' => ['approved'],
],
    function(){
        include_once 'dashboard/dashboard.php';
        include_once 'order/order.php';
        include_once 'product/product.php';
        include_once 'cart/cart.php';
    }
);

Route::group([
    'as' => 'reseller.',
    'namespace' => 'Reseller',
    'prefix' => 'reseller',
],
    function(){
        include_once 'profile/profile.php';
    }
);
