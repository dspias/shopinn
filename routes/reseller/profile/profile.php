<?php

// Customer Routes
Route::group(
    [
        'as' => 'profile.',
        'prefix' => 'profile',
    ],
    function () {
        Route::get('/', 'ProfileController@index')->name('index');
        Route::post('/store_avatar', 'ProfileController@storeAvatar')->name('store.avatar');
    }
);
