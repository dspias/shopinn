<?php

// Customer Routes
Route::group(
    [
        'as' => 'cart.',
        'prefix' => 'cart',
    ],
    function () {
        Route::get('/', 'CartController@index')->name('index');
        Route::get('/checkout', 'CartController@checkout')->name('checkout');
        Route::post('/order/confirmation', 'CartController@confirmation')->name('confirmation');
    }
);
