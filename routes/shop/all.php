<?php

// Customer Routes
Route::group(
    [
        'as' => 'shop.',
        'namespace' => 'Shop',
        'prefix' => 'shop',
        'middleware' => ['approved', 'subsribed'],
    ],
    function () {
        include_once 'dashboard/dashboard.php';
        include_once 'profile/profile.php';

        include_once 'product/product.php';
        include_once 'payment/payment.php';
        include_once 'order/order.php';
        include_once 'setting/advertise.php';
        include_once 'setting/offer_coupon.php';
        include_once 'notificaitons/notificaitons.php';
    }
);
// Customer Routes
Route::group(
    [
        'as' => 'shop.',
        'namespace' => 'Shop',
        'prefix' => 'shop',
        'middleware' => ['approved'],
    ],
    function () {
        include_once 'setting/package.php';
    }
);

Route::group(
    [
        'as' => 'shop.',
        'namespace' => 'Shop',
        'prefix' => 'shop',
    ],
    function () {
        include_once 'setting/shop.php';
    }
);
