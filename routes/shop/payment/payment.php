<?php

// Customer Routes
Route::group(
    [
        'as' => 'payment.',
        'prefix' => 'payment',
    ],
    function () {
        Route::get('/payments', 'PaymentController@index')->name('index');
    }
);
