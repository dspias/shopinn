<?php

// Customer Routes
Route::group(
    [
        'as' => 'profile.',
        'prefix' => 'profile',
    ],
    function () {
        Route::get('/', 'ProfileController@index')->name('index');
        Route::get('/edit', 'ProfileController@edit')->name('edit');
        Route::post('/product/store', 'ProfileController@store')->name('product.store');
        Route::post('/product/update/{id}', 'ProfileController@update')->name('product.update');
    }
);
