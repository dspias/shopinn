<?php

// Shop Routes
Route::group(
    [
        'as' => 'setting.',
        'prefix' => 'settings',
    ],
    function () {
        Route::get('/offer', 'OfferCouponController@offerIndex')->name('offer.index');
        Route::get('/offer/create', 'OfferCouponController@offerCreate')->name('offer.create');
        Route::get('/offer/edit/{name}', 'OfferCouponController@offerEdit')->name('offer.edit');
        Route::post('/offer/store', 'OfferCouponController@offerStore')->name('offer.store');
        Route::post('/offer/update/{name}', 'OfferCouponController@offerUpdate')->name('offer.update');


        /**
         * ============================================================================
         * ===================================< Coupons >==============================
         * ============================================================================
         */

        Route::get('/coupon', 'OfferCouponController@couponIndex')->name('coupon.index');
        Route::get('/coupon/create', 'OfferCouponController@couponCreate')->name('coupon.create');
        Route::get('/coupon/edit/{id}', 'OfferCouponController@couponEdit')->name('coupon.edit');
        Route::post('/coupon/store', 'OfferCouponController@couponStore')->name('coupon.store');
        Route::post('/coupon/update/{id}', 'OfferCouponController@couponUpdate')->name('coupon.update');
    }
);
