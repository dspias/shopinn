<?php

// Shop Routes
Route::group(
    [
        'as' => 'setting.advertise.',
        'prefix' => 'settings/advertise',
    ],
    function () {
        Route::get('/', 'AdvertiseController@index')->name('index');
        Route::get('/package/register', 'AdvertiseController@package_register')->name('package.register');
        Route::post('/package/register/store', 'AdvertiseController@register')->name('package.register.store');

        Route::get('/create/{log_id}', 'AdvertiseController@create')->name('create');
        Route::post('/store/{log_id}', 'AdvertiseController@store')->name('store');
        Route::post('/update/{ad_id}', 'AdvertiseController@update')->name('update');
        Route::get('/show/{ad_id}', 'AdvertiseController@show')->name('show');
    }
);
