<?php

// Package Routes
Route::group(
    [
        'as' => 'setting.package.',
        'prefix' => 'settings',
    ],
    function () {
        Route::get('/package', 'PackageController@index')->name('index');
        Route::get('/package/buy/{package_id}', 'PackageController@buy')->name('buy');
        Route::get('/package/store/activeted/{package_id}', 'PackageController@activeted')->name('activeted');
    }
);
