<?php

// Shop Routes
Route::group(
    [
        'as' => 'setting.shop.',
        'prefix' => 'settings/shop',
    ],
    function () {
        Route::get('/', 'ShopController@index')->name('index');
        Route::post('/store_avatar', 'ShopController@storeAvatar')->name('store.avatar');
    }
);
