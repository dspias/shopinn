<?php

// Customer Routes
Route::group(
    [
        'as' => 'product.',
        'prefix' => 'product',
    ],
    function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::get('/review', 'ProductController@review')->name('review');
        Route::get('/featured', 'ProductController@featured')->name('featured');
        Route::get('/active', 'ProductController@active')->name('active');
        Route::get('/inactive', 'ProductController@inactive')->name('inactive');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::get('/edit/{product_id}', 'ProductController@edit')->name('edit');
        Route::get('/change_featured_status/{product_id}', 'ProductController@changeFeaturedStatus')->name('change_featured_status');
        
        Route::get('/change_status/{product_id}', 'ProductController@change_status')->name('change_status');
    }
);
