<?php

// Customer Routes
Route::group(
    [
        'as' => 'order.',
        'prefix' => 'order',
    ],
    function () {
        Route::get('/orders', 'OrderController@index')->name('index');
        Route::get('/orders/new', 'OrderController@new')->name('new');
        Route::get('/orders/running', 'OrderController@running')->name('running');
        Route::get('/orders/completed', 'OrderController@completed')->name('completed');
        Route::get('/orders/canceled', 'OrderController@canceled')->name('canceled');

        Route::get('/orders/{order_type}/{order_id}', 'OrderController@show')->name('show');

        Route::get('/orders/status/{order_id}/{value}', 'OrderController@status')->name('status');
    }
);
